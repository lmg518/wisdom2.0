#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

local_ip_addr=`ifconfig | grep inet | grep -v inet6 | grep -v "127.0.0.1" | awk '{print $2}'|cut -d':' -f2`
local_ip_port="3030"
server_pid_file=/home/envsafe/AirQueen/tmp/pids/server.pid
work_dir=/home/envsafe/AirQueen

if [ -e "${server_pid_file}" ]
then
  pid_number=`cat ${server_pid_file}`
  is_proc=`ps -ef | grep ${pid_number} | grep -v "grep" | wc -l`
  if [ ${is_proc} -eq 0 ]
  then
    cd ${work_dir}
    rails s -d -b ${local_ip_addr} -p ${local_ip_port}
  fi
  
else
  cd ${work_dir}
  rails s -d -b ${local_ip_addr} -p ${local_ip_port}
fi




