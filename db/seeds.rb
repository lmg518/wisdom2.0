# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#功能
@func = []
@func << shouye = SFuncMsg.find_or_create_by!( func_code: 'M100', func_name: '首页',    valid_flag: 1, root_distance: 1,queue_index: 1, func_url: '/search_five_datas', father_func_id: '9999',)
@func << yichang = SFuncMsg.find_or_create_by!( func_code: 'M200', func_name: '异常监控', valid_flag: 1, root_distance: 1,queue_index: 2, func_url: '/exception_monitorings', father_func_id: '9999',)
@func << baojing = SFuncMsg.find_or_create_by!( func_code: 'M300', func_name: '报警监控', valid_flag: 1, root_distance: 1,queue_index: 3, func_url: '/alarm_monitorings', father_func_id: '9999',)
@func << tongzhi = SFuncMsg.find_or_create_by!( func_code: 'M400', func_name: '报警通知', valid_flag: 1, root_distance: 1,queue_index: 4, func_url: '/alarm_notices', father_func_id: '9999',)
@func << lishi = SFuncMsg.find_or_create_by!( func_code: 'M500', func_name: '历史查询', valid_flag: 1, root_distance: 1,queue_index: 6, func_url: '/d_alarms', father_func_id: '9999',)
@func << baobiao = SFuncMsg.find_or_create_by!( func_code: 'M600', func_name: '运维报表', valid_flag: 1, root_distance: 1,queue_index: 7, func_url: '/superior_air_days', father_func_id: '9999',)
@func << titong = SFuncMsg.find_or_create_by!( func_code: 'M700', func_name: '系统设置', valid_flag: 1, root_distance: 1,queue_index: 8, func_url: '/d_stations', father_func_id: '9999',)
@func << SFuncMsg.find_or_create_by!( func_code: 'F301', func_name: '报警图表监控',       valid_flag: 0, root_distance: 2,queue_index: 1, func_url: '/alarm_monitorings', father_func_id: baojing.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F302', func_name: '报警汇总查看',       valid_flag: 0, root_distance: 2,queue_index: 2, func_url: '/alarm_lists', father_func_id: baojing.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F303', func_name: '报警地图查看',       valid_flag: 0, root_distance: 2,queue_index: 3, func_url: '/alarm_maps', father_func_id: baojing.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F501', func_name: '报警历史查询',       valid_flag: 0, root_distance: 2,queue_index: 1, func_url: '/d_alarms', father_func_id: lishi.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F601', func_name: '优良天数对比统计',    valid_flag: 0, root_distance: 2,queue_index: 1, func_url: '/superior_air_days', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F602', func_name: '单站点多污染分析统计', valid_flag: 0, root_distance: 2,queue_index: 2, func_url: '/single_station_pollutants_analysis', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F603', func_name: '多站点多污染分析统计', valid_flag: 0, root_distance: 2,queue_index: 3, func_url: '/more_station_pollutants_analysis', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F604', func_name: '站点有效性统计',      valid_flag: 0, root_distance: 2,queue_index: 4, func_url: '/data_statistics', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F605', func_name: '站点获取率统计',      valid_flag: 0, root_distance: 2,queue_index: 5, func_url: '/data_access_statistics', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F606', func_name: '站点离线统计',        valid_flag: 0, root_distance: 2,queue_index: 6, func_url: '/station_data_off', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F607', func_name: '报警数量统计',        valid_flag: 0, root_distance: 2,queue_index: 7, func_url: '/alarm_num_statistics', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F608', func_name: '报警占比分析',        valid_flag: 0, root_distance: 2,queue_index: 8, func_url: '/alarm_num_scales', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F609', func_name: '报警时段分析',        valid_flag: 0, root_distance: 2,queue_index: 9, func_url: '/alarm_num_time_analyse', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F610', func_name: '污染天气级别分布',     valid_flag: 0, root_distance: 2,queue_index: 10, func_url: '/contaminant_leves', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F611', func_name: '浓度值对比分析',       valid_flag: 0, root_distance: 2,queue_index: 11, func_url: '/concentration_analysis', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F612', func_name: '设备运行故障日报',     valid_flag: 0, root_distance: 2,queue_index: 12, func_url: '/facility_fault_day_reports', father_func_id: baobiao.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F701', func_name: '站点配置',     valid_flag: 0, root_distance: 2,queue_index: 1, func_url: '/d_stations', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F701', func_name: '工号管理',     valid_flag: 0, root_distance: 2,queue_index: 2, func_url: '/d_login_msgs', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F702', func_name: '角色权限',     valid_flag: 0, root_distance: 2,queue_index: 3, func_url: '/s_role_msgs', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F703', func_name: '异常配置',     valid_flag: 0, root_distance: 2,queue_index: 4, func_url: '/exception_sets', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F703', func_name: '报警设置',     valid_flag: 0, root_distance: 2,queue_index: 5, func_url: '/alarm_sets', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F704', func_name: '通知设置',     valid_flag: 0, root_distance: 2,queue_index: 6, func_url: '/alarm_notice_sets', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F705', func_name: '区域站点配置',     valid_flag: 0, root_distance: 2,queue_index: 7, func_url: '/handle_station_regions', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F706', func_name: 'SLA任务配置',     valid_flag: 0, root_distance: 2,queue_index: 8, func_url: '/s_slas', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F707', func_name: '项目工作',     valid_flag: 0, root_distance: 2,queue_index: 10, func_url: '/work_pros', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'F708', func_name: '单位信息管理',     valid_flag: 0, root_distance: 2,queue_index: 9, func_url: '/s_region_code', father_func_id: titong.id,)
@func << SFuncMsg.find_or_create_by!( func_code: 'M800', func_name: '报警任务',     valid_flag: 1, root_distance: 1,queue_index: 5, func_url: '/s_slas', father_func_id: 9999,)


#公司架构
level1 = SRegionLevel.find_or_create_by!(:level_name => "全国")
level2 = SRegionLevel.find_or_create_by!(:level_name => "办事处")
regin_code = SRegionCode.find_or_create_by!(:region_code => level1.level_name,:region_name=>'公司总部', :father_region_id => 99999, :s_region_level_id => level1.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'商丘运维主管', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'南阳信阳许昌主管', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'豫南主管', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'河北办事处', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'山西办事处', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'内蒙办事处', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'宁夏办事处', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'安徽办事处', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'黑龙江办事处', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'北京办事处', :father_region_id => regin_code.id, :s_region_level_id => level2.id)
SRegionCode.find_or_create_by!(:region_code => level2.level_name,:region_name=>'售后维修', :father_region_id => regin_code.id, :s_region_level_id => level2.id)

# 初始化角色

role =SRoleMsg.find_or_create_by!(:role_name=>'超级管理员',:valid_flag=>1)
@func.each do |func|
  SRoleFunc.find_or_create_by!(:s_func_msg_id => func.id, :s_role_msg_id => role.id )
end
#
DLoginMsg.find_or_create_by!(login_no: 'hzf', login_name: '超级管理员', password: '123456.', password_confirmation: '123456.',
                             s_role_msg_id: role.id,group_id: regin_code.id,
                             valid_flag: 1 )

#
# region = SRegionCode.find_or_create_by!( :region_code => 'CHNS',:region_name=>'河南省', :father_region_id => 99999, :s_region_level_id => 2)
# SRegionCode.find_or_create_by!( :region_code => 'C0375',:region_name=>'平顶山市', :father_region_id => region.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0376',:region_name=>'信阳市', :father_region_id => region.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0394',:region_name=>'周口市', :father_region_id => region.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0374',:region_name=>'许昌市', :father_region_id => region.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0395',:region_name=>'漯河市', :father_region_id => region.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0377',:region_name=>'南阳市', :father_region_id => region.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0370',:region_name=>'商丘市', :father_region_id => region.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0396',:region_name=>'驻马店市', :father_region_id => region.id, :s_region_level_id => 3)
#
# shb = SRegionCode.find_or_create_by!( :region_code => 'CHBS',:region_name=>'河北', :father_region_id => 99999, :s_region_level_id => 2)
# SRegionCode.find_or_create_by!( :region_code => 'C0311',:region_name=>'石家庄市', :father_region_id => shb.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0310',:region_name=>'邯郸市', :father_region_id => shb.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0317',:region_name=>'沧州市', :father_region_id => shb.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0318',:region_name=>'衡水市', :father_region_id => shb.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0319',:region_name=>'邢台市', :father_region_id => shb.id, :s_region_level_id => 3)
#
# ssx = SRegionCode.find_or_create_by!( :region_code => 'CSXS',:region_name=>'山西', :father_region_id => 99999, :s_region_level_id => 2)
# SRegionCode.find_or_create_by!( :region_code => 'C0351',:region_name=>'太原市', :father_region_id => ssx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0352',:region_name=>'大同市', :father_region_id => ssx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0353',:region_name=>'阳泉市', :father_region_id => ssx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0349',:region_name=>'朔州市', :father_region_id => ssx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0354',:region_name=>'晋中市', :father_region_id => ssx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0350',:region_name=>'忻州市', :father_region_id => ssx.id, :s_region_level_id => 3)
#
# snmg = SRegionCode.find_or_create_by!( :region_code => 'CNMG',:region_name=>'内蒙古', :father_region_id => 99999, :s_region_level_id => 2)
# SRegionCode.find_or_create_by!( :region_code => 'C0471',:region_name=>'呼和浩特市', :father_region_id => snmg.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0472',:region_name=>'包头市', :father_region_id => snmg.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0477',:region_name=>'鄂尔多斯市', :father_region_id => snmg.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0473',:region_name=>'乌海市', :father_region_id => snmg.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0478',:region_name=>'巴彦淖尔市', :father_region_id => snmg.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0483',:region_name=>'阿拉善盟', :father_region_id => snmg.id, :s_region_level_id => 3)
#
# shlj = SRegionCode.find_or_create_by!( :region_code => 'CHLJ',:region_name=>'黑龙江', :father_region_id => 99999, :s_region_level_id => 2)
# SRegionCode.find_or_create_by!( :region_code => 'C0451',:region_name=>'哈尔滨市', :father_region_id => shlj.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0452',:region_name=>'齐齐哈尔市', :father_region_id => shlj.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0459',:region_name=>'大庆市', :father_region_id => shlj.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0456',:region_name=>'黑河市', :father_region_id => shlj.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0455',:region_name=>'绥化市', :father_region_id => shlj.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0457',:region_name=>'大兴安岭地区', :father_region_id => shlj.id, :s_region_level_id => 3)
#
# sah = SRegionCode.find_or_create_by!( :region_code => 'CAHS',:region_name=>'安徽', :father_region_id => 99999, :s_region_level_id => 2)
# SRegionCode.find_or_create_by!( :region_code => 'C0551',:region_name=>'合肥市', :father_region_id => sah.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0553',:region_name=>'芜湖市', :father_region_id => sah.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0555',:region_name=>'马鞍山市', :father_region_id => sah.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0562',:region_name=>'铜陵市', :father_region_id => sah.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0556',:region_name=>'安庆市', :father_region_id => sah.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0559',:region_name=>'黄山市', :father_region_id => sah.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0566',:region_name=>'池州市', :father_region_id => sah.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0563',:region_name=>'宣城市', :father_region_id => sah.id, :s_region_level_id => 3)
#
# snx = SRegionCode.find_or_create_by!( :region_code => 'CNXS',:region_name=>'宁夏', :father_region_id => 99999, :s_region_level_id => 2)
# SRegionCode.find_or_create_by!( :region_code => 'C0951',:region_name=>'银川市', :father_region_id => snx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0952',:region_name=>'石嘴山市', :father_region_id => snx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0953',:region_name=>'吴忠市', :father_region_id => snx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0955',:region_name=>'中卫市', :father_region_id => snx.id, :s_region_level_id => 3)
# SRegionCode.find_or_create_by!( :region_code => 'C0954',:region_name=>'固原市', :father_region_id => snx.id, :s_region_level_id => 3)
# DLoginMsg.find_or_create_by!(:login_no => 'hzf',:login_name=>"超级管理员",:password=>'123456',
#                             :password_confirmation=>'123456',:group_id => region.id,:s_role_msg_id=>role.id)

# 归属地域
s1 = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CHNS', :zone_name=> '河南',:active_state=>true,:sort_num=>1)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0375', :zone_name=> '平顶山市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0376', :zone_name=> '信阳市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0394', :zone_name=> '周口市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0374', :zone_name=> '许昌市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0395', :zone_name=> '漯河市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0377', :zone_name=> '南阳市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0370', :zone_name=> '商丘市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0396', :zone_name=> '驻马店市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)

SAdministrativeArea.find_or_create_by!(:zone_rank => 'C010', :zone_name=> '北京市',:active_state=>true,:sort_num=>1)

hb = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CHBS', :zone_name=> '河北',:active_state=>true,:sort_num=>1)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0311', :zone_name=> '石家庄市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0310', :zone_name=> '邯郸市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0317', :zone_name=> '沧州市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0318', :zone_name=> '衡水市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0319', :zone_name=> '邢台市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)

sx = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CSXS', :zone_name=> '山西',:active_state=>true,:sort_num=>1)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0351', :zone_name=> '太原市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0352', :zone_name=> '大同市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0353', :zone_name=> '阳泉市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0349', :zone_name=> '朔州市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0354', :zone_name=> '晋中市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0350', :zone_name=> '忻州市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)

nmg = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CNMG', :zone_name=> '内蒙古',:active_state=>true,:sort_num=>1)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0471', :zone_name=> '呼和浩特市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0472', :zone_name=> '包头市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0477', :zone_name=> '鄂尔多斯市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0473', :zone_name=> '乌海市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0478', :zone_name=> '巴彦淖尔市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0483', :zone_name=> '阿拉善盟',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)

hlj = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CHLJ', :zone_name=> '黑龙江',:active_state=>true,:sort_num=>1)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0451', :zone_name=> '哈尔滨市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0452', :zone_name=> '齐齐哈尔市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0459', :zone_name=> '大庆市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0456', :zone_name=> '黑河市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0455', :zone_name=> '绥化市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0457', :zone_name=> '大兴安岭地区',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)

ah = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CAHS', :zone_name=> '安徽',:active_state=>true,:sort_num=>1)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0551', :zone_name=> '合肥市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0553', :zone_name=> '芜湖市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0555', :zone_name=> '马鞍山市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0562', :zone_name=> '铜陵市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0556', :zone_name=> '安庆市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0559', :zone_name=> '黄山市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0566', :zone_name=> '池州市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0563', :zone_name=> '宣城市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)

nx = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CNXS', :zone_name=> '宁夏',:active_state=>true,:sort_num=>1)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0951', :zone_name=> '银川市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0952', :zone_name=> '石嘴山市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0953', :zone_name=> '吴忠市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0955', :zone_name=> '中卫市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0954', :zone_name=> '固原市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)


#创建因子
SAbnormalLtem.find_or_create_by!(id: 101,item_code: 'ASO2', item_name: 'SO2', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 102,item_code: 'ANO2', item_name: 'NO2', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 103,item_code: 'ANO', item_name: 'NO', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 104,item_code: 'ANOX', item_name: 'NOX', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 105,item_code: 'ACO', item_name: 'CO', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 106,item_code: 'AO3', item_name: 'O3', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 107,item_code: 'APM2.5', item_name: 'PM2.5', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 108,item_code: 'APM10', item_name: 'PM10', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 201,item_code: 'AWIND', item_name: '风速', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 202,item_code: 'AWFX', item_name: '风向', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 203,item_code: 'AWHOT', item_name: '气温', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 204,item_code: 'AWPP', item_name: '气压', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 205,item_code: 'AWWTT', item_name: '湿度', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')
SAbnormalLtem.find_or_create_by!(id: 206,item_code: 'ALOO', item_name: '能见度', obj_type: '', data_cycle: 300, table_name: '', field_name: '', field_type: '', condition_claus: '')





# 创建规则分类
SAbnormalType.find_or_create_by!(rule_type: 'KZKCZ', type_name: '质控操作', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KLX', type_name: '离线', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KCLCF', type_name: '出零出负', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KDZ', type_name: '定值', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KSBGZ', type_name: '设备故障', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KSBLX', type_name: '设备离线', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KSJYC', type_name: '数据异常标识', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KDG', type_name: '倒挂', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KDBG', type_name: '对标高', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KDBD', type_name: '对标低', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KTRD', type_name: '突然低', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KTRG', type_name: '突然高', note: '')
SAbnormalType.find_or_create_by!(rule_type: 'KBDD', type_name: '波动大', note: '')

# 监控对象
SMonitorObjectType.find_or_create_by!( obj_type: 'JCSB', type_name: '监控设备')
SMonitorObjectType.find_or_create_by!( obj_type: 'JCHJ', type_name: '设备环境')
SMonitorObjectType.find_or_create_by!( obj_type: 'YWSJ', type_name: '业务数据')

#参考值代码
SAbnormalCompare.find_or_create_by!(compare_code: 'KSJBS', compare_name: '数据标示异常', compare_type: '标示')
SAbnormalCompare.find_or_create_by!(compare_code: 'KOFL', compare_name: '离线', compare_type: '动态')
SAbnormalCompare.find_or_create_by!(compare_code: 'KZERO', compare_name: '出零出负', compare_type: '定植')
SAbnormalCompare.find_or_create_by!(compare_code: 'KALL', compare_name: '定值', compare_type: '动态')
SAbnormalCompare.find_or_create_by!(compare_code: 'KERO', compare_name: '设备故障', compare_type: '动态')
SAbnormalCompare.find_or_create_by!(compare_code: 'KSOFL', compare_name: '设备离线', compare_type: '动态')
SAbnormalCompare.find_or_create_by!(compare_code: 'KSOFL', compare_name: '设备离线', compare_type: '动态')
SAbnormalCompare.find_or_create_by!(compare_code: 'KTOL', compare_name: '突然高', compare_type: '动态')
SAbnormalCompare.find_or_create_by!(compare_code: 'KDG', compare_name: '倒挂', compare_type: '定植')
SAbnormalCompare.find_or_create_by!(compare_code: 'KCXL', compare_name: '突然低', compare_type: '变量')
SAbnormalCompare.find_or_create_by!(compare_code: 'KDBH', compare_name: '对标高', compare_type: '变量')
SAbnormalCompare.find_or_create_by!(compare_code: 'KDBL', compare_name: '对标低', compare_type: '变量')
SAbnormalCompare.find_or_create_by!(compare_code: 'KBDD', compare_name: '波动大', compare_type: '变量')



# SAbnormalLtem.find_or_create_by!(id: 101,item_code: 'ASO2', item_name: 'SO2', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 102,item_code: 'ANO2', item_name: 'NO2', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 103,item_code: 'ANO', item_name: 'NO', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 104,item_code: 'ANOX', item_name: 'NOX', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 105,item_code: 'ACO', item_name: 'CO', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 106,item_code: 'AO3', item_name: 'O3', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 107,item_code: 'APM2.5', item_name: 'PM2.5', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 108,item_code: 'APM10', item_name: 'PM10', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 201,item_code: 'AWIND', item_name: '风速', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 202,item_code: 'AWFX', item_name: '风向', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 203,item_code: 'AWHOT', item_name: '气温', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 204,item_code: 'AWPP', item_name: '气压', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 205,item_code: 'AWWTT', item_name: '湿度', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')
# SAbnormalLtem.find_or_create_by!(id: 206,item_code: 'ALOO', item_name: '能见度', obj_type: '', data_cycle: 3600, table_name: '', field_name: '', field_type: '', condition_claus: '')

#标示表

FlagType.find_or_create_by!(flag_code: 'BB', flag_name: '连接不良', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'B', flag_name: '运行不良', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'W', flag_name: '等待数据恢复', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'PS', flag_name: '跨度检查', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'PZ', flag_name: '零点检查', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'AS', flag_name: '精度检查', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'CZ', flag_name: '零点校准', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'CS', flag_name: '跨度校准', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'RM', flag_name: '自动或人工审核为无效', note: '', type_id: 1)
FlagType.find_or_create_by!(flag_code: 'HSp', flag_name: '数据超上限', note: '', type_id: 7)
FlagType.find_or_create_by!(flag_code: 'LSp', flag_name: '数据超下限', note: '', type_id: 7)
FlagType.find_or_create_by!(flag_code: 'H', flag_name: '有效数据不足', note: '', type_id: 7)
FlagType.find_or_create_by!(flag_code: 'ZERO', flag_name: '出零出负', note: '', type_id: 3)
FlagType.find_or_create_by!(flag_code: 'TRG', flag_name: '突然高', note: '', type_id: 11)
FlagType.find_or_create_by!(flag_code: 'TRD', flag_name: '突然低', note: '', type_id: 11)
FlagType.find_or_create_by!(flag_code: 'DBGG', flag_name: '对标高', note: '', type_id: 9)
FlagType.find_or_create_by!(flag_code: 'DBGD', flag_name: '对标低', note: '', type_id: 9)
FlagType.find_or_create_by!(flag_code: 'DING', flag_name: '定值', note: '', type_id: 9)
FlagType.find_or_create_by!(flag_code: 'KDG', flag_name: '颗粒物倒挂', note: '', type_id: 9)
FlagType.find_or_create_by!(flag_code: 'LOST', flag_name: '站点质控离线', note: '', type_id: 8)
FlagType.find_or_create_by!(flag_code: 'D2DC1B', flag_name: '因子连续2天平均值超1倍', note: '', type_id: 8)

# 创建因子模版

# SAbnormalLtem.all.each do |item|
#   FlagType.all.each do |flag|
#     SAbnormmalRule.rule_create(item.id,flag.flag_code,flag.type_id)
#   end
# end

# 创建报警级别
SAlarmLevel.find_or_create_by!(level_name: '严重', level_desc: '', alarm_level: 3, level_color: 'red', note: '')
SAlarmLevel.find_or_create_by!(level_name: '较重', level_desc: '', alarm_level: 2, level_color: 'origin', note: '')
SAlarmLevel.find_or_create_by!(level_name: '一般', level_desc: '', alarm_level: 1, level_color: 'yellow', note: '')

#报警分类
SAlarmType.find_or_create_by!(alarm_type: '', type_name: '站点离线', note: '',)
SAlarmType.find_or_create_by!(alarm_type: '', type_name: '设备故障', note: '',)
SAlarmType.find_or_create_by!(alarm_type: '', type_name: '参数异变', note: '',)
SAlarmType.find_or_create_by!(alarm_type: '', type_name: '数据异常', note: '',)
SAlarmType.find_or_create_by!(alarm_type: '', type_name: '数据超标', note: '',)




