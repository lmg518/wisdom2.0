# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180605090851) do

  create_table "boot_checks", force: :cascade do |t|
    t.string   "check_project", limit: 255
    t.string   "check_site",    limit: 255
    t.string   "chcek_norm",    limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "crono_jobs", force: :cascade do |t|
    t.string   "job_id",            limit: 255,        null: false
    t.text     "log",               limit: 4294967295
    t.datetime "last_performed_at"
    t.boolean  "healthy"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "crono_jobs", ["job_id"], name: "index_crono_jobs_on_job_id", unique: true, using: :btree

  create_table "d_abnormal_data_yyyymms", force: :cascade do |t|
    t.string   "abnormal_id",                 limit: 255
    t.integer  "station_id",                  limit: 4
    t.string   "rule_instance",               limit: 255
    t.string   "create_acce",                 limit: 255
    t.string   "ab_lable",                    limit: 255
    t.string   "ab_data_time",                limit: 255
    t.integer  "ab_value",                    limit: 4
    t.integer  "compare_value",               limit: 4
    t.integer  "compare_value2",              limit: 4
    t.integer  "compare_value3",              limit: 4
    t.string   "ab_desc",                     limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "s_abnormal_rule_instance_id", limit: 4
    t.integer  "item_code_id",                limit: 4
  end

  add_index "d_abnormal_data_yyyymms", ["create_acce"], name: "index_d_abnormal_data_yyyymms_on_create_acce", using: :btree
  add_index "d_abnormal_data_yyyymms", ["item_code_id"], name: "index_d_abnormal_data_yyyymms_on_item_code_id", using: :btree
  add_index "d_abnormal_data_yyyymms", ["s_abnormal_rule_instance_id"], name: "index_d_abnormal_data_yyyymms_on_s_abnormal_rule_instance_id", using: :btree
  add_index "d_abnormal_data_yyyymms", ["station_id", "create_acce"], name: "index_d_abnormal_data_yyyymms_on_station_id_and_create_acce", using: :btree
  add_index "d_abnormal_data_yyyymms", ["station_id"], name: "index_d_abnormal_data_yyyymms_on_station_id", using: :btree

  create_table "d_accs", force: :cascade do |t|
    t.string   "unit_name",                limit: 255
    t.integer  "s_region_code_info_id",    limit: 4
    t.string   "city_name",                limit: 255
    t.integer  "s_administrative_area_id", limit: 4
    t.string   "brand",                    limit: 255
    t.string   "supply_name",              limit: 255
    t.string   "supply_no",                limit: 255
    t.string   "supply_num",               limit: 255
    t.string   "supply_unit",              limit: 255
    t.datetime "validity_date"
    t.string   "owner",                    limit: 255
    t.integer  "d_login_msg_id",           limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "d_accs", ["d_login_msg_id"], name: "index_d_accs_on_d_login_msg_id", using: :btree
  add_index "d_accs", ["s_administrative_area_id"], name: "index_d_accs_on_s_administrative_area_id", using: :btree
  add_index "d_accs", ["s_region_code_info_id"], name: "index_d_accs_on_s_region_code_info_id", using: :btree

  create_table "d_alarm_abnormal_rels", force: :cascade do |t|
    t.integer  "d_alarm_detail_id",           limit: 4
    t.integer  "s_abnormal_rule_instance_id", limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "d_alarm_abnormal_rels", ["d_alarm_detail_id"], name: "index_d_alarm_abnormal_rels_on_d_alarm_detail_id", using: :btree
  add_index "d_alarm_abnormal_rels", ["s_abnormal_rule_instance_id"], name: "index_d_alarm_abnormal_rels_on_s_abnormal_rule_instance_id", using: :btree

  create_table "d_alarm_despatch_alarm_rels", force: :cascade do |t|
    t.integer  "d_alarm_id",          limit: 4
    t.integer  "d_alarm_despatch_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "d_alarm_despatch_alarm_rels", ["d_alarm_despatch_id"], name: "index_d_alarm_despatch_alarm_rels_on_d_alarm_despatch_id", using: :btree
  add_index "d_alarm_despatch_alarm_rels", ["d_alarm_id"], name: "index_d_alarm_despatch_alarm_rels_on_d_alarm_id", using: :btree

  create_table "d_alarm_despatches", force: :cascade do |t|
    t.datetime "despatch_time"
    t.string   "despatch_login", limit: 255
    t.string   "task_status",    limit: 255
    t.string   "receive_login",  limit: 255
    t.datetime "receive_time"
    t.datetime "deal_time"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "d_login_msg_id", limit: 4
  end

  add_index "d_alarm_despatches", ["d_login_msg_id"], name: "index_d_alarm_despatches_on_d_login_msg_id", using: :btree

  create_table "d_alarm_details", force: :cascade do |t|
    t.integer  "alarm_detail_id",  limit: 4
    t.integer  "alarm_record_id",  limit: 4
    t.integer  "station_id",       limit: 4
    t.string   "alarm_rule",       limit: 255
    t.string   "last_sample_time", limit: 255
    t.integer  "ab_num",           limit: 4
    t.integer  "alarm_level",      limit: 4
    t.datetime "create_time"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "d_alarm_notices", force: :cascade do |t|
    t.integer  "alarm_id",       limit: 4
    t.integer  "alarm_level",    limit: 4
    t.string   "notice_type",    limit: 255
    t.string   "notice_login",   limit: 255
    t.string   "notice_content", limit: 255
    t.datetime "notice_time"
    t.string   "notice_accept",  limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "status",         limit: 255
    t.integer  "d_login_msg_id", limit: 4
  end

  add_index "d_alarm_notices", ["d_login_msg_id"], name: "index_d_alarm_notices_on_d_login_msg_id", using: :btree

  create_table "d_alarms", force: :cascade do |t|
    t.integer  "alarm_id",               limit: 4
    t.integer  "station_id",             limit: 4
    t.string   "alarm_rule",             limit: 255
    t.integer  "alarm_level",            limit: 4
    t.datetime "first_alarm_time"
    t.datetime "last_alarm_time"
    t.integer  "continuous_alarm_times", limit: 4
    t.string   "status",                 limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "s_alarm_rule_id",        limit: 4
  end

  add_index "d_alarms", ["s_alarm_rule_id"], name: "index_d_alarms_on_s_alarm_rule_id", using: :btree

  create_table "d_daily_reports", force: :cascade do |t|
    t.string   "datetime",         limit: 255
    t.float    "nums",             limit: 24
    t.float    "sum_nums",         limit: 24
    t.string   "status",           limit: 255
    t.string   "submit_man",       limit: 255
    t.datetime "summit_time"
    t.integer  "s_region_code_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",             limit: 255
    t.string   "out_nums",         limit: 255
    t.integer  "s_material_id",    limit: 4
    t.string   "code",             limit: 255
    t.float    "daily_yield",      limit: 24
  end

  add_index "d_daily_reports", ["datetime", "s_region_code_id", "name"], name: "idx_d_daily_reports", unique: true, using: :btree
  add_index "d_daily_reports", ["s_region_code_id"], name: "index_d_daily_reports_on_s_region_code_id", using: :btree

  create_table "d_data_analyzes", force: :cascade do |t|
    t.string   "station_name",      limit: 255
    t.integer  "data_type",         limit: 4
    t.datetime "data_time"
    t.integer  "hour_standard_num", limit: 4
    t.integer  "hour_num",          limit: 4
    t.integer  "min_standard_num",  limit: 4
    t.integer  "min_num",           limit: 4
    t.integer  "d_station_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",            limit: 255
  end

  add_index "d_data_analyzes", ["d_station_id"], name: "index_d_data_analyzes_on_d_station_id", using: :btree

  create_table "d_data_dailies", force: :cascade do |t|
    t.integer  "station_id",      limit: 4
    t.string   "station_name",    limit: 255
    t.datetime "data_time"
    t.integer  "avg_so2",         limit: 4
    t.integer  "iaqi_so2",        limit: 4
    t.integer  "avg_no2",         limit: 4
    t.integer  "iaqi_no2",        limit: 4
    t.float    "avg_co",          limit: 24
    t.integer  "iaqi_co",         limit: 4
    t.integer  "max_avg_1h_o3",   limit: 4
    t.integer  "iaqi_o3",         limit: 4
    t.integer  "max_ma8_o3",      limit: 4
    t.integer  "iaqi_ma8_o3",     limit: 4
    t.float    "avg_pm10",        limit: 24
    t.integer  "iaqi_pm10",       limit: 4
    t.float    "avg_pm25",        limit: 24
    t.integer  "iaqi_pm25",       limit: 4
    t.integer  "aqi",             limit: 4
    t.string   "chief_pollutant", limit: 255
    t.string   "aqi_level",       limit: 255
    t.string   "aqi_class",       limit: 255
    t.string   "aqi_color",       limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "d_data_dailies", ["data_time"], name: "index_d_data_dailies_on_data_time", using: :btree
  add_index "d_data_dailies", ["station_id"], name: "index_d_data_dailies_on_station_id", using: :btree

  create_table "d_data_hour_yyyys", force: :cascade do |t|
    t.integer  "station_id",     limit: 4
    t.string   "item_code",      limit: 255
    t.integer  "data_cycle",     limit: 4
    t.string   "data_attr",      limit: 255
    t.datetime "data_time"
    t.integer  "data_vale",      limit: 4
    t.string   "data_label",     limit: 255
    t.datetime "receive_time"
    t.integer  "receive_accept", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "d_data_hourly_yyyys", force: :cascade do |t|
    t.integer  "station_id",          limit: 4
    t.string   "station_name",        limit: 255
    t.datetime "data_time"
    t.integer  "avg_so2",             limit: 4
    t.string   "so2_label",           limit: 255
    t.integer  "iaqi_so2",            limit: 4
    t.integer  "avg_no2",             limit: 4
    t.string   "no2_label",           limit: 255
    t.integer  "iaqi_no2",            limit: 4
    t.integer  "avg_co",              limit: 4
    t.string   "co_label",            limit: 255
    t.integer  "iaqi_co",             limit: 4
    t.integer  "avg_o3",              limit: 4
    t.string   "o3_label",            limit: 255
    t.integer  "iaqi_o3",             limit: 4
    t.integer  "ma8_o3",              limit: 4
    t.integer  "iaqi_ma8_o3",         limit: 4
    t.integer  "avg_pm10",            limit: 4
    t.string   "pm10_label",          limit: 255
    t.integer  "ma24_pm10",           limit: 4
    t.integer  "iaqi_ma24_pm10",      limit: 4
    t.integer  "avg_pm25",            limit: 4
    t.string   "pm25_label",          limit: 255
    t.integer  "ma24_pm25",           limit: 4
    t.integer  "iaqi_ma24_pm25",      limit: 4
    t.float    "windspeed",           limit: 24
    t.string   "windspeed_label",     limit: 255
    t.float    "winddirection",       limit: 24
    t.string   "winddirection_label", limit: 255
    t.float    "temperature",         limit: 24
    t.string   "temperature_label",   limit: 255
    t.float    "barometer",           limit: 24
    t.string   "barometer_label",     limit: 255
    t.float    "humidity",            limit: 24
    t.string   "humidity_label",      limit: 255
    t.float    "visibility",          limit: 24
    t.string   "visibility_label",    limit: 255
    t.integer  "aqi",                 limit: 4
    t.string   "chief_pollutant",     limit: 255
    t.string   "aqi_level",           limit: 255
    t.string   "aqi_class",           limit: 255
    t.string   "aqi_color",           limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "d_data_hourly_yyyys", ["data_time"], name: "index_d_data_hourly_yyyys_on_data_time", using: :btree
  add_index "d_data_hourly_yyyys", ["station_id"], name: "index_d_data_hourly_yyyys_on_station_id", using: :btree

  create_table "d_data_yyyymms", force: :cascade do |t|
    t.integer  "station_id",     limit: 4
    t.integer  "item_code",      limit: 4
    t.integer  "data_cycle",     limit: 4
    t.string   "data_attr",      limit: 255
    t.datetime "data_time"
    t.string   "data_value",     limit: 255
    t.string   "data_label",     limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.datetime "receive_time"
    t.integer  "receive_accept", limit: 4
  end

  add_index "d_data_yyyymms", ["data_time", "data_label", "receive_time"], name: "data_lable_receive", using: :btree
  add_index "d_data_yyyymms", ["item_code"], name: "index_d_data_yyyymms_on_item_code", using: :btree
  add_index "d_data_yyyymms", ["receive_accept"], name: "index_d_data_yyyymms_on_receive_accept", using: :btree
  add_index "d_data_yyyymms", ["station_id", "data_cycle", "data_time"], name: "station_code_time", using: :btree
  add_index "d_data_yyyymms", ["station_id"], name: "index_d_data_yyyymms_on_station_id", using: :btree

  create_table "d_equip_works", force: :cascade do |t|
    t.integer  "s_equip_id",            limit: 4
    t.string   "equip_code",            limit: 255
    t.string   "work_person",           limit: 255
    t.datetime "work_start_time"
    t.datetime "work_end_time"
    t.string   "work_status",           limit: 255
    t.string   "work_desc",             limit: 255
    t.datetime "handle_time"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "equip_name",            limit: 255
    t.integer  "s_region_code_id",      limit: 4
    t.string   "region_name",           limit: 255
    t.string   "founder",               limit: 255
    t.integer  "s_area_id",             limit: 4
    t.integer  "s_region_code_info_id", limit: 4
  end

  create_table "d_fault_handles", force: :cascade do |t|
    t.string   "handle_man",     limit: 255
    t.string   "handle_type",    limit: 255
    t.string   "handle_des",     limit: 255
    t.string   "handle_remote",  limit: 255
    t.datetime "handle_time"
    t.string   "handle_forms",   limit: 255
    t.string   "img_one",        limit: 255
    t.string   "img_two",        limit: 255
    t.string   "img_there",      limit: 255
    t.string   "img_four",       limit: 255
    t.integer  "d_fault_job_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "d_task_form_id", limit: 4
  end

  add_index "d_fault_handles", ["d_fault_job_id"], name: "index_d_fault_handles_on_d_fault_job_id", using: :btree
  add_index "d_fault_handles", ["d_task_form_id"], name: "index_d_fault_handles_on_d_task_form_id", using: :btree

  create_table "d_fault_job_details", force: :cascade do |t|
    t.string   "job_status",        limit: 255
    t.string   "handle_man",        limit: 255
    t.datetime "begin_time"
    t.datetime "end_time"
    t.string   "note",              limit: 255
    t.string   "handle_status",     limit: 255
    t.string   "without_time_flag", limit: 255
    t.integer  "d_fault_job_id",    limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "d_task_form_id",    limit: 4
  end

  add_index "d_fault_job_details", ["d_fault_job_id"], name: "index_d_fault_job_details_on_d_fault_job_id", using: :btree
  add_index "d_fault_job_details", ["d_task_form_id"], name: "index_d_fault_job_details_on_d_task_form_id", using: :btree

  create_table "d_fault_jobs", force: :cascade do |t|
    t.string   "author",           limit: 255
    t.string   "handle_man",       limit: 255
    t.string   "handle_man_old",   limit: 255
    t.string   "job_no",           limit: 255
    t.string   "job_status",       limit: 255
    t.string   "fault_type",       limit: 255
    t.string   "create_type",      limit: 255
    t.string   "urgent_level",     limit: 255
    t.string   "send_type",        limit: 255
    t.integer  "d_station_id",     limit: 4
    t.string   "station_name",     limit: 255
    t.string   "fault_phenomenon", limit: 255
    t.string   "other_fault",      limit: 255
    t.string   "content",          limit: 255
    t.string   "audit_if",         limit: 255
    t.string   "audit_man",        limit: 255
    t.datetime "audit_time"
    t.string   "audit_des",        limit: 255
    t.integer  "s_region_code_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "title",            limit: 255
    t.string   "device_name",      limit: 255
  end

  add_index "d_fault_jobs", ["d_station_id"], name: "index_d_fault_jobs_on_d_station_id", using: :btree
  add_index "d_fault_jobs", ["s_region_code_id"], name: "index_d_fault_jobs_on_s_region_code_id", using: :btree

  create_table "d_form_headers", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "d_groups", force: :cascade do |t|
    t.string   "group_name",   limit: 255
    t.string   "group_des",    limit: 255
    t.string   "group_num",    limit: 255
    t.string   "group_status", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "d_localizes", force: :cascade do |t|
    t.string   "login_name",     limit: 255
    t.datetime "get_time"
    t.string   "long_login",     limit: 255
    t.string   "lat_login",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "d_login_msg_id", limit: 4
    t.string   "wx_id",          limit: 255
    t.string   "uuid",           limit: 255
  end

  add_index "d_localizes", ["d_login_msg_id"], name: "index_d_localizes_on_d_login_msg_id", using: :btree

  create_table "d_login_msgs", force: :cascade do |t|
    t.string   "login_no",                  limit: 255, null: false
    t.string   "login_name",                limit: 255, null: false
    t.string   "password",                  limit: 255
    t.string   "password_confirmation",     limit: 255
    t.string   "password_digest",           limit: 255, null: false
    t.integer  "group_id",                  limit: 4,   null: false
    t.integer  "s_role_msg_id",             limit: 4,   null: false
    t.string   "valid_flag",                limit: 255
    t.date     "valid_begin"
    t.date     "valid_end"
    t.string   "ip_address",                limit: 255
    t.string   "describe",                  limit: 255
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "session",                   limit: 255
    t.integer  "s_administrative_areas_id", limit: 4
    t.string   "telphone",                  limit: 255
    t.string   "id_card",                   limit: 255
    t.string   "email",                     limit: 255
    t.string   "work_num",                  limit: 255
    t.string   "work_status",               limit: 255
    t.integer  "s_region_code_info_id",     limit: 4
    t.integer  "s_area_id",                 limit: 4
  end

  add_index "d_login_msgs", ["login_no"], name: "index_d_login_msgs_on_login_no", unique: true, using: :btree
  add_index "d_login_msgs", ["s_administrative_areas_id"], name: "index_d_login_msgs_on_s_administrative_areas_id", using: :btree
  add_index "d_login_msgs", ["s_region_code_info_id"], name: "index_d_login_msgs_on_s_region_code_info_id", using: :btree

  create_table "d_logion_task_lines", force: :cascade do |t|
    t.integer  "d_task_form_id", limit: 4
    t.string   "login_name",     limit: 255
    t.datetime "get_time"
    t.string   "long_login",     limit: 255
    t.string   "lat_login",      limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "d_logion_task_lines", ["d_task_form_id"], name: "index_d_logion_task_lines_on_d_task_form_id", using: :btree

  create_table "d_material_regin_codes", force: :cascade do |t|
    t.integer  "s_region_code_id",   limit: 4
    t.integer  "s_material_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "d_relation_type_id", limit: 4
  end

  add_index "d_material_regin_codes", ["s_material_id"], name: "index_d_material_regin_codes_on_s_material_id", using: :btree
  add_index "d_material_regin_codes", ["s_region_code_id", "s_material_id", "d_relation_type_id"], name: "idx_d_material_regin_codes", unique: true, using: :btree
  add_index "d_material_regin_codes", ["s_region_code_id"], name: "index_d_material_regin_codes_on_s_region_code_id", using: :btree

  create_table "d_month_reports", force: :cascade do |t|
    t.string   "datetime",         limit: 255
    t.float    "nums",             limit: 24
    t.float    "sum_nums",         limit: 24
    t.string   "status",           limit: 255
    t.string   "submit_man",       limit: 255
    t.datetime "summit_time"
    t.integer  "s_region_code_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code",             limit: 255
    t.string   "out_nums",         limit: 255
  end

  add_index "d_month_reports", ["s_region_code_id"], name: "index_d_month_reports_on_s_region_code_id", using: :btree

  create_table "d_notice_infos", force: :cascade do |t|
    t.string   "auther",             limit: 255
    t.string   "templete_name",      limit: 255
    t.integer  "d_templete_id",      limit: 4
    t.string   "title",              limit: 255
    t.datetime "send_time"
    t.string   "send_type",          limit: 255
    t.string   "content",            limit: 255
    t.string   "status",             limit: 255
    t.integer  "d_notice_person_id", limit: 4
    t.string   "other",              limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "d_notice_infos", ["d_templete_id"], name: "index_d_notice_infos_on_d_templete_id", using: :btree

  create_table "d_notice_workers", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "describe",       limit: 255
    t.string   "telphone",       limit: 255
    t.string   "wee_chat",       limit: 255
    t.string   "qq_num",         limit: 255
    t.string   "email",          limit: 255
    t.string   "work_num",       limit: 255
    t.string   "work_status",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "d_login_msg_id", limit: 4
  end

  add_index "d_notice_workers", ["d_login_msg_id"], name: "index_d_notice_workers_on_d_login_msg_id", using: :btree

  create_table "d_ops_plan_details", force: :cascade do |t|
    t.integer  "d_ops_plan_manage_id", limit: 4
    t.string   "job_name",             limit: 255
    t.string   "job_flag",             limit: 255
    t.integer  "d_work_pro_id",        limit: 4
    t.datetime "job_time"
    t.string   "job_checked",          limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "d_ops_plan_details", ["d_ops_plan_manage_id"], name: "index_d_ops_plan_details_on_d_ops_plan_manage_id", using: :btree
  add_index "d_ops_plan_details", ["d_work_pro_id"], name: "index_d_ops_plan_details_on_d_work_pro_id", using: :btree

  create_table "d_ops_plan_manages", force: :cascade do |t|
    t.integer  "d_station_id",             limit: 4
    t.string   "station_name",             limit: 255
    t.integer  "s_administrative_area_id", limit: 4
    t.string   "zone_name",                limit: 255
    t.integer  "s_region_code_info_id",    limit: 4
    t.string   "unit_name",                limit: 255
    t.string   "edit_status",              limit: 255
    t.datetime "week_begin_time"
    t.datetime "week_end_time"
    t.string   "month_flag",               limit: 255
    t.string   "week_flag",                limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "report_if",                limit: 255
    t.datetime "report_time"
  end

  add_index "d_ops_plan_manages", ["d_station_id"], name: "index_d_ops_plan_manages_on_d_station_id", using: :btree
  add_index "d_ops_plan_manages", ["s_administrative_area_id"], name: "index_d_ops_plan_manages_on_s_administrative_area_id", using: :btree
  add_index "d_ops_plan_manages", ["s_region_code_info_id"], name: "index_d_ops_plan_manages_on_s_region_code_info_id", using: :btree

  create_table "d_plants", force: :cascade do |t|
    t.string   "status",           limit: 255
    t.string   "name",             limit: 255
    t.integer  "s_region_code_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "d_plants", ["s_region_code_id"], name: "index_d_plants_on_s_region_code_id", using: :btree

  create_table "d_power_cut_audit_logs", force: :cascade do |t|
    t.string   "audit_cause",           limit: 255
    t.string   "note",                  limit: 255
    t.integer  "d_power_cut_record_id", limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "d_power_cut_audit_logs", ["d_power_cut_record_id"], name: "index_d_power_cut_audit_logs_on_d_power_cut_record_id", using: :btree

  create_table "d_power_cut_records", force: :cascade do |t|
    t.integer  "d_station_id",          limit: 4
    t.string   "station_name",          limit: 255
    t.string   "unit_yw",               limit: 255
    t.integer  "s_region_code_id",      limit: 4
    t.datetime "begin_time"
    t.datetime "end_time"
    t.float    "time_range",            limit: 24
    t.string   "exception_device_type", limit: 255
    t.string   "handle_man",            limit: 255
    t.string   "origin",                limit: 255
    t.string   "note",                  limit: 255
    t.string   "img_path",              limit: 255
    t.string   "audit_if",              limit: 255
    t.string   "audit_man",             limit: 255
    t.datetime "audit_time"
    t.string   "audit_des",             limit: 255
    t.string   "job_status",            limit: 255
    t.integer  "d_login_msg_id",        limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "exception_name",        limit: 255
  end

  add_index "d_power_cut_records", ["d_login_msg_id"], name: "index_d_power_cut_records_on_d_login_msg_id", using: :btree
  add_index "d_power_cut_records", ["d_station_id"], name: "index_d_power_cut_records_on_d_station_id", using: :btree
  add_index "d_power_cut_records", ["s_region_code_id"], name: "index_d_power_cut_records_on_s_region_code_id", using: :btree

  create_table "d_relation_types", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "d_report_form_headers", force: :cascade do |t|
    t.integer  "d_report_form_id", limit: 4
    t.integer  "d_form_header_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "d_report_form_headers", ["d_form_header_id"], name: "index_d_report_form_headers_on_d_form_header_id", using: :btree
  add_index "d_report_form_headers", ["d_report_form_id"], name: "index_d_report_form_headers_on_d_report_form_id", using: :btree

  create_table "d_report_forms", force: :cascade do |t|
    t.string   "code",                  limit: 255
    t.string   "name",                  limit: 255
    t.integer  "s_region_code_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "s_nenrgy_form_type_id", limit: 255
  end

  add_index "d_report_forms", ["s_region_code_id"], name: "index_d_report_forms_on_s_region_code_id", using: :btree

  create_table "d_station_rule_rels", force: :cascade do |t|
    t.integer  "d_station_id",                limit: 4
    t.integer  "s_abnormal_rule_instance_id", limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "d_stations", force: :cascade do |t|
    t.string   "station_name",              limit: 255, null: false
    t.string   "region_code",               limit: 255, null: false
    t.string   "station_type",              limit: 255, null: false
    t.string   "object_id",                 limit: 255
    t.string   "station_location",          limit: 255, null: false
    t.string   "longitude",                 limit: 255, null: false
    t.string   "latitude",                  limit: 255, null: false
    t.string   "station_ip",                limit: 255
    t.string   "station_mac",               limit: 255
    t.string   "service_no",                limit: 255
    t.datetime "setup_date"
    t.string   "station_status",            limit: 255
    t.string   "is_online",                 limit: 255
    t.string   "note",                      limit: 255
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "s_administrative_areas_id", limit: 4
    t.integer  "s_administrative_area_id",  limit: 4
    t.string   "match_string",              limit: 255
    t.integer  "wc_station_id",             limit: 4
    t.string   "distance",                  limit: 255
    t.integer  "arrive_time",               limit: 4
    t.integer  "s_region_code_info_id",     limit: 4
  end

  add_index "d_stations", ["s_administrative_area_id"], name: "index_d_stations_on_s_administrative_area_id", using: :btree
  add_index "d_stations", ["s_administrative_areas_id"], name: "index_d_stations_on_s_administrative_areas_id", using: :btree
  add_index "d_stations", ["s_region_code_info_id"], name: "index_d_stations_on_s_region_code_info_id", using: :btree
  add_index "d_stations", ["wc_station_id"], name: "index_d_stations_on_wc_station_id", using: :btree

  create_table "d_steam_region_codes", force: :cascade do |t|
    t.integer  "s_region_code_id",      limit: 4
    t.integer  "s_material_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "s_nenrgy_form_type_id", limit: 4
  end

  add_index "d_steam_region_codes", ["s_material_id"], name: "index_d_steam_region_codes_on_s_material_id", using: :btree
  add_index "d_steam_region_codes", ["s_region_code_id"], name: "index_d_steam_region_codes_on_s_region_code_id", using: :btree

  create_table "d_supplies", force: :cascade do |t|
    t.string   "unit_name",                limit: 255
    t.integer  "s_region_code_info_id",    limit: 4
    t.string   "city_name",                limit: 255
    t.integer  "s_administrative_area_id", limit: 4
    t.string   "brand",                    limit: 255
    t.string   "supply_name",              limit: 255
    t.string   "supply_no",                limit: 255
    t.string   "supply_num",               limit: 255
    t.string   "supply_unit",              limit: 255
    t.datetime "validity_date"
    t.string   "owner",                    limit: 255
    t.integer  "d_login_msg_id",           limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "d_supplies", ["d_login_msg_id"], name: "index_d_supplies_on_d_login_msg_id", using: :btree
  add_index "d_supplies", ["s_administrative_area_id"], name: "index_d_supplies_on_s_administrative_area_id", using: :btree
  add_index "d_supplies", ["s_region_code_info_id"], name: "index_d_supplies_on_s_region_code_info_id", using: :btree

  create_table "d_task_deal_records", force: :cascade do |t|
    t.integer  "task_id",      limit: 4
    t.integer  "flow_no",      limit: 4
    t.string   "task_status",  limit: 255
    t.string   "deal_login",   limit: 255
    t.integer  "deal_type_id", limit: 4
    t.string   "note",         limit: 255
    t.datetime "deal_time"
    t.string   "deal_accept",  limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "d_task_forms", force: :cascade do |t|
    t.string   "author",                limit: 255
    t.string   "handle_man",            limit: 255
    t.string   "handle_man_old",        limit: 255
    t.string   "job_no",                limit: 255
    t.string   "job_status",            limit: 255
    t.string   "fault_type",            limit: 255
    t.string   "create_type",           limit: 255
    t.string   "urgent_level",          limit: 255
    t.string   "send_type",             limit: 255
    t.integer  "d_station_id",          limit: 4
    t.string   "station_name",          limit: 255
    t.string   "device_name",           limit: 255
    t.datetime "polling_plan_time"
    t.integer  "polling_ops_type",      limit: 4
    t.string   "polling_job_type",      limit: 255
    t.string   "clraring_if",           limit: 255
    t.datetime "abnormal_begin_time"
    t.datetime "abnormal_end_time"
    t.string   "abnormal_notes",        limit: 255
    t.string   "fault_phenomenon",      limit: 255
    t.string   "other_fault",           limit: 255
    t.string   "title",                 limit: 255
    t.string   "content",               limit: 255
    t.string   "audit_if",              limit: 255
    t.string   "audit_man",             limit: 255
    t.datetime "audit_time"
    t.string   "audit_des",             limit: 255
    t.integer  "s_region_code_id",      limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "handle_man_id",         limit: 4
    t.integer  "s_region_code_info_id", limit: 4
    t.integer  "s_equipment_info_id",   limit: 4
    t.string   "img_one",               limit: 255
    t.string   "form_module_id",        limit: 255
    t.string   "review_man",            limit: 255
    t.datetime "review_time"
    t.string   "review_des",            limit: 255
    t.string   "review_if",             limit: 255
    t.string   "pro_type",              limit: 255
  end

  add_index "d_task_forms", ["d_station_id"], name: "index_d_task_forms_on_d_station_id", using: :btree
  add_index "d_task_forms", ["handle_man_id"], name: "index_d_task_forms_on_handle_man_id", using: :btree
  add_index "d_task_forms", ["polling_ops_type"], name: "index_d_task_forms_on_polling_ops_type", using: :btree
  add_index "d_task_forms", ["s_equipment_info_id"], name: "index_d_task_forms_on_s_equipment_info_id", using: :btree
  add_index "d_task_forms", ["s_region_code_id"], name: "index_d_task_forms_on_s_region_code_id", using: :btree
  add_index "d_task_forms", ["s_region_code_info_id"], name: "index_d_task_forms_on_s_region_code_info_id", using: :btree

  create_table "d_templetes", force: :cascade do |t|
    t.string   "create_user", limit: 255
    t.string   "modifier",    limit: 255
    t.datetime "modify_time"
    t.string   "name",        limit: 255
    t.string   "title",       limit: 255
    t.string   "type",        limit: 255
    t.string   "content",     limit: 255
    t.string   "status",      limit: 255
    t.string   "other",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "d_work_pros", force: :cascade do |t|
    t.string   "work_name",  limit: 255
    t.string   "work_code",  limit: 255
    t.integer  "work_type",  limit: 4
    t.string   "work_flag",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "short_name", limit: 255
  end

  create_table "d_worker_groups", force: :cascade do |t|
    t.integer  "d_notice_worker_id", limit: 4
    t.integer  "d_group_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "d_worker_groups", ["d_group_id"], name: "index_d_worker_groups_on_d_group_id", using: :btree
  add_index "d_worker_groups", ["d_notice_worker_id"], name: "index_d_worker_groups_on_d_notice_worker_id", using: :btree

  create_table "day_hour_falg_data", force: :cascade do |t|
    t.integer  "region_code_id", limit: 4
    t.string   "province",       limit: 255
    t.string   "city",           limit: 255
    t.integer  "d_station_id",   limit: 4
    t.string   "station_name",   limit: 255
    t.integer  "item_id",        limit: 4
    t.string   "item_name",      limit: 255
    t.string   "item_flag",      limit: 255
    t.integer  "item_value",     limit: 4
    t.datetime "acce_data"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "day_hour_falg_data", ["d_station_id"], name: "index_day_hour_falg_data_on_d_station_id", using: :btree
  add_index "day_hour_falg_data", ["item_id"], name: "index_day_hour_falg_data_on_item_id", using: :btree
  add_index "day_hour_falg_data", ["region_code_id"], name: "index_day_hour_falg_data_on_region_code_id", using: :btree

  create_table "day_station_hour_lengths", force: :cascade do |t|
    t.integer  "d_station_id", limit: 4
    t.string   "station_name", limit: 255
    t.integer  "so2",          limit: 4
    t.integer  "co",           limit: 4
    t.integer  "no2",          limit: 4
    t.integer  "o3",           limit: 4
    t.integer  "pm2_5",        limit: 4
    t.integer  "pm10",         limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.datetime "day_time"
  end

  add_index "day_station_hour_lengths", ["d_station_id"], name: "index_day_station_hour_lengths_on_d_station_id", using: :btree

  create_table "day_valid_data", force: :cascade do |t|
    t.integer  "d_station_id",      limit: 4
    t.string   "station_name",      limit: 255
    t.float    "so2_avg",           limit: 24
    t.float    "so2_val_max",       limit: 24
    t.float    "so2_val_min",       limit: 24
    t.integer  "so2_count",         limit: 4
    t.integer  "so2_valid_count",   limit: 4
    t.integer  "so2_vain_count",    limit: 4
    t.float    "no2_avg",           limit: 24
    t.float    "no2_val_max",       limit: 24
    t.float    "no2_val_min",       limit: 24
    t.integer  "no2_count",         limit: 4
    t.integer  "no2_valid_count",   limit: 4
    t.integer  "no2_vain_count",    limit: 4
    t.float    "o3_avg",            limit: 24
    t.float    "o3_val_max",        limit: 24
    t.float    "o3_val_min",        limit: 24
    t.integer  "o3_count",          limit: 4
    t.integer  "o3_valid_count",    limit: 4
    t.integer  "o3_vain_count",     limit: 4
    t.float    "co_avg",            limit: 24
    t.float    "co_val_max",        limit: 24
    t.float    "co_val_min",        limit: 24
    t.integer  "co_count",          limit: 4
    t.integer  "co_valid_count",    limit: 4
    t.integer  "co_vain_count",     limit: 4
    t.float    "pm2_5_avg",         limit: 24
    t.float    "pm2_5_val_max",     limit: 24
    t.float    "pm2_5_val_min",     limit: 24
    t.integer  "pm2_5_count",       limit: 4
    t.integer  "pm2_5_valid_count", limit: 4
    t.integer  "pm2_5_vain_count",  limit: 4
    t.float    "pm10_avg",          limit: 24
    t.float    "pm10_val_max",      limit: 24
    t.float    "pm10_val_min",      limit: 24
    t.integer  "pm10_count",        limit: 4
    t.integer  "pm10_valid_count",  limit: 4
    t.integer  "pm10_vain_count",   limit: 4
    t.datetime "day_time"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "day_valid_data", ["d_station_id"], name: "index_day_valid_data_on_d_station_id", using: :btree

  create_table "f_field_titles", force: :cascade do |t|
    t.string   "code",             limit: 255
    t.string   "name",             limit: 255
    t.string   "title_if_value",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "f_form_module_id", limit: 4
    t.string   "field_type",       limit: 255
  end

  add_index "f_field_titles", ["f_form_module_id"], name: "index_f_field_titles_on_f_form_module_id", using: :btree

  create_table "f_field_values", force: :cascade do |t|
    t.string   "field_title",      limit: 255
    t.string   "field_value",      limit: 255
    t.string   "field_type",       limit: 255
    t.integer  "tr_row",           limit: 4
    t.integer  "tr_col",           limit: 4
    t.integer  "title_row",        limit: 4
    t.integer  "title_col",        limit: 4
    t.integer  "field_width",      limit: 4
    t.integer  "font_size",        limit: 4
    t.integer  "d_task_form_id",   limit: 4
    t.integer  "f_form_module_id", limit: 4
    t.integer  "f_field_title_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "f_field_values", ["d_task_form_id"], name: "index_f_field_values_on_d_task_form_id", using: :btree
  add_index "f_field_values", ["f_field_title_id"], name: "index_f_field_values_on_f_field_title_id", using: :btree
  add_index "f_field_values", ["f_form_module_id"], name: "index_f_field_values_on_f_form_module_id", using: :btree

  create_table "f_form_modules", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "module_type",    limit: 4
    t.integer  "d_work_pro_id",  limit: 4
    t.string   "field_title_id", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code",           limit: 255
  end

  add_index "f_form_modules", ["d_work_pro_id"], name: "index_f_form_modules_on_d_work_pro_id", using: :btree

  create_table "f_module_images", force: :cascade do |t|
    t.string   "img_one",          limit: 255
    t.string   "img_two",          limit: 255
    t.string   "img_three",        limit: 255
    t.string   "img_four",         limit: 255
    t.integer  "d_task_form_id",   limit: 4
    t.integer  "f_form_module_id", limit: 4
    t.string   "module_name",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cons_name",        limit: 255
    t.string   "num",              limit: 255
  end

  add_index "f_module_images", ["d_task_form_id"], name: "index_f_module_images_on_d_task_form_id", using: :btree
  add_index "f_module_images", ["f_form_module_id"], name: "index_f_module_images_on_f_form_module_id", using: :btree

  create_table "file_categories", force: :cascade do |t|
    t.string   "category_name",  limit: 255
    t.integer  "id_sort_num",    limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "parent_id",      limit: 4
    t.integer  "lft",            limit: 4
    t.integer  "rgt",            limit: 4
    t.integer  "depth",          limit: 4
    t.integer  "children_count", limit: 4
  end

  create_table "file_maintenances", force: :cascade do |t|
    t.string   "title",            limit: 255
    t.text     "content",          limit: 65535
    t.string   "flag",             limit: 255
    t.integer  "file_category_id", limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "author",           limit: 255
    t.integer  "d_login_msg_id",   limit: 4
  end

  add_index "file_maintenances", ["d_login_msg_id"], name: "index_file_maintenances_on_d_login_msg_id", using: :btree
  add_index "file_maintenances", ["file_category_id"], name: "index_file_maintenances_on_file_category_id", using: :btree

  create_table "flag_types", force: :cascade do |t|
    t.string   "flag_code",  limit: 255
    t.string   "flag_name",  limit: 255
    t.string   "note",       limit: 255
    t.integer  "type_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "h_equip_boots", force: :cascade do |t|
    t.integer  "handle_equip_id", limit: 4
    t.integer  "boot_check_id",   limit: 4
    t.string   "check_project",   limit: 255
    t.string   "check_status",    limit: 255
    t.string   "exec_desc",       limit: 255
    t.datetime "handle_time"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "h_equip_spares", force: :cascade do |t|
    t.integer  "handle_equip_id", limit: 4
    t.text     "spare_name",      limit: 65535
    t.datetime "change_time"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "h_overhaul_notes", force: :cascade do |t|
    t.integer  "handle_equip_id", limit: 4
    t.string   "note",            limit: 255
    t.string   "img_path",        limit: 255
    t.string   "sort_out",        limit: 255
    t.string   "overhaul_desc",   limit: 255
    t.string   "note_type",       limit: 255
    t.datetime "overhaul_time"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "h_overhaul_notes", ["handle_equip_id"], name: "index_h_overhaul_notes_on_handle_equip_id", using: :btree

  create_table "handle_equips", force: :cascade do |t|
    t.integer  "s_equip_id",        limit: 4
    t.string   "equip_code",        limit: 255
    t.string   "handle_name",       limit: 255
    t.datetime "handle_start_time"
    t.datetime "handle_end_time"
    t.string   "snag_desc",         limit: 255
    t.string   "status",            limit: 255
    t.string   "desc",              limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "equip_name",        limit: 255
    t.integer  "s_region_code_id",  limit: 4
    t.string   "region_name",       limit: 255
    t.string   "founder",           limit: 255
    t.string   "img_path",          limit: 255
  end

  add_index "handle_equips", ["s_equip_id"], name: "index_handle_equips_on_s_equip_id", using: :btree

  create_table "notice_rule_roles", force: :cascade do |t|
    t.integer  "s_notice_rule_id", limit: 4
    t.integer  "s_role_msg_id",    limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "notice_rule_roles", ["s_notice_rule_id"], name: "index_notice_rule_roles_on_s_notice_rule_id", using: :btree
  add_index "notice_rule_roles", ["s_role_msg_id"], name: "index_notice_rule_roles_on_s_role_msg_id", using: :btree

  create_table "s_abnormal_compares", force: :cascade do |t|
    t.string   "compare_code", limit: 255
    t.string   "compare_name", limit: 255
    t.string   "compare_type", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "s_abnormal_ltems", force: :cascade do |t|
    t.string   "item_code",       limit: 255
    t.string   "item_name",       limit: 255
    t.string   "obj_type",        limit: 255
    t.integer  "data_cycle",      limit: 4
    t.string   "table_name",      limit: 255
    t.string   "field_name",      limit: 255
    t.string   "field_type",      limit: 255
    t.string   "condition_claus", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "s_abnormal_rule_instances", force: :cascade do |t|
    t.string   "rule_instance",       limit: 255
    t.string   "instance_name",       limit: 255
    t.string   "rule_code",           limit: 255
    t.integer  "parameter_value",     limit: 4
    t.integer  "parameter_value2",    limit: 4
    t.integer  "parameter_value3",    limit: 4
    t.integer  "parameter_value4",    limit: 4
    t.string   "status",              limit: 255
    t.string   "note",                limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "s_abnormal_ltem_id",  limit: 4
    t.integer  "s_abnormmal_rule_id", limit: 4
    t.string   "item_code",           limit: 255
  end

  add_index "s_abnormal_rule_instances", ["s_abnormal_ltem_id"], name: "index_s_abnormal_rule_instances_on_s_abnormal_ltem_id", using: :btree
  add_index "s_abnormal_rule_instances", ["s_abnormmal_rule_id"], name: "index_s_abnormal_rule_instances_on_s_abnormmal_rule_id", using: :btree

  create_table "s_abnormal_types", force: :cascade do |t|
    t.string   "rule_type",  limit: 255
    t.string   "type_name",  limit: 255
    t.string   "note",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "s_abnormmal_rules", force: :cascade do |t|
    t.string   "rule_code",           limit: 255
    t.string   "ab_lable",            limit: 255
    t.string   "item_code",           limit: 255
    t.string   "rule_type",           limit: 255
    t.string   "rule_name",           limit: 255
    t.string   "rule_expression",     limit: 255
    t.string   "rule_expression_des", limit: 255
    t.string   "compare_code",        limit: 255
    t.string   "compare_code2",       limit: 255
    t.string   "comoare_code3",       limit: 255
    t.integer  "parameter_num",       limit: 4
    t.string   "note",                limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "s_abnormal_type_id",  limit: 4
    t.integer  "s_abnormal_ltem_id",  limit: 4
  end

  add_index "s_abnormmal_rules", ["s_abnormal_ltem_id"], name: "index_s_abnormmal_rules_on_s_abnormal_ltem_id", using: :btree
  add_index "s_abnormmal_rules", ["s_abnormal_type_id"], name: "index_s_abnormmal_rules_on_s_abnormal_type_id", using: :btree

  create_table "s_administrative_areas", force: :cascade do |t|
    t.string   "zone_name",    limit: 255
    t.string   "zone_rank",    limit: 255
    t.integer  "parent_id",    limit: 4
    t.integer  "lft",          limit: 4,   null: false
    t.integer  "rgt",          limit: 4,   null: false
    t.string   "des",          limit: 255
    t.boolean  "active_state"
    t.integer  "sort_num",     limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "s_administrative_areas", ["lft"], name: "index_product_categories_on_lft", using: :btree
  add_index "s_administrative_areas", ["parent_id"], name: "index_product_categories_on_parent_id", using: :btree
  add_index "s_administrative_areas", ["rgt"], name: "index_product_categories_on_rgt", using: :btree

  create_table "s_alarm_levels", force: :cascade do |t|
    t.string   "level_name",  limit: 255
    t.string   "level_desc",  limit: 255
    t.integer  "alarm_level", limit: 4
    t.string   "level_color", limit: 255
    t.string   "note",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "s_alarm_rule_levels", force: :cascade do |t|
    t.string   "rule_code",       limit: 255
    t.integer  "alarm_level",     limit: 4
    t.integer  "ab_times",        limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "s_alarm_rule_id", limit: 4
  end

  add_index "s_alarm_rule_levels", ["s_alarm_rule_id"], name: "index_s_alarm_rule_levels_on_s_alarm_rule_id", using: :btree

  create_table "s_alarm_rules", force: :cascade do |t|
    t.string   "rule_code",        limit: 255
    t.string   "rule_name",        limit: 255
    t.string   "rule_type",        limit: 255
    t.string   "rule_desc",        limit: 255
    t.string   "ab_rule_instance", limit: 255
    t.integer  "deal_interval",    limit: 4
    t.integer  "sample_num",       limit: 4
    t.string   "status",           limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "rule_instance_id", limit: 4
    t.integer  "rule_type_id",     limit: 4
  end

  add_index "s_alarm_rules", ["rule_instance_id"], name: "index_s_alarm_rules_on_rule_instance_id", using: :btree
  add_index "s_alarm_rules", ["rule_type_id"], name: "index_s_alarm_rules_on_rule_type_id", using: :btree

  create_table "s_alarm_status", force: :cascade do |t|
    t.string   "status",      limit: 255
    t.string   "status_name", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "s_alarm_types", force: :cascade do |t|
    t.string   "alarm_type", limit: 255
    t.string   "type_name",  limit: 255
    t.string   "note",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "s_area_equips", force: :cascade do |t|
    t.integer  "s_equip_id",       limit: 4
    t.integer  "s_area_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "s_region_code_id", limit: 4
  end

  add_index "s_area_equips", ["s_area_id"], name: "index_s_area_equips_on_s_area_id", using: :btree
  add_index "s_area_equips", ["s_equip_id"], name: "index_s_area_equips_on_s_equip_id", using: :btree

  create_table "s_area_logins", force: :cascade do |t|
    t.integer  "d_station_id",   limit: 4
    t.integer  "d_login_msg_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "s_areas", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.string   "location",   limit: 255
    t.string   "note",       limit: 255
    t.string   "is_leaf",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "s_brand_msgs", force: :cascade do |t|
    t.string   "brand",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "s_data_items", force: :cascade do |t|
    t.string   "item_type",      limit: 255
    t.string   "item_code",      limit: 255
    t.string   "item_name",      limit: 255
    t.string   "data_type",      limit: 255
    t.string   "data_unit",      limit: 255
    t.integer  "data_precision", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "s_equip_calibs", force: :cascade do |t|
    t.string   "calibs_man",     limit: 255
    t.string   "calibs_if",      limit: 255
    t.string   "calibs_des",     limit: 255
    t.datetime "calibs_time"
    t.string   "calibs_forms",   limit: 255
    t.string   "img_one",        limit: 255
    t.string   "img_two",        limit: 255
    t.string   "img_three",      limit: 255
    t.string   "img_four",       limit: 255
    t.integer  "d_task_form_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "s_equip_calibs", ["d_task_form_id"], name: "index_s_equip_calibs_on_d_task_form_id", using: :btree

  create_table "s_equipment_infos", force: :cascade do |t|
    t.string   "equip_type",     limit: 255
    t.string   "equip_brands",   limit: 255
    t.integer  "equip_num",      limit: 4
    t.string   "equip_code",     limit: 255
    t.string   "fault_des",      limit: 255
    t.string   "qualified_if",   limit: 255
    t.string   "type",           limit: 255
    t.string   "status",         limit: 255
    t.datetime "purchase_time"
    t.datetime "shelves_time"
    t.string   "add_man",        limit: 255
    t.string   "stand_num",      limit: 255
    t.integer  "d_task_form_id", limit: 4,   null: false
    t.integer  "d_station_id",   limit: 4,   null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "equip_nums",     limit: 255
  end

  add_index "s_equipment_infos", ["d_station_id"], name: "index_s_equipment_infos_on_d_station_id", using: :btree
  add_index "s_equipment_infos", ["d_task_form_id"], name: "index_s_equipment_infos_on_d_task_form_id", using: :btree

  create_table "s_equips", force: :cascade do |t|
    t.integer  "s_region_code_id", limit: 4
    t.string   "region_name",      limit: 255
    t.string   "bit_code",         limit: 255
    t.string   "equip_code",       limit: 255
    t.string   "equip_name",       limit: 255
    t.string   "equip_location",   limit: 255
    t.string   "equip_norm",       limit: 255
    t.string   "equip_nature",     limit: 255
    t.string   "equip_material",   limit: 255
    t.integer  "equip_num",        limit: 4
    t.string   "apper_code",       limit: 255
    t.datetime "apper_time"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "equip_status",     limit: 255
    t.string   "factory",          limit: 255
    t.string   "equip_note",       limit: 255
    t.integer  "s_area_id",        limit: 4
  end

  create_table "s_func_msgs", force: :cascade do |t|
    t.string   "func_code",       limit: 255, null: false
    t.string   "func_name",       limit: 255, null: false
    t.string   "valid_flag",      limit: 255, null: false
    t.integer  "root_distance",   limit: 4,   null: false
    t.integer  "queue_index",     limit: 4,   null: false
    t.string   "func_url",        limit: 255
    t.string   "new_window_flag", limit: 255
    t.string   "func_note",       limit: 255
    t.integer  "father_func_id",  limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "s_login_report_forms", force: :cascade do |t|
    t.integer  "d_login_msg_id",   limit: 4
    t.integer  "d_report_form_id", limit: 4
    t.integer  "s_region_code_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "s_login_report_forms", ["d_login_msg_id"], name: "index_s_login_report_forms_on_d_login_msg_id", using: :btree
  add_index "s_login_report_forms", ["d_report_form_id"], name: "index_s_login_report_forms_on_d_report_form_id", using: :btree
  add_index "s_login_report_forms", ["s_region_code_id"], name: "index_s_login_report_forms_on_s_region_code_id", using: :btree

  create_table "s_maint_projects", force: :cascade do |t|
    t.string   "maint_name",     limit: 255
    t.string   "maint_code",     limit: 255
    t.string   "work_flag",      limit: 255
    t.integer  "d_task_form_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "s_maint_projects", ["d_task_form_id"], name: "index_s_maint_projects_on_d_task_form_id", using: :btree

  create_table "s_materials", force: :cascade do |t|
    t.string   "mate_type",  limit: 255
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.string   "desc",       limit: 255
    t.string   "batch_num",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "s_monitor_object_types", force: :cascade do |t|
    t.string   "obj_type",   limit: 255
    t.string   "type_name",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "s_month_week_plans", force: :cascade do |t|
    t.string   "week_num",        limit: 255
    t.datetime "week_begin_time"
    t.datetime "week_end_time"
    t.string   "week_time",       limit: 255
    t.string   "month_time",      limit: 255
    t.string   "year_time",       limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "s_nenrgy_form_region_codes", force: :cascade do |t|
    t.integer  "s_nenrgy_form_type_id", limit: 4
    t.integer  "s_region_code_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "s_nenrgy_form_region_codes", ["s_nenrgy_form_type_id"], name: "index_s_nenrgy_form_region_codes_on_s_nenrgy_form_type_id", using: :btree
  add_index "s_nenrgy_form_region_codes", ["s_region_code_id"], name: "index_s_nenrgy_form_region_codes_on_s_region_code_id", using: :btree

  create_table "s_nenrgy_form_types", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "s_nenrgy_values", force: :cascade do |t|
    t.string   "datetime",         limit: 255
    t.integer  "s_material_id",    limit: 4
    t.string   "field_name",       limit: 255
    t.string   "field_code",       limit: 255
    t.string   "field_value",      limit: 255
    t.integer  "d_report_form_id", limit: 4
    t.integer  "s_region_code_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",           limit: 255
  end

  add_index "s_nenrgy_values", ["d_report_form_id"], name: "index_s_nenrgy_values_on_d_report_form_id", using: :btree
  add_index "s_nenrgy_values", ["s_material_id"], name: "index_s_nenrgy_values_on_s_material_id", using: :btree
  add_index "s_nenrgy_values", ["s_region_code_id"], name: "index_s_nenrgy_values_on_s_region_code_id", using: :btree

  create_table "s_notice_rules", force: :cascade do |t|
    t.integer  "alarm_level",          limit: 4
    t.string   "notice_type",          limit: 255
    t.integer  "role_id",              limit: 4
    t.integer  "up_region_level",      limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "title",                limit: 255
    t.string   "content",              limit: 255
    t.integer  "s_notice_template_id", limit: 4
  end

  add_index "s_notice_rules", ["s_notice_template_id"], name: "index_s_notice_rules_on_s_notice_template_id", using: :btree

  create_table "s_notice_templates", force: :cascade do |t|
    t.string   "template_name",     limit: 255
    t.integer  "alarm_rule",        limit: 4
    t.integer  "abnormal_instance", limit: 4
    t.integer  "station_info",      limit: 4
    t.integer  "login_name",        limit: 4
    t.integer  "abnormal_time",     limit: 4
    t.integer  "alarm_time",        limit: 4
    t.integer  "alarm_last_time",   limit: 4
    t.integer  "alarm_num",         limit: 4
    t.integer  "alarm_status",      limit: 4
    t.string   "template_des",      limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "s_region_code_areas", force: :cascade do |t|
    t.integer  "s_region_code_id", limit: 4
    t.integer  "s_area_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "s_region_code_areas", ["s_area_id"], name: "index_s_region_code_areas_on_s_area_id", using: :btree
  add_index "s_region_code_areas", ["s_region_code_id"], name: "index_s_region_code_areas_on_s_region_code_id", using: :btree

  create_table "s_region_code_infos", force: :cascade do |t|
    t.integer  "s_region_code_id", limit: 4
    t.string   "linkman",          limit: 255
    t.string   "linkman_tel",      limit: 255
    t.string   "unit_address",     limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "unit_name",        limit: 255
    t.string   "unit_code",        limit: 255
    t.string   "status",           limit: 255
    t.string   "station_id",       limit: 255
  end

  add_index "s_region_code_infos", ["s_region_code_id"], name: "index_s_region_code_infos_on_s_region_code_id", using: :btree

  create_table "s_region_codes", force: :cascade do |t|
    t.string   "region_code",       limit: 255, null: false
    t.string   "region_name",       limit: 255, null: false
    t.integer  "s_region_level_id", limit: 4
    t.string   "is_leaf",           limit: 255
    t.integer  "father_region_id",  limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "s_region_levels", force: :cascade do |t|
    t.string   "level_name", limit: 255, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "s_role_funcs", force: :cascade do |t|
    t.integer  "s_func_msg_id", limit: 4, null: false
    t.integer  "s_role_msg_id", limit: 4, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "s_role_msgs", force: :cascade do |t|
    t.string   "role_name",     limit: 255, null: false
    t.string   "valid_flag",    limit: 255, null: false
    t.integer  "root_distance", limit: 4
    t.integer  "queue_index",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "s_slas", force: :cascade do |t|
    t.integer  "alarm_level",      limit: 4
    t.integer  "receive_minutes",  limit: 4
    t.integer  "arrive_minutes",   limit: 4
    t.integer  "recovery_minutes", limit: 4
    t.integer  "deal_minutes",     limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "s_software_versions", force: :cascade do |t|
    t.string   "station_type", limit: 255, null: false
    t.string   "version_no",   limit: 255, null: false
    t.datetime "release_date",             null: false
    t.string   "note",         limit: 255
    t.string   "version_link", limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "s_spares", force: :cascade do |t|
    t.integer  "s_equip_id",     limit: 4
    t.string   "equip_code",     limit: 255
    t.string   "spare_code",     limit: 255
    t.string   "spare_name",     limit: 255
    t.string   "spare_desc",     limit: 255
    t.string   "spare_material", limit: 255
    t.integer  "spare_num",      limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "s_station_types", force: :cascade do |t|
    t.string   "station_type", limit: 255, null: false
    t.string   "type_name",    limit: 255, null: false
    t.string   "note",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "s_task_deal_types", force: :cascade do |t|
    t.string   "type_name",           limit: 255
    t.string   "form_status",         limit: 255
    t.string   "to_status",           limit: 255
    t.integer  "d_alarm_despatch_id", limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "s_technic_values", force: :cascade do |t|
    t.string   "date_time",        limit: 255
    t.integer  "s_technic_id",     limit: 4
    t.string   "field_code",       limit: 255
    t.string   "field_value",      limit: 255
    t.integer  "d_report_form_id", limit: 4
    t.integer  "s_region_code_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "s_technic_values", ["d_report_form_id"], name: "index_s_technic_values_on_d_report_form_id", using: :btree
  add_index "s_technic_values", ["s_region_code_id"], name: "index_s_technic_values_on_s_region_code_id", using: :btree
  add_index "s_technic_values", ["s_technic_id"], name: "index_s_technic_values_on_s_technic_id", using: :btree

  create_table "s_technics", force: :cascade do |t|
    t.string   "technic_01",    limit: 255
    t.string   "technic_02",    limit: 255
    t.string   "technic_03",    limit: 255
    t.string   "technic_04",    limit: 255
    t.string   "technic_05",    limit: 255
    t.string   "technic_06",    limit: 255
    t.string   "technic_07",    limit: 255
    t.string   "technic_08",    limit: 255
    t.string   "technic_09",    limit: 255
    t.string   "technic_10",    limit: 255
    t.string   "technic_11",    limit: 255
    t.string   "technic_12",    limit: 255
    t.string   "technic_13",    limit: 255
    t.string   "technic_14",    limit: 255
    t.string   "technic_15",    limit: 255
    t.string   "technic_16",    limit: 255
    t.string   "technic_17",    limit: 255
    t.string   "technic_18",    limit: 255
    t.string   "technic_19",    limit: 255
    t.string   "technic_20",    limit: 255
    t.string   "technic_21",    limit: 255
    t.string   "technic_22",    limit: 255
    t.string   "technic_23",    limit: 255
    t.string   "technic_24",    limit: 255
    t.string   "technic_25",    limit: 255
    t.string   "technic_26",    limit: 255
    t.string   "technic_27",    limit: 255
    t.string   "technic_28",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "datetime",      limit: 255
    t.string   "status",        limit: 255
    t.string   "material_code", limit: 255
  end

  create_table "sessions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "station_item_avgs", force: :cascade do |t|
    t.string   "d_station_id", limit: 255
    t.float    "co_avg",       limit: 24
    t.float    "co2_avg",      limit: 24
    t.float    "o3_avg",       limit: 24
    t.float    "pm10_avg",     limit: 24
    t.float    "pm2_5_avg",    limit: 24
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "station_name", limit: 255
    t.float    "so2_avg",      limit: 24
    t.datetime "day_time"
    t.float    "no2_avg",      limit: 24
  end

  add_index "station_item_avgs", ["d_station_id"], name: "index_station_item_avgs_on_d_station_id", using: :btree

  create_table "station_region_code_ids", force: :cascade do |t|
    t.integer  "d_station_id",     limit: 4, null: false
    t.integer  "s_region_code_id", limit: 4, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "station_region_code_ids", ["d_station_id"], name: "index_station_region_code_ids_on_d_station_id", using: :btree
  add_index "station_region_code_ids", ["s_region_code_id"], name: "index_station_region_code_ids_on_s_region_code_id", using: :btree

  create_table "test_iframes", force: :cascade do |t|
    t.string   "mark",       limit: 255
    t.string   "yangna",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "version_ups", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "url",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "w_login_oprs", force: :cascade do |t|
    t.string   "op_code",     limit: 255, null: false
    t.datetime "op_time",                 null: false
    t.string   "login_no",    limit: 255, null: false
    t.string   "ip_addr",     limit: 255
    t.string   "op_note",     limit: 255
    t.string   "op_describe", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "wc_d_data_days", force: :cascade do |t|
    t.integer  "RECEIVE_ACCEPT", limit: 4
    t.string   "STATION_ID",     limit: 255
    t.string   "ITEM_CODE",      limit: 255
    t.string   "DATA_TIME",      limit: 255
    t.float    "DATA_VALUE",     limit: 24
    t.float    "OLD_DATA_VALUE", limit: 24
    t.string   "EXT",            limit: 255
    t.string   "STATUS",         limit: 255
    t.string   "AUDIT_DESC",     limit: 255
    t.string   "INPUT_VALUE",    limit: 255
    t.datetime "RECEIVE_TIME"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "wc_d_data_days", ["STATION_ID"], name: "index_wc_d_data_days_on_STATION_ID", using: :btree

  create_table "work_boots", force: :cascade do |t|
    t.integer  "d_equip_work_id", limit: 4
    t.integer  "boot_check_id",   limit: 4
    t.string   "check_project",   limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "work_result",     limit: 255
    t.string   "abnor_desc",      limit: 255
  end

  create_table "work_notes", force: :cascade do |t|
    t.integer  "d_equip_work_id", limit: 4
    t.string   "login_name",      limit: 255
    t.string   "status",          limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

end
