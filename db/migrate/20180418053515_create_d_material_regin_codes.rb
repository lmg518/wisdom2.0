class CreateDMaterialReginCodes < ActiveRecord::Migration
  def change
    create_table :d_material_regin_codes do |t|
		t.integer :s_region_code_id
		t.integer :s_material_id
      t.timestamps
    end
	
	 add_index :d_material_regin_codes, :s_region_code_id
    add_index :d_material_regin_codes, :s_material_id
  end
end
