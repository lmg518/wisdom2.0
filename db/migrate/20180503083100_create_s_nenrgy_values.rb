class CreateSNenrgyValues < ActiveRecord::Migration
  def change
    create_table :s_nenrgy_values do |t|
		t.string :datetime
		t.integer :s_material_id
		t.string :field_name
		t.string :field_code
		t.string :field_value
		t.integer :d_report_form_id
		t.integer :s_region_code_id
      t.timestamps
    end
	add_index :s_nenrgy_values, :s_material_id
	add_index :s_nenrgy_values, :d_report_form_id
	add_index :s_nenrgy_values, :s_region_code_id
  end
end
