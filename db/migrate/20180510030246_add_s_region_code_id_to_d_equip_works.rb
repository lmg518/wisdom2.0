class AddSRegionCodeIdToDEquipWorks < ActiveRecord::Migration
  def change
    add_column :d_equip_works, :s_region_code_id, :integer
    add_column :d_equip_works, :region_name, :string
    add_column :d_equip_works, :founder, :string
  end
end
