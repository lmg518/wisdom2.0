class CreateSTechnics < ActiveRecord::Migration
  def change
    create_table :s_technics do |t|
      t.string :technic_01
	  t.string :technic_02
	  t.string :technic_03
	  t.string :technic_04
	  t.string :technic_05
	  t.string :technic_06
	  t.string :technic_07
	  t.string :technic_08
	  t.string :technic_09
	  t.string :technic_10
	  t.string :technic_11
	  t.string :technic_12
	  t.string :technic_13
	  t.string :technic_14
	  t.string :technic_15
	  t.string :technic_16
	  t.string :technic_17
	  t.string :technic_18
	  t.string :technic_19
	  t.string :technic_20
	  t.string :technic_21
	  t.string :technic_22
	  t.string :technic_23
	  t.string :technic_24
	  t.string :technic_25
	  t.string :technic_26
	  t.string :technic_27
	  t.string :technic_28
      t.timestamps
    end
  end
end
