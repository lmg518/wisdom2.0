class CreateWorkBoots < ActiveRecord::Migration
  def change
    create_table :work_boots do |t|
      t.integer :d_equip_work_id
      t.integer :boot_check_id
      t.string :check_project

      t.timestamps null: false
    end
  end
end
