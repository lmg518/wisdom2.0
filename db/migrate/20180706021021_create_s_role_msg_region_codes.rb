class CreateSRoleMsgRegionCodes < ActiveRecord::Migration
  def change
    create_table :s_role_msg_region_codes do |t|
		t.integer :s_role_msg_id
		t.integer :s_region_code_id
		t.integer :d_report_form_id
		t.integer :s_material_id
      t.timestamps
    end
	add_index :s_role_msg_region_codes, :s_role_msg_id
	add_index :s_role_msg_region_codes, :s_region_code_id
	add_index :s_role_msg_region_codes, :d_report_form_id
	add_index :s_role_msg_region_codes, :s_material_id
  end
end
