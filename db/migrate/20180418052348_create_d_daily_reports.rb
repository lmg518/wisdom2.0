class CreateDDailyReports < ActiveRecord::Migration
  def change
    create_table :d_daily_reports do |t|
	  t.string :datetime
      t.float :nums
	  t.float :sum_nums
	  t.string :status
	  t.string :submit_man
	  t.datetime :summit_time
	  t.integer :s_region_code_id

      t.timestamps
    end
	add_index :d_daily_reports, :s_region_code_id
  end
end
