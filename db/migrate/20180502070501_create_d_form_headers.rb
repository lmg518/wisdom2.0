class CreateDFormHeaders < ActiveRecord::Migration
  def change
    create_table :d_form_headers do |t|
	 t.string :code
      t.string :name
      t.timestamps
    end
  end
end
