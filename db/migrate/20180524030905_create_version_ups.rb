class CreateVersionUps < ActiveRecord::Migration
  def change
    create_table :version_ups do |t|
      t.string :name
      t.string :url

      t.timestamps null: false
    end
  end
end
