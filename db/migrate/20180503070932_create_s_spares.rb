class CreateSSpares < ActiveRecord::Migration
  def change
    create_table :s_spares do |t|
      t.integer :s_equip_id
      t.string :equip_code
      t.string :spare_code
      t.string :spare_name
      t.string :spare_desc
      t.string :spare_material
      t.integer :spare_num

      t.timestamps null: false
    end
  end
end
