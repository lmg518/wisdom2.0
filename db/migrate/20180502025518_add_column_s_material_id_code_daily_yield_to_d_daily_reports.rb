class AddColumnSMaterialIdCodeDailyYieldToDDailyReports < ActiveRecord::Migration
  def change
  add_column :d_daily_reports, :s_material_id, :integer
  add_column :d_daily_reports, :code, :string
  add_column :d_daily_reports, :daily_yield, :float
  end
end
