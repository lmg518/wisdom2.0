class CreateDReportForms < ActiveRecord::Migration
  def change
    create_table :d_report_forms do |t|
	  t.string :code
      t.string :name
	  t.integer :s_region_code_id
      t.timestamps
    end
	add_index :d_report_forms, :s_region_code_id
  end
end
