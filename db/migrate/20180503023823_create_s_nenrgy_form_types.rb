class CreateSNenrgyFormTypes < ActiveRecord::Migration
  def change
    create_table :s_nenrgy_form_types do |t|
	  t.string :code
      t.string :name
      t.timestamps
    end
  end
end
