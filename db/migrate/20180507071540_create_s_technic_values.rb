class CreateSTechnicValues < ActiveRecord::Migration
  def change
    create_table :s_technic_values do |t|
		t.string :date_time
		t.integer :s_technic_id
		t.string :field_code
		t.string :field_value
		t.integer :d_report_form_id
		t.integer :s_region_code_id
      t.timestamps
    end
	 add_index :s_technic_values, :d_report_form_id
    add_index :s_technic_values, :s_region_code_id
	add_index :s_technic_values, :s_technic_id
  end
end
