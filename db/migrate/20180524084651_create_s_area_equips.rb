class CreateSAreaEquips < ActiveRecord::Migration
  def change
    create_table :s_area_equips do |t|
		t.integer :s_equip_id
		t.integer :s_area_id
      t.timestamps
    end
	 add_index :s_area_equips, :s_equip_id
	 add_index :s_area_equips, :s_area_id
  end
end
