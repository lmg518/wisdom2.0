class AddColumsToDLoginMsgs < ActiveRecord::Migration
  def change
    add_column :d_login_msgs, :s_administrative_areas_id, :integer
    add_index :d_login_msgs, :s_administrative_areas_id
  end
end
