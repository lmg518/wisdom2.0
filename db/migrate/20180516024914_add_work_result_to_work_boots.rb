class AddWorkResultToWorkBoots < ActiveRecord::Migration
  def change
    add_column :work_boots, :work_result, :string
    add_column :work_boots, :abnor_desc, :string
  end
end
