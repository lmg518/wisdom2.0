class CreateSLoginReportForms < ActiveRecord::Migration
  def change
    create_table :s_login_report_forms do |t|
		t.integer :d_login_msg_id
		t.integer :d_report_form_id
		t.integer :s_region_code_id
      t.timestamps
    end
	add_index :s_login_report_forms, :d_login_msg_id
	add_index :s_login_report_forms, :d_report_form_id
	add_index :s_login_report_forms, :s_region_code_id
  end
end
