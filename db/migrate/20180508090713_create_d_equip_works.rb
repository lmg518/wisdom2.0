class CreateDEquipWorks < ActiveRecord::Migration
  def change
    create_table :d_equip_works do |t|
      t.integer :s_equip_id
      t.string :equip_code
      t.string :work_person
      t.datetime :work_start_time
      t.datetime :work_end_time
      t.string :work_status
      t.string :work_desc
      t.string :work_img
      t.string :work_result
      t.string :abnor_desc
      t.datetime :handle_time

      t.timestamps null: false
    end
  end
end
