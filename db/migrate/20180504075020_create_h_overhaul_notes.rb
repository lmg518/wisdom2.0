class CreateHOverhaulNotes < ActiveRecord::Migration
  def change
    create_table :h_overhaul_notes do |t|
      t.integer :handle_equip_id
      t.string :note
      t.string :img_path
      t.string :sort_out
      t.string :overhaul_desc
      t.string :note_type
      t.datetime :overhaul_time

      t.timestamps null: false
    end
      add_index :h_overhaul_notes, :handle_equip_id
  end
end
