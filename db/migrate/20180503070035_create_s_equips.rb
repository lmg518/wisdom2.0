class CreateSEquips < ActiveRecord::Migration
  def change
    create_table :s_equips do |t|
      t.integer :s_region_code_id
      t.string :region_name
      t.string :bit_code
      t.string :equip_code
      t.string :equip_name
      t.string :equip_location
      t.string :equip_norm
      t.string :equip_nature
      t.string :equip_material
      t.integer :equip_num
      t.string :apper_code
      t.datetime :apper_time

      t.timestamps null: false
    end
  end
end
