class CreateSMaterials < ActiveRecord::Migration
  def change
    create_table :s_materials do |t|
	 t.string :mate_type
	  t.string :code
	  t.string :name
	  t.string :desc
	  t.string :batch_num
      t.timestamps
    end
  end
end
