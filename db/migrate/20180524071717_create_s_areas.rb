class CreateSAreas < ActiveRecord::Migration
  def change
    create_table :s_areas do |t|
		t.string :code
		t.string :name
		t.string :location
		t.string :note
		t.string :is_leaf
      t.timestamps
    end
  end
end
