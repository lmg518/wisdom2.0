class CreateSRegionCodeAreas < ActiveRecord::Migration
  def change
    create_table :s_region_code_areas do |t|
		t.integer :s_region_code_id
		t.integer :s_area_id
      t.timestamps
    end
	 add_index :s_region_code_areas, :s_region_code_id
	 add_index :s_region_code_areas, :s_area_id
  end
end
