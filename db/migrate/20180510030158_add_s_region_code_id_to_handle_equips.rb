class AddSRegionCodeIdToHandleEquips < ActiveRecord::Migration
  def change
    add_column :handle_equips, :s_region_code_id, :integer
    add_column :handle_equips, :region_name, :string
    add_column :handle_equips, :founder, :string
  end
end
