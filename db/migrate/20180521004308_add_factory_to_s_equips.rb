class AddFactoryToSEquips < ActiveRecord::Migration
  def change
    add_column :s_equips, :factory, :string
    add_column :s_equips, :equip_note, :string
  end
end
