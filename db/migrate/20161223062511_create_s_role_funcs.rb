class CreateSRoleFuncs < ActiveRecord::Migration
  def change
    create_table :s_role_funcs do |t|
      t.integer :s_func_msg_id, null:false
      t.integer :s_role_msg_id, null:false

      t.timestamps null: false
    end
  end
end
