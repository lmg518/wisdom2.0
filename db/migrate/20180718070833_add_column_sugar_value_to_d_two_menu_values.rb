class AddColumnSugarValueToDTwoMenuValues < ActiveRecord::Migration
  def change
  add_column :d_two_menu_values, :sugar_value, :float
  add_column :d_two_menu_values, :volume_value, :float
  add_column :d_two_menu_values, :concen_value, :float
  add_column :d_two_menu_values, :fold_value, :float
  add_column :d_two_menu_values, :bottom_area_value, :float
  add_column :d_two_menu_values, :level_value, :float
  add_column :d_two_menu_values, :nums_value, :float
  add_column :d_two_menu_values, :batch_value, :string
  add_column :d_two_menu_values, :rotation_value, :float
  add_column :d_two_menu_values, :bottom_volume_value, :float
  add_column :d_two_menu_values, :bottom_concen_value, :float
  add_column :d_two_menu_values, :feed_volumn_value, :float
  add_column :d_two_menu_values, :storage_value, :string
  end
end
