class CreateWorkNotes < ActiveRecord::Migration
  def change
    create_table :work_notes do |t|
      t.integer :d_equip_work_id
      t.string :login_name
      t.string :status

      t.timestamps null: false
    end
  end
end
