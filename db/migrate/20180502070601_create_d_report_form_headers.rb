class CreateDReportFormHeaders < ActiveRecord::Migration
  def change
    create_table :d_report_form_headers do |t|
		t.integer :d_repor_form_id
		t.integer :d_form_header_id
      t.timestamps
    end
	add_index :d_report_form_headers, :d_repor_form_id
	add_index :d_report_form_headers, :d_form_header_id
  end
end
