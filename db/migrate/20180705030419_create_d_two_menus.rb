class CreateDTwoMenus < ActiveRecord::Migration
  def change
    create_table :d_two_menus do |t|
		t.string :code
		t.string :name
		t.integer :d_one_menu_id
		t.timestamps
    end
	add_index :d_two_menus, :d_one_menu_id
  end
end
