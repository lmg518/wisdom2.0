class CreateHandleEquips < ActiveRecord::Migration
  def change
    create_table :handle_equips do |t|
      t.integer :s_equip_id
      t.string :equip_code
      t.string :handle_name
      t.datetime :handle_start_time
      t.datetime :handle_end_time
      t.string :snag_desc
      t.string :status
      t.string :desc

      t.timestamps null: false
    end
      add_index :handle_equips, :s_equip_id
  end
end
