class CreateSNenrgyFormRegionCodes < ActiveRecord::Migration
  def change
    create_table :s_nenrgy_form_region_codes do |t|
		t.integer :s_nenrgy_form_type_id
		t.integer :s_region_code_id
      t.timestamps
    end
	add_index :s_nenrgy_form_region_codes, :s_nenrgy_form_type_id
	add_index :s_nenrgy_form_region_codes, :s_region_code_id
  end
end
