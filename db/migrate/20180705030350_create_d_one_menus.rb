class CreateDOneMenus < ActiveRecord::Migration
  def change
    create_table :d_one_menus do |t|
		t.string :code
		t.string :name
		t.string :data_type
		t.integer :s_material_id
		t.timestamps
    end
	add_index :d_one_menus, :s_material_id
  end
end
