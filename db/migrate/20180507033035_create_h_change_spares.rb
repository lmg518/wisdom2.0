class CreateHChangeSpares < ActiveRecord::Migration
  def change
    create_table :h_change_spares do |t|
      t.integer :handle_equip_id
      t.integer :s_spare_id
      t.string :spare_code
      t.datetime :change_time

      t.timestamps null: false
    end
      add_index :h_change_spares, :handle_equip_id
      add_index :h_change_spares, :s_spare_id
  end
end
