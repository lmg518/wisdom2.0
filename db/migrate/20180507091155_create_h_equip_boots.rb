class CreateHEquipBoots < ActiveRecord::Migration
  def change
    create_table :h_equip_boots do |t|
      t.integer :handle_equip_id
      t.integer :boot_check_id
      t.string :check_project
      t.string :check_status
      t.string :exec_desc
      t.datetime :handle_time

      t.timestamps null: false
    end
  end
end
