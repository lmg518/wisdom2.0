class ReoveWorkImgToDEquipWorks < ActiveRecord::Migration
  def change
	remove_column :d_equip_works, :work_result, :string
	remove_column :d_equip_works, :abnor_desc, :string
	remove_column :d_equip_works, :work_img, :string
  end
end
