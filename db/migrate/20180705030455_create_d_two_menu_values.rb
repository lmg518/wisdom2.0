class CreateDTwoMenuValues < ActiveRecord::Migration
  def change
    create_table :d_two_menu_values do |t|
		t.string :field_code
		t.float :field_value
		t.string :datetime
		t.string :status
		t.string :submit_man
		t.string :submit_time
		t.float :ratio
		t.integer :d_one_menu_id
		t.integer :s_material_id
		t.timestamps
    end
	add_index :d_two_menu_values, :d_one_menu_id
	add_index :d_two_menu_values, :s_material_id
  end
end
