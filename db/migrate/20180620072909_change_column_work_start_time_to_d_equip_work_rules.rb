class ChangeColumnWorkStartTimeToDEquipWorkRules < ActiveRecord::Migration
  def change
  change_column :d_equip_work_rules, :work_start_time, :string
  change_column :d_equip_work_rules, :work_end_time, :string
  end
end
