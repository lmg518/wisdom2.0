class CreateSFuncMsgs < ActiveRecord::Migration
  def change
    create_table :s_func_msgs do |t|
      t.string :func_code, null:false
      t.string :func_name, null:false
      t.string :valid_flag, null: false
      t.integer :root_distance, null: false
      t.integer :queue_index, null: false
      t.string :func_url
      t.string :new_window_flag
      t.string :func_note

      t.references :father_func


      t.timestamps null: false
    end
  end
end
