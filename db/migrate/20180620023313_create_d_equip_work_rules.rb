class CreateDEquipWorkRules < ActiveRecord::Migration
  def change
    create_table :d_equip_work_rules do |t|

	  t.integer :s_region_code_id
	  t.string :region_name
	  t.integer :s_area_id
	  t.integer :s_region_code_info_id
      t.datetime :work_start_time
      t.datetime :work_end_time
      t.string :work_desc
	  t.string :work_person
      t.string :fourder

      t.timestamps
    end
	add_index :d_equip_work_rules, :s_region_code_id
	add_index :d_equip_work_rules, :s_area_id
	add_index :d_equip_work_rules, :s_region_code_info_id
  end
end
