class CreateHEquipSpares < ActiveRecord::Migration
  def change
    create_table :h_equip_spares do |t|
      t.integer :handle_equip_id
      t.text :spare_name
      t.datetime :change_time

      t.timestamps null: false
    end
  end
end
