class AddColumnDatatimeStatusSMaterialCodeToSTechnics < ActiveRecord::Migration
  def change
  add_column :s_technics, :datetime, :string
  add_column :s_technics, :status, :string
  add_column :s_technics, :material_code, :string
  end
end
