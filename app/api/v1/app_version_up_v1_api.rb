class AppVersionUpV1Api < Grape::API
  format :json

  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end
  end

  desc "更新版本"

  get '/get_version' do
    version = VersionUp.first
    return {version: version.name,url:version.url}
  end


end