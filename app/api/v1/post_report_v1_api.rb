class PostReportV1API < Grape::API
#车间岗位上报
  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end
  end

  desc "车间岗位上报 首页显示岗位数据，根据权限判定", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    # optional :station_ids, type: String, desc: "站点id， 101,102,103"
    # optional :page, type: Integer, desc: "页码"
  end
  get "/" do
    authenticate!
    #从权限报表配置s_role_msg_region_codes 获取关联数据
    @SRoleMsgs = SRoleMsgRegionCode.where(:s_region_code_id => @current_user.group_id, :s_role_msg_id => @current_user.s_role_msg_id)
    if @SRoleMsgs.present?
      @region_code = SRegionCode.find_by(:id => @SRoleMsgs.first.s_region_code_id)
      @smaterials = SMaterial.where(:id => @SRoleMsgs.pluck(:s_material_id)) #从材料表中获取
      return {smaterials: @smaterials, region_name: @region_code.region_name}
    end
  end

  desc "车间岗位上报 岗位详细页面v1.0头粉使用", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :id, type: Integer, desc: "岗位id 1"
    optional :date_time, type: String, desc: '查询日期 2018-05-06'
  end
  get "/show" do
    authenticate!
    s_material_id = params[:id]
    date_time = params[:date_time]
    @smaterial = SMaterial.find_by(:id => s_material_id)
    #根据 岗位id 找岗位下的一级菜单数据、二级菜单数据
    @d_one_menus = DOneMenu.where(:s_material_id => s_material_id)
    #查询值
    if params[:date_time].present?
      date_time = params[:date_time]
    else
      date_time = Time.now.strftime("%Y-%m-%d")
    end
    @day_report = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time)
    two_datas=[]
    @d_one_menus.each do |d|
      data=[]
      one_item ={} #一级菜单数据
      one_item[:name] = d.name
      one_item[:d_one_menu_id] = d.id
      @d_two_menus = d.d_two_menus
      @d_two_menus.each do |t|
        two_item ={} #单个二级菜单数据
        two_item[:id] = t.id
        two_item[:field_code] = t.code
        two_item[:name] = t.name
        daily_report = @day_report.select {|x| x.field_code == t.code}
        if daily_report[0].present? && daily_report[0].field_value.present?
          field_value = daily_report[0].field_value
          #糖化 默认值
        elsif ['Concentrated_sugar', 'concentration'].include? t.code
          field_value = 42.0 #浓缩糖、浓度 默认值 比例
        elsif ['Bottom_sugar（%）'].include? t.code
          field_value = 32.0 #底糖 默认值 比例
        else
          field_value = ''
        end

        #头粉系数
        if daily_report[0].present? && daily_report[0].ratio.present?
          ratio = daily_report[0].ratio
        elsif ['dryMiillLeft', 'dryMiillBuy'].include? t.code #干粉系数默认 1
          ratio = 1
        elsif ['wetMillLeft', 'wetMillInBuy'].include? t.code #湿粉系数默认 0.53
          ratio = 0.53
          #糖化 默认系数
        elsif ['sugar_nums', 'Starch_pond', 'Feeding_tank', 'laminar_flow_pot', 'evaporator'].include? t.code
          ratio = 1 #糖表读数、淀粉池、上料罐、层流罐、蒸发器
        elsif ['PH_tank'].include? t.code
          ratio = 2.54 #调PH罐
        elsif ['Evaporation_tank'].include? t.code
          ratio = 5.08 #蒸前罐
        elsif ['1#', '2#', '3#', '4#', '5#', '6#', '7#', '8#', '9#', '10#'].include? t.code
          ratio = 3.33 # 1#、2# ...10#
        elsif ['Sugar_storageV01', 'Sugar_storageV02'].include? t.code
          ratio = 13.847 #糖储V01（M）、糖储V02（M）
        else
          ratio = '' #结算 系数不存在  Sugar_storageV01
        end
        two_item[:field_value] = field_value #保存的值
        two_item[:ratio] = ratio #保存的系数
        data.push(two_item)
      end
      one_item[:data] = data
      two_datas.push(one_item)
    end

    tf_datas = PostReportsHelper.tf_method(date_time, s_material_id)
    if tf_datas.present?
      tf_data = [{name: '干粉（包）', field_value: tf_datas[0]},
                 {name: '湿粉（包）', field_value: tf_datas[1]},
                 {name: '湿粉比例', field_value: tf_datas[2]},
                 {name: '小计（T）', field_value: tf_datas[3]}]
      tf_item = {name: '时耗', data: tf_data} #添加头粉时耗小计数据
    else
      tf_item = []
    end
    #日报状态
    reports = @day_report.select {|x| x.status == 'Y'} if @day_report.present?
    if reports.present? && reports.length >0
      return {status: '已上报', date_time: date_time, two_datas: two_datas, name: @smaterial.name, s_material_id: @smaterial.id, tf_item: tf_item}
    else
      return {status: '未上报', date_time: date_time, two_datas: two_datas, name: @smaterial.name, s_material_id: @smaterial.id, tf_item: tf_item}
    end
  end

  desc "车间岗位上报 岗位详细页面V2.1糖化、发酵、酸化使用", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :id, type: Integer, desc: "岗位id 1"
    optional :date_time, type: String, desc: '查询日期 2018-05-06'
  end
  get "/show_v2.1" do
    authenticate!
    s_material_id = params[:id]
    date_time = params[:date_time]
    @smaterial = SMaterial.find_by(:id => s_material_id)
    #根据 岗位id 找岗位下的一级菜单数据、二级菜单数据
    @d_one_menus = DOneMenu.where(:s_material_id => s_material_id)
    #查询值
    if params[:date_time].present?
      date_time = params[:date_time]
    else
      date_time = Time.now.strftime("%Y-%m-%d")
    end

    begin_time = (Time.parse(date_time) + (-7).day).strftime("%Y-%m-%d")
    end_time = (Time.parse(date_time) + (-1).day).strftime("%Y-%m-%d")
    @day_report = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time)

    #取上一周的最近的一天数据
    @nst_bef_day_report = DTwoMenuValue.where(:s_material_id => s_material_id, :field_code => 'Concentrated_sugar').by_datetime(begin_time, end_time).order(:datetime => :desc).first #浓缩糖 昨天的数据
    @dt_bef_day_report = DTwoMenuValue.where(:s_material_id => s_material_id, :field_code => 'Bottom_sugar（m3）').by_datetime(begin_time, end_time).order(:datetime => :desc).first #底糖 昨天的数据
    @th_ratio_day_report = DTwoMenuValue.find_by(:s_material_id => s_material_id, :field_code => 'ratio', :datetime => date_time) #糖化 下余物料2 系数 数据
    two_datas={}
    @d_one_menus.each do |d|
      data=[]
      data_hash={}
      one_item ={} #一级菜单数据
      one_item[:name] = d.name
      one_item[:d_one_menu_id] = d.id

      #封装一级菜单，菜单数据
      #糖液菜单数据
      if 'TY' == @smaterial.code && 'product' == d.code # 糖液 交料产量 菜单数据
        titles = [{name: '项目', code: 'project'},
                  {name: '液位高度', code: 'sugar_value'},
                  {name: '体积', code: 'volume_value'},
                  {name: '浓度', code: 'concen_value'},
                  {name: '折存', code: 'fold_value'}
        ]
      end
      if 'TY' == @smaterial.code # 糖液 下余物料1/2  菜单数据
        if ['left1', 'left2'].include? d.code
          titles = [{name: '项目', code: 'project'},
                    {name: '底面积', code: 'bottom_area_value'},
                    {name: '液位', code: 'level_value'},
                    {name: '体积', code: 'volume_value'},
                    {name: '浓度%', code: 'concen_value'},
                    {name: '折纯', code: 'fold_value'},
                    {name: '数量', code: 'nums_value'}
          ]
        end

        if ['access'].include? d.code #糖液 交料辅料 菜单数据
          titles = [{name: '项目', code: 'project'},
                    {name: '折纯(kg)', code: 'fold_value'}
          ]
        end

        #糖化 下余物料2 添加系数
        if ['left2'].include? d.code
          if @th_ratio_day_report.present?
            ratio_data = {field_code: 'ratio', field_value: @th_ratio_day_report.field_value}
          else
            ratio_data = {field_code: 'ratio', field_value: 0.925} #默认系数
          end
          one_item[:ratio_data] = ratio_data #糖化 下余物料2 系数数据
        end
      end

      if 'FJY' == @smaterial.code #发酵 菜单数据
        if 'product' == d.code #交料产量
          titles = [{name: '批号'},
                    {name: '旋光'},
                    {name: '体积'},
                    {name: '折产品'}
          ]
        end
        if 'left1' == d.code #下余物料1
          titles = [{name: '项目'},
                    {name: '底面积'},
                    {name: '液位m'},
                    {name: '体积m3'},
                    {name: '浓度%'},
                    {name: '数量'}
          ]
        end
        if 'left2' == d.code #下余物料2
          titles = [{name: '项目'},
                    {name: '批号'},
                    {name: '底糖体积'},
                    {name: '底糖浓度%'},
                    {name: '补糖体积'},
                    {name: '浓度%'},
                    {name: '数量'}
          ]
        end
        if ['access'].include? d.code #发酵 交料辅料 菜单数据
          titles = [{name: '项目', code: 'project'},
                    {name: '质量(kg)', code: 'fold_value'}
          ]
        end
      end

      if 'SHY' == @smaterial.code #酸化 菜单数据
        if 'product' == d.code #交料产量
          titles = [{name: '批号'},
                    {name: '旋光'},
                    {name: '底面积'},
                    {name: '高度'},
                    {name: '体积'},
                    {name: '折产品'}
          ]
        end
        if 'left_fjy' == d.code #下余发酵液/酸化液
          titles = [{name: '储罐'},
                    {name: '批号'},
                    {name: '体积'},
                    {name: '旋光'},
                    {name: '折酸'}
          ]
        end
        if 'left_ly' == d.code #下余滤液
          titles = [{name: '储罐'},
                    {name: '批号'},
                    {name: '旋光'},
                    {name: '底面积'},
                    {name: '高度'},
                    {name: '体积'},
                    {name: '折产品'}
          ]
        end
      end

      one_item[:titles] = titles #菜单数据
      #one_item[:ratio_data] = ratio_data #糖化 下余物料2 系数数据
      @d_two_menus = d.d_two_menus
      #发酵 下余物料2 二级菜单数据，从选择的数据中获取
      if 'FJY' == @smaterial.code #发酵
        if 'left2' == d.code #下余物料2 选择数据
          select_data = @d_two_menus.map {|x| {name: x.name, field_code: x.code}}
          one_item[:select_data] = select_data
          #下余物料2 保存的数据
          d_one_menu = DOneMenu.find_by(:code => 'left2', :s_material_id => @smaterial.id)
          two_menus_data = DTwoMenuValue.where(:d_one_menu_id => d_one_menu.id, :status => 'Y', :s_material_id => @smaterial.id, :datetime => date_time)
          if two_menus_data.present? && two_menus_data.length >0
            field_codes = two_menus_data.pluck(:field_code)
            @d_two_menus = DTwoMenu.where(:d_one_menu_id => d_one_menu.id, :code => field_codes) #从保存的值中获取
          else
            @d_two_menus = [] #初始化数据
          end
        end
      end

      #酸化  选择框数据
      if 'SHY' == @smaterial.code #发酵
        if 'left_fjy' == d.code #下余发酵液/酸化液 选择数据
          select_data = @d_two_menus.map {|x| {name: x.name, field_code: x.code}}
          one_item[:select_data] = select_data
          #下余发酵液/酸化液 保存的数据
          d_one_menu = DOneMenu.find_by(:code => 'left_fjy', :s_material_id => @smaterial.id)
          two_menus_data = DTwoMenuValue.where(:d_one_menu_id => d_one_menu.id, :status => 'Y', :s_material_id => @smaterial.id, :datetime => date_time)
          if two_menus_data.present?
            field_codes = two_menus_data.pluck(:field_code)
            @d_two_menus = DTwoMenu.where(:d_one_menu_id => d_one_menu.id, :code => field_codes) #二级菜单 从保存的值中获取
          else
            @d_two_menus = [] #初始数据
          end
        end

        if 'left_ly' == d.code #下余滤液 选择数据
          select_data = @d_two_menus.map {|x| {name: x.name, field_code: x.code}}
          one_item[:select_data] = select_data
          #下余发酵液/酸化液 保存的数据
          d_one_menu = DOneMenu.find_by(:code => 'left_ly', :s_material_id => @smaterial.id)
          two_menus_data = DTwoMenuValue.where(:d_one_menu_id => d_one_menu.id, :status => 'Y', :s_material_id => @smaterial.id, :datetime => date_time)
          if two_menus_data.present?
            field_codes = two_menus_data.pluck(:field_code)
            @d_two_menus = DTwoMenu.where(:d_one_menu_id => d_one_menu.id, :code => field_codes) #二级菜单 从保存的值中获取
          else
            @d_two_menus = [] #初始数据
          end
        end
      end

      @d_two_menus.each do |t|
        two_item ={} #单个二级菜单数据
        two_item[:id] = t.id
        two_item[:field_code] = t.code
        two_item[:name] = t.name
        if 'Concentrated_sugar' == t.code #浓缩糖 昨天的数据
          if @nst_bef_day_report.present?
            two_item[:bf_value] = @nst_bef_day_report.sugar_value
          else
            two_item[:bf_value] = 0 #作为初始数据
          end
        end
        if 'Bottom_sugar（m3）' == t.code #底糖 昨天的数据
          if @dt_bef_day_report.present?
            two_item[:bf_value] = @dt_bef_day_report.sugar_value
          else
            two_item[:bf_value] = 0
          end
        end

        #daily_report = @day_report.select {|x| x.field_code == t.code} #根据code 找值
        daily_report = @day_report.where(:field_code => t.code) #根据code 找值
        if daily_report.present? #已经存过数据时
          #daily_value = daily_report[0]
          if 'TY' == @smaterial.code #糖化  返回 指定字段数据
            if ['Concentrated_sugar', 'Bottom_sugar（m3）'].include? t.code #交料产量  浓缩糖 底糖
              daily_data = daily_report.select(:id, :sugar_value, :volume_value, :concen_value, :fold_value)
              daily_value = daily_data[0]
            end
            if ['Starch_pond', 'Feeding_tank', 'laminar_flow_pot', 'evaporator'].include? t.code #下余物料1   淀粉池 ... 蒸发器
              daily_data = daily_report.select(:id, :nums_value) #目前系数字段客户不输入，先只返数量
              daily_value = daily_data[0]
            end
            if ['PH_tank1', 'Evaporation_tank1', '1#', '2#', '3#', '4#', '5#', '6#', '7#', '8#', '9#', '10#', 'Sugar_storageV01', 'Sugar_storageV02', 'PH_tank2', 'Evaporation_tank2'].include? t.code #下余物料2   调pH罐1(M) ... 糖储V02
              daily_data = daily_report.select(:id, :bottom_area_value, :level_value, :volume_value, :concen_value, :fold_value, :nums_value)
              daily_value = daily_data[0]
            end

            #新增字段 返回 指定字段数据
            if ['sugar_plate'].include? t.code #交料辅料  糖板框
              daily_data = daily_report.select(:id, :volume_value, :concen_value, :fold_value)
              daily_value = daily_data[0]
            end
            if ['amylase', 'Glucoamylase'].include? t.code #交料辅料  淀粉酶 糖化酶
              daily_data = daily_report.select(:id, :fold_value)
              daily_value = daily_data[0]
            end

          end

          if 'FJY' == @smaterial.code #发酵
            if ['Canceller'].include? t.code #下余物料1  连消器(m3)
              daily_data = daily_report.select(:id, :volume_value, :concen_value, :nums_value)
              daily_value = daily_data[0]
            end
            if ['filling_sugar1', 'filling_sugar2'].include? t.code #下余物料1  补糖罐1(m)  补糖罐2(m)
              daily_data = daily_report.select(:id, :bottom_area_value, :level_value, :volume_value, :concen_value, :nums_value)
              daily_value = daily_data[0]
            end
            if ['1#_m3', '2#_m3', '3#_m3', '4#_m3', '5#_m3', '6#_m3'].include? t.code #下余物料2  1#发酵(m3) ... 6#发酵(m3)
              daily_data = daily_report.select(:id, :batch_value, :bottom_volume_value, :bottom_concen_value, :feed_volumn_value, :concen_value, :nums_value)
              daily_value = daily_data[0]
            end

            #新增字段 返回 指定字段数据
            if ['Foaming_agent'].include? t.code #交料辅料  消沫剂
              daily_data = daily_report.select(:id,:fold_value)
              daily_value = daily_data[0]
            end

          end

          if 'SHY' == @smaterial.code #酸化
            if ['R01', 'R02', 'R03', 'R04', 'R05'].include? t.code #下余发酵液/酸化液   R01 ... R05
              daily_data = daily_report.select(:id, :batch_value, :volume_value, :rotation_value, :fold_value)
              daily_value = daily_data[0]
            end
            if ['V01', 'V02', 'V03', 'V04', 'V05', 'V06'].include? t.code #下余滤液  V01 ... V06
              daily_data = daily_report.select(:id, :batch_value, :rotation_value, :bottom_area_value, :level_value, :volume_value, :fold_value)
              daily_value = daily_data[0]
            end
          end
        else #没存数据时(返默认系数)
          daily_value = '' #不存在时 返回null eval报错（其他的默认值）

          if 'TY' == @smaterial.code #糖化  返回 指定字段数据
            if ['Concentrated_sugar', 'Bottom_sugar（m3）'].include? t.code #交料产量  浓缩糖 底糖
              ty_arr = {concen_value: 0.42}
              daily_value = ty_arr
              daily_value = '' #V2.1新需求：取消返系数
            end
            if ['Starch_pond', 'Feeding_tank', 'laminar_flow_pot', 'evaporator'].include? t.code #下余物料1   淀粉池 ... 蒸发器
              daily_value = ''
            end
            if ['PH_tank1', 'Evaporation_tank1', '1#', '2#', '3#', '4#', '5#', '6#', '7#', '8#', '9#', '10#', 'Sugar_storageV01', 'Sugar_storageV02', 'PH_tank2', 'Evaporation_tank2'].include? t.code #下余物料2   调pH罐1(M) ... 糖储V02
              ty_arr = {concen_value: 0.32}
              daily_value = ty_arr
              daily_value = '' #V2.1新需求：取消返系数
            end
            #新增字段
            if ['sugar_plate'].include? t.code #交料辅料  糖板框 系数
              ty_arr = {concen_value: 0.32}
              daily_value = ty_arr
            end
          end

          if 'FJY' == @smaterial.code #发酵
            if ['Canceller', 'filling_sugar1', 'filling_sugar2'].include? t.code #下余物料1 连消器(m3) 补糖罐1(m)  补糖罐2(m)
              ty_arr = {concen_value: 0.42}
              daily_value = ty_arr
              daily_value = '' #V2.1新需求：取消返系数
            end
          end
          #daily_value = '' #不存在时 返回null eval报错
        end
        two_item[:value] = daily_value
        data.push(two_item)
      end

      one_item[:data] = data

      if 'FJY' == @smaterial.code && 'product' == d.code #发酵 交料产量 返回数据
        d_one_menu = DOneMenu.find_by(:code => 'product', :s_material_id => @smaterial.id)
        batch_datas = DTwoMenuValue.where(:d_one_menu_id => d_one_menu.id, :status => 'Y', :s_material_id => @smaterial.id, :field_code => 'batch', :datetime => date_time)
        if batch_datas.present?
          daily_value = batch_datas.select(:id, :batch_value, :rotation_value, :volume_value, :fold_value)
          one_item[:data] = daily_value #重新对 data 赋值
        else
          #从质量报表中查值
          teachnics = STechnic.where(:material_code => 'FJY', :datetime => date_time)
          if teachnics.present?
            s_teachnics = teachnics.select {|x| x.technic_01.present?} #获取有批号的记录数据
            s_arr = []
            s_teachnics.each do |s|
              s_hash={}
              s_hash[:batch_value] = s.technic_01 #批号
              s_hash[:rotation_value] = s.technic_07.to_f #旋光（=放罐旋光）
              s_hash[:volume_value] = '' #体积
              s_hash[:fold_value] = '' #折产品
              s_arr.push(s_hash)
            end
            one_item[:data] = s_arr #重新对 data 赋值
          end
        end
      end

      if 'SHY' == @smaterial.code && 'product' == d.code #酸化 交料产量 返回数据
        d_one_menu = DOneMenu.find_by(:code => 'product', :s_material_id => @smaterial.id)
        batch_datas = DTwoMenuValue.where(:d_one_menu_id => d_one_menu.id, :status => 'Y', :s_material_id => @smaterial.id, :field_code => 'batch', :datetime => date_time)
        if batch_datas.present?
          daily_value = batch_datas.select(:id, :batch_value, :rotation_value, :bottom_area_value, :level_value, :volume_value, :fold_value)
          one_item[:data] = daily_value #重新对 data 赋值
        else
          #从质量报表中查值
          teachnics = STechnic.where(:material_code => 'SHY', :datetime => date_time)
          #Rails.logger.info "---【酸化质量】----#{teachnics}-----"
          if teachnics.present?
            s_teachnics = teachnics.select {|x| x.technic_01.present?} #获取有批号的记录数据
            s_arr = []
            s_teachnics.each do |s|
              s_hash={}
              s_hash[:batch_value] = s.technic_01 #批号
              s_hash[:rotation_value] = s.technic_09.to_f #旋光(=交料旋光)
              s_hash[:bottom_area_value] = '' #底面积
              s_hash[:level_value] = '' #高度
              s_hash[:volume_value] = '' #体积
              s_hash[:fold_value] = '' #折产品
              s_arr.push(s_hash)
            end
            one_item[:data] = s_arr #重新对 data 赋值
          end
        end
      end
      two_datas[d.code] = one_item #将一级菜单 按 key封装
    end

    tf_datas = PostReportsHelper.tf_method(date_time, s_material_id)
    if tf_datas.present?
      tf_data = [{name: '干粉（包）', field_value: tf_datas[0]},
                 {name: '湿粉（包）', field_value: tf_datas[1]},
                 {name: '湿粉比例', field_value: tf_datas[2]},
                 {name: '小计（T）', field_value: tf_datas[3]}]
      tf_item = {name: '时耗', data: tf_data} #添加头粉时耗小计数据
    else
      tf_item = []
    end

    #添加计算数据
    if 'TY' == @smaterial.code #添加糖液 综合计算数据
      ty_datas = PostReportsHelper.th_method_v2(date_time, @smaterial.id)
      if ty_datas.present?
        two_datas['zh'] = ty_datas
      else
        two_datas['zh'] = ['-', '-', '-', '-']
      end

      # #糖液上报页面 客户新增字段
      # titles = [{name:'名称'},{name:'体积'},{name:'浓度%'},{name:'折纯(kg)'}]
      # add_data = {}
    end
    if 'FJY' == @smaterial.code #添加发酵 综合计算数据
      ty_datas = PostReportsHelper.fj_method_v2(date_time, @smaterial.id)
      if ty_datas.present?
        two_datas['zh'] = ty_datas
      else
        two_datas['zh'] = ['-', '-', '-', '-', '-']
      end
    end
    if 'SHY' == @smaterial.code #添加酸化 综合计算数据
      ty_datas = PostReportsHelper.sh_method_v2(date_time, @smaterial.id)
      if ty_datas.present?
        two_datas['zh'] = ty_datas
      else
        two_datas['zh'] = ['-', '-', '-', '-']
      end
    end


    #日报状态
    reports = @day_report.select {|x| x.status == 'Y'} if @day_report.present?
    if reports.present? && reports.length >0
      return {status: '已上报', date_time: date_time, two_datas: two_datas, name: @smaterial.name, s_material_id: @smaterial.id, tf_item: tf_item}
    else
      return {status: '未上报', date_time: date_time, two_datas: two_datas, name: @smaterial.name, s_material_id: @smaterial.id, tf_item: tf_item}
    end
    #return {two_datas: two_datas}

  end

  desc "保存、上报操作 V2.1", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :date_time, type: String, desc: '当前日期'
    requires :two_menu_values_params, type: Array[String], desc: "保存的参数"
  end
  post "/report_v2.1" do
    authenticate!
    @two_menu_values_params = params[:two_menu_values_params]
    date_time = params[:date_time]
    # Rails.logger.info "----1----#{@two_menu_values_params}----------"
    # Rails.logger.info "----11----#{@two_menu_values_params[0]}----------"
    # Rails.logger.info "---2-----#{@two_menu_values_params[0].class}----------"
    # Rails.logger.info "-----3---#{eval(@two_menu_values_params[0])}----------"
    @d_daily = eval(@two_menu_values_params[0]) if @two_menu_values_params[0].present?
    s_material_id = @d_daily[:s_material_id]
    s_material = SMaterial.find_by(:id => s_material_id)
    two_datas = @d_daily[:two_datas]
    # #获取岗位下的一级菜单数据
    d_one_menus = DOneMenu.where(:s_material_id => s_material_id)
    d_one_menus.each do |one|
      str = one.code.to_sym #将字符串转 symbol符号类型
      one_menu_datas = two_datas[str]
      #Rails.logger.info "----one_menu_datas-------#{one_menu_datas}-----"
      d_one_menu_id = one_menu_datas[:d_one_menu_id]

      #存酸化  交料产量数据
      if 'SHY' == s_material.code && 'product' == one.code
        batch_hashs = one_menu_datas[:data] #前台封装的数据
        batch_hashs.each do |b|
          b.delete(:arrIndex)
          b.delete(:all)
          b.delete(:allDv)
          b.delete(:allId) #删除多余的字段
        end
        #Rails.logger.info "----前台封装的数据---酸化----#{batch_hashs}-----"
        #判断是否已经存在当天的数据
        day_report = DTwoMenuValue.where(:s_material_id => s_material_id, :d_one_menu_id => d_one_menu_id, :datetime => date_time, :field_code => 'batch')
        if day_report.present? && day_report.length > 0
          day_report.each do |d|
            d.delete #删除原数据，因为code 一样，没法修改
          end
          #删除后 创建新的记录
          batch_hashs.each do |b|
            @d_daily_report = DTwoMenuValue.new(b)
            @d_daily_report.field_code = 'batch'
            @d_daily_report.s_material_id = s_material_id
            @d_daily_report.d_one_menu_id = d_one_menu_id
            @d_daily_report.datetime = date_time
            @d_daily_report.status = 'Y'
            @d_daily_report.save
          end
        else
          #直接创建
          batch_hashs.each do |b|
            @d_daily_report = DTwoMenuValue.new(b)
            @d_daily_report.field_code = 'batch'
            @d_daily_report.s_material_id = s_material_id
            @d_daily_report.d_one_menu_id = d_one_menu_id
            @d_daily_report.datetime = date_time
            @d_daily_report.status = 'Y'
            @d_daily_report.save
          end
        end
      end

      #存发酵 交料产量数据
      if 'FJY' == s_material.code
        if 'product' == one.code
          batch_hashs = one_menu_datas[:data] #前台封装的数据
          batch_hashs.each do |b|
            b.delete(:arrIndex) #删除多余的字段
            b.delete(:allId) #删除多余的字段
          end
          #Rails.logger.info "----前台封装的数据2-------#{batch_hashs}-----"
          #判断是否已经存在当天的数据
          day_report = DTwoMenuValue.where(:s_material_id => s_material_id, :d_one_menu_id => d_one_menu_id, :datetime => date_time, :field_code => 'batch')
          if day_report.present? && day_report.length > 0
            day_report.each do |d|
              d.delete #删除原数据，因为code 一样，没法修改
            end
            #删除后 创建新的记录
            batch_hashs.each do |b|
              @d_daily_report = DTwoMenuValue.new(b)
              @d_daily_report.field_code = 'batch'
              @d_daily_report.s_material_id = s_material_id
              @d_daily_report.d_one_menu_id = d_one_menu_id
              @d_daily_report.datetime = date_time
              @d_daily_report.status = 'Y'
              @d_daily_report.save
            end
          else
            #直接创建
            batch_hashs.each do |b|
              @d_daily_report = DTwoMenuValue.new(b)
              @d_daily_report.field_code = 'batch'
              @d_daily_report.s_material_id = s_material_id
              @d_daily_report.d_one_menu_id = d_one_menu_id
              @d_daily_report.datetime = date_time
              @d_daily_report.status = 'Y'
              @d_daily_report.save
            end
          end
        end
      end

      #存糖化 下余物料2 的系数
      if 'TY' == s_material.code && 'left2' == one.code
        ratio_data = one_menu_datas[:ratio_data]
        #判断是否已经存在当天的系数
        day_report = DTwoMenuValue.find_by(:s_material_id => s_material_id, :d_one_menu_id => d_one_menu_id, :datetime => date_time, :field_code => ratio_data[:field_code])
        if day_report.present?
          day_report.update(field_value: ratio_data[:field_value])
        else
          @d_daily_report = DTwoMenuValue.new
          @d_daily_report.field_code = ratio_data[:field_code]
          @d_daily_report.field_value = ratio_data[:field_value]
          @d_daily_report.s_material_id = s_material_id
          @d_daily_report.d_one_menu_id = d_one_menu_id
          @d_daily_report.datetime = date_time
          @d_daily_report.status = 'Y'
          @d_daily_report.save
        end
      end

      data = one_menu_datas[:data]
      data.each do |d|
        if d[:value] != '' && d[:value].present?
          #判断是否已经存在当天的日报
          @day_report = DTwoMenuValue.find_by(:s_material_id => s_material_id, :d_one_menu_id => d_one_menu_id, :datetime => date_time, :field_code => d[:field_code])
          if @day_report.present?
            @day_report.update(d[:value])
          else
            @d_daily_report = DTwoMenuValue.new(d[:value])
            @d_daily_report.field_code = d[:field_code]
            @d_daily_report.s_material_id = s_material_id
            @d_daily_report.d_one_menu_id = d_one_menu_id
            @d_daily_report.datetime = date_time
            @d_daily_report.status = 'Y'
            @d_daily_report.save
            # Rails.logger.info "-------保存数据--#{d}---"
            # Rails.logger.info @d_daily_report.inspect
          end
        end
      end

      #将盘点数据进行计算，上报到车间日报
      @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
      if 'TF' == s_material.code #头粉 上报
        tf_sum = PostReportsHelper.tf_total(date_time, s_material.id)
        #头粉 时耗小计
        if tf_sum.present?
          #Rails.logger.info "----------头粉 上报计算数据-----#{tf_sum}----"
          #判断是否已经存在当天的日报
          day_report = DDailyReport.find_by(:s_region_code_id => @region_code.id, :datetime => date_time, :code => s_material.code)
          if day_report.present?
            day_report.update(:nums => tf_sum)
          else
            d_daily_report = DDailyReport.new
            d_daily_report.nums = tf_sum
            d_daily_report.name = s_material.name
            d_daily_report.s_region_code_id = @region_code.id
            d_daily_report.datetime = date_time
            d_daily_report.status = 'Y'
            d_daily_report.code = s_material.code #物料项目code
            d_daily_report.s_material_id = s_material.id #物料项目id
            d_daily_report.daily_yield = 100 #日收率
            d_daily_report.save
          end
        end
      end

      if 'TY' == s_material.code #糖液 上报
        ty_datas = PostReportsHelper.th_method_v2(date_time, s_material.id) #下余（总计） 产糖  时耗  收率
        #Rails.logger.info "----------糖液 上报计算数据-----#{ty_datas}----"
        if ty_datas.present?
          #判断是否已经存在当天的日报
          day_report = DDailyReport.find_by(:s_region_code_id => @region_code.id, :datetime => date_time, :code => s_material.code)
          if day_report.present?
            day_report.update(:nums => ty_datas[1])
          else
            d_daily_report = DDailyReport.new
            d_daily_report.nums = ty_datas[1]
            d_daily_report.name = s_material.name
            d_daily_report.s_region_code_id = @region_code.id
            d_daily_report.datetime = date_time
            d_daily_report.status = 'Y'
            d_daily_report.code = s_material.code #物料项目code
            d_daily_report.s_material_id = s_material.id #物料项目id
            d_daily_report.daily_yield = ty_datas[3] #日收率
            d_daily_report.save
          end
        end
      end

      if 'FJY' == s_material.code #发酵 上报
        ty_datas = PostReportsHelper.fj_method_v2(date_time, s_material.id) #下余（总计） 产量  时耗  收率  对淀粉收率
        #Rails.logger.info "----------发酵 上报计算数据-----#{ty_datas}----"
        if ty_datas.present?
          #判断是否已经存在当天的日报
          day_report = DDailyReport.find_by(:s_region_code_id => @region_code.id, :datetime => date_time, :code => s_material.code)
          if day_report.present?
            day_report.update(:nums => ty_datas[1])
          else
            d_daily_report = DDailyReport.new
            d_daily_report.nums = ty_datas[1]
            d_daily_report.name = s_material.name
            d_daily_report.s_region_code_id = @region_code.id
            d_daily_report.datetime = date_time
            d_daily_report.status = 'Y'
            d_daily_report.code = s_material.code #物料项目code
            d_daily_report.s_material_id = s_material.id #物料项目id
            d_daily_report.daily_yield = ty_datas[3] #日收率
            d_daily_report.save
          end
        end
      end

      if 'SHY' == s_material.code #酸化 上报
        ty_datas = PostReportsHelper.shjl_method_v2(date_time, s_material.id) #返回 交料产量  下余  实耗  收率  总收率
        #Rails.logger.info "----------酸化 上报计算数据-----#{ty_datas}----"
        if ty_datas.present?
          #判断是否已经存在当天的日报
          day_report = DDailyReport.find_by(:s_region_code_id => @region_code.id, :datetime => date_time, :code => s_material.code)
          if day_report.present?
            day_report.update(:nums => ty_datas[0])
          else
            d_daily_report = DDailyReport.new
            d_daily_report.nums = ty_datas[0]
            d_daily_report.name = s_material.name
            d_daily_report.s_region_code_id = @region_code.id
            d_daily_report.datetime = date_time
            d_daily_report.status = 'Y'
            d_daily_report.code = s_material.code #物料项目code
            d_daily_report.s_material_id = s_material.id #物料项目id
            d_daily_report.daily_yield = ty_datas[3] #日收率
            d_daily_report.save
          end
        end
      end
    end
    return {status: 'success'}
  end


  desc "保存、上报操作", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :date_time, type: String, desc: '当前日期'
    #optional :status, type: String, desc: 'Y  上报  N 保存'
    requires :two_menu_values_params, type: Array[String], desc: "保存的参数，格式：{s_material_id:1,two_datas:{d_one_menu_id: 1, data:[{field_code:'dryMiillLeft',field_value:10,ratio:1},{field_code:'wetMillLeft',field_value:10,ratio:1}]}}"
  end
  post "/report" do
    authenticate!
    @two_menu_values_params = params[:two_menu_values_params]
    date_time = params[:date_time]
    # Rails.logger.info "----1----#{@two_menu_values_params}----------"
    # Rails.logger.info "----11----#{@two_menu_values_params[0]}----------"
    # Rails.logger.info "---2-----#{@two_menu_values_params[0].class}----------"
    # Rails.logger.info "-----3---#{eval(@two_menu_values_params[0])}----------"
    @d_daily = eval(@two_menu_values_params[0]) if @two_menu_values_params[0].present?
    s_material_id = @d_daily[:s_material_id]
    two_datas = @d_daily[:two_datas]
    two_datas.each do |t|
      d_one_menu_id = t[:d_one_menu_id]
      data= t[:data]
      data.each do |d|
        #判断是否已经存在当天的日报
        @day_report = DTwoMenuValue.find_by(:s_material_id => s_material_id, :d_one_menu_id => d_one_menu_id, :datetime => date_time, :field_code => d[:field_code])
        if @day_report.present?
          @day_report.update(:field_value => d[:field_value], :ratio => d[:ratio])
        else
          @d_daily_report = DTwoMenuValue.new
          @d_daily_report.field_code = d[:field_code]
          @d_daily_report.field_value = d[:field_value]
          @d_daily_report.ratio = d[:ratio]
          @d_daily_report.s_material_id = s_material_id
          @d_daily_report.d_one_menu_id = d_one_menu_id
          @d_daily_report.datetime = date_time
          @d_daily_report.status = 'Y'
          @d_daily_report.save
        end
      end
    end

    #将盘点数据进行计算，上报到车间日报
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    smaterial = SMaterial.find_by(:id => s_material_id)

    if 'TF' == smaterial.code #头粉上报
      tf_sum = PostReportsHelper.tf_total(date_time, smaterial.id)
      #头粉 时耗小计
      if tf_sum.present?
        Rails.logger.info "----------头粉 上报计算数据-----#{tf_sum}----"
        #判断是否已经存在当天的日报
        day_report = DDailyReport.find_by(:s_region_code_id => @region_code.id, :datetime => date_time, :code => smaterial.code)
        if day_report.present?
          day_report.update(:nums => tf_sum)
        else
          d_daily_report = DDailyReport.new
          d_daily_report.nums = tf_sum
          d_daily_report.name = smaterial.name
          d_daily_report.s_region_code_id = @region_code.id
          d_daily_report.datetime = date_time
          d_daily_report.status = 'Y'
          d_daily_report.code = smaterial.code #物料项目code
          d_daily_report.s_material_id = smaterial.id #物料项目id
          d_daily_report.daily_yield = 100 #日收率
          d_daily_report.save
        end
      end
    end

    if 'TY' == smaterial.code #糖液 上报
      ty_datas = PostReportsHelper.th_method(date_time, smaterial.id)
      Rails.logger.info "----------糖液 上报计算数据-----#{ty_datas}----"
      if ty_datas.present?
        #判断是否已经存在当天的日报
        day_report = DDailyReport.find_by(:s_region_code_id => @region_code.id, :datetime => date_time, :code => smaterial.code)
        if day_report.present?
          day_report.update(:nums => ty_datas[2])
        else
          d_daily_report = DDailyReport.new
          d_daily_report.nums = ty_datas[2]
          d_daily_report.name = smaterial.name
          d_daily_report.s_region_code_id = @region_code.id
          d_daily_report.datetime = date_time
          d_daily_report.status = 'Y'
          d_daily_report.code = smaterial.code #物料项目code
          d_daily_report.s_material_id = smaterial.id #物料项目id
          d_daily_report.daily_yield = ty_datas[3] #日收率
          d_daily_report.save
        end
      end
    end


    return {status: 'success'}
  end


end