class FormInfoV1API < Grape::API

  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
      get_region
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end

    def get_region
      #根据登录人员 获取人员所在的车间 或公司
      # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
      # @region_code = SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
      @region_code = SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    end

  end

  desc "报表首页", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    # optional :station_ids, type: String, desc: "站点id， 101,102,103"
    # optional :page, type: Integer, desc: "页码"
  end
  get "/" do
    authenticate!
    time = Time.now
    @unit_info = []
    region_level = @region_code.s_region_level
    #  总公司级别的权限
    if "总公司" == region_level.level_name
      #分公司
      all_info = SRegionCode.where(:s_region_level_id => 2, :father_region_id => @region_code.id)
      info_h = {}
      info_h[:title] = "总报表"
      info_h[:time] = time.strftime("%Y-%m-%d")
      @z_arr = []
      all_info.each do |info|
        forms = DReportForm.where(:s_region_code_id => info.id).order(:created_at => :desc)
        forms.map {|x| @z_arr << {form_code: x.code, form_name: x.name, region_code: info.region_code, region_name: info.region_name}}
      end
      info_h[:unit_data] = @z_arr
      @unit_info << info_h
    end
    #  分公司级别的权限
    if "分公司" == region_level.level_name
      forms = DReportForm.where(:s_region_code_id => @region_code.id).order(:created_at => :desc)
      info_h = {}
      info_h[:title] = "总报表"
      info_h[:time] = time.strftime("%Y-%m-%d")
      @z_arr = []
      forms.each do |info|
        forms = DReportForm.where(:s_region_code_id => @region_code.id).order(:created_at => :desc)
        forms.map {|x| @z_arr << {form_code: x.code, form_name: x.name, region_code: @region_code.region_code, region_name: @region_code.region_name}}
      end
      info_h[:unit_data] = @z_arr
      @unit_info << info_h
      # 车间
      all_info = SRegionCode.where(:s_region_level_id => 3, :father_region_id => @region_code.id)
      info_h = {}
      info_h[:title] = "车间生产报表"
      info_h[:time] = time.strftime("%Y-%m-%d")
      @z_arr = []
      all_info.each do |info|
        forms = DReportForm.where(:s_region_code_id => info.id)
        forms.map {|x| @z_arr << {form_code: x.code, form_name: x.name, region_code: info.region_code, region_name: info.region_name}}
      end
      info_h[:unit_data] = @z_arr
      @unit_info << info_h
      # 能源
      all_info = SRegionCode.where(:s_region_level_id => 4, :father_region_id => @region_code.id)
      info_h = {}
      info_h[:title] = "能源报表"
      info_h[:time] = time.strftime("%Y-%m-%d")
      @z_arr = []
      all_info.each do |info|
        forms = DReportForm.where(:s_region_code_id => info.id)
        forms.each do |f|
          if info.region_code == "TYSYFGS08"
            @z_arr << {form_code: 'Biological_once_water_from', form_name: "一次水排污", region_code: info.region_code, region_name: info.region_name}
          else
            @z_arr << {form_code: f.code, form_name: f.name, region_code: info.region_code, region_name: info.region_name}
          end
        end
      end
      info_h[:unit_data] = @z_arr.uniq!
      @z_arr = @z_arr.sort_by{|x| x[:form_code]}  #排序
      @unit_info << info_h

      if "分公司" == region_level.level_name
        data = @unit_info[1][:unit_data].sort_by{|x| x[:form_code]}  #车间排序
        @unit_info[1][:unit_data] = data
      end
    end



    if ["车间", "能源"].include? region_level.level_name
      # 车间
      forms = DReportForm.where(:s_region_code_id => @region_code.id)
      info_h = {}
      info_h[:title] = "车间生产报表"
      info_h[:time] = time.strftime("%Y-%m-%d")
      forms.each do |info|
        @f_arr = []
        forms = DReportForm.where(:s_region_code_id => @region_code.id)
        forms.map {|x| @f_arr << {form_code: x.code, form_name: x.name, region_code: @region_code.region_code, region_name: @region_code.region_name}}
        info_h[:unit_data] = @f_arr
        @unit_info << info_h
      end
    end
    @form_hash = {}
    @form_hash[:name] = "公司日生产总报表"
    @form_hash[:time] = time.strftime("%Y-%m-%d")
    @form_hash[:data_info] = @unit_info
    return @form_hash
  end

end