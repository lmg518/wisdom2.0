require 'grape'
class AirAppV1API < Grape::API

  mount UserV1API => 'user'

  mount DailyReportV1API => 'daily_report'  #日报上报
  mount EnergyReportV1API => 'energy_report'  #能源上报
  mount EnergyReportShowV1API => 'energy_report_show'  #能源报表
  mount FormInfoV1API => 'form_info_v1_api'  # 报表首页

  mount PlantReportShowV1API => 'plant_report_show'  #车间报表

  mount CompanyDayFormV1API => 'company_day_form'  #公司报表(日的)
  mount CompanyMonthFormV1API => 'company_month_form'  #公司报表(月的)


  mount SEquipsV1API => 's_equips'  #设备台账管理
  mount HandleEquipsV1API => 'handle_equips'  #设备检修管理
  mount DEquipWorksV1API => 'd_equip_works'  #设备点检管理
  
  mount AppVersionUpV1Api => "app_version_up" #更新版本

  #V2.0
  mount PostReportV1API => 'post_report'  #车间岗位上报

  mount JpushV1Api => 'jpush'
  

  add_swagger_documentation base_path: '/api/v1', hide_format: true
  
end