class PlantReportShowV1API < Grape::API
  include EnergyFormHelper
  #车间报表  4个车间报表
  $flag = false    #差值计算和不差值计算的 标识  true 差值计算   false 不差值计算


  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
      get_region
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end

    def get_region
      #根据登录人员 获取人员所在的车间 或公司
      # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
      # @region_code = SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
      @region_code = SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    end

    #封装计算方法
    #差值计算方法1 计算多个项目的，参数有项目的
  def js_method_many(time, s_region_code_id, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

     #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期

    @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => time, :field_code => item) 
    @form_values_bf = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => bf_time, :field_code => item)  
      
    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :field_code => item)                   
    min_time = @form_values_month.minimum("datetime")   #获取最小日期
    max_time = @form_values_month.maximum("datetime")   #获取最大日期
    @form_values_min = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => min_time, :field_code => item)                  
    @form_values_max = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => max_time, :field_code => item)                  

    d1 = 0
    d2 = 0
    @form_values.each do |m|
      d1 += m.field_value.to_f
    end
    @form_values_bf.each do |m|
      d2 += m.field_value.to_f
    end
    @steam_values = (d1 - d2).round(2)   #日用量

    dd1 = 0
    dd2 = 0
    @form_values_max.each do |m|
      dd1 += m.field_value.to_f
    end
    @form_values_min.each do |m|
      dd2 += m.field_value.to_f
    end
    @steam_month_sum = (dd1 - dd2).round(2)   #累计用量
  end


  #差值计算方法 2  计算单个项目的  item 项目参数名
  def js_method_one(time, s_region_code_id, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期

    @form_values = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)  
    @form_values_bf = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id,:field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)  
   
    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id,:field_code => item, :d_report_form_id => d_report_form_id)  
    min_time = @form_values_month.minimum("datetime")   #获取最小日期
    max_time = @form_values_month.maximum("datetime")   #获取最大日期

    @form_values_min = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)                  
    @form_values_max = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)                  

    if @form_values.present? && @form_values.field_value.present?
      d1 = @form_values.field_value.to_f
    else
      d1 = 0
    end

    if @form_values_bf.present? && @form_values_bf.field_value.present?
      d2 = @form_values_bf.field_value.to_f
    else
      d2 = 0
    end

    #添加判断
    if @form_values_min.present? && @form_values_min.field_value.present?
      min = @form_values_min.field_value.to_f
    else
      min = 0
    end
    if @form_values_max.present? && @form_values_max.field_value.present?
      max = @form_values_max.field_value.to_f
    else
      max = 0
    end
    @steam_month_sum = max -min

    @steam_values = d1 - d2  #昨天的 - 前天的
    #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f   #月累计用量
  end

  #计算公司总产量的方法
  def js_company_finished(before_time)
    #计算 公司 总产量 
    time  = before_time
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
    @company_day_total = 1    #日产量量      计算日单耗时  除数不能为0
    @company_month_total = 1  #累计用量

    @na_form_values = DDailyReport.find_by(:s_region_code_id => 8 , :code => 'na_input', :datetime => time)  # 8 烘包车间  na_input 钠入库
    @sour_form_values = DDailyReport.find_by(:s_region_code_id => 8 , :code => 'sour_input', :datetime => time)  # 8 烘包车间  sour_input 酸入库

    @na_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8 , :code => 'na_input') 
    @sour_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8 , :code => 'sour_input') 
    
    if @na_form_values.present? && @sour_form_values.present?
      @company_day_total = (@na_form_values.nums.to_f + @sour_form_values.nums.to_f / 0.73).round(2)
    end

    d1 = 0
    d2 = 0
    @na_form_values_month.each do |m|
      d1 += m.nums.to_f
    end
    @sour_form_values_month.each do |m|
      d2 += m.nums.to_f
    end

    #添加判断
    if @na_form_values_month.present? && @sour_form_values_month.present?
      @company_month_total = (d1 + d2 / 0.73).round(2)     ##公司月累计产量
    end
    
  end




  end

  desc "烘包车间报表接口", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
  }
  params do
    #requires :r_time_type, type: String, desc: "请求时间类型，年 Y 月 M 日 D ;默认传 D "
    optional :r_time, type: String, desc: "请求时间，非必填，格式< 2018-05-08 >"
    optional :form_code, type: String, desc: "表单code"
  end
  get "/baking" do
    authenticate!
    @plant_hash = {}
    
    if params[:r_time].present?
      time = params[:r_time]
    else
      #time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
      time = Time.now.strftime("%Y-%m-%d")  #默认查询当天的日期
    end

    form_code = params[:form_code]  #表单code

    #计算公司总产量
    js_company_finished(time)
    
    #--------------------
#根据region_code id获取报表
@d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
@form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名

@form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
@region_datas = []

@ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
@plants = SMaterial.where(:id => @ids)      #从材料表中获取
@plants.each do |plant|
  item = {}
  item[:name] = plant.name
  item[:code] = plant.code

  field_value = @form_values.find_by(:code => plant.code)  

  item[:nums] = field_value.present? ? field_value.nums : ''                #日产值
  item[:daily_yield] = field_value.present? ? field_value.daily_yield : ''  #日收率

  #计算月累计
  @month_sum = 0
  month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
  @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
  @month_values.each do |m|
    @month_sum += m.nums.to_f
  end
  item[:month_value] =  @month_sum.round(2)  #月累计用电量

  #烘包车间 质量信息
  data2 = []
  if 'SPCN' == plant.code  #湿成品钠
    data2.push({:technic_01 => '批号' , :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end

  if 'SCPS' == plant.code  #湿成品酸
    data2.push({:technic_01 => '批号' , :technic_24 => '白度', :technic_27 => '透光度', :technic_14 => '水份'})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end

  if 'na_nums' == plant.code  #钠包装量
    data2.push({:technic_01 => '批号' , :technic_24 => '白度', :technic_26 => '2小时白度', :technic_27 => '透光度', :technic_14 => '水份', :technic_28 => '等级'})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end

  if 'suan_nums' == plant.code  #酸包装量
    data2.push({:technic_01 => '批号' , :technic_24 => '白度', :technic_26 => '2小时白度', :technic_27 => '透光度', :technic_14 => '水份', :technic_28 => '等级'})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end

  item[:data] = data2
  @region_datas.push(item)
end

#---------v3-------------
# 烘包车间 蒸汽  总收率  数据
if $flag
  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id_v1(time, @d_report_forms.s_region_code_id, 3)
else
  #不进行差值计算
  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id(time, @d_report_forms.s_region_code_id, 3)
end

@day_expend = (@steam_values / @company_day_total.to_f).round(2)                #烘包车间日单耗
@month_expend = (@steam_month_sum / @company_month_total.to_f).round(2)         #烘包车间月单耗
@day_steam = @steam_values                #蒸汽日用量
@steam_month_sum = @steam_month_sum       #蒸汽月用量


#--------------------
@today_time = time

@plant_hash[:form_name] = @form_name
@plant_hash[:d_time] = @today_time
@plant_hash[:info] = @region_datas
@plant_hash[:steam_values] =@day_steam               #蒸汽 日用量
@plant_hash[:steam_month_sum] =@steam_month_sum    #累计用量
@plant_hash[:day_expend] =@day_expend              #日单耗
@plant_hash[:month_expend] =@month_expend          #月单耗


return @plant_hash

end


  desc "合成车间报表接口", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
  }
  params do
    #requires :r_time_type, type: String, desc: "请求时间类型，年 Y 月 M 日 D ;默认传 D "
    optional :r_time, type: String, desc: "请求时间，非必填，格式< 2018-05-08 >"
    optional :form_code, type: String, desc: "表单code"
  end
  get "/compound" do
    authenticate!
    @plant_hash = {}
    
    if params[:r_time].present?
      time = params[:r_time]
    else
     # time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
      time = Time.now.strftime("%Y-%m-%d")  #默认查询当天的日期
    end

    form_code = params[:form_code]  #表单code


    #--------------------
#根据region_code id获取报表
@d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
@form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名

@form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
@region_datas = []

@ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
@plants = SMaterial.where(:id => @ids)      #从材料表中获取
@plants.each do |plant|
  item = {}
  item[:name] = plant.name
  item[:code] = plant.code

  field_value = @form_values.find_by(:code => plant.code)  

  item[:nums] = field_value.present? ? field_value.nums : ''                #日产值
  item[:daily_yield] = field_value.present? ? field_value.daily_yield : ''  #日收率

  #计算月累计
  @month_sum = 0
  month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
  @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
  @month_values.each do |m|
    @month_sum += m.nums.to_f
  end
  item[:month_value] =  @month_sum.round(2)  #合成车间月累计

  #质量信息
  data2 = []

  if 'LJ' == plant.code  #离交
    data2.push({:technic_01 => '批号' , :technic_09 => '交料旋光', :technic_10 => '交料透光', :technic_11 => 'Ca2+',:technic_12 => 'PH',})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end

  if 'CP' == plant.code  #粗品
    data2.push({:technic_01 => '批号' , :technic_13 => '含量', :technic_12 => 'PH', :technic_14 => '水份', :technic_15 => '料浆比重',:technic_16 => '母液含量',})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end

  item[:data] = data2
  @region_datas.push(item)
end


    #-------------------v2----------
    #合成车间 蒸汽  总收率  数据
    #合成车间 总收率= 离交*粗品*三效
    @daily_report_lj = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'LJ')
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'CP')
    @daily_report_sx = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'steam_1')
    
    lj_yield= @daily_report_lj.present? ? @daily_report_lj.daily_yield : 0
    cp_yield= @daily_report_cp.present? ? @daily_report_cp.daily_yield : 0
    sx_yield= @daily_report_sx.present? ? @daily_report_sx.daily_yield : 0

    @total_yield = ((lj_yield.to_f * cp_yield.to_f * sx_yield.to_f) / 10000).round(2)  #合成车间 总收率

    #合成车间日产量= 粗品
    #合成车间 计算 日单耗 = 日蒸汽/ 合成车间日产量     月单耗 = 月蒸汽/ 合成车间月产量
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => 5, :datetime => time, :code => 'CP')   # CP  在 s_region_code_id = 5
    @daily_report_cp_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 5, :code => 'CP')

    @cp_day_nums = @daily_report_cp.present? ? @daily_report_cp.nums : 1    #粗品日产量

    @cp_month_nums = 0          #粗品月累计
    if @daily_report_cp_month.present? && @daily_report_cp_month.length >0
      @daily_report_cp_month.each do |d1|
        @cp_month_nums += d1.nums.to_f
      end
    else
      @cp_month_nums = 1   #数据不存在是 默认值为 1  除数不能为0
    end

    #蒸汽 表格数据
    @steam_datas = []
    steam_names = [{:name => '酯转化', :code => 'steam_3'}, 
                  {:name => '三浓', :code => 'steam_2'}, 
                  {:name => '三效', :code => 'steam_1'}, 
                  {:name => '甲醇塔', :code => 'steam_4'}, 
                  {:name => '合计', :code => 'total'},
                  {:name => '洗柱水', :code => 'pollution_2'}, 
                  {:name => '中合水', :code => 'pollution_3'}, 
                  {:name => '生产废水', :code => 'pollution_4'},
                  {:name => '降膜水', :code => 'pollution_5'},
                  {:name => '甲醇废水', :code => 'pollution_6'},
                  {:name => '洗效水', :code => 'pollution_7'}
                ]
    steam_names.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]

      #合成车间 计算单个 项目 蒸汽的日用量  月累计
      if s[:code] == 'steam_3'   #酯转化
        if $flag
         js_method_one(time, 5, 3, s[:code])  #返回 @steam_values   @steam_month_sum 

        else
        #不进行差值计算
        @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
      end


        item1[:steam_values]=  @steam_values           #日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      if s[:code] == 'steam_2'   #三浓
        if $flag
          js_method_one(time, 5, 3, s[:code])  #返回 @steam_values   @steam_month_sum 

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]=  @steam_values           #日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      if s[:code] == 'steam_1'   #三效
        if $flag
         js_method_one(time, 5, 3, s[:code])  #返回 @steam_values   @steam_month_sum 
        
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]=  @steam_values           #日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end
      
      if s[:code] == 'steam_4'   #甲醇塔
        if $flag
          js_method_one(time, 5, 3, s[:code])  #返回 @steam_values   @steam_month_sum 
        
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]=  @steam_values           #日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end


      if s[:code] == 'total'   #合计


        item = ['steam_1', 'steam_2', 'steam_3', 'steam_4']
        if $flag
          js_method_many(time, 5, 3, item)  #返回 @steam_values   @steam_month_sum 
        
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 5, 3, item)
        end
  
        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      #-----------------
      #排污 数据  
      if s[:code] == 'pollution_2'   #洗柱水

        if $flag
          js_method_one(time, 5, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      if s[:code] == 'pollution_3'   #中合水

        if $flag
          js_method_one(time, 5, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      if s[:code] == 'pollution_4'   #生产废水
        if $flag
           js_method_one(time, 5, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end
      
      if s[:code] == 'pollution_5'   #降膜水

        if $flag
          js_method_one(time, 5, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量

        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      if s[:code] == 'pollution_6'   #甲醇废水
        if $flag
          js_method_one(time, 5, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      if s[:code] == 'pollution_7'   #洗效水
        if $flag
          js_method_one(time, 5, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end
      @steam_datas.push(item1)
    end


#--------------------------






    #--------------------
    @today_time = time
    
    @plant_hash[:form_name] = @form_name
    @plant_hash[:d_time] = @today_time
    @plant_hash[:info] = @region_datas
    @plant_hash[:steam_info] =@steam_datas
    @plant_hash[:total_yield] =@total_yield

    return @plant_hash



  end


  
  desc "发酵车间报表接口", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    #requires :r_time_type, type: String, desc: "请求时间类型，年 Y 月 M 日 D ;默认传 D "
    optional :r_time, type: String, desc: "请求时间，非必填，格式< 2018-05-08 >"
    optional :form_code, type: String, desc: "表单code"
  end
  get "/ferment" do
    authenticate!

    @plant_hash = {}
    if params[:r_time].present?
      time = params[:r_time]
    else
     # time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
      time = Time.now.strftime("%Y-%m-%d")  #默认查询当天的日期
    end

    form_code = params[:form_code]  #表单code
    
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名

    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids)      #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)  

      item[:nums] = field_value.present? ? field_value.nums : ''         #日产值
      item[:daily_yield] = field_value.present? && field_value.daily_yield.present? ? field_value.daily_yield : ''  #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] =  @month_sum.round(2)  #月累计

      #质量信息
      data2 = []

      if 'TF' == plant.code  #头粉
        data2.push({:technic_01 => '批号' , :technic_02 => '波美度', :technic_03 => '淀粉酶量'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'TY' == plant.code  #糖液
        data2.push({:technic_01 => '批号' , :technic_04 => '糖值', :technic_05 => 'DE值', :technic_06 => '周期'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'FJY' == plant.code  #发酵液
        data2.push({:technic_01 => '批号' , :technic_07 => '放罐旋光', :technic_08 => '滤速', :technic_06 => '周期'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'SHY' == plant.code  #酸化液
        data2.push({:technic_01 => '批号' , :technic_09 => '交料旋光', :technic_10 => '交料透光', :technic_11 => 'Ca2+', :technic_12 => 'PH'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas.push(item)
    end



    #--------v2-----------
    #发酵车间蒸汽  总收率  数据
    @daily_report_shy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'SHY')
    @daily_report_fjy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'FJY')
    shy_yield= @daily_report_shy.present? ? @daily_report_shy.daily_yield : 0
    fjy_yield= @daily_report_fjy.present? ? @daily_report_fjy.daily_yield : 0

    @total_yield = ((shy_yield.to_f * fjy_yield.to_f) / 100).round(2)    #发酵车间总收率
    
    #蒸汽 日产量
    @steam_values = SNenrgyValue.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => 3, :datetime => time)
    @steam_month_sum = 0              #蒸汽 月累计
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #查询日期月的开始日期
    @steam_month_values = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => 3)
    @steam_month_values.each do |data|
      @steam_month_sum += data.field_value.to_f
    end
    @steam_month_sum = @steam_month_sum.round(2)  #蒸汽 月累计

    @day_steam = @steam_values.present? ? @steam_values.field_value : ''   #发酵车间 蒸汽日用量
    #发酵车间计算 日单耗 月单耗
    @shy_nums= @daily_report_shy.present? ? @daily_report_shy.nums : 1   #酸化液的 日产量
    @shy_month_nums = 1    #酸化液的 月产量
    @shy_months = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => 'SHY')  #酸化液的 月累计
    @shy_months.each do |s|
      @shy_month_nums += s.nums.to_f
      @shy_month_nums -= 1
    end

    #@shy_month_nums = @shy_month_nums.round(2)
    # @day_expend_fj = (@day_steam.to_f / @shy_nums.to_f).round(2)                #发酵车间日单耗
    # @month_expend_fj = (@steam_month_sum.to_f / @shy_month_nums.to_f).round(2)  #发酵车间月单耗

    @day_expend = (@day_steam.to_f / @shy_nums.to_f).round(2)                #发酵车间日单耗
    @month_expend = (@steam_month_sum.to_f / @shy_month_nums.to_f).round(2)  #发酵车间月单耗



   #-------------------------------
    #蒸汽 表格数据
    @steam_datas = []
    steam_data ={:day_expend => @day_expend, :month_expend => @month_expend, :steam_values => @day_steam, :steam_month_sum => @steam_month_sum}
    @steam_hash = {:name => '蒸汽', :steam_data => steam_data}
    @steam_datas.push(@steam_hash)


    #------------------------
    @today_time = time

    @plant_hash[:form_name] = @form_name
    @plant_hash[:d_time] = @today_time
    @plant_hash[:info] = @region_datas
    @plant_hash[:steam_info] =@steam_datas
    @plant_hash[:total_yield] =@total_yield

    return @plant_hash
  end

  desc "精制车间报表接口", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
  }
  params do
    #requires :r_time_type, type: String, desc: "请求时间类型，年 Y 月 M 日 D ;默认传 D "
    optional :r_time, type: String, desc: "请求时间，非必填，格式< 2018-05-08 >"
    optional :form_code, type: String, desc: "表单code"
  end
  get "/refining" do
    authenticate!
    @plant_hash = {}
    if params[:r_time].present?
      time = params[:r_time]
    else
      #time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
      time = Time.now.strftime("%Y-%m-%d")     #默认查询当天的数据
    end

    form_code = params[:form_code]  #表单code

   #------------------------
#根据region_code id获取报表
@d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
@form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名

@form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
@region_datas = []

@ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
@plants = SMaterial.where(:id => @ids)      #从材料表中获取
@plants.each do |plant|
  item = {}
  item[:name] = plant.name
  item[:code] = plant.code

  field_value = @form_values.find_by(:code => plant.code)  

  item[:nums] = field_value.present? ? field_value.nums : ''                #日产值
  item[:daily_yield] = field_value.present? ? field_value.daily_yield : ''  #日收率

  #计算月累计
  @month_sum = 0
  month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
  @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
  @month_values.each do |m|
    @month_sum += m.nums.to_f
  end
  item[:month_value] =  @month_sum.round(2)  #月累计

  #质量信息
  data2 = []

  if 'YCL' == plant.code  #一次料
    data2.push({:technic_01 => '批号' , :technic_17 => '比重', :technic_18 => '料浆含量', :technic_19 => '加炭量',:technic_20 => '一次母液含量',:technic_21 => '溶解温度',})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end

  if 'ECL' == plant.code  #二次料
    data2.push({:technic_01 => '批号' , :technic_21 => '溶解温度', :technic_12 => 'PH', :technic_22 => '加EDTA量', :technic_23 => '加草酸量'})
    @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
    data2.push({:data => @s_teachnics})
  end


  item[:data] = data2
  @region_datas.push(item)
end

  #------------v2--------
    #蒸汽  总收率  数据
    #计算精制车架总收率 = 一次料 * 二次料
    @daily_report_yc = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'YCL')
    @daily_report_ec = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'ECL')
    
    yc_yield= @daily_report_yc.present? ? @daily_report_yc.daily_yield : 0
    ec_yield= @daily_report_ec.present? ? @daily_report_ec.daily_yield : 0

    @total_yield_jz = ((yc_yield.to_f * ec_yield.to_f) / 100).round(2)   #精制车间总收率

    #精制车间日产量=钠包装量+酸包装量*0.73
    #精制车间 计算 日单耗 = 日蒸汽/ 精制车间日产量     月单耗 = 月蒸汽/ 精制车间月产量
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
    @daily_report_na = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'na_nums')   # na_nums  在 s_region_code_id = 8
    @daily_report_na_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'na_nums')
    @daily_report_suan = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'suan_nums')
    @daily_report_suan_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'suan_nums')

    na_day_nums = @daily_report_na.present? ? @daily_report_na.nums : 1        #钠日产量
    suan_day_nums = @daily_report_suan.present? ? @daily_report_suan.nums : 1  #酸日产量

    @nums_day_jz =  na_day_nums + suan_day_nums * 0.73     #精制车间日产量

    dd1= 0
    dd2 =0
    @daily_report_na_month.each do |d1|
      dd1 += d1.nums.to_f
    end
    @daily_report_suan_month.each do |d1|
      dd2 += d1.nums.to_f
    end

    if @daily_report_na_month.present? && @daily_report_suan_month.present?
      @nums_month_jz = dd1 + dd2 * 0.73      #精制车间 月累计产量
    else
      @nums_month_jz = 1
    end

    #@nums_month_jz = dd1 + dd2 * 0.73      #精制车间 月累计产量

    #精制车间 蒸汽 表格数据
    @steam_datas = []
    steam_names = [{:name => '浓缩', :code => 'steam_6'}, 
                  {:name => '溶解', :code => 'steam_5'}, 
                  {:name => '合计', :code => 'total'},
                  {:name => '生产废水', :code => 'pollution_4'},
                  {:name => '降膜水', :code => 'pollution_5'}
                ]
    steam_names.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]

      #计算单个 项目 蒸汽的日用量  月累计
      if s[:code] == 'steam_6'   #浓缩
        if $flag
          js_method_one(time, 7, 3, s[:code])  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 3, s[:code])
        end

        item1[:steam_values]=  @steam_values.round(2)           #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2)   #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz ).round(2)        #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2)    #月单耗
      end

      if s[:code] == 'steam_5'   #溶解
        if $flag
          js_method_one(time, 7, 3, s[:code])  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 3, s[:code])
        end

        item1[:steam_values]=  @steam_values.round(2)            #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2)    #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz ).round(2)        #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2)    #月单耗
      end

      if s[:code] == 'total'   #合计
        item = ['steam_6', 'steam_5']
        if $flag
          js_method_many(time, 7, 3, item)  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 7, 3, item)
        end

        item1[:steam_values]=  @steam_values.round(2)            #合计日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2)    #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz ).round(2)        #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2)    #月单耗
      end


      if s[:code] == 'pollution_4'   #生产废水
        if $flag
          js_method_one(time, 7, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz ).round(2)        #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2)    #月单耗
      end

      if s[:code] == 'pollution_5'   #降膜水
        if $flag
          js_method_one(time, 7, 5, s[:code])  #返回 @steam_values   @steam_month_sum 
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 5, s[:code])
        end

        item1[:steam_values]=  @steam_values           #合计日用量
        item1[:steam_month_sum]= @steam_month_sum   #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz ).round(2)        #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2)    #月单耗
      end

      @steam_datas.push(item1)
    end




    #-----------------
    @today_time = time

    @plant_hash[:form_name] = @form_name
    @plant_hash[:d_time] = @today_time
    @plant_hash[:info] = @region_datas
    @plant_hash[:steam_info] =@steam_datas
    @plant_hash[:total_yield] =@total_yield_jz

    return @plant_hash
  end


end
