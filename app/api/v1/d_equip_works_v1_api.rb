class DEquipWorksV1API < Grape::API

  #设备点检管理

  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
      get_region
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end

    def get_region
      #根据登录人员 获取人员所在的车间 或公司
      # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
      # @region_code = SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
      @region_code = SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    end

    #上传图片
    def upload_img(image_file, equip)
      if image_file.present?
        time = Time.now
        img_flag = "jpeg" if image_file[0, 15] == "data:image/jpeg"
        img_flag = "png" if image_file[0, 14] == "data:image/png"
        img_file = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
        img_name = "#{equip.id}.#{img_flag}"
        file_path = "#{Rails.root}/public/test/d_equip_works/#{time.year}/#{time.month}"
        FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
        #向dir目录写入文件
        File.open(Rails.root.join("#{file_path}", img_name), 'wb') {|f| f.write(img_file)}
        return "test/d_equip_works/#{time.year}/#{time.month}/#{img_name}"
      end
    end

  end

  desc "设备点检管理首页", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
     optional :status, type: String, desc: "状态查询：wait_carried 处理中； carry_out 已完成 "
     optional :begin_time, type: String, desc: "点检计划开始开始时间 2018-06-04 "
     optional :end_time, type: String, desc: "点检计划结束时间 2018-06-12 "
     optional :region_id, type: Integer, desc: "车间id"
    # optional :page, type: Integer, desc: "页码"
  end
  get "/" do
    authenticate!

    if @region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      region_z = SRegionCode.where(:s_region_level_id => 3)
      region_code_ids = region_z.pluck(:id)  #查询所有车间
    end

    if @region_code.s_region_level_id == 2  #2 分公司   1 总公司   3 车间
      region_f = SRegionCode.where(:father_region_id => @region_code.id, :s_region_level_id => 3)
      region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
    end

    if @region_code.s_region_level_id == 3  #2 分公司   1 总公司   3 车间
      region_code_ids = @region_code.id
    end

    #Rails.logger.info "-----region_code_ids-------#{region_code_ids}----------"

    end_time = params[:end_time]
    if end_time.present?
      time_end = Time.parse(end_time).end_of_day
    end

   # Rails.logger.info "-----time_end-------#{time_end}----------"
    
    
    #机修人员只看自己的工单
    if @current_user.s_role_msg.role_code == 'mechanic_repair'
      @equip_works = DEquipWork.where(:s_region_code_id => region_code_ids,:work_person => @current_user.login_name).by_region_id(params[:region_id])
                              .by_work_time(params[:begin_time], time_end)
                              .by_work_status(params[:status])
                              .order(:created_at => 'desc')
    else
      @equip_works = DEquipWork.where(:s_region_code_id => region_code_ids).by_region_id(params[:region_id])
                              .by_work_time(params[:begin_time], time_end)
                              .by_work_status(params[:status])
                              .order(:created_at => 'desc')
    end

    #按车间进行分组
    @equips_hc = @equip_works.select{|x| x.region_name == '合成车间'}
    @equips_fj = @equip_works.select{|x| x.region_name == '发酵车间'}
    @equips_jz = @equip_works.select{|x| x.region_name == '精制车间'}
    @equips_hb = @equip_works.select{|x| x.region_name == '烘包车间'}

    @form_data = []     #所有车间数据

    if @equips_hc.present? && @equips_hc.length > 0
      form_data_hc = {}   #合成车间数据
      form_data_hc[:name] = '合成车间'
      data=[]
      @equips_hc.each do |e|
        item = {}
        item[:id] = e.id                    #点检记录id
        s_area = SArea.find_by(:id => e.s_area_id)  #查询区域
        area_name = s_area.present? ? s_area.name : ''
        item[:area_name] = area_name    #区域名称
        item[:work_status] = e.work_status_str  #状态
        item[:founder] = e.founder          #创建人
        item[:work_desc] = e.work_desc      #描述
        item[:work_start_time] = e.work_start_time.strftime("%Y-%m-%d %H:%M")      
        item[:work_end_time] = e.work_end_time.strftime("%Y-%m-%d %H:%M")      
        data.push(item)
      end
      form_data_hc[:data] = data
      @form_data.push(form_data_hc)
    end


    if @equips_fj.present? && @equips_fj.length >0
      form_data_fj = {}   #发酵车间数据
      form_data_fj[:name] = '发酵车间'
      data=[]
      @equips_fj.each do |e|
        item = {}
        item[:id] = e.id                    #点检记录id
        s_area = SArea.find_by(:id => e.s_area_id)  #查询区域
        area_name = s_area.present? ? s_area.name : ''
        item[:area_name] = area_name    #区域名称
        item[:work_status] = e.work_status_str  #状态
        item[:founder] = e.founder          #创建人
        item[:work_desc] = e.work_desc      #描述
        item[:work_start_time] = e.work_start_time.strftime("%Y-%m-%d %H:%M")      
        item[:work_end_time] = e.work_end_time.strftime("%Y-%m-%d %H:%M")      
        data.push(item)
      end
      form_data_fj[:data] = data
      @form_data.push(form_data_fj)
    end

    if @equips_jz.present? && @equips_jz.length >0
      form_data_jz = {}   #精制车间数据
      form_data_jz[:name] = '精制车间'
      data=[]
      @equips_jz.each do |e|
        item = {}
        item[:id] = e.id                    #点检记录id
        s_area = SArea.find_by(:id => e.s_area_id)  #查询区域
        area_name = s_area.present? ? s_area.name : ''
        item[:area_name] = area_name    #区域名称
        item[:work_status] = e.work_status_str  #状态
        item[:founder] = e.founder          #创建人
        item[:work_desc] = e.work_desc      #描述
        item[:work_start_time] = e.work_start_time.strftime("%Y-%m-%d %H:%M")      
        item[:work_end_time] = e.work_end_time.strftime("%Y-%m-%d %H:%M")      
        data.push(item)
      end
      form_data_jz[:data] = data
      @form_data.push(form_data_jz)
    end

    if @equips_hb.present? && @equips_hb.length >0
      form_data_hb = {}   #烘包车间数据
      form_data_hb[:name] = '烘包车间'
      data=[]
      @equips_hb.each do |e|
        item = {}
        item[:id] = e.id                    #点检记录id
        s_area = SArea.find_by(:id => e.s_area_id)  #查询区域
        area_name = s_area.present? ? s_area.name : ''
        item[:area_name] = area_name    #区域名称
        item[:work_status] = e.work_status_str  #状态
        item[:founder] = e.founder          #创建人
        item[:work_desc] = e.work_desc      #描述
        item[:work_start_time] = e.work_start_time.strftime("%Y-%m-%d %H:%M")      
        item[:work_end_time] = e.work_end_time.strftime("%Y-%m-%d %H:%M")      
        data.push(item)
      end
      form_data_hb[:data] = data
      @form_data.push(form_data_hb)
    end

    return @form_data
  end

  desc "查询接口---获取所有点检状态", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
}
params do
  #  optional :id, type: Integer, desc: "维修记录id"
  #  optional :image_file, type: File,desc: '待执行图片'
end
get "/get_status" do
  authenticate!

 # 变量 使用 @ 时  会报错
  status = [{code:'wait_issued', name:'待下发'},{code:'wait_carried', name:'处理中'},{code:'carry_out', name:'完成'}]
  
  return  {status:status }
end






desc "设备点检管理详细页面", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
}
params do
   optional :id, type: Integer, desc: "点检记录id"

end
get "/show" do
    authenticate!
    @equip_work = DEquipWork.find(params[:id])
    flow = [{code:'wait_issued', name:'待下发'},{code:'wait_carried', name:'处理中'},{code:'carry_out', name:'完成'}]

    boot_s = @equip_work.work_boots
    s_area = SArea.find_by(:id => @equip_work.s_area_id)  #查询区域
    area_name = s_area.present? ? s_area.name : ''

    @boots = []
    boot_s.each {|x| @boots << {id: x.id, boot_check_id: x.boot_check_id, check_project: x.check_project, work_result: DEquipWork.set_work_result(x.work_result), abnor_desc: x.abnor_desc, }}
    note_s = @equip_work.work_notes
    @notes = []
    note_s.each {|x| @notes << {id: x.id, login_name: x.login_name, status: WorkNote.set_status(x.status), note_time: x.created_at.strftime("%Y-%m-%d %H:%M:%S")}}
    
    @form_data = {equip_work: {id: @equip_work.id,area_name: area_name, region_name: @equip_work.region_name, equip_name: @equip_work.equip_name,
                                   equip_code: @equip_work.equip_code, work_person: @equip_work.work_person, work_start_time: @equip_work.work_start_time ? @equip_work.work_start_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                                   work_end_time: @equip_work.work_end_time ? @equip_work.work_end_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                                   work_status: @equip_work.work_status, work_desc: @equip_work.work_desc,
                                   handle_time: @equip_work.handle_time ? @equip_work.handle_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                                   founder: @equip_work.founder}, 
                  boots: @boots, 
                  notes: @notes,
                  img_path: @equip_work.img_path.present? ? @equip_work.img_path : '' ,
                  flow: flow
                }


    return @form_data
  end



  desc "设备点检管理，保存操作", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
}
params do
   optional :id, type: Integer, desc: "点检记录id"
   optional :image_file, type: String, desc: "点检图片"
   optional :boot_hash, type: Array[String],desc: '[{"boot_id"=>"69", "work_result"=>"N", "abnor_desc"=>"asfasf"},{"boot_id"=>"70", "work_result"=>"Y", "abnor_desc"=>""}]'
end

post "/carried_out" do
  authenticate!

  @equip_work = DEquipWork.find(params[:id])

  params_datas = params[:boot_hash]

  Rails.logger.info "----params_datas----#{params_datas}-----"
  Rails.logger.info "----params_datas----#{params_datas[0]}-----"
  Rails.logger.info "----params_datas----#{params_datas[0].class}-----"

  datas = eval(params_datas[0]) if params_datas[0].present?


  if @equip_work.work_status == 'wait_carried'
    datas.each do |b|

      Rails.logger.info "----bbbbb-----#{b}----"

      boot = WorkBoot.find_by(:id => b[:boot_id].to_i)

      if boot.update(:work_result =>b[:work_result], :abnor_desc =>b[:abnor_desc])
        note = WorkNote.find_by(:d_equip_work_id => @equip_work.id, :status => 'deal_with')
        if note
          note.update(:login_name => @current_user.login_name)
        else
          WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'deal_with', :login_name => @current_user.login_name)
        end
      end
    end

    #添加上传图片
    image_file = params[:image_file]
    if image_file.present?
      #获取到上传的图片路径
      img_path = upload_img(image_file, @equip_work)
      @equip_work.update(:work_status => 'carry_out', :handle_time => Time.now, :img_path => img_path)
    end

    #@equip_work.update(:work_status => 'carry_out', :handle_time => Time.now)
    WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'carry_out', :login_name => @current_user.login_name)

   return {status: 200}
  end

end



end