class DailyReportV1API < Grape::API

  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end
  end

  desc "车间生产上报 首页显示数据", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    # optional :station_ids, type: String, desc: "站点id， 101,102,103"
    # optional :page, type: Integer, desc: "页码"
  end
  get "/" do
    authenticate!

    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => current_user.group_id) #获取人员所在车间 或公司

    #车间项目 信息
    #@plants = @region_code.d_plants
    @ids = DMaterialReginCode.where(:s_region_code_id => @region_code.id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取

    @d_report_form = DReportForm.find_by(:s_region_code_id => @region_code.id) #车间的报表
    @form_name = @d_report_form.present? ? @d_report_form.name : ''

    #当天的 日报表
    time = Time.now.strftime("%Y-%m-%d")
    @daily_report = DDailyReport.where(:datetime => time, :s_region_code_id => @region_code.id)

    @date=[]
    @plants.each do |p|
      item={}
      item["id"] = p.id
      item["name"] = p.name
      daily_report = @daily_report.find_by(:s_material_id => p.id)

      if daily_report.present? && daily_report.nums.present?
        nums = daily_report.nums
      else
        nums = ''
      end
      item["nums"] = nums

      #查询关系类型
      relationcode = DMaterialReginCode.find_by(:s_material_id => p.id)
      @d_relation_type = DRelationType.find_by(:id => relationcode.d_relation_type_id)
      item["relation_type"] = @d_relation_type.present? ? @d_relation_type.name : '' #消耗/产出

      if daily_report.present? && daily_report.daily_yield.present?
        daily_yield = daily_report.daily_yield
      else
        daily_yield = ''
      end
      item["daily_yield"] = daily_yield #日收率

      @date.push(item)
    end

    #日报状态
    reports = @daily_report.where(:status => 'Y') if @daily_report.present?

    if reports.present? && reports.length >0
      return {status: '已上报', plants: @date, form_name: @form_name, region_code: @region_code}
    else
      return {status: '未上报', plants: @date, form_name: @form_name, region_code: @region_code}
    end
  end


  desc "保存、上报操作", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :date_time, type: String, desc: '当前日期'
    optional :status, type: String, desc: 'Y  上报  N 保存'


    # requires :d_daily_reports,type: Array[Hash] do
    #     requires :data, type: Hash do
    #       requires :id, type: Integer
    #       requires :name, type: String
    #       requires :nums, type: String
    #    end
    # end

    requires :d_daily_reports, type: Array[String], desc: "项目名称"
  end
  post "/report" do
    authenticate!
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    @d_daily_reports = params[:d_daily_reports]
    status = params[:status] #  Y 上报   N 保存
    date = params[:date_time]

    # Rails.logger.info "----1----#{@d_daily_reports}----------"
    # Rails.logger.info "----11----#{@d_daily_reports[0]}----------"
    # Rails.logger.info "---2-----#{@d_daily_reports[0].class}----------"
    #Rails.logger.info "-----3---#{eval(@d_daily_reports[0])}----------"

    @d_daily = eval(@d_daily_reports[0]) if @d_daily_reports[0].present?

    @d_daily.each do |d|

#   Rails.logger.info "-------#{d}-------"
#   Rails.logger.info "-------#{d[0]}-------"
#   Rails.logger.info "-------#{d[1]}-------"

#   #判断是否已经存在当天的日报
      @day_report = DDailyReport.find_by(:s_region_code_id => @region_code.id, :datetime => date, :name => d[:name])
      if @day_report.present?
        @day_report.update(:nums => d[:nums], :status => status, :daily_yield => d[:daily_yield])
      else
        @d_daily_report = DDailyReport.new
        @d_daily_report.name = d[:name]
        @d_daily_report.nums = d[:nums]
        @d_daily_report.s_region_code_id = @region_code.id
        @d_daily_report.datetime = date
        @d_daily_report.status = status

        #根据id 获取code
        s_material = SMaterial.find_by(:id => d[:id])
        @d_daily_report.s_material_id = s_material.id #物料项目id
        @d_daily_report.code = s_material.code #物料项目code

        @d_daily_report.daily_yield = d[:daily_yield] #日收率
        @d_daily_report.save
      end
    end

    return {status: 'success'}
  end


  desc "公司生产日报", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :begin_day, type: DateTime, desc: "查询的开始日期"
    optional :end_day, type: DateTime, desc: "查询的结束日期"
    optional :region_id, type: Array[String], desc: "选择的车间id，数组形式"
  end

  get "/day_reports" do
    authenticate!

    #获取查询参数
    begin_day1 = params[:begin_day]
    end_day1 = params[:end_day]
    params_region_id = params[:region_id]

    if params_region_id.present?
      params_region_ids = params_region_id[0].split(",")
    end

    if begin_day1.present? && end_day1.present?
      begin_day = begin_day1.strftime("%Y-%m-%d")
      end_day = end_day1.strftime("%Y-%m-%d")
    else
      begin_day = Time.now.beginning_of_month.strftime("%Y-%m-%d")
      end_day = Time.now.strftime("%Y-%m-%d")
    end


    #根据登录人员 获取人员所在的车间 或公司
    # @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code1= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司


    #当天的 日报表
    time_begion=Time.now.beginning_of_month.strftime("%Y-%m-%d")
    time = Time.now.strftime("%Y-%m-%d")


    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1 #2 分公司   1 总公司   3 车间
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 2
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_f = SRegionCode.where(:father_region_id => @region_code1.id)
        region_code_ids = region_f.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 3
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        @region_code = @region_code1
        region_code_ids = @region_code.id
      end
    end


    @daily_reports = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                         .by_datetime(begin_day, end_day) #按日期查询
                         .order(:s_region_code_id => :desc, :created_at => :desc)

    #折现图数据 不要分页
    @daily_reports1 = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                          .by_datetime(begin_day, end_day) #按日期查询
                          .order(:created_at => :desc)


    @sum_dates = DDailyReport.by_datetime(time_begion, time).where(:s_region_code_id => region_code_ids, :status => 'Y')

    #车间项目 信息
    @ids = DMaterialReginCode.where(:s_region_code_id => region_code_ids).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取


    #获取当前月份
    @months=[]
    @months = @daily_reports1.pluck(:datetime).uniq.sort #折现图使用
    #@months = @daily_reports1.pluck(:datetime).uniq  #折现图使用

    #查询当前车间  各个项目的 月数据
    #根据登录人员 获取人员所在的车间 或公司
    # @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => @region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    @month_days=[]

    @plants.each do |p|
      item={}
      item["name"] = p.name

      z_data=[] #折现数据
      @months.each do |m|
        num= DDailyReport.find_by(:datetime => m, :s_region_code_id => region_code_ids, :name => p.name)
        if num.present?
          nums = num.nums
        else
          nums = nil
        end
        z_data.push(nums) #不管当天有没有数据都填充一条记录
      end

      item["data"] = z_data

      @month_days.push(item)
    end

    #列表数据
    @date2 = []
    @daily_reports.each do |d|
      item = {}

      item["id"] = d.id
      item["nums"] = d.nums
      item["month"] = d.datetime
      item["name"] = d.name

      #分公司
      #json.region_code_info @region_code_info.unit_name
      #判断是车间级  分公司级  还是总公司级
      code = SRegionCode.find_by(:id => d.s_region_code_id)
      @region_level = SRegionLevel.find_by(:id => code.s_region_level_id)
      if @region_level.level_name == '车间'
        region_name = SRegionCode.find_by(:id => code.father_region_id).region_name
      elsif @region_level.level_name == '分公司'
      elsif @region_level.level_name == '总公司'
      end
      item["region_code_info"] = region_name #分公司


      #车间
      #根据 项目  查找 车间
      #material = SMaterial.find_by(:name => d.name)
      Rails.logger.info "-------code-----#{d.code}-----------"
      material = SMaterial.find_by(:code => d.code)
      relationcode = DMaterialReginCode.find_by(:s_material_id => material.id)
      region_code = SRegionCode.find_by(:id => relationcode.s_region_code_id)
      item["region_code"] = region_code.region_name #车间

      #查询关系类型
      @d_relation_type = DRelationType.find_by(:id => relationcode.d_relation_type_id)
      item["relation_type"] = @d_relation_type.name #消耗/产出


      #计算累计量
      sum_nums = 0
      nums= @sum_dates.where(:name => d.name).pluck(:nums)
      # sum_nums = nums.sum
      if nums.length > 0
        nums.each do |n|
          if n.present?
            sum_nums += n
          end
        end
      end
      item["sum_nums"] = sum_nums
      @date2.push(item)
    end

    return {months: @months, month_days: @month_days, daily_reports: @date2}

  end


  desc "公司生产月报", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :month_now, type: String, desc: "查询的结束月份"
    optional :month_begin, type: String, desc: "查询的开始月份"
    optional :region_id, type: Array[String], desc: "选择的车间id，数组形式"
  end

  get "/month_reports" do
    authenticate!

    #获取查询参数
    params_region_id = params[:region_id]
    @month_begin1 = params[:month_begin]
    @month_end1 = params[:month_now]

    if params_region_id.present?
      params_region_ids = params_region_id[0].split(",")
    end

    if @month_begin1.present? && @month_end1.present?
      month_begin = @month_begin1 + '-1'
      dd1 = Time.parse(@month_end1 + '-1')
      month_end = dd1.end_of_month.strftime("%Y-%m-%d")
      @time_begion = month_begin #列表显示的月份
      time = month_end
    else
      @time_begion=Time.now.beginning_of_month.strftime("%Y-%m")
      time = Time.now.strftime("%Y-%m-%d")
    end


    #根据登录人员 获取人员所在的车间 或公司
    # @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code1= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司


    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1 #2 分公司   1 总公司   3 车间
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 2
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_f = SRegionCode.where(:father_region_id => @region_code1.id)
        region_code_ids = region_f.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 3
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        @region_code = @region_code1
        region_code_ids = @region_code.id
      end
    end

    @ids = DMaterialReginCode.where(:s_region_code_id => region_code_ids).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取


    @daily_reports = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                         .by_region_ids(params_region_ids) #按车间查询
                         .by_datetime(@time_begion, time) #按月查询
                         .order(:created_at => :desc)
                         .page(params[:page]).per(params[:per])


    #折线图数据
    @daily_reports2 = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                          .by_datetime(@time_begion, time)
                          .order(:created_at => :desc)

    @sum_dates = DDailyReport.by_datetime(@time_begion, time).where(:s_region_code_id => region_code_ids, :status => 'Y')


    #获取当前月份
    @months=[]
    @m2 =[]
    @m22 =[] #折现图使用
    @months = @daily_reports2.pluck(:datetime).uniq.sort #折现图时间数据
    @months.each do |m|
      t1 = Time.parse(m).strftime("%Y-%m") +'-1'
      @m2.push(t1)
    end
    @months.each do |m|
      t1 = Time.parse(m).strftime("%Y-%m")
      @m22.push(t1)
    end
    @m3 = @m2.uniq.sort #带 日的
    @m33 = @m22.uniq.sort #折现图使用  不带 日的

    @ids = DMaterialReginCode.where(:s_region_code_id => region_code_ids).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids).page(params[:page]).per(params[:per]) #从材料表中获取

    #列表数据
    @date2 =[] #自己封装多个月的数据
    @m33.each do |m|
      @plants.each do |p|
        item={}
        item["month"] = m
        code1 = DMaterialReginCode.find_by(:s_material_id => p.id)
        code = SRegionCode.find_by(:id => code1.s_region_code_id)
        #判断是车间级  分公司级  还是总公司级
        @region_level = SRegionLevel.find_by(:id => code.s_region_level_id)
        if @region_level.level_name == '车间'
          region_name = SRegionCode.find_by(:id => code.father_region_id).region_name

        elsif @region_level.level_name == '分公司'

        elsif @region_level.level_name == '总公司'

        end
        item["region_code_info"] = region_name #分公司

        #根据 项目  查找 车间
        relationcode = DMaterialReginCode.find_by(:s_material_id => p.id)
        region_code = SRegionCode.find_by(:id => relationcode.s_region_code_id)
        item["region_code"] = region_code.region_name #车间
        item["name"] = p.name #车间

        #查询关系类型
        @d_relation_type = DRelationType.find_by(:id => relationcode.d_relation_type_id)
        item["relation_type"] = @d_relation_type.name #消耗/产出

        #计算累计量 要使用@m33中月份的最后一天作为结束条件  列表数据
        @m_end = Time.parse(m + '-1').end_of_month.strftime("%Y-%m-%d")
        nums= DDailyReport.by_datetime(m, @m_end).where(:s_region_code_id => region_code_ids, :name => p.name).pluck(:nums)
        sum_nums = 0
        if nums.length > 0
          nums.each do |n|
            sum_nums += n
          end
        end

        item["sum_nums"] = sum_nums #累计产量
        @date2.push(item)
      end
    end

    @date2 = @date2.sort_by {|d| d["region_code_info"]} #按分公司排序

    #-----------------------------
    #折现图数据

    #查询当前车间  各个项目的 月数据
    #根据登录人员 获取人员所在的车间 或公司
    # @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => @region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    #车间项目 信息
    @month_days=[]
    #获取最小查询日期
    min_day = @m3.min

    @plants.each do |p|
      item={}
      item["name"] = p.name
      @data=[] #存3 月  4月的总量
      @m3.each do |m|

        #要使用@m3中月份的最后一天作为结束条件   折线图数据
        m_end = Time.parse(m).end_of_month.strftime("%Y-%m-%d")
        nums= DDailyReport.by_datetime(m, m_end).where(:s_region_code_id => region_code_ids, :name => p.name).pluck(:nums)

        sum_nums = 0
        if nums.length > 0
          nums.each do |n|
            sum_nums += n
          end
        else
          sum_nums = nil
        end

        @data.push(sum_nums)
      end
      item["data"] = @data #必须是数组
      @month_days.push(item)
    end


    return {months: @m33, month_days: @month_days, daily_reports: @date2}
  end


  desc "日生产报表", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }

  params do
    optional :end_day, type: String, desc: "查询的日期"
    optional :region_id, type: Array[String], desc: "选择的车间id，数组形式"
  end

  get "/days_report" do
    authenticate!

    #获取查询参数
    end_day1 = params[:end_day]
    params_region_id = params[:region_id]

    if params_region_id.present?
      params_region_ids = params_region_id[0].split(",")
    end


    if end_day1.present?
      end_day = end_day1
    else
      end_day = Time.now.end_of_month.strftime("%Y-%m-%d")
    end


    #根据登录人员 获取人员所在的车间 或公司
    # @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code1= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    #当天的 日报表
    time_begion=Time.now.beginning_of_month.strftime("%Y-%m-%d")

    if end_day1.present?
      end_day = end_day1
    else
      end_day = Time.now.end_of_month.strftime("%Y-%m-%d")
    end
    #time = Time.now.strftime("%Y-%m-%d")

    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1 #2 分公司   1 总公司   3 车间

      @regin_code_f = SRegionCode.where(:s_region_level_id => 2).map {|x| {id: x.id, region_code: x.region_code, region_name: x.region_name}}

      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 2
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_f = SRegionCode.where(:father_region_id => @region_code1.id)
        region_code_ids = region_f.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 3
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        @region_code = @region_code1
        region_code_ids = @region_code.id
      end
    end


    #Rails.logger.info "-region_code_ids----#{region_code_ids}-------"

    @daily_reports = DDailyReport.where(:s_region_code_id => region_code_ids)
                         .by_datetime(end_day, end_day) #按日期查询
                         .order(:s_region_code_id => :desc, :created_at => :desc)


    @sum_dates = DDailyReport.by_datetime(time_begion, end_day).where(:s_region_code_id => region_code_ids, :status => 'Y')

    @dates2=[]
    @daily_reports.each do |d|
      item={}
      item["id"] = d.id
      item["nums"] = d.nums
      item["name"] = d.name
      item["datetime"] = d.datetime

      #分公司
      #json.region_code_info @region_code_info.unit_name
      #判断是车间级  分公司级  还是总公司级
      code = SRegionCode.find_by(:id => d.s_region_code_id)
      @region_level = SRegionLevel.find_by(:id => code.s_region_level_id)
      if @region_level.level_name == '车间'
        region_name = SRegionCode.find_by(:id => code.father_region_id).region_name
        #分公司
        #json.region_code_info region_name

      elsif @region_level.level_name == '分公司'
      elsif @region_level.level_name == '总公司'
      end

      #item["region_code_info"] =@region_code_info.unit_name  #分公司
      item["region_code_info"] = region_name #分公司


      #车间
      #json.region_code @region_code.region_name
      #根据 项目  查找 车间
      material = SMaterial.find_by(:name => d.name)
      relationcode = DMaterialReginCode.find_by(:id => material.id)
      region_code = SRegionCode.find_by(:id => relationcode.s_region_code_id)
      #json.region_code region_code.region_name

      #item["region_code"] = @region_code.region_name
      item["region_name"] = region_code.region_name #车间


      #查询关系类型
      @d_relation_type = DRelationType.find_by(:id => relationcode.d_relation_type_id)
      item["relation_type"] = @d_relation_type.name #消耗/产出


      #计算累计量
      sum_nums = 0
      nums= @sum_dates.where(:name => d.name).pluck(:nums)
      if nums.length > 0
        nums.each do |n|
          sum_nums += n
        end
      end
      item["sum_nums"] = sum_nums
      @dates2.push(item)
    end

    return {daily_reports: @dates2, regin_code_f: @regin_code_f}
  end


  desc "根据车间查询的弹窗接口", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do

  end
  get "/search_region_codes" do
    authenticate!
    #根据登录人员 获取人员所在的车间 或公司
    # @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    # @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id) #获取人员所在车间 或公司
    @region_code1= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司


    @unit1 = []
    region_code = SRegionCode.where(:father_region_id => 99999)
    region_code.each do |region|

      item={}
      item["id"] = region.id
      item["name"] = region.region_name #总公司级
      date1=[]

      #判断 是 车间级 还是  分公司级
      if @region_code1.s_region_level_id == 1 #2 分公司   1 总公司   3 车间
        two = SRegionCode.where(:father_region_id => region.id)
      elsif @region_code1.s_region_level_id == 2
        two = SRegionCode.where(:id => @region_code1.id)
      elsif @region_code1.s_region_level_id == 3
        two = SRegionCode.where(:id => @region_code1.father_region_id) #车间所在的分公司
      end

      two.each do |t|
        item2={}
        item2["id"] = t.id
        item2["name"] = t.region_name #分公司级

        date3 = []

        if @region_code1.s_region_level_id == 3
          three = SRegionCode.where(:id => @region_code1.id, :s_region_level_id => 3)
        else
          three = SRegionCode.where(:father_region_id => t.id, :s_region_level_id => 3)
        end


        three.each do |th|
          item3={}
          item3["id"] = th.id
          item3["name"] = th.region_name #车间级
          date3.push(item3)
        end

        item2["chil_data"] = date3 #分公司下的车间
        date1.push(item2)
      end

      item["chil_data"] = date1 #总公司下的分公司

      @unit1.push(item)
    end

    return {unit_region: @unit1}
  end


end