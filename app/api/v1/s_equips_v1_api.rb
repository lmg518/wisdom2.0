class SEquipsV1API < Grape::API

  #设备台账管理

  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
      get_region
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end

    def get_region
      #根据登录人员 获取人员所在的车间 或公司
      # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
      # @region_code = SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
      @region_code = SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    end

  end

  #查询接口
  desc "查询接口--获取车间", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
  }
  params do
    # optional :page, type: Integer, desc: "页码"
  end
  get "/get_region" do
    authenticate!
    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    if region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      @region_code = SRegionCode.select(:id, :region_name).where(:s_region_level_id =>3)
    elsif region_code.s_region_level_id == 2
      @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id =>region_code.id, :s_region_level_id =>3)
    elsif region_code.s_region_level_id == 3
      @region_code = SRegionCode.select(:id,:region_name).where(:id =>region_code.id, :s_region_level_id =>3)
    else
      @region_code = ''
    end

    
    return {region_code: @region_code}

  end

  desc "查询接口--根据车间查询车间下的设备", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
  }
  params do
    optional :region_ids, type: String, desc: "查询的设备id 例： 5 "
  end
  get "/get_equips" do
    authenticate!
    region_ids = params[:region_ids]
    @equips = SEquip.select(:id,:equip_name,:equip_code).where(s_region_code_id: region_ids)
    return {equips: @equips}
  end

  desc "设备台账管理首页", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
     optional :equip_ids, type: String, desc: "查询的设备id"
    # optional :page, type: Integer, desc: "页码"
  end
  get "/" do
    authenticate!
    if @region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      region_z = SRegionCode.where(:s_region_level_id => 3)
      region_code_ids = region_z.pluck(:id)  #查询所有车间
    end

    if @region_code.s_region_level_id == 2  #2 分公司   1 总公司   3 车间
      region_f = SRegionCode.where(:father_region_id => @region_code.id, :s_region_level_id => 3)
      region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
    end

    if @region_code.s_region_level_id == 3  #2 分公司   1 总公司   3 车间
      region_code_ids = @region_code.id
    end


    #Rails.logger.info "-----region_code_ids-------#{region_code_ids}----------"

    #机修人员只看自己区域的的设备
    if @current_user.s_role_msg.role_code == 'mechanic_repair'
      @equips = SEquip.where(:s_region_code_id => region_code_ids).by_s_area_id(@current_user.s_area_id).by_region(params[:region_ids])
                    .by_equip_ids(params[:equip_ids])
                    .order(:created_at => 'desc')
    else
      @equips = SEquip.where(:s_region_code_id => region_code_ids).by_region(params[:region_ids])
                    .by_equip_ids(params[:equip_ids])
                    .order(:created_at => 'desc')
    end    


    #按车间进行分组
    @equips_hc = @equips.select{|x| x.region_name == '合成车间'}
    @equips_fj = @equips.select{|x| x.region_name == '发酵车间'}
    @equips_jz = @equips.select{|x| x.region_name == '精制车间'}
    @equips_hb = @equips.select{|x| x.region_name == '烘包车间'}

    @form_data = []     #所有车间数据

    if @equips_hc.present? && @equips_hc.length > 0
      form_data_hc = {}   #合成车间数据
      form_data_hc[:name] = '合成车间'
      data=[]
      @equips_hc.each do |e|
        item = {}
        item[:equip_id] = e.id           #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:bit_code] = e.bit_code        #位号
        item[:equip_code] = e.equip_code        #设备编号
        item[:equip_location] = e.equip_location        #安装位置
        item[:equip_status] = e.equip_status        #设备状态  
        data.push(item)
      end
      form_data_hc[:data] = data
      @form_data.push(form_data_hc)
    end


    if @equips_fj.present? && @equips_fj.length >0
      form_data_fj = {}   #发酵车间数据
      form_data_fj[:name] = '发酵车间'
      data=[]
      @equips_fj.each do |e|
        item = {}
        item[:equip_id] = e.id           #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:bit_code] = e.bit_code        #位号
        item[:equip_code] = e.equip_code        #设备编号
        item[:equip_location] = e.equip_location        #安装位置
        item[:equip_status] = e.equip_status        #设备状态  
        data.push(item)
      end
      form_data_fj[:data] = data
      @form_data.push(form_data_fj)
    end

    if @equips_jz.present? && @equips_jz.length >0
      form_data_jz = {}   #精制车间数据
      form_data_jz[:name] = '精制车间'
      data=[]
      @equips_jz.each do |e|
        item = {}
        item[:equip_id] = e.id           #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:bit_code] = e.bit_code        #位号
        item[:equip_code] = e.equip_code        #设备编号
        item[:equip_location] = e.equip_location        #安装位置
        item[:equip_status] = e.equip_status        #设备状态  
        data.push(item)
      end
      form_data_jz[:data] = data
      @form_data.push(form_data_jz)
    end

    if @equips_hb.present? && @equips_hb.length >0
      form_data_hb = {}   #烘包车间数据
      form_data_hb[:name] = '烘包车间'
      data=[]
      @equips_hb.each do |e|
        item = {}
        item[:equip_id] = e.id           #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:bit_code] = e.bit_code        #位号
        item[:equip_code] = e.equip_code        #设备编号
        item[:equip_location] = e.equip_location    #安装位置
        item[:equip_status] = e.equip_status        #设备状态  
        data.push(item)
      end
      form_data_hb[:data] = data
      @form_data.push(form_data_hb)
    end

    return @form_data
  end


desc "设备台账管理详情页面,显示备件、维修记录", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
}
params do
   optional :equip_id, type: Integer, desc: "设备id"
  # optional :page, type: Integer, desc: "页码"
end
get "/show" do
  authenticate!
  @equip = SEquip.find(params[:equip_id])
  s_area = SArea.find_by(:id => @equip.s_area_id)
  area_name = s_area.present? ? s_area.name : ''

  #设备详情
  equip_data = {
      region_name: @equip.region_name,
      area_name: area_name,
      bit_code: @equip.bit_code,
      equip_code: @equip.equip_code,
      equip_name: @equip.equip_name,
      equip_location: @equip.equip_location,
      equip_norm: @equip.equip_norm,
      equip_nature: @equip.equip_nature,
      equip_material: @equip.equip_material,
      equip_num: @equip.equip_num,
      apper_code: @equip.apper_code,
      apper_time: @equip.apper_time.strftime("%Y-%m-%d"),
      factory: @equip.factory,
      equip_note: @equip.equip_note,
      equip_status: @equip.equip_status_str
  }
  @form_data = {}

  if @equip.present?
    #handle_equips = @equip.handle_equips   #设备的维修记录
    #只显示已完成的维修记录
    handle_equips = HandleEquip.where(:s_equip_id => @equip.id, :status => 'carry_out')

    s_spares = @equip.s_spares            #设备的备件
    handle = []  #设备的维修记录
    handle_equips.each do |h|  
      item = {}
      item[:id] = h.id
      item[:s_equip_id] = h.s_equip_id  
      item[:region_name] = h.region_name  #车间名称
      item[:equip_name] = h.equip_name  #设备名称
      item[:handle_name] = h.handle_name  #处理人
      item[:handle_start_time] = h.handle_start_time.strftime("%Y-%m-%d %H:%M")  #处理开始时间
      item[:handle_end_time] = h.handle_end_time.strftime("%Y-%m-%d %H:%M")      #处理结束时间
      item[:status] = h.status  #检修状态
      item[:desc] = h.desc  #描述
      item[:founder] = h.founder  #创建人
      item[:snag_desc] = h.snag_desc  #检修原因
      handle.push(item)
    end

    spares=[]  #设备的备件
    s_spares.each do |s|
      item={}
      item[:id] = s.id
      item[:s_equip_id] = s.s_equip_id
      equip =  SEquip.find_by(:id => s.s_equip_id)
      equip_name = equip.present? ? equip.equip_name : ''
      equip_code = equip.present? ? equip.equip_code : ''

      item[:equip_name] = equip_name  #设备名称
      item[:equip_code] = equip_code  #设备编号

      item[:spare_name] = s.spare_name  #备件名称
      item[:spare_code] = s.spare_code  #备件型号
      item[:spare_desc] = s.spare_desc  #备件描述
      item[:spare_material] = s.spare_material  #备件材质
      item[:spare_num] = s.spare_num  #备件数量
      spares.push(item)
    end
    @form_data[:equip_data] = equip_data
    @form_data[:handle_datas] = handle
    @form_data[:spares_datas] = spares
  end
  return @form_data
end



desc "设备台账管理维修记录详情页面", {
  headers: {
      "Authentication-Token" => {
          description: "用户Token",
          required: true
      }
  },
  failure: [[401, 'Unauthorized']]
}
params do
 optional :id, type: Integer, desc: "维修记录id"
# optional :page, type: Integer, desc: "页码"
end
get "/handler_show" do
  authenticate!

  @handle_equip = HandleEquip.find(params[:id])
  @boots = BootCheck.select(:id, :check_project)  #获取所有检查项信息

  h_overhaul_notes = @handle_equip.h_overhaul_notes
  if h_overhaul_notes
    start_note = h_overhaul_notes.find_by(:note_type => 'overhaul_begin')
    if start_note.present?
      @start_note = {id: start_note.id, note: start_note.note, img_path: start_note.img_path,
                     overhaul_desc: start_note.overhaul_desc, note_type: start_note.note_type,
                     overhaul_time: start_note.overhaul_time ? start_note.overhaul_time.strftime("%Y-%m-%d %H:%M:%S") : ''
      }
    else
      @start_note = {}
    end
    end_note = h_overhaul_notes.find_by(:note_type => 'overhaul_end')
    if end_note.present?
      @end_note = {id: end_note.id, note: end_note.note, img_path: end_note.img_path, sort_out: end_note.sort_out,
                   overhaul_desc: end_note.overhaul_desc, note_type: end_note.note_type,
                   overhaul_time: end_note.overhaul_time ? end_note.overhaul_time.strftime("%Y-%m-%d %H:%M:%S") : ''
      }
    else
      @end_note = {}
    end
  end
  #更换的备件信息
  spare = @handle_equip.h_equip_spare
  if spare.present?
    @spares = {id: spare.id, spare_name: spare.spare_name, change_time: spare.change_time ? spare.change_time.strftime("%Y-%m-%d %H:%M:%S") : ''}
  else
    @spares = {}
  end
  #试车检查项
  boot_s = @handle_equip.h_equip_boots
  boots = []
  if boot_s.present?
    boot_s.each{|b| boots << {id: b.id,handle_equip_id: b.handle_equip_id, boot_check_id: b.boot_check_id, check_project: b.check_project}}
    @equip_boots = {boots: boots,check_status: boot_s[0].check_status,exec_desc: boot_s[0].exec_desc }
  else
    @equip_boots = {boots: [],check_status: '',exec_desc: ''}
  end

  #维修记录头部信息
  @handle_equip = {id: @handle_equip.id, 
                   status: @handle_equip.status,
                   region_name: @handle_equip.region_name,   
                   equip_name: @handle_equip.equip_name,     #设备名称
                   handle_name: @handle_equip.handle_name,   #执行人
                   handle_start_time: @handle_equip.handle_start_time.strftime("%Y-%m-%d %H:%M"),  
                   handle_end_time: @handle_equip.handle_end_time.strftime("%Y-%m-%d %H:%M"),  
                   snag_desc: @handle_equip.snag_desc,    #维修原因
                   desc: @handle_equip.desc,              #维修说明
                   img_path: @handle_equip.img_path              #执行状态图片url
                  }

  @form_data={}
  @form_data[:handle_equip] = @handle_equip    #维修记录头部信息
  @form_data[:start_note] = @start_note        #检修前记录
  @form_data[:end_note] = @end_note            #检修后记录
  @form_data[:spares] = @spares                #更换备件信息
  @form_data[:equip_boots] = @equip_boots      #试车检查项信息
  @form_data[:boots] = @boots                  #所有检查项信息

  return @form_data

end





end