class HandleEquipsV1API < Grape::API

  #设备检修管理

  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
      get_region
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end

    def get_region
      #根据登录人员 获取人员所在的车间 或公司
      # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
      # @region_code = SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
      @region_code = SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    end

    #上传图片
    def upload_img(image_file, equip)
      if image_file.present?
        img_flag = "jpeg" if image_file[0, 15] == "data:image/jpeg"
        img_flag = "png" if image_file[0, 14] == "data:image/png"
        img_file = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
        img_name = "#{equip.id}.#{img_flag}"
        file_path = "#{Rails.root}/public/test/equip/"
        FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
        #向dir目录写入文件
        File.open(Rails.root.join("#{file_path}", img_name), 'wb') {|f| f.write(img_file)}
        return "test/equip/#{img_name}"
      end
    end

     #对上传的图片和记录进行删除
    def delete_img(arg)
      if arg.img_path.present?
        File::unlink("#{Rails.root}/public/#{arg.img_path}")
      end
    end


    #创建检修记录
    def create_overhaul(handle_equip, *arg)
      note = arg[0]
      note_type = arg[1]
      overhaul_desc = arg[2]
      overhaul_time = arg[3]
      sort_out = arg[4] ? arg[4] : ''
      image_file = arg[5]
      h_overhaul = handle_equip.h_overhaul_notes.find_by(:note_type => note_type)
      #判断是否存在检修记录
      if h_overhaul.blank?
        note1 = HOverhaulNote.create(:handle_equip_id => handle_equip.id, :note => note,
                                    :note_type => note_type, :overhaul_desc => overhaul_desc,
                                    :overhaul_time => overhaul_time, :sort_out => sort_out)
        #获取到上传的图片路径
        img_path = upload_img(image_file, note1)
        note1.img_path = img_path
        note1.save
      else
        #先删除原来的图片
        delete_img(h_overhaul)
        #获取到上传的图片路径
        img_path = upload_img(image_file, h_overhaul)
        h_overhaul.update(:note => note, :overhaul_desc => overhaul_desc,
                          :overhaul_time => overhaul_time, :sort_out => sort_out,
                          :img_path => img_path)
      end
    end

  end


  desc "设备检修管理首页", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
     optional :equip_ids, type: String, desc: "查询的设备id"
     optional :status, type: String, desc: "状态查询 tamp_save 待维修；boot_save  待试车"
     optional :begin_time, type: String, desc: "检修开始时间 2018-06-04 "
     optional :end_time, type: String, desc: "检修结束时间 2018-06-12 "
  end
  get "/" do
    authenticate!

    if @region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      region_z = SRegionCode.where(:s_region_level_id => 3)
      region_code_ids = region_z.pluck(:id)  #查询所有车间
    end

    if @region_code.s_region_level_id == 2  #2 分公司   1 总公司   3 车间
      region_f = SRegionCode.where(:father_region_id => @region_code.id, :s_region_level_id => 3)
      region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
    end

    if @region_code.s_region_level_id == 3  #2 分公司   1 总公司   3 车间
      region_code_ids = @region_code.id
    end

    Rails.logger.info "-----region_code_ids-------#{region_code_ids}----------"
    
    # @handle_equips = HandleEquip.where(:s_region_code_id => region_code_ids).by_equip(params[:equip_ids])
    #                           .by_create_time(params[:begin_time], params[:end_time])
    #                           .by_handle_status(params[:status])
    #                           .order(:created_at => 'desc')


    if @current_user.s_role_msg.role_code == 'mechanic_repair'
      @handle_equips = HandleEquip.where(:s_region_code_id => region_code_ids,:handle_name => @current_user.login_name).by_equip(params[:equip_ids])
                                .by_handle_time(params[:begin_time], params[:end_time])
                                .by_handle_status(params[:status])
                                .order(:created_at => 'desc')
    else
      @handle_equips = HandleEquip.where(:s_region_code_id => region_code_ids).by_equip(params[:equip_ids])
                                .by_handle_time(params[:begin_time], params[:end_time])
                                .by_handle_status(params[:status])
                                .order(:created_at => 'desc')
    end

    #按车间进行分组
    @handle_equips_hc = @handle_equips.select{|x| x.region_name == '合成车间'}
    @handle_equips_fj = @handle_equips.select{|x| x.region_name == '发酵车间'}
    @handle_equips_jz = @handle_equips.select{|x| x.region_name == '精制车间'}
    @handle_equips_hb = @handle_equips.select{|x| x.region_name == '烘包车间'}

    @form_data = []     #所有车间数据

    if @handle_equips_hc.present? && @handle_equips_hc.length > 0
      form_data_hc = {}   #合成车间数据
      form_data_hc[:name] = '合成车间'
      data=[]
      @handle_equips_hc.each do |e|
        item = {}
        item[:equip_id] = e.id             #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:status] = e.status_str       #状态
        item[:founder] = e.founder         #创建人
        item[:desc] = e.desc               #描述
        item[:handle_name] = e.handle_name.present? ? e.handle_name : '' #执行人
        item[:handle_start_time] = e.handle_start_time.strftime("%Y-%m-%d %H:%M")              
        item[:handle_end_time] = e.handle_end_time.strftime("%Y-%m-%d %H:%M")        
        data.push(item)
      end
      form_data_hc[:data] = data
      @form_data.push(form_data_hc)
    end

    if @handle_equips_fj.present? && @handle_equips_fj.length > 0
      form_data_fj = {}   #发酵车间数据
      form_data_fj[:name] = '发酵车间'
      data=[]
      @handle_equips_fj.each do |e|
        item = {}
        item[:equip_id] = e.id             #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:status] = e.status_str       #状态
        item[:founder] = e.founder         #创建人
        item[:desc] = e.desc               #描述
        item[:handle_name] = e.handle_name.present? ? e.handle_name : '' #执行人
        item[:handle_start_time] = e.handle_start_time.strftime("%Y-%m-%d  %H:%M")              
        item[:handle_end_time] = e.handle_end_time.strftime("%Y-%m-%d  %H:%M")        
        data.push(item)
      end
      form_data_fj[:data] = data
      @form_data.push(form_data_fj)
    end

    if @handle_equips_jz.present? && @handle_equips_jz.length > 0
      form_data_jz = {}   #精制车间数据
      form_data_jz[:name] = '精制车间'
      data=[]
      @handle_equips_jz.each do |e|
        item = {}
        item[:equip_id] = e.id             #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:status] = e.status_str       #状态
        item[:founder] = e.founder         #创建人
        item[:desc] = e.desc               #描述
        item[:handle_name] = e.handle_name.present? ? e.handle_name : '' #执行人
        item[:handle_start_time] = e.handle_start_time.strftime("%Y-%m-%d %H:%M")              
        item[:handle_end_time] = e.handle_end_time.strftime("%Y-%m-%d %H:%M")        
        data.push(item)
      end
      form_data_jz[:data] = data
      @form_data.push(form_data_jz)
    end

    if @handle_equips_hb.present? && @handle_equips_hb.length > 0
      form_data_hb = {}   #烘包车间数据
      form_data_hb[:name] = '烘包车间'
      data=[]
      @handle_equips_hb.each do |e|
        item = {}
        item[:equip_id] = e.id             #设备id
        item[:equip_name] = e.equip_name   #设备名称
        item[:status] = e.status_str       #状态
        item[:founder] = e.founder         #创建人
        item[:desc] = e.desc               #描述
        item[:handle_name] = e.handle_name.present? ? e.handle_name : '' #执行人
        item[:handle_start_time] = e.handle_start_time.strftime("%Y-%m-%d %H:%M")              
        item[:handle_end_time] = e.handle_end_time.strftime("%Y-%m-%d %H:%M")        
        data.push(item)
      end
      form_data_hb[:data] = data
      @form_data.push(form_data_hb)
    end

    return @form_data
  end


  desc "查询接口---获取所有维修状态", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
}
params do
  #  optional :id, type: Integer, desc: "维修记录id"
  #  optional :image_file, type: File,desc: '待执行图片'
end
get "/get_status" do
  authenticate!

  # @status = [{code:'wait_issued', name:'待下发'},{code:'wait_carried', name:'处理中'},{code:'carry_out', name:'完成'}]
  # Rails.logger.info "----@status-----#{@status}------"

  #  变量 使用 @ 时  会报错
  status = [
          {name:'待执行', code:'wait_carried'},
          {name:'待维修', code:'tamp_save'},
          {name:'待试车', code:'boot_save'},
          {name:'待审核', code:'wait_review'},
          {name:'完成', code:'carry_out'}
  ]     
  return  {status:status }
end

desc "设备维修管理，保存操作", {
    headers: {
        "Authentication-Token" => {
            description: "用户Token",
            required: true
        }
    },
    failure: [[401, 'Unauthorized']]
}
params do
   optional :id, type: Integer, desc: "维修记录id"
   optional :image_file, type: String,desc: '待执行图片'

   optional :start_note_type, type: String, desc: '检修类型----- 检修前记录参数 '
   optional :start_note, type: String, desc: '记录'
   optional :start_overhaul_desc, type: String,desc: '备注'
   optional :start_overhaul_time, type: String,desc: '检修时间 2018-06-14 10:20'
   optional :start_image_file, type: String,desc: '图片'
   optional :end_note_type, type: String, desc: '检修类型----- 检修后记录参数 '
   optional :end_note, type: String, desc: '记录'
   optional :end_overhaul_desc, type: String,desc: '备注'
   optional :end_sort_out, type: String,desc: '是否整理  是Y；否N'
   optional :end_overhaul_time, type: String,desc: '检修时间 2018-06-14 10:20'
   optional :end_image_file, type: String,desc: '图片'

   optional :spare_name, type: String,desc: '备件名称 --- 更换备件参数'
   optional :change_time, type: String,desc: '更换时间 2018-06-14 10:20'
   
   optional :check_status, type: String,desc: '结果 Y 合格 N 不合格'
   optional :exec_desc, type: String,desc: '不合格原因'
   optional :boot_ids, type: Array[String],desc: '试车记录参数，数组形式["1", "4", "7"] '
   optional :status, type: String,desc: '状态'
end
post "/carried_out" do
  authenticate!

  @handle_equip = HandleEquip.find(params[:id])

   #-------填写检修前记录----------
   if params[:start_note_type]
    note = params[:start_note].present? ? params[:start_note] : ''
    note_type = params[:start_note_type]
    overhaul_desc = params[:start_overhaul_desc] ? params[:start_overhaul_desc] : ''
    overhaul_time = params[:start_overhaul_time]
    image_file = params[:start_image_file]
    create_overhaul(@handle_equip, note, note_type, overhaul_desc, overhaul_time, '', image_file)
  end
  #--------填写检修后记录---------
  if params[:end_note_type]
    note = params[:end_note].present? ? params[:end_note] : ''
    note_type = params[:end_note_type]
    overhaul_desc = params[:end_overhaul_desc] ? params[:end_overhaul_desc] : ''
    overhaul_time = params[:end_overhaul_time]
    sort_out = params[:end_sort_out]
    image_file = params[:end_image_file]
    create_overhaul(@handle_equip, note, note_type, overhaul_desc, overhaul_time, sort_out, image_file)
  end
  #更换备件
  spare = @handle_equip.h_equip_spare
  if spare
    if params[:spare_name] && params[:change_time]
      spare.update(:spare_name => params[:spare_name], :change_time => params[:change_time])
    end
  else
    HEquipSpare.create(:handle_equip_id => @handle_equip.id, :spare_name => params[:spare_name],
                       :change_time => params[:change_time])
  end
  #试车记录
  boots = @handle_equip.h_equip_boots
  if boots
    if boots.destroy_all
      boot_ids = params[:boot_ids]

      Rails.logger.info "----boot_ids----#{boot_ids}-----"
      # Rails.logger.info "----boot_ids----#{boot_ids[0]}-----"
      # Rails.logger.info "----boot_ids----#{boot_ids[0].class}-----"
      if boot_ids.present? && boot_ids[0].present? 
        ids = eval(boot_ids[0])
      end

      if ids && ids.length >0
        ids.each do |b|
          Rails.logger.info "----boot_ids----#{b}-----"
          boot = BootCheck.find(b.to_i).check_project
          HEquipBoot.create(:handle_equip_id => @handle_equip.id, :boot_check_id => b.to_i,
                            :check_project => boot, :check_status => params[:check_status], :exec_desc => params[:exec_desc],
                            :handle_time => params[:handle_time])
        end
      end
    end
  end
  #改变设备检修单状态
  if params[:image_file]
    image_file = params[:image_file]
    img_flag = "jpeg" if image_file[0, 15] == "data:image/jpeg"
    img_flag = "png" if image_file[0, 14] == "data:image/png"
    img_file = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
    img_name = "#{@handle_equip.id}.#{img_flag}"
    file_path = "#{Rails.root}/public/test/handle/"
    FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
    #向dir目录写入文件
    File.open(Rails.root.join("#{file_path}", img_name), 'wb') {|f| f.write(img_file)}
    img_path = "test/handle/#{img_name}"
    @handle_equip.update(:img_path => img_path)
  end
  if params[:status]
    @handle_equip.update(:status => params[:status])
  end
  

  return {status: 200}

end





end