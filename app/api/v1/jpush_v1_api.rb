class JpushV1Api < Grape::API

  format :json

  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end
  end

  desc "保存用户的regionID",{
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    requires :region_id, type: String, desc: "region_id"
  end
  post "/" do
    authenticate!
    uuid = params[:region_id]
    @current_user.uuid = uuid
    if @current_user.save
      return  {msg: 'success', status: "201"}
    else
      return  {msg: 'false', status: "302"}
    end
  end

end