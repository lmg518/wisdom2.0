class EnergyReportShowV1API < Grape::API
  include EnergyFormHelper

  $flag = false    #差值计算和不差值计算的 标识  true 差值计算   false 不差值计算

  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
      get_region
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end

    def get_region
      #根据登录人员 获取人员所在的车间 或公司
      # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
      # @region_code = SRegionCode.find_by(:id => region_code_info.s_region_code_id) #获取人员所在车间 或公司
      @region_code = SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    end

    #差值计算方法 3  计算单个项目的  能源使用的
    def js_method_energy(time, d_report_form_id, item)
      @steam_values = 0
      @steam_month_sum = 0

      #计算差值
      bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

      @form_values = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)
      @form_values_bf = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)

      @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:field_code => item, :d_report_form_id => d_report_form_id)
      min_time = @form_values_month.minimum("datetime") #获取最小日期
      max_time = @form_values_month.maximum("datetime") #获取最大日期

      @form_values_min = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)
      @form_values_max = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)

      if @form_values.present? && @form_values.field_value.present?
        d1 = @form_values.field_value.to_f
      else
        d1 = 0
      end

      if @form_values_bf.present? && @form_values_bf.field_value.present?
        d2 = @form_values_bf.field_value.to_f
      else
        d2 = 0
      end

       #添加判断
      if @form_values_min.present? && @form_values_min.field_value.present?
        min = @form_values_min.field_value.to_f
      else
        min = 0
      end
      if @form_values_max.present? && @form_values_max.field_value.present?
        max = @form_values_max.field_value.to_f
      else
        max = 0
      end
      @steam_month_sum = max - min

      @steam_values = d1 - d2 #昨天的 - 前天的
      #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f #月累计用量
    end


    def get_pullution_form(time, form_code, region_code = 'Biological_once_water_from')
      # # 一次性水排污部门两个报表
      @form_code = form_code.present? ? form_code : 'Biological_once_water_from'
      @form_name = "一次性水排污报表"
      if 2 == region_code.s_region_level_id
        chil_region = SRegionCode.find_by(:region_code => 'TYSYFGS08', :father_region_id => region_code.id)
        @d_report_forms = DReportForm.where(:s_region_code_id => chil_region.id)
        @form_list = []
        @d_report_forms.map {|x| @form_list << {tag_name: x.name, tag_code: x.code}}
        @energy_hash[:tag_list] = @form_list
        @d_report_forms = DReportForm.find_by(:code => @form_code, :s_region_code_id => chil_region.id)
      else
        @d_report_forms = DReportForm.where(:s_region_code_id => region_code.id)
        @form_list = []
        @d_report_forms.map {|x| @form_list << {tag_name: x.name, tag_code: x.code}}
        @energy_hash[:tag_list] = @form_list
        @d_report_forms = DReportForm.find_by(:code => @form_code, :s_region_code_id => region_code.id)
      end

      if 'Biological_pollution_from' == form_code # 排污
        if $flag   #差值计算

        #低浓度水计算
        steam_values = 0 #日的
        steam_month_sum = 0 #月的
        #计算差值
        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
        bf_time = (Time.parse(time) - 1.day).strftime("%Y-%m-%d") #前天的日期
        @form_values = SNenrgyValue.where(:d_report_form_id => @d_report_forms.id, :datetime => time) #   5 排污报表
        @form_values_bf = SNenrgyValue.where(:d_report_form_id => @d_report_forms.id, :datetime => bf_time)
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => @d_report_forms.id)
        min_time = @form_values_month.minimum("datetime") #获取最小日期
        max_time = @form_values_month.maximum("datetime") #获取最大日期
        @form_values_min = SNenrgyValue.where(:d_report_form_id => @d_report_forms.id, :datetime => min_time)
        @form_values_max = SNenrgyValue.where(:d_report_form_id => @d_report_forms.id, :datetime => max_time)
        d1 = 0
        d2 = 0
        @form_values.each do |m|
          if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
            next
          end
          d1 += m.field_value.to_f
        end
        @form_values_bf.each do |m|
          if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
            next
          end
          d2 += m.field_value.to_f
        end

        steam_values = (d1 - d2).round(2) #日用量

        dd1 = 0
        dd2 = 0
        @form_values_max.each do |m| ##去除二个  pollution_6甲醇废水    pollution_7洗效水
          if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
            next
          end
          dd1 += m.field_value.to_f
        end
        @form_values_min.each do |m|
          if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
            next
          end
          dd2 += m.field_value.to_f
        end

        steam_month_sum = (dd1 - dd2).round(2) #累计用量

        @low_water = steam_values.round(2)
        @low_month_sum = steam_month_sum.round(2)

      else
        #-------不差值计算-----------
        @low_water = 0   #低浓度水日用量
        @low_water_values = SNenrgyValue.where(:d_report_form_id => '5', :datetime => time)  #要去除二个  甲醇废水 洗效水
        if @low_water_values.present? && @low_water_values.length > 0
          @low_water_values.each do |low|
            if low.field_code == 'pollution_6' || low.field_code == 'pollution_7' then
              next
            end
            @low_water += low.field_value.to_f
          end
        end
        #计算低浓度 月累计
        @low_month_sum = 0
        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        @low_month_values = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => @d_report_forms.id)
        if @low_month_values.present? && @low_month_values.length > 0
          @low_month_values.each do |m|
            if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then # pollution_6 ==甲醇废水 pollution_7=洗效水
              next
            end
            @low_month_sum += m.field_value.to_f
          end
        end
      end


       #---------v2--------   高浓度水计算
      if $flag
        js_method_energy(time, 5, 'pollution_6')  #d_report_form_id  5  item  高浓度水
        #返回  @steam_values 日的  @steam_month_sum 累计的
      else
        #不差值计算
        @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_item_form_id_time('pollution_6', 5, time)
      end

      #   高浓度
      #js_method_energy(time, @d_report_forms.id, 'pollution_6')
      @hei_water = @steam_values
      @hei_month_sum = @steam_month_sum

      #日单耗
      @low_water_piece = (@low_water / @count_pro_d).round(2)
      @hei_water_piece = (@hei_water / @count_pro_d).round(2)
      #月单耗
      @mon_low_water_piece = (@low_month_sum / @count_pro_m).round(2)
      @mon_hig_water_piece = (@hei_month_sum / @count_pro_m).round(2)


      end
    end

    #计算 公司 总产量
    def get_company_finished(before_time, region_code_info)
      time = before_time
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @company_day_total = 1 #日用量      计算日单耗时  除数不能为0
      @company_month_total = 1 #累计用量
      if "TYSYFGS03" == region_code_info.region_code # TYSYFGS03 烘包车间的code
        region_info = region_code_info.id
      else
        region_level = SRegionLevel.find_by(:id => region_code_info.s_region_level_id)
        # if '总公司' == region_level.level_name
        #   f_info = SRegionCode.find_by(:father_region_id => region_code_info.id, :s_region_level_id => SRegionLevel.find_by(:level_name => "分公司").id)
        #   region_info = SRegionCode.find_by(:father_region_id => f_info.id, :region_code => "TYSYFGS03")
        # end
        if '分公司' == region_level.level_name
          region_info = SRegionCode.find_by(:father_region_id => region_code_info.id, :region_code => "TYSYFGS03")
        end
        if ['车间', "能源"].include? region_level.level_name
          region_info = SRegionCode.find_by(:father_region_id => region_code_info.father_region_id, :region_code => "TYSYFGS03")
        end
      end
      @na_form_values = DDailyReport.find_by(:s_region_code_id => region_info.id, :code => 'na_input', :datetime => time) # 8 烘包车间  na_input 钠入库
      @sour_form_values = DDailyReport.find_by(:s_region_code_id => region_info.id, :code => 'sour_input', :datetime => time) # 8 烘包车间  sour_input 酸入库

      @na_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => region_info.id, :code => 'na_input')
      @sour_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => region_info.id, :code => 'sour_input')

      if @na_form_values.present? && @sour_form_values.present?
        @company_day_total = (@na_form_values.nums.to_f + @sour_form_values.nums.to_f / 0.73).round(2)
      end
      d1 = 0
      d2 = 0
      @na_form_values_month.each do |m|
        d1 += m.nums.to_f
      end
      @sour_form_values_month.each do |m|
        d2 += m.nums.to_f
      end
      @company_month_total = (d1 + d2 / 0.73).round(2) ##累计用量
    end

    # 计算项目的日用量和月累积
    def js_method_one(time, s_region_code_id, d_report_form_id, item)
      @steam_values = 0
      @steam_month_sum = 0

      #计算差值
      bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

      @form_values = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)
      @form_values_bf = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)

      @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id)
      min_time = @form_values_month.minimum("datetime") #获取最小日期
      max_time = @form_values_month.maximum("datetime") #获取最大日期

      @form_values_min = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)
      @form_values_max = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)

      if @form_values.present? && @form_values.field_value.present?
        d1 = @form_values.field_value.to_f
      else
        d1 = 0
      end

      if @form_values_bf.present? && @form_values_bf.field_value.present?
        d2 = @form_values_bf.field_value.to_f
      else
        d2 = 0
      end

      #添加判断
      if @form_values_min.present? && @form_values_min.field_value.present?
        min = @form_values_min.field_value.to_f
      else
        min = 0
      end
      if @form_values_max.present? && @form_values_max.field_value.present?
        max = @form_values_max.field_value.to_f
      else
        max = 0
      end
      @steam_month_sum = max - min

      @steam_values = d1 - d2 #昨天的 - 前天的
      #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f #月累计用量
    end


  end

  desc "能源-用电-循环水-一次水-排污>报表接口", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    requires :r_time_type, type: String, desc: "请求时间类型，年 Y 月 M 日 D ;默认传 D "
    optional :r_time, type: String, desc: "请求时间，非必填，格式< 2018-05-08 >"
    optional :form_code, type: String, desc: "表单code"
  end
  get "/power" do
    authenticate!
    @energy_hash = {}
    Rails.logger.info params[:r_time]
    # 时间
    if params[:r_time].present?
      time = Time.parse(params[:r_time])
    else
      #time = Time.now - 1.days
      time = Time.now            #默认查询当天的日期
    end

    # 时间的运算
    @today_time = time.strftime("%Y-%m-%d")
    @begin_time = (time - 1.days).strftime("%Y-%m-%d")
    @r_time = params[:r_time]
    @month_begin = time.beginning_of_month.strftime("%Y-%m-%d")
    @month_end = time.end_of_month.strftime("%Y-%m-%d")

    #获取报表
    #  #计算成品入了量，需要从 "烘包车间日报里获取"
    # @company_day_total = 1 #日用量      计算日单耗时  除数不能为0
    # @company_month_total = 1 #累计用量
    get_company_finished(@today_time, @region_code)
    @count_pro_d = @company_day_total #日的
    @count_pro_m = @company_month_total #月累计的
    #  判断当前用户的级别是不是分公司，如果是分公司的级别走以下逻辑
    if 2 == @region_code.s_region_level_id
      if ['Biological_once_water_from', 'Biological_pollution_from'].include? params[:form_code]
        get_pullution_form(@today_time, params[:form_code], @region_code)
      else
        @d_report_forms = DReportForm.find_by(:code => params[:form_code])
        @form_name = @d_report_forms.present? ? @d_report_forms.name : '' #表名
      end
    else
      if 'TYSYFGS08' == @region_code.region_code
        get_pullution_form(@today_time, params[:form_code], @region_code)
      else
        @d_report_forms = DReportForm.find_by(:s_region_code_id => @region_code.id)
        @form_name = @d_report_forms.present? ? @d_report_forms.name : '' #表名
      end
    end

    report_form_id = @d_report_forms.id #报表id
    if @d_report_forms.s_nenrgy_form_type_id.present?
      type_ids = @d_report_forms.s_nenrgy_form_type_id.split(",")
      @form_type = SNenrgyFormType.where(:id => type_ids) #报表内容第一列内容类型
      @type_name = @form_type[0].name # #报表内容第一列内容类型名称
    end

    #获取数据
    @region_datas = []

    if 'Biological_pollution_from' == params[:form_code] # 排污
      #  车间和排污项目关联表
      @region_code_ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_region_code_id).uniq #去重
      @region_code_ids.each do |id|
        item = {}
        region_code = SRegionCode.find_by(:id => id)
        item[:region_name] = region_code.region_name #车间名称
        item[:region_id] = region_code.id #车间id
        #根据车间 取值
        @form_values = SNenrgyValue.where(:s_region_code_id => region_code.id, :d_report_form_id => report_form_id, :datetime => @today_time)
        # 报表物料
        @steam_code = DSteamRegionCode.where(:s_region_code_id => id, :s_nenrgy_form_type_id => report_form_id)
        s_material_ids = @steam_code.pluck(:s_material_id) #一个车间的项目id
        plants1 = SMaterial.where(:id => s_material_ids) # =>
        plants_datas = [] #存车间下的项目信息
        plants1.each do |p|
          item1 = {}
          item1[:name] = p.name
          if $flag
            js_method_one(@today_time, region_code.id, 5, p.code)
          else
            #不差值计算
            @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, region_code.id, 5, p.code)
          end






          item1[:field_value] = @steam_values
          item1[:month_value] = @steam_month_sum #月累计
          #日单耗 月单耗
          daily_piece = (@steam_values.to_f / @count_pro_d.to_f).round(2)
          month_piece = (@steam_month_sum.to_f / @count_pro_m.to_f).round(2)
          item1[:daily_piece] = daily_piece #日单耗
          item1[:month_piece] = month_piece #月单耗
          plants_datas.push(item1)
        end
        item[:data] = plants_datas
        @region_datas.push(item)
      end
      @energy_hash[:form_name] = @form_name
      @energy_hash[:d_time] = time.strftime("%Y-%m-%d")
      @energy_hash[:w_body] = @region_datas
      # 生物排污 公司汇总
      @energy_hash[:company_info] = {count_pro: {daily_pro: @count_pro_d, month_pro: @count_pro_m},
                                     l_water: {l_day: @low_water, l_month: @low_month_sum, day_expend: @low_water_piece, month_expend: @mon_low_water_piece},
                                     h_water: {h_day: @hei_water, h_month: @hei_month_sum, day_expend: @hei_water_piece, month_expend: @mon_hig_water_piece},
      }

    else # 用电 循环水 一次水  日报表计算
      # 取值  分为 历史数据取指定时间的数据，最新数据默认取前一天的数据
      # 月数据根据月最后一天减去月第一天的数据或每个第一个数据和最后一个数据的差
      # @now_values = SNenrgyValue.where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :datetime => @today_time) # 前一天数据或指定日数据
      # @begin_values = SNenrgyValue.where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :datetime => @begin_time) # 前两天数据
      # @month_vals = SNenrgyValue.where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :datetime => (@month_begin)..(@today_time)) # 指定月的所有数据

      #获取 用电报表公司的物料
      @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
      @plants = SMaterial.where(:id => @ids) #从材料表中获取
      @day_count, @month_count = 0, 0
      @plants.each do |s|
        item = {}
        item[:name] = s.name

        if $flag
        # 计算用电量
        # 取值  分为 历史数据取指定时间的数据，最新数据默认取前一天的数据
        # 月数据根据月最后一天减去月第一天的数据或每个第一个数据和最后一个数据的差
        @now_val = SNenrgyValue.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :field_code => s.code, :datetime => @today_time) # 前一天数据或指定日数据
        @befor_val = SNenrgyValue.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :field_code => s.code, :datetime => @begin_time) # 前两天数据
        @month_vals = SNenrgyValue.where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :field_code => s.code, :datetime => (@month_begin)..(@today_time)) # 指定月的所有数据

        if params[:r_time].present?
          @m_max = time.strftime("%Y-%m-%d")
          @m_min = @month_vals.minimum("datetime")
        else
          @m_max = @month_vals.maximum("datetime")
          @m_min = @month_vals.minimum("datetime")
        end
        @m_max_val = @month_vals.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :field_code => s.code, :datetime => @m_max) # 月的最大值
        @m_min_val = @month_vals.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :field_code => s.code, :datetime => @m_min) # 月的最小值
        field_value = @now_val.present? ? @now_val.field_value.present? ? @now_val.field_value : 0 : 0
        befor_value = @befor_val.present? ? @befor_val.field_value.present? ? @befor_val.field_value : 0 : 0
        month_max_val = @m_max_val.present? ? @m_max_val.field_value.present? ? @m_max_val.field_value : 0 : 0
        month_min_val = @m_min_val.present? ? @m_min_val.field_value.present? ? @m_min_val.field_value : 0 : 0
         # 日用电量 # 月累积
         field_value = field_value.to_i - befor_value.to_i
         month_value = month_max_val.to_i - month_min_val.to_i
      else
        #不差值计算
        @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, @d_report_forms.s_region_code_id, report_form_id, s.code)
        field_value = @steam_values
        month_value = @steam_month_sum
      end

        item[:field_value] = field_value # 日
        item[:month_value] = month_value # 月累积
        # 生物循环水报表有总结：日 月
        if 'Biological_water_from' == @d_report_forms.code || 'Biological_once_water_from' == @d_report_forms.code
          @day_count += field_value # 日总计
          @month_count += month_value # 月总计
        end
        # 用电的日单耗 月单耗
        if 'Biological_electricity_form' == @d_report_forms.code
          daily_piece = (field_value.to_f / @count_pro_d.to_f).round(2) #保留二位精度
          daily_piece = 0 if @count_pro_d == 0
          item[:daily_piece] = daily_piece #日单耗
          month_piece = (month_value.to_f / @count_pro_m.to_f).round(2) #保留二位精度
          month_piece = 0 if @count_pro_m == 0
          item[:month_piece] = month_piece #月单耗
        end
        @region_datas.push(item)
      end
      # 报表公用部分
      @energy_hash[:form_name] = @form_name # 报表名
      @energy_hash[:d_time] = @today_time # 报表时间
      if 2 == @region_code.s_region_level_id
        if 'company_sum_form' == @d_report_forms.code
          @energy_hash[:title] = @region_code.region_name
        else
          region = SRegionCode.find_by(:id => @d_report_forms.s_region_code_id)
          @energy_hash[:title] = region.region_name
        end
      else
        @energy_hash[:title] = @region_code.region_name
      end
      @day_expend, @month_expend = 0.00, 0.00
      if 'Biological_water_from' == @d_report_forms.code
        @day_expend = (@day_count.to_f / @count_pro_d.to_f).round(2)
        @month_expend = (@month_count.to_f / @count_pro_m.to_f).round(2)
      end
      @energy_hash[:w_body] = @region_datas # 中间物料内容部分
      @energy_hash[:total_info] = {day_totle: @day_count, month_totle: @month_count, day_expend: @day_expend, month_expend: @month_expend}
      @energy_hash[:count_pro] = {daily_pro: @count_pro_d, month_pro: @count_pro_m}
    end

    return @energy_hash

  end

  desc "能源蒸汽报表接口", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    requires :r_time_type, type: String, desc: "请求时间类型，年 Y 月 M 日 D ;默认传 D "
    optional :r_time, type: String, desc: "请求时间，非必填，格式< 2018-05-08 >"
    requires :form_code, type: String, desc: "表单code"
  end
  get "/steam" do
    authenticate!
    @energy_hash = {}
    if params[:r_time].present?
      time = Time.parse(params[:r_time])
    else
      #time = Time.now - 1.days
      time = Time.now    #默认查询当天的数据
    end
    @today_time = time.strftime("%Y-%m-%d")
    @begin_time = (time - 1.days).strftime("%Y-%m-%d")

    @r_time = params[:r_time]
    month_begin = time.beginning_of_month.strftime("%Y-%m-%d")
    @month_end = time.end_of_month.strftime("%Y-%m-%d")

    if params[:form_code].present?
      if 2 == @region_code.s_region_level_id
        region_child = SRegionCode.find_by(:region_code => 'TYSYFGS09', :father_region_id => @region_code.id)
        @d_report_forms = DReportForm.find_by(:code => params[:form_code], :s_region_code_id => region_child.id)
      else
        @d_report_forms = DReportForm.find_by(:code => params[:form_code], :s_region_code_id => @region_code.id)
        @form_name = @d_report_forms.present? ? @d_report_forms.name : '' #表名
      end
    end
    report_form_id = @d_report_forms.id #报表id
    get_company_finished(@today_time, @region_code)
    @count_pro_d = @company_day_total #日的
    @count_pro_m = @company_month_total #月累计的
    # 获取合成车间日报表的日产量粗品的日产量和月累计值
    #蒸汽报表合成车间 合计计算 日单耗 = 日蒸汽/ 合成车间日报表的日产量粗品     月单耗 = 月蒸汽/ 合成车间日报表的月产量粗品
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => 5, :datetime => @today_time, :code => 'CP') #合成车间日报表的日产量粗品
    @daily_report_cp_month = DDailyReport.by_datetime(month_begin, @today_time).where(:s_region_code_id => 5, :code => 'CP') #合成车间日报表的月产量粗品
    @cp_day_nums = @daily_report_cp.present? ? @daily_report_cp.nums : 1 #合成车间日报表粗品的日产量
    @cp_month_nums = 0 #合成车间日报表粗品月累计
    if @daily_report_cp_month.present? && @daily_report_cp_month.length > 0
      @daily_report_cp_month.each do |d1|
        @cp_month_nums += d1.nums.to_f
      end
    else
      @cp_month_nums = 1 #数据不存在是 默认值为 1  除数不能为0
    end
    #获取精制车间日产量=钠包装量+酸包装量*0.73
    #蒸汽报表-精制车间 合计计算 日单耗 = 日蒸汽/ 精制车间日产量     月单耗 = 月蒸汽/ 精制车间月产量
    @daily_report_na = DDailyReport.find_by(:s_region_code_id => 8, :datetime => @today_time, :code => 'na_nums') # na_nums  在 s_region_code_id = 8
    @daily_report_na_month = DDailyReport.by_datetime(month_begin, @today_time).where(:s_region_code_id => 8, :code => 'na_nums')
    @daily_report_suan = DDailyReport.find_by(:s_region_code_id => 8, :datetime => @today_time, :code => 'suan_nums')
    @daily_report_suan_month = DDailyReport.by_datetime(month_begin, @today_time).where(:s_region_code_id => 8, :code => 'suan_nums')
    na_day_nums = @daily_report_na.present? ? @daily_report_na.nums : 1 #钠日产量
    suan_day_nums = @daily_report_suan.present? ? @daily_report_suan.nums : 1 #酸日产量
    @nums_day_jz = na_day_nums + suan_day_nums * 0.73 #精制车间日产量
    dd1 = 0
    dd2 = 0
    @daily_report_na_month.each do |d1|
      dd1 += d1.nums.to_f
    end
    @daily_report_suan_month.each do |d1|
      dd2 += d1.nums.to_f
    end
    @nums_month_jz = dd1 + dd2 * 0.73 #精制车间 月累计产量

    # 封装数据
    #表格头信息
    @form_name = @d_report_forms.name #表格名称
    type_ids = @d_report_forms.s_nenrgy_form_type_id.split(",") #报表类型id
    @region_datas = [] #存类型  蒸汽 甲醇母液
    if type_ids.length > 0
      type_ids.each do |id|
        item1 = {} #存类型
        nenrgy_type = SNenrgyFormType.find_by(:id => id)
        item1[:type_name] = nenrgy_type.name #类型
        region_ids = steam_region_codes = DSteamRegionCode.where(:s_nenrgy_form_type_id => id).pluck(:s_region_code_id).uniq # 要去重（车间）   蒸汽项目与车间 类型的关联
        @s2 = [] #存车间
        @unit_day_sum, @unit_month_sum = 0, 0
        region_ids.each do |region_id|
          item2 = {} #存车间
          region_code = SRegionCode.find_by(:id => region_id)
          item2[:region_name] = region_code.region_name
          item2[:region_id] = region_code.id #车间id
          @form_values = SNenrgyValue.where(:s_region_code_id => region_code.id, :d_report_form_id => report_form_id, :datetime => @today_time)
          @ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => id, :s_region_code_id => region_id).pluck(:s_material_id)

          #找车间下的项目
          s_materials = SMaterial.where(:id => @ids)
          @s3 = [] #存车间下的 岗位
          @g_w_arr = []
          @item_day_sum, @item_month_sum = 0, 0
          s_materials.each do |m| #车间下的项目
            @ht_val = 0.00
            item3 = {}
            item3[:name] = m.name
            item3[:field_code] = m.code

            if $flag
              js_method_one(@today_time, region_code.id, report_form_id, m.code)
            else
              #不差值计算
              @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, region_code.id, report_form_id, m.code)
            end

            @ht_val = @steam_values if 'steam_8' == m.code # 核糖的日用量
            item3[:field_value] = @steam_values.round(2)    #日用量
            item3[:month_sum] = @steam_month_sum.round(2)   #月累计
            @item_day_sum += @steam_values.to_f
            @item_month_sum += @steam_month_sum.to_f
            # 判断车间显示附加信息
            if 'fermentation' == m.code # 发酵车间
              #发酵车间蒸汽  总收率  数据
              @daily_report_shy = DDailyReport.find_by(:s_region_code_id => region_code.id, :datetime => @today_time, :code => 'SHY')
              if $flag
                js_method_one(@today_time, region_code.id, 3, 'fermentation')
                #返回  @steam_values 日的  @steam_month_sum 累计的
              else
                #不差值计算
                @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, region_code.id, 3, 'fermentation')
              end

              @day_steam_fj = @steam_values
              @steam_month_sum_fj = @steam_month_sum

              #发酵车间计算 日单耗 月单耗
              @shy_nums = @daily_report_shy.present? ? @daily_report_shy.nums : 1 #酸化液的 日产量
              @shy_month_nums = 1 #酸化液的 月产量
              @shy_months = DDailyReport.by_datetime(month_begin, @today_time).where(:s_region_code_id => region_code.id, :code => 'SHY') #酸化液的 月累计
              @shy_months.each do |s|
                @shy_month_nums += s.nums.to_f
                @shy_month_nums = @shy_month_nums - 1
              end
              @day_expend_fj = (@day_steam_fj.to_f / @shy_nums.to_f).round(2) #发酵车间日单耗
              @month_expend_fj = (@steam_month_sum_fj.to_f / @shy_month_nums.to_f).round(2) #发酵车间月单耗

              @g_w_arr << {name: m.name, day_expend: @day_expend_fj, month_expend: @month_expend_fj, day_totle: @shy_nums, month_totle: @shy_month_nums.round(2)}
            end

            if ['steam_4', "steam_7", 'steam_8', 'steam_9'].include? m.code # 甲醇塔 烘包  核糖 稀甲醇
              if 'steam_4' == m.code #甲醇塔 日单耗 = 甲醇塔 steam_4 / 蒸精甲醇 steam_10
                if $flag
                  js_method_one(@today_time, region_code.id, 3, 'steam_4')
                  #返回  @steam_values 日的  @steam_month_sum 累计的
                  d1 = @steam_values
                  d2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, region_code.id, 3, 'steam_4')
                  d1 = @steam_values
                  d2 = @steam_month_sum
                end

                if $flag
                  js_method_one(@today_time, region_code.id, 3, 'steam_10')
                  dd1 = @steam_values
                  dd2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, region_code.id, 3, 'steam_4')
                  d1 = @steam_values
                  d2 = @steam_month_sum
                end

                @day_expend_item = (d1.to_f / dd1.to_f).round(2)
                @month_expend_item = (d2.to_f / dd2.to_f).round(2)
              end
              if 'steam_7' == m.code #烘包 日单耗 = 烘包 steam_7 / 公司合计

                if $flag
                  js_method_one(@today_time, region_code.id, 3, 'steam_7')
                  #返回  @steam_values 日的  @steam_month_sum 累计的
                  s1 = @steam_values
                  s2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, region_code.id, 3, 'steam_4')
                  s1 = @steam_values
                  s2 = @steam_month_sum
                end



                #计算公司合计
              #   #-------------
              #   steam_values = 0
              #   steam_month_sum = 0
              #   if $flag
              #   #计算差值
              #   bf_time = (Time.parse(@today_time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
              #   @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => @today_time) #   3 蒸汽报表
              #   @form_values_bf = SNenrgyValue.where(:d_report_form_id => 3, :datetime => bf_time) #   3 蒸汽报表
              #   @form_values_month = SNenrgyValue.by_datetime(month_begin, @today_time).where(:d_report_form_id => 3) # 3 蒸汽报表
              #   min_time = @form_values_month.minimum("datetime") #获取最小日期
              #   max_time = @form_values_month.maximum("datetime") #获取最大日期
              #   @form_values_min = SNenrgyValue.where(:d_report_form_id => 3, :datetime => min_time)
              #   @form_values_max = SNenrgyValue.where(:d_report_form_id => 3, :datetime => max_time)
              #   d1 = 0
              #   d2 = 0
              #   @form_values.each do |m|
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     d1 += m.field_value.to_f
              #   end
              #   @form_values_bf.each do |m|
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     d2 += m.field_value.to_f
              #   end
              #   steam_values = (d1 - d2).round(2) #日用量
              #   dd1 = 0
              #   dd2 = 0
              #   @form_values_max.each do |m| #不包括  steam_9稀甲醇  steam_10蒸精甲醇  steam_11浓缩母液
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     dd1 += m.field_value.to_f
              #   end
              #   @form_values_min.each do |m|
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     dd2 += m.field_value.to_f
              #   end
              #   steam_month_sum = (dd1 - dd2).round(2) #累计用量
              # else
              #   #不差值计算
              #   @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => time)  #   3 蒸汽报表
              #   @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 3)     # 3 蒸汽报表
               
              #   #日用量计算
              #   if @form_values.present? && @form_values.length > 0
              #     @form_values.each do |m|
              #       if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #         next
              #       end
              #       steam_values += m.field_value.to_f   #日用量
              #     end
              #   else
              #       steam_values = 1  #数据不存在时  除数默认为1
              #   end
              #   #月累计计算
              #   if @form_values_month.present? && @form_values_month.length > 0
              #     @form_values_month.each do |m|
              #       if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #         next
              #       end
              #       steam_month_sum += m.field_value.to_f   #日用量
              #     end
              #   else
              #       steam_month_sum = 1  #数据不存在时  除数默认为1
              #   end
              # end

              @day_expend_item = (s1.to_f / @company_day_total).round(2) #日单耗
              @month_expend_item = (s2.to_f / @company_month_total).round(2) #月单耗

              # item3[:day_expend] = (s1.to_f /  @company_day_total).round(2)       #日单耗
              # item3[:month_expend] = (s2.to_f / @company_month_total).round(2)    #月单耗
            end
              if 'steam_9' == m.code #稀甲醇 日单耗 = 稀甲醇 steam_9 / 合成车间 合计
                if $flag
                  js_method_one(@today_time, region_code.id, 3, 'steam_9')
                  #返回  @steam_values 日的  @steam_month_sum 累计的
                  d1 = @steam_values
                  d2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(@today_time, region_code.id, 3, 'steam_4')
                  d1 = @steam_values
                  d2 = @steam_month_sum
                end

                @day_expend_item = (d1.to_f / @day_total_hc.to_f).round(2) #日单耗
                @month_expend_item = (d2.to_f / @month_total_hc.to_f).round(2) #月单耗
                @day_expend_item = 0 if @day_total_hc == 0
                @month_expend_item = 0 if @month_total_hc == 0
              end
              @g_w_arr << {name: m.name, day_expend: @day_expend_item, month_expend: @month_expend_item, day_totle: '', month_totle: ''}
            end

            @s3.push(item3)
          end
          if ['TYSYFGS02', 'TYSYFGS05'].include? region_code.region_code ##蒸汽 合成车间  精制车间
            if 'TYSYFGS02' == region_code.region_code
              @cp_day_expend = (@item_day_sum.to_f / @cp_day_nums).round(2) #合成车间日单耗
              @cp_day_expend = 0 if @cp_day_nums == 0
              @cp_mon_expend = (@item_month_sum / @cp_month_nums).round(2) #合成车间月单耗
              @cp_mon_expend = 0 if @cp_month_nums == 0
              @cp_day_totle = @cp_day_nums #等于 粗品 日产量
              @cp_mon_totle = @cp_month_nums #等于 粗品 月累计
            end
            if 'TYSYFGS05' == region_code.region_code
              @cp_day_expend = (@item_day_sum.to_f / @nums_day_jz).round(2) #精制车间日单耗
              @cp_day_expend = 0 if @nums_day_jz == 0
              @cp_mon_expend = (@item_month_sum / @nums_month_jz).round(2) #精制车间月单耗
              @cp_mon_expend = 0 if @nums_month_jz == 0
              @cp_day_totle = @nums_day_jz #等于 粗品 日产量
              @cp_mon_totle = @nums_month_jz #等于 粗品 月累计
            end
            @g_w_arr << {name: "合计", day_expend: @cp_day_expend, month_expend: @cp_mon_expend, day_totle: @cp_day_totle.round(2), month_totle: @cp_mon_totle.round(2)}
            @s3.push({name: "合计",
                      "field_code": "totle",
                      "field_value": @item_day_sum.round(2),
                      "month_sum": @item_month_sum.round(2),
                     })
          end

          @unit_day_sum += @item_day_sum
          @unit_month_sum += @item_month_sum

          item2[:m_data] = @g_w_arr
          item2[:data] = @s3 #车间下的项目

          @s2.push(item2)
        end
        item1[:data] = @s2
        if "蒸汽" == nenrgy_type.name
          # 公司合计日消耗计算方式： （蒸汽公司总计日用量 - 核糖车间核糖日用量 ） / 公司报表总产量日用量
          # 公司合计月消耗计算方式：   蒸汽公司总计月累积用量 / 公司报表总产量月累积
          steam_values = (@unit_day_sum.to_f - @ht_val.to_f)
          day_expend = (steam_values / @count_pro_d).round(2) #日单耗
          month_expend = (@item_month_sum / @count_pro_m).round(2) #月单耗
          item1[:g_totle] = {"name": "公司合计",
                             "field_code": "totle",
                             "field_value": @unit_day_sum.round(2),
                             "month_sum": @item_month_sum,
                             m_data: [{day_expend: day_expend, month_expend: month_expend, day_totle: @count_pro_d.round(2), month_totle: @count_pro_m}]
          }
        end
        @region_datas.push(item1)
      end
    end
    @energy_hash[:form_name] = @form_name
    @energy_hash[:d_time] = @today_time
    @energy_hash[:info] = @region_datas

    return @energy_hash
  end

end
