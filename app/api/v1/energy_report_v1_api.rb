class EnergyReportV1API < Grape::API

  format :json
  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end
  end

  desc "用电、循环水 上报 首页显示数据", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    # optional :station_ids, type: String, desc: "站点id， 101,102,103"
    # optional :page, type: Integer, desc: "页码"
  end
  get "/" do
    authenticate!

    #根据登录人员 获取人员所在的车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => @region_code.id) #表格信息  二个表时

    #表格信息
    @form_name = @d_report_forms.name #表格名称
    @form_id = @d_report_forms.id #表格id

    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")
    @form_type = SNenrgyFormType.where(:id => type_ids) #报表类型数据
    @type_name = @form_type[0].name #公司

    #项目 信息
    @ids = DMaterialReginCode.where(:s_region_code_id => @region_code.id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取

    time = Time.now.strftime("%Y-%m-%d")
    #根据人员车间 找报表id
    @d_report_form = DReportForm.find_by(:s_region_code_id => @region_code.id)
    report_form_id = @d_report_form.id #报表id
    @form_values = SNenrgyValue.where(:s_region_code_id => @region_code.id, :d_report_form_id => report_form_id, :datetime => time)


    @date=[]
    @plants.each do |p|
      item={}
      item["id"] = p.id
      item["name"] = p.name
      item["field_code"] = p.code
      energy_report = @form_values.find_by(:field_code => p.code)

      if energy_report.present? && energy_report.field_value.present?
        field_value = energy_report.field_value.to_f
      else
        field_value = ''
      end
      item["field_value"] = field_value
      @date.push(item)
    end

    #日报状态
    reports = @form_values.where(:status => 'Y') if @form_values.present?

    if reports.present? && reports.length >0
      return {status: '已上报', plants: @date, type_name: @type_name, form_name: @form_name, form_id: @form_id, region_code: @region_code}
    else
      return {status: '未上报', plants: @date, type_name: @type_name, form_name: @form_name, form_id: @form_id, region_code: @region_code}
    end
  end


  desc "用电，循环水 保存、上报操作", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :date_time, type: String, desc: '当前日期'
    optional :form_id, type: String, desc: '报表id'
    optional :status, type: String, desc: 'Y  上报  N 保存'
    #[{field_code: "electric", field_value: "11"}]
    optional :d_daily_reports, type: Array[String], desc: "项目信息"
  end
  post "/report" do
    authenticate!
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    @d_daily_reports = params[:d_daily_reports]
    status = params[:status] #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:form_id] #报表id


    # Rails.logger.info "----1----#{@d_daily_reports}----------"
    # Rails.logger.info "----11----#{@d_daily_reports[0]}----------"
    # Rails.logger.info "---2-----#{@d_daily_reports[0].class}----------"

    @d_daily = eval(@d_daily_reports[0]) if @d_daily_reports[0].present?


    # Rails.logger.info "---22-----#{@d_daily}----------"
    @d_daily.each do |d|

      #Rails.logger.info "---33-----#{d}----------"

      #判断是否已经存在当天的日报
      @day_report = SNenrgyValue.find_by(:s_region_code_id => @region_code.id, :datetime => date, :field_code => d[:field_code])
      if @day_report.present?
        @day_report.update(:field_value => d[:field_value], :status => status)
      else
        @d_daily_report = SNenrgyValue.new
        @d_daily_report.field_value = d[:field_value]
        @d_daily_report.field_code = d[:field_code]
        @d_daily_report.datetime = date
        @d_daily_report.status = status

        @d_daily_report.s_region_code_id = @region_code.id #车间id
        @d_daily_report.d_report_form_id = d_report_form_id #报表id
        @d_daily_report.save
      end
    end
    return {status: 'success'}
  end

#----------------------------------------------
  desc "蒸汽 上报 首页显示数据", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do

  end
  get "/steam_report_index" do
    authenticate!

    #根据登录人员 获取人员所在的车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => @region_code.id) #表格信息

    #表格头信息
    @form_name = @d_report_forms.name #表格名称
    @form_id = @d_report_forms.id #表格id
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",") #报表类型id
    #根据人员车间 找报表id   找值
    time = Time.now.strftime("%Y-%m-%d")
    report_form_id = @d_report_forms.id #报表id

    @s1 = [] #存类型
    if type_ids.length >0
      type_ids.each do |id|
        item1 = {} #存类型
        nenrgy_type = SNenrgyFormType.find_by(:id => id)
        item1[:type_name] = nenrgy_type.name #类型
        region_ids = steam_region_codes = DSteamRegionCode.where(:s_nenrgy_form_type_id => id).pluck(:s_region_code_id).uniq # 要去重（车间）   蒸汽项目与车间 类型的关联
        @s2 = [] #存车间
        region_ids.each do |region_id|
          item2 = {} #存车间
          region_code = SRegionCode.find_by(:id => region_id)
          item2[:region_name] = region_code.region_name
          item2[:region_id] = region_code.id #车间id
          @form_values = SNenrgyValue.where(:s_region_code_id => region_code.id, :d_report_form_id => report_form_id, :datetime => time)
          @ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => id, :s_region_code_id => region_id).pluck(:s_material_id)
          #找车间下的项目
          s_materials = SMaterial.where(:id => @ids)
          @s3 = [] #存项目
          s_materials.each do |m| #车间下的项目
            item3 = {}
            item3[:name] = m.name
            item3[:field_code] = m.code
            @form_value = @form_values.select {|x| x.field_code == m.code} #找到项目的值
            if @form_value.present? && @form_value.length == 1 && @form_value[0].field_value.present? #只能有有个值
              field_value = @form_value[0].field_value.to_f
            else
              field_value = ''
            end
            item3[:field_value] = field_value #值
            @s3.push(item3)
          end

          item2[:data] = @s3 #车间下的项目

          @s2.push(item2)
        end
        item1[:data] = @s2
        @s1.push(item1)
      end
    end
    @form_values2 = SNenrgyValue.where(:d_report_form_id => report_form_id, :datetime => time, :status => 'Y')
    if @form_values2.present? && @form_values2.length > 0
      status = '已上报'
    else
      status = '未上报'
    end

    return {status: status, plants: @s1, form_name: @form_name, form_id: @form_id, region_code: @region_code}
  end


#蒸汽上报
  desc "蒸汽上报操作", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :date_time, type: String, desc: '当前日期'
    optional :form_id, type: String, desc: '报表id'
    optional :status, type: String, desc: 'Y  上报  N 保存'
    optional :d_daily_reports, type: Array[String], desc: "项目信息"
  end
  post "/steam_report" do
    authenticate!
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    @d_daily_reports = params[:d_daily_reports]
    status = params[:status] #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:form_id] #报表id
    @d_daily = eval(@d_daily_reports[0]) if @d_daily_reports[0].present?

    #Rails.logger.info "---22-----#{@d_daily}----------"
    @d_daily.each do |d|

      #获取蒸汽下的车间
      region_datas = d[:data]
      region_datas.each do |region_data|
        plant_datas = region_data[:data] #车间下的项目
        region_id = region_data[:region_id] #车间id

        plant_datas.each do |p|

          #判断是否已经存在当天的日报
          @day_report = SNenrgyValue.find_by(:s_region_code_id => region_id, :datetime => date, :field_code => p[:field_code])
          if @day_report.present?
            @day_report.update(:field_value => p[:field_value], :status => status)
          else
            @d_daily_report = SNenrgyValue.new
            @d_daily_report.field_value = p[:field_value]
            @d_daily_report.field_code = p[:field_code]
            @d_daily_report.datetime = date
            @d_daily_report.status = status

            @d_daily_report.s_region_code_id = region_id #车间id
            @d_daily_report.d_report_form_id = d_report_form_id #报表id
            @d_daily_report.save
          end
        end
      end
    end
    return {status: 'success'}
  end

#------------一次水排污 上报----------------
  desc "一次水 排污 上报 首页显示数据", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do

  end
  get "/pollution_report_index" do
    authenticate!
    #根据登录人员 获取人员所在的车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    #根据region_code id获取报表
    @d_report_forms1 = DReportForm.where(:s_region_code_id => @region_code.id) #表格信息
    @d_report_forms = @d_report_forms1[1] #获取第二个表格

    #表格头信息
    @form_name = @d_report_forms.name #表格名称
    @form_id = @d_report_forms.id #表格id
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",") #报表类型id
    @region_code_ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_region_code_id).uniq #去重处理
    @region_datas = []
    time = Time.now.strftime("%Y-%m-%d")
    @region_code_ids.each do |id|
      item = {}
      region_code = SRegionCode.find_by(:id => id)
      item[:region_name] = region_code.region_name #车间名称
      item[:region_id] = region_code.id #车间id

      #根据车间 取值
      @form_values = SNenrgyValue.where(:s_region_code_id => region_code.id, :d_report_form_id => '5', :datetime => time)
      @steam_code = DSteamRegionCode.where(:s_region_code_id => id, :s_nenrgy_form_type_id => '5')
      s_material_ids = @steam_code.pluck(:s_material_id) #一个车间的项目id
      plants1 = SMaterial.where(:id => s_material_ids)
      plants_datas = [] #存车间下的项目信息
      plants1.each do |p|
        item1 = {}
        item1[:field_code] = p.code
        item1[:name] = p.name
        @form_value = @form_values.select {|x| x.field_code == p.code}
        if @form_value.present? && @form_value.length == 1 && @form_value[0].field_value.present? #只能有有个值
          field_value = @form_value[0].field_value.to_f
        else
          field_value = ''
        end
        item1[:field_value] = field_value
        plants_datas.push(item1)
      end
      item[:data] = plants_datas
      @region_datas.push(item)
    end
    @form_values2 = SNenrgyValue.where(:d_report_form_id => '5', :datetime => time, :status => 'Y')
    if @form_values2.present? && @form_values2.length > 0
      status = '已上报'
    else
      status = '未上报'
    end
    return {region_datas: @region_datas, form_name: @form_name, form_id: @form_id, status: status}
  end


#一次水、排污 上报操作
  desc "一次水、排污 上报操作", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :date_time, type: String, desc: '当前日期'
    optional :form_id, type: String, desc: '报表id'
    optional :status, type: String, desc: 'Y  上报  N 保存'
    #[{field_code: "electric", field_value: "11"}]
    optional :d_daily_reports, type: Array[String], desc: "项目信息"
  end
  post "/pollution_report" do
    authenticate!
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    @d_daily_reports = params[:d_daily_reports]
    status = params[:status] #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:form_id] #报表id
    @d_daily = eval(@d_daily_reports[0]) if @d_daily_reports[0].present?
    @d_daily.each do |d|
      region_id = d[:region_id] #车间id
      region_datas = d[:data]
      region_datas.each do |p|
        #判断是否已经存在当天的日报
        @day_report = SNenrgyValue.find_by(:s_region_code_id => region_id, :datetime => date, :field_code => p[:field_code])
        if @day_report.present?
          @day_report.update(:field_value => p[:field_value], :status => status)
        else
          @d_daily_report = SNenrgyValue.new
          @d_daily_report.field_value = p[:field_value]
          @d_daily_report.field_code = p[:field_code]
          @d_daily_report.datetime = date
          @d_daily_report.status = status

          @d_daily_report.s_region_code_id = region_id #车间id
          @d_daily_report.d_report_form_id = d_report_form_id #报表id
          @d_daily_report.save
        end
      end

    end
    return {status: 'success'}

  end


end