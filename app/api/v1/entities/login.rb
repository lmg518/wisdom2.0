module Entities
  class Login < Grape::Entity
    expose :id, documentation: {type: :Integer, desc: '用户ID'}
    expose :login_name, documentation: {type: :String, desc: '用户姓名'}
    expose :s_role_msg_id, documentation: {type: :Integer, desc: '角色ID'}
  end
end