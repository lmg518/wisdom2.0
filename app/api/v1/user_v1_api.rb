require 'grape'
class UserV1API < Grape::API
  format :json

  # before do
  #   header['Access-Control-Allow-Origin'] = '*'
  #   header['Access-Control-Allow-Methods'] =  %w{GET POST PUT}.join(",")
  #   header('Access-Control-Allow-Headers: X-Requested-With');
  # end

  helpers do
    def current_user
      token = headers['Authentication-Token']
      @current_user = DLoginMsg.find_by(session: token)
    rescue
      error!('401 Unauthorized', 401)
    end

    def authenticate!
      error!('401 Unauthorized', 401) unless current_user
    end
  end

  desc "用户登陆"
  params do
    requires :user_no, type: String, desc: "用户名"
    requires :user_pw, type: String, desc: "用户密码"
  end
  post '/' do
    @user = DLoginMsg.find_by(:login_no => params[:user_no], "valid_flag" => "1")

    #人员登录后返回  部门 code
    @region_code1= SRegionCode.find_by(:id => @user.group_id) #获取人员所在车间 或公司
    @region_code = @region_code1.present? ? @region_code1.region_code : '' #人员所在的车间/部门 code
    @region_name = @region_code1.present? ? @region_code1.region_name : '' #人员所在的车间/部门 name
    #人员权限
    role = SRoleMsg.find_by(:id => @user.s_role_msg_id)
    role_code = role.present? ? role.role_code : ''
    role_name = role.present? ? role.role_name : ''

    if @user.nil?
      return {message: "用户名或密码错误", status: 404}
    end
    unless @user.authenticate(params[:user_pw])
      return {message: "用户名或密码错误", status: 404}
    end
    puts request.ip
    if @user.update(:session => @user.new_session, :ip_address => env['action_dispatch.remote_ip'].to_s)
      {user: @user.attributes.slice("id", "session", "login_name", "login_no"),
       region_code: @region_code,
       region_name: @region_name,
       role_code: role_code,
       role_name: role_name
      }
      #{user: @user.attributes.slice("id","session","login_name","login_no")}
    else
      {user: "", message: "登陆失败", status: 401}
    end
  end

  desc "用户退出", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }

  post '/sign_out' do
    authenticate!
    begin
      @current_user.update(:session => "---------------")
      session[:session_code] = "----"
      return {message: "用户已退出", status: 201}
    rescue Exception => e
      puts e
      return error!('401 Unauthorized', 401)
    end

  end

  desc "用户修改", {
      headers: {
          "Authentication-Token" => {
              description: "用户Token",
              required: true
          }
      },
      failure: [[401, 'Unauthorized']]
  }
  params do
    optional :pwd, :type => String, desc: '密码'
    optional :tel, :type => String, desc: '手机号码'
    optional :name, :type => String, desc: '用户名'
  end
  post '/fix_infos' do
    authenticate!
    @current_user.password = params[:pwd] if params[:pwd].present?
    @current_user.telphone = params[:tel] if params[:tel].present?
    @current_user.login_name = params[:name] if params[:name].present?
    if @current_user.save
      return {msg: 'success', status: "200"}
    else
      return {msg: @current_user.errors, status: "500"}
    end
  end

end