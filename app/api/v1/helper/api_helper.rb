module ApiHelper

  def ApiHelper.current_user
    token = headers['Authentication-Token']
    @current_user = DLoginMsg.find_by(session: token)
  rescue
    error!('401 Unauthorized', 401)
  end

  def ApiHelper.authenticate!
    error!('401 Unauthorized', 401) unless current_user
  end

end