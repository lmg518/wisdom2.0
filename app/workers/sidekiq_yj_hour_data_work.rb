class SidekiqYjHourDataWork
    include Sidekiq::Worker
    include Sidetiq::Schedulable
    require 'rest-client'

    recurrence do
        #hourly.minute_of_hour(25)
        #minutely(20)
    end
   # SidekiqYjHourDataWork.perform_async()
    def perform(*args)
        Rails.logger.info "---抓取每天的小时数据---------"
        # 取数据
        @time = Time.now + (-1.month)
        @begin_time = @time.beginning_of_month.strftime("%Y%m%d%H")
        @end_time = @time.end_of_month.strftime("%Y%m%d%H")
        @days=@time.end_of_month.strftime("%d").to_i      #上月最后一天作为总天数

        Rails.logger.info "---上月开始时间---#{@begin_time}------"
        Rails.logger.info "---上月结束时间----#{@end_time}-------"
        Rails.logger.info "---总天数----#{@days}-------"

        url = "http://172.168.10.7:3000/api/v1/air_hour/station_hour_data"
        headers = {appkey: ApiAuth::AIR_KEY, Authorization: "Token token=#{ApiAuth::AIR_TOKEN}", params:{begin_time: "2017120100",end_time: "2017123123"}}
        response = RestClient.get( url, headers )
        json = JSON.parse(response.body)
        @hour_datas = json["data"]
        @stations=DStation.active
        if @hour_datas.present?
            @stations.each do |s|
                @station_hour_data=@hour_datas.select{|h| h["station_id"] == s.id }     #遍历站点获取每个站点一个月的数据
                if @station_hour_data.present?
                    #循环天数，获取每天的数据
                    @days.times do |i|
                        @day_str=@time.beginning_of_month + i.day
                        begin_num=@day_str.beginning_of_day.strftime("%Y%m%d%H")
                        end_num=@day_str.end_of_day.strftime("%Y%m%d%H")

                        Rails.logger.info "---Begin_num--#{begin_num.to_i}------"
                        Rails.logger.info "---End_num--#{end_num.to_i}------"

                        #@day_data=@station_hour_data.select{|d| (2017123100..2017123123) === d["data_time"].to_i }        
                        @day_data=@station_hour_data.select{|d| (begin_num.to_i..end_num.to_i) === d["data_time"].to_i }   #获取站点一天的数据
                        if @day_data.present?
                            Rails.logger.info "---day_data--#{@day_data}------"
                            Rails.logger.info "---Num--#{@day_data.size}------"
                            hour_num=@day_data.size
                            Rails.logger.info "-------创建小时数据统计------"
                            @d_data_analyze=DDataAnalyze.new(:station_name => s.station_name, 
                                                            :hour_standard_num => 24, 
                                                            :data_type => 3600,
                                                            :data_time => @day_str.strftime("%Y%m%d"),
                                                            :hour_num => hour_num, 
                                                            :d_station_id => s.id)
                            @d_data_analyze.save
                        end
                    end
                end
            end
        end



    end
end