class DayAccessStatistucsWorker

  @queue = :day_access_statistucs_worker

  def self.perform(*args)
    now_time = Time.now
    @data_yyyy_mms = DDataYyyymm.where(:data_cycle => 3600,
                                       :data_time => ((now_time - 1.days).beginning_of_day)..((now_time - 1.days).end_of_day))
                         .order(:station_id)
    @station_id_arr = @data_yyyy_mms.pluck(:station_id).uniq
    @station_id_arr.each do |id_arr|
      @so2,@co,@no2,@o3,@pm2_5,@pm10, = 0,0,0,0,0,0,0
      @so2 = @data_yyyy_mms.select{|x| x.station_id == id_arr.to_i && x.item_code == 101}.length
      @co = @data_yyyy_mms.select{|x| x.station_id == id_arr.to_i && x.item_code == 105}.length
      @no2 = @data_yyyy_mms.select{|x| x.station_id == id_arr.to_i && x.item_code == 102}.length
      @o3 = @data_yyyy_mms.select{|x| x.station_id == id_arr.to_i && x.item_code == 106}.length
      @pm2_5 = @data_yyyy_mms.select{|x| x.station_id == id_arr.to_i && x.item_code == 107}.length
      @pm10 = @data_yyyy_mms.select{|x| x.station_id == id_arr.to_i && x.item_code == 108}.length
      station =  DStation.find(id_arr.to_i)
      DayStationHourLength.create!(d_station_id: station.id,
                                   station_name: station.station_name,
                                   so2: @so2,
                                   co: @co,
                                   no2: @no2,
                                   o3: @o3,
                                   pm2_5: @pm2_5,
                                   pm10: @pm10,
                                   day_time: now_time - 1.days
      )
    end
  end

end