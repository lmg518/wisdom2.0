class StationLostWorker

  @queue = :station_lost_worker

  def self.perform(*args)
    @time = Time.now
    stations = DStation.all
    stations.each do |station|
      data_hours = station.d_data_yyyymms.where(:data_cycle => 3600, :data_time => (@time.beginning_of_hour - 1.hour))
      data_minutes = station.d_data_yyyymms.where(:data_cycle => 300, :data_time => (@time.beginning_of_minute - 30.minutes)..(@time.beginning_of_minute + 5.minutes))
      if data_hours.blank?
        unless station.d_abnormal_data_yyyymms.where(:create_acce => (@time.beginning_of_hour - 1.hour).strftime("%Y-%m-%d %H:%M:%S"), :ab_lable => 'lost')
          station.create_alarm_create(@time.beginning_of_hour - 1.hour,"#{station.station_name}小时离线" )
        end
      end
      if data_minutes.blank?
        unless station.d_abnormal_data_yyyymms.where(:create_acce => (@time.beginning_of_minute - 30.minutes).strftime("%Y-%m-%d %H:%M:%S"), :ab_lable => 'lost')
          station.create_alarm_create(@time.beginning_of_minute - 30.minutes,"#{station.station_name}5分钟离线" )
        end
      end
    end
  end

end