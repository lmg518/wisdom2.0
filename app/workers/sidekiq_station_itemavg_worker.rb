class SidekiqStationItemavgWorker
  include Sidekiq::Worker
  # include Sidetiq::Schedulable
  # recurrence do
  #   daily.hour_of_day(4)
  # end

  def perform(*args)
    puts "??????? 站点item day 平均值 Start==#{Time.now} "
    now_time = Time.now
    station_ids = DStation.where(:station_status => '1').pluck(:id)
    return if station_ids.blank?
    station_ids.each do |ids|
      @so2,@co,@no2,@o3,@pm2_5,@pm10, = 0,0,0,0,0,0,0
      @data_yyyu_mms = DDataYyyymm.where(:data_cycle => 3600, :station_id => ids.to_i,
                                         :data_time => ((now_time - 1.days).beginning_of_day)..((now_time - 1.days).end_of_day))
      data_101 = @data_yyyu_mms.select{|x| x.item_code == 101}
      @so2 = data_101.sum(0.0){|e| e.data_value.to_f} / data_101.length.to_f  if data_101.length != 0
      data_105 = @data_yyyu_mms.select{|x| x.item_code == 105}
      @co = data_105.sum(0.0){|e| e.data_value.to_f} / data_105.length.to_f if data_105.length != 0
      data_106 = @data_yyyu_mms.select{|x| x.item_code == 106}
      @no2 = data_106.sum(0.0){|e| e.data_value.to_f} / data_106.length.to_f if data_106.length != 0
      data_106 = @data_yyyu_mms.select{|x| x.item_code == 106}
      @o3 = data_106.sum(0.0){|e| e.data_value.to_f} / data_106.length.to_f if data_106.length != 0
      data_pm25 = @data_yyyu_mms.select{|x| x.item_code == 107}
      @pm2_5 = data_pm25.sum(0.0){|e| e.data_value.to_f} / data_pm25.length.to_f if data_pm25.length != 0
      data_pm10 = @data_yyyu_mms.select{|x| x.item_code == 108}
      @pm10 = data_pm10.sum(0.0){|e| e.data_value.to_f} / data_pm10.length.to_f if data_pm10.length != 0
      station = DStation.find(ids.to_i)
      StationItemAvg.create!(d_station_id: station.id, station_name: station.station_name,
                             so2_avg: @so2.to_f.round(2),
                             co_avg: @co.to_f.round(2),
                             no2_avg: @no2.to_f.round(2),
                             o3_avg: @o3.to_f.round(2),
                             pm2_5_avg: @pm2_5.to_f.round(2),
                             pm10_avg: @pm10.to_f.round(2),
                             day_time: now_time - 1.days
      )
    end
    @so2,@co,@no2,@o3,@pm2_5,@pm10, = 0,0,0,0,0,0,0
    @data_yyyu_mms = DDataYyyymm.where(:data_cycle => 3600,
                                       :data_time => ((now_time - 1.days).beginning_of_day)..((now_time - 1.days).end_of_day))
    @so2 = @data_yyyu_mms.where(:item_code => 101).average("data_value").to_f.round(2)
    @co = @data_yyyu_mms.where(:item_code => 102).average("data_value").to_f.round(2)
    @no2 = @data_yyyu_mms.where(:item_code => 105).average("data_value").to_f.round(2)
    @o3 = @data_yyyu_mms.where(:item_code => 106).average("data_value").to_f.round(2)
    @pm2_5 = @data_yyyu_mms.where(:item_code => 107).average("data_value").to_f.round(2)
    @pm10 = @data_yyyu_mms.where(:item_code => 108).average("data_value").to_f.round(2)
    StationItemAvg.create!(d_station_id: 0, station_name: "平均",
                           so2_avg: @so2,
                           co_avg: @co,
                           no2_avg: @no2,
                           o3_avg: @o3,
                           pm2_5_avg: @pm2_5,
                           pm10_avg: @pm10,
                           day_time: now_time - 1.days)

    # time_now = now_time.beginning_of_day + 25.hours + 45.minutes
    # SidekiqAlarmRuleWorker.perform_in(time_now)
    # puts "??????? 站点item day 平均值 End==#{time_now} "
  end
end
