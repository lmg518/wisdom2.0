class SidekiqYjHourlyWork
  include Sidekiq::Worker
  include Sidetiq::Schedulable
  include YjzhAirHourHelper
  require 'rest-client'

  recurrence do
    #hourly.minute_of_hour(25)
    #minutely(20)
  end

  # SidekiqYjHourlyWork.perform_async()
  def perform(*args)
    # 取数据
    @time = Time.now
    @hour = @time.beginning_of_hour
    time_h =@hour.strftime("%Y%m%d%H")
    #url = "http://localhost:3030/station_hourly_data.json"
    url = "http://172.168.10.7:3000/station_hourly_data.json"
    headers = {appkey: ApiAuth::AIR_KEY, Authorization: "Token token=#{ApiAuth::AIR_TOKEN}", params:{time_h: time_h}}   # "2017121020"  
    response = RestClient.get( url, headers )
    json = JSON.parse(response.body)
    @hour_datas = json["data"]
    if @hour_datas.present?
      @hour_datas.each do |f|
        YjzhAirHourHelper.is_exception? (f) # 判断是否异常
      end
    end
  end
end
