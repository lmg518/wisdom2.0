class SidekiqCreateMonthPlanWorker
    #定时自动生成下月计划
    include Sidekiq::Worker
    include Sidetiq::Schedulable

    #recurrence { daily }  每天午夜执行
 
    recurrence do
       # 每隔15分钟运行一次：minutely(15)
       # 每个小时的0，15，30，45分都执行：hourly.minute_of_hour(0, 15, 30, 45)
       # minutely(1)
       #monthly.day_of_month(15).hour_of_day(8).minute_of_hour(50)  #每月的13号1点执行
    end

    def perform(*args)
        #Rails.logger.info "statrt ==> #{Time.now}"
        #puts "AQI小时   statrt ==> #{Time.now}"
        #next_mmm_work_jobs?month=1  #0 本月   1 下月
        MonthWorkPlanJob.new.perform(5)
    end
end