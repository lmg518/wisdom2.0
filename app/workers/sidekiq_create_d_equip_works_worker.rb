class SidekiqCreateDEquipWorksWorker
  #定时自动生成点检记录
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  #自定义一个队列名称 启动队列时 可以指定改队列名
  #bundle exec sidekiq -d -q wisdom -l sidekiq_log.txt
  sidekiq_options :queue => :wisdom

  #recurrence { daily }  每天午夜执行

  recurrence do
    # 每隔15分钟运行一次：minutely(15)
    # 每个小时的0，15，30，45分都执行：hourly.minute_of_hour(0, 15, 30, 45)
    # minutely(20)
    #monthly.day_of_month(15).hour_of_day(8).minute_of_hour(50)  #每月的13号1点执行
    #daily.hour_of_day(2) #每天2点执行
  end

  def perform(*args)

    rule_datas= DEquipWorkRule.all
    datas=[]
    rule_datas.each do |r|
      item={}
      item[:s_region_code_id] = r.s_region_code_id
      item[:s_area_id] = r.s_area_id
      item[:region_name] = r.region_name
      item[:work_desc] = r.work_desc
      item[:s_region_code_info_id] = r.s_region_code_info_id
      item[:d_login_msg_id] = r.d_login_msg_id

      work_name = DLoginMsg.find_by(:id => r.d_login_msg_id)
      if work_name.present?
        work_person = work_name.login_name
        work_status = 'wait_carried' #处理中
      else
        work_person = ''
        work_status = 'wait_issued' #待下发
      end
      item[:work_person] = work_person
      item[:fourder] = r.fourder

      time = (Time.now).strftime("%Y-%m-%d") + " "
      work_start_time = time + r.work_start_time
      work_end_time = time + r.work_end_time

      # Rails.logger.info "------time---#{work_start_time}------"
      # Rails.logger.info "------time---#{work_end_time}------"
      # Rails.logger.info "------time---#{Time.parse(work_end_time)}------"

      item[:work_start_time] = Time.parse(work_start_time)
      item[:work_end_time] = Time.parse(work_end_time)
      item[:work_status] = work_status
      item[:created_at] = Time.now
      item[:updated_at] = Time.now
      item[:boot_ids] = r.boot_ids
      datas.push(item)
    end

    datas.each do |d|
      equip_work = DEquipWork.new

      equip_work.work_status =d[:work_status]
      equip_work.founder = d[:fourder]
      equip_work.work_person = d[:work_person]
      equip_work.d_login_msg_id = d[:d_login_msg_id]
      equip_work.s_region_code_id = d[:s_region_code_id]
      equip_work.region_name = d[:region_name]
      equip_work.s_region_code_info_id = d[:s_region_code_info_id]
      equip_work.s_area_id = d[:s_area_id]
      equip_work.work_start_time = d[:work_start_time]
      equip_work.work_end_time = d[:work_end_time]
      equip_work.work_desc = d[:work_desc]
      #equip_work.save

      boot_ids = d[:boot_ids].split(",")

      if equip_work.save
        #极光推送 提醒机修人员
        content = '你有新的点检记录要处理，请查看'
        delay_time = ((equip_work.work_start_time - Time.now) / 60).to_i
        d_login_msg = DLoginMsg.find_by(:login_name => equip_work.work_person)
        if d_login_msg.present? && d_login_msg.uuid.present?
          #SidekiqJpushSingleWorker.perform_async(content, d_login_msg.uuid, 'DJ')
          SidekiqJpushSingleWorker.perform_in(delay_time.minutes, content,d_login_msg.uuid,'DJ')  #延迟推送
        end

        boot_ids.each do |b|
          WorkBoot.create!(:d_equip_work_id => equip_work.id, :boot_check_id => b.to_i,
                           :check_project => BootCheck.find(b).check_project)
        end
        WorkNote.create!(:d_equip_work_id => equip_work.id, :status => 'created',
                         :login_name => '系统')
        WorkNote.create!(:d_equip_work_id => equip_work.id, :status => 'issued',
                         :login_name => d[:work_person])
      end
    end
  end
end