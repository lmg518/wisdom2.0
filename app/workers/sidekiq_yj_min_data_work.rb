class SidekiqYjMinDataWork
    include Sidekiq::Worker
    include Sidetiq::Schedulable
    include YjzhAirMinHelper
    require 'rest-client'

    recurrence do
        #hourly.minute_of_hour(25)
        #minutely(20)
    end
   # SidekiqYjMinDataWork.perform_async()
    def perform(*args)
        Rails.logger.info "---抓取每天的5min数据---------"
        # 取数据
        @time = Time.now + (-1.month)
        # @begin_time = @time.beginning_of_month.strftime("%Y%m%d%H%M")
        # @end_time = @time.end_of_month.strftime("%Y%m%d%H%M")

        @days=@time.end_of_month.strftime("%d").to_i      #上月最后一天作为总天数

        # Rails.logger.info "---上月开始时间---#{@begin_time}------"
        # Rails.logger.info "---上月结束时间----#{@end_time}-------"
        Rails.logger.info "---总天数----#{@days}-------"

        #循环天数获取每天的5min数据
         @days.times do |i|
            @day_str=@time.beginning_of_month + i.day
            begin_time=@day_str.beginning_of_day.strftime("%Y%m%d%H%M")
            end_time=@day_str.end_of_day.strftime("%Y%m%d%H%M")

            Rails.logger.info "----当天---开始日期：#{begin_time}--------"
            Rails.logger.info "----当天---结束日期：#{end_time}--------"

            YjzhAirMinHelper.get_data(begin_time, end_time)   #获取每天5min数据

         end
    end
end