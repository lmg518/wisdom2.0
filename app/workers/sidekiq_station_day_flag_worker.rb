class SidekiqStationDayFlagWorker
  include Sidekiq::Worker
  # include Sidetiq::Schedulable
  # recurrence do
  #   daily.hour_of_day(3)
  # end

  def perform(*args)
    puts "?????? 站点有效率  Start==#{Time.now} "
    now_time = Time.now
    @data_yyyy_mms = DDataYyyymm.where.not(:data_label => ['',"PS","PZ",'AS','CZ','CS','RM'])
                         .where(:data_cycle => 3600,
                                :data_time => ((now_time - 1.days).beginning_of_day)..((now_time - 1.days).end_of_day))
    @data_yyyy_mms.each do |yy_mm|
      station = yy_mm.d_station
      next if station.blank?
      region_code = station.s_region_codes
      next if region_code.blank?
      region_code = region_code[0]
      city = station.s_administrative_area
      province = city.parent
      DayHourFalgData.create!(region_code_id: region_code.id,
                              province: province.present? ? province.zone_name : "",
                              city: city.zone_name,
                              d_station_id: station.id,
                              station_name: station.station_name,
                              item_id: yy_mm.item_code,
                              item_name: yy_mm.item_code_name,
                              item_flag: yy_mm.data_label,
                              item_value: yy_mm.data_value,
                              acce_data: yy_mm.data_time)
    end

    # time_now = now_time.beginning_of_day + 25.hours + 18.minutes
    # SidekiqStationDayFlagWorker.perform_in(time_now)
    # puts "?????? 站点有效率  End==#{time_now} "
  end
end
