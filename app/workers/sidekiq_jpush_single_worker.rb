class SidekiqJpushSingleWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  #自定义一个队列名称 启动队列时 可以指定改队列名
  #bundle exec sidekiq -d -q wisdom -l sidekiq_log.txt
  sidekiq_options :queue => :wisdom

  #设备点检 设备检修 新建工单时 消息推送
  # SidekiqJpushSingleWorker.perform_in(1.minutes, '你好','100d855909102188826','DJ')  #延迟执行
  # SidekiqJpushSingleWorker.perform_async                     #立即执行
  def perform(*args)
    Rails.logger.info "----消息推送的uuid----#{args[1]}------"
    content,uuid,code = args[0],args[1],args[2]
    result = JpushSend.send_msg(content,uuid,code)
    Rails.logger.info "----消息推送结果----#{result}------"
  end

end