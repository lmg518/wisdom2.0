require 'jpush'
#  JpushSend.send_msg("你有工单要处理1","100d855909102188826",'JX')
class JpushSend
  def self.send_msg(*args)
    begin
      msg, registration_ids = args[0], args[1]
      code = args[2] # DJ 点检记录    JX 检修记录
      msg_content = '消息内容本身'
      title = '消息标题'
      extras = {code: code, content: ''} # DJ 点检记录    JX 检修记录
      # 创建推送设备对象
      audience = JPush::Push::Audience.new.set_registration_id(registration_ids)
      jpush = JPush::Client.new(JpushConfig::APP_KEY, JpushConfig::MASTER_SECRET)
      push_payload = JPush::Push::PushPayload.new(
          platform: 'all',     # 是	表示推送的平台，其可接受的参数为 'all'（表示推送到所有平台）, 'android' 或 'ios' 或 ['android', 'ios']
          audience: audience,  # 是	表示推送的设备，其可接受的参数为 'all' （表示发广播,推送到全部设备） 或者一个 Audience 对象
          notification: msg
      ).set_message(     #应用内消息
          msg_content,
          title: title,
          extras: extras
      )
      result = jpush.pusher.push(push_payload)
      Rails.logger.info "----1----#{result}------"
    rescue Exception => ex
      if result
        if result[:error]
          Rails.logger.error "JPush send fail! response code: #{result[:error]}"
        end
      end
      Rails.logger.error ex
    end
  end
end