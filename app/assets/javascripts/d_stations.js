$(function(){
//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
//	获取区域
	function getAreas(){
		$.getJSON('/s_administrative_areas.json').done(function(data){
	        var regions = template('regions_Choose',data);
			$('.regionChoose .Region_content').html(regions);
	    })
	}
//	遮罩层
	$('body').delegate(".public_area h4 .close","click",function(){
 		$('#public_box').hide();
 	})
	$('body').delegate(".public_revise h4 .close","click",function(){
 		$('#public_box').hide();
 	})
	$('body').delegate(".public_config .close","click",function(){
 		$('#public_box').hide();
 		$('.public_config .config_list input').prop('checked',false)
 	})
	$('body').delegate(".filed .map_choose","click",function(){
 		$('#dStations').hide();
		$('#public_box').hide();
		$('#allmap').show();
		$('#r-result').show();
 	})
//	$('body').delegate('#r-result .new_add_btn','click',function(){
//		$('#r-result').hide();
//		$('#public_box').hide();
//	})
    $('body').delegate(".filed .dateTimepicker","focus",function(){
            getStationTime();
   	})
	$('body').delegate(".public_area .area_btn .btn-cancel","click",function(){
 		$('#public_box').hide();
 	})
	$('body').delegate(".public_revise .area_btn .btn-cancel","click",function(){
 		$('#public_box').hide();
 	})
   //遮罩层区域选择
   $('body').delegate(".regionChoose .pull-right","click",function(){
 		$('.regionChoose').hide();
 	})
    $('body').delegate(".filed .region_choose","click",function(){
 		$('.regionChoose').show();
 		var textId=$('#textId').val()
    	$('.regionChoose .Region_content .region_content .regionID').each(function(){
    		var cId=$(this).text();
    		if(cId==textId){
    			$(this).next('input[type=radio]').prop('checked',true)	
    			return false;
    		}
    		
    	})
    	
    	
 	})
    $('body').delegate(".regionChoose .region_btn .btn-sure","click",function(){
		var radioCheck_name=$('.regionChoose').find('input[type=radio]:checked').next('.regionName').text();
		var radioCheck_ID=$('.regionChoose').find('input[type=radio]:checked').prev('.regionID').text();
		$('.filed .region_attr').val(radioCheck_name);
		$('.filed .regionID').val(radioCheck_ID);
		$('.regionChoose').hide();
	})
    $('body').delegate(".regionChoose .region_btn .btn-cancel","click",function(){
		$('.regionChoose').hide();
	})
//查询
	var stationid="";
	function pageName_Data(){
		$('.public_search .ids').val($('.public_search .control_stationIDs').val());
		if($('.public_search .ids').val()){
			stationid=$('.public_search .ids').val().split(",");
		}
	
		
	};
	$(document).delegate('.public_search .search','click',function(){
		$('.choose-site').hide()
		$('.loading').show()
//		if($('.control_stationID').val()){
//			stationid=$('.public_search .ids').val().split(',');
//		}
		stationid=""
		pageName_Data();
		
		$.ajax({
			type:"get",
			url:"/d_stations/all_stations.json",
			async:true,
			dataType:'json',
			data:{
				station_ids:stationid
			},
			success:function (data){
				data_hash = data
				$('.loading').hide()
		   		$("#page").initPage(data.list_size, 1, origin);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				alert("查询站点过多")
				console.log(err);
			}
		});
	})
//添加站点
	$('.public_search .btn_area').click(function(){	 
		$('.choose-site').hide()
		$('#search-station .choose-site').hide();
//		$('#public_box').show();
//		var data = {};
//		var htmlStr=template("d_stations_add_area",data);
//		$("#public_box").empty();
//		$("#public_box").html(htmlStr);
		$('.filed .region_attr').val('');
		$('.filed .region_attr').attr("disabled",false);
		$('.filed .station_name').val('');
		$('.filed .station_name').attr("disabled",false);
		$('.filed .region_choose').show();
		valid();
		 getAreas();
	})
	//添加提交
	$(document).delegate('#addModal .modal-footer  #addBtn','click',function(){
//		$('.public_area .area_btn .btn-sure').text("提交中...");
//		if($('.public_area .area_btn .btn-sure').text("提交中...")){
//			$('.public_area .area_btn .btn-sure').attr('disabled',true)
//		}else{
//			$('.public_area .area_btn .btn-sure').attr('disabled',false)
//		}		
		var station_name=$('#addModal .filed .station_name').val();
		var station_type=$('#addModal .filed .station_type option:selected').val();
		var dateTimepicker=$('#addModal .filed .day-start').val();
		// var version_number=$('.filed .version_number').val();
		var longGat=$('#addModal .filed .longGat').val();
		var server_number=$('#addModal .filed .server_number').val();
		var longitude =$('#addModal .filed .longLat').val();
		var status =$("#addModal .status input[type='radio']:checked").val();
		var region_attr =$('#addModal .filed .region_attr').val();
		var location =$('#addModal .filed .location').val();
		var remarks=$('#addModal .filed .remarks').val();
		var distance=$('#addModal .filed .distance').val();
		var arrive_time=$('#addModal .filed .arrive_time').val();
		var match=$('#addModal .filed #match').val();
		var stationIp=$('#addModal .filed .stationIp').val();
//		新增站点
		$.ajax({
			type: "POST",
			url: "/d_stations",
			async: true,
			dataType: "json",
			data: {
				d_station: 
					{
						station_name: station_name,
						station_type:station_type,
						created_at:dateTimepicker,
						// version_number:version_number,
						latitude:longGat,
						longitude:longitude,
						service_no:server_number,
						station_status:status,
						region_code:region_attr,
						station_location:location,
						note:remarks,
						wc_station_id:match,
						arrive_time:arrive_time,
						distance:distance,
						station_ip:stationIp

					}
				},
			success:function(data){	
				page_state = "change";
				alert("提交成功");
				$('#addModal ').modal('hide');
				origin();
				document.getElementById("add_station_form").reset()
			},
			error: function(err){
                //请求出错处理
                console.log(err)
                alert('添加失败')
            }       
		});

	})

//	配置获取
    var idConfig;
//  function config(){
	$('body').delegate('.table .config','click',function(){
		 $('#search-station .choose-site').hide();
//          $('#public_box').show();           
//          var data={};
//          var htmlStr=template("d_stations_config",data);
//		    $("#public_box").html(htmlStr);
//		    $('.public_config').show();
			idConfig=$(this).parents('tr').find('.id').text();
			$.ajax({
				url: '/search_regin_logins/'+idConfig+'.json',
			    type: 'get',
			    async: true,
			    dataType:'json',
			    success:function( data ) {
			    	var configTemplate = template('configTemp',data);
				    $('.config_content').html(configTemplate);
				 	$('.config_list .left .renyuan_span .flag').each(function(i){
					  	var chec=$(this).text();
					  	if(chec=="true"){
					  		
					  		$(this).next('.check').attr('checked',true)
					  	}
				    })

			   },
			   error:function(err){
			   	console.log(err)
			   }
			})
	})
//}
// 配置全选框
	$(document).delegate('.config_list .right input','click',function(){
		$(this).parents('.left').find('input').prop('checked',this.checked);
	})

//	配置提交
	$('body').delegate('#configModal .modal-footer .btn-sure','click',function(){
//		$('.public_config .config_btn .btn-sure').text('配置中...');
//		if($('.public_config .config_btn .btn-sure').text('配置中...')){
//			$('.public_config .config_btn .btn-sure').attr('disabled',true);
//		}else{
//			$('.public_config .config_btn .btn-sure').attr('disabled',false);
//		}
        var Array=[];
        var oIn_left=$('.config_list .left input[type="checkbox"]')
		$(oIn_left).each(function(i) {		
			if(oIn_left[i].checked){
				var ar=$(this).parent().find('.renid').text();
				Array.push(ar);
			}
		});
		if(Array.length!=0){
			$.ajax({
				url: '/d_stations/'+idConfig+'/handle_station',
			    type: 'post',
			    async: true,
			    data:{
			    	login_arr:Array			    
			    	},
			    success:function( data ) {
					$('#configModal').modal('hide');
			    	alert("配置成功");
			    	$('.public_config .config_list input').prop('checked',false)
			   },
			   error:function(err){
			   	console.log(err)
			   	alert("配置失败");
			   	$('.public_config .config_list input').prop('checked',false)
			   }
			})
		}
	})
//	取消配置
	$('body').delegate('.public_config .config_btn .btn-cancel','click',function(){
		$('#public_box').hide();
		$('.public_config .config_list input').prop('checked',false)
		})
	
//	停用启用

	$(document).delegate('table tbody td .stop','click',function(){
		
		 $('#search-station .choose-site').hide();
		$(this).siblings('.revise').css({"cursor":"not-allowed"},{'color':"#666"});
		$(this).siblings('.config').css({"cursor":"not-allowed"},{'color':"#666"});
		var statusStop;
		if($(this).text()=='停用'){
			
			statusStop='off'
			
		}else{
			
			statusStop='on'
			
		}
//		$(this).text()=='停用'? (statusStop='on') : (statusStop='off');
		
		var idStop=$(this).parents('tr').find('.id').text();
		$.ajax({
		    url: '/d_stations/'+idStop+'.json',
		    type: 'PUT',
		    data:{onoff_flag:statusStop},
		    success:function( data ) {
		    	
		    	page_state = "change";
		    	
				origin();

		   },
		   error:function(err){
		   	console.log(err);
		   }
		});	
	})
	
//	修改
	var id;
	$('body').delegate('#dStations table tbody tr td .revise','click',function(){
		
		$('#search-station .choose-site').hide();
		var region_name=$(this).parents('tr').find('.region_name').text();
		var station_name=$(this).parents('tr').find('.station_name').text();
		$('.filed .region_attr').val(region_name);
		$('.filed .region_attr').attr("disabled",true);
		$('.filed .station_name').val(station_name);
		$('.filed .station_name').attr("disabled",true);
		$('.filed .region_choose').css("display","none");
		id=$(this).parents('tr').find('.id').text();

		$.ajax({
			type:'get',
			url:'/d_stations/'+id+'.json',
			success:function(data){

				var edit={
					edit:data
				}
				var htmlStr=template("d_stations_revise",edit);
			    $("#editModal .modal-body").html(htmlStr);
				valid();
			    getAreas();
			    $('.day-start').datetimepicker({  
			        format: 'yyyy-mm-dd',  
				       weekStart: 1,  
				       autoclose: true,  
				       startView: 2,  
				       minView: 2,  
				       forceParse: true,  
				       language: 'zh-CN' ,
				       setEndDate:$('.day-end').val(),
			    }).on('change',function(ev){
					    var startDate = $('.day-start').val();
					    $(".day-end").datetimepicker('setStartDate',startDate);
					    $(".day-start").datetimepicker('hide');
			    });
			},
			error:function(err){
				console.log(err)
			}
		})
	})
//      修改提交
	$('body').delegate('#editModal .modal-footer #editBtn','click',function(){
//		$('.public_revise .area_btn .btn-sure').text("提交中...");
//			if($('.public_revise .area_btn .btn-sure').text("提交中...")){
//				$('.public_revise .area_btn .btn-sure').attr('disabled',true)
//			}else{
//				$('.public_revise .area_btn .btn-sure').attr('disabled',false)
//		}
		var station_name=$('#editModal .filed .station_name').val();
		var station_type=$('#editModal .filed .station_type option:selected').val();
		var dateTimepicker=$('#editModal .filed .day-start').val();
		// var version_number=$('.filed .version_number').val();
		var longGat=$('#editModal .filed .longGat').val();//纬度
		var server_number=$('#editModal .filed .server_number').val();
		var longitude =$('#editModal .filed .longLat').val();//经度
		var status =$("#editModal .status input[type='radio']:checked").val();
		
		var region_attr =$('#editModal .filed .region_attr').val();
		var locations =$('#editModal .filed .location').val();//地理位置
		var remarks=$('#editModal .filed .remarks').val();
		var match=$('#editModal .filed #match').val();
		var stationIp=$('#editModal .filed .stationIp').val();
		$.ajax({
		    url: '/d_stations/'+id+'.json',
		    type: 'PUT',
			data: {
				d_station: 
				{
					station_name: station_name,
					station_type:station_type,
					created_at:dateTimepicker,
					// version_number:version_number,
					latitude:longGat,
					longitude:longitude,
					service_no:server_number,
					station_status:status,
					region_code:region_attr,
					station_location:locations,
					note:remarks,
					wc_station_id:match,
					station_ip:stationIp
					
				}
			},
		    success:function( Data ) {
		    	page_state = "change";
				origin();
				$('#editModal ').modal('hide');
				alert('修改成功')
		   },
		   error:function(err){
		   	console.log(err)
		   	alert('修改失败了');
		   }
		});
	})
		
		
//	时间
    function getStationTime(){
		$('.dateTimepicker').datetimepicker({
			format:"Y-m-d",
			todayButton:true,
			timepicker:false
		});
    }
//	   初始数据
	function origin(param){
//		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
    		var html = template('d_stations',data_hash);
			$('#public_table table').html(html);
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var station_ids = $('.public_search .ids').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
			$.ajax({
				url: '/d_stations/all_stations.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					station_ids:station_ids
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var html = template('d_stations',data);
					$('#public_table table').html(html);
					$('.page_total span a').text(pp);
					$('#dStations table .dStatus').each(function(i){
				    	var s=$(this).text();
//				    	if(s="停用"){
//				    		$(this).siblings().find('.revise').hide();
//				    	}
				    })
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	}; 
	$('.loading').show();
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/d_stations/all_stations.json',
		success:function(data){
			console.log(data)
			$('.loading').hide();
			var total = data.list_size;
			data_hash=data
		    $("#page").initPage(total, 1, origin);
		    $('.page_total span small').text(total);
		    $('#dStations table .dStatus').each(function(i){
		    	var s=$(this).text();
//		    	if(s="停用"){
//		    		$(this).siblings().find('.revise').hide();
//		    	}
		    })
		},
		error:function(err){
			console.log(err)
		}
	})
	 //每页显示更改
    $("#pagesize").change(function(){
    	$('.loading').show()
    	var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/d_stations/all_stations.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	// 百度地图API功能
	function G(id) {
		return document.getElementById(id);
	}
	var map = new BMap.Map("allmap");
        map.centerAndZoom(new BMap.Point(113.630729, 34.73962), 11);
		var point = new BMap.Point(113.630729,34.73962);
        map.centerAndZoom(point);
        var geoc = new BMap.Geocoder();
        map.enableScrollWheelZoom(true);
		map.enableContinuousZoom(); 
	var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
		{"input" : "suggestId"
		,"location" : map
	});
	
		
	ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
	var str = "";
		var _value = e.fromitem.value;
		var value = "";
		if (e.fromitem.index > -1) {
			value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		}    
		str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;
		
		value = "";
		if (e.toitem.index > -1) {
			_value = e.toitem.value;
			value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		}    
		str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
		G("searchResultPanel").innerHTML = str;
	});

	var myValue;
	ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
	var _value = e.item.value;
		myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;
		
		setPlace();
	});

	function setPlace(){
		map.clearOverlays();    //清除地图上所有覆盖物
		function myFun(){
			var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
			map.centerAndZoom(pp, 18);
			map.addOverlay(new BMap.Marker(pp));    //添加标注
		}
		var local = new BMap.LocalSearch(map, { //智能搜索
		  onSearchComplete: myFun
		});
		local.search(myValue);
	}

	map.onclick = function(e){
		var pt = e.point;
		geoc.getLocation(pt, function(rs){
            var addComp = rs.addressComponents;
			var place = addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street;
            if(confirm("当前选址："+place)){
				map.addEventListener("click");
				$('.location').val(place);
				$(".longLat").val(e.point.lng);
				$(".longGat").val( e.point.lat);
				$("#allmap").css('display','none');
				$('#r-result').css('display','none')
				$('#dStations').show();
				$('#public_box').show();
            }else{
				map.removeEventListener("click");
			};
		});	
	}
//	站点名称表单验证
	function valid(){
//		站点名称
		var stationName = document.getElementById("stationName");
		var station_nameTip = document.getElementById("station_nameTip");
		$('body').delegate("#stationName","focus",function(){
			station_nameTip.className = "control-default";
			station_nameTip.firstChild.nodeValue = "请输入站点名称."
		});
		$('body').delegate("#stationName","blur",function(){
			if(stationName.validity.valid){
				station_nameTip.className = "control-success";
				station_nameTip.firstChild.nodeValue = "站点名称正确.";
			}else if(stationName.validity.valueMissing){
				station_nameTip.className = "control-error";
				station_nameTip.firstChild.nodeValue = "站点名称不能为空."
			}else if(stationName.validity.patternMismatch){
				station_nameTip.className = "control-error";
				station_nameTip.firstChild.nodeValue = "站点名称输入有误."
			}
		});
//		日期
		var datetime=document.getElementById("datetime");
		var time_nameTip=document.getElementById("time_nameTip");
		$('body').delegate("#time_nameTip","focus",function(){
			time_nameTip.className = "control-default";
			time_nameTip.firstChild.nodeValue = "请选择日期.";
			if(datetime.value != ""){
				time_nameTip.className = "control-default";
				time_nameTip.firstChild.nodeValue = "请选择日期.";
			}else{
				time_nameTip.className = "control-error";
				time_nameTip.firstChild.nodeValue = "日期不能为空."
			}
		
		});
		$('body').delegate("#datetime","blur",function(){
			if(datetime.validity.valid){
				time_nameTip.className = "control-success";
				time_nameTip.firstChild.nodeValue = "日期选择正确.";
			}else if(datetime.validity.valueMissing){
				time_nameTip.className = "control-error";
				time_nameTip.firstChild.nodeValue = "日期不能为空."
			}else if(datetime.validity.patternMismatch){
				time_nameTip.className = "control-error";
				time_nameTip.firstChild.nodeValue = "日期输入有误."
			}
			
		});
//		纬度
		var longGat=document.getElementById("longGat");
		var longGat_Tip=document.getElementById("longGat_Tip");
		$('body').delegate("#longGat","blur",function(){
			if(longGat.value != ""){
				longGat_Tip.className = "control-success";
				longGat_Tip.firstChild.nodeValue = "格式正确.";
			}else{
				longGat_Tip.className = "control-error";
				longGat_Tip.firstChild.nodeValue = "请点击地图选址."
			}
		});
	
//		经度
		var longLat=document.getElementById("longLat");
		var longLat_Tip=document.getElementById("longLat_Tip");
		$('body').delegate("#longLat","blur",function(){
			if(longLat.value != ""){
				longLat_Tip.className = "control-success";
				longLat_Tip.firstChild.nodeValue = "格式正确.";
			}else{
				longLat_Tip.className = "control-error";
				longLat_Tip.firstChild.nodeValue = "请点击地图选址."
			}
		});
//		区域归属
		var region_attr=document.getElementById("region_attr");
		var region_attrTip=document.getElementById("region_attrTip");
		$('body').delegate("#region_attr","blur",function(){
			if(region_attr.value != ""){
				region_attrTip.className = "control-success";
				region_attrTip.firstChild.nodeValue = "格式正确.";
			}else{
				region_attrTip.className = "control-error";
				region_attrTip.firstChild.nodeValue = "区域归属不能为空."
			}
		});
//		地理位置
		var location=document.getElementById("locat");
		var location_Tip=document.getElementById("location_Tip");
		$('body').delegate("#location","blur",function(){
			if(location.value != ""){
				location_Tip.className = "control-success";
				location_Tip.firstChild.nodeValue = "格式正确.";
			}else{
				location_Tip.className = "control-error";
				location_Tip.firstChild.nodeValue = "地图选址有误."
			}
		});
//		匹配字段
		var match=document.getElementById("match");
		var match_Tip=document.getElementById("match_Tip");
		$('body').delegate("#match","focus",function(){
			match_Tip.className = "control-default";
			match_Tip.firstChild.nodeValue = "请输入匹配字段"
		});
		$('body').delegate("#match","blur",function(){
			if(match.validity.valid){
				match_Tip.className = "control-success";
				match_Tip.firstChild.nodeValue = "匹配字段输入正确.";
			}else if(match.validity.valueMissing){
				match_Tip.className = "control-error";
				match_Tip.firstChild.nodeValue = "匹配字段不能为空."
			}else if(match.validity.patternMismatch){
				match_Tip.className = "control-error";
				match_Tip.firstChild.nodeValue = "字段输入有误."
			}
		});
	}
	
})
   