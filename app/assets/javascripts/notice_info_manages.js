$(function(){
	$(document).delegate('#addModal .modal-footer .btn-default','click',function(){
		$('#add_form .form-group input').val("");
	})
	
	
	$(document).ready(function() {
	    $('#add_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            title: {
	                validators: {
	                    notEmpty: {
	                        message: '姓名不能为空'
	                    }
	                }
	            },
	            content: {
	                validators: {
	                    notEmpty: {
	                        message: '姓名不能为空'
	                    }
	                }
	            },
	            'send': {
                 validators: {
                     choice: {
                        min: 1,
                         max: 3,
                         message: '请选择下发方式'
                     }
                 }
             }
	        }
	    });
	});
	$('#addSave').click(function(){
		var addValidator = $('#add_form').data('bootstrapValidator');
		addValidator.validate();
		var title=$('#notice_info #add_form .form-group .project_title').val();
		var send_type=$('#notice_info #add_form .form-group .send_type  input[type="radio"]:checked').val();
		var content=$('#notice_info #add_form .form-group .content').val();
		if (addValidator.isValid()){
			$.ajax({
				type:"post",
				url:"/notice_info_manages.json",
				data:{
					d_notice_info:{
						title:title,
						content:content,
						send_type:send_type
					}
						
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#addModal').modal('hide')
   					$('#add_form').data('bootstrapValidator').resetForm(true);
				},
				error:function(err){
					console.log(err)
				}
			});
		}
	})
	//修改
	var id;
	$('body').delegate('#notice_info table tbody tr td .work_edit','click',function(){
		id=$(this).parents('tr').find('.id').text();
		console.log(id)
		$.ajax({
			type:"get",
			url:'/notice_info_manages/'+id+'.json',
			success:function(data){
			var title=data.d_notice_info.title;
			var content=data.d_notice_info.content;
			$('#edit_form .form-group .project_title').val(title)
			$('#edit_form .form-group .content').val(content)
			},
			error:function(err){
				console.log(err)
			}
		});
	})
		$(document).ready(function() {
	    $('#editModal').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            title: {
	                validators: {
	                    notEmpty: {
	                        message: '姓名不能为空'
	                    }
	                }
	            },
	            content: {
	                validators: {
	                    notEmpty: {
	                        message: '姓名不能为空'
	                    }
	                }
	            },
	            'send': {
                 validators: {
                     choice: {
                        min: 1,
                         max: 3,
                         message: '请选择下发方式'
                     }
                 }
             }
	        }
	    });
	});
	$('#editSave').click(function(){
		var editValidator = $('#editModal').data('bootstrapValidator');
		editValidator.validate();
		var title=$('#notice_info #edit_form .form-group .project_title').val();
		var send_type=$('#notice_info #edit_form .form-group .send_type  input[type="radio"]:checked').val();
		var content=$('#notice_info #edit_form .form-group .content').val();
		if (editValidator.isValid()){
			$.ajax({
				type:"put",
				url:'/notice_info_manages/'+id+'.json',
				data:{
					d_notice_info:{
						title:title,
						content:content,
						send_type:send_type
					}
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#editModal').modal('hide')
				},
				error:function(err){
					console.log(err)
				}
			});
		}
		
	})
		//删除
	$("body").delegate("#notice_info table tbody tr td .work_delete","click",function() {
		$('.delete_prop').hide();
		$(this).parent().children('div').show();	
	})
	$('body').delegate('#notice_info table tbody tr td .delete_prop .btn-sure','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"delete",
			url:'/notice_info_manages/'+id+'.json',
			success:function(data){
				page_state = "change";
				originData();
				$('.delete_prop').hide();
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#notice_info table tbody tr td .delete_prop .btn-cancel','click',function(){
		$('.delete_prop').hide();
	})
	
//	发送


var sendId;
$('body').delegate('.sends','click',function(){
	var sendTypes=$(this).parents('tr').find('.sendTypes span').text();
	
	 sendId=$(this).parents('tr').find('.id').text();
	 $.ajax({
	 	type:"get",
	 	url:'/notice_info_manages/'+sendId+'.json',
	 	success:function(data){
	 		console.log(data)
	 		$.ajax({
				type:"get",
				url:"/notice_info_manages/get_all_groups.json",
				success:function(data){
					var unitTemp = template('sendPers',data);
					$('#send_per').html(unitTemp);
					$('#sendModal .modal-body .tabTypes').val(sendTypes)
				},
				error:function(err){
					console.log(err)
				}
			});
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})	
$('body').delegate('#paiSave','click',function(){
//	$('.loading').show()
	var unit_id=$('#sendModal #send_per .unit_user input[name="pers"]:checked').parents('.unit_user').siblings('.unit_name').find('.unit_id').text();
	var send_type=$('#sendModal .modal-body .tabTypes').val();
	console.log(send_type)
	var ids=[];
	var id=$('#sendModal #send_per .unit_user input[type="checkbox"]');
	for(var i=0;i<id.length;i++){
		if(id[i].checked){
			ids.push($(id[i]).val());
		}
	}
	var b = ids.join(",");
	console.log(ids)
	console.log(b)
	$.ajax({
	 	type:"get",
	 	url:'/notice_info_manages/'+sendId+'/send_message.json',

	 	data:{
			worker_ids:b,
			send_type:send_type
		},
	 	success:function(data){
	 		page_state = "change";
	 		originData();
	 		$('.loading').hide()
	 		$('#sendModal').modal('hide');
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})	
	
	$.ajax({
		type:"get",
		url:"/notice_info_manages.json",
		success:function(data){
			var sendTemp = template('temCont',data);
			$('#addModal .send_content').html(sendTemp);
			$('#editModal .send_content').html(sendTemp);
		},
		error:function(err){
			console.log(err)
		}
	});
	$('#addModal #sendApp').click(function(){
		$('#addModal .template_type').show();
		$('#addModal .template_type .appList').show();
		$('#addModal .template_type .smsList').hide();
		$('#addModal .template_type .emailList').hide();
	})
	$('#addModal #sendSms').click(function(){
		$('#addModal .template_type').show();
		$('#addModal .template_type .appList').hide();
		$('#addModal .template_type .smsList').show();
		$('#addModal .template_type .emailList').hide();
	})
	$('#addModal #sendEmail').click(function(){
		$('#addModal .template_type').show();
		$('#addModal .template_type .appList').hide();
		$('#addModal .template_type .smsList').hide();
		$('#addModal .template_type .emailList').show();
	})
	
	$('body').delegate('#addModal .template_type .send_content input[type="radio"]','click',function(){
		var texts=$(this).siblings('span').text();
		$('#addModal .form-group .content').val(texts)
	})
	$('#editModal #sendApp').click(function(){
		$('#editModal .template_type').show();
		$('#editModal .template_type .appList').show();
		$('#editModal .template_type .smsList').hide();
		$('#editModal .template_type .emailList').hide();
	})
	$('#editModal #sendSms').click(function(){
		$('#editModal .template_type').show();
		$('#editModal .template_type .appList').hide();
		$('#editModal .template_type .smsList').show();
		$('#editModal .template_type .emailList').hide();
	})
	$('#editModal #sendEmail').click(function(){
		$('#editModal .template_type').show();
		$('#editModal .template_type .appList').hide();
		$('#editModal .template_type .smsList').hide();
		$('#editModal .template_type .emailList').show();
	})
	
	$('body').delegate('#editModal .template_type .send_content input[type="radio"]','click',function(){
		var texts=$(this).siblings('span').text();
		$('#editModal .form-group .content').val(texts)
	})
	//	分页+初始数据
	function originData(param){
//		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('infoTables',data_hash);
			$('#notice_info table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/notice_info_manages.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('infoTables',data);
					$('#notice_info table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
					$('#notice_info .public_table .status').each(function(i){
				    	var s=$(this).text()
							if(s=="已发送"){
								$(this).siblings('td').find('.sends').hide()
							}
				    	})
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/notice_info_manages.json').done(function(data){
		console.log(data)
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	    $('#notice_info .public_table .status').each(function(i){
	    	var s=$(this).text()
				if(s=="已发送"){
					$(this).siblings('td').find('.sends').hide()
				}
	    })
	})
})
