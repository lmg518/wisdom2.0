$(function() {
	//  派遣
//$.getJSON('/search_unit_manys').done(function(data){
//		var unitTemp = template('sendPers',data);
//		$(' #send_per').html(unitTemp);
//	})
var id;
$('body').delegate('.paiq','click',function(){
	
	 id=$('#stationModal #pId').text();
	 $.ajax({
	 	type:"get",
	 	url:'/pending_works/'+id+'.json',
	 	success:function(data){
	 		console.log(data)
	 		var regionId=data.s_region_code_info;
	 		console.log(regionId)
	 		$.ajax({
	 			type:"get",
	 			url:"/search_unit_manys.json?s_region_code_info_id="+regionId,
	 			success:function(data){
	 				var unitTemp = template('sendPers',data);
					$(' #send_per').html(unitTemp);
	 			}
	 		});
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
$('body').delegate('#paiSave','click',function(){
	$('.loading').show()
	var unit_id=$('#sendModal #send_per .unit_user input[name="pers"]:checked').parents('.unit_user').siblings('.unit_name').find('.unit_id').text();
	var handle_man_id=$('#sendModal #send_per .unit_user input[name="pers"]:checked').val();
	var handle_man=$('#sendModal #send_per .unit_user input[name="pers"]:checked').next('span').text();
	$.ajax({
	 	type:"PUT",
	 	url:'/search_unit_manys/'+id+'.json',
	 	data:{
			unit_id:unit_id,
			handle_man_id:handle_man_id,
			handle_man:handle_man
		},
	 	success:function(data){
	 		page_state = "change";
	   		var created_time = $('#pending_works .public_search .create_datetime').val();
        	var end_time = $('#pending_works .public_search .end_datetime').val();
	        $.ajax({
	            type: 'get',
	            url: "/pending_works.json?created_time=" + created_time + "&end_time=" + end_time + "&polling_time=" + created_time,
	            success: function(data) {
	                var works_size = data.works_size;
	                var wait_deal = data.wait_deal;
	                var un_write = data.un_write;
	                var un_audit = data.un_audit;
	                var over_time = data.over_time;
	                var deal_with = data.deal_with;
	
	                var htmlData = template('searchTime', data);
	                $('#pending_works #day_list').html(htmlData);
	
	                var stationData = template('stationChild', data);
	                $('#pending_works #station_list').html(stationData);
	
	                $('#pending_works #total .works_size .num').text(works_size)
	                $('#pending_works #total .undis .num').text(un_write)
	                $('#pending_works #total .allocated .num').text(wait_deal)
	                $('#pending_works #total .rough .num').text(deal_with)
	                $('#pending_works #total .stocks .num').text(un_audit)
	                $('#pending_works #total .overtime .num').text(over_time)
	                stations();
	
	                //				$('#pending_works #day_list .list_top').each(function(i){
	                //					var times=$(this).text().toString();
	                //					var newTimes=new Date().Format("yyyy-MM-dd");
	                //					if(times==newTimes){
	                //						$(this).addClass('activeTime')
	                //						return false;
	                //					}
	                //				})
	            },
	            error: function(err) {
	                console.log(err)
	            }
	        })
	 		$('.loading').hide()
	 		$('#sendModal').modal('hide');
	 		$('#stationModal').modal('hide');
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
	var id;
	$('body').delegate('#pending_works #station_list .station','click',function(){
		id=$(this).val();
		var staName=$(this).text();
		$.ajax({
			type:"get",
			url:'/pending_works/'+id+'.json',
			success:function(data){
				var stationHtml = template('station_table', data);
                $('#stationModal #public_table .exception-list').html(stationHtml);
                $('#stationModal #myModalLabel').text(staName)
                $('#stationModal .delId').text(id)
                $('#stationModal #pId').text(id)
                $('#stationModal #public_table .exception-list .status').each(function(){
                	var st=$(this).text();
                	if(st=="已分配"){
                		$(this).parents('.modal-body').find('.paiq').hide();
                	}else{
                		$(this).parents('.modal-body').find('.paiq').show();
                	}
                })
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	$('body').delegate('#stationModal .lookFor','click',function(){
		$('#stationModal').modal('hide');
		$('#pending_works').hide();
		var id=$(this).children('span').text();
		$.ajax({
			type:"get",
			url:'/pending_works/'+id+'.json',
			success:function(data){
				var detailHtml = template('detail', data);
                $('#detail_look').html(detailHtml);
                $('#detail_look').show();
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	$('body').delegate('#detail_look .saveback','click',function(){
		$('#detail_look').hide();
		$('#pending_works').show();
	})
    //查询
    $('.public_search .search').click(function() {
   		var created_time = $('#pending_works .public_search .create_datetime').val();
        var end_time = $('#pending_works .public_search .end_datetime').val();
        $.ajax({
            type: 'get',
            url: "/pending_works.json?created_time=" + created_time + "&end_time=" + end_time + "&polling_time=" + created_time,
            success: function(data) {
            	console.log(data)
                var works_size = data.works_size;
                var wait_deal = data.wait_deal;
                var un_write = data.un_write;
                var un_audit = data.un_audit;
                var over_time = data.over_time;
                var deal_with = data.deal_with;

                var htmlData = template('searchTime', data);
                $('#pending_works #day_list').html(htmlData);

                var stationData = template('stationChild', data);
                $('#pending_works #station_list').html(stationData);
					if(works_size){
						$('#pending_works #total .works_size .num').text(works_size)
						
					}else{
						$('#pending_works #total .works_size .num').text("0")	
					}
               		if(un_write){
               			$('#pending_works #total .undis .num').text(un_write)	
               		}else{
               			$('#pending_works #total .undis .num').text("0")
               		}
                	if(wait_deal){
                		$('#pending_works #total .allocated .num').text(wait_deal)
                	}else{
                		$('#pending_works #total .allocated .num').text("0")
                	}
                	if(deal_with){
                		$('#pending_works #total .rough .num').text(deal_with)
                	}else{
                		$('#pending_works #total .rough .num').text("0")
                	}
                	if(un_audit){
                		$('#pending_works #total .stocks .num').text(un_audit)
                	}else{
                		$('#pending_works #total .stocks .num').text("0")
                	}
                	if(over_time){
                		$('#pending_works #total .overtime .num').text(over_time)
                	}else{
                		$('#pending_works #total .overtime .num').text("0")
                	}
                
                
                stations();

                //				$('#pending_works #day_list .list_top').each(function(i){
                //					var times=$(this).text().toString();
                //					var newTimes=new Date().Format("yyyy-MM-dd");
                //					if(times==newTimes){
                //						$(this).addClass('activeTime')
                //						return false;
                //					}
                //				})
            },
            error: function(err) {
                console.log(err)
            }
        })
    })
    function startTime(){
    	var created_time = $('#pending_works .public_search .create_datetime').val();
        var end_time = $('#pending_works .public_search .end_datetime').val();
        $.ajax({
            type: 'get',
            url: "/pending_works.json?created_time=" + created_time + "&end_time=" + end_time + "&polling_time=" + created_time,
            success: function(data) {
                var works_size = data.works_size;
                var wait_deal = data.wait_deal;
                var un_write = data.un_write;
                var un_audit = data.un_audit;
                var over_time = data.over_time;
                var deal_with = data.deal_with;

                var htmlData = template('searchTime', data);
                $('#pending_works #day_list').html(htmlData);

                var stationData = template('stationChild', data);
                $('#pending_works #station_list').html(stationData);

                if(works_size){
						$('#pending_works #total .works_size .num').text(works_size)
						
					}else{
						$('#pending_works #total .works_size .num').text("0")	
					}
               		if(un_write){
               			$('#pending_works #total .undis .num').text(un_write)	
               		}else{
               			$('#pending_works #total .undis .num').text("0")
               		}
                	if(wait_deal){
                		$('#pending_works #total .allocated .num').text(wait_deal)
                	}else{
                		$('#pending_works #total .allocated .num').text("0")
                	}
                	if(deal_with){
                		$('#pending_works #total .rough .num').text(deal_with)
                	}else{
                		$('#pending_works #total .rough .num').text("0")
                	}
                	if(un_audit){
                		$('#pending_works #total .stocks .num').text(un_audit)
                	}else{
                		$('#pending_works #total .stocks .num').text("0")
                	}
                	if(over_time){
                		$('#pending_works #total .overtime .num').text(over_time)
                	}else{
                		$('#pending_works #total .overtime .num').text("0")
                	}
                stations();

                //				$('#pending_works #day_list .list_top').each(function(i){
                //					var times=$(this).text().toString();
                //					var newTimes=new Date().Format("yyyy-MM-dd");
                //					if(times==newTimes){
                //						$(this).addClass('activeTime')
                //						return false;
                //					}
                //				})
            },
            error: function(err) {
                console.log(err)
            }
        })
    }
 
    $('body').delegate('#day_list .list_top', 'click', function() {
        $(this).addClass('activeTime')
  		$(this).parent('li').siblings().find('.list_top').removeClass('activeTime'); 
        var created_time = $('#pending_works .public_search .create_datetime').val();
        var end_time = $('#pending_works .public_search .end_datetime').val();
        var polling_time = $(this).text();
        $.ajax({
        	type:"get",
        	url:"/pending_works.json?created_time=" + created_time + "&end_time=" + end_time + "&polling_time=" + polling_time,
        	success:function(data){
				var stationData = template('stationChild', data);
                $('#pending_works #station_list').html(stationData);
                stations();
        	},
        	error:function(err){
        		console.log(err)
        	}
        });

    })
	$('body').delegate('#day_list .list_child .status', 'click', function() {
		
		$(this).parents('li').find('.list_top').addClass('activeTime');
		$(this).parents('li').siblings().find('.list_top').removeClass('activeTime');
		var created_time = $('#pending_works .public_search .create_datetime').val();
        var end_time = $('#pending_works .public_search .end_datetime').val();
        var polling_time=$(this).parents('.list_child').siblings('.list_top').text();
        var job_status=$(this).attr("value");
        $.ajax({
            type: 'get',
            url: "/pending_works.json?created_time=" + created_time + "&end_time=" + end_time + "&polling_time=" + polling_time+"&job_status="+job_status,
            success: function(data) {
            
                var stationData = template('stationChild', data);
                $('#pending_works #station_list').html(stationData);
                stations();
            },
            error: function(err) {
                console.log(err)
            }
        })
	})
	$('body').delegate('#day_list .list_child .over_time', 'click', function() {
		$(this).parents('li').find('.list_top').addClass('activeTime');
		$(this).parents('li').siblings().find('.list_top').removeClass('activeTime');
		var created_time = $('#pending_works .public_search .create_datetime').val();
        var end_time = $('#pending_works .public_search .end_datetime').val();
        var polling_time=$(this).parents('.list_child').siblings('.list_top').text();
		var overTimes=new Date().Format("yyyy-MM-dd hh:mm:ss");
        $.ajax({
            type: 'get',
            url: "/pending_works.json?created_time=" + created_time + "&end_time=" + end_time + "&polling_time=" + polling_time+"&over_time="+overTimes,
            success: function(data) {
                var stationData = template('stationChild', data);
                $('#pending_works #station_list').html(stationData);
                stations();
            },
            error: function(err) {
                console.log(err)
            }
        })		
	})
    var cells = document.getElementById('monitor').getElementsByTagName('td');
    var clen = cells.length;
    var currentFirstDate;

    function formatDate(date) {
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var newMonths = month < 10 ? '0' + month : month;
        var newDates = day < 10 ? '0' + day : day;
        return year + "-" + newMonths + "-" + newDates + " ";

    }

    function addDate(date, n) {
        date.setDate(date.getDate() + n);
        return date;
    }

    function setDate(date) {
        var week = date.getDay() - 1;
        date = addDate(date, week * -1);
        currentFirstDate = new Date(date);

        for (var i = 0; i < clen; i++) {
            cells[i].innerHTML = formatDate(i == 0 ? date : addDate(date, 1));
        }
    }
    setDate(new Date());
    var createTime = $('#monitor .firstTime').text();
    var endTime = $('#monitor .listTime').text();
    $('#pending_works .public_search .create_datetime').val(createTime);
    $('#pending_works .public_search .end_datetime').val(endTime);

    var startTime = $('#pending_works .public_search .create_datetime').val();
    var endTime = $('#pending_works .public_search .end_datetime').val();
    var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates + " ";
    $('.loading').show()
    var data_hash = {};
    var page_state = "init";
    $.ajax({
        type: 'get',
        url: "/pending_works.json?created_time=" + startTime + "&end_time=" + endTime + "&polling_time=" + datestamp,
        success: function(data) {
            var works_size = data.works_size;
            var wait_deal = data.wait_deal;
            var un_write = data.un_write;
            var un_audit = data.un_audit;
            var over_time = data.over_time;
            var deal_with = data.deal_with;

            var htmlData = template('searchTime', data);
            $('#pending_works #day_list').html(htmlData);
            var stationData = template('stationChild', data);
            $('#pending_works #station_list').html(stationData);

            if(works_size){
						$('#pending_works #total .works_size .num').text(works_size)
						
					}else{
						$('#pending_works #total .works_size .num').text("0")	
					}
               		if(un_write){
               			$('#pending_works #total .undis .num').text(un_write)	
               		}else{
               			$('#pending_works #total .undis .num').text("0")
               		}
                	if(wait_deal){
                		$('#pending_works #total .allocated .num').text(wait_deal)
                	}else{
                		$('#pending_works #total .allocated .num').text("0")
                	}
                	if(deal_with){
                		$('#pending_works #total .rough .num').text(deal_with)
                	}else{
                		$('#pending_works #total .rough .num').text("0")
                	}
                	if(un_audit){
                		$('#pending_works #total .stocks .num').text(un_audit)
                	}else{
                		$('#pending_works #total .stocks .num').text("0")
                	}
                	if(over_time){
                		$('#pending_works #total .overtime .num').text(over_time)
                	}else{
                		$('#pending_works #total .overtime .num').text("0")
                	}
			stations();
  			$('#pending_works #day_list .list_top').each(function(i){
  				var times=$(this).text().toString();
  				var newTimes=new Date().Format("yyyy-MM-dd");
  				if(times==newTimes){
  					$(this).addClass('activeTime')
  					return false;
  				}
  			})
        },
        error: function(err) {
            console.log(err)
        }
    })
    Date.prototype.Format = function(fmt) {
        var o = {
            "M+": this.getMonth() + 1,
            //月份 
            "d+": this.getDate(),
            //日 
            "h+": this.getHours(),
            //小时 
            "m+": this.getMinutes(),
            //分 
            "s+": this.getSeconds(),
            //秒 
            "q+": Math.floor((this.getMonth() + 3) / 3),
            //季度 
            "S": this.getMilliseconds() //毫秒 
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    }
    
    
	function stations(){
		$('#pending_works #station_list .station').each(function(){
	    	var status=$(this).attr('title')
	    	if(status=="un_write"){
    			$(this).css({ "color": "#fff", "background": "#f19c77" })
	    	}
	    	if(status=="wait_deal"){
	    		$(this).css({ "color": "#fff", "background": "#7fcff4" })
	    	}
	    	if(status=="un_audit"){
	    		$(this).css({ "color": "#fff", "background": "#aab2e0" })
	    	}
	    	if(status=="deal_with"){
	    		$(this).css({ "color": "#fff", "background": "#87d66d" })
	    	}
	    	if(status=="over_time"){
	    		$(this).css({ "color": "#fff", "background": "#f29ec2" })
	    	}
	    	
	    })
	}
    
})