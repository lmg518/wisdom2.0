$(function(){
	
		//	日期
	var time = new Date();
	var year = time.getFullYear();
	var month = time.getMonth()+1;
	var day=time.getDate();
	var hour= time.getHours();
	var result = year+'-'+(month<10?'0'+month:month)+'-'+(day<10?'0'+day:day)+' '+(hour<10?'0'+hour:hour)+':00';
//	$('.dateTimepickerH').val(result)
	$('.dateTimepickerH').datetimepicker({
		format:"Y-m-d H:i",
//		minDate:year+'-01-01',
//		maxDate: year+'-12-31',
		todayButton:true,
		step:5
	});
	
	
    var data_init = originData();
//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	$.ajax({
		type:"get",
		url:"/all_stations_area.json",
		success:function(data){
			var stationTemp = template('Addstations',data);
		$('#sq_radioStation .station_contents').html(stationTemp);
	
		},
		error:function(err){
			console.log(err)
		}
	});
    $.getJSON('/search_region_codes').done(function(data){
		var stationTemp = template('editUnit',data);
		$('#search-unit .choose-site').html(stationTemp);
	})
    $.getJSON('/search_region_codes').done(function(data){
		var stationTemp = template('editUnits',data);
		$('#search-units .choose-sites').html(stationTemp);
	})
     //来源/异常
    $.getJSON('/power_cut_records').done(function(data){
		var originsTemp = template('orangeList',data);
		$('#powerCutRecordsOther .tablePower .otherTd .orange').html(originsTemp);
		var abnormalsTemp=template('abnormalsList',data);
		$('#powerCutRecordsOther .tablePower .otherTd .exception_name').html(abnormalsTemp);
	})
     

////	查询	
	var unit_yw = '';
	var station_id = '';
	var status;
	var origions='';
	var begintime = '';
	var endtime = '';
	var audit_des='';
	function pageName_Data(){
		$('#powerCutRecords .public_search .searchSta').val($('#powerCutRecords .control_stationIDs').val())
		if($('#powerCutRecords .public_search .searchSta').val()){
            station_id = $('#powerCutRecords .public_search .searchSta').val().split(',')
        }
		
		
		var searchDes=$('.public_search #audit_des option:selected').val();
		$('#powerCutRecords .public_search .searchDes').val(searchDes)
		var searchStatus=$('.public_search .status option:selected').val();
		$('#powerCutRecords .public_search .searchStatus').val(searchStatus)
		var searchOrigins= $('.origins option:selected').val();
		$('#powerCutRecords .public_search .searchOrigins').val(searchOrigins)
		$('#powerCutRecords .public_search .searchUnit').val($('.public_search .unit_yw').val());
		$('#powerCutRecords .public_search .startTime1').val($('.create_datetime').val());
		$('#powerCutRecords .public_search .endTime1').val($('.end_datetime').val());
		audit_des=$('#powerCutRecords .public_search .searchDes').val();
		status = $('#powerCutRecords .public_search .searchStatus').val();
		origions =$('#powerCutRecords .public_search .searchOrigins').val();
		unit_yw = $('#powerCutRecords .public_search .searchUnit').val();
		begintime = $('#powerCutRecords .public_search .startTime1').val()
		endtime = $('#powerCutRecords .public_search .endTime1').val()
		if(begintime && endtime){
			begintime=begintime;
			endtime=endtime;
		}else if(begintime){
			endtime = begintime
		}else if(endtime){
			begintime = endtime
		}else{
			begintime = ''
	        endtime = ''
		}
	}
	
	$('.public_search .search').click(function(){
		unit_yw = '';
	    station_id = '';
	    status = '';
	    origions = ''
	    begintime = ''
	    endtime = ''
	    audit_des=''
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/power_cut_records.json',
			data:{
				station_id:station_id,
				job_status:status,
				unit_yw:unit_yw,
				created_at:begintime,
				end_at:endtime,
				origin:origions,
				audit_des:audit_des
			},
			success:function(data){
				
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
		   		$('.page_total small').text(data.list_size);
		   		$('#powerCutRecords table .job_status').each(function(){
		    	var s=$(this).text()
		    	if(s=="待审核"){
					$(this).siblings().find('.editFor').hide()
					$(this).siblings().find('#editDone').hide()
				}else if(s=="审核通过"){
					$(this).siblings().find('.editFor').hide()
					$(this).siblings().find('#editDone').hide()
					$(this).siblings().find('.auditFor').hide()
				}
				if(s=="待上报"){
					$(this).siblings().find('.auditFor').hide()
					
				}
				if(s=="审核不通过"){
					$(this).siblings().find('.auditFor').hide()
				}
				    
		    })
			},
			error:function(err){
				console.log(err)
			}
		})
	})
    
    
    
    
    $('body').delegate('.add_power','click',function(){
    	$('#powerCutRecords').hide()
    	$('#powerCutRecordsOther').show()
    })
//	点击添加
       $('body').delegate('#powerCreateSub','click',function(){
         
       	var station_name=$('#powerCutRecordsOther td #addControl_stations').val();
       	var d_station_id=$('#powerCutRecordsOther td #addControl_stationIDs').val();
   		var unit_yw=$('#powerCutRecordsOther  td .unit_yws').val();
   		var start_datetime = $('#powerCutRecordsOther  td .start_datetime').val();
   		var end_datetime = $('#powerCutRecordsOther  td .end_datetime').val();
// 		var time_range = $('#powerCutRecordsOther  td .time_range').val();
   		var exception_name = $('#powerCutRecordsOther  td .exception_name option:selected').text();
           var handle_man = $('#powerCutRecordsOther  td .handle_man').val();
           var orange = $('#powerCutRecordsOther  td .orange option:selected').text();
           var img_path = $('#powerCutRecordsOther  td #prvid img').attr("src");
           var note = $('#powerCutRecordsOther  td textarea').val();
           if($('#powerCutRecordsOther .exception_device_type  input[type="checkbox"]:checked')){
               var text = []
               $('.exception_device_type input[type="checkbox"]:checked').next('label').each(function(){
                   var news = $(this).text()
                    text.push(news)
               })
               var allText = []
               allText=text.join(',')
           }
		   var exception_device_type = allText;
		   var imgIf=$('#powerCutRecordsOther #prvid').html();
			  if(imgIf==null||imgIf.length==0){
				alert("请上传凭证")
			  }else{
				 $.ajax({
					type:"POST",
					url:"/power_cut_records.json",
					async:true,
					data:{
						d_power_cut_record:{
							station_name:station_name,
							unit_yw:unit_yw,
							begin_time:start_datetime,
							end_time:end_datetime,
							d_station_id:d_station_id,
							exception_device_type:exception_device_type,
							exception_name:exception_name,
							handle_man:handle_man,
							origin:orange,
							note:note,
							image_file:img_path
						}
					},
					success:function(data){
						console.log(data)
						page_state = "change";
						originData()
						$('#powerCutRecords').show()
						$('#powerCutRecordsOther').hide()
						$('#powerCutRecordsOther table input[type="text"]').val(" ");
						$('#powerCutRecordsOther table textarea').val(" ");
						
					},
					error:function(err){
						alert("添加失败")
						console.log(err);
					}
				}); 
			}

   	})
	$('body').delegate('#showBack','click',function(){
        $('#powerCutRecords').show()
    	$('#powerCutRecordsOther').hide()
    	$('#powerCutRecordsOthers').hide()
    	$('#editPowers').hide()
    	$('#auditPowers').hide();
	})
    $('body').delegate('#saveBack','click',function(){
        $('#powerCutRecords').show()
    	$('#powerCutRecordsOther').hide()
    	$('#powerCutRecordsOthers').hide()
    	$('#editPowers').hide()
    	$('#powerCutRecordsOther table td input').val("");
	})
    $('body').delegate('#editPowers #editBack','click',function(){
    	$('#powerCutRecords').show()
    	$('#powerCutRecordsOther').hide()
    	$('#powerCutRecordsOthers').hide()
    	$('#editPowers').hide()
    })
    $('body').delegate('#powerCutRecords .public_table .picture_max .checkImg','click',function(){
    	$(this).siblings('.showImg').show();
    })
    $('body').delegate('#powerCutRecords .public_table .picture_max .close','click',function(){
    	$('.showImg').hide();
    })
    
    //点击查看时
    $('body').delegate('.lookFor','click',function(){
    	var exception_device_type,exception_device_typeName=[];
    	$('#powerCutRecords').hide()
    	$('#powerCutRecordsOthers').show()
    	$('#powerCutRecordsOther').hide()
    	station_two=[]
        var id = $(this).parent().siblings('td').find('.hide').text()
        $.ajax({
		type:'get',
		url:'/power_cut_records/'+id + '.json',
		success:function(data){
			var dataObj={
					editData:data
				}
			var els=data.powerCutRecord.exception_device_type;
			var arr=els.split(',');
			console.log(arr)
			
//			$('.exception_device_type span').each(function(i){
//				console.log(i)
//				var labelText = $(this).find('label').text();
//				console.log(labelText)
//				$.each(arr, function(n, v){
//					console.log(v)
//					if(v == labelText){
//		            	$(this).find('input').prop('checked' , true);
//		            }
//				})
//			})
//			exception_device_type = data.powerCutRecord
//			exception_device_typeName.push(exception_device_type.exception_device_type)
//			var labelText;
//			var els
//				exception_device_typeName.map(function(el,i,arr){
//					els = el
//					console.log('els==',els)
//					$('.exception_device_type span').each(function(index,ele){
////				        labelText = $(this).find('label').text()
////				        console.log(els)
////				        console.log(labelText)
//						var arr=els.split(',');
//						console.log(arr)

//						console.log(labelText)
//						if(els == labelText){
//							alert('111')
//		                    $(this).find('input').checked = true
//					    }
//					
//				})
//
//			})
			
			var originData = template('powerCutRecordNews',dataObj)
			$('#powerCutRecordsOthers').html(originData)
//			$('#powerCutRecordsOthers img').zoomify();
			var imgs = document.getElementsByTagName("img");
			imgs[0].focus();
			for(var i = 0;i<imgs.length;i++){
				imgs[i].onclick = expandPhoto;
				imgs[i].onkeydown = expandPhoto;
			}
		},
		error:function(err){
			console.log(err)
		}
	    })
	})
//  放大图片
function expandPhoto(){
	var overlay = document.createElement("div");
	overlay.setAttribute("id","overlay");
	overlay.setAttribute("class","overlay");
	document.body.appendChild(overlay);

	var img = document.createElement("img");
	img.setAttribute("class","overlayimg");
	img.src = this.getAttribute("src");
	document.getElementById("overlay").appendChild(img);

	img.onclick = restore;
}
function restore(){
	document.body.removeChild(document.getElementById("overlay"));
	document.body.removeChild(document.getElementById("img"));
}
    //编辑
    var id;
    $('body').delegate('.editFor','click',function(){
    	$('#powerCutRecords').hide()
    	$('#powerCutRecordsOthers').hide()
    	$('#powerCutRecordsOther').hide()
    	$('#editPowers').show();
		
    	id = $(this).parent().siblings('td').find('.hide').text()
    	$.ajax({
			type:'get',
			url:'/power_cut_records/'+id + '/edit.json',
			success:function(data){
				var dataObj={
					editData:data
				}
				var originData = template('edit_powers',dataObj)
				$('#editPowers').html(originData)
//				var img=$('#editPowers td #prvid1 img').attr("src");
//		    	function getBase64Image(img) {
//				    var canvas = document.createElement("canvas");
//				    canvas.width = img.width;
//				    canvas.height = img.height;
//				    var ctx = canvas.getContext("2d");
//				    ctx.drawImage(img, 0, 0, img.width, img.height);
//				    var ext = img.src.substring(img.src.lastIndexOf(".")+1).toLowerCase();
//				    var dataURL = canvas.toDataURL("image/"+ext);
//				    return dataURL;
//				}
//				var image = new Image();
//				image.src = img;
//				$(image).load(function(){
//					var base64 = getBase64Image(image);
//				    $('#editPowers td #prvid1 img').attr("src",base64);
//				})
			},
			error:function(err){
				console.log(err)
			}
		})
    })
    //编辑保存
    $('body').delegate('#editSave', 'click',function(){
    	var C=$(this).val();
       	var exception_name = $('#editPowers td .exception_names option:selected').text();
       	var orange = $('#editPowers td .origins option:selected').text();
        var img_path = $('#editPowers td #prvid1 img').attr("src");
        var note = $('#editPowers td textarea').val();
        var station_name=$('#editPowers td .control_station').val();
        var d_station_id=$('#editPowers td .control_stationID').val();
    	$.ajax({
    		type:"put",
    		url:'/power_cut_records/'+id + '.json',
    		data:{
    			d_power_cut_record:{
    				station_name:station_name,
    				d_station_id:d_station_id,
   					exception_name:exception_name,
   					origin:orange,
   					note:note,
   					image_file:img_path,
   					job_status:C
   				}
    		},
    		success:function(data){
    			alert("提交成功")
   				page_state = "change";
   				originData()
   				$('#powerCutRecords').show()
   				$('#powerCutRecordsOther').hide()
   				$('#editPowers').hide();
   				
   			},
   			error:function(err){
   				alert("提交失败")
   				console.log(err);
   			}
    	});
    })
	//完成
	$('body').delegate('#editDone', 'click',function(){
		var Y=$(this).val();
		id = $(this).parent().siblings('td').find('.hide').text()
			$.ajax({
    		type:"get",
    		url:'/power_cut_records/'+id + '/report.json',
    		data:{
    			d_power_cut_record:{
   					job_status:Y
   				}
    		},
    		success:function(data){
    			alert("提交成功")
   				page_state = "change";
   				originData()
   				$('#powerCutRecords').show()
   				$('#powerCutRecordsOther').hide()
   				$('#auditPowers').hide();
   				$('#editPowers').hide();
   				
   			},
   			error:function(err){
   				alert("提交失败")
   				console.log(err);
   			}
    	});
	})
	$('body').delegate('#audit_N','click',function(){
		$('#auditPowers #audit_text').show();
	})
	$('body').delegate('#audit_w','click',function(){
		$('#auditPowers #audit_text').hide();
	})
	//审核
	 $('body').delegate('.auditFor','click',function(){
	 	$('#powerCutRecords').hide()
    	$('#powerCutRecordsOthers').hide()
    	$('#powerCutRecordsOther').hide()
    	$('#editPowers').hide()
    	$('#auditPowers').show();
	 	id = $(this).parent().siblings('td').find('.hide').text()
	 	$.ajax({
	 		type:"get",
	 		url:'/power_cut_records/'+id + '.json',
	 		success:function(data){
	 		var dataObj={
					editData:data
				}
	 		var auditHtml = template('powerAudit',dataObj)
			$('#auditPowers').html(auditHtml)
			var imgs = document.getElementsByTagName("img");
			imgs[0].focus();
			for(var i = 0;i<imgs.length;i++){
				imgs[i].onclick = expandPhoto;
				imgs[i].onkeydown = expandPhoto;
			}
	 		},
	 		error:function(err){
	 			console.log(err)
	 		}
	 	});
	 })
	 $('body').delegate('#auditSave','click',function(){
	 	var audit_if=$('#auditPowers #audit_adopt .checkbox-radio input[name="Approve"]:checked').val();
		var audit_des=$('#auditPowers #audit_des option:selected').val();
		$.ajax({
			type:"get",
			url:'/power_cut_records/'+id + '/power_audit.json',
			data:{
				d_power_cut_record:{
					audit_if:audit_if,
					audit_des:audit_des
				}
			},
			success:function(data){
				page_state = "change";
				alert("提交成功");
				originData();
				$('#powerCutRecords').show()
   				$('#powerCutRecordsOther').hide()
   				$('#auditPowers').hide();
   				$('#editPowers').hide();
			},
			error:function(err){
				console.log(err)
			}
		});
	 })
	
		$.ajax({
			type:"get",
			url:"/power_cut_records/get_all_msgs.json",
			success:function(data){
				var unitTemp = template('unitList',data);
				$('#unitModal #unit_list').html(unitTemp);
			},
			error:function(err){
				console.log(err)
			}
		});

	
	$('body').delegate('#unitModal .modal-footer .btn-sure','click',function(){
		var unitName=$('#unitModal  #unit_list input[type="radio"]:checked').siblings('span').text();
		var unitId=$('#unitModal  #unit_list input[type="radio"]:checked').val();
		$('#operation .handle_man').val(unitName)
		$('#operation .unitId').val(unitId)
		$('#unitModal').modal('hide')
	})
	
	
	//分页
    function originData(param){
//	    console.log("sss")
//		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originDatas = template('powerCutRecord',data_hash)
			$('.exception-list').html(originDatas);
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var station_ids = $('#powerCutRecords .public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
			$.ajax({
				url: '/power_cut_records.json?page=' + pp,
		    	type: 'get',
		    	data:{
					station_id:station_ids,
					job_status:$('#powerCutRecords .public_search .searchStatus').val(),
					unit_yw:$('#powerCutRecords .public_search .searchUnit').val(),
					created_at:$('#powerCutRecords .public_search .startTime1').val(),
					end_at:$('#powerCutRecords .public_search .endTime1').val(),
					origin:$('#powerCutRecords .public_search .searchOrigins').val(),
					audit_des:$('#powerCutRecords .public_search .searchDes').val()
				},
		    	success:function(data){
//		    		console.log(data)
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var originDatas = template('powerCutRecord',data)
			        $('.public_table .exception-list').html(originDatas);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
					$('#powerCutRecords table .job_status').each(function(){
				    	var s=$(this).text()
				    	if(s=="待审核"){
							$(this).siblings().find('.editFor').hide()
							$(this).siblings().find('#editDone').hide()
						}else if(s=="审核通过"){
							$(this).siblings().find('.editFor').hide()
							$(this).siblings().find('#editDone').hide()
							$(this).siblings().find('.auditFor').hide()
						}
						if(s=="待上报"){
							$(this).siblings().find('.auditFor').hide()
							
						}
						if(s=="审核不通过"){
							$(this).siblings().find('.auditFor').hide()
						}
						    
				    })
					var roles=data.role_name;
			        if(roles=="运维成员"){
						$('#powerCutRecords table .auditFor').hide()
			        }
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url: "/power_cut_records.json",
		success:function(data){
			console.log(data)
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
		    var statusNews = template('powerStatus',data)
		    $('.status').html(statusNews)
		    var origins = template('powerOrigins',data)
		    $('.origins').html(origins)
		    var originDatas = template('powerCutRecord',data)
		    $('.public_table .exception-list').html(originDatas);
		    var desHtml = template('powerDes',data);
			$('#audit_des').html(desHtml);
			
	        $("#page").initPage(total, 1, originData)
	        $('.page_total span small').text(total)
	       $('#powerCutRecords table .job_status').each(function(){
				    	var s=$(this).text()
				    	if(s=="待审核"){
							$(this).siblings().find('.editFor').hide()
							$(this).siblings().find('#editDone').hide()
						}else if(s=="审核通过"){
							$(this).siblings().find('.editFor').hide()
							$(this).siblings().find('#editDone').hide()
							$(this).siblings().find('.auditFor').hide()
						}
						if(s=="待上报"){
							$(this).siblings().find('.auditFor').hide()
							
						}
						if(s=="审核不通过"){
							$(this).siblings().find('.auditFor').hide()
						}
						    
				    })
	        var roles=data.role_name;
	        if(roles=="运维成员"){
				$('#powerCutRecords table .auditFor').hide()
	        }
	        
		},
		error:function(err){
			console.log(err)
		}
	})
	
  //每页显示更改
//  $("#pagesize").change(function(){
//      $.ajax({
//          type: "get",
//          url: '/power_cut_records.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
//          async: true,
//          dataType: "json",
//          success: function(data) {
//              data_hash = data;
//              $('.loading').hide()
//              var total = data.list_size;
//              $("#page").initPage(total, 1, originData)
//              $('.page_total span small').text(total)
//          },
//          error:function(err){
//              console.log(err)
//          }
//      })
//  })  
//  
    
})
	
function previewImage(file, prvid) { 
	/* file：file控件 
	* prvid: 图片预览容器 
	*/ 
	var tip = "Expect jpg or png or gif!"; // 设定提示信息 
	var filters = { 
	"jpeg" : "/9j/4", 
	"gif" : "R0lGOD", 
	"png" : "iVBORw" 
	} 
	var prvbox = document.getElementById(prvid); 
	prvbox.innerHTML = ""; 
	if (window.FileReader) { // html5方案 
		for (var i=0, f; f = file.files[i]; i++) { 
			var fr = new FileReader(); 
			fr.onload = function(e) { 
				var src = e.target.result; 
				if (!validateImg(src)) { 
					alert(tip) 
				} else { 
					showPrvImg(src); 
				} 
			} 
			fr.readAsDataURL(f); 
		} 
	} else { // 降级处理
	
		if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) { 
			alert(tip); 
		} else { 
			showPrvImg(file.value); 
		} 
	} 
	
		function validateImg(data) { 
			var pos = data.indexOf(",") + 1; 
			for (var e in filters) { 
				if (data.indexOf(filters[e]) === pos) { 
					return e; 
				} 
			} 
			return null; 
		} 
	
		function showPrvImg(src) { 
			var img = document.createElement("img"); 
			img.src = src; 
			prvbox.appendChild(img); 
		} 
}