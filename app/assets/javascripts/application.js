// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require kindeditor
//= require jquery/jquery.cookie
//= require echart/echarts
//= require echart/highcharts
//= require echart/exporting
//= require datetime/jquery.datetimepicker.full

//= require bootstrap/bootstrap
//= require bootstrap/bootstrapValidator
//= require bootstrap/bootstrap-datetimepicker
//= require bootstrap/bootstrap-datetimepicker.zh-CN
//=	require jquery/template
//= require turbolinks
// require_tree .
//= require jquery/page
//= require home
//= require public
//= require zoomify/zoomify
//= require zoomify/jquery.imgbox.pack
//= require bootstrap_table/bootstrap-table.min
//= require bootstrap_table/bootstrap-table-export
//= require bootstrap_table/tableExport
//= require bootstrap_table/bootstrap-table-fixed-columns
//= require bootstrap_table/bootstrap-table-zh-CN.min
