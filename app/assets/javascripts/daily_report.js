$(function(){
	//获取当前时间
	var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates;
    console.log(datestamp)
    $('#daily_report #public_table .time').html(datestamp)
//  首页获取数据
$('.loading').show()
    $.ajax({
    	type:"get",
    	url:"/daily_report.json",
    	success:function(data){
    		console.log(data)
    		var region_name=data.region_code.region_name
    		$('#daily_report #public_table .Work_name').html(region_name)
    		var status=data.status;
    		$('#daily_report #public_table .state').html(status)
    		var tableData = template('dailyTable',data);
			$('#daily_report #public_table .exception-list').html(tableData);
			$('.loading').hide()
			if(status=="已上报"){
				
				$('#daily_report #public_table .table_button').hide()
				$('#daily_report #public_table .state').css("color","#00923f")
			
			}
    	},
    	error:function(err){
    		console.log(err)
    	}
    });
    //保存
    var d_daily_reports=[]
    var objname;
   
	$('#daily_report #save').click(function(){
		$('.loading').show()
		var status="N"
		submit(status)
	})
    //上报
    $('#daily_report #report').click(function(){
    	$('.loading').show()
    	var status="Y"
		submit(status)
	})
    //保存和上报调用的方法
    function submit(status){
    	var date_time=$('#daily_report #public_table .time').html()
		
		$('#daily_report #public_table .exception-list .name').each(function(){
			var name=$(this).html()
			var nums=$(this).siblings().find('.nums').val()
			var daily_yield=$(this).siblings().find('.yield').val()
			var id=$(this).siblings('.hide').html()
		
			var list={
				id:id,
				name:name, 
				nums:nums,
				daily_yield:daily_yield
				}
			d_daily_reports.push(list)
		})
		objname={d_daily_reports,date_time,status}
		console.log(objname)
		$.ajax({
	    	type:"post",
	    	url:"/daily_report.json",
	    	data:objname,
	    	success:function(data){
	    		$('.loading').hide()
	    		var status=data.status
	    		alert(status)
	    		window.location.reload();
	    	},
	    	error:function(err){
	    		console.log(err)
	    		alert("保存失败")
	    	}
	    });
    }
})


		