$(function(){
var time = new Date();
var year = time.getFullYear();
var month = time.getMonth() + 1;
var day = time.getDate();
var newMonths = month < 10 ? '0' + month : month;
var newDates = day < 10 ? '0' + day : day;
var stringDate= year + "-" + newMonths + "-" + newDates;

$('#post_reports #lookHtml #showSearch .endTime').val(stringDate)	
	
//	详细信息
	$('body').delegate('#post_reports #indexTable td .lookFor','click',function(){
		$('#post_reports #powerCutRecords').hide()
		var url;
		var id=$(this).parents('tr').find('.id').text();
		var status=$(this).parents('tr').find('.status').text();
		var region_name=$(this).parents('tr').find('.region_name').text();
		$('#loading').show()
		if(status=="Y"){
			$.ajax({
				type:"get",
				url:"/post_reports/"+id+'.json',
				success:function(data){
					var btnHtml=template('itemlButton',data);
					$('#post_reports #lookHtml .project_button .pull-left').html(btnHtml);
					$('#post_reports #lookHtml .project_button .pull-left').children().eq(0).addClass('btn-chack');
					$('#post_reports #lookHtml').show()
					$('#post_reports #lookHtml .lineId').val(id)
					$('#post_reports #lookHtml .regionName').val(region_name)
					var s_material_id=$('#post_reports #lookHtml .project_button .pull-left .btn-chack').val();
					var data={
				    		s_material_id:s_material_id
				    	}
					projectData(id,data)
					$('#loading').hide()
					
				},
				error:function(err){
					console.log(err)
					
				
				}
			});
			
		}else{
			$('#post_reports #refresh').show()
		}
		  
	})

	//项目按钮切换样式及获取数据
	$('body').delegate('#post_reports #lookHtml .project_button .pull-left button','click',function(){
		$(this).siblings().removeClass('btn-chack');
    	$(this).addClass('btn-chack');

    	$('#loading').show()
    	var id=$('#post_reports #lookHtml .lineId').val()
    	var itemID=$(this).val()
    	var begin_time=$('#post_reports #lookHtml #showSearch .startTime').val()
    	var end_time=$('#post_reports #lookHtml #showSearch .endTime').val()
    	var data={
    		s_material_id:itemID,
    		begin_time:begin_time,
			end_time:end_time
    	}
    	projectData(id,data)
    	$('#loading').hide()
	})
	  //	列表和折线图切换
	$('#post_reports #lookHtml .project_button .pull-right button').click(function(){
		$(this).siblings().removeClass('new_add_btn');
    	$(this).addClass('new_add_btn');
	})
	
    $('#post_reports #lookHtml #listTable').click(function(){
	  	$('#post_reports #lookHtml #public_table .listTable').show();
	  	$('#post_reports #lookHtml .chartContent').hide();
    })
    $('#post_reports #lookHtml .btn_chart').click(function(){
	  	$('#post_reports #lookHtml .chartContent').show();
	  	$('#post_reports #lookHtml #public_table .listTable').hide();
//		chartData()
    })
    //	列表和折线图切换结束
   
//首页获取数据
$('#loading').show()
	$.ajax({
		type:"get",
		url:"/post_reports.json",
		success:function(data){
			var indexTable=template('postIndextable',data);
			$('#post_reports #powerCutRecords #public_table .exception-list').html(indexTable);
			$('#loading').hide()
		},
		error:function(err){
			console.log(err)
		}
	});
	//查询
	$('#post_reports #lookHtml #showSearch .search').click(function(){
		var id=$('#post_reports #lookHtml .lineId').val()
		var s_material_id=$('#post_reports #lookHtml .project_button .pull-left .btn-chack').val();
		$('#post_reports #lookHtml #showSearch .startTime').val($('#post_reports #lookHtml #showSearch .createStart').val())
		
		var begin_time=$('#post_reports #lookHtml #showSearch .startTime').val()
		$('#post_reports #lookHtml #showSearch .endTime').val($('#post_reports #lookHtml #showSearch .createEnd').val())
		
		var end_time=$('#post_reports #lookHtml #showSearch .endTime').val()
		var data={
			s_material_id:s_material_id,
			begin_time:begin_time,
			end_time:end_time
		}
		projectData(id,data)
	})
	//导出表格
	$('#post_reports #lookHtml .public_search #excell').click(function(){
		var name=$('#post_reports #lookHtml .regionName').val();
		var code_name=$('#post_reports #lookHtml .project_button .pull-left .btn-chack').text();
		var begin_time=$('#post_reports #lookHtml .public_search .startTime').val();
		var end_time=$('#post_reports #lookHtml .public_search .endTime').val();
		var exportName=name+'-'+code_name+'-'+begin_time+'-'+end_time;
//		console.log(exportName)
		method5('example',exportName)
	})
	
	//发酵和酸化的交料产量详细信息
	$('body').delegate('#post_reports #lookHtml #example .product','click',function(){
		$('#yieldModal').modal('show')
		$('#loading').show()
		var s_material_id=$('#post_reports #lookHtml .project_button .pull-left .btn-chack').val();
		var date_time=$(this).siblings('.date_time').text();
		$.ajax({
			type:"get",
			url:"/post_reports/get_product.json",
			data:{
				s_material_id:s_material_id,
				date_time:date_time
			},
			success:function(data){
				
				var yieldTable=template('yieldTable',data);
				$('#post_reports #lookHtml #yieldModal #public_table table').html(yieldTable);
				$('#loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	//期盘点初始数据
	$('#post_reports #lookHtml #periodInventory').click(function(){
		var s_material_id=$('#post_reports #lookHtml .project_button .pull-left .btn-chack').val();
		var begin_time=$('#post_reports #lookHtml #periodSearch .createStart').val()
		var end_time=$('#post_reports #lookHtml #periodSearch .createEnd').val()
		$('#post_reports #lookHtml #periodInventoryModal table').html('');
		var datas={
			s_material_id:s_material_id,
			begin_time:begin_time,
			end_time:end_time
		}
		periodData(datas)
	})
	//期盘点查询
	$('#post_reports #lookHtml #periodSearch .search').click(function(){
		$('#loading').show()
		var s_material_id=$('#post_reports #lookHtml .project_button .pull-left .btn-chack').val();
		var begin_time=$('#post_reports #lookHtml #periodSearch .createStart').val()
		var end_time=$('#post_reports #lookHtml #periodSearch .createEnd').val()
		$('#post_reports #lookHtml #periodInventoryModal table').html('');
		var datas={
			s_material_id:s_material_id,
			begin_time:begin_time,
			end_time:end_time
		}
		periodData(datas)
		$('#loading').hide()
	})
})
//期盘点
function periodData(datas){
	$('#loading').show()
	$.ajax({
		type:"get",
		url:"/post_reports/period_inventory.json",
		data:datas,
		success:function(data){
			$('#post_reports #lookHtml #periodInventoryModal table').html('');	
			var periodTable=template('periodTable',data);
			$('#post_reports #lookHtml #periodInventoryModal table').html(periodTable);	
			$('#loading').hide()
		},
		error:function(err){
			console.log(err)
		}
	});
}
//项目所调用方法
function projectData(id,datas){
$('#loading').show()
	$.ajax({
		type:"get",
		url:"/post_reports/"+id+'.json',
		data:datas,
		success:function(data){
			console.log(data)
				$('#post_reports #lookHtml #public_table #tabList').html('');
				var showTable=template('showTables',data);
				$('#post_reports #lookHtml #public_table #tabList').html(showTable);
				
//				周盘点
				var invenHtml=template('InventoryTable',data);
				$('#post_reports #lookHtml #InventoryModal #public_table table').html(invenHtml);
				//去除tr标签内<td>带hide的子元素
				$('#post_reports #lookHtml #public_table #example .data_list tr').children('.hide').remove()
				putTable()
				//给表格添加颜色
				$('#post_reports #lookHtml #public_table #example .data_list tr').each(function(i){
//					$(this).children('.hide').remove()
					if(data.code=="TF"){
						
						$(this).children().eq(4).css({"background-color":"#ff8080"});
						$(this).children().eq(8).css({"background-color":"#ff8080"});
						$(this).children().eq(9).css({"background-color":"#ffcc00"});
						$(this).children().eq(10).css({"background-color":"#ffcc00"});
						$(this).children().eq(12).css({"background-color":"#ffcc00"});
						$(this).children().eq(11).css({"background-color":"#ffcc99","color":"#ff0000"});
					}else if(data.code=="TY"){
						$(this).children().eq(2).css({"background-color":"#969696"});
						$(this).children().eq(3).css({"background-color":"#c0c0c0"});
						$(this).children().eq(30).css({"background-color":"#ffff99"});
						$(this).children().eq(31).css({"background-color":"#ffff99"});
						$(this).children().eq(32).css({"background-color":"#ffff99","color":"red"});
						$(this).children().eq(29).css({"background-color":"#0066cc","color":"#fff"});
						$(this).children().eq(27).css({"background-color":"#c0c0c0"});
						$(this).children().eq(28).css({"background-color":"#c0c0c0"});
						
					}
					
				})
				
				$('#post_reports #lookHtml #public_table #example .data_list td').each(function(){
					
					$(this).html().replace(/\s+/g,"")
					if($(this).html()=="周四"){
						$(this).addClass('Thur_color')
						$(this).prev('td').addClass('Thur_color')
					}
				})
				$('#post_reports #lookHtml #public_table .fixed-table-column td').each(function(){
					
					$(this).html().replace(/\s+/g,"")
					if($(this).html()=="周四"){
						$(this).addClass('Thur_color')
						$(this).prev('td').addClass('Thur_color')
					}
				})
				$('#listTable').trigger('click')
			
			chartData()
	
			$('#loading').hide()

     
		},
		error:function(err){
			console.log(err)
		
		}
	});
}
//折线图获取数据
function chartData(){
	var code=$('#post_reports #lookHtml .project_button .pull-left .btn-chack').attr('value1');
	  	var series;
	  	var	xData=[];
	  	var data=[]
	  	var dataArr2=[]
	  	var dataArr3=[]
	  	var dataArr4=[]
	  	var dataArr5=[]
	  	var dataArr6=[]
	 	
	  	$('#post_reports #lookHtml #public_table #example .data_list tr').each(function(i){
	  		$(this).html().replace(/\s+/g,"")
					if(code=="TF"){
					var data1=parseFloat($(this).children('.leftNum').html());
					data.push(data1)
					var data2=parseFloat($(this).children('.gNum').html());
					dataArr2.push(data2)
					var data3=parseFloat($(this).children('.sum1').html());
					dataArr3.push(data3)
					var data4=parseFloat($(this).children('.sum1').html());
					dataArr4.push(data4)
					var data5=parseFloat($(this).children('.sfbl').html());
					dataArr5.push(data5)
					var data6=parseFloat($(this).children('.shNum').html());
					dataArr6.push(data6)
					var list={
						name:"下余小计",
						data:data
					}
					var list2={
						name:"购进小计",
						data:dataArr2
					}
					var list3={
						name:"实耗干粉",
						data:dataArr3
					}
					var list4={
						name:"实耗湿粉",
						data:dataArr4
					}
					var list5={
						name:"湿粉比例",
						data:dataArr5
					}
					var list6={
						name:"实耗小计",
						data:dataArr6
					}
					var xdata=$(this).children().eq(0).html();
					xData.push(xdata)
					series=[list,list2,list3,list4,list5,list6]
	
					}else if(code=="TY"){
						var data1=parseFloat($(this).children('.productNum').html());
						data.push(data1)
						var data2=parseFloat($(this).children('.xy1Num').html());
						dataArr2.push(data2)
						var data3=parseFloat($(this).children('.xy2Num').html());
						dataArr3.push(data3)
						var data4=parseFloat($(this).children().eq(30).html());
						dataArr4.push(data4)
						var data5=parseFloat($(this).children().eq(31).html());
						dataArr5.push(data5)
						var data6=parseFloat($(this).children().eq(32).html());
						dataArr6.push(data6)
						var list={
							name:"交料产量总计",
							data:data
						}
						var list2={
							name:"下余物料1小计",
							data:dataArr2
						}
						var list3={
							name:"下余物料2小计",
							data:dataArr3
						}
						var list4={
							name:"总结余	",
							data:dataArr4
						}
						var list5={
							name:"时耗淀粉",
							data:dataArr5
						}
						var list6={
							name:"产糖（T）",
							data:dataArr6
						}
						var xdata=$(this).children().eq(0).html();
						xData.push(xdata)
						series=[list,list2,list3,list4,list5,list6]
					}else if(code=="FJY"){
						var data1=parseFloat($(this).children('.product').html());
						data.push(data1)
						var data2=parseFloat($(this).children().eq(15).html());
						dataArr2.push(data2)
						var data3=parseFloat($(this).children().eq(16).html());
						dataArr3.push(data3)
						var data4=parseFloat($(this).children().eq(17).html());
						dataArr4.push(data4)
						var data5=parseFloat($(this).children().eq(18).html());
						dataArr5.push(data5)
						var data6=parseFloat($(this).children().eq(19).html());
						dataArr6.push(data6)
						var list={
							name:"交料产量小计",
							data:data
						}
						var list2={
							name:"总计",
							data:dataArr2
						}
						var list3={
							name:"产量",
							data:dataArr3
						}
						var list4={
							name:"时耗	",
							data:dataArr4
						}
						var list5={
							name:"收率",
							data:dataArr5
						}
						var list6={
							name:"对淀粉收率",
							data:dataArr6
						}
						var xdata=$(this).children().eq(0).html();
						xData.push(xdata)
						series=[list,list2,list3,list4,list5,list6]
					}else if(code=="SHY"){
						var data1=parseFloat($(this).children('.product').html());
						data.push(data1)
						var data2=parseFloat($(this).children().eq(16).html());
						dataArr2.push(data2)
						var data3=parseFloat($(this).children().eq(17).html());
						dataArr3.push(data3)
						var data4=parseFloat($(this).children().eq(18).html());
						dataArr4.push(data4)
						var data5=parseFloat($(this).children().eq(19).html());
						dataArr5.push(data5)
						var list={
							name:"交料产量小计",
							data:data
						}
						var list2={
							name:"总计",
							data:dataArr2
						}
						var list3={
							name:"时耗",
							data:dataArr2
						}
						var list4={
							name:"收率",
							data:dataArr3
						}
						var list5={
							name:"总收率",
							data:dataArr4
						}
						var xdata=$(this).children().eq(0).html();
						xData.push(xdata)
						series=[list,list2,list3,list4,list5]
					}
					
				})

	  	lineChart(xData,series)
}
//折线图所调用方法
var chartLine 
function lineChart(xData,seriesData){

	var NaN=null;

//	var xData= data.days;
 Highcharts.setOptions({
        lang:{
            contextButtonTitle: "图表导出菜单",
            downloadJPEG:"下载 JPEG 图片",
            downloadPDF:"下载 PDF 文件",
            downloadPNG:"下载 PNG 文件",
            downloadSVG:"下载 SVG 文件",
            printChart:"打印图表"
        }
    });
		chartLine = Highcharts.chart('lineChart', {
		    title: {
		        text: ''
		    },
		    credits: {
		        enabled: false//右下角的版权信息不显示
		    },
		    yAxis: {
		        title: {
		            text: ''
		        }
		    },
//		    tooltip: {
//		        crosshairs: true,
//		        shared: true
//		    },
		    legend: {
		        layout: 'horizontal',
		        align: 'center',
		        verticalAlign: 'bottom'
		    },
		    xAxis: {
	        
	            categories: xData
	        },
	        plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true
	                }
	            }
	        },
		    series:seriesData,
		
		});
	
		//折线图和柱状图切换
		$('.btn_chart').click(function(){
			var self = $(this);
			for(var i=0;i<chartLine.series.length;i++) {
				var serie = chartLine.series[i];
				serie.update({
						type: self.data('type')
				});
			}
		});

}

//固定列
function putTable(){
	$('#example').bootstrapTable("destroy").bootstrapTable({
		fixedColumns: true,
		fixedNumber: 2,//固定列
//		showExport: true, //是否显示导出
//		exportDataType: "all", //basic', 'all', 'selected'.
//		exportTypes:['excel'],	    //导出类型
////		exportButton: $('#excell'),     //为按钮btn_export  绑定导出事件  自定义导出按钮(可以不用)
//		exportOptions:{  
//		          //ignoreColumn: [0,0],            //忽略某一列的索引  
//		          fileName: '数据导出',              //文件名称设置  
//		          worksheetName: 'Sheet1',          //表格工作区名称  
//		          tableName: '数据表',  
//		          excelstyles: ['background-color', 'color', 'font-weight'],  
//		          //onMsoNumberFormat: DoOnMsoNumberFormat  
//		      }
	})

}
