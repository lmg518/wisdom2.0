$(function(){
	$.ajax({
		type:'get',
		url:'/facility_fault_day_reports/init_index.json',
		success:function(data){
			$('.loading').hide()
			var originData = template('facilityTab',data)
			$('.public_table .exception-list').html(originData)
			
		},
		error:function(err){
			console.log(err)
		}
	})
	$.ajax({
		type:"get",
		url:"/search_by_proviences/index",
		success:function(data){
			$('.loading').hide()
			var dataHtml = template('province',data)
			$('.province').html(dataHtml)
		
		},
		error:function(err){
			console.log(err)
		}
	});
	
	//查询
	var start_datetime = '';
	var provience;
	function pageName_Data(){
		if($('.public_search .start_time').val()){
			start_datetime = $('.public_search .start_time').val();
		}
		provience=$('.public_search .province option:selected').val();
	}
	$('.loading').show()
	$('.public_search .search').click(function(){
		$('.choose-site').hide()
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/facility_fault_day_reports/init_index.json',
			data:{
				search:{
					provience:provience,
					data_times:start_datetime
				}
			},
			success:function(data){
				$('.loading').hide()
				var originData = template('facilityTab',data)
			$('.public_table .exception-list').html(originData)
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	//	导出表格
	
	$('.putTable').click(function(){
    	$('.choose-site').hide()
    	$(this).attr('disabled',true)
    	pageName_Data()
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("method","get");
		form.attr("action","/facility_fault_day_reports/export_file.csv");
		var input1=$("<input>");
		input1.attr("type","hidden");
		input1.attr("name","provience");
		input1.attr("value",provience);
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","data_times");
		input2.attr("value",start_datetime);
		
		$("body").append(form);//将表单放置在web中
		form.append(input1);
		form.append(input2);
		
		form.submit();//表单提交 
    })	
})
