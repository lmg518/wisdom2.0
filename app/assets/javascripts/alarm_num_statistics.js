$(function(){
//	站点
    var placeId = ''
	$('.place_leval').change(function(){
		placeId = $(this).val()
	})
	
	$('.place_leval').click(function(){
		$('.control_station').val('')
	})
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	$('.search_bottom').click(function(){
		$('.choose-site').hide()
	})
	$('.public_search .control_station').click(function(){
		if(placeId == ''){
			$('#search-station').hide()
			alert('请先选择区域级别')
	    }else{
	    	$('#search-station').show()
	    	$.ajax({
			type:'get',
			url:'/search_station_bindings.json',
			data:{
				region_level:placeId
			},
			success:function(data){
				var bindTemp = template('editStation',data);
		        $('#search-station .choose-site').html(bindTemp);
		        $('#search-station .choose-site').show();
			},
			error:function(err){
				console.log(err)
			}
		})
	    }
	})
	
	$(document).delegate('.public_search .control_station','blur',function(){
			var control_stationName=$('.public_search .control_station').val();
			if(control_stationName==""){
				$('.control_stationID').val("");
			}else{
				$('.control_stationID').val();
			}
	})
	
	function plicBox(){
	//  报警级别
	    $.getJSON('/s_alarm_levels.json').done(function(data){
	    	var htmlStr=template('notice_leval',data);
	    	$('#alarm_num_statistics .public_search .notice_leval').html(htmlStr)
	    })
	
    }
	plicBox();
	
	
	
	
	var station_id='';
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	var alarm_level;
//	var status;
	function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val().split(',');
		}
		if($('.public_search .control_stationID').val()==""){
			station_id="";
		}
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
		alarm_level = $('.public_search .notice_leval option:selected').val();
	}
	$('.public_search .search').click(function(){
		$('.choose-site').hide()
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/alarm_num_statistics/init_index.json',
			data:{
				search:{
					station_ids:station_id,
					alarm_levels:alarm_level,
					data_times:data_times
				}
			},
			success:function(data){
				$('.loading').hide()
				alarms_arr = data
				data_hash = data
				var originData = template('originDatas',data)
				$('.public_table .exception-list').html(originData)
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	$('body').delegate('.add_factor .close','click',function(){
		$('#public_box').hide();
		$('#alarm_num_statistics .lookforRules').hide();
	})
	$('body').delegate('#alarm_num_statistics .area_btn .btn-cancel','click',function(){
		$('#public_box').hide();
		$('#alarm_num_statistics .lookforRules').hide();
	})
	
    var time,stations,alarmLevel
	$('body').delegate('#alarm_num_statistics .public_table .lookfor','click',function(){
		time = $(this).parent().siblings('.times').text();
		stations = $(this).parent().siblings().find('.stationID').text();
		alarmLevel = $(this).parent().siblings('.level').find('.levelId').text();
		$('#public_box').show();
		$('#alarm_num_statistics .lookforRules').show();
		$.ajax({
			type:'get',
			url:'/d_alarms.json',
			dataType:'json',
			data:{
				search:{
					station_ids:stations,
					alarm_levels:alarmLevel,
					data_times:time
				},status: "alarm_num_statistics"
			},
			success:function(data){
				var htmlStr=template('notice_rules',data);
				$('#alarm_num_statistics .lookforRules').html(htmlStr);
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	
//	function originData(param){
//		pageName_Data();
//		var pp = $('#page .pageItemActive').html();
//		if(parseInt(pp) == 1 && page_state == "init"){
//			var originData = template('originDatas',data_hash)
//			$('.public_table .exception-list').html(originData)
//			$('.page_total span a').text(pp);
//		}else{
//			$('.choose-site').hide()
//			$('.loading').show()
//			page_state = "change";
//          data_hash = {};
//          $.ajax({
//				url: '/alarm_num_statistics.json?page=' + pp,
//		    	type: 'get',
//				data:{
//					search:{
//						station_ids:station_id,
//						alarm_levels:alarm_level,
//						data_times:data_times
//						
//					}
//				},			   
//		    	success:function(data){
//		    		$('.loading').hide()
//		    		var originData = template('originDatas',data)
//					$('.public_table .exception-list').html(originData)
//					$('.page_total span a').text(pp);
//		    	},
//		    	error:function(err){
//		    		console.log(err)	
//		    	}
//			})
//		}
//	
//		
//	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/alarm_num_statistics/init_index.json',
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
			var dataHtml = template('place_leval',data)
			$('.place_leval').html(dataHtml)
	        var originData = template('originDatas',data)
			$('.public_table .exception-list').html(originData)
		},
		error:function(err){
			console.log(err)
		}
	})
	
	
//	导出表格
	
	$('.putTable').click(function(){
    	$(this).attr('disabled',true)
    	pageName_Data();
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("method","get");
		form.attr("action","/alarm_num_statistics/export_file.csv");
		var input1=$("<input>");
		input1.attr("type","hidden");
		input1.attr("name","station_ids");
		input1.attr("value",station_id);
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","data_times");
		input2.attr("value",data_times);
		var input3=$("<input>");
		input3.attr("type","hidden");
		input3.attr("name","alarm_levels");
		input3.attr("value",alarm_level);
		$("body").append(form);//将表单放置在web中
		form.append(input1);
		form.append(input2);
		form.append(input3);
		form.submit();//表单提交 
    })
	
	
	
})
