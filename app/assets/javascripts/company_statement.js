$(function(){
var time = new Date();
var year = time.getFullYear();
var month = time.getMonth() + 1;
var day = time.getDate();
var newMonths = month < 10 ? '0' + month : month;
var newDates = day < 10 ? '0' + day : day;
var stringDate 
stringDate= year + "-" + newMonths + "-" + newDates;
	$('#company_statement .public_search #start_date_range').val(stringDate)

	dayReport(stringDate)
   //查询
   $('#company_statement .public_search .search').click(function(){
   	$('#loading').show()
   		var time=$('#company_statement .public_search #start_date_range').val();
   		$.ajax({
	   	type:"get",
	   	url:"/company_statement/index_table",
	   	data:{
	   		time:time
	   	},
	    success:function(data){
		   	$('#company_statement #public_table .public_table').html(data)
		   	  $('#loading').hide()
		   },
		   error:function(err){
		   	alert("出错啦！！")
	    		console.log(err)
	    	}
	   });
   })
})
//公司日报表
function dayReport(stringDate){
$('#loading').show()
   $.ajax({
   	type:"get",
   	url:"/company_statement/index_table",
   	data:{
				time:stringDate
			},
    success:function(data){
	   	$('#company_statement #public_table .public_table').html(data)
	   	  $('#loading').hide()
	   },
	   error:function(err){
	   	alert("出错啦！！")
    		console.log(err)
    	}
   });
}

//时间格式转换
Date.prototype.format = function (format) {
        var args = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
            "q+": Math.floor((this.getMonth() + 3) / 3), //quarter

            "S": this.getMilliseconds()
        };
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var i in args) {
            var n = args[i];

            if (new RegExp("(" + i + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
        }
        return format;
    };  