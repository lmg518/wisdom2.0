//= require jquery
//= require jquery_ujs
//= require kindeditor
//= require jquery/jquery.cookie
//= require echart/echarts
//= require datetime/jquery.datetimepicker.full
//= require bootstrap/bootstrap
//= require bootstrap/bootstrapValidator
//=	require jquery/template
//= require turbolinks

//= require jquery/page
//= require home
//= require public
