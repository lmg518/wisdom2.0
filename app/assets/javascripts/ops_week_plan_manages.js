$(function(){
	
	//运维单位
	$(document).ready(function(){
		$.getJSON('/search_region_codes').done(function(data){
			var stationTemp = template('editUnit',data);
			$('#search-unit .choose-site').html(stationTemp);
		})
		$('.loading').show();
		$.ajax({
			type:'get',
			url:'/ops_week_plan_manages.json?begin_time='+begin_time,
			success:function(data){
				$('.loading').hide()
				var total = data.list_size;
				data_hash=data;
			    $("#page").initPage(total, "1", originData)
			    $('.page_total span small').text(total)
			    var startDate=$('#monitor .firstTime').text()
				var endDate=$('#monitor .listTime').text()
				$('#week_plan #startDate').text(startDate)
				$('#week_plan #endDate').text(endDate)
					
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	 

	var unit_name_id ='';
	var zone_name='';
	function pageName_Data(){
		$('#week_plan .public_search .searchUnit').val($('.public_search .unit_ywID').val())
		$('#week_plan .public_search .searchCity').val($('#week_plan #city').val())
		
		edit_status="";
		if($('#week_plan .public_search .searchUnit').val()){
            unit_name_id = $('#week_plan .public_search .searchUnit').val().split(',')
        }
		
		zone_name=$('#week_plan .public_search .searchCity').val();
	}
	$('.public_search .search').click(function(){
		$('#week_plan #whole').css({ "color": "#fff", "background": "#558ad8" })
			$('#week_plan #completed').css({ "color": "#333", "background": "#fff" })
			$('#week_plan #pending').css({ "color": "#333", "background": "#fff" })
			$('#week_plan #allocated').css({ "color": "#333", "background": "#fff" })
		var begin_time=$('#monitor .firstTime').text()
		$('.loading').show()
		unit_name_id ='';
		zone_name='';
		pageName_Data()
		$.ajax({
			type:"get",
			url:"/ops_week_plan_manages.json?begin_time="+begin_time,
			data:{
				info_id:unit_name_id,
				zone_name:zone_name,
				edit_status:edit_status
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, "1", originData);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				
			}
		});
	})
	//已完成
	var edit_status;
	$('#completed').click(function(){
		$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
		$('.loading').show()
		var begin_time=$('#monitor .firstTime').text()
		edit_status=$(this).val()
		var unit_name_id = $('#week_plan .public_search .searchUnit').val();
		if(unit_name_id){
			unit_name_id = unit_name_id.split(",");
		}
		$.ajax({
			type:"get",
			url:"/ops_week_plan_manages.json",
			data:{
				begin_time:begin_time,
				edit_status:edit_status,
				unit_name_id:unit_name_id,
				zone_name:$('#week_plan .public_search .searchCity').val()
			},
			success:function(data){
				$('.loading').hide()
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, "1", originData);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				
			}
		});
	})
	//待处理
	$('#pending').click(function(){
		$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
		$('.loading').show()
		var begin_time=$('#monitor .firstTime').text()
		edit_status=$(this).val()
		var unit_name_id = $('#week_plan .public_search .searchUnit').val();
		if(unit_name_id){
			unit_name_id = unit_name_id.split(",");
		}
		$.ajax({
			type:"get",
			url:"/ops_week_plan_manages.json",
			data:{
				begin_time:begin_time,
				edit_status:edit_status,
				unit_name_id:unit_name_id,
				zone_name:$('#week_plan .public_search .searchCity').val()
			},
			success:function(data){
				$('.loading').hide()
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, "1", originData);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				
			}
		});
	})
	//待分配
	$('#allocated').click(function(){
		$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
		$('.loading').show()
		var begin_time=$('#monitor .firstTime').text()
		edit_status=["N","O"]
		var unit_name_id = $('#week_plan .public_search .searchUnit').val();
		if(unit_name_id){
			unit_name_id = unit_name_id.split(",");
		}
		$.ajax({
			type:"get",
			url:"/ops_week_plan_manages.json",
			data:{
				begin_time:begin_time,
				edit_status:edit_status,
				unit_name_id:unit_name_id,
				zone_name:$('#week_plan .public_search .searchCity').val()
			},
			success:function(data){
			
				$('.loading').hide()
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, "1", originData);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				
			}
		});
	})

	 //初始数据
	 
	var cells = document.getElementById('monitor').getElementsByTagName('td');
            var clen = cells.length;
            var currentFirstDate;
            function formatDate(date){
            	 var year = date.getFullYear();
                var month = date.getMonth()+1;
                var day = date.getDate();
//              var week = '('+['星期天','星期一','星期二','星期三','星期四','星期五','星期六'][date.getDay()]+')';  
				
				var newMonths=month<10?'0'+month:month;
				var newDates=day<10?'0'+day:day;
                return year+"-"+newMonths+"-"+newDates+" ";
            }
			function addDate(date,n){
				 date.setDate(date.getDate()+n);        
                return date;
			}
			function setDate(date){
				 var week = date.getDay()-1;
                date = addDate(date,week*-1);
                currentFirstDate = new Date(date);

                for(var i = 0;i<clen;i++){                 
                    cells[i].innerHTML = formatDate(i==0 ? date : addDate(date,1));
                }     
			}
			//上周
			$('#btnPreWeek').click(function(){
				$(this).css({ "color": "#fff", "background": "#f29a76" })
				$(this).siblings('#btnNextWeek').css({ "color": "#f29a76", "background": "#fff" })
				$('.loading').show()
				setDate(addDate(currentFirstDate,-7));
				var begin_time1=$('#monitor .firstTime').text()
				console.log(begin_time1)
				$.ajax({
					type:"get",
					url:'/ops_week_plan_manages.json?begin_time='+begin_time1,
					data:{
						begin_time:begin_time1
					},
					success:function(data){
						console.log(data)
						$('.loading').hide()
						data_hash = data
						$("#page").initPage(data.list_size, 1, originData);
		   				$('.page_total small').text(data.list_size);
//						var originData_data = template('ops_week_table',data)
//						$('#week_plan .public_table .exception-list').html(originData_data)
						var startDate1=$('#monitor .firstTime').text()
						var endDate1=$('#monitor .listTime').text()
						$('#week_plan #startDate').text(startDate1)
						$('#week_plan #endDate').text(endDate1)
						
					},
					error:function(err){
						console.log(err)
					}
				});
			})
			//下周
			 $('#btnNextWeek').click(function(){
			 	$(this).css({ "color": "#fff", "background": "#f29a76" })
				$(this).siblings('#btnPreWeek').css({ "color": "#f29a76", "background": "#fff" })
			 	$('.loading').show()
				setDate(addDate(currentFirstDate,7));
				var begin_time2=$('#monitor .firstTime').text()
				console.log(begin_time2)
				$.ajax({
					type:"get",
					url:'/ops_week_plan_manages.json?begin_time='+begin_time2,
					data:{
						begin_time:begin_time2
					},
					success:function(data){
						$('.loading').hide()
						data_hash = data
						$("#page").initPage(data.list_size, 1, originData);
		   				$('.page_total small').text(data.list_size);
//						var originData_data = template('ops_week_table',data)
//						$('#week_plan .public_table .exception-list').html(originData_data)
						var startDate2=$('#monitor .firstTime').text()
						var endDate2=$('#monitor .listTime').text()
						$('#week_plan #startDate').text(startDate2)
						$('#week_plan #endDate').text(endDate2)
						
					},
					error:function(err){
						console.log(err)
					}
				});
			})           
             
            setDate(new Date());
            
            var begin_time=$('#monitor .firstTime').text()
            //	全部
$('#week_plan #whole').click(function(){
	var begin_times=$('#monitor .firstTime').text()
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	edit_status="";
	var unit_name_id = $('#week_plan .public_search .searchUnit').val();
	if(unit_name_id){
		unit_name_id = unit_name_id.split(",");
	}
	$.ajax({
		type:'get',
		url:'/ops_week_plan_manages.json?begin_time='+begin_times,
		data:{
			edit_status:edit_status,
			unit_name_id:unit_name_id,
			zone_name:$('#week_plan .public_search .searchCity').val()
		},
		success:function(data){
			console.log(data)
			$('.loading').hide()
			
			var originDatas = template('ops_week_table',data)
			$('#week_plan .public_table .exception-list').html(originDatas)
			var total = data.list_size;
			$('.page_total span small').text(total)
			$("#page").initPage(data.list_size, "1", originData);
			var startDate=$('#monitor .firstTime').text()
			var endDate=$('#monitor .listTime').text()
			$('#week_plan #startDate').text(startDate)
			$('#week_plan #endDate').text(endDate)
			
		},
		error:function(err){
			console.log(err)
		}
	})
})

	
	//	分页+初始数据
var begin_time;
	   function originData(param){
		// pageName_Data();
		begin_time=$('#monitor .firstTime').text()
		var pp = $('#page .pageItemActive').html();
//		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originDatas = template('ops_week_table',data_hash)
			$('#week_plan .public_table .exception-list').html(originDatas);
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
			data_hash = {};
			 var unit_name_id = $('#week_plan .public_search .searchUnit').val();
            if(unit_name_id){
            	unit_name_id = unit_name_id.split(",");
            }
			$.ajax({
				url: '/ops_week_plan_manages.json?begin_time='+begin_time+'&page='+pp,
		    	type: 'get',
		    	data:{
					unit_name_id:unit_name_id,
					zone_name:$('#week_plan .public_search .searchCity').val(),
					edit_status:edit_status
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var teHtml= template('ops_week_table',data)
					$('#week_plan .public_table .exception-list').html(teHtml)
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	begin_time=$('#monitor .firstTime').text()
		
	 
//	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/ops_week_plan_manages.json?begin_time='+begin_time+'&page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })        
})
