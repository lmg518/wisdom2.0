$(function(){
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	
	//查询
 	var station_id = '';
 	var created_time = '';
	var end_time = '';
	var job_no='';
	var author='';
	var handle_man='';
	var create_type='';
	var job_status='';
	   function public_search(){
		   $('#equip_mains .public_search .startTime1').val($('.created_time').val())
		   $('#equip_mains .public_search .endTime1').val($('.end_datetime').val())
			created_time = $('#equip_mains .public_search .startTime1').val();
	    	end_time =$('#equip_mains .public_search .endTime1').val();
			if(created_time && end_time){
				created_time=created_time;
				end_time=end_time;
			}else if(created_time){
				end_time = created_time
			}else if(end_time){
				created_time = end_time
			}else{
				created_time = ''
		        end_time = ''
			}
			$('#equip_mains .public_search .searchSta').val( $('.control_stationIDs').val())
			if($('#equip_mains .public_search .searchSta').val()){
	             station_id = $('#equip_mains .public_search .searchSta').val().split(',')
	        }else{
	        	 station_id = ''
			}
		$('#equip_mains .public_search .searchNo').val($('.public_search .job_no').val())
        $('#equip_mains .public_search .searchAuthor').val($('.public_search .author').val())
        $('#equip_mains .public_search .searchHan').val($('.public_search .search_handle').val())			
		var searchCrea=$('.public_search .create_type option:selected').val();
		$('#equip_mains .public_search .searchCrea').val(searchCrea);
		var searchStatus=$('.public_search .job_status option:selected').val();
		$('#equip_mains .public_search .searchStatus').val(searchStatus)
		var searchFault=$('.public_search .fault_type option:selected').val();
		$('#equip_mains .public_search .searchFault').val(searchFault)

		job_no=$('#equip_mains .public_search .searchNo').val();
	   	author= $('#equip_mains .public_search .searchAuthor').val();
	    handle_man= $('#equip_mains .public_search .searchHan').val();
	    create_type=$('#equip_mains .public_search .searchCrea').val()
	    job_status=$('#equip_mains .public_search .searchStatus').val();
	    fault_type=$('#equip_mains .public_search .searchFault').val();
		}
	   $('.public_search .search').click(function(){
	   	
		 station_id = '';
	 	 created_time = '';
		 end_time = '';
		 job_no='';
		 author='';
		 handle_man='';
		 create_type='';
		  job_status='';
		 public_search()

		$.ajax({
			type:"get",
			url:"/equip_mains.json",
			data:{
				created_time:created_time,
				end_time:end_time,
				d_station_id:station_id,
				job_no:job_no,
				author:author,
				handle_man:handle_man,
				job_status:job_status,
				create_type:create_type
				
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
				$('.page_total small').text(data.list_size);
				$('#equip_mains .public_table .exception-list .job_status').each(function(i){
						var s=$(this).text()
						if(s=="工单待审核"){
							$(this).siblings().find('.writing').hide()
						}else if(s=="工单已审核"){
							$(this).siblings().find('.writing').hide()
						}else if(s=="设备待校准"){
							$(this).siblings().find('.writing').hide()	
						}
				})
			},
			error:function(err){
				console.log(err)
			}
		});
	   })
	 $('body').delegate('#equip_writings #editBack','click',function(){
 		 	$('#equip_mains').show();
			$('#equit_looks').hide();
			$('#equip_writings').hide();
 		 })
 		 $('body').delegate('#IsSceneN','click',function(){
 		 	$('#FaultProcess #DeviceInfo').show()
 		 })
 		 $('body').delegate('#IsSceneY','click',function(){
 		 	$('#FaultProcess #DeviceInfo').hide()
 		 })
 		 $('body').delegate('#ProcessG','click',function(){
 		 	$('#FaultProcess #DeviceInfo_Code').show()
 		 	$('#FaultProcess #FaultContent').show()
 		 })
 		 $('body').delegate('#ProcessX','click',function(){
 		 	$('#FaultProcess #DeviceInfo_Code').hide()
 		 	$('#FaultProcess #FaultContent').show()
 		 })
 		 $('body').delegate('#ProcessQ','click',function(){
 		 	$('#FaultProcess #DeviceInfo_Code').hide()
 		 	$('#FaultProcess #FaultContent').hide()
 		 })
	//编辑
	var id
	var DeviceTypeName;
	$('body').delegate('#equip_mains table .writing','click',function(){
		$('#equip_mains').hide();
		$('#equip_looks').hide();
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"get",
			url:'/equip_mains/'+id + '/edit.json',
			success:function(data){
			var dataObj = {
					editData:data
				}
			
			var editHtml=template('equipWriting',dataObj)
				$('#equip_writings').html(editHtml)
				DeviceTypeName=data.equilp.equip_type;
				$('#equip_writings').show();
				var remote=data.handle_remote;
				var types=data.handle_type;
				if(remote=="Y"){
					$('#equip_writings .IsScene #IsSceneY').prop('checked',true)
					$('#equip_writings #DeviceInfo').hide();
				}else{
					$('#equip_writings .IsScene #IsSceneN').prop('checked',true)
				}
				
				if(types=="现场维修"){
					$('#equip_writings #DeviceInfo #ProcessX').prop('checked',true)	
				}else if(types=="更换备机"){
					$('#equip_writings #DeviceInfo_Code').show();
					$('#equip_writings #DeviceInfo #ProcessG').prop('checked',true)
				}else{
					$('#equip_writings #DeviceInfo #ProcessQ').prop('checked',true)
				}
				typeNames(DeviceTypeName)
			},
			error:function(err){
				console.log(err)
			}
		});
	})

	$('body').delegate('#equip_writings  .btn-checkitem','click',function(){
		$('#equip_writings .edit_jobs').hide();
		typeName=$(this).children('span').html()
		
		console.log(typeName)
		var Data_DZM = {
				id:id,
				name:typeName
                }
		Replacement(Data_DZM)
		 $('#equip_writings #typeListBox').show();
	})
	$('body').delegate('#equip_looks  .btn-checkitem','click',function(){
		$('#equip_looks .edit_jobs').hide();
		typeName=$(this).children('span').html()
		
		console.log(typeName)
		var Data_DZM = {
				id:id,
				name:typeName
                }
		Replacement(Data_DZM)
		 $('#equip_looks #typeListBox').show();
	})
	function Replacement(Data_DZM) {
	  var url='';
	  if(Data_DZM.name=="备机更换记录表"){
		url = "/get_form_module/replacement_recorder";
	  }else if(Data_DZM.name=="空气自动监测仪器设备检修记录表"){
		url = "/get_form_module/atmosphere_automatic_monitoring_instrument";
	}else if(Data_DZM.name =="颗粒物PM10自动监测分析仪运行状况检查记录" || Data_DZM.name =="颗粒物PM2.5自动监测分析仪运行状况检查记录"){
			url = "/get_form_module/pm_check_week";
	}else if(Data_DZM.name =="臭氧（O3）分析仪运行状况检查记录表"){//
		url = "/get_form_module/analyser_status_week";
	}else if(Data_DZM.name =="氮氧化物（NOX）分析仪运行状况检查记录表"){//
		url = "/get_form_module/nox_analyser_status_week";
	}else if(Data_DZM.name =="二氧化硫（SO2）分析仪运行状况检查记录表"){//（每周）    
		url = "/get_form_module/so2_analyser_status_week";
	}else if(Data_DZM.name =="一氧化碳（CO）分析仪运行状况检查记录表"){//
		url = "/get_form_module/co_analyser_status_week";
	}
		$.ajax({
                    type:"get",
					url:url,
                    data:Data_DZM,
                    async:true,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
					   $('#equip_writings #typeListBox').html(data)
					   $('#equip_looks #typeListBox').html(data)
					   
						  var modalID=$('#typeListBox .modalID').html();
							if(modalID=="37" || modalID=="4" || modalID=="5" || modalID=="7" || modalID=="11" || modalID=="12" || modalID=="13"){
								$('#typeListBox #Save').hide();
								$('#typeListBox #supplyEdit').hide();
								$('#typeListBox #supplyLook').show();
							}else{
								$('#typeListBox #supplyEdit').show();
								$('#typeListBox #supplyLook').hide();
							}
					   $('#typeListBox').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#equip_looks #typeListBox table input').attr("disabled",true)
						$('#equip_looks #typeListBox table textarea').attr("disabled",true)
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
							todayButton:true,
							step:5
						});
						$('#equip_looks #typeListBox #supplyEdit').hide();
						$('#equip_looks #typeListBox #supplyLook').show();
                       
                    },
                    error:function(err){
                        console.log(err);
                        $('.loading').hide();
                    }
                });
	}
	$('body').delegate('#typeListBox #Save','click',function(){
		var datas=[]
		var objname={}
		var f_form_module_id=$('#typeListBox .modalID').html()
		
			$('#typeListBox table .list_name').each(function(){
				var listName=$(this).html();
				var listValue=$(this).parent('td').next().children().val();
				var list1={
					"check_name":listName,
					"check_value":listValue
				}
				datas.push(list1)
				
			})
			var SLOPE=$('#typeListBox table #SLOPE').val();
			var INTERCEPT=$('#typeListBox table #INTERCEPT').val();
			var COEFFICIENTR=$('#typeListBox table #COEFFICIENTR').val();
			var ZERODRIFT=$('#typeListBox table #ZERODRIFT').val();
			var SPANDRIFT=$('#typeListBox table #SPANDRIFT').val();
			var ZERODRIFTSATISFY=$('#typeListBox table #ZERODRIFTSATISFY').val();
			var SPANDRIFTSATISFY=$('#typeListBox table #SPANDRIFTSATISFY').val();
			var check_name2=$('#typeListBox table .list_name2').html();
			var check_name3=$('#typeListBox table .list_name3').html();
			var check_name4=$('#typeListBox table .list_name4').html();
				var list1={
					"check_name":check_name2+"_01",
					"check_value":SLOPE
				}
				datas.push(list1)
				var list2={
					"check_name":check_name2+"_02",
					"check_value":INTERCEPT
				}
				datas.push(list2)
				var list3={
					"check_name":check_name2+"_03",
					"check_value":COEFFICIENTR
				}
				datas.push(list3)
				var list4={
					"check_name":check_name3+"_01",
					"check_value":ZERODRIFT
				}
				datas.push(list4)
				var list5={
					"check_name":check_name3+"_02",
					"check_value":SPANDRIFT
				}
				datas.push(list5)
				var list6={
					"check_name":check_name4+"_01",
					"check_value":ZERODRIFTSATISFY
				}
				datas.push(list6)
				var list7={
					"check_name":check_name4+"_02",
					"check_value":SPANDRIFTSATISFY
				}
				datas.push(list7)
			console.log(datas)
			var tasks=[];
			$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	
		objname={datas,f_form_module_id:f_form_module_id,task}
		$.ajax({
			type:"put",
			url:'/get_form_module/'+id,
			data:objname,
			success:function(data){
				$('#equip_writings .edit_jobs').show();
				$('#equip_writings #typeListBox').hide();
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
	})
$('body').delegate('#equip_writings #typeListBox #Back','click',function(){
		$('#equip_writings .edit_jobs').show();
		$('#equip_writings #typeListBox').hide();
	})
	$('body').delegate('#equip_looks #typeListBox #Back','click',function(){
		$('#equip_looks #fault_jobs_look').show();
		$('#equip_looks #typeListBox').hide();
	})

	//编辑保存
	$('body').delegate('#equip_writings #editSave','click',function(){
		var repair=$('#equip_writings #repairContents input[type="radio"]:checked').val()
		console.log(repair)
		var image_file=$('#prvid img').attr("src")
		$.ajax({
			type:"put",
			url:'/equip_mains/'+id + '.json',
			data:{
				maint_name:repair,
				image_file:image_file
			},
			success:function(data){
				page_state = "change";
				alert("提交成功");
				originData();
				$('#equip_writings').hide();
				$('#equip_mains').show();
				$('#equip_looks').hide();
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	//详细
	 var id;
	$('body').delegate('#equip_mains table .lookFor','click',function(){
		$('#equip_mains').hide();
		$('#equip_writings').hide();
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:'get',
			url:'/equip_mains/'+id + '.json',
			success:function(data){
				var dataObj = {
					editData:data
				}
				var originData = template('equipLook',dataObj)
				$('#equip_looks').html(originData)
				$('#equip_looks').show();
				DeviceTypeName=data.equilp.equip_type;
				var remote=data.handle_remote;
				var types=data.handle_type;
				var maintName=data.maint_project.maint_name;
				var status=data.job_status;
				var chekAudit=data.audit_if;
				var libg=data.flow;
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					$("#equip_looks .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				}
 				$('#equip_looks .dTaskTop li').each(function(i){
   					
   					var libg=$(this).find('.lbg-txt').text();
   					if(status==libg){
   						
   						var bgs=$(this).index()
   						var lis = $('#equip_looks .dTaskTop li');
   						for(var j=0;j<=bgs;j++){
							lis[j].className="on";
   						}
   					}
   				})
				$('#equip_looks #repairContents input[type="radio"]').each(function(i){
					var names=$(this).val();
					if(maintName==names){
						$(this).prop('checked',true)	
    					return false;
					}
				})
				if(chekAudit=="Y"){
					$('#equip_looks #audit_des').hide();
					$('#equip_looks .checkbox-radio #Approve_Y').prop('checked',true);
				}else{
					$('#equip_looks #audit_des').show();
					$('#equip_looks #ApproveProcessType').hide();
					$('#equip_looks .checkbox-radio #Approve_N').prop('checked',true);
				}
				if(status=="设备待维修"){
					$('#equip_looks #repair').hide();
					$('#equip_looks #audit').hide();
				}else if(status=="工单待审核"){
					$('#equip_looks #audit').hide();
				}else if(status=="创建工单"){
					$('#equip_looks #audit').hide();
					$('#equip_looks #guzhang').hide();
				}else if(status=="工单待处理"){
					$('#equip_looks #audit').hide();
					$('#equip_looks #guzhang').hide();
				}
				if(status=='设备待校准'){
					$('#equip_looks #audit').hide();
				}
				if(remote=="Y"){
					$('#equip_looks .IsScene #IsSceneY').prop('checked',true)
					$('#equip_looks #DeviceInfo').hide();
				}else{
					$('#equip_looks .IsScene #IsSceneN').prop('checked',true)
				}
				
				if(types=="现场维修"){
					$('#equip_looks #DeviceInfo #ProcessX').prop('checked',true)
					$('#equip_looks #repair').hide();
				}else if(types=="更换备机"){
					$('#equip_looks #DeviceInfo_Code').show();
					$('#equip_looks #DeviceInfo #ProcessG').prop('checked',true)
					$('#equip_looks #ApproveProcessType').show();
				}else{
					$('#equip_looks #DeviceInfo #ProcessQ').prop('checked',true)
					$('#equip_looks #repair').hide();
				}
				if(types=="更换备机" && chekAudit=="N"){
					$('#equip_looks #ApproveProcessType').hide();
				}
				if(status=="设备待维修" && types=="更换备机"){
					$('#equip_looks #repair').hide();
				}
				
//				$('#equip_looks img').zoomify();
				var imgs = document.getElementsByTagName("img");
				imgs[0].focus();
				for(var i = 0;i<imgs.length;i++){
					imgs[i].onclick = expandPhoto;
					imgs[i].onkeydown = expandPhoto;
				}
				typeNames(DeviceTypeName)
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	function typeNames(DeviceTypeName){
			if(DeviceTypeName=="o3"){
						$('#FaultContent .typeName span').html("臭氧（O3）分析仪运行状况检查记录表")
					}else if(DeviceTypeName=="pm10"){
						$('#FaultContent .typeName span').html("颗粒物PM10自动监测分析仪运行状况检查记录")
					}else if(DeviceTypeName=="pm2.5"){
						$('#FaultContent .typeName span').html("颗粒物PM2.5自动监测分析仪运行状况检查记录")
					}else if(DeviceTypeName=="so2"){
						$('#FaultContent .typeName span').html("二氧化硫（SO2）分析仪运行状况检查记录表")
					}else if(DeviceTypeName=="no2"){
						$('#FaultContent .typeName span').html("氮氧化物（NOX）分析仪运行状况检查记录表")
					}else if(DeviceTypeName=="co"){
						$('#FaultContent .typeName span').html("一氧化碳（CO）分析仪运行状况检查记录表")
					}
		 }
//放大图片
function expandPhoto(){
	var overlay = document.createElement("div");
	overlay.setAttribute("id","overlay");
	overlay.setAttribute("class","overlay");
	document.body.appendChild(overlay);

	var img = document.createElement("img");
	img.setAttribute("class","overlayimg");
	img.src = this.getAttribute("src");
	document.getElementById("overlay").appendChild(img);

	img.onclick = restore;
}
function restore(){
	document.body.removeChild(document.getElementById("overlay"));
	document.body.removeChild(document.getElementById("img"));
}
	$('body').delegate('#look_back','click',function(){
		$('#equip_mains').show();
		$('#equip_looks').hide();
	})
//分页
    function originData(param){
//		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originDatas = template('equipTables',data_hash)
			$('#equip_mains #public_table table .exception-list').html(originDatas);
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
			data_hash = {};
			 var station_ids = $('#equip_mains .public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
			$.ajax({
				url: '/equip_mains.json?page=' + pp,
		    	type: 'get',
		    	data:{
					created_time:$('#equip_mains .public_search .startTime1').val(),
					end_time:$('#equip_mains .public_search .endTime1').val(),
					d_station_id:station_ids,
					job_no:$('#equip_mains .public_search .searchNo').val(),
					author:$('#equip_mains .public_search .searchAuthor').val(),
					handle_man:$('#equip_mains .public_search .searchCrea').val(),
					job_status:$('#equip_mains .public_search .searchStatus').val(),
					create_type:$('#equip_mains .public_search .searchCrea').val()
					
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var equipTable = template('equipTables',data)
		    		$('#equip_mains #public_table table .exception-list').html(equipTable)
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
					$('#equip_mains .public_table .exception-list .job_status').each(function(i){
				       	var s=$(this).text()
				       	if(s=="工单待审核"){
				       		$(this).siblings().find('.writing').hide()
				       	}else if(s=="工单已审核"){
				       		$(this).siblings().find('.writing').hide()
				       	}else if(s=="设备待校准"){
				       		$(this).siblings().find('.writing').hide()	
				       	}
			       	})
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url: "/equip_mains.json",
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
		    var equipTable = template('equipTables',data)
		    $('#equip_mains #public_table table .exception-list').html(equipTable)
	        $("#page").initPage(total, 1, originData)
	        $('.page_total span small').text(total)
	       $('#equip_mains .public_table .exception-list .job_status').each(function(i){
		       	var s=$(this).text()
		       	if(s=="工单待审核"){
		       		$(this).siblings().find('.writing').hide()
		       	}else if(s=="工单已审核"){
		       		$(this).siblings().find('.writing').hide()
		       	}else if(s=="设备待校准"){
		       		$(this).siblings().find('.writing').hide()	
		       	}
	       })
		},
		error:function(err){
			console.log(err)
		}
	})
})

function previewImage(file, prvid) { 
	/* file：file控件 
	* prvid: 图片预览容器 
	*/ 
	var tip = "Expect jpg or png or gif!"; // 设定提示信息 
	var filters = { 
	"jpeg" : "/9j/4", 
	"gif" : "R0lGOD", 
	"png" : "iVBORw" 
	} 
	var prvbox = document.getElementById(prvid); 
	prvbox.innerHTML = ""; 
	if (window.FileReader) { // html5方案 
		for (var i=0, f; f = file.files[i]; i++) { 
			var fr = new FileReader(); 
			fr.onload = function(e) { 
				var src = e.target.result; 
				if (!validateImg(src)) { 
					alert(tip) 
				} else { 
					showPrvImg(src); 
				} 
			} 
			fr.readAsDataURL(f); 
		} 
	} else { // 降级处理
	
		if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) { 
			alert(tip); 
		} else { 
			showPrvImg(file.value); 
		} 
	} 
	
		function validateImg(data) { 
			var pos = data.indexOf(",") + 1; 
			for (var e in filters) { 
				if (data.indexOf(filters[e]) === pos) { 
					return e; 
				} 
			} 
			return null; 
		} 
	
		function showPrvImg(src) { 
			var img = document.createElement("img"); 
			img.src = src; 
			prvbox.appendChild(img); 
			$('#prvid img').zoomify();
		} 
}