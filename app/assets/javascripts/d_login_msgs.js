 $(function(){
// 	获取角色权限
    $('#loading').show();
    function role(){
		$.ajax({
			type:'get',
			url:'/s_role_msgs.json?all=sqyw',
			success:function(data){
				$('#loading').hide()
				var roleTemp = template('roleTemp',data);
				$('#loginMsgs  #role_id').html(roleTemp);
			},
			error:function(err){
				console.log(err)
			}
		})
 	}
    role();

   //角色单选
     function roleRadio(){
     	$.ajax({
			type:'get',
			url:'/s_role_msgs.json?all=sqyw',
			success:function(data){
				$('#loading').hide()
				var roleTemp2 = template('roleTemp2',data);
				$('.new_d_login_msg #roleId .notice_role').html(roleTemp2);
//				$('.public_area .area_content  .edit_form #roleIds #role_ids').html(roleTemp2);
				
			
			},
			error:function(err){
				console.log(err)
			}
		})
     }
    roleRadio();
 	$('body').delegate(".public_area h4 span","click",function(){
 		$('.public_box').hide();
 		$('.single-station').hide();
 		$('#search-units .choose-sites').hide();
 		$('.area_content .form-group .col-xs-5 input').val("");
 		
 	})

    //删除


var id;
var login_no;
$("body").delegate("#loginMsgs table tbody .d_login_delete","click",function() {
	id=Number($(this).parents('tr').find('.id').text())
	login_no=$(this).parents('tr').find('.login_no').text()
	$('#sureCannel').show();
	var sure_str = '<div class="point-out">确认删除 <span>"' + login_no + '"</span> 吗？</div>';
    $('#sureCannel .sureCannel-body').html(sure_str);

    var sureCannelFoot_str = '';
   
    sureCannelFoot_str += '<button class="btn-sure" id="login_delete">确认</button><button onclick="resetPswd_cannel()" class="btn-cannel">取消</button>'

    $('#sureCannel .box-footer').html(sureCannelFoot_str);
})
$("body").delegate("#login_delete","click",function() {
	$('#sureCannel').hide();
	$.ajax({
			type:"delete",
			url:'/d_login_msgs/'+id+'.json',
			success:function(data){
				page_state = "change";
				 $("#success").show().delay(3000).hide(300);
	            var successFoot_str = '';
	            successFoot_str += '<div class="resetPs1"><span class="resetPs1-span">" ' + login_no + ' "</span>，删除成功！</div>'
	
	            $('.success-footer').html(successFoot_str)
				getOriginalData();
			
			},
			error:function(err){
				console.log(err)
			}
		})

})
	//分配站点
	$('body').delegate("#new_d_login_msg .choice2","click",function(){
		
		$.ajax({
			type: "get",
			url: "/search_stations.json",
			async: true,
			dataType: "json",
			success:function(data){
				var htmlStr = template('allot',data);
				$('#single-station2 .content').html(htmlStr)
			}
		})
	
	})
	$('body').delegate(".edit_form .choice2","click",function(){
		$.ajax({
			type: "get",
			url: "/search_stations.json",
			async: true,
			dataType: "json",
			success:function(data){
				var htmlStr = template('allot',data);
				$('#single-station2 .content').html(htmlStr)
			}
		})
		
	})
	
	//全选
	$('body').delegate('#loginMsgs #single-station2  .checkall input','click',function(){
		$(this).parents('#single-station2').find('.tab-station p input').prop('checked',this.checked);
	})
	$('body').delegate('#single-station2 .station-one  p input','click',function(){
		$(this).parents('.tab-station').find('.station-two p input').prop('checked',this.checked);
	})
		$('body').delegate('#fenModal #fenSave','click',function(){
			if($('#single-station2 .station-two p input[type="checkbox"]:checked')){
				var text=[];
				var textid=[];
				$('#single-station2 .station-two p input[type="checkbox"]:checked').siblings('span').each(function(){
					text.push(this.innerHTML)
				})
				$('#single-station2 .station-two p input[type="checkbox"]:checked').siblings('small').each(function(){
					textid.push(this.innerHTML)
				})
				var allText = text.join(',')
				var textid=textid.join(',');
				$('#addModal #allot_station').val(allText)
				$('#addModal #d_login_msg_station_id').val(textid)
				$('#editModal #allot_station').val(allText)
				$('#editModal #d_login_msg_station_id').val(textid)
			}else{
				$('#addModal #allot_station').val('')
				$('#addModal #d_login_msg_station_id').val('')
				$('#editModal #allot_station').val('')
				$('#editModal #d_login_msg_station_id').val('')
			}
			$('#fenModal').modal('hide')
		})


	$('body').delegate(".edit_form .choice","click",function(){
		var textId=$('#d_login_msg_group_id').val()
//		console.log( textId)
		$('.single-station .regionCode .regionCode_content input').each(function(){
    		var cId=$(this).val();
//  		console.log(cId)
    		if(cId==textId){
    		
    			$(this).prop('checked',true)	
    			return false;
    		}
    		
    	})
		
		$('.single-station').show();
	})
	//查询
	var login_name='';
	var role_id='';
	function pageName_Data(){
		var searchName=$('#loginMsgs  #login_name').val();
		var searchId=$('#loginMsgs  #role_id option:selected').val();
		$('.public_search .searchName').val(searchName);
		$('.public_search .searchId').val(searchId);
		login_name = $('.public_search .searchName').val();
		role_id = $('.public_search .searchId').val();
//		d_login_msgs = 'login_name='+login_name+'&role_id='+role_id
	}
	$(' .search').click(function(){
		$('#loading').show()
//		login_name = $('#loginMsgs  #login_name').val();
//		role_id = $('#loginMsgs  #role_id option:selected').val();
//		d_login_msgs = 'login_name='+login_name+'&role_id='+role_id;
		login_name='';
		role_id='';
		pageName_Data()
		$.ajax({
			type: "get",
			url: "/d_login_msgs.json",
			dataType: "json",
			data:{
				login_name:login_name,
				role_id:role_id
			},
			success: function(data) {
				data_hash = data
	   		 	$("#page").initPage(data.list_size, 1, getOriginalData);
		   		$('.page_total small').text(data.list_size);
		   		$('#loading').hide()
	   		 	
			}
		})
	})
	//编辑
	var id;
	var alarmRole_a;
	$('body').delegate(".d_login_edit","click",function(){
		
		id = $(this).parents('tr').find('.id').text();
		alarmRole_a=$(this).parents('tr').find('.alarm_role').text();
		var stations;
		$.ajax({
			type: "get",
			url: "/d_login_msgs/"+ id +".json",
			async: false,
			dataType: "json",
			success: function(data) {
				console.log(data)
//				var dataObj = {
//					editData:data
//				}
//				var htmlStr = template('d_login_edit',dataObj);
//				$('.area_content').html(htmlStr);
				groupId=data.d_login_msg.group_id;
				role_id=data.d_login_msg.role_id;
				role_code=data.d_login_msg.role_code;
				region_id=data.d_login_msg.region_id;
				region_code=data.d_login_msg.id;
				var login_no=data.d_login_msg.login_no;//登录账号
				var login_name=data.d_login_msg.login_name;//姓名
				var unit_name=data.d_login_msg.unit_name;//所属单位
				var region_codeID=data.d_login_msg.id;//单位ID
				var id_card=data.d_login_msg.id_card;//身份证号
				var work_num=data.d_login_msg.work_num;//工作证编号
				var email=data.d_login_msg.email;//email
				var telphone=data.d_login_msg.telphone;//电话号码
				$('.edit_form .form-group #login_Name').val(login_name);
				$('.edit_form .form-group .unit_ywIDs').val(region_codeID);
				$('.edit_form .form-group .unit_yws').val(unit_name);
				$('.edit_form .form-group #login_no').val(login_no);
				$('.edit_form .form-group #id_card').val(id_card);
				$('.edit_form .form-group #work_num').val(work_num);
				
				$('.edit_form .form-group .email').val(email);
				$('.edit_form .form-group #telphone').val(telphone);
			
				
    			$('.single-station').hide();
//				$('.public_box').show();
				//选中角色
				$.ajax({
					type:'get',
					url:'/s_role_msgs.json?all=sqyw',
					success:function(data){
						$('.loading').hide()
						var roleTemp2 = template('roleTemp2',data);
						$('.edit_form #roleIds #role_ids').html(roleTemp2);
						var numbers = $(".edit_form #roleIds #role_ids").find("option"); //获取select下拉框的所有值
						for (var j = 1; j < numbers.length; j++) {
							if ($(numbers[j]).val() == role_id) {
									$(numbers[j]).attr("selected", "selected");
								}
						}
					
					},
					error:function(err){
						console.log(err)
					}
				})
				if(role_code=="mechanic_repair"){
			    		$('#editModal #shop_name').show()
			    	}else{
			    		$('#editModal #shop_name').hide()
			    	}
				//选中所属单位
				var checkFather;
				var fatherId=data.d_login_msg.father_id;
				if(fatherId==null){
					checkFather=groupId
				}else{
					checkFather=fatherId
				}
				console.log(checkFather)
				$.getJSON('/d_login_msgs/get_branchs').done(function(data){
					var dataObj = {
								Data:data
							};
					var stationTemp = template('unit_region',dataObj);
					$('.edit_form #areaAffiliation').html(stationTemp);
					$(".edit_form .form-group #areaAffiliation").find("option[value = '"+checkFather+"']").attr("selected","selected");

				})
				//			选中部门
					$.ajax({
						type:"get",
						url:"/d_login_msgs/get_region_codes.json",
						async:true,
						data:{
				               id:checkFather
				            },
				            success:function(data){
				            	var dataObj = {
									Data:data
								};
						
							var dataHtml= template('departHtml',dataObj);
							$('#editModal #department').html(dataHtml);
							$(".edit_form .form-group #department").find("option[value = '"+groupId+"']").attr("selected","selected");
				            },
				            error:function(err){
				            	console.log(err)
				            }
					});
				
				//选中车间区域
				areaName=data.d_login_msg.area_name
				console.log(areaName)
				$.ajax({
		            type:"get",
		            url:"/search_region_codes_areas",
		            data:{
		                region_id:groupId
		            },
		            success:function(data){
		                var result = template('workname',data);
		                $('#editModal #workshop').html(result);
		                var area_name = $(".edit_form .form-group #workshop").find("option"); //获取select下拉框的所有值
		                $(".edit_form .form-group #workshop").find("option[text = '"+areaName+"']").attr("selected","selected");
//						for (var j = 1; j < area_name.length; j++) {
//							if ($(area_name[j]).text() ==areaName ) {
//									$(area_name[j]).attr("selected", "selected");
//								}
//						}
		            },
		            error:function(err){
		                console.log(err)
		            }
		        });
//				regionCodes(region_id)
				
//				$('.edit_form  .edit_form #roleIds #role_ids .s_role_msg_id').each(function(){
//					alert("each")
//				})
				getTime();
//				formValid();
//				editChioce()
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	//添加车间区域
    $('body').delegate('#addModal #department','change',function(){
    
        $.ajax({
            type:"get",
            url:"/search_region_codes_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('workname',data);
                $('#addModal #workshop').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
    //修改车间区域
    $('body').delegate('#editModal #department','change',function(){
    
        $.ajax({
            type:"get",
            url:"/search_region_codes_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('workname',data);
                $('#editModal #workshop').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
     $('body').delegate('#addModal .notice_role','change',function(){
    	var code=$(this).children('option:selected').attr('value1');
    	if(code=="mechanic_repair"){
    		$('#addModal #shop_name').show()
    	}else{
    		$('#addModal #shop_name').hide()
    	}
    })
     $('body').delegate('#editModal #role_ids','change',function(){
    	var code=$(this).children('option:selected').attr('value1');
    	if(code=="mechanic_repair"){
    		$('#editModal #shop_name').show()
    	}else{
    		$('#editModal #shop_name').hide()
    	}
    })
    
	//增加所属单位
	$('#add').click(function() {
		$.getJSON('/d_login_msgs/get_branchs').done(function(data){
		var dataObj = {
					Data:data
				};
		
		var stationTemp = template('unit_region',dataObj);
		$('#addModal #areaAffiliation').html(stationTemp);
	})
//	所属部门
$('body').delegate('#addModal #areaAffiliation','change',function(){
	$.ajax({
		type:"get",
		url:"/d_login_msgs/get_region_codes.json",
		async:true,
		data:{
               id:$(this).val()
            },
            success:function(data){
            	var dataObj = {
					Data:data
				};
		
			var dataHtml= template('departHtml',dataObj);
			$('#addModal #department').html(dataHtml);
            },
            error:function(err){
            	console.log(err)
            }
	});
})
	
	
		roleRadio();
		var dataObj = {};

		getTime();
	});
//	修改所属部门
$('body').delegate('#editModal #areaAffiliation','change',function(){
	$.ajax({
		type:"get",
		url:"/d_login_msgs/get_region_codes.json",
		async:true,
		data:{
               id:$(this).val()
            },
            success:function(data){
            	var dataObj = {
					Data:data
				};
		
			var dataHtml= template('departHtml',dataObj);
			$('#editModal #department').html(dataHtml);
            },
            error:function(err){
            	console.log(err)
            }
	});
})
	

	
	$(document).ready(function() {
	    $('#new_d_login_msg').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            login_no: {
	                validators: {
	                    notEmpty: {
	                        message: '登录账号不能为空'
	                    },
	                    regexp: {
							regexp: /^[a-zA-Z0-9_\.]+$/,
							message: '只接受数字和字母 '
						}
	                }
	            },
	            login_name: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	            password:{
                    message:'密码非法',
                    validators:{
                        notEmpty:{
                            message:'密码不能为空'
                        },
//                        限制字符串长度
                        stringLength:{
                            min:3,
                            max:20,
                            message:'密码长度必须位于3到20之间'
                        },
//                        相同性检测
                        identical:{
//                            需要验证的field
                            field:'password',
                            message:'两次密码输入不一致'
                        },
//                        基于正则表达是的验证
                        regexp:{
                            regexp:/^[a-zA-Z0-9_\.]+$/,
                            message:'密码由数字字母下划线和.组成'
                        }
                    }
                },

                //                确认密码
                password_confirmation:{
                    message:'密码非法',
                    validators:{
                        notEmpty:{
                            message:'密码不能为空'
                        },
//                        限制字符串长度
                        stringLength:{
                            min:3,
                            max:20,
                            message:'密码长度必须位于3到20之间'
                        },
//                        相同性检测
                        identical:{
//                            需要验证的field
                            field:'password',
                            message:'两次密码输入不一致'
                        },
//                        基于正则表达是的验证
                        regexp:{
                            regexp:/^[a-zA-Z0-9_\.]+$/,
                            message:'密码由数字字母下划线和.组成'
                        }
                    }
                },
                unit_yws: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	            unit_yws: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
//	            id_card: {
//                  message:'身份证验证失败',
//                  validators: {
//                      notEmpty: {
//                          message: '身份证不能为空'
//                      },
//                      stringLength: {
//                          min: 18,
//                          max: 18,
//                          message: '请输入18位身份证号码'
//                      },
//                      regexp: {
//                          regexp: /^[1-9]{1}[0-9]{16}[xX1-9]{1}$/,
//                          message: '请输入正确的身份证号码'
//                      }
//                  }
//              },
                s_role_msg_id: {
                 validators: {
                    notEmpty: {
                        message: '请选择角色'
                     }
                 }
             	},
             	work_num: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	            telphone: {
                 message: 'The phone is not valid',
                 validators: {
                     notEmpty: {
                         message: '手机号码不能为空'
                     },
                     stringLength: {
                         min: 11,
                         max: 11,
                         message: '请输入11位手机号码'
                     }
                    
                 }
             },
	            email: {
                    validators: {
                        notEmpty: {
                            message: '邮箱不能为空'
                        },
                        emailAddress: {
                            message: '邮箱地址格式有误'
                        }
                    }
                }
	        }
	    });
	});
	
	//注册
	$('body').delegate(".register","click",function(){
		var addValidator = $('#new_d_login_msg').data('bootstrapValidator');
		addValidator.validate();
		
//		var regionCode = $('#new_d_login_msg .areaAffiliation input').val();
		var login_no=$('#new_d_login_msg .form-group #login_no').val();
		var login_name=$('#new_d_login_msg .form-group #login_Name').val();
		var password=$('#new_d_login_msg .form-group #password').val();
		var password_confirmation=$('#new_d_login_msg .form-group #password_confirmation').val();
		var s_region_code_info_id;
		s_region_code_info_id=$('#new_d_login_msg .form-group #department option:selected').val();
		var s_role_msg_id=$('#new_d_login_msg .form-group #roleId option:selected').val();
		var id_card=$('#new_d_login_msg .form-group #id_card').val();
		var work_num=$('#new_d_login_msg .form-group #work_num').val();
		var valid_flag=$('#new_d_login_msg .form-group #d_login_msg_valid_flag option:selected').val();
		var work_status=$('#new_d_login_msg .form-group #incumbency option:selected').val();
		var email=$('#new_d_login_msg .form-group .email').val();
		var telphone=$('#new_d_login_msg .form-group #telphone').val();
		var station_ids=$('#new_d_login_msg .form-group .stationID').val();
		var s_area_id=$('#new_d_login_msg .form-group #workshop option:selected').val();
		if(s_region_code_info_id==""){
			s_region_code_info_id=$('#new_d_login_msg .form-group #areaAffiliation option:selected').val()
		}else{
			s_region_code_info_id=$('#new_d_login_msg .form-group #department option:selected').val();
		}
		if (addValidator.isValid()){
			$.ajax({
				type: "post",
				url: "/d_login_msgs.json",
				async: true,
				dataType: "json",
				data:{
					d_login_msg:{
						login_no:login_no,
						login_name:login_name,
						password:password,
						password_confirmation:password_confirmation,
						s_region_code_info_id:s_region_code_info_id,
						s_role_msg_id:s_role_msg_id,
						id_card:id_card,
						work_num:work_num,
						valid_flag:valid_flag,
						work_status:work_status,
						email:email,
						telphone:telphone,
						station_ids:station_ids,
						s_area_id:s_area_id
					},
		
				},
				success: function(data) {
					$('#addModal').modal('hide')
	                page_state = "change";
					getOriginalData()

					$('#new_d_login_msg').data('bootstrapValidator').resetForm(true);
					alert("创建成功")
				},
				error:function(err){
					console.log(err)
					alert("创建失败")
				}
			})
		}
		
	})
	$('.modal-header .close').click(function(){
		$('#search-units .choose-sites').hide();
	})
	$('.modal-footer .btn-default').click(function(){
		$('#search-units .choose-sites').hide();
	})
	
		$(document).ready(function() {
	    $('#edit_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            
	            login_name: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
                unit_yws: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	            unit_yws: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
//	            id_card: {
//                  message:'身份证验证失败',
//                  validators: {
//                      notEmpty: {
//                          message: '身份证不能为空'
//                      },
//                      stringLength: {
//                          min: 18,
//                          max: 18,
//                          message: '请输入18位身份证号码'
//                      },
//                      regexp: {
//                          regexp: /^[1-9]{1}[0-9]{16}[xX1-9]{1}$/,
//                          message: '请输入正确的身份证号码'
//                      }
//                  }
//              },
                s_role_msg_id: {
                 validators: {
                    notEmpty: {
                        message: '请选择角色'
                     }
                 }
             	},
             	work_num: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	            telphone: {
                 message: 'The phone is not valid',
                 validators: {
                     notEmpty: {
                         message: '手机号码不能为空'
                     },
                     stringLength: {
                         min: 11,
                         max: 11,
                         message: '请输入11位手机号码'
                     }
                     
                 }
             },
	            email: {
                    validators: {
                        notEmpty: {
                            message: '邮箱不能为空'
                        },
                        emailAddress: {
                            message: '邮箱地址格式有误'
                        }
                    }
                }
	        }
	    });
	});
	
	//修改
	$('body').delegate(".edit_btn","click",function(){
		var editValidator = $('#edit_form').data('bootstrapValidator');
		editValidator.validate();
		var regionCode = $('.edit_form .area').val();
		var login_name=$('.edit_form .form-group #login_Name').val();
		var s_region_code_info_id;
		s_region_code_info_id=$('.edit_form .form-group #department option:selected').val();
		var s_role_msg_id=$('.edit_form .form-group #role_ids option:selected').val();
		var id_card=$('.edit_form .form-group #id_card').val();
		var work_num=$('.edit_form .form-group #work_num').val();
		var valid_flag=$('.edit_form .form-group #d_login_msg_valid_flag option:selected').val();
		var work_status=$('.edit_form .form-group #incumbency option:selected').val();
		var email=$('.edit_form .form-group .email').val();
		var telphone=$('.edit_form .form-group #telphone').val();
		var station_ids=$('.edit_form .form-group .stationID').val();
		var s_area_id=$('.edit_form .form-group #workshop option:selected').val();
		if(s_region_code_info_id==""){
			s_region_code_info_id=$('.edit_form .form-group #areaAffiliation option:selected').val();
		}else{
			s_region_code_info_id=$('.edit_form .form-group #department option:selected').val();
		}
		if (editValidator.isValid()){
			$.ajax({
				type: "put",
				url: "/d_login_msgs/"+id+".json",
				async: true,
				dataType: "json",
				data:{
					d_login_msg:{
						login_name:login_name,
						s_region_code_info_id:s_region_code_info_id,
						s_role_msg_id:s_role_msg_id,
						id_card:id_card,
						work_num:work_num,
						valid_flag:valid_flag,
						work_status:work_status,
						email:email,
						telphone:telphone,
						station_ids:station_ids,
						s_area_id:s_area_id
					},
			
				},
				success: function(data) {
					$('#editModal').modal('hide')
	                page_state = "change";
					getOriginalData();
					alert("修改成功")
				
				},
				error:function(err){
					console.log(err)
					alert("修改失败")
				}
			})
		}
		
	})
	$('body').delegate(".area","focus",function(){
		this.blur();
	})
	$('body').delegate("#ip_address","focus",function(){
		this.blur();
	})

	function getTime(){
		var time = new Date();
		var year = time.getFullYear();
		var month = time.getMonth()+1;
		$('.dateTimepicker').datetimepicker({
				format:"Y-m-d",
				minDate:'2017-01-01',
				maxDate: year+'-12-31',
				todayButton:true,
				timepicker:false
		});	
	}

//	分页+初始数据
	function getOriginalData(param){
//		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
	    	var htmlStr = template('d_login_msgs',data_hash);
			$('table .login_msg').html(htmlStr)
			$('.page_total span a').text(pp);
		}else{
			$('#loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: "/d_login_msgs.json?page="+pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					login_name:$('.public_search .searchName').val(),
					role_id:$('.public_search .searchId').val()
				},
		    	success:function(data){
		    	
		    		$('#loading').hide()
		    		
		    		var htmlStr = template('d_login_msgs',data);
					$('table .login_msg').html(htmlStr)
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
					
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/d_login_msgs.json').done(function(data){
		var total = data.list_size;
		data_hash = data;
	    $("#page").initPage(total, 1, getOriginalData);
	    $('.page_total span small').text(total)
	})
     init_index();
     function init_index(){
         $.getJSON('/d_login_msgs.json').done(function(data){
             var total = data.list_size;
             data_hash = data;
             $("#page").initPage(total, 1, getOriginalData);
             $('.page_total span small').text(total)
         })
     }
     //每页显示更改
    $("#pagesize").change(function(){
    	$('.loading').show()
    	var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/d_login_msgs.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('#loading').hide()
                var total = data.list_size;
                 $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, getOriginalData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })


//重置密码	
var id_fix;
var login_no;
	$('body').delegate(".d_login_reset","click",function(){
		id_fix=Number($(this).parents('tr').find('.id').text());
		login_no=$(this).parents('tr').find('.login_no').text();
		console.log(id_fix)
		$('#sureCannel').show();
	
	var sure_str='<div class="point-out">是否重置 <span>"'+login_no+'"</span> 的密码？</div>';
	$('#sureCannel .sureCannel-body').html(sure_str);
	
	var sureCannelFoot_str='';
	sureCannelFoot_str+='<button class="btn-sure reset_btn">确认</button><button onclick="resetPswd_cannel()" class="btn-cannel">取消</button>'
	$('#sureCannel .box-footer').html(sureCannelFoot_str);		
//		if(confirm("确定要重置密码吗？")){
//			$.ajax({
//			type:"PATCH",
//			url:"/reset_pds/"+ id_fix+".json",
//			async:true,
//			data:{
//					re_pw:true
//			},
//			success:function(data){
//				$('.reset_passwd').hide();
//				alert('重置密码成功，新密码为：123456')
//			},
//			error:function(err){
//				console.log(err)
//			}
//		});
//		}
		
	})
	$('body').delegate(".reset_btn","click",function(){

		resetPswd_sure(id_fix,login_no)
	})


   
	
}) 	

//重置密码弹框
//function resetPswd(id,login_no){
//	$('#sureCannel').show();
//	
//	var sure_str='<div class="point-out">是否重置 <span>"'+login_no+'"</span> 的密码？</div>';
//	$('#sureCannel .sureCannel-body').html(sure_str);
//	
//	var sureCannelFoot_str='';
//	sureCannelFoot_str+='<button class="btn-sure" onclick="confirm_delete('+id+','+"'"+login_no+"'"+')">确认</button><button onclick="resetPswd_cannel()" class="btn-cannel">取消</button>'
//	$('#sureCannel .box-footer').html(sureCannelFoot_str);				
//}

//重置密码弹框结束
//重置密码确认
function resetPswd_sure(id,login_no){
	$.ajax({
				type:'PATCH',
				url:'/reset_pds/'+id+'.json',
				async:true,
				data:{
						re_pw:true
				},
				success:function(data){
					$('#sureCannel').hide();
	
					var successFoot_str='';
					successFoot_str+='<div class="resetPs1"><span class="resetPs1-span">" '+ login_no+' "</span>，密码重置成功！</div>'
					successFoot_str+='<div class="resetPs2">重置密码为：123456</div>'
					$('.success-footer').html(successFoot_str)
					
					 $("#success").show().delay(3000).hide(300);
					$('#success .success-box').css("width","300px")
					$('#success .success-main').css("width","260px")
					
				},
				error:function(err){
					console.log(err)
					$('#sureCannel').hide();
					$('#success').show();
					var successFoot_str='';
					successFoot_str+='<div class="resetPs1"><span class="resetPs1-span">" '+ login_no+' "</span>重置密码失败</div>'
					$('.success-footer').html(successFoot_str)
				}
			});
}
//重置密码确认结束

//重置密码弹框消失
function resetPswd_cannel(){
	$('#sureCannel').hide();

}