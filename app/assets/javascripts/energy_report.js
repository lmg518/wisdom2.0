$(function(){
		//获取当前时间
	var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates;
    console.log(datestamp)
 
    //  首页获取数据

	
$('.loading').show()

   $.ajax({
   	type:"get",
   	url:"/energy_report/water_report",
    success:function(data){
	   	$('#energy_report #public_table .excelTable').html(data)
	   	   $('#energy_report #public_table #water_report .time' ).html(datestamp)
	   	   $("#energy-table").rowspan(0);//合并单元格
	   	   $('.loading').hide()
	   	   //暂存
	   	   $('body').delegate('#energy_report #save','click',function(){
			var status='N'
				submit(status)
			})
	   	   var status=$('#energy_report #public_table #water_report .state').html();
	   	   if(status=="已上报"){
	   	   	$('#energy_report #public_table #water_report .state').css("color","#00923f")

			$('#energy_report #public_table .table_button').hide();
	   	   }
	   },
	   error:function(err){
    		console.log(err)
    	}
   }); 
// 上报
   $('body').delegate('#energy_report #report','click',function(){
   	$('.loading').show()
		var status='Y'
		submit(status)
   })
   var energy_params=[]
	var objname={}
	//暂存和上报所调用的方法
   function submit(status){
   	var date_time=$('#energy_report #public_table #water_report .time').html();
   	var form_arr = $(".form-horizontal").serializeArray();
		var f_form_module_id=$('.excelTable .modalID').html()
			$.each(form_arr, function () {
				field_code=this.name;
				field_value=this.value;
				var list={
					field_code:field_code,
					field_value:field_value
				}
				energy_params.push(list)
			
				console.log(energy_params)
			
			});
			objname={energy_params,date_time:date_time,status:status,id:f_form_module_id}
			console.log(objname)
					
   		$.ajax({
			type:"POST",
			url:'/energy_report',
			data:objname,
			success:function(data){
				console.log(data)
				alert("成功")
				$('.loading').hide()
				window.location.reload();
			},
			error:function(err){
				console.log(err)
			}
		});
   }
})

//合并单元格
jQuery.fn.rowspan = function(colIdx) { //封装的一个JQuery小插件
    return this.each(function(){
        var that;
        $('tr', this).each(function(row) {
            $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                if (that!=null && $(this).html() == $(that).html()) {
                    rowspan = $(that).attr("rowSpan");
                    if (rowspan == undefined) {
                    $(that).attr("rowSpan",1);
                    rowspan = $(that).attr("rowSpan"); }
                    rowspan = Number(rowspan)+1;
                    $(that).attr("rowSpan",rowspan);
                    $(this).hide();
                } else {
                 that = this;
                }
            });
        });
    });
}