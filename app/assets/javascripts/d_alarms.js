$(function(){	
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	$('body').delegate('.add_factor h4 .right','click',function(){
		$('.lookforRules').hide();
		$('#public_box').hide();
	});
	$('body').delegate('.add_factor .area_btn .btn-cancel','click',function(){
		$('#public_box').hide();
		$('.lookforRules').hide();
		$('.lookforRules').find('input').val('');
		$('.lookforRules .factor_content').find('input').val('')
		
	});
	$('body').delegate('.notice_rules .close','click',function(){
		$('#public_box').hide();
	})
	$('body').delegate('.notice_rules .notice_btn .btn-cancel','click',function(){
		$('#public_box').hide();
	})
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	$('.notice_leval').click(function(){
		$('.choose-site').hide()
	})
	$('.search_bottom').click(function(){
		$('.choose-site').hide()
	})
	var time = new Date();
	var year = time.getFullYear();
	var myMonth = time.getMonth()+1;
	var myDate = time.getDate();
	var result = year+'-'+(myMonth<10?'0'+myMonth:myMonth)+'-'+(myDate<10?'0'+myDate:myDate)
	function plicBox(){
	//  报警级别
	    $.getJSON('/s_alarm_levels.json').done(function(data){
	    	var htmlStr=template('notice_leval',data);
	    	$('#d_alarms .public_search .notice_leval').html(htmlStr)
	    })
	//  状态
		$.ajax({
			type:'get',
			url:'/search_by_status.json',
			cache:false,
			success:function(data){
				var htmlStr=template('alarmStatus',data);
	    		$('#d_alarms .public_search .status').html(htmlStr)
			},
			error:function(err){
				console.log(err)
			}
		})
    }
	plicBox();
//	查询
	var station_id;
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	var alarm_level;
	var status;
	function pageName_Data(){

		$('.public_search .searchSta').val($('.control_stationIDs').val())
		if($('.public_search .searchSta').val()){
             station_id = $('.public_search .searchSta').val().split(',')
        }else{
        	 station_id = ''
        }
		$('.public_search .startTime1').val($('.public_search .start_datetime').val())
		$('.public_search .endTime1').val($('.public_search .end_datetime').val())
		start_datetime = $('#d_alarms .public_search .startTime1').val();
    	end_datetime = $('#d_alarms .public_search .endTime1').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
		var searchAlarm=$('.public_search .notice_leval option:selected').val();
		$('.public_search .searchNotice').val(searchAlarm)
		var searchSta=$('.public_search .status option:selected').val();
		$('.public_search .searchStatus').val(searchSta)
		alarm_level = $('.public_search .searchNotice').val();
		status = $('.public_search .searchStatus').val();
	}
	var alarms_arr = {}
	var alarms
	$('.public_search .search').click(function(){
		$('.choose-site').hide()
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/d_alarms.json',
			cache:false,
			data:{
				search:{
					station_ids:station_id,
					alarm_levels:alarm_level,
					data_times:data_times,
					status:status
				}
			},
			success:function(data){
				alarms_arr = data
				data_hash = data
		   		$('.page_total small').text(data.list_size);
				$('.loading').hide();
		    	$("#page").initPage(data.list_size, 1, originData);
			},
			error:function(err){
				console.log(err)
				alert("查询站点过多")
			}
		})
	})
//	查看
	var lookforID;
	$('body').delegate('#d_alarms .public_table .lookfor','click',function(){
		lookforID = $(this).siblings('.id').text();
//		$('#public_box').show();
//		$('#d_alarms .lookforRules').show();
		$.ajax({
			type:'get',
			url:'/data_view/d_alarms/'+lookforID+'.json',
			dataType:'json',
			cache:false,
			success:function(data){
				var htmlStr=template('notice_rules',data);
				$('#lookModal .modal-body').html(htmlStr);
//				var htmlStr2 = template('alarm_news_list',data)
//				$('.exception-list').html(htmlStr2)
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originData = template('originData',data_hash)
			$('.public_table table tbody').html(originData)
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var station_ids = $('.public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
            $.ajax({
				url: '/d_alarms.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	cache:false,
				data:{
					search:{
						station_ids:station_ids,
						alarm_levels:$('.public_search .searchNotice').val(),
						data_times:data_times,
						status:$('.public_search .searchStatus').val()
					}
				},			   
		    	success:function(data){
		    		$('.loading').hide()
		    		var originData = template('originData',data)
					$('.public_table table tbody').html(originData)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	
		
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/d_alarms.json',
		cache:false,
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
		},
		error:function(err){
			console.log(err)
		}
	})
	
    //每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/d_alarms.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            cache:false,
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})
