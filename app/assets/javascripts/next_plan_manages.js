$(function(){
	$('body').delegate('#public_table .btn-group button','click',function(){
//		$(this).addClass('btn-chack');
//		$(this).siblings().removeClass('btn-chack')
	})
	//查询
	var unit_ywID = '';
	var cityName='';
	var edit_status
	function pageName_Data(){
		$('#plan_edit .public_search .searchUnit').val($('.public_search .unit_ywID').val())
		$('#plan_edit .public_search .searchCity').val($('#plan_edit #city').val())
		unit_ywID = $('#plan_edit .public_search .searchUnit').val();
		cityName=$('#plan_edit .public_search .searchCity').val();
		
		edit_status=$('#plan_edit .public_search .searchStatus').val();
	}
		$('.public_search .search').click(function(){
			$('#plan_edit #whole').css({ "color": "#fff", "background": "#558ad8" })
			$('#plan_edit #custom').css({ "color": "#333", "background": "#fff" })
			$('#plan_edit #not_custom').css({ "color": "#333", "background": "#fff" })
			$('#plan_edit #not_report').css({ "color": "#333", "background": "#fff" })
		unit_ywID = '';
		cityName='';
		pageName_Data()
		
		var week_times=$('#plan_edit #weekTime').val()
		console.log(edit_status)
		$.ajax({
			type:'get',
			url:"/next_plan_manages.json?week_time="+week_times,
			data:{
				zone_name:cityName,
				unit_name:unit_ywID,
				edit_status:edit_status
			},
			success:function(data){
				
				$('.loading').hide()
	   		 	var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
				var total = data.list_size;
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
				$('#plan_edit .public_table .exception-list .status').each(function(i){
					var s=$(this).text()
					if(s=="已审核"){
						$(this).css({ "color": "#fff", "background": "#87d66d" })
					}
					if(s=="未制定"){
						$(this).css({ "color": "#fff", "background": "#f19c77" })
					}
					if(s=="待上报"){
						$(this).css({ "color": "#fff", "background": "#7fcff4" })
					}
					
				})
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#index_table .edit','click',function(){
		$('.loading').show()
		$('#next_plan').hide();
		$('#plan_edit').show();
		$('#plan_made').hide();
		var week_time=$(this).parents('tr').find('.id').text();
		var ediytDate=$(this).parents('tr').find('.edit_date').text();
			//	分页+初始数据
		function lookData(param){
		// pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('editTable',data_hash);
			$('#plan_edit .public_table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/next_plan_manages.json?week_time='+week_time+'&page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					
					unit_name: $('#plan_edit .public_search .searchUnit').val(),
					zone_name:$('#plan_edit .public_search .searchCity').val(),
					edit_status:$('#plan_edit .public_search .searchStatus').val()
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var editHtml = template('editTable',data)
					$('#plan_edit .public_table .exception-list').html(editHtml)
					var weekTime=data.week_time;
					$('#plan_edit #weekTime').val(weekTime)
					status();
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
		$.ajax({
			type:"get",
			url:"/next_plan_manages.json?week_time="+week_time,
			success:function(data){
				var total = data.list_size;
				data_hash=data;
			    $("#page").initPage(total, 1, lookData)
			    $('.page_total span small').text(total)
				var weekTime=data.week_time;
				var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
				$('#plan_edit .navTop .tableName').text(ediytDate);
				$('#plan_edit #weekTime').val(weekTime)
				status();
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
		
	})
	function status(){
	    $('#plan_edit .public_table .exception-list .status').each(function(i){
	    	var s=$(this).text()
	    	if(s=="已审核"){
	    		$(this).css({ "color": "#fff", "background": "#87d66d" })
			}
	    	if(s=="未制定"){
	    		$(this).css({ "color": "#fff", "background": "#f19c77" })
			}
	    	if(s=="待上报"){
	    		$(this).css({ "color": "#fff", "background": "#7fcff4" })
			}
	    	
	    })
	}
//	$('body').delegate('#editBback','click',function(){
//		$('#next_plan').show();
//		$('#plan_edit').hide();
//		$('#plan_made').hide();
//	})
//	全部
$('body').delegate('#plan_edit #whole','click',function(){
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	var week_times=$('#plan_edit #weekTime').val()
	edit_status="";
	$.ajax({
			type:"get",
			url:"/next_plan_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status
			},
			success:function(data){
				var weekTime=data.week_time;
				var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
				var total = data.list_size;
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
//				$('#plan_edit .navTop .tableName').text(ediytDate);
				$('.loading').hide()
				
			},
			error:function(err){
				console.log(err)
			}
		});
})
var week_times;
var edit_status; 
//已定制
$('body').delegate('#plan_edit #custom','click',function(){
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	week_times=$('#plan_edit #weekTime').val()
	console.log(week_times)
	
	edit_status=["Y","W"];
	$.ajax({
			type:"get",
			url:"/next_plan_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status
			},
			success:function(data){
				var total = data.list_size;
				var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
				$('.loading').hide()
				
			},
			error:function(err){
				console.log(err)
			}
		});
})

//未定制
$('body').delegate('#plan_edit #not_custom','click',function(){
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	week_times=$('#plan_edit #weekTime').val()
	edit_status=$(this).val();
	$.ajax({
			type:"get",
			url:"/next_plan_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status
			},
			success:function(data){
				var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
				var total = data.list_size;
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
				$('.loading').hide()
				
			},
			error:function(err){
				console.log(err)
			}
		});
})
//未上报
$('body').delegate('#plan_edit #not_report','click',function(){
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	week_times=$('#plan_edit #weekTime').val()
	edit_status=$(this).val();
	$.ajax({
		type:"get",
		url:"/next_plan_manages.json?week_time="+week_times,
		data:{
			edit_status:edit_status
		},
		success:function(data){
			var editHtml = template('editTable',data)
			$('#plan_edit .public_table .exception-list').html(editHtml)
			var total = data.list_size;
			var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
			$('.loading').hide()
			
		},
		error:function(err){
			console.log(err)
		}
	});
})

function ss(pa){
	week_times=$('#plan_edit #weekTime').val()
	$('#plan_edit .public_search .searchUnit').val($('.public_search .unit_ywID').val())
	$('#plan_edit .public_search .searchCity').val($('#plan_edit #city').val())
	unit_ywID = $('#plan_edit .public_search .searchUnit').val();
	cityName=$('#plan_edit .public_search .searchCity').val();
	// pageName_Data()
	$.ajax({
			type:"get",
			url:"/next_plan_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status,
				page:pa,
				unit_name:unit_ywID,
				zone_name:cityName
			},
			success:function(data){
				var total = data.list_size;
				var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$('#plan_edit .public_table .exception-list .status').each(function(i){
					var s=$(this).text()
					if(s=="已审核"){
						$(this).css({ "color": "#fff", "background": "#87d66d" })
					}
					if(s=="未制定"){
						$(this).css({ "color": "#fff", "background": "#f19c77" })
					}
					if(s=="待上报"){
						$(this).css({ "color": "#fff", "background": "#7fcff4" })
					}
					
				})
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
}

	//上报
	$('body').delegate('#plan_edit .edit_report','click',function(){
		$('.loading').show()
		var stationID=$(this).parents('tr').find('.stationID').text();
		var weekTimes=$('#plan_edit #weekTime').val()
		$.ajax({
			type:'get',
			url:"/next_plan_manages/report.json",
			data:{
				week_time:weekTimes,
				d_station_id:stationID
			},
			success:function(data){
				alert("上报成功")
				$('.loading').hide()
	   		 	var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
			},
			error:function(err){
				console.log(err)
				alert("上报失败")
			}
		})
	})
	//运维单位
	$.getJSON('/search_region_codes').done(function(data){
		var stationTemp = template('editUnit',data);
		$('#search-unit .choose-site').html(stationTemp);
	})
	$('#plan_edit .save_back .saveback').click(function(){
		$('#next_plan').show();
		$('#plan_edit').hide();
		$('#plan_made').hide();
	})
	 var id;
	 $('body').delegate('#edit_table .edit_station','click',function(){
	 	 id=$(this).siblings('.id').text();
	 	$('#next_plan').hide();
		$('#plan_edit').hide();
		$('#plan_made').show();
		$.ajax({
			type:"get",
			url:'/next_plan_manages/'+id+'.json',
			success:function(data){
				var w=data.week_pro.week_pro.length;
				var m=data.month_pro.month_pro.length;
				var j=data.jd_pro.jd_pro.length;
				var y=data.year_pro.year_pro.length;
				var wc=data.week_pro.week_pro_checked.length;
				var mc=data.month_pro.month_pro_checked.length;
				var jc=data.jd_pro.jd_pro_checked.length;
				var yc=data.year_pro.year_pro_checked.length;
				var dataObj={
					editData:data
				}
				var editStatus=data.ops_plan.edit_status;
				var madHtml = template('madeTable',dataObj)
				$('#made_table  .exception-list').html(madHtml)
				if(editStatus=="Y"){
					$('#plan_made #madeSave').hide();
					$('#plan_made #made_table .check-item').click(function(event){
						 event.stopPropagation();
					})
				}else{
					$('#plan_made #madeSave').show();
				}
				$('#plan_made #made_table .week_pro').each(function(i){
					$('#plan_made #made_table .week_pro').find('td').eq(0).attr("rowspan",w)
					$('#plan_made #made_table .week_pro').eq(i+1).find('td').eq(0).remove();
				})
				$('#plan_made #made_table .month_pro').each(function(i){
					$('#plan_made #made_table .month_pro').find('td').eq(0).attr("rowspan",m)
					$('#plan_made #made_table .month_pro').eq(i+1).find('td').eq(0).remove();
				})
				$('#plan_made #made_table .year_pro').each(function(i){
					$('#plan_made #made_table .year_pro').find('td').eq(0).attr("rowspan",y)
					$('#plan_made #made_table .year_pro').eq(i+1).find('td').eq(0).remove();
				})
				$('#plan_made #made_table .jd_pro').each(function(i){
					$('#plan_made #made_table .jd_pro').find('td').eq(0).attr("rowspan",j)
					$('#plan_made #made_table .jd_pro').eq(i+1).find('td').eq(0).remove();
				})
				
				
				$('#plan_made #made_table .week_proc').each(function(i){
					$('#plan_made #made_table .week_proc').find('td').eq(0).attr("rowspan",wc)
					$('#plan_made #made_table .week_proc').eq(i+1).find('td').eq(0).remove();
				})
				$('#plan_made #made_table .month_proc').each(function(i){
					$('#plan_made #made_table .month_proc').find('td').eq(0).attr("rowspan",mc)
					$('#plan_made #made_table .month_proc').eq(i+1).find('td').eq(0).remove();
				})
				$('#plan_made #made_table .year_proc').each(function(i){
					$('#plan_made #made_table .year_proc').find('td').eq(0).attr("rowspan",yc)
					$('#plan_made #made_table .year_proc').eq(i+1).find('td').eq(0).remove();
				})
				$('#plan_made #made_table .jd_proc').each(function(i){
					$('#plan_made #made_table .jd_proc').find('td').eq(0).attr("rowspan",jc)
					$('#plan_made #made_table .jd_proc').eq(i+1).find('td').eq(0).remove();
				})
				$('#plan_made #made_table .week_time').each(function(i){
					var weekTimes=$(this).text();
					var jobTime=$(this).parents('tr').find('td .job_time').text();
					if(weekTimes==jobTime){
						$(this).parents('.check-item').addClass('yChack')
					}
				})
				$('.loading').hide()
				
			},
			error:function(err){
				console.log(err)
			}
		});
	 })
	 
	  $('body').delegate('#made_table .check-item','click',function(){
		if($(this).hasClass('yChack')){
			$(this).removeClass('yChack')
		}else{
			$(this).addClass('yChack')
	  		$(this).siblings().removeClass("yChack");
		}

	  })
	$('body').delegate('#madeBback','click',function(){
		$('#next_plan').hide();
		$('#plan_edit').show();
		$('#plan_made').hide();
		$('#search-station .choose-site').hide();
		$('#plan_made .form-group input[type="text"]').val(' ');
	})
	 //	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	 var params=[];
	$('body').delegate('#madeSave','click',function(){
		$('.yChack').each(function(i){
			var checkID=$('.yChack').siblings('td').find('.id').eq(i).text();
			var job_time = $(this).children('.hide').text();
			var obj={
				"id":checkID,
				"job_time":job_time
			}
			params.push(obj)
		})
		var weektime=$('#plan_made table #weekTime').text();
		var stationId=$('#plan_made #public_table .control_stationIDs').val().split(',');
		var objName={plan_details_params:{params},week_time:weektime,station_ids:stationId}
		console.log(stationId)
		if(params.length > 0 ){
			$.ajax({
			type:"put",
			url:'/next_plan_manages/'+id+'.json',
			data:objName,
			success:function(data){
				data_hash = data
				page_state = "change";
//				lookData();
				var editHtml = template('editTable',data)
				$('#plan_edit .public_table .exception-list').html(editHtml)
				status();
				$('#plan_made .form-group input[type="text"]').val(' ');
				$('#next_plan').hide();
				$('#plan_edit').show();
				$('#plan_made').hide();
			},
			error:function(err){
				console.log(err)
			}
		});
		}else{
			alert("请排计划！！")
			return false;
		}
		
	})
	 //初始数据
	 $('.loading').show()
	$.ajax({
		type:'get',
		url:'/next_plan_manages.json',
		dataType:'json',
		success:function(data){
			$('.loading').hide()
			var originData = template('next_plan_table',data)
			$('#next_plan .public_table .exception-list').html(originData)
			
		},
		error:function(err){
			console.log(err)
		}
	})
	 //本月
	$('#this_month').click(function(){
	 	$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
	 	$('.loading').show()
	 	$('#next_plan .public_table .exception-list .currnt_week').show();
	 	$('#next_plan .public_table .exception-list .next_week').hide();
	 	$('.loading').hide()
	})
	 //下月
	$('#next_month').click(function(){
	 	$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
	 	$('.loading').show()
	 	$('#next_plan .public_table .exception-list .currnt_week').hide();
	 	$('#next_plan .public_table .exception-list .next_week').show();
	 	$('.loading').hide()
	})
	 //全部
	$('#whole').click(function(){
	 	$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
	 	$('.loading').show()
	 	$('#next_plan .public_table .exception-list .currnt_week').show();
	 	$('#next_plan .public_table .exception-list .next_week').show();
	 	$('.loading').hide()
	})
	 
})
