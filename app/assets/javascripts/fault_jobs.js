$(function(){
	
	//  派遣
//	$.getJSON('/search_unit_manys').done(function(data){
//		var unitTemp = template('sendPers',data);
//		$(' #send_per').html(unitTemp);
//	})
	var id;
$('body').delegate('.paiq','click',function(){
	
	 id=$(this).parents('tr').find('.id').text();
	 $.ajax({
	 	type:"get",
	 	url:'/fault_jobs/'+id+'.json',
	 	success:function(data){
	 		var regionId=data.s_region_code_info_id;
	 		$.ajax({
	 			type:"get",
	 			url:"/search_unit_manys.json?s_region_code_info_id="+regionId,
	 			success:function(data){
	 				var unitTemp = template('sendPers',data);
					$(' #send_per').html(unitTemp);
	 			}
	 		});
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
$('body').delegate('#paiSave','click',function(){
	$('.loading').show()
	var unit_id=$('#myModal #send_per .unit_user input[name="pers"]:checked').parents('.unit_user').siblings('.unit_name').find('.unit_id').text();
	var handle_man_id=$('#myModal #send_per .unit_user input[name="pers"]:checked').val();
	var handle_man=$('#myModal #send_per .unit_user input[name="pers"]:checked').next('span').text();
	$.ajax({
	 	type:"PUT",
	 	url:'/search_unit_manys/'+id+'.json',
	 	data:{
			unit_id:unit_id,
			handle_man_id:handle_man_id,
			handle_man:handle_man
		},
	 	success:function(data){
	 		page_state = "change";
	 		originData();
	 		$('.loading').hide()
	 		$('#myModal').modal('hide');
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
	//更换处理人
$('body').delegate('.workPerson','click',function(){
	 id=$(this).parents('tr').find('.id').text();
	 $.ajax({
	 	type:"get",
	 	url:'/fault_jobs/'+id+'.json',
	 	success:function(data){
	 		var regionId=data.s_region_code_info_id;
	 		$.ajax({
	 			type:"get",
	 			url:"/search_unit_pers.json?s_region_code_info_id="+regionId,
	 			success:function(data){
	 				var unitTemp = template('sendPers',data);
					$(' #send_per').html(unitTemp);
	 			}
	 		});
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
$('body').delegate('#ghSave','click',function(){
	$('.loading').show()
	var unit_id=$('#ghModal #send_per .unit_user input[name="pers"]:checked').parents('.unit_user').siblings('.unit_name').find('.unit_id').text();
	var handle_man_id=$('#ghModal #send_per .unit_user input[name="pers"]:checked').val();
	var handle_man=$('#ghModal #send_per .unit_user input[name="pers"]:checked').next('span').text();
	$.ajax({
	 	type:"PUT",
	 	url:'/search_unit_pers/'+id+'.json',
	 	data:{
			unit_id:unit_id,
			handle_man_id:handle_man_id,
			handle_man:handle_man
		},
	 	success:function(data){
	 		page_state = "change";
	 		originData();
	 		$('.loading').hide()
	 		$('#ghModal').modal('hide');
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
	//新增
	$('#fault_jobs #add').click(function(){
		
		$('#fault_add').show();
		$('#fault_jobs').hide();
		$.ajax({
		type:"get",
		url:"/all_stations_area.json",
		success:function(data){
			var stationTemp = template('Addstations',data);
		$('#sq_radioStation .station_contents').html(stationTemp);
			$('body').delegate('#device_name','change',function(){
				var item=$('#device_name option:selected').val();
				if(item==""){
					$('#fault_phenomenon').html('');
				}
				else{
					$.ajax({
						type:'get',
						url:"/s_item_job.json?item="+item,
						success:function(data){
							var dataHtml = template('faults',data)
							$('#fault_phenomenon').html(dataHtml)
			
							$("#otherFault").click(function () {
			                    otherFault();
			                });
			                $("input[name='FaultContent']").click(function () {
			                    if ($(this).prop('checked')) {
			                        $("#otherFault").prop("checked", false);
			                        $("#FaultContent").hide().val("");
			                    }
			                });
						},
						error:function(err){
							console.log(err)
						}
					})	
				}
				
			})
		},
		error:function(err){
			console.log(err)
		}
	});
	
	})
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	$.ajax({
		type:"get",
		url:"/all_stations_area.json",
		success:function(data){
			var stationTemp = template('Addstations',data);
		$('#sq_radioStation .station_contents').html(stationTemp);
	
		},
		error:function(err){
			console.log(err)
		}
	});
	
 	function otherFault() {
        //$(".ch-fault").toggle();
        $("#FaultContent").toggle();
        if ($("#otherFault").prop('checked')) {
            $("input[name='FaultContent']").prop("checked", false);
        }
    }
 	
 	//查询
 	var station_id;
 	var created_time;
	var end_time;
	var job_no;
	var author;
	var handle_man;
	var create_type;
	var job_status;
	   function public_search(){
	   	
	   	$('#fault_jobs .public_search .startTime1').val($('.created_time').val());
		$('#fault_jobs .public_search .endTime1').val($('.end_datetime').val());
		$('#fault_jobs .public_search .searchNo').val($('.public_search .job_no').val());
        $('#fault_jobs .public_search .searchAuthor').val($('.public_search .author').val());
        $('#fault_jobs .public_search .searchHan').val($('.public_search .search_handle').val());
        var searchCrea=$('.public_search .create_type option:selected').val();
		$('#fault_jobs .public_search .searchCrea').val(searchCrea);
		var searchStatus=$('.public_search .job_status option:selected').val();
		$('#fault_jobs .public_search .searchStatus').val(searchStatus)
		var searchFault=$('.public_search .fault_type option:selected').val();
		$('#fault_jobs .public_search .searchFault').val(searchFault)
		
		created_time = $('#fault_jobs .public_search .startTime1').val();
    	end_time = $('#fault_jobs .public_search .endTime1').val();
		job_no=$('#fault_jobs .public_search .searchNo').val();
	   	author=$('#fault_jobs .public_search .searchAuthor').val();
	    handle_man=$('#fault_jobs .public_search .searchHan').val();
	    console.log(handle_man)
	    create_type=$('#fault_jobs .public_search .searchCrea').val()
	    job_status=$('#fault_jobs .public_search .searchStatus').val();
	    fault_type=$('#fault_jobs .public_search .searchFault').val();	
			if(created_time && end_time){
				created_time=created_time;
				end_time=end_time;
			}else if(created_time){
				end_time = created_time
			}else if(end_time){
				created_time = end_time
			}else{
				created_time = ''
		        end_time = ''
			}
			$('#fault_jobs .public_search .searchSta').val($('.control_stationIDs').val())
			if($('#fault_jobs .public_search .searchSta').val()){
	             station_id = $('#fault_jobs .public_search .searchSta').val().split(',')
	        }else{
	        	 station_id = ''
	        }
		
		}
	   $('.public_search .search').click(function(){
	   	
		 
		 public_search()
//	   	var create_type=$('.public_search .create_type').val();
		$.ajax({
			type:"get",
			url:"/fault_jobs.json",
			data:{
				created_time:created_time,
				end_time:end_time,
				d_station_id:station_id,
				job_no:job_no,
				author:author,
				handle_man:handle_man,
				job_status:job_status,
				create_type:create_type
				
			},
			success:function(data){
				
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
		   		$('.page_total small').text(data.list_size);
		   		jobstatus();
		   		var roles=data.role_name;
			        if(roles=="运维成员"){
			        	$('#fault_jobs #add').hide();
						$('#fault_jobs table .fault_audit').hide();
						$('#fault_jobs .workPerson').hide()
						$('#fault_jobs .paiq').hide()
						
			        }
			        if(roles=="运维复核人员"){
			        	$('#fault_jobs .fault_writing').hide();
			        }
			         var handleMan=data.login_name;
			        $('#fault_jobs .public_table .exception-list .handleMan').each(function(i){
			        	var phandle=$(this).text();
			        	if(phandle==handleMan){
		//	        		$(this).siblings().find('.d_taskEdit').show()
		
			        	}else{
			        		$(this).siblings().find('.fault_writing').hide()
		//	        		$(this).siblings().find('.workPerson').hide()
		//		        	$(this).siblings().find('.paiq').hide()
			        	}
			        })
			},
			error:function(err){
				console.log(err)
			}
		});
	   })
 	//添加提交
 			$('body').delegate('#fault_add #addSave','click',function(){
 				var fault_type=$('#add_form #fault_type').val();
 				var urgent_level=$('#add_form #urgent_level input:checked').val();
 				var station_name=$('#add_form #addControl_stations').val();
 				var d_station_id=$('#add_form #addControl_stationIDs').val();
 				var device_name=$('#add_form #device_name option:selected').val();
 				
 				var other_fault=$('#add_form #FaultContent').val();
 				var title=$('#add_form #title').val();
 				var content=$('#add_form #content').val();
 				//下发类型
 				var checkSend=[];
 				var checkSends=$('#add_form #send_type input[type="checkbox"]');
 				for (var i=0;i<checkSends.length;i++){
 					if(checkSends[i].checked){
 						checkSend.push($(checkSends[i]).val());
 					}
 				}
 				$('#add_form #send_type_val').val(checkSend)
 				var send_type=$('#add_form #send_type_val').val();
 				
 				var phm=[];
 				var phms=$('#add_form #fault_phenomenon label input[type="checkbox"]');
 				for (var s=0;s<phms.length;s++){
 					if(phms[s].checked){
 						phm.push($(phms[s]).val());
 					}
 				}
 				$('#add_form #fault_val').val(phm)
 				var fault_phenomenon=$('#add_form #fault_val').val();
 				$.ajax({
 					type:"post",
 					url:"/fault_jobs.json",
 					async:true,
 					data:{
 						d_fault_job:{
 							fault_type:fault_type,
 							urgent_level:urgent_level,
 							send_type:send_type,
 							station_name:station_name,
 							d_station_id:d_station_id,
 							device_name:device_name,
 							fault_phenomenon:fault_phenomenon,
 							other_fault:other_fault,
 							title:title,
 							content:content
 						}
 					},
 					success:function(data){
 						page_state = "change";
						alert("提交成功");
						originData();
						$('#fault_writings').hide()
						$('#fault_audit').hide();
						$('#fault_looks').hide();
			 			$('#fault_add').hide();
			 			$('#fault_jobs').show();
//						$('#add_form .form-group input[type="text"]').val("");
//						$('#add_form .form-group textarea').val("");
//						document.getElementById("device_name")[0].selected=true;
						$('#fault_phenomenon').html('');
						document.getElementById("add_form").reset()
 					},
 					error:function(err){
 						
 					}
 				});
 			})
 			
 		$('body').delegate('#fault_add #btnBack','click',function(){
 			$('#fault_add').hide();
			$('#fault_jobs').show();
 		})
		 //详情
		  var DeviceTypeName;
		  var status;
 		$('body').delegate('#fault_jobs .public_table .fault_look','click',function(){
 			$('#fault_jobs').hide();
 			id=$(this).parents('tr').find('.id').text();
 			$.ajax({
				type:'get',
				url:'/fault_jobs/'+id + '.json',
				success:function(data){
					
					console.log(data)
					var dataObj = {
						editData:data
					}
					
					var originData = template('faultLook',dataObj)
					$('#fault_looks').html(originData)
					$('#fault_looks').show();
					status=data.job_status;
					var remote=data.handle.handle_remote;
					var types=data.handle.handle_type;
					var chekAudit=data.audit_if;
					var workFlag=data.work_flag;
					var maintName=data.maint_project.maint_name;
					var calibs_if=data.equip_calib.calibs_if;
					var libg=data.flow;
					DeviceTypeName=data.equilp.equip_type;
					
	 				for(var i = 0; i<libg.length; i++){
	 					var industry = libg[i];
	 					$("#fault_looks .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
	 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
	 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
					 }
				$('#fault_looks .dTaskTop li').each(function(i){
   					
   					var libg=$(this).find('.lbg-txt').text();
   					
   					if(status==libg){
   						
   						var bgs=$(this).index()
   						
   						var lis = $('#fault_looks .dTaskTop li');
   						for(var j=0;j<=bgs;j++){
							lis[j].className="on";
   						}
   					}
   				})
				$('#fault_looks .dTaskTop li').eq(0).addClass("on")
					$('#fault_looks #repairContents input[type="radio"]').each(function(i){
						var names=$(this).val();
						if(maintName==names){
							$(this).prop('checked',true)	
	    					return false;
						}
					})
					$('#fault_looks #ApproveProcessType input[type="radio"]').each(function(i){
						var flag=$(this).val();
						if(workFlag==flag){
							$(this).prop('checked',true)	
	    					return false;
						}
					})
					$('#fault_looks #calibration .ProcessStatus input[type="radio"]').each(function(i){
						var calibs=$(this).val();
						if(calibs_if==calibs){
							$(this).prop('checked',true)	
	    					return false;
						}
					})
					if(calibs_if=="N"){
						$('#fault_looks #calibration #calib_des').show();
					}
					if(status=="工单待分配"){
						$('#fault_looks #guzhang').hide();
						$('#fault_looks #audit').hide();
					}
					if(status=="工单待审核"){
					$('#fault_looks #audit').hide();	
					}else if(status=="设备待维修"){
						$('#fault_looks #repair').hide();
						$('#fault_looks #audit').hide();
						$('#fault_looks #calibration').hide()
					}else if(status=="工单待处理"){
						$('#fault_looks #audit').hide();
						$('#fault_looks #guzhang').hide()
					}
					if(status=='设备待校准'){
						$('#fault_looks #audit').hide();
						$('#fault_looks #calibration').hide()
					}
					
					if(remote=="Y"){
						$('#fault_looks .IsScene #IsSceneY').prop('checked',true)
						$('#fault_looks #DeviceInfo').hide();
					}else{
						$('#fault_looks .IsScene #IsSceneN').prop('checked',true)
					}
					if(chekAudit=="Y"){
						$('#fault_looks #audit_des').hide();
						$('#fault_looks .checkbox-radio #Approve_Y').prop('checked',true);
					}else{
						$('#fault_looks #audit_des').show();
						$('#fault_looks #ApproveProcessType').hide();
						$('#fault_looks .checkbox-radio #Approve_N').prop('checked',true);
					}
					var lis = $('#fault_looks .dTaskTop li');
					if(types=="现场维修"){
						$('#fault_looks #DeviceInfo #ProcessX').prop('checked',true)
						$('#fault_looks #repair').hide()
						lis[2].remove()
						lis[3].remove()
						$("#fault_looks .dTaskTop li").css("width","20%");
					}else if(types=="更换备机"){
						$('#fault_looks #DeviceInfo_Code').show();
						$('#fault_looks #DeviceInfo #ProcessG').prop('checked',true)
						$("#fault_looks #ApproveProcessType").show();
						$('#fault_looks #repair').show()
						$('#fault_looks #calibration').show();
						$('#fault_looks #FaultContent').show();
					}else{
						$('#fault_looks #DeviceInfo #ProcessQ').prop('checked',true)
						$('#fault_looks #repair').hide()
						lis[2].remove()
						lis[3].remove()
						$("#fault_looks .dTaskTop li").css("width","20%");
					}
					if(types=="更换备机" && chekAudit=="N"){
						$('#equip_looks #ApproveProcessType').hide();
					}
					if(status=="设备待维修" && types=="更换备机"){
						$('#fault_looks #repair').hide();
						$('#fault_looks #calibration').hide()
					}
					if(status=="设备待校准" && types=="更换备机"){
						
						$('#fault_looks #calibration').hide()
					}
//					$('#fault_looks img').zoomify();
					var imgs = document.getElementsByTagName("img");
					imgs[0].focus();
					for(var i = 0;i<imgs.length;i++){
						imgs[i].onclick = expandPhoto;
						imgs[i].onkeydown = expandPhoto;
					}

					typeNames(DeviceTypeName)
				},
				error:function(err){
					console.log(err)
				}
			})
 		})
// 		放大图片
function expandPhoto(){
	var overlay = document.createElement("div");
	overlay.setAttribute("id","overlay");
	overlay.setAttribute("class","overlay");
	document.body.appendChild(overlay);

	var img = document.createElement("img");
	img.setAttribute("class","overlayimg");
	img.src = this.getAttribute("src");
	document.getElementById("overlay").appendChild(img);

	img.onclick = restore;
}
function restore(){
	document.body.removeChild(document.getElementById("overlay"));
	document.body.removeChild(document.getElementById("img"));
}
 		$('body').delegate('#fault_jobs_look #look_back','click',function(){
 			$('#fault_looks').hide();
 			$('#fault_add').hide();
 			$('#fault_jobs').show();
 		})
 		$('body').delegate('#audit_N','click',function(){
			$('#fault_audit #audit_text').show();
			$('#fault_audit #ApproveProcessType').hide();
		})
		
 		//审核
   		var clickId
   		var types
	    $('body').delegate('.fault_audit','click',function(){
    		checkid=$(this).parents('tr').find('.id').text();
    		$.ajax({
    			type:"get",
    			url:'/fault_jobs/'+checkid + '.json',
    			success:function(data){
    				var dataObj = {
						editData:data
					}
					var originData = template('faultAudit',dataObj)
					$('#fault_audit').html(originData)
					$('#fault_audit').show();
					
					$('#fault_looks').hide();
		 			$('#fault_add').hide();
		 			$('#fault_jobs').hide();
		 			var remote=data.handle_remote;
					types=data.handle.handle_type;
					status=data.job_status;
					var maintName=data.maint_project.maint_name;
					var calibs_if=data.equip_calib.calibs_if;
					DeviceTypeName=data.equilp.equip_type;
					if(status=="设备待维修"){
						$('#fault_audit #repair').hide();
						$('#fault_audit #audit').hide();
					}else if(status=="工单待审核"){
						$('#fault_audit #audit').hide();
					}
					if(remote=="Y"){
						$('#fault_audit .IsScene #IsSceneY').prop('checked',true)
						$('#fault_audit #DeviceInfo').hide();
					}else{
						$('#fault_audit .IsScene #IsSceneN').prop('checked',true)
					}
					$('#fault_audit #repairContents input[type="radio"]').each(function(i){
						var names=$(this).val();
						if(maintName==names){
							$(this).prop('checked',true)	
	    					return false;
						}
					})
					$('#fault_audit #calibration .ProcessStatus input[type="radio"]').each(function(i){
						var calibs=$(this).val();
						if(calibs_if==calibs){
							$(this).prop('checked',true)	
	    					return false;
						}
					})
					if(types=="现场维修"){
						$('#fault_audit #DeviceInfo #ProcessX').prop('checked',true)
						$('#fault_audit #ApproveProcessType').hide();
						$('#fault_audit #repair').hide()
					}else if(types=="更换备机"){
						$('#fault_audit #DeviceInfo_Code').show();
						$('#fault_audit #DeviceInfo #ProcessG').prop('checked',true)
						$('#fault_audit #ApproveProcessType').show();
						$('#fault_audit #calibration').show();
						$('#fault_audit #FaultContent').show();
						
					}else{
						$('#fault_audit #DeviceInfo #ProcessQ').prop('checked',true)
						$('#fault_audit #ApproveProcessType').hide();
						$('#fault_audit #repair').hide()
					}
					$('body').delegate('#audit_Y','click',function(){
						$('#fault_audit #audit_text').hide();
						if(types=="更换备机"){
							$('#fault_audit #ApproveProcessType').show();
						}else{
							$('#fault_audit #ApproveProcessType').hide();	
						}
						
					})
					
					var imgs = document.getElementsByTagName("img");
					imgs[0].focus();
					for(var i = 0;i<imgs.length;i++){
						imgs[i].onclick = expandPhoto;
						imgs[i].onkeydown = expandPhoto;
					}
					typeNames(DeviceTypeName)
    				
    			},
    			error:function(err){
    				console.log(err)
    			}
    		});
    	})
 		
 		$('body').delegate('#fault_audit #auditSave','click',function(){
 			var audit_if=$('#fault_audit #audit_adopt .checkbox-radio input[name="Approve"]:checked').val();
			var audit_des=$('#fault_audit #audit_not').val();
			var work_flag
			if($('#fault_audit #ApproveProcessType input[name="ApproveProcessType"]:checked').val()){
				work_flag = $('#fault_audit #ApproveProcessType input[name="ApproveProcessType"]:checked').val()
			}else{
				work_flag='';
			}
			if(types=="更换备机" && work_flag==''){
				alert("请选择处理方式")
			}else{
				$.ajax({
					type:"post",
					url:'/fault_jobs/'+checkid + '/audit.json',
					data:{
						
						audit_if:audit_if,
						audit_des:audit_des,
						work_flag:work_flag
						
					},
					success:function(data){
						page_state = "change";
						alert("提交成功");
						originData();
						$('#fault_jobs').show()
						$('#fault_audit').hide()
						$('#fault_looks').hide()
					},
					error:function(err){
						console.log(err)
					}
				});
			}
			
 		})
 		$('body').delegate('#fault_audit .saveback','click',function(){
	        $('#fault_jobs').show()
	    	$('#fault_audit').hide()
	    	$('#fault_looks').hide()
		})
		 //填写
		
 		 $('body').delegate('.fault_writing','click',function(){
 		 	 checkid=$(this).parents('tr').find('.id').text();
 		 	 $.ajax({
    			type:"get",
    			url:'/fault_jobs/'+checkid + '/edit.json',
    			success:function(data){
    				var dataObj = {
						editData:data
					}
					var originData = template('faultWriting',dataObj)
					$('#fault_writings').html(originData)
					DeviceTypeName=data.equilp.equip_type;
					status=data.faultJob.job_status
					$('#fault_writings').show()
					$('#fault_audit').hide();
					$('#fault_looks').hide();
		 			$('#fault_add').hide();
		 			$('#fault_jobs').hide();
    				typeNames(DeviceTypeName)
    			},
    			error:function(err){
    				console.log(err)
    			}
    		});
		  })
		 function typeNames(DeviceTypeName){
			if(DeviceTypeName=="o3"){
						$('#FaultContent .typeName span').html("臭氧（O3）分析仪运行状况检查记录表")
					}else if(DeviceTypeName=="pm10"){
						$('#FaultContent .typeName span').html("颗粒物PM10自动监测分析仪运行状况检查记录")
					}else if(DeviceTypeName=="pm2.5"){
						$('#FaultContent .typeName span').html("颗粒物PM2.5自动监测分析仪运行状况检查记录")
					}else if(DeviceTypeName=="so2"){
						$('#FaultContent .typeName span').html("二氧化硫（SO2）分析仪运行状况检查记录表")
					}else if(DeviceTypeName=="no2"){
						$('#FaultContent .typeName span').html("氮氧化物（NOX）分析仪运行状况检查记录表")
					}else if(DeviceTypeName=="co"){
						$('#FaultContent .typeName span').html("一氧化碳（CO）分析仪运行状况检查记录表")
					}
		 }



 		 //编辑提交
 		  $('body').delegate('#fault_writings #editSave','click',function(){
 		  	//远程处理
 		  	var handle_remote=$('#fault_writings .IsScene input[type="radio"]:checked').val();
 		  	//处理说明
 		  	var handle_des=$('#fault_writings #H_Desc').val();
 		  	var handle_type=$('#fault_writings #DeviceInfo input[type="radio"]:checked').siblings('span').text()
 		  	var equilp_id=$('#fault_writings #DeviceCode option:selected').val();
   		  	$.ajax({
   		  		type:"put",
   		  		url:'/fault_jobs/'+checkid + '.json',
   		  		data:{
   		  			d_fault_handle:{
	   		  			handle_remote:handle_remote,
	   		  			handle_des:handle_des,
	   		  			handle_type:handle_type,
	   		  			equilp_id:equilp_id
   		  			}
   		  			
   		  		},
   		  		success:function(data){
   		  			page_state = "change";
					alert("提交成功");
					originData();
					$('#fault_writings').hide()
					$('#fault_audit').hide();
					$('#fault_looks').hide();
		 			$('#fault_add').hide();
		 			$('#fault_jobs').show();
   		  		},
   		  		error:function(err){
   		  			console.log(err)
   		  		}
   		  	});
		   })
		  
	$('body').delegate('#fault_writings #FaultContent .btn-checkitem','click',function(){
		
		typeName=$(this).children('span').html()
		
		console.log(typeName)
		var Data_DZM = {
				id:checkid,
				name:typeName
                }
		Replacement(Data_DZM)
		$('#fault_writings .edit_jobs').hide();
		 $('#fault_writings #typeListBox').show();
	})
	$('body').delegate('#fault_looks .btn-checkitem','click',function(){
		$('#fault_looks #fault_jobs_look').hide();
		typeName=$(this).children('span').html()
		
		console.log(typeName)
		var Data_DZM = {
				id:id,
				name:typeName
                }
		Replacement(Data_DZM)
		 $('#fault_looks #typeListBox').show();
	})
	$('body').delegate('#fault_audit .btn-checkitem','click',function(){
		$('#fault_audit .edit_jobs').hide();
		typeName=$(this).children('span').html()
		
		console.log(typeName)
		var Data_DZM = {
				id:checkid,
				name:typeName
                }
		Replacement(Data_DZM)
		 $('#fault_audit #typeListBox').show();
	})
   function Replacement(Data_DZM) {
	  var url='';
		if(Data_DZM.name=="备机更换记录表"){
			url = "/get_form_module/replacement_recorder";
		}else if(Data_DZM.name=="空气自动监测仪器设备检修记录表"){
			url = "/get_form_module/atmosphere_automatic_monitoring_instrument";
		}else if(Data_DZM.name =="颗粒物PM10自动监测分析仪运行状况检查记录" || Data_DZM.name =="颗粒物PM2.5自动监测分析仪运行状况检查记录"){
				url = "/get_form_module/pm_check_week";
		}else if(Data_DZM.name =="臭氧（O3）分析仪运行状况检查记录表"){//
			url = "/get_form_module/analyser_status_week";
		}else if(Data_DZM.name =="氮氧化物（NOX）分析仪运行状况检查记录表"){//
			url = "/get_form_module/nox_analyser_status_week";
		}else if(Data_DZM.name =="二氧化硫（SO2）分析仪运行状况检查记录表"){//（每周）    
			url = "/get_form_module/so2_analyser_status_week";
		}else if(Data_DZM.name =="一氧化碳（CO）分析仪运行状况检查记录表"){//
			url = "/get_form_module/co_analyser_status_week";
		}
		$.ajax({
                    type:"get",
					url:url,
                    data:Data_DZM,
                    async:true,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
					  
					   $('#fault_writings #typeListBox').html(data)
					   $('#fault_looks #typeListBox').html(data);
					   $('#fault_audit #typeListBox').html(data);
					   $('#typeListBox').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#fault_looks #typeListBox table input').attr("disabled",true)
						$('#fault_looks #typeListBox table textarea').attr("disabled",true)
						$('#fault_audit #typeListBox table input').attr("disabled",true)
						$('#fault_audit #typeListBox table textarea').attr("disabled",true)
						$('#fault_audit #typeListBox #Save').hide()
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
							todayButton:true,
							step:5
						});
						if(status=="工单待审核" || status=="工单已审核" || status=="工单待复核"){
							
							$('#typeListBox #supplyLook').show();
							$('#typeListBox #supplyEdit').hide();
						}else{
						
							$('#typeListBox #supplyEdit').show();
							$('#typeListBox #supplyLook').hide();
							
						}
						$('#fault_looks #typeListBox #supplyLook').show()
						$('#fault_looks #typeListBox #supplyEdit').hide()
						$('#fault_audit #typeListBox #supplyLook').show()
						$('#fault_audit #typeListBox #supplyEdit').hide()
                       
                    },
                    error:function(err){
                        console.log(err);
                        $('.loading').hide();
                    }
                });
    }

	$('body').delegate('#fault_writings #typeListBox #Back','click',function(){
		$('#fault_writings .edit_jobs').show();
		$('#fault_writings #typeListBox').hide();
	})
	$('body').delegate('#fault_looks #typeListBox #Back','click',function(){
		$('#fault_looks #fault_jobs_look').show();
		$('#fault_looks #typeListBox').hide();
	})
	$('body').delegate('#fault_audit #typeListBox #Back','click',function(){
		$('#fault_audit .edit_jobs').show();
		$('#fault_audit #typeListBox').hide();
	})

	$('body').delegate('#typeListBox #Save','click',function(){
		var datas=[]
		var objname={}
		var tasks=[]
		var f_form_module_id=$('#typeListBox .modalID').html()
		
		if(f_form_module_id=="37"){
			$('#typeListBox table .list_name').each(function(){
				var listName=$(this).html();
				var listValue=$(this).parent('td').next().children().val();
				var list1={
					"check_name":listName,
					"check_value":listValue
				}
				datas.push(list1)
				
			})
			console.log(datas)
			$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
		}else if(f_form_module_id=="4" || f_form_module_id=="5"){
		$('#typeListBox table .td-text-left').each(function(){
			var check_name=$(this).children('p').html();
			var check_value=$(this).next().children().val();
			var instrument1={
				"check_name":check_name,
				"check_value":check_value
			}
			datas.push(instrument1)
		})
		$('#typeListBox table .td-text-left2').each(function(){
			var check_name=$(this).children('p').html();
			var check_value=$(this).siblings().children('.check_value').val();
			var if_normal=$(this).siblings().children('.if_normal').val();
			var abnormal_handle=$(this).siblings().children('.abnormal_handle').val();
			var instrument2={
				"check_name":check_name+"_01",
				"check_value":check_value
			}
			datas.push(instrument2)
			var instrument3={
				"check_name":check_name+"_02",
				"check_value":if_normal
			}
			datas.push(instrument3)
			var instrument4={
				"check_name":check_name+"_03",
				"check_value":abnormal_handle
			}
			datas.push(instrument4)
		})
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="7" || f_form_module_id=="11" || f_form_module_id=="12" || f_form_module_id=="13"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue=$(this).parent('td').next().children().val();
			var list1={
				"check_name":listName,
				"check_value":listValue
			}
			datas.push(list1)
		
		})
		$('#typeListBox table .list_name2').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.start_time').val();
			var listValue2=$(this).parent('td').siblings().children('.end_time').val();
			var listValue3=$(this).parent('td').siblings().children('.ppm').val();
			var listValue4=$(this).parent('td').siblings().children('.calibrated_value').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			var list4={
				"check_name":listName+"_04",
				"check_value":listValue4
			}
			datas.push(list4)
		
		})
		$('#typeListBox table .list_name3').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.check_value1').val();
			var listValue2=$(this).parent('td').siblings().children('.check_value2').val();
			var listValue3=$(this).parent('td').siblings().children('.check_value3').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			
		})
		$('#typeListBox table .list_name4').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.check_value1').val();
			var listValue2=$(this).parent('td').siblings().children('.check_value2').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			
		})
		console.log(datas)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}
		objname={datas,f_form_module_id:f_form_module_id,task}
		Submit(objname)
	})
	function Submit(objname){
		$.ajax({
			type:"put",
			url:'/get_form_module/'+checkid,
			data:objname,
			success:function(data){
				$('#fault_writings .edit_jobs').show();
				$('#fault_writings #typeListBox').hide();
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
	}

 		 $('body').delegate('#fault_writings #editBack','click',function(){
 		 	$('#fault_writings').hide()
 		 	$('#fault_audit').hide();
			$('#fault_looks').hide();
 			$('#fault_add').hide();
 			$('#fault_jobs').show();
 		 })
 		 $('body').delegate('#IsSceneN','click',function(){
 		 	$('#FaultProcess #DeviceInfo').show()
 		 })
 		 $('body').delegate('#IsSceneY','click',function(){
 		 	$('#FaultProcess #DeviceInfo').hide()
 		 })
 		 $('body').delegate('#ProcessG','click',function(){
 		 	$('#FaultProcess #DeviceInfo_Code').show()
 		 	$('#FaultProcess #FaultContent').show()
 		 })
 		 $('body').delegate('#ProcessX','click',function(){
 		 	$('#FaultProcess #DeviceInfo_Code').hide()
 		 	$('#FaultProcess #FaultContent').hide()
 		 })
 		 $('body').delegate('#ProcessQ','click',function(){
 		 	$('#FaultProcess #DeviceInfo_Code').hide()
 		 	$('#FaultProcess #FaultContent').hide()
 		 })
 		 
 		 
 		 
 		 //转发
 	$("body").delegate("#fault_jobs table tbody tr td .forward","click",function() {
		$('.forward_prop').hide();
		$(this).parent().children('div').show();	
	})
	$('body').delegate('#fault_jobs table tbody tr td .forward_prop .btn-sure','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
 		 		type:"get",
 		 		url:"/fault_jobs/"+id+"/forword.json",
 		 		success:function(data){
 		 			page_state = "change";
					alert("提交成功");
					originData();
					$('.forward_prop').hide();
 		 		},
 		 		error:function(err){
 		 			console.log(err)
 		 		}
 		 	});
	})
	$('body').delegate('#fault_jobs table tbody tr td .forward_prop .btn-cancel','click',function(){
		$('.forward_prop').hide();
	})


	//校准处理
$('body').delegate('#fault_looks #calibration .calibration ','click',function(){
		
		$('#fault_looks #fault_jobs_look').hide();
		var url='';
	if(DeviceTypeName=="o3"){
			url = "/get_form_module/o3_monitoring_instrument_calibration";
			typeNames1="臭氧监测仪器校准数据报告"
		}else if(DeviceTypeName=="pm10"){
			url = "/get_form_module/pm10_check_calibration_record";
			typeNames1="PM10检查校准记录表"
		}else if(DeviceTypeName=="pm2.5"){
			url = "/get_form_module/pm25_check_calibration_record";
			typeNames1="PM2.5检查校准记录表"
		}else if(DeviceTypeName=="so2"){
			url = "/get_form_module/so2_monitoring_instrument_calibration";
			typeNames1="二氧化硫监测仪器校准数据报告"
		}else if(DeviceTypeName=="no2"){
			url = "/get_form_module/nox_monitoring_instrument_calibration";
			typeNames1="氮氧化物监测仪器校准数据报告"
		}else if(DeviceTypeName=="co"){
			url = "/get_form_module/co_monitoring_instrument_calibration";
			typeNames1="一氧化碳监测仪器校准数据报告"
		}
		console.log(typeNames1)
		var Data_DZM = {
				id:id,
				name:typeNames1
                }
		cailbTrea(Data_DZM,url)
		$('#fault_looks #typeListBox').show();
		 
	})
$('body').delegate('#fault_audit #calibration .calibration ','click',function(){
		
		$('#fault_audit .edit_jobs').hide();
		var url='';
	if(DeviceTypeName=="o3"){
			url = "/get_form_module/o3_monitoring_instrument_calibration";
			typeNames="臭氧监测仪器校准数据报告"
		}else if(DeviceTypeName=="pm10"){
			url = "/get_form_module/pm10_check_calibration_record";
			typeNames="PM10检查校准记录表"
		}else if(DeviceTypeName=="pm2.5"){
			url = "/get_form_module/pm25_check_calibration_record";
			typeNames="PM2.5检查校准记录表"
		}else if(DeviceTypeName=="so2"){
			url = "/get_form_module/so2_monitoring_instrument_calibration";
			typeNames="二氧化硫监测仪器校准数据报告"
		}else if(DeviceTypeName=="no2"){
			url = "/get_form_module/nox_monitoring_instrument_calibration";
			typeNames="氮氧化物监测仪器校准数据报告"
		}else if(DeviceTypeName=="co"){
			url = "/get_form_module/co_monitoring_instrument_calibration";
			typeNames="一氧化碳监测仪器校准数据报告"
		}
		console.log(typeNames)
		var Data_DZM = {
				id:checkid,
				name:typeNames
                }
		cailbTrea(Data_DZM,url)
		$('#fault_audit #typeListBox').show();
		 
	})
function cailbTrea(Data_DZM,url){
	
	$.ajax({
			type:"get",
			url:url,
			data:Data_DZM,
			async:true,
			beforeSend:function(){
				$('.loading').show();
			},
			success:function(data){
				$('#fault_looks #typeListBox').html(data)
				$('#fault_audit #typeListBox').html(data)
				$('#typeListBox').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
				$('#fault_audit #typeListBox input,#fault_audit #typeListBoxtextarea').attr('disabled',true);
				$('#fault_looks #typeListBox input,#fault_looks #typeListBoxtextarea').attr('disabled',true);
				$('.dateTimepicker').datetimepicker({
					format:"Y-m-d",
					todayButton:true,
					timepicker:false
				});
				$('.dateTimepickerH').datetimepicker({
					format:"Y-m-d H:i",
					todayButton:true,
					step:5
				});
				
			},
			error:function(err){
				console.log(err);
				$('.loading').hide();
			}
		});
}
	    
	//	分页+初始数据
	function originData(param){
//		public_search();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('faultTables',data_hash);
			$('table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var station_ids = $('#fault_jobs .public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
			$.ajax({
				url: '/fault_jobs.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
				created_time:$('#fault_jobs .public_search .startTime1').val(),
				end_time:$('#fault_jobs .public_search .endTime1').val(),
				d_station_id:station_ids,
				job_no:$('#fault_jobs .public_search .searchNo').val(),
				author:$('#fault_jobs .public_search .searchAuthor').val(),
				handle_man:$('#fault_jobs .public_search .searchHan').val(),
				job_status:$('#fault_jobs .public_search .searchStatus').val(),
				create_type:$('#fault_jobs .public_search .searchCrea').val()
				
			},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('faultTables',data);
					$('table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
					jobstatus();
					var roles=data.role_name;
			        if(roles=="运维成员"){
			        	$('#fault_jobs #add').hide();
						$('#fault_jobs table .fault_audit').hide();
						$('#fault_jobs .workPerson').hide()
						$('#fault_jobs .paiq').hide()
						
			        }
			        if(roles=="运维复核人员"){
			        	$('#fault_jobs .fault_writing').hide();
			        }
			        var handleMan=data.login_name;
			        $('#fault_jobs .public_table .exception-list .handleMan').each(function(i){
			        	var phandle=$(this).text();
			        	if(phandle==handleMan){
		//	        		$(this).siblings().find('.d_taskEdit').show()
		
			        	}else{
			        		$(this).siblings().find('.fault_writing').hide()
		//	        		$(this).siblings().find('.workPerson').hide()
		//		        	$(this).siblings().find('.paiq').hide()
			        	}
			        })
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/fault_jobs.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	    jobstatus();
	    var roles=data.role_name;
        if(roles=="运维成员"){
        	$('#fault_jobs #add').hide();
			$('#fault_jobs table .fault_audit').hide();
			$('#fault_jobs .workPerson').hide()
			$('#fault_jobs .paiq').hide()
			
        }
        if(roles=="运维复核人员"){
        	$('#fault_jobs .fault_writing').hide();
        }
        var handleMan=data.login_name;
        $('#fault_jobs .public_table .exception-list .handleMan').each(function(i){
        	var phandle=$(this).text();
        	if(phandle==handleMan){
//	        		$(this).siblings().find('.d_taskEdit').show()

        	}else{
        		$(this).siblings().find('.fault_writing').hide()
//	        		$(this).siblings().find('.workPerson').hide()
//		        	$(this).siblings().find('.paiq').hide()
        	}
        })
	})
	function jobstatus(){
		$('#fault_jobs table .job_status').each(function(){
	    	var s=$(this).text()
	    	if(s=="工单待审核"){
				$(this).siblings().find('.fault_writing').hide()
				$(this).siblings().find('.workPerson').hide();
			}else if(s=="工单已审核"){
				$(this).siblings().find('.forward').hide()
				$(this).siblings().find('.fault_writing').hide()
				$(this).siblings().find('.fault_audit').hide()
				$(this).siblings().find('.workPerson').hide()
			}else if(s=="工单待处理"){
				$(this).siblings().find('.fault_audit').hide()
				
			}else if(s=="设备待维修"){
				$(this).siblings().find('.fault_writing').hide()
				$(this).siblings().find('.fault_audit').hide()
			}
			if(s=="设备待校准"){
				$(this).siblings().find('.fault_writing').hide()
				$(this).siblings().find('.fault_audit').hide()
			}
			if(s=="工单待分配"){
				$(this).siblings().find('.forward').hide()
				$(this).siblings().find('.fault_writing').hide()
				$(this).siblings().find('.workPerson').hide();
				$(this).siblings().find('.fault_audit').hide();
				
			}
			if(s=="创建工单"){
				$(this).siblings().find('.fault_audit').hide();
			}
	    })
	}
//	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/fault_jobs.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })

})
var index=0
function previewImage2(file, supplyPrvid) { 
	index++


	/* file：file控件 
	* prvid: 图片预览容器 
	*/ 
	var tip = "Expect jpg or png or gif!"; // 设定提示信息 
	var filters = { 
	"jpeg" : "/9j/4", 
	"gif" : "R0lGOD", 
	"png" : "iVBORw" 
	} 
	var prvbox = document.getElementById(supplyPrvid); 

	prvbox.innerHTML = ""; 
	if (window.FileReader) { // html5方案 
		for (var i=0, f; f = file.files[i]; i++) { 
			var fr = new FileReader(); 
			fr.onload = function(e) { 
				var src = e.target.result; 
				if (!validateImg(src)) { 
					alert(tip) 
				} else { 
					showPrvImg(src); 
				} 
			} 
			fr.readAsDataURL(f); 
		} 
	} else { // 降级处理
	
		if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) { 
			alert(tip); 
		} else { 
			showPrvImg(file.value); 
		} 
	} 
	
		function validateImg(data) { 
			
			var pos = data.indexOf(",") + 1; 
			for (var e in filters) { 
				if (data.indexOf(filters[e]) === pos) { 
					return e; 
				} 
			} 
			return null; 
		} 
	
		function showPrvImg(src) { 
			
			var img = document.createElement("img"); 
			img.src = src; 
			prvbox.appendChild(img); 
		
		} 
}