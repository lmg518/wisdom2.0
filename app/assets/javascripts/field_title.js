$(function () {
	var objId = '';
    $('#myModal').on('hide.bs.modal', function () {
        $('#myModalLabel').text("");
        $('.modal-body').empty();
        $('.modal-footer').empty();
    })
    indexInit();
})
function indexInit() {
    $('.body-infos').empty();
    $.ajax({
        url: '/field_title/table_infos',
        type: 'get',
        success: function (data) {
            $('.body-infos').html(data)
        }
    });

}
function modal_show(obj_id) {
    $('#myModalLabel').text("我是标题" + obj_id);
    get_func_show(obj_id);
    $('#myModal').modal({
        keyboard: true
    });
}
function get_func_show(obj_id) {
    objId = obj_id
    $.ajax({
        url: '/field_title/' + obj_id + "/show",
        type: 'get',
        success: function (data) {
            $('.modal-body').html(data);
            var foot_str = ''
            foot_str += '<button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button> '
            foot_str += '<button type="button" class="btn btn-primary" onclick="form_action(2)"> 提交更改  </button> '
            $('.modal-footer').html(foot_str);
            /*是否有值*/
			if($("#myModal .title_if_value").html() == "Y"){
				$("#fieldRadio1").prop("checked","checked");
				$("#fieldRadio2").removeAttr("checked");
			}
			if($("#myModal .title_if_value").html() == "N"){
				$("#fieldRadio2").prop("checked","checked");
				$("#fieldRadio1").removeAttr("checked");
			}
        }
    });
}

function create_func() {
    $('#myModal').modal({
        keyboard: true
    });
    $('#myModalLabel').text("添加模板字段");
    $.ajax({
        url: '/field_title/new',
        type: 'get',
        success: function (data) {
            $('.modal-body').html(data);
            var foot_str = ''
            foot_str += '<button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button> '
            foot_str += '<button type="button" class="btn btn-primary" onclick="form_action(1)"> 提交更改  </button> '
            $('.modal-footer').html(foot_str);

        }
    });
}
function form_action(obj) {
    var url = '';
    var type = '';
    var form_hash = {};
    var form_arr = $(".form-horizontal").serializeArray();
    $.each(form_arr, function () {
        form_hash[this.name] = this.value;
    });

    if (parseInt(obj) == 1) {
        objId = ''
        url = "/field_title";
        type = "post";
    }
    if (parseInt(obj) == 2) {
        url = "/field_title/" + objId;
        type = "put"
    }
    $.ajax({
        url: url,
        type: type,
        data: form_hash,
        success: function (data) {
            indexInit();
            $('#myModal').modal('hide');
        },
    });
}
function delete_func(obj_id) {
    $.ajax({
        url: '/field_title/' + String(obj_id) + '.json',
        type: 'delete',
        dataType: 'json',
        success: function (data) {
            indexInit();
           
        },
    });
}