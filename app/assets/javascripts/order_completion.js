$(function(){
//	运维单位
	$.getJSON('/search_region_codes').done(function(data){
			var stationTemp = template('editUnit',data);
			$('#search-unit .choose-site').html(stationTemp);
		})
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	//查询
	var station_id = '';
	var created_time = '';
	var end_time = '';
	var unit_name_id ='';
	var zone_name='';
	$('.public_search .search').click(function(){
		if($('.public_search .control_stations').val()==''){
			$('.public_search .control_stationIDs').val('')
		}
		if($('.public_search .control_stationIDs').val()==''){
			$('#order_completion .public_search .searchSta').val('')
		}
		$('#order_completion .public_search .searchUnit').val($('.public_search .unit_ywID').val())
		$('#order_completion .public_search .searchCity').val($('#order_completion #city').val())
		if($('#order_completion .public_search .searchUnit').val()){
            unit_name_id = $('#order_completion .public_search .searchUnit').val().split(',')
        }else{
        	unit_name_id=''
        }
		zone_name=$('#order_completion .public_search .searchCity').val();
		$('#order_completion .public_search .searchSta').val($('.control_stationIDs').val())
		if($('#order_completion .public_search .searchSta').val()){
            station_id = $('#order_completion .public_search .searchSta').val().split(',')
        }else{
        	station_id=''
        }
		$('#order_completion .public_search .startTime1').val($('.create_datetime').val());
		$('#order_completion .public_search .endTime1').val($('.end_datetime').val());
		created_time = $('#order_completion .public_search .startTime1').val();
    	end_time = $('#order_completion .public_search .endTime1').val();
		if(created_time && end_time){
			created_time=created_time;
			end_time=end_time;
		}else if(created_time){
			end_time = created_time
		}else if(end_time){
			created_time = end_time
		}else{
			created_time = ''
	        end_time = ''
		}
		
		$.ajax({
			type:'get',
			url:'/order_completion.json',
			data:{
				station_id:station_id,
				s_region_code_info_id:unit_name_id,
				region_code:zone_name,
				begion_time:created_time,
				end_time:end_time

			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
		   		$('.page_total small').text(data.list_size);
		   		
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	//详细信息
		var id;
	$('body').delegate('#order_completion table .lookFor','click',function(){
		id=$(this).parent().siblings('.id').text();
		console.log(id)
		$('#orderLook').show()
    	$('#order_completion').hide()
    				//	分页+初始数据
		function lookData(param){
//		pageName_Data()
		var pp = $('#orderLook #page2 .pageItemActive').html();
		var per=$('#orderLook #pagesize2 option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('lookTable',data_hash);
			$('#orderLook #orderLook table .exception-list').html(htmlData);
			$('#orderLook .page_total2 span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/order_completion/'+ id +'.json?&page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var lookHtml = template('lookTable',data);
					$('#orderLook table .exception-list').html(lookHtml);	
					$('#orderLook .page_total2 span small').text(total)
					$('#orderLook .page_total2 span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
		
		$.ajax({
			type:"get",
			url:"/order_completion/"+ id +".json",
			success:function(data){
				var total = data.list_size;
				data_hash=data;
			    $("#orderLook #page2").initPage(total, 1, lookData)
			    $('#orderLook .page_total2 span small').text(total)
				var lookHtml = template('lookTable',data);
				$('#orderLook table .exception-list').html(lookHtml);	
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	
//	$('body').delegate('#lookBack','click',function(){
//		
////		$('#orderLook').hide()
////  	$('#order_completion').show()
//  	
//	})
	//	分页+初始数据
	function originData(param){
		var pp = $('#order_completion #page .pageItemActive').html();
		var per=$('#order_completion #pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('placeTable',data_hash);
			$('#order_completion table .exception-list').html(htmlData);
			$('#order_completion .page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var station_ids = $('#order_completion .public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }else{
            	station_ids=''
            }
            var unit_name_id = $('#order_completion .public_search .searchUnit').val();
            if(unit_name_id){
            	unit_name_id = unit_name_id.split(",");
            }else{
            	unit_name_id=''
            }
			$.ajax({
				url: '/order_completion.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					station_id:station_ids,
					s_region_code_info_id:unit_name_id,
					region_code:$('#order_completion .public_search .searchCity').val(),
					begion_time:$('#order_completion .public_search .startTime1').val(),
					end_time:$('#order_completion .public_search .endTime1').val()
	
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlData = template('placeTable',data);
					$('#order_completion table .exception-list').html(htmlData);
					var total = data.list_size;
					$('#order_completion .page_total span small').text(total)
					$('#order_completion .page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/order_completion.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#order_completion #page").initPage(total, 1, originData)
	    $('#order_completion .page_total span small').text(total)
	})
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/order_completion.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#order_completion #page").initPage(total, 1, originData)
                $('#order_completion .page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
//	$.getJSON('/order_completion.json').done(function(data){
//		console.log(data)
//		$('.loading').hide()
//	    var htmlData = template('placeTable',data);
//		$('#order_completion table .exception-list').html(htmlData);
//	    
//	})


		$('.putTable').click(function(){
    	$(this).attr('disabled',true)
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("method","get");
		form.attr("action","/order_completion/export_file.csv");
		
		$("body").append(form);//将表单放置在web中
		
		form.submit();//表单提交 
    })
})
