$(function(){
	 	//查询
 	var station_id = '';
 	var created_time = '';
	var end_time = '';
	var job_no='';
	var author='';
	var handle_man='';
	var create_type='';
	var job_status='';
	   function public_search(){
			$('#check_ups .public_search .startTime1').val($('.create_datetime').val())
		    $('#check_ups .public_search .endTime1').val($('.end_datetime').val())
			created_time = $('#check_ups .public_search .startTime1').val();
	    	end_time = $('#check_ups .public_search .endTime1').val();
			if(created_time && end_time){
				created_time=created_time;
				end_time=end_time;
			}else if(created_time){
				end_time = created_time
			}else if(end_time){
				created_time = end_time
			}else{
				created_time = ''
		        end_time = ''
			}
			$('#check_ups .public_search .searchSta').val( $('.control_stationIDs').val())
			if($('#check_ups .public_search .searchSta').val()){
	             station_id = $('#check_ups .public_search .searchSta').val().split(',')
	        }else{
	        	 station_id = ''
			}
						
		$('#check_ups .public_search .searchNo').val($('.public_search .job_no').val())
        $('#check_ups .public_search .searchAuthor').val($('.public_search .author').val())
        $('#check_ups .public_search .searchHan').val($('.public_search .search_handle').val())			
		var searchCrea=$('.public_search .create_type option:selected').val();
		$('#check_ups .public_search .searchCrea').val(searchCrea);
		var searchStatus=$('.public_search .job_status option:selected').val();
		$('#check_ups .public_search .searchStatus').val(searchStatus)
		

		job_no=$('#check_ups .public_search .searchNo').val();
	   	author=$('#check_ups .public_search .searchAuthor').val();
	    handle_man= $('#check_ups .public_search .searchHan').val();
	    create_type=$('#check_ups .public_search .searchCrea').val()
	    job_status=$('#check_ups .public_search .searchStatus').val();
		}
	   $('.public_search .search').click(function(){
	   	
		 station_id = '';
	 	 created_time = '';
		 end_time = '';
		 job_no='';
		 author='';
		 handle_man='';
		 create_type='';
		  job_status='';
		 public_search()
//	   	var create_type=$('.public_search .create_type').val();
		$.ajax({
			type:"get",
			url:"/check_ups.json",
			data:{
				created_time:created_time,
				end_time:end_time,
				station_id:station_id,
				job_no:job_no,
				author:author,
				handle_man:handle_man,
				job_status:job_status,
				create_type:create_type
				
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
				$('.page_total small').text(data.list_size);
					$('#check_ups .public_table .exception-list .job_status').each(function(i){
			        	var s=$(this).text()
			        	
			        	if(s=="工单待审核" || s=="工单待复核"	){
							$(this).siblings().find('.workPerson').hide()
							$(this).siblings().find('.fault_writing').hide()
						}
			        	if(s=="工单已审核"){
							$(this).siblings().find('.workPerson').hide()
							$(this).siblings().find('.fault_writing').hide()
						}
				        	
				        	
				    })
					var roles=data.role_name;
			        if(roles=="运维成员"){
			        	$('#check_ups #add').hide();
						$('#check_ups .workPerson').hide()
						$('#check_ups .paiq').hide()
						
			        }
			        var handleMan=data.login_name;
			        $('#check_ups .public_table .exception-list .handleMan').each(function(i){
			        	var phandle=$(this).text();
			        	if(phandle==handleMan){
		//	        		$(this).siblings().find('.d_taskEdit').show()
		
			        	}else{
			        		$(this).siblings().find('.fault_writing').hide()
		//	        		$(this).siblings().find('.workPerson').hide()
		//		        	$(this).siblings().find('.paiq').hide()
			        	}
			        })
			},
			error:function(err){
				console.log(err)
			}
		});
	   })
	
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	$.ajax({
		type:"get",
		url:"/all_stations_area.json",
		success:function(data){
			var stationTemp = template('Addstations',data);
		$('#sq_radioStation .station_contents').html(stationTemp);
	
		},
		error:function(err){
			console.log(err)
		}
	});
	//检查类型
		$.ajax({
			type:"get",
			url:"/work_pros.json?job_type=polling_form",
			success:function(data){
				var weak_jobs = template('weak_jobs',data)
				$('.seeTypes #weakJobs').html(weak_jobs)
				var month_jobs = template('month_jobs',data)
				$('.seeTypes #monthJobs').html(month_jobs)
				var ji_jobs = template('ji_jobs',data)
				$('.seeTypes #jiJobs').html(ji_jobs)
				var hal_year_jobs = template('hal_year_jobs',data)
				$('.seeTypes #hal_yearJobs').html(hal_year_jobs)
			}
		});
		
	$('body').delegate('#check_ups #add','click',function(){
		$('#check_ups').hide();
		$('#inspection_add').show();
	})
		//周
	$('body').delegate('#week_type','click',function(){
		$('#jiJobs').hide();
		$('#hal_yearJobs').hide();
		$('#weakJobs').show();
		$('#monthJobs').hide();
		$(".seeTypes input[type='checkbox']").prop("checked", false);
	})
	//月
	$('body').delegate('#mouth_type','click',function(){
		$('#jiJobs').hide();
		$('#hal_yearJobs').hide();
		$('#weakJobs').hide();
		$('#monthJobs').show();
		$(".seeTypes input[type='checkbox']").prop("checked", false);
	})
	//季
	$('body').delegate('#ji_type','click',function(){
		$('#jiJobs').show();
		$('#hal_yearJobs').hide();
		$('#weakJobs').hide();
		$('#monthJobs').hide();
		$(".seeTypes input[type='checkbox']").prop("checked", false);
	})
	//半年
	$('body').delegate('#hal_type','click',function(){
		$('#jiJobs').hide();
		$('#hal_yearJobs').show();
		$('#weakJobs').hide();
		$('#monthJobs').hide();
		$(".seeTypes input[type='checkbox']").prop("checked", false);
	})
	$('body').delegate('#btnBack','click',function(){
		$('#check_ups').show();
		$('#inspection_add').hide();
	})
	////	点击添加
    $('body').delegate('#inspection_add #addSave','click',function(){
    	var type=$('#workType  input[type="radio"]:checked').val();
		var level=$('#level input[type="radio"]:checked').val();
		var additional = $('#additional span input[type="radio"]:checked').val()
		var station_id=$('#add_form #addControl_stationIDs').val();
		var station_name=$('#add_form #addControl_stations').val();
		var plan = $('#plan .plan').val()
		var unitType = $('#unitType span input[type="radio"]:checked').val()
		if($('.seeTypes span  input[type="checkbox"]:checked')){
			var seeNews = []
			$('.seeTypes span input[type="checkbox"]:checked').each(function(){
                var news = $(this).val()
                 seeNews.push(news)
            })
            var allNews = []
            allNews=seeNews.join(',')
		}
		
		var total = $('#title').val()
		var testarea = $('#content').val()
        if($('#send_type input[type="checkbox"]:checked')){
            var text = []
            $('#send_type input[type="checkbox"]:checked').each(function(){
                var news = $(this).val()
                 text.push(news)
            })
            var allText = []
            allText=text.join(',')
        }


		$.ajax({
			type:"POST",
			url:"/check_ups.json",
			async:true,
			data:{
				d_task_form:{
					fault_type:type,
					urgent_level:level,
					send_type:allText,
					clraring_if:additional,
					d_station_id:station_id,
					station_name:station_name,
					polling_plan_time:plan,
					polling_ops_type:unitType,
					polling_job_type:allNews,
					title:total,
					content:testarea
					
				}
			},
			success:function(data){
				page_state = "change";
				originData()
				$('#check_ups').show();
				$('#inspection_add').hide();
//				$('#add_form .form-group input[type="text"]').val("");
//				
//				$('#add_form .form-group textarea').val("");
				document.getElementById("add_form").reset()
			
			},
			error:function(err){
				console.log(err);
			}
		});
//  	
    	
	})
    
    //  派遣
//	$.getJSON('/search_unit_manys').done(function(data){
//		var unitTemp = template('sendPers',data);
//		$(' #send_per').html(unitTemp);
//	})
	
	var id;
$('body').delegate('.paiq','click',function(){
	
	 id=$(this).parents('tr').find('.id').text();
	 $.ajax({
	 	type:"get",
	 	url:'/check_ups/'+id+'.json',
	 	success:function(data){
	 		var regionId=data.task_from.s_region_code_info_id;
	 		console.log(regionId)
	 		$.ajax({
	 			type:"get",
	 			url:"/search_unit_manys.json?s_region_code_info_id="+regionId,
	 			success:function(data){
	 				var unitTemp = template('sendPers',data);
					$(' #send_per').html(unitTemp);
	 			}
	 		});
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
$('body').delegate('#paiSave','click',function(){
	$('.loading').show()
	var unit_id=$('#myModal #send_per .unit_user input[name="pers"]:checked').parents('.unit_user').siblings('.unit_name').find('.unit_id').text();
	var handle_man_id=$('#myModal #send_per .unit_user input[name="pers"]:checked').val();
	var handle_man=$('#myModal #send_per .unit_user input[name="pers"]:checked').next('span').text();
	console.log(unit_id)
	console.log(handle_man_id)
	console.log(handle_man)
	$.ajax({
	 	type:"PUT",
	 	url:'/search_unit_manys/'+id+'.json',
	 	data:{
			unit_id:unit_id,
			handle_man_id:handle_man_id,
			handle_man:handle_man
		},
	 	success:function(data){
	 		page_state = "change";
	 		originData();
	 		$('.loading').hide()
	 		$('#myModal').modal('hide');
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
//更换处理人
$('body').delegate('.workPerson','click',function(){
	 id=$(this).parents('tr').find('.id').text();
	 console.log(id)
	 $.ajax({
	 	type:"get",
	 	url:'/check_ups/'+id+'.json',
	 	success:function(data){
	 		var regionId=data.task_from.s_region_code_info_id;
	 		$.ajax({
	 			type:"get",
	 			url:"/search_unit_pers.json?s_region_code_info_id="+regionId,
	 			success:function(data){
	 				var unitTemp = template('sendPers',data);
					$(' #send_per').html(unitTemp);
	 			}
	 		});
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
$('body').delegate('#ghSave','click',function(){
	$('.loading').show()
	var unit_id=$('#ghModal #send_per .unit_user input[name="pers"]:checked').parents('.unit_user').siblings('.unit_name').find('.unit_id').text();
	var handle_man_id=$('#ghModal #send_per .unit_user input[name="pers"]:checked').val();
	var handle_man=$('#ghModal #send_per .unit_user input[name="pers"]:checked').next('span').text();
	$.ajax({
	 	type:"PUT",
	 	url:'/search_unit_pers/'+id+'.json',
	 	data:{
			unit_id:unit_id,
			handle_man_id:handle_man_id,
			handle_man:handle_man
		},
	 	success:function(data){
	 		page_state = "change";
	 		originData();
	 		$('.loading').hide()
	 		$('#ghModal').modal('hide');
	 	},
	 	error:function(err){
	 		console.log(err)
	 	}
	 });
})
	
	
		//编辑
	var id;
	var status;
	$('body').delegate('.fault_writing','click',function(){
		$('#check_ups').hide()
		$('#inspection_add').hide()
		
		id=$(this).parents('tr').find('.id').text();
		console.log(id)
		$.ajax({
    		type:"get",
    		url:'/check_ups/'+id + '.json',
    		success:function(data){
    			var dataObj={
					editData:data
				}
				status=data.task_from.job_status;
    			var dTaskFormsNews = template('dTaskFormsEDit',dataObj)
 				$('#d_task_edit').html(dTaskFormsNews)
 				$('#d_task_edit').show();
 				imgSrc(status)
 				var libg=data.flow;
 				status=data.task_from.job_status;
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					
 					$("#d_task_edit .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				
 				}
 				$('#d_task_edit .dTaskTop li').each(function(i){
			
   					var libg=$(this).find('.lbg-txt').text();
   					if(status==libg){
   						
   						var bgs=$(this).index()
   						var lis = $('#d_task_edit .dTaskTop li');
   						
   						for(var j=0;j<=bgs;j++){
   						
							lis[j].className="on";
   						}
   					}
   				})
 				$('#d_task_edit .dTaskTop li').eq(0).addClass("on")
 				
    		},
    		error:function(err){
    			console.log(err)
    		}
    	});
	})
	function imgSrc(job_status){
		if(job_status=="工单待审核" || job_status=="工单已审核"){
			$('.look_insp .handle_name img').attr('src','/images/com.png'); 
		}else{
			$('.look_insp .handle_name img').attr('src','/images/undo.png');
		}
	}
	$('body').delegate('#editBack','click',function(){
		$('#check_ups').show()
		$('#inspection_add').hide()
		$('#d_task_edit').hide();
	})
		//保存
	
	$('body').delegate('#editSave','click',function(){
		var tasks=[];
		$('#d_task_edit #editkXun .look_insp').each(function(i){
			var s=$('#d_task_edit #editkXun .look_insp').eq(i).find('img');
			if(s.length>0){
				
				var taskid=$(this).children('.typesId').text()
				var taskname=$(this).children('.handle_name').text()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"id":taskid,
					"handle_type":taskname,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		var deal_with=$(this).attr("value")
		var task={tasks}
		var objname={task,job_status:deal_with}
		console.log(objname)
		$.ajax({
			type:"put",
			url:'/check_ups/'+id + '.json',
			data:objname,
			success:function(data){
				page_state = "change";
				originData();
				$('#check_ups').show()
				$('#inspection_add').hide()
				$('#d_task_edit').hide();
				
			},
			error:function(err){
				
			}
		});
	})
	
		//完成
	
	$('body').delegate('#editDone','click',function(){
		var tasks=[];
		$('#d_task_edit #editkXun .look_insp').each(function(i){
			var h=$('#d_task_edit #editkXun .look_insp').eq(i).find('img');
			if(h.length>0){
				var taskid2=$(this).children('.typesId').text()
			var taskname2=$(this).children('.handle_name').text()
			var tasksrc2=$(this).children('div').find('img').attr("src")
			var obj={
				"id":taskid2,
				"handle_type":taskname2,
				"image_file":tasksrc2
			}
			tasks.push(obj)	
			}
			
		})
		var un_audit=$(this).attr("value")
		var task={tasks}
		
		var objnames={task,job_status:un_audit}
		console.log(objname)
	$.ajax({
			type:"put",
			url:'/check_ups/'+id + '.json',
			data:objnames,
			success:function(data){
				page_state = "change";
				originData();
				$('#check_ups').show()
				$('#inspection_add').hide()
				$('#d_task_edit').hide();
			},
			error:function(err){
				console.log(err)
			}
		});
		
	})
	
	
	//  //点击查看时
    $('body').delegate('.fault_look','click',function(){
    	
//  	var exception_device_type,exception_device_typeName=[];
    	$('#check_ups').hide()
    	$('#inspection_add').hide()
    	$('#d_task_edit').hide();
//  	$('#d_task_formsNews').show()
    	id=$(this).parents('tr').find('.id').text();
    	$.ajax({
    		type:"get",
    		url:'/check_ups/'+id + '.json',
    		success:function(data){
    			status=data.task_from.job_status;
    			var dataObj={
					editData:data
				}
				console.log(data)
    			var dTaskFormsNews = template('dTaskFormsNews',dataObj)
 				$('#d_task_formsNews').html(dTaskFormsNews)
 				$('#d_task_formsNews').show();
 				var libg=data.flow;
 				
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					$("#d_task_formsNews .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				}
 				$('#d_task_formsNews .dTaskTop li').each(function(i){
 				
   					var libg=$(this).find('.lbg-txt').text();
   					if(status==libg){
   						
   						var bgs=$(this).index()
   						var lis = $('#d_task_formsNews .dTaskTop li');
   						
   						for(var j=0;j<=bgs;j++){
   						
							lis[j].className="on";
   						}
   					}
   				})
 				$('#d_task_formsNews .dTaskTop li').eq(0).addClass("on")
				if(status=="工单处理中"){
 					$('#audit').hide();
 				}
				if(status=="创建工单"){
 					$('#audit').hide();
 					$('#lookXun').hide();
 				}
//				$('#lookXun .look_insp img').zoomify();
				var imgs = document.getElementsByTagName("img");
				imgs[0].focus();
				for(var i = 0;i<imgs.length;i++){
					imgs[i].onclick = expandPhoto;
					imgs[i].onkeydown = expandPhoto;
				}
				imgSrc(status)
    		},
    		error:function(err){
    			console.log(err)
    		}
    	});


	})
//  放大图片
function expandPhoto(){
	var overlay = document.createElement("div");
	overlay.setAttribute("id","overlay");
	overlay.setAttribute("class","overlay");
	document.body.appendChild(overlay);

	var img = document.createElement("img");
	img.setAttribute("class","overlayimg");
	img.src = this.getAttribute("src");
	document.getElementById("overlay").appendChild(img);

	img.onclick = restore;
}
function restore(){
	document.body.removeChild(document.getElementById("overlay"));
	document.body.removeChild(document.getElementById("img"));
}
	
	$('body').delegate('#lookBack','click',function(){
		$('#d_task_formsNews').hide();
    	$('#check_ups').show()
    	$('#inspection_add').hide()
    	$('#d_task_edit').hide();
	})
		//点击检查项弹出电子表格
		var typeId;
		var typeName;
		$('body').delegate('#d_task_edit #editkXun .seeTypes .handle_name','click',function(){
			$('#d_task_edit #dTask').hide();
			typeId=$(this).siblings('.typesCode').text();
			typeName=$(this).children('span').html()

			var Data_DZM = {
                        id:id,
                        name:typeName
                }
                cliclType(Data_DZM);
                $('#d_task_edit #typeListBox').show();
		})
		$('body').delegate('#d_task_formsNews #lookXun .seeTypes .handle_name','click',function(){
			$('#d_task_formsNews #dTask').hide();
			typeId=$(this).siblings('.typesCode').text();
			typeName=$(this).children('span').html()

			var Data_DZM = {
                        id:id,
                        name:typeName
                }
                cliclType(Data_DZM);
                
                $('#d_task_formsNews #typeListBox').show();
                
		})
		function cliclType(Data_DZM){
			
			var url = '';
			//周
			if(typeId =="wk_003"){//站点巡检记录
				url = "/get_form_module/station_inspection_week";
			}else if(typeId =="wk_010"){//其他仪器、设备运行状况检查记录
				url = "/get_form_module/run_status_week";
			}else if(typeId =="wk_001"){//每周(次)巡检工作汇总表
				url = "/get_form_module/inspection_work_week";
			}else if(typeId =="wk_009" || typeId =="wk_008"){
//				颗粒物PM2.5自动监测分析仪运行状况检查记录\颗粒物PM10自动监测分析仪运行状况检查记录
				url = "/get_form_module/pm_check_week";
			}else if(typeId =="wk_002"){//长光程(SO2,NO2,O3)分析仪器运行状况检查记录表
				url = "/get_form_module/optical_distance_week";
			}else if(typeId =="wk_006"){//臭氧（O3）分析仪运行状况检查记录表
				url = "/get_form_module/analyser_status_week";
			}else if(typeId =="wk_005"){//氮氧化物（NOX）分析仪运行状况检查记录表
				url = "/get_form_module/nox_analyser_status_week";
			}else if(typeId =="wk_004"){//二氧化硫（SO2）分析仪运行状况检查记录表（每周）    
				url = "/get_form_module/so2_analyser_status_week";
			}else if(typeId =="wk_007"){//一氧化碳（CO）分析仪运行状况检查记录表
				url = "/get_form_module/co_analyser_status_week";
			}
			//月
			else if(typeId =="mh_001"){//站点设备维护记录
				url = "/get_form_module/site_plant_maintenance_month";
			}else if(typeId =="mh_002"){//气体分析仪流量检查记录表
				url = "/get_form_module/gas_analyzer_FlowCheck_month";
			}else if(typeId =="mh_003"){//多气体动态校准仪校准检查记录表
				url = "/get_form_module/multiGas_dynamic_calibrator_month";
			}
			//季度
			else if(typeId =="jd_001"|| typeId =="jd_002" || typeId =="jd_003" || typeId =="jd_004"){
				//气体分析仪（二氧化硫、臭氧、二氧化氮、一氧化碳）多点校准记录表（每季度）
				url = "/get_form_module/multipoint_calibration_quarter";
			}else if(typeId =="jd_005"|| typeId =="jd_008" || typeId =="jd_006" || typeId =="jd_007"){
				//（二氧化硫、臭氧、二氧化氮、一氧化碳)仪器精密度审核记录表（每季度）
				url = "/get_form_module/instrument_precision_review_quarter";
			}else if(typeId =="jd_009"){
//				站点设备清洁记录（每季度）
				url = "/get_form_module/site_equipment_cleaning_quarter";
			}else if(typeId =="jd_010"){
				//颗粒物温度、压力校准记录表
				url = "/get_form_module/grain_temperature_pressure_calibration_quarter";
			}else if (typeId =="jd_011" || typeId =="jd_012"){
				//颗粒物PM10、PM2.5自动监测分析仪运行状况检查记录（每季度）   
				url = "/get_form_module/automatic_monitoring_analyzer_quarter";
			}else if(typeId =="jd_013" || typeId =="jd_014"){
				//长光程SO2\NO2分析仪单点校准检查记录表（每季度）
				url = "/get_form_module/long_path_analyzer_quarter";
			}else if(typeId =="jd_015"){
				//长光程O3分析仪单点校准检查记录表（每季度）
				url = "/get_form_module/o3long_path_analyzer_quarter";
			}
			//半年
			else if(typeId =="yr_001"){
				//站点设备半年维护记录（每半年）
				url = "/get_form_module/site_equipment_maintain_year";
			}else if(typeId =="yr_002"){
				//氮氧化物分析仪钼炉转化率记录表（每半年）
				url = "/get_form_module/nox_analyzer_mofce_year";
			}else if(typeId =="yr_003"){
				//多气体动态校准仪校准检查记录表
				url = "/get_form_module/multi_gas_dynamic_calibrator_year";
			}else if(typeId =="yr_005"){
				//能见度分析仪校准记录表（每半年）  
				url = "/get_form_module/visibility_analyzer_calibration_year";
			}else if(typeId =="yr_004"){
				//臭氧（O3）校准仪（工作标准）量值传递记录表    
				url = "/get_form_module/o3_calibrator_value_transfer";
			}else if(typeId=="mh_004"){
				// 颗粒物手工比对采样记录表（每月）
				url="/get_form_module/manual_comparison_particulates_month"
			}
			
                $.ajax({
                    type:"get",
					url:url,
                    data:Data_DZM,
                    async:true,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
					   $('#d_task_edit #typeListBox').html(data)
					   $('#d_task_formsNews #typeListBox').html(data)
					   $('#typeListBox').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#d_task_formsNews #typeListBox table input').attr("disabled",true)
						$('#d_task_formsNews #typeListBox table textarea').attr("disabled",true)
						
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
//							minDate:'2017-01-01',
//							maxDate: year+'-12-31',
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
					//		minDate:year+'-01-01',
					//		maxDate: year+'-12-31',
							todayButton:true,
							step:5
						});
						if(status=="工单待审核" || status=="工单已审核" || status=="工单待复核"){
							
							$('#typeListBox #supplyLook').show();
							$('#typeListBox #supplyEdit').hide();
						}else{
						
							$('#typeListBox #supplyEdit').show();
							$('#typeListBox #supplyLook').hide();
							
						}
						$('#d_task_formsNews #typeListBox #supplyLook').show()
						$('#d_task_formsNews #typeListBox #supplyEdit').hide()
						var imgs = document.getElementsByTagName("img");
						imgs[0].focus();
						for(var i = 0;i<imgs.length;i++){
							imgs[i].onclick = expandPhoto;
							imgs[i].onkeydown = expandPhoto;
						}
                       
                    },
                    error:function(err){
                        console.log(err);
                        $('.loading').hide();
                    }
                });
		}
$('body').delegate('#d_task_edit #typeListBox #Back','click',function(){
	$('#d_task_edit #dTask').show();
	$('#d_task_edit #typeListBox').hide();
})
$('body').delegate('#d_task_formsNews #typeListBox #Back','click',function(){
	$('#d_task_formsNews #dTask').show();
	$('#d_task_formsNews #typeListBox').hide();
})
//颗粒物手工比对采样记录表查看页面
		 $('body').delegate(' #typeListBox table .btn-sm','click',function(){
			$(' #dTask').hide();
			$('#typeListBox').hide();
			$('#weighing').hide();
				var typeName=$(this).find('i').attr('sampling_record');
				$.ajax({
                    type:"get",
					url:"/get_form_module/sampling_record_table",
                    data:{
                    	id:id,
                    	code:typeName
                    },
                    async:true,
                    cache:false,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
					   $('#sampling').html(data);
						  var fu=typeName.substring(16)
							var fujianID="weigh_record_"+fu
							console.log(fujianID)
						 $('#sampling .fujianID').val(fujianID)  
					   $('#sampling').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#d_task_formsNews #sampling table input').attr("disabled",true)
						$('#d_task_formsNews #sampling table textarea').attr("disabled",true)
						$('#d_task_formsNews #sampling #Save').hide();
						$('#d_task_formsNews #weighing #Save').hide();
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
							todayButton:true,
							step:5
						});
                       
                    },
                    error:function(err){
                        console.log(err);
                        $('.loading').hide();
                    }
				});
				$(' #sampling').show();

		 })
			 	//采样记录表保存
	$('body').delegate('#sampling #Save','click',function(){
		var datas=[]
	var objname={}

	var f_form_module_id=$('#sampling .modalID').html()

		var form_arr = $(".form-horizontal2").serializeArray();
	
		$.each(form_arr, function () {
			checkname=this.name;
			checkvalue=this.value;
			var list={
				check_name:checkname,
				check_value:checkvalue
			}
			datas.push(list)
		
			console.log(datas)
		
		
		});
		objname={datas,f_form_module_id:f_form_module_id}
		$.ajax({
			type:"put",
			url:'/get_form_module/'+id+'/save',
			data:objname,
			success:function(data){
				
				typeId=data.id
				var name=data.name
				$('#d_task_edit #typeListBox').html(" ")
				$('#d_task_edit #sampling').hide();
				$('#d_task_edit #dTask').hide();
				$('#weighing').hide();
				var Data_DZM = {
                        id:id,
                        name:typeName
                }
                cliclType(Data_DZM);
			 	 $('#d_task_edit #typeListBox').show();
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	$('body').delegate('#sampling #Back','click',function(){
		$(' #sampling').hide();
		$('#weighing').hide();
		$(' #dTask').hide();
		$(' #typeListBox').show();
		$('.loading').hide();
	})
						//采样记录表附件表
 $('body').delegate(' #sampling input[name="fujian"]','click',function(){
	$(' #dTask').hide();
			$('#typeListBox').hide();
			$(' #sampling').hide();
				var typeName=$('#sampling .fujianID').val();
				$.ajax({
                    type:"get",
					url:"/get_form_module/weighing_record_table",
                    data:{
                    	id:id,
                    	code:typeName
                    },
                    async:true,
                    cache:false,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
					   $('#weighing').html(data);
						  var fu=typeName.substring(16)
						 $('#sampling .fujianID').val("weigh_record_"+fu)  
					   $('#sampling').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#d_task_formsNews #weighing table input').attr("disabled",true)
						$('#d_task_formsNews #weighing table textarea').attr("disabled",true)
						$('#d_task_formsNews #sampling #Save').hide();
						$('#d_task_formsNews #weighing #Save').hide();
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
							todayButton:true,
							step:5
						});
                       
                    },
                    error:function(err){
                        console.log(err);
						$('.loading').hide();
						
                    }
				});
				
				$('#weighing').show();
 })
	$('body').delegate('#weighing #Back','click',function(){
		$(' #sampling').show();
		$('#weighing').hide();
		$(' #dTask').hide();
		$(' #typeListBox').hide();
		$('.loading').hide();
	})
	//保存
	$('body').delegate('#weighing #Save','click',function(){
		var datas=[]
	var objname={}

	var f_form_module_id=$('#weighing .modalID').html()

		var form_arr = $(".form-horizontal3").serializeArray();
	
		$.each(form_arr, function () {
			checkname=this.name;
			checkvalue=this.value;
			var list={
				check_name:checkname,
				check_value:checkvalue
			}
			datas.push(list)
		
			console.log(datas)
		
		
		});
		objname={datas,f_form_module_id:f_form_module_id}
		$.ajax({
			type:"put",
			url:'/get_form_module/'+id+'/save',
			data:objname,
			success:function(data){
				$('#sampling').show();
				$('#dTask').hide();
				$('#weighing').hide();
			 	 $('#typeListBox').hide();
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	//检查项编辑保存
$('body').delegate('#typeListBox #Save','click',function(){
	$('.loading').show()
	var datas=[]
	var objname={}
	var f_form_module_id=$('#typeListBox .modalID').html()
	var tasks=[]
	
	if(f_form_module_id=="2"){
		var handle_value=$('#typeListBox #remark').val()
		var handle_name=$('#typeListBox #remark').siblings('p').html()
	    $('#typeListBox table .select-check-box').each(function(){
	    	var  check_name=$(this).parent('td').siblings('td').find('p').html();
	    	
	    	var check_value=$(this).find('.checkbox-radio input[type="radio"]:checked').val();
	    	
	    	var obj={
						"check_name": check_name,
						"check_value":check_value
					}
	    	datas.push(obj)
		})
	    var han={
	    	"check_name": handle_name,
			"check_value":handle_value
	    }
	    datas.push(han)

	    $('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="1"){
		var checkTime=$('#typeListBox table .check-list-table .start_datetime').val();
		var checkTime_name=$('#typeListBox table .check-list-table .check_timeName').html();
		checkTime_name = checkTime_name.substr(0, checkTime_name.length - 1);  
		var time={
			"check_name": checkTime_name,
			"check_value":checkTime
		}
		datas.push(time)
		$('#typeListBox table .instrument_name').each(function(){
			var instrument_name=$(this).children('p').html();
			var instrument_code=$(this).siblings().children('.instrument_code').val();
			var instrument_status=$(this).siblings().children('.instrument_status').val();
			var instrument1={
				"check_name": instrument_name+"_01",
				"check_value":instrument_code
			}
			datas.push(instrument1)
			var instrument2={
				"check_name": instrument_name+"_02",
				"check_value":instrument_status
			}
			datas.push(instrument2)
			console.log(datas)
		})

		console.log(objname)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="3"){
		$('#typeListBox table td textarea').each(function(){
			var check_value=$(this).val()
			var check_name=$(this).parent().siblings().html();
			
			var instrument1={
				"check_name":check_name,
				"check_value":check_value
			}
			datas.push(instrument1)
	
		})
		$('#typeListBox table td input[name="save"]').each(function(){
			var check_value=$(this).val()
			var check_name=$(this).parent().prev().html();
			
			var instrument2={
				"check_name":check_name,
				"check_value":check_value
			}
			datas.push(instrument2)
			console.log(datas)
		})
	$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="4" || f_form_module_id=="5"){
		$('#typeListBox table .td-text-left').each(function(){
			var check_name=$(this).children('p').html();
			var check_value=$(this).next().children().val();
			var instrument1={
				"check_name":check_name,
				"check_value":check_value
			}
			datas.push(instrument1)
		})
		$('#typeListBox table .td-text-left2').each(function(){
			var check_name=$(this).children('p').html();
			var check_value=$(this).siblings().children('.check_value').val();
			var if_normal=$(this).siblings().children('.if_normal').val();
			var abnormal_handle=$(this).siblings().children('.abnormal_handle').val();
			var instrument2={
				"check_name":check_name+"_01",
				"check_value":check_value
			}
			datas.push(instrument2)
			var instrument3={
				"check_name":check_name+"_02",
				"check_value":if_normal
			}
			datas.push(instrument3)
			var instrument4={
				"check_name":check_name+"_03",
				"check_value":abnormal_handle
			}
			datas.push(instrument4)
		})
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="6"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue=$(this).parent('td').next().children().val();
			var list1={
				"check_name":listName,
				"check_value":listValue
			}
			datas.push(list1)
			console.log(datas)
		})
		$('#typeListBox table .list_name2').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.listValue01').val();
			var listValue2=$(this).parent('td').siblings().children('.listValue02').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			console.log(datas)
		})
		$('#typeListBox table .list_name3').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.start_time').val();
			var listValue2=$(this).parent('td').siblings().children('.end_time').val();
			var listValue3=$(this).parent('td').siblings().children('input[name="calibrated_value"]').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			
		})		
	$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="7" || f_form_module_id=="11" || f_form_module_id=="12" || f_form_module_id=="13"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue=$(this).parent('td').next().children().val();
			var list1={
				"check_name":listName,
				"check_value":listValue
			}
			datas.push(list1)
		
		})
		$('#typeListBox table .list_name2').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.start_time').val();
			var listValue2=$(this).parent('td').siblings().children('.end_time').val();
			var listValue3=$(this).parent('td').siblings().children('.ppm').val();
			var listValue4=$(this).parent('td').siblings().children('.calibrated_value').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			var list4={
				"check_name":listName+"_04",
				"check_value":listValue4
			}
			datas.push(list4)
		
		})
		$('#typeListBox table .list_name3').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.check_value1').val();
			var listValue2=$(this).parent('td').siblings().children('.check_value2').val();
			var listValue3=$(this).parent('td').siblings().children('.check_value3').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			
		})
		$('#typeListBox table .list_name4').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.check_value1').val();
			var listValue2=$(this).parent('td').siblings().children('.check_value2').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			
		})
		console.log(datas)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="14" || f_form_module_id=="25" || f_form_module_id=="36"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.start_datetime').val();
			var listValue2=$(this).parent('td').siblings().children('.form-control').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
		})
		var listValue3=$('#typeListBox table td textarea').val();
		var listName2=$('#typeListBox table td textarea').siblings('.pb10').html();
		var list3={
				"check_name":listName2,
				"check_value":listValue3
			}
			datas.push(list3)
			console.log(datas)
			$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="15"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('input[name="DEVICEMODEL"]').val();
			var listValue2=$(this).parent('td').siblings().children('input[name="FLOWRANG"]').val();
			var listValue3=$(this).parent('td').siblings().children('input[name="DISPLAYVALUE"]').val();
			var listValue4=$(this).parent('td').siblings().children('input[name="MEASUREDVALUE"]').val();
			var listValue5=$(this).parent('td').siblings().children('input[name="MEASUREDERROR"]').val();
			var listValue6=$(this).parent('td').siblings().children('input[name="SITUATION"]').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			var list4={
				"check_name":listName+"_04",
				"check_value":listValue4
			}
			datas.push(list4)
			var list5={
				"check_name":listName+"_05",
				"check_value":listValue5
			}
			datas.push(list5)
			var list6={
				"check_name":listName+"_06",
				"check_value":listValue6
			}
			datas.push(list6)
		})
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="16"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
			
		})
		$('#typeListBox table .list_name2').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('input[name="FLOWDEVICEVALUE"]').val();
			var listValue2=$(this).parent('td').siblings().children('input[name="FLOWMETERVALUE"]').val();
			var listValue3=$(this).parent('td').siblings().children('input[name="FLOWERRORVALUE"]').val();
			var listValue4=$(this).parent('td').siblings().children('input[name="FLOWQUALITYVALUE"]').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			var list4={
				"check_name":listName+"_04",
				"check_value":listValue4
			}
			datas.push(list4)

		})
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="17" || f_form_module_id=="18" || f_form_module_id=="19" || f_form_module_id=="20"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
			
		})
		$('#typeListBox table .list_name2').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.start_time').val();
			var listValue2=$(this).parent('td').siblings().children('.end_time').val();
			var listValue3=$(this).parent('td').siblings().children('input[name="LINGDIANBZ"]').val();
			var listValue4=$(this).parent('td').siblings().children('input[name="LINGDIANBY1"]').val();
			var listValue5=$(this).parent('td').siblings().children('input[name="LINGDIANBY2"]').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			var list4={
				"check_name":listName+"_04",
				"check_value":listValue4
			}
			datas.push(list4)
			var list5={
				"check_name":listName+"_05",
				"check_value":listValue5
			}
			datas.push(list5)
			
		})
		var checkedValue=$('#typeListBox table td input[type="radio"]:checked').val();
		var checkedName=$('#typeListBox table td input[type="radio"]').parents('td').siblings().children().html();
		var list6={
				"check_name":checkedName,
				"check_value":checkedValue
			}
			datas.push(list6)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="21" || f_form_module_id=="22" || f_form_module_id=="23" || f_form_module_id=="24"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
			
		})
		var checkedValue=$('#typeListBox table td input[type="radio"]:checked').val();
		var checkedName=$('#typeListBox table td input[type="radio"]').parents('td').siblings().children().html();
		
			var list6={
					"check_name":checkedName,
					"check_value":checkedValue
				}
			datas.push(list6)
			
			console.log(datas)
			$('#typeListBox table .yiqisz-input').each(function(i){
				var checkName=$(this).parent('td').siblings().children('p').html()
				var checkValue=$(this).val();
//				var ss=[]
				var list={
					"check_name":checkName+"_0"+(i+1),
					"check_value":checkValue
				}
				datas.push(list)
			})
			$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="26"){
		var checkedValue=$('#typeListBox table td .create_datetime ').val();
		var checkedName=$('#typeListBox table td .create_datetime').parent('td').siblings().children('p').html();
		
			var list={
					"check_name":checkedName,
					"check_value":checkedValue
				}
			datas.push(list)
			$('#typeListBox table .list_name').each(function(){
				var ckname=$(this).html()
				var temName=$(this).parent().siblings().children('p').html()
				var temValue1=$(this).parent().siblings().children('.check_value1').val()
				var temValue2=$(this).parent().siblings().children('.check_value2').val()
				var temValue3=$(this).parent().siblings().children('.check_value3').val()
				var temValue4=$(this).parent().siblings().children('.check_value4').val()
				var pressureName=$(this).parents('tr').next().children('td').children('p').html()
				var pressureValue1=$(this).parents('tr').next().children('td').children('.check_value1').val()
				var pressureValue2=$(this).parents('tr').next().children('td').children('.check_value2').val()
				var pressureValue3=$(this).parents('tr').next().children('td').children('.check_value3').val()
				var pressureValue4=$(this).parents('tr').next().children('td').children('.check_value4').val()
				var temlist1={
					"check_name":ckname+"_"+temName+"_01",
					"check_value":temValue1
				}
				datas.push(temlist1)
				var temlist2={
					"check_name":ckname+"_"+temName+"_02",
					"check_value":temValue2
				}
				datas.push(temlist2)
				var temlist3={
					"check_name":ckname+"_"+temName+"_03",
					"check_value":temValue3
				}
				datas.push(temlist3)
				var temlist4={
					"check_name":ckname+"_"+temName+"_04",
					"check_value":temValue4
				}
				datas.push(temlist4)
				var prelist1={
					"check_name":ckname+"_"+pressureName+"_01",
					"check_value":pressureValue1
				}
				datas.push(prelist1)
				var prelist2={
					"check_name":ckname+"_"+pressureName+"_02",
					"check_value":pressureValue2
				}
				datas.push(prelist2)
				var prelist3={
					"check_name":ckname+"_"+pressureName+"_03",
					"check_value":pressureValue3
				}
				datas.push(prelist3)
				var prelist4={
					"check_name":ckname+"_"+pressureName+"_04",
					"check_value":pressureValue4
				}
				datas.push(prelist4)
				console.log(datas)
			})
			$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="30" || f_form_module_id=="31"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
			
		})
		$('#typeListBox table td input[name="checkvalue1"]').each(function(i){
			var checkName=$(this).parent('td').siblings().children('p').html()
			var checkValue=$(this).val();
			var list={
				"check_name":checkName+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		$('#typeListBox table td input[name="checkvalue2"]').each(function(i){
			var checkName=$(this).parent('td').siblings().children('p').html()
			var checkValue=$(this).val();
			var list={
				"check_name":checkName+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		console.log(datas)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="27" || f_form_module_id=="28" || f_form_module_id=="29"){
		var standard=$('#typeListBox table td .standard').html();
		var pool_length=$('#typeListBox table td .pool_length').html();
		var Optica_path=$('#typeListBox table td .Optica_path').html();
		var concentration=$('#typeListBox table td .concentration').html();
		var Response=$('#typeListBox table td .Response').html();
		var after_calibration=$('#typeListBox table td .after_calibration').html();
		$('#typeListBox table td input[name="DENSITYVALUE"]').each(function(i){
			var checkValue=$(this).val();
			var list={
				"check_name":standard+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		$('#typeListBox table td input[name="SIZEVALUE"]').each(function(i){
			var checkValue=$(this).val();
			var list={
				"check_name":pool_length+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		$('#typeListBox table td input[name="OPTPATHVALUE"]').each(function(i){
			var checkValue=$(this).val();
			var list={
				"check_name":Optica_path+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		$('#typeListBox table td input[name="EQDENSITYVALUE"]').each(function(i){
			var checkValue=$(this).val();
			var list={
				"check_name":concentration+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		$('#typeListBox table td input[name="RESPONSEVALUE"]').each(function(i){
			var checkValue=$(this).val();
			var list={
				"check_name":Response+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		$('#typeListBox table td input[name="SCORESPONSEVALUE"]').each(function(i){
			var checkValue=$(this).val();
			var list={
				"check_name":after_calibration+"_0"+(i+1),
				"check_value":checkValue
			}
			datas.push(list)
		})
		$('#typeListBox table td .list_name').each(function(i){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
		})
		$('#typeListBox table td .list_name2').each(function(i){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.check_value1').val();
			var listValue2=$(this).parent('td').siblings().children('.check_value2').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
		})
		console.log(datas)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="32" || f_form_module_id=="35" ){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
			
		})
		$('#typeListBox table .list_name2').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('.check_value1').val();
			var listValue2=$(this).parent('td').siblings().children('.check_value2').val();
			var listValue3=$(this).parent('td').siblings().children('.check_value3').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			
		})
		console.log(datas)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="33"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
		
		})
		var l_min=$('#typeListBox table .l_min').html();
		var ml_min=$('#typeListBox table .ml_min').html();
		$('#typeListBox table .list_name2').each(function(){
		
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			var list1={
				"check_name":l_min+"_"+listName,
				"check_value":listValue1
			}
			datas.push(list1)
			
		})
		$('#typeListBox table .list_name3').each(function(){
		
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			var list1={
				"check_name":ml_min+"_"+listName,
				"check_value":listValue1
			}
			datas.push(list1)
			
		})
		$('#typeListBox table .list_name4').each(function(){
			var ckname=$(this).html()
			var temValue1=$(this).parent().siblings().children('.check_value1').val()
			var temValue2=$(this).parent().siblings().children('.check_value2').val()
			var temValue3=$(this).parent().siblings().children('.check_value3').val()
			var temValue4=$(this).parent().siblings().children('.check_value4').val()
			var list1={
				"check_name":ckname+"_01",
				"check_value":temValue1
			}
			datas.push(list1)
			var list2={
				"check_name":ckname+"_02",
				"check_value":temValue2
			}
			datas.push(list2)
			var list3={
				"check_name":ckname+"_03",
				"check_value":temValue3
			}
			datas.push(list3)
			var list4={
				"check_name":ckname+"_04",
				"check_value":temValue4
			}
			datas.push(list4)
		})
		console.log(datas)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}else if(f_form_module_id=="34"){
		$('#typeListBox table .list_name').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').next().children().val();
			
			var list1={
				"check_name":listName,
				"check_value":listValue1
			}
			datas.push(list1)
		
		})
		$('#typeListBox table .list_name2').each(function(){
			var listName=$(this).html();
			var listValue1=$(this).parent('td').siblings().children('input[name="DENSITYVALUE"]').val();
			var listValue2=$(this).parent('td').siblings().children('input[name="DELIVERVALUE"]').val();
			var listValue3=$(this).parent('td').siblings().children('input[name="WORKDENSITYVALUE"]').val();
			var listValue4=$(this).parent('td').siblings().children('input[name="DELIVERFROMVALUE"]').val();
			var listValue5=$(this).parent('td').siblings().children('input[name="DELIVERTOVALUE"]').val();
			var list1={
				"check_name":listName+"_01",
				"check_value":listValue1
			}
			datas.push(list1)
			var list2={
				"check_name":listName+"_02",
				"check_value":listValue2
			}
			datas.push(list2)
			var list3={
				"check_name":listName+"_03",
				"check_value":listValue3
			}
			datas.push(list3)
			var list4={
				"check_name":listName+"_04",
				"check_value":listValue4
			}
			datas.push(list4)
			var list5={
				"check_name":listName+"_05",
				"check_value":listValue5
			}
			datas.push(list5)

		})
		var calibration_curve=$('#typeListBox table .calibration_curve').html();
		var AVALUE=$('#typeListBox table #AVALUE').val();
		var BVALUE=$('#typeListBox table #BVALUE').val();
		var RVALUE=$('#typeListBox table #RVALUE').val();
		var list1={
				"check_name":calibration_curve+"_01",
				"check_value":AVALUE
			}
			datas.push(list1)
			var list2={
				"check_name":calibration_curve+"_02",
				"check_value":BVALUE
			}
			datas.push(list2)
			var list3={
				"check_name":calibration_curve+"_03",
				"check_value":RVALUE
			}
			datas.push(list3)
		console.log(datas)
		$('#supplyDemo .supcon').each(function(i){
	
			var s=$('#supplyDemo .supcon div').eq(i).find('img');

			if(s.length>0){
				
				var taskname=$(this).siblings().children('#ITEMID').val()
				var taskcode=$(this).siblings().children('input[name="supplyCode"]').val()
				var tasksrc=$(this).children('div').find('img').attr("src")
				
				var obj={
					"cons_name":taskname,
					"num":taskcode,
					"image_file":tasksrc
				}
				tasks.push(obj)
			}
			
		})
		console.log(tasks)
		var task={tasks}
	}


		objname={datas,f_form_module_id:f_form_module_id,task}
 Submit(objname)
})

	function Submit(objname){
		$.ajax({
			type:"put",
			url:'/get_form_module/'+id,
			data:objname,
			success:function(data){
				$('#d_task_edit #dTask').show();
				$('#d_task_edit #typeListBox').hide();
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
	}
//	//审核
//	var checkid;
//	$('body').delegate('.fault_audit','click',function(){
//		$('#check_ups').hide();
//		$('#inspection_add').hide()
//  	$('#d_task_edit').hide();
//  	$('#d_task_formsNews').hide();
//	       checkid=$(this).parents('tr').find('.id').text();
//	       $.ajax({
//  			type:"get",
//  			url:'/check_ups/'+checkid + '.json',
//  			success:function(data){
//  				var dataObj = {
//						editData:data
//					}
//					var originData = template('faultAudit',dataObj)
//					$('#ops_audits').html(originData)
//					$('#ops_audits').show();
//  			},
//  			error:function(err){
//  				console.log(err)
//  			}
//  		});
//	})
//		$('body').delegate('#ops_audits .save','click',function(){
//			
//			var audit_if=$('#ops_audits #audit_adopt .checkbox-radio input[name="Approve"]:checked').val();
//			var audit_des=$('#ops_audits #audit_not').val();
//			console.log(audit_if)
//			console.log(audit_des)
// 			$.ajax({
// 				type:"put",
// 				url:'/check_ups/'+checkid + '.json',
// 				data:{
// 					audit_if:audit_if,
// 					audit_des:audit_des
// 				},
// 				success:function(data){
//					 
//					console.log(data)
//
// 					page_state = "change";
//					alert("提交成功");
//					originData();
//					$('#d_task_formsNews').hide();
//			    	$('#check_ups').show()
//			    	$('#inspection_add').hide()
//			    	$('#d_task_edit').hide();
//			    	$('#ops_audits').hide()
// 				},
// 				error:function(err){
// 					console.log(err)
// 				}
// 			});
// 		})
// 		$('body').delegate('#ops_audits .saveback','click',function(){
//	        $('#d_task_formsNews').hide();
//	    	$('#check_ups').show()
//	    	$('#inspection_add').hide()
//	    	$('#d_task_edit').hide();
//	    	$('#ops_audits').hide()
//		})
	//	分页+初始数据
	function originData(param){
		// public_search();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('Tables',data_hash);
			$('#check_ups table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
			data_hash = {};
			var station_ids = $('#check_ups .public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
			$.ajax({
				url: '/check_ups.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					created_time:$('#check_ups .public_search .startTime1').val(),
					end_time:$('#check_ups .public_search .endTime1').val(),
					station_id:station_ids,
					job_no:$('#check_ups .public_search .searchNo').val(),
					author:$('#check_ups .public_search .searchAuthor').val(),
					handle_man:$('#check_ups .public_search .searchCrea').val(),
					job_status:$('#check_ups .public_search .searchStatus').val(),
					create_type:$('#check_ups .public_search .searchCrea').val()
				
			},
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlData = template('Tables',data);
					$('#check_ups table .exception-list').html(htmlData);
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
					$('#check_ups .public_table .exception-list .job_status').each(function(i){
			        	var s=$(this).text()
			        	
			        	if(s=="工单待审核" || s=="工单待复核"	){
							$(this).siblings().find('.workPerson').hide()
							$(this).siblings().find('.fault_writing').hide()
						}
			        	if(s=="工单已审核"){
							$(this).siblings().find('.workPerson').hide()
							$(this).siblings().find('.fault_writing').hide()
						}
				        	
				        	
				    })
					var roles=data.role_name;
			        if(roles=="运维成员"){
			        	$('#check_ups #add').hide();
						$('#check_ups .workPerson').hide()
						$('#check_ups .paiq').hide()
						
			        }
			        var handleMan=data.login_name;
			        $('#check_ups .public_table .exception-list .handleMan').each(function(i){
			        	var phandle=$(this).text();
			        	if(phandle==handleMan){
		//	        		$(this).siblings().find('.d_taskEdit').show()
		
			        	}else{
			        		$(this).siblings().find('.fault_writing').hide()
		//	        		$(this).siblings().find('.workPerson').hide()
		//		        	$(this).siblings().find('.paiq').hide()
			        	}
			        })
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/check_ups.json').done(function(data){
		console.log(data)
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	    $('#check_ups .public_table .exception-list .job_status').each(function(i){
        	var s=$(this).text()
        	
        	if(s=="工单待审核" || s=="工单待复核"	){
				$(this).siblings().find('.workPerson').hide()
				$(this).siblings().find('.fault_writing').hide()
			}
	        if(s=="工单已审核"){
				$(this).siblings().find('.workPerson').hide()
				$(this).siblings().find('.fault_writing').hide()
			}	
	        	
	    })
	     	var roles=data.role_name;
	        if(roles=="运维成员"){
	        	$('#check_ups #add').hide();
				$('#check_ups .workPerson').hide()
				$('#check_ups .paiq').hide()
				
	        }
	        var handleMan=data.login_name;
	        $('#check_ups .public_table .exception-list .handleMan').each(function(i){
	        	var phandle=$(this).text();
	        	if(phandle==handleMan){
//	        		$(this).siblings().find('.d_taskEdit').show()

	        	}else{
	        		$(this).siblings().find('.fault_writing').hide()
//	        		$(this).siblings().find('.workPerson').hide()
//		        	$(this).siblings().find('.paiq').hide()
	        	}
	        })
	})
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/check_ups.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})

function previewImage(file, prvid) { 
	/* file：file控件 
	* prvid: 图片预览容器 
	*/ 
	var tip = "Expect jpg or png or gif!"; // 设定提示信息 
	var filters = { 
	"jpeg" : "/9j/4", 
	"gif" : "R0lGOD", 
	"png" : "iVBORw" 
	} 
	var prvbox = document.getElementById(prvid); 
	prvbox.innerHTML = ""; 
	if (window.FileReader) { // html5方案 
		for (var i=0, f; f = file.files[i]; i++) { 
			var fr = new FileReader(); 
			fr.onload = function(e) { 
				var src = e.target.result; 
				if (!validateImg(src)) { 
					alert(tip) 
				} else { 
					showPrvImg(src); 
				} 
			} 
			fr.readAsDataURL(f); 
		} 
	} else { // 降级处理
	
		if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) { 
			alert(tip); 
		} else { 
			showPrvImg(file.value); 
		} 
	} 
	
		function validateImg(data) { 
			var pos = data.indexOf(",") + 1; 
			for (var e in filters) { 
				if (data.indexOf(filters[e]) === pos) { 
					return e; 
				} 
			} 
			return null; 
		} 
	
		function showPrvImg(src) { 
			var img = document.createElement("img"); 
			img.src = src; 
			prvbox.appendChild(img); 
			$('.look_insp img').zoomify();
		} 
}
var index=0
function previewImage2(file, supplyPrvid) { 
	index++


	/* file：file控件 
	* prvid: 图片预览容器 
	*/ 
	var tip = "Expect jpg or png or gif!"; // 设定提示信息 
	var filters = { 
	"jpeg" : "/9j/4", 
	"gif" : "R0lGOD", 
	"png" : "iVBORw" 
	} 
	var prvbox = document.getElementById(supplyPrvid); 

	prvbox.innerHTML = ""; 
	if (window.FileReader) { // html5方案 
		for (var i=0, f; f = file.files[i]; i++) { 
			var fr = new FileReader(); 
			fr.onload = function(e) { 
				var src = e.target.result; 
				if (!validateImg(src)) { 
					alert(tip) 
				} else { 
					showPrvImg(src); 
				} 
			} 
			fr.readAsDataURL(f); 
		} 
	} else { // 降级处理
	
		if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) { 
			alert(tip); 
		} else { 
			showPrvImg(file.value); 
		} 
	} 
	
		function validateImg(data) { 
			
			var pos = data.indexOf(",") + 1; 
			for (var e in filters) { 
				if (data.indexOf(filters[e]) === pos) { 
					return e; 
				} 
			} 
			return null; 
		} 
	
		function showPrvImg(src) { 
			
			var img = document.createElement("img"); 
			img.src = src; 
			prvbox.appendChild(img); 
		
		} 
}