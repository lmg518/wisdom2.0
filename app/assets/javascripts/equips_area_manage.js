$(function(){

     //添加车间区域
    $('body').delegate('#addModal #addworkshop','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('regionnameadd',data);
                $('#addModal .regionName').html(result);
        		
                // $('#addModal').modal('hide');

            },
            error:function(err){
                console.log(err)
            }
        });
    })
    //修改车间区域
    $('body').delegate('#editModal .ShopName','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('regionnameadd',data);
               
        			$('#editModal .editregionName').html(result);
                // $('#addModal').modal('hide');

            },
            error:function(err){
                console.log(err)
            }
        });
    })
    ///设备
    $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            success:function(data){
               
                var result = template('addequipsname',data);
                $('#addModal .addequips').html(result);
               
                // $('#addModal').modal('hide');

            },
            error:function(err){
                console.log(err)
            }
        });
   
     //查询接口
   
       $('body').delegate(".search", 'click', function(){
   		$('#equips_area_manage .public_search .searchDevice').val($('.public_search #workshop option:selected').val())
		var searchDevice=$('.public_search .searchDevice').val();
        $.ajax({
            type: "get",
            url: "/equips_area_manage.json",
            data: {
                region_id: $('.navMain .searchDevice').val(),
            },
            beforeSend: function () {
                $('.loading').show();
            },
            success:function(data){
                $('.loading').hide()
                data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, getOriginalData);
		   		$('.page_total small').text(data.list_size);
            },
            complete: function () {
                $('.loading').hide();
            },
            error:function(err){
                console.log(err);
                alert("获取数据有误")
                $('.loading').hide();
            }
        })
    })
       
       //添加提交
    $('body').delegate('#equips_area_manage #addModal .btn-sure', 'click', function(){
       var region_id=$('#addModal #addworkshop option:selected').val()
       var area_id=$('#addModal .regionName option:selected').val()
       var equip_ids=[]
        for (var i = 0; i < $("#addModal .addequips input[type='checkbox']").length; i++) {

            if ($(".checkYN").eq(i).is(':checked')) {
                var data = $(".checkYN").eq(i).val();
                equip_ids.push(data);
            }
        }
        
        $.ajax({
        	type:"post",
        	url:"/equips_area_manage",
        	data:{
                region_id: region_id,
                area_id: area_id,
                equip_ids:equip_ids
            },
            success:function(data){
            	alert("添加成功")
               page_state = "change";
					getOriginalData()
                $('#addModal').modal('hide');
               
            },
            error:function(err){
                console.log(err)
            }
        });
    })
//  修改获取数据
     var areaId;
     var regionId;
     var code;
    $('body').delegate('.revise', 'click', function () {
    	
    	$('#editModal .form-group input').val("");
        areaId = $(this).parents('tr').find('.area_id').text();
        regionId = $(this).parents('tr').find('.region_id').text();

         $.ajax({
            type: "get",
            url: "/equips_area_manage/area_edit",
            data:{
            	region_id:regionId,
            	area_id:areaId
            },
            success: function (data) {
            	
							console.log(data)
							$("#editModal .workName").val(data.region_name)
							$("#editModal .areaName").val(data.area_name)
							$("#editModal .workId").val(regionId)
							$("#editModal .areaId").val(areaId)
							var data1=data
							//获取设备开始
							$.ajax({
			            type:"get",
			            url:"/equips_area_manage/get_areas",
			            success:function(data){
			                var result2 = template('editequipsname',data);
			                $('#editModal .editequips').html(result2);
			                //选中checkbox
			                $.each(data1.equips, function(index,element) {
												code=element.equip_code;
												$('#editModal .editequips li input[type="checkbox"]').each(function(){
												
														if($(this).attr('value1')==code){
												
															$(this).attr("checked","checked");
															return;
			
														}
													})
											});
			            },
			            error:function(err){
			                console.log(err)
			            }
			        });
							//获取设备结束
            },
            error: function (err) {
                console.log(err);
            }
        });
    })
//修改提交
 $('body').delegate('#equips_area_manage #editModal .btn-sure', 'click', function(){
 			var region_id=$('#editModal .workId').val()
       var area_id=$('#editModal .areaId').val()
       var equip_ids=[]
        for (var i = 0; i < $("#editModal .editequips input[type='checkbox']").length; i++) {

            if ($(".checkYN").eq(i).is(':checked')) {
                var data = $(".checkYN").eq(i).val();
                equip_ids.push(data);
            }
        }
        $.ajax({
        	type:"post",
        	url:"/equips_area_manage",
        	data:{
                region_id: region_id,
                area_id: area_id,
                equip_ids:equip_ids
            },
            success:function(data){
            	alert("添加成功")
               page_state = "change";
					getOriginalData()
                $('#editModal').modal('hide');
               
            },
            error:function(err){
                console.log(err)
            }
        });
 })
    
	//	分页+初始数据
	function getOriginalData(param){
//		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
	    	var htmlStr = template('equips_area',data_hash);
			$('#public_table table').html(htmlStr)
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: "/equips_area_manage.json?page="+pp+ '&per=' +per,
		    	type: 'get',
		    	 data: {
		                region_id: $('.navMain .searchDevice').val(),
		            },
		    	success:function(data){
		    		console.log(data)
		    		$('.loading').hide()
		    		
		    		var htmlStr = template('equips_area',data);
					$('#public_table table').html(htmlStr)
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
					
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/equips_area_manage.json').done(function(data){
		var total = data.list_size;
		data_hash = data;
	    $("#page").initPage(total, 1, getOriginalData);
	    $('.page_total span small').text(total)
	})

     //每页显示更改
    $("#pagesize").change(function(){
    	$('.loading').show()
    	var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/equips_area_manage.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                 $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, getOriginalData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })

})
