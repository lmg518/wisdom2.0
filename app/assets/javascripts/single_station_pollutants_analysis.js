$(function(){
//	站点
    var placeId = ''
	$('.place_leval').change(function(){
		placeId = $(this).val()
		
	})
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	$('.place_leval').click(function(){
		$('.control_station').val('')
	})
	
	$('.public_search .control_station').click(function(){
		if(placeId == ''){
			$('#search-station').hide()
			alert('请先选择区域级别')
	    }else{
	    	$('#search-station').show()
	    	$.ajax({
			type:'get',
			url:'/search_station_bindings.json',
			data:{
				region_level:placeId
			},
			success:function(data){
				var bindTemp = template('editStation',data);
		        $('#search-station .choose-site').html(bindTemp);
		        $('#search-station .choose-site').show();
			},
			error:function(err){
				console.log(err)
			}
		})
	    }
		
		
	})
	
	$(document).delegate('.public_search .control_station','blur',function(){
			var control_stationName=$('.public_search .control_station').val();
			if(control_stationName==""){
				$('.control_stationID').val("");
			}else{
				$('.control_stationID').val();
			}
	})
	$('.loading').show()
	$.ajax({
		type:'get',
		url:'/single_station_pollutants_analysis.json',
		success:function(data){
			$('.loading').hide()
			var dataHtml = template('place_leval',data)
			$('.place_leval').html(dataHtml)
		},
		error:function(err){
			console.log(err)
		}
	})
	
	var station_id='';
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val();
		}
		if($('.public_search .control_stationID').val()==""){
			station_id="";
		}
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
	}
	var stationName,station
	var datas,datasSo2,so2DataVal,so2DataTime;
	var so2ValArr = []
	var so2TimeArr = []
	var datasCo,coDataVal,coDataTime;
	var coValArr = []
	var coTimeArr = []
	var datasNo2,no2DataVal,no2DataTime;
	var no2ValArr = []
	var no2TimeArr = []
	var datasO3,o3DataVal,o3DataTime;
	var o3ValArr = []
	var o3TimeArr = []
	var datasPm25,pm25DataVal,pm25DataTime;
	var pm25ValArr = []
	var pm25TimeArr = []
	var datasPm10,pm10DataVal,pm10DataTime;
	var pm10ValArr = []
	var pm10TimeArr = []
	$('.public_search .search').click(function(){
		$('.choose-site').hide()
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/single_station_pollutants_analysis.json',
			data:{
				search:{
					station_ids:station_id,
					data_times:data_times
				}
			},
			success:function(data){
				
				$('.loading').hide()
				station = data.station
				stationName = station.station_name
				datas = data.station_data
				console.log(datas)
				datasSo2 = datas.so2
				datasSo2.map(function(item,index,arr){
					so2DataVal = item.val
					so2DataTime = item.data_time
					so2ValArr.push(so2DataVal)
					so2TimeArr.push(so2DataTime.substring(0,16))
				})
		   		datasCo = datas.co
				datasCo.map(function(item,index,arr){
					coDataVal = item.val
					coDataTime = item.data_time
					coValArr.push(coDataVal)
					coTimeArr.push(coDataTime)
				})
		   		datasNo2 = datas.no2
				datasNo2.map(function(item,index,arr){
					no2DataVal = item.val
					no2DataTime = item.data_time
					no2ValArr.push(no2DataVal)
					no2TimeArr.push(no2DataTime)
				})
				
				datasO3 = datas.o3
				datasO3.map(function(item,index,arr){
					o3DataVal = item.val
					o3DataTime = item.data_time
					o3ValArr.push(o3DataVal)
					o3TimeArr.push(o3DataTime)
				})
				
				datasPm25 = datas.pm2_5
				datasPm25.map(function(item,index,arr){
					pm25DataVal = item.val
					pm25DataTime = item.data_time
					pm25ValArr.push(pm25DataVal)
					pm25TimeArr.push(pm25DataTime)
				})
				
				datasPm10 = datas.pm_10
				datasPm10.map(function(item,index,arr){
					pm10DataVal = item.val
					pm10DataTime = item.data_time
					pm10ValArr.push(pm10DataVal)
					pm10TimeArr.push(pm10DataTime)
				})
				
				var myChart = echarts.init(document.getElementById('line'));	
				option = {
					title: {
				        text: stationName
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:['SO2','NO2','CO','O3','PM10','PM2.5']
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
				        feature: {
				            saveAsImage: {}
				        }
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data: so2TimeArr
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series: [
				        {
				            name:'SO2',
				            type:'line',
				            stack: '总量',
				            data:so2ValArr
				        },
				        {
				            name:'NO2',
				            type:'line',
				            stack: '总量',
				            data:no2ValArr
				        },
				        {
				            name:'CO',
				            type:'line',
				            stack: '总量',
				            data:coValArr
				        },
				        {
				            name:'O3',
				            type:'line',
				            stack: '总量',
				            data:o3ValArr
				        },
				        {
				            name:'PM10',
				            type:'line',
				            stack: '总量',
				            data:pm10ValArr
				        },
				        {
				            name:'PM2.5',
				            type:'line',
				            stack: '总量',
				            data:pm25ValArr
				        }
				    ]
				};
				myChart.setOption(option);
				
				
				
			},
			error:function(err){
				console.log(err)
			}
		})
	})

	
	
})
