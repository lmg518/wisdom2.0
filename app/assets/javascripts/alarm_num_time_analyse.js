$(function(){
//	报警级别
    function plicBox(){
	//  报警级别
	    $.getJSON('/s_alarm_levels.json').done(function(data){
	    	var htmlStr=template('notice_leval',data);
	    	$('#alarm_num_time_analyse .public_search .notice_leval').html(htmlStr)
	    })
    }
	plicBox();
	
	var placeId = ''
	$('.place_leval').change(function(){
		placeId = $(this).val()
		if(placeId == '5'){
			$('.control_stationPort').prop('disabled',true)
		}else{
			$('.control_stationPort').prop('disabled',false)
		}
	})
	
	$('.place_leval').click(function(){
		$('.control_station').val('')
	})
	
//	$('.public_search .control_stationPort').click(function(){
//  	$.ajax({
//			type:'get',
//			url:'/search_station_bindings.json',
//			
//			success:function(data){
//				var bindTemp = template('editStation',data);
//		        $('#search-station .choose-site').html(bindTemp);
//		        $('#search-station .choose-site').show();
//			},
//			error:function(err){
//				console.log(err)
//			}
//	    })
//	  
//	})
//	
//	$(document).delegate('.public_search .control_station','blur',function(){
//			var control_stationName=$('.public_search .control_station').val();
//			if(control_stationName==""){
//				$('.control_stationID').val("");
//			}else{
//				$('.control_stationID').val();
//			}
//	})
//	
//	
//	查询	
    
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	var alarm_level= '';
	var grading_analysi = ''
	function pageName_Data(){
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
		alarm_level = $('.public_search .notice_leval option:selected').val();
		grading_analysi = $('.public_search .notice_levals option:selected').val();
	}
	var alarm_count = ''
	var alarmNameArr = []
	var alarmArr = []
	var items = ''
	var alarmName = ''
	var max = ''
	var alarmTotal = ''
	var dAlarms = ''
	$('.public_search .search').click(function(){
		alarm_count = ''
		alarmNameArr = []
		alarmArr = []
		items = ''
		alarmName = ''
		max = ''
		alarmTotal = ''
		dAlarms = ''
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/alarm_num_time_analyse.json',
			data:{
				search:{
					alarm_levels:alarm_level,
					data_times:data_times,
					grading_analysis:grading_analysi
				}
			},
			success:function(data){
				alarms_arr = data
				data_hash = data
				alarmTotal = data.aram_total
				dAlarms = data.arams
				dAlarms.map(function(item,index,arr){
					items = item
					alarm_count = items.grading_num
					alarmName = items.grading_time
					alarmNameArr.push(alarmName)
					alarmArr.push({value:alarm_count,name:alarmName})
				})
		   		
				$('.loading').hide();
                var originData = template('analyseTab',data)
		        $('.public_table .exception-list').html(originData)	
		        
		        
		        
		        
		        
		        
		        
		        
		    	/////////////////////////////////////////////////////////////////////////////
		    	
		    	
		    	var myChart = echarts.init(document.getElementById('scal'));
				option = {
				    tooltip: {
				        trigger: 'item',
				        formatter: "{a} <br/>{b}: {c} ({d}%)"
				    },
				    legend: {
				        orient: 'vertical',
				        x: 'left',
				        data:alarmNameArr
				    },
				    series: [
				       
				        {
				            name:'访问来源',
				            type:'pie',
				            radius: ['40%', '55%'],
				
				            data:alarmArr
				        }
				    ]
				};
                myChart.setOption(option);	
		    	
		    	
		    	
		    	
		    	
		    	//////////////////////////////////////////////////////////////////////////
			},
			error:function(err){
				console.log(err)
			}
		})
	})

	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/alarm_num_time_analyse.json',
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var dataHtml = template('place_leval',data)
			$('.place_leval').html(dataHtml)
	        var originData = template('analyseTab',data)
		    $('.public_table .exception-list').html(originData)
		},
		error:function(err){
			console.log(err)
		}
	})
	
	
//	导出表格
	
	$('.putTable').click(function(){
    	$(this).attr('disabled',true)
		pageName_Data();
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("method","get");
		form.attr("action","/alarm_num_time_analyse/export_file.csv");
		var input1=$("<input>");
		input1.attr("type","hidden");
		input1.attr("name","data_times");
		input1.attr("value",data_times);
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","alarm_levels");
		input2.attr("value",alarm_level);
		var input3=$("<input>");
		input3.attr("type","hidden");
		input3.attr("name","grading_analysis");
		input3.attr("value",grading_analysi);
		$("body").append(form);//将表单放置在web中
		form.append(input1);
		form.append(input2);
		form.append(input3);
		form.submit();//表单提交 
    })
    
    
    
})
