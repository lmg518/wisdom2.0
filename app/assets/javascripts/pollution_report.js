$(function(){
		//获取当前时间
	var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates;
    console.log(datestamp)
 
    //  首页获取数据

$('#pollution_report #oneWeater').click(function(){
	$('.loading').show()
	$(this).css({ "color": "#fff", "background": "#00923f" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	 once_water_report()  
})
//一次水
   once_water_report();
   function once_water_report(){
   	$.ajax({
   	type:"get",
   	url:"/pollution_report/once_water_report",
    success:function(data){
	   	$('#pollution_report #public_table .excelTable').html(data)
	   	   $('#pollution_report #public_table .time').html(datestamp)
	   	   $("#one-water-table").rowspan(0);//合并单元格
	 $('.loading').hide()
	   	   //暂存
	   	   $('body').delegate('#one_water_report #save','click',function(){
			var status='N'
				submit(status)
			})
	   	   var statusC=$('#pollution_report #one_water_report .state').html();
	   	   if(statusC=="已上报"){
	   	   	$('#pollution_report  #one_water_report .state').css("color","#00923f")

			$('#one_water_report  .table_button').hide();
	   	   }
	   },
	   error:function(err){
    		console.log(err)
    	}
   }); 
   }
// 一次水上报
   $('body').delegate('#one_water_report #report','click',function(){
   	$('.loading').show()
		var status='Y'
		submit(status)
   })
   //排污暂存
   $('body').delegate('#poll_report #save','click',function(){
   	$('.loading').show()
	var status='N'
		pollutionSubmit(status)
	
	})
   $('body').delegate('#poll_report #report','click',function(){
   	$('.loading').show()
		var status='Y'
		pollutionSubmit(status)
	
   })
   
   $('#pollution_report #pollutionReport').click(function(){
   	$('.loading').show()
   	$(this).css({ "color": "#fff", "background": "#00923f" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
  		poll_report()
   })
   function poll_report(){
   	 	   $.ajax({
		   	type:"get",
		   	url:"/pollution_report/pollution_report",
		    success:function(data){
		    	$('#pollution_report #public_table .excelTable').html('')
			   	$('#pollution_report #public_table .excelTable').html(data)
			   	   $('#pollution_report #public_table .time').html(datestamp)
			   	   $("#poll-table").rowspan(0);//合并单元格
			   	   $("#poll-table").rowspan(1);//合并单元格
			 		$('.loading').hide()
			   	   
			   	   var status=$('#pollution_report #poll_report .state').html();
			   	   console.log(status)
			   	   if(status=="已上报"){
			   	   	
			   	   	$('#pollution_report  #poll_report .state').css("color","#00923f")
		
					$('#poll_report  .table_button').hide();
			   	   }
			   },
			   error:function(err){
		    		console.log(err)
		    	}
		   }); 
   }
   
	//暂存和上报所调用的方法
   function submit(status){
   	var energy_params=[]
	var objname={}
   	var date_time=$('#pollution_report #public_table  .time').html();
   	var form_arr = $(".form-horizontal").serializeArray();
		var f_form_module_id=$('.excelTable .modalID').html()
			$.each(form_arr, function () {
				field_code=this.name;
				field_value=this.value;
				var list={
					field_code:field_code,
					field_value:field_value
				}
				energy_params.push(list)
			
				console.log(energy_params)
			
			});
			objname={energy_params,date_time:date_time,status:status,id:f_form_module_id}
			console.log(objname)
					
   		$.ajax({
			type:"POST",
			url:'/pollution_report',
			data:objname,
			success:function(data){
				console.log(data)
				alert("成功")
				$('.loading').hide()
				window.location.reload();
			},
			error:function(err){
				console.log(err)
			}
		});
   }
})

function pollutionSubmit(status){
	var energy_params=[]
	var objname1={}
   	var date_time=$('#pollution_report #public_table  .time').html();
   	var f_form_module_id=$('.excelTable .modalID').html()
   	$('#poll-table input[type="text"]').each(function(){
   		var field_code=$(this).attr('name')
   		var field_value=$(this).val();
   		var region_id=$(this).attr('alt');
   		var list={
				field_code:field_code,
				field_value:field_value,
				region_id:region_id
			}
   		energy_params.push(list)
   	})
   	objname1={energy_params,date_time:date_time,status:status,id:f_form_module_id}
   	console.log(objname1)
					
   		$.ajax({
			type:"get",
			url:'/pollution_report/report',
			data:objname1,
			success:function(data){
				console.log(data)
				alert("成功")
				$('.loading').hide()
				window.location.reload();
			},
			error:function(err){
				console.log(err)
			}
		});
   }


//合并单元格
jQuery.fn.rowspan = function(colIdx) { //封装的一个JQuery小插件
    return this.each(function(){
        var that;
        $('tr', this).each(function(row) {
            $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                if (that!=null && $(this).html() == $(that).html()) {
                    rowspan = $(that).attr("rowSpan");
                    if (rowspan == undefined) {
                    $(that).attr("rowSpan",1);
                    rowspan = $(that).attr("rowSpan"); }
                    rowspan = Number(rowspan)+1;
                    $(that).attr("rowSpan",rowspan);
                    $(this).hide();
                } else {
                 that = this;
                }
            });
        });
    });
}