$(function(){
//  //获取车间设备
    $('body').delegate('#workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/s_equips/get_equips",
            data:{
                region_ids:$(this).val()
            },
            success:function(data){
                var result = template('devicename',data);
                $('#deviceName').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
   //添加车间区域
    $('body').delegate('#addModal #workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('regionnameadd',data);
                $('#addModal .regionName').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
    //修改车间区域
    $('body').delegate('#editModal .ShopName','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('regionnameedit',data);
               
        			$('#editModal .regionName').html(result);
                // $('#addModal').modal('hide');

            },
            error:function(err){
                console.log(err)
            }
        });
    })
    //查询接口
    $('body').delegate('#deviceName', 'click', function(){
       if($('.navMain #workshop').val() == ''){
           alert("请先选择车间")
       }
    });
    $('body').delegate(".search", 'click', function(){
        var per=$('#pagesize option:selected').val();
        $.ajax({
            type: "get",
            url: "/s_equips.json",
            data: {
                equip_ids: $('.navMain #deviceName').val(),
                page: 1,
                per: per,
            },
            beforeSend: function () {
                $('#loading').show();
            },
            success:function(data){
                $('#loading').hide()
                var html = template('s_equips',data);
                $('#public_table table').html(html);

                var total = data.equips_list;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', searchPage);
                var roles=data.role_code
                    Roles(roles)
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err);
                alert("获取数据有误")
                $('#loading').hide();
            }
        })
    })
    //增加表单验证
    $(document).ready(function() {
	    $('#add_station_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	        	match: {
	               
	                validators: {
	                    notEmpty: {
	                        message: '位号不能为空'
	                    }
	                }
	           },
	            AssetNumber: {
	                validators: {
	                    notEmpty: {
	                        message: '固定资产编号不能为空'
	                    }
	                }
	            },
	            
	            deviceName: {
	                validators: {
	                    notEmpty: {
	                        message: '设备名称不能为空'
	                    }
	                }
	            },
	            location: {
	                validators: {
	                    notEmpty: {
	                        message: '安装位置不能为空'
	                    }
	                }
	            },
	            UnitType: {
	                validators: {
	                    notEmpty: {
	                        message: '设备型号不能为空'
	                    }
	                }
	            },
	            UnitPower: {
	                validators: {
	                    notEmpty: {
	                        message: '设备工作能力不能为空'
	                    }
	                }
	            },
	            MainTexture: {
	                validators: {
	                    notEmpty: {
	                        message: '主要材质不能为空'
	                    }
	                }
	            },
	            Number: {
	                validators: {
	                    notEmpty: {
	                        message: '数量不能为空'
	                    }
	                }
	            },
	            factory: {
	                validators: {
	                    notEmpty: {
	                        message: '厂家不能为空'
	                    }
	                }
	            },
	            FactoryNumber: {
	                validators: {
	                    notEmpty: {
	                        message: '出厂编号不能为空'
	                    }
	                }
	            },
	            DateOfProduction: {
	                validators: {
	                    notEmpty: {
	                        message: '出厂日期不能为空'
	                    }
	                }
	            },
	            workshop: {
	                validators: {
	                    notEmpty: {
	                        message: '车间名称不能为空'
	                    },
	                    callback: {
                        message: '必须选择一个车间',
	                        callback: function(value, validator) {
	 
	                             if (value == '') {
	                                 return false;
	                             } else {
	                                 return true;
	                             }
	 
	                        }
	                    }
	                    
	                }
	            },

	        }
	    });
	});
    
    // 添加台账
    function add_create(){
    	var addValidator = $('#add_station_form').data('bootstrapValidator');
		addValidator.validate();
        var data = {
            s_region_code_id: $('#addModal .ShopName').val(),
            region_name: $('#addModal .ShopName').find("option:selected").text(),
            bit_code: $('#addModal .bitNumber input').val(),
            equip_code: $('#addModal .FixedAssetNumber input').val(),
            equip_name: $('#addModal .Name input').val(),
            equip_location: $('#addModal .InstallationSite input').val(),
            equip_norm: $('#addModal .UnitType input').val(),
            equip_nature: $('#addModal .UnitPower input').val(),
            equip_material: $('#addModal .MainTexture input').val(),
            equip_num: $('#addModal .Number input').val(),
            factory: $('#addModal .factory input').val(),
            apper_code: $('#addModal .FactoryNumber input').val(),
            apper_time: $('#addModal .DateOfProduction #datetime').val(),
            equip_status: $('#addModal #deviceState').val(),
            equip_note: $('#addModal .equip_note textarea').val(),
            s_area_id:$('#addModal .regionName option:selected').val()
        };
        if (addValidator.isValid()){
        	$.ajax({
	            type:'post',
	            url:'/s_equips',
	            data:{
	               s_equips: data,
	            },
	            success:function(data){
	                if(data.status == 200){
	                    alert("添加成功");
	                }else{
	                    alert("添加失败")
	                }
	                origin();
	                $('#addModal').modal('hide');
	                $('#add_station_form').data('bootstrapValidator').resetForm(true);
	            },
	            error:function(err){
	                alert("添加失败")
	                console.log(err)
	            }
	        })
        }
        
    }
    //添加缓存
    $('body').delegate('#add', 'click', function(){
        $('#addModal').find('input,textarea').val('');
        $('#addModal').find('.match_vlide').addClass('hide');
        $('#addModal').find('.match_vlide').removeClass('control-default');
        $('#addModal').find('.match_vlide').removeClass('control-error');
        $('#addModal').find('.match_vlide').removeClass('control-success');
    })
    //添加提交
    $('body').delegate('#addModal .btn-sure', 'click', function(){
        add_create();
    })

    // 修改提交
    var editID;
    $('body').delegate('.revise', 'click', function () {
        $('#editModal .form-group input').val("");
        editID = $(this).parents('tr').find('.id').text();
        $.ajax({
            type: "get",
            url: "/s_equips/" + editID + "/edit.json",
            success: function (data) {
                $('#editModal .ShopName').val(data.equip.s_region_code_id);
                $('#editModal .ShopName').find("option:selected").text(data.equip.region_name);//错误
                $('#editModal .bitNumber input').val(data.equip.bit_code);
                $('#editModal .FixedAssetNumber input').val(data.equip.equip_code);
                $('#editModal .Name input').val(data.equip.equip_name);
                $('#editModal .InstallationSite input').val(data.equip.equip_location);
                $('#editModal .UnitType input').val(data.equip.equip_norm);
                $('#editModal .UnitPower input').val(data.equip.equip_nature);
                $('#editModal .MainTexture input').val(data.equip.equip_material);
                $('#editModal .Number input').val(data.equip.equip_num);
                $('#editModal .FactoryNumber input').val(data.equip.apper_code);
                $('#editModal .DateOfProduction #datetime').val(data.equip.apper_time);
                $('#editModal #deviceState').val(data.equip.equip_status)
                $('#editModal .factory input').val(data.equip.factory)
                $('#editModal .equip_note textarea').val(data.equip.equip_note)
                //选中车间区域
                region_code=data.equip.s_region_code_id
				areaName=data.equip.area_name
				
				$.ajax({
		            type:"get",
		            url:"/equips_area_manage/get_areas",
		            data:{
		                region_id:region_code
		            },
		            success:function(data){
		                var result = template('regionnameedit',data);
        				$('#editModal .regionName').html(result);
		                var area_name = $("#editModal .regionName").find("option"); //获取select下拉框的所有值
						for (var j = 1; j < area_name.length; j++) {
							if ($(area_name[j]).text() ==areaName ) {
									$(area_name[j]).attr("selected", "selected");
								}
						}
		            },
		            error:function(err){
		                console.log(err)
		            }
		        });
            },
            error: function (err) {
                console.log(err);
            }
        });
    })
    //修改方法
    function add_update(){
        var data = {
            s_region_code_id: $('#editModal .ShopName').val(),
            region_name: $('#editModal .ShopName').find("option:selected").text(),
            bit_code: $('#editModal .bitNumber input').val(),
            equip_code: $('#editModal .FixedAssetNumber input').val(),
            equip_name: $('#editModal .Name input').val(),
            equip_location: $('#editModal .InstallationSite input').val(),
            equip_norm: $('#editModal .UnitType input').val(),
            equip_nature: $('#editModal .UnitPower input').val(),
            equip_material: $('#editModal .MainTexture input').val(),
            equip_num: $('#editModal .Number input').val(),
            apper_code: $('#editModal .FactoryNumber input').val(),
            apper_time: $('#editModal .DateOfProduction #datetime').val(),
            equip_status: $('#editModal #deviceState').val(),
            factory: $('#editModal .factory input').val(),
            equip_note: $('#editModal .equip_note textarea').val(),
            s_area_id:$('#editModal .regionName option:selected').val()
        };
        $.ajax({
            type:'put',
            url:'/s_equips/' + editID,
            data:{
                s_equips: data,
            },
            success:function(data){
               
                if(data.status == 200){
                    alert("修改成功");
                }else{
                    alert("修改失败")
                }
                origin();
                $('#editModal').hide();
            },
            error:function(err){
                alert("修改失败")
                console.log(err)
            }
        })
    }
    $('body').delegate('#editModal .btn-sure', 'click', function(){
        add_update();
    })
//	删除

var id;
var name;
$("body").delegate("table tbody .delete","click",function() {
	id=Number($(this).parents('tr').find('.id').text())
	name=$(this).parents('tr').find('.equip_name').text()
	$('#sureCannel').show();
	var sure_str = '<div class="point-out">确认删除 <span>"' + name + '"</span> 吗？</div>';
    $('#sureCannel .sureCannel-body').html(sure_str);

    var sureCannelFoot_str = '';
   
    sureCannelFoot_str += '<button class="btn-sure" id="equips_delete">确认</button><button onclick="resetPswd_cannel()" class="btn-cannel">取消</button>'

    $('#sureCannel .box-footer').html(sureCannelFoot_str);
})
$("body").delegate("#equips_delete","click",function() {
	$('#sureCannel').hide();
	$.ajax({
			type:"delete",
			url:'/s_equips/'+id+'.json',
			success:function(data){
				
				 $("#success").show().delay(3000).hide(300);
	            var successFoot_str = '';
	            successFoot_str += '<div class="resetPs1"><span class="resetPs1-span">" ' + name + ' "</span>，删除成功！</div>'
	
	            $('.success-footer').html(successFoot_str)
				origin();
			
			},
			error:function(err){
				console.log(err)
			}
		})

})
	
    //每页显示更改
    $("#pagesize").change(function(){
        $('#loading').show()
        var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/s_equips.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            data: {
                equip_ids: $('.navMain #deviceName').val(),
            },
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('#loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
                var roles=data.role_code
                    Roles(roles)
            },
            error:function(err){
                console.log(err)
            }
        })
    })


    //	   初始数据
    function origin(){
        var per=$('#pagesize option:selected').val();
        $.ajax({
            url: '/s_equips.json?page=1&per=' +per,
            type: 'get',
            beforeSend: function () {
                $('#loading').show();
            },
            success:function(data){
                $('#loading').hide()
                var html = template('s_equips',data);
                $('#public_table table').html(html);

                var total = data.equips_list;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', Page);
                var roles=data.role_code
                    Roles(roles)
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err);
                alert("获取数据有误")
                $('#loading').hide();
            }
        })
    }
    origin();

    //查询页
    var page_state = "init";
    function searchPage(page) {
    	
    	 var per=$('#pagesize option:selected').val();
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;

            var urlStr = "/s_equips.json?";
            $.ajax({
                type: 'get',
                url: urlStr,
                data: {
                    equip_ids: $('.navMain #deviceName').val(),
                    page: page,
                    per: per,
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('s_equips',data);
                    $('#public_table table').html(html);

                    //分页
                    $('.page_total span a').text(page);
                    $('#loading').hide();
                    var roles=data.role_code
                    Roles(roles)

                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert("请求数据错误！！！")
                }
            })
        }
    }
    function Page(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;
            $.ajax({
                async: true,
                type: 'get',
                dataType: "json",
                url: '/s_equips.json?page=' + page + '&per=' + $('#pagesize option:selected').val(),
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('s_equips',data);
                    $('#public_table table').html(html);
                    $('.page_total span a').text(page);
                    $('#loading').hide();
                    var roles=data.role_code
                    Roles(roles)
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert('请求数据失败,请稍后重试');
                    $('#loading').hide();
                }

            })
        }
    }

//维修记录分页
    function servicePage(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;
            $.ajax({
                async: true,
                type: 'get',
                dataType: "json",
                url: '/s_equips/get_handle_equip?page=' + page + '&per=' + $('#pagesize2 option:selected').val(),
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('handle_equips',data);
                    $('#maintenanceRecord #public_table table').html(html);
                    $('#maintenanceRecord .page_total2 span a').text(page);
                    $('#loading').hide();
                    var roles=data.role_code
                    Roles(roles)
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert('请求数据失败,请稍后重试');
                    $('#loading').hide();
                }

            })
        }
    }
    var SpareData;
    // 设备
    var spareID;
    $('body').delegate('.sparePart', 'click', function(){
       var spareID = $(this).parents('tr').find('.id').html();
        window.location.href = "/s_equips/" + spareID +'?spareID='+ spareID + '&equip_code=' + $(this).parents('tr').find('.equip_code').html();
       
    });



    //维修记录
    $('body').delegate('.serviceActionLog', 'click', function(){
        $('#maintenanceRecord').removeClass('hide');
        $('#dStations').addClass('hide');

        var editID = $(this).parents('tr').find('.id').text();
        $.ajax({
            type: "get",
            // url: '/s_equips/get_handle_equip?page=' + $('#maintenanceRecord #page .pageItemActive').html() + '&per=' + $('#maintenanceRecord #pagesize option:selected').val(),
            url: '/s_equips/get_handle_equip',
            data:{
              id: editID,
            },
            async: true,
            dataType: "json",
            success: function(data) {
                var result = template('handle_equips',data);
                // console.log(result);
                $('#maintenanceRecord table').html(result);

                var total = data.handle_size;
                $('#maintenanceRecord .page_total2 span small').text(total);
                $("#page2").initPage(total, '1', servicePage);
                // $("#maintenanceRecord #page").initPage(total, '1', servicePage);
            },
            error:function(err){
                console.log(err)
            }
        })

    })
    //维修记录返回
    $('body').delegate('#maintenanceRecord #editBack', 'click', function(){
        $('#maintenanceRecord').addClass('hide');
        $('#dStations').removeClass('hide');
    })
    //查看详情返回
    $('body').delegate('#showRecord #editBack', 'click', function(){
        $('#maintenanceRecord').removeClass('hide');
        $('#showRecord').addClass('hide');
    })


    //查看详情
    $('body').delegate('#maintenanceRecord td', 'dblclick', function(){
        var watchModalID = $(this).parent().find('.id').html();
        $.ajax({
            type: "get",
            url: "/handle_equips/" + watchModalID + ".json",
            success: function (data) {
                if(data.handle_equip.status == "待下发"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(0).find('p').html('待下发');
                    $('#showRecord ul li').eq(1).find('p').html('待执行');
                    $('#showRecord ul li').eq(2).find('p').html('待维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                    $('#showRecord #MaintenanceToBePerformed').addClass('hide');
                    $('#showRecord #lookXun').addClass('hide');
                    $('#showRecord #RecordAfterMaintenance').addClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').addClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');
                }else if(data.handle_equip.status == "待执行"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').addClass('hide');
                    $('#showRecord #lookXun').addClass('hide');
                    $('#showRecord #RecordAfterMaintenance').addClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').addClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('待执行');
                    $('#showRecord ul li').eq(2).find('p').html('待维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待维修"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').addClass('hide');
                    $('#showRecord #RecordAfterMaintenance').addClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').addClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('待维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待试车"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').removeClass('hide');
                    $('#showRecord #RecordAfterMaintenance').removeClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').removeClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('已维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待审核"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').removeClass('hide');
                    $('#showRecord #RecordAfterMaintenance').removeClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').removeClass('hide');
                    $('#showRecord #TrialRunRecord').removeClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('已维修');
                    $('#showRecord ul li').eq(3).find('p').html('已试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');

                }else if(data.handle_equip.status == "完成"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').addClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').removeClass('hide');
                    $('#showRecord #RecordAfterMaintenance').removeClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').removeClass('hide');
                    $('#showRecord #TrialRunRecord').removeClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('已维修');
                    $('#showRecord ul li').eq(3).find('p').html('已试车');
                    $('#showRecord ul li').eq(4).find('p').html('已审核');

                }
                SEquipID = data.handle_equip.s_equip_id;
                var result = template('workNews2', data);
                $('#showRecord .MaintenanceIfo').html(result);




                //执行中
                $('#showRecord #prvid5 img').attr('src',data.handle_equip.img_path);
                //检修之前
                $('#showRecord #RecordBeforeMaintenance').val(data.start_note.note);
                $('#showRecord #prvid3 img').attr('src',data.start_note.img_path);
                $('#showRecord #PreServiceRemarks').val(data.start_note.overhaul_desc);
                $('#showRecord #PreTime').val(data.start_note.overhaul_time);
                //检修之后
                $('#showRecord #RecordAfter').val(data.end_note.note);
                $('#showRecord #prvid4 img').attr('src',data.end_note.img_path);
                $('#showRecord #PostOverhaulRemarks').val(data.end_note.overhaul_desc);
                $('#showRecord #AfterTime ').val(data.end_note.overhaul_time);

                //备件信息
                $('#showRecord #ReplacementParts ').val(data.spares.spare_name);
                $('#showRecord #RecordTimeAfterMaintenance').val(data.spares.change_time);

                getBoot();

                //checked默认选中
                data.equip_boots.boots.forEach(function(ele,index,arr){
                    for(var i=0; i < $('#showRecord .checkTI').length; i++){
                        if($('#showRecord .checkTI').eq(i).val() == ele.boot_check_id){
                            $('#showRecord .checkTI').eq(i).attr('checked','true')
                        }
                    }
                })

                //是否合格
                if(data.equip_boots.check_status == 'Y'){
                    $('#TestResults .hege').attr('checked',true)
                }else if(data.equip_boots.check_status == 'N'){
                    $('#TestResults .buhege').attr('checked',true);
                    $('#disqualificationResult').removeClass('hide');
                    $('#disqualificationResult textarea').val(data.equip_boots.exec_desc)
                }

                //是否整理
                if(data.end_note.sort_out == 'Y'){
                    $('#WhetherToArrange .yes').attr('checked',true)
                }else if(data.end_note.sort_out == 'N'){
                    $('#WhetherToArrange .no').attr('checked',true)
                }
            },
            error: function (err) {
                console.log(err);
            }
        })
        $('#maintenanceRecord').addClass('hide');
        $('#showRecord').removeClass('hide');

    })
    //获取检查项
    function getBoot(){
        $.ajax({
            type: "get",
            url: " /handle_equips/get_boots",
            async: false,
            success: function (data) {
                var result2 = template('TestItem2', data);
                $('#showRecord .TestItem').html(result2);
            },
            error: function (err) {
                console.log(err);
            }
        })
    }



    //车间项目验证
    $('body').delegate("#addModal .ShopName","focus",function(){
        $('#addModal #ShopName').addClass('control-default')
        $('#addModal #ShopName').removeClass('hide')

    });
    $('body').delegate("#addModal .ShopName","blur",function(){
        if($('#addModal .ShopName').val() != ''){
            $('#addModal #ShopName').removeClass('control-default')
            $('#addModal #ShopName').removeClass('control-error')
            $('#addModal #ShopName').addClass("control-success");
            $('#addModal #ShopName').html('车间名称已选择');
        }else if($('#addModal .ShopName').val() == ''){
            $('#addModal #ShopName').removeClass('control-default')
            $('#addModal #ShopName').removeClass('control-success')
            $('#addModal #ShopName').addClass("control-error");
            $('#addModal #ShopName').html("车间名称不能为空")
        }
    });
    //位号验证
    $('body').delegate("#addModal #match","focus",function(){
        $('#addModal #bitNumber').addClass('control-default')
        $('#addModal #bitNumber').removeClass('hide')

    });
    $('body').delegate("#addModal #match","blur",function(){
        if($('#addModal #match').val() != ''){
            $('#addModal #bitNumber').removeClass('control-default')
            $('#addModal #bitNumber').removeClass('control-error')
            $('#addModal #bitNumber').addClass("control-success");
            $('#addModal #bitNumber').html('位号已输入');
        }else if($('#addModal #match').val() == ''){
            $('#addModal #bitNumber').removeClass('control-default')
            $('#addModal #bitNumber').removeClass('control-success')
            $('#addModal #bitNumber').addClass("control-error");
            $('#addModal #bitNumber').html("位号不能为空")
        }
    });
    //备件描述、验证
    $('body').delegate("#addModal .FixedAssetNumber input","focus",function(){
        $('#addModal #FixedAssetNumber').addClass('control-default')
        $('#addModal #FixedAssetNumber').removeClass('hide')

    });
    $('body').delegate("#addModal .FixedAssetNumber input","blur",function(){
        if($('#addModal .FixedAssetNumber input').val() != ''){
            $('#addModal #FixedAssetNumber').removeClass('control-default')
            $('#addModal #FixedAssetNumber').removeClass('control-error')
            $('#addModal #FixedAssetNumber').addClass("control-success");
            $('#addModal #FixedAssetNumber').html('备件描述已输入');
        }else if($('#addModal .FixedAssetNumber input').val() == ''){
            $('#addModal #FixedAssetNumber').removeClass('control-default')
            $('#addModal #FixedAssetNumber').removeClass('control-success')
            $('#addModal #FixedAssetNumber').addClass("control-error");
            $('#addModal #FixedAssetNumber').html("备件描述不能为空")
        }
    });

    //设备名称验证
    $('body').delegate("#addModal .Name input","focus",function(){
        $('#addModal #Name').addClass('control-default')
        $('#addModal #Name').removeClass('hide')

    });
    $('body').delegate("#addModal .Name input","blur",function(){
        if($('#addModal .Name input').val() != ''){
            $('#addModal #Name').removeClass('control-default')
            $('#addModal #Name').removeClass('control-error')
            $('#addModal #Name').addClass("control-success");
            $('#addModal #Name').html('设备名称已输入');
        }else if($('#addModal .Name input').val() == ''){
            $('#addModal #Name').removeClass('control-default')
            $('#addModal #Name').removeClass('control-success')
            $('#addModal #Name').addClass("control-error");
            $('#addModal #Name').html("设备名称不能为空")
        }
    });

    //安装位置验证
    $('body').delegate("#addModal .InstallationSite input","focus",function(){
        $('#addModal #InstallationSite').addClass('control-default')
        $('#addModal #InstallationSite').removeClass('hide')

    });
    $('body').delegate("#addModal .InstallationSite input","blur",function(){
        if($('#addModal .InstallationSite input').val() != ''){
            $('#addModal #InstallationSite').removeClass('control-default')
            $('#addModal #InstallationSite').removeClass('control-error')
            $('#addModal #InstallationSite').addClass("control-success");
            $('#addModal #InstallationSite').html('安装位置已输入');
        }else if($('#addModal .InstallationSite input').val() == ''){
            $('#addModal #InstallationSite').removeClass('control-default')
            $('#addModal #InstallationSite').removeClass('control-success')
            $('#addModal #InstallationSite').addClass("control-error");
            $('#addModal #InstallationSite').html("安装位置不能为空")
        }
    });
    //设备型号验证
    $('body').delegate("#addModal .UnitType input","focus",function(){
        $('#addModal #UnitType').addClass('control-default')
        $('#addModal #UnitType').removeClass('hide')

    });
    $('body').delegate("#addModal .UnitType input","blur",function(){
        if($('#addModal .UnitType input').val() != ''){
            $('#addModal #UnitType').removeClass('control-default')
            $('#addModal #UnitType').removeClass('control-error')
            $('#addModal #UnitType').addClass("control-success");
            $('#addModal #UnitType').html('设备型号已输入');
        }else if($('#addModal .UnitType input').val() == ''){
            $('#addModal #UnitType').removeClass('control-default')
            $('#addModal #UnitType').removeClass('control-success')
            $('#addModal #UnitType').addClass("control-error");
            $('#addModal #UnitType').html("设备型号不能为空")
        }
    });
    //设备工作能力验证
    $('body').delegate("#addModal .UnitPower input","focus",function(){
        $('#addModal #UnitPower').addClass('control-default')
        $('#addModal #UnitPower').removeClass('hide')

    });
    $('body').delegate("#addModal .UnitPower input","blur",function(){
        if($('#addModal .UnitPower input').val() != ''){
            $('#addModal #UnitPower').removeClass('control-default')
            $('#addModal #UnitPower').removeClass('control-error')
            $('#addModal #UnitPower').addClass("control-success");
            $('#addModal #UnitPower').html('设备工作能力已输入');
        }else if($('#addModal .UnitPower input').val() == ''){
            $('#addModal #UnitPower').removeClass('control-default')
            $('#addModal #UnitPower').removeClass('control-success')
            $('#addModal #UnitPower').addClass("control-error");
            $('#addModal #UnitPower').html("设备工作能力不能为空")
        }
    });
    //主要材质验证
    $('body').delegate("#addModal .MainTexture input","focus",function(){
        $('#addModal #MainTexture').addClass('control-default')
        $('#addModal #MainTexture').removeClass('hide')

    });
    $('body').delegate("#addModal .MainTexture input","blur",function(){
        if($('#addModal .MainTexture input').val() != ''){
            $('#addModal #MainTexture').removeClass('control-default')
            $('#addModal #MainTexture').removeClass('control-error')
            $('#addModal #MainTexture').addClass("control-success");
            $('#addModal #MainTexture').html('主要材质已输入');
        }else if($('#addModal .MainTexture input').val() == ''){
            $('#addModal #MainTexture').removeClass('control-default')
            $('#addModal #MainTexture').removeClass('control-success')
            $('#addModal #MainTexture').addClass("control-error");
            $('#addModal #MainTexture').html("主要材质不能为空")
        }
    });
    //数量验证
    $('body').delegate("#addModal .Number input","focus",function(){
        $('#addModal #Number').addClass('control-default')
        $('#addModal #Number').removeClass('hide')

    });
    $('body').delegate("#addModal .Number input","blur",function(){
        if($('#addModal .Number input').val() != ''){
            $('#addModal #Number').removeClass('control-default')
            $('#addModal #Number').removeClass('control-error')
            $('#addModal #Number').addClass("control-success");
            $('#addModal #Number').html('数量已输入');
        }else if($('#addModal .Number input').val() == ''){
            $('#addModal #Number').removeClass('control-default')
            $('#addModal #Number').removeClass('control-success')
            $('#addModal #Number').addClass("control-error");
            $('#addModal #Number').html("数量不能为空")
        }
    });
    //出厂编号验证
    $('body').delegate("#addModal .FactoryNumber input","focus",function(){
        $('#addModal #FactoryNumber').addClass('control-default')
        $('#addModal #FactoryNumber').removeClass('hide')

    });
    $('body').delegate("#addModal .FactoryNumber input","blur",function(){
        if($('#addModal .FactoryNumber input').val() != ''){
            $('#addModal #FactoryNumber').removeClass('control-default')
            $('#addModal #FactoryNumber').removeClass('control-error')
            $('#addModal #FactoryNumber').addClass("control-success");
            $('#addModal #FactoryNumber').html('出厂编号已输入');
        }else if($('#addModal .FactoryNumber input').val() == ''){
            $('#addModal #FactoryNumber').removeClass('control-default')
            $('#addModal #FactoryNumber').removeClass('control-success')
            $('#addModal #FactoryNumber').addClass("control-error");
            $('#addModal #FactoryNumber').html("出厂编号不能为空")
        }
    });
    //出厂日期验证
    $('body').delegate("#addModal .DateOfProduction input","focus",function(){
        $('#addModal #time_nameTip').addClass('control-default')
        $('#addModal #time_nameTip').removeClass('hide')

    });
    $('body').delegate("#addModal .DateOfProduction input","blur",function(){
        if($('#addModal .DateOfProduction input').val() != ''){
            $('#addModal #time_nameTip').removeClass('control-default')
            $('#addModal #time_nameTip').removeClass('control-error')
            $('#addModal #time_nameTip').addClass("control-success");
            $('#addModal #time_nameTip').html('出厂日期已选择');
        }else if($('#addModal .DateOfProduction input').val() == ''){
            $('#addModal #time_nameTip').removeClass('control-default')
            $('#addModal #time_nameTip').removeClass('control-success')
            $('#addModal #time_nameTip').addClass("control-error");
            $('#addModal #time_nameTip').html("出厂日期不能为空")
        }
    });
    //出厂日期验证
    $('body').delegate("#addModal .DateOfProduction input","focus",function(){
        $('#addModal #time_nameTip').addClass('control-default')
        $('#addModal #time_nameTip').removeClass('hide')

    });
    $('body').delegate("#addModal .DateOfProduction input","blur",function(){
        if($('#addModal .DateOfProduction input').val() != ''){
            $('#addModal #time_nameTip').removeClass('control-default')
            $('#addModal #time_nameTip').removeClass('control-error')
            $('#addModal #time_nameTip').addClass("control-success");
            $('#addModal #time_nameTip').html('出厂日期已选择');
        }else if($('#addModal .DateOfProduction input').val() == ''){
            $('#addModal #time_nameTip').removeClass('control-default')
            $('#addModal #time_nameTip').removeClass('control-success')
            $('#addModal #time_nameTip').addClass("control-error");
            $('#addModal #time_nameTip').html("出厂日期不能为空")
        }
    });

    //厂家验证
    $('body').delegate("#addModal .factory input","focus",function(){
        $('#addModal #factory').addClass('control-default')
        $('#addModal #factory').removeClass('hide')

    });
    $('body').delegate("#addModal .factory input","blur",function(){
        if($('#addModal .factory input').val() != ''){
            $('#addModal #factory').removeClass('control-default')
            $('#addModal #factory').removeClass('control-error')
            $('#addModal #factory').addClass("control-success");
            $('#addModal #factory').html('厂家已输入');
        }else if($('#addModal .factory input').val() == ''){
            $('#addModal #factory').removeClass('control-default')
            $('#addModal #factory').removeClass('control-success')
            $('#addModal #factory').addClass("control-error");
            $('#addModal #factory').html("厂家不能为空")
        }
    });
    //备注验证
    $('body').delegate("#addModal .equip_note textarea","focus",function(){
        $('#addModal #equip_note').addClass('control-default')
        $('#addModal #equip_note').removeClass('hide')

    });
    $('body').delegate("#addModal .equip_note textarea","blur",function(){
        if($('#addModal .equip_note textarea').val() != ''){
            $('#addModal #equip_note').removeClass('control-default')
            $('#addModal #equip_note').removeClass('control-error')
            $('#addModal #equip_note').addClass("control-success");
            $('#addModal #equip_note').html('备注已输入');
        }else if($('#addModal .equip_note textarea').val() == ''){
            $('#addModal #equip_note').removeClass('control-default')
            $('#addModal #equip_note').removeClass('control-success')
            $('#addModal #equip_note').addClass("control-error");
            $('#addModal #equip_note').html("备注不能为空")
        }
    });
    // }
})
//根据角色显示和隐藏新增
function Roles(roles){
	if(roles=="mechanic_repair"){
		$('#dStations .public_search #add').hide()
		$('#dStations #public_table table td .revise').hide()
	}
	if(roles=="division_level"){
		$('#dStations #public_table table td .delete').show()
	}else{
		$('#dStations #public_table table td .delete').hide()
	}
}
function  machInfoImport(){  
	var formData = new FormData(document.getElementById("excelImportForm"));

        $.ajax({  
      type:'POST',  
      url:'/s_equips/excel_import',  
      data:formData,
//    async: false,    
//        cache: false,    
          contentType: false,    
          processData: false,  //必须要  
          success: function (data) {
        
          		alert("提交成功")
          
          	$('#uploadModal').modal('hide');
          	location.reload();
 		
 				
             },    
           error: function (data) { 
           	alert("提交失败：请选择.xlsx文件")
//               console.log(JSON.stringify(data))    
            }    
      });     
    }  