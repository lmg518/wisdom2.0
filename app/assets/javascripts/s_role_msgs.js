$(function(){
//	增加权限
	$('body').delegate('#juri','click',function(){
//		$('#manage').show();
		valid();
		alertData();
	})
	$('#loading').show()
	//是否全选
	$('.right-radio #optionsRadios1').click(function(){
		$('.list li input').prop('checked',true);
	})
	$('.right-radio #optionsRadios2').click(function(){
		$('.list li input').prop('checked',false);
	})
	//二级菜单的显示和隐藏
	$('body').delegate('.che','click',function(){    

		var UL = $(this).find("ul"); 
			if(UL.css("display")=="none"){ 
				UL.css("display","block"); 
			} 
			else{ 
				UL.css("display","none"); 
			} 
			$(this).siblings().find('ul').hide();
  	})   
	$('body').delegate('.list .che p input','click',function(){
		$(this).parents('.che').find('ul li input').prop('checked',this.checked);
	})
	//添加提交
	$("#addModal #roleSubmit").click(function(){
//		$('#manage .roleSubmit').val("提交中...");
//		if($('#manage .roleSubmit').val("提交中...")){
//			$('#manage .roleSubmit').attr('disabled',true)
//		}else{
//			$('#manage .roleSubmit').attr('disabled',false)
//		}
		var name = $("input[name='rol_name']").val() ;//角色名称
		var flag;
		if($('#manage .content input[type="radio"]').prop('checked')==true){
			flag='1';
		}else{
			flag='0';
		}
		var showid1 = [];
		var hideid1 = [];
		for(var f=0;f<$("#manage .che input").length;f++ ){
			if($('#manage .che input')[f].checked == true){
				showid1.push($("#manage .che i")[f].innerHTML);
				$('#manage .showId').val(showid1);
			}else{
				hideid1.push($("#manage .che i")[f].innerHTML);
				$('#manage .hideId').val(hideid1);
			}
		}
		$.ajax({
			type:"POST",
			url:"/s_role_msgs.json",
			data:{
				s_role_msg:
				{
					role_name: name, 
					valid_flag: flag
				},
				func_code_true: showid1,
				func_code_false:hideid1
			},
			success:function(data){
				page_state = "change";
				originData();
				alert("提交成功");
				$('#addModal').modal('hide');
				$('#manage .roleSubmit').val('提交');
			},
			error: function(err){
				alert('创建失败');
				$('#manage .roleSubmit').val('提交');
            }       
		});
		$("input[name='rol_name']").val("") ;		
		$("input[name='s_role_msg[valid_flag]']").val("") ;
	});
		
	$('.content-outside h4 .close').click(function(){
		$('#manage').hide();
	})
	
	$('body').delegate('#s_role_edit .content-outside h4 .close','click',function(){
		$('#s_role_edit').hide();
	});
//	编辑
	var id;
	$('body').delegate('table .role_edit','click',function(){
		id = $(this).parents('tr').find('.id').text();
		$.getJSON('/s_role_msgs/'+ id +'.json').done(function(data){
			var dataObj={
				editData:data
			}
			var editHtml=template('editTemp',dataObj);
			$('#editModal .modal-body').html(editHtml);
//			$('#s_role_edit').show();
			valids();
			var valid_flag = data.valid_flag;
			var list1=[]; //无首页
			var list2=[]; //有首页
			if(valid_flag==1){
				$('#s_role_edit .content #Radios1').prop('checked',true);
			}else{
				$('#s_role_edit .content #Radios2').prop('checked',true);
			}
			$.ajax({
				type:"get",
				url:"/s_func_msgs.json",
				success:function(data){
					var html = template('msg',data);
					$('#s_role_edit .list2').html(html);
					$('.list1 input[type="checkbox"]').each(function(i){
						list1.push($('.list1 input[type="checkbox"]').eq(i).val())
					});
					$('.list2 input[type="checkbox"]').each(function(i){
						list2.push($('.list2 input[type="checkbox"]').eq(i).val())
					});
					list1.map(function(ele,index,arr){
						var checkedID=list2.indexOf(ele)
						$('.list2 input[type="checkbox"]').eq(checkedID).prop('checked',true)
					})
					
					
				}
			});
		})
	})
//	编辑提交
	$('body').delegate('#editSub','click',function(){
//		$('#editSub').val("提交中...");
//		if($('#editSub').val("提交中...")){
//			$('#editSub').attr('disabled',true);
//		}else{
//		$('#editSub').attr('disabled',false);
//		}
		var role_name = $('#s_role_edit #role_name_edit').val();
		var role_flag;
		
		if($('#s_role_edit .content input[type="radio"]').prop('checked')==true){
			role_flag='1';
		}else{
			role_flag='0';
		}
		var showid = [];
		var hideid = [];
		for(var f=0;f<$("#s_role_edit .list2 .che input").length;f++ ){
			if($('#s_role_edit .list2  .che input')[f].checked == true){
				showid.push($("#s_role_edit  .list2 .che i")[f].innerHTML);
				$('#s_role_edit .showId').val(showid);
			}else{
				hideid.push($("#s_role_edit .list2 .che i")[f].innerHTML);
				$('.hideId').val(hideid);
			}
		}
		$.ajax({
			type: "PUT",
			url: '/s_role_msgs/'+ id +'.json',
			dataType: "json",
			data:{
				s_role_msg:{
					role_name:role_name,
					valid_flag:role_flag
				},
				func_code_true:showid,
				func_code_false:hideid
			},
			success: function(data) {
				page_state = "change";
				alert("提交成功");
//				$('#editSub').val("提交");
				$('#editModal').modal('hide');
				originData();
			},
			error: function(err){
				$('#editSub').val("提交");
				alert('提交失败')
			}
		})
	})
	//编辑的是否全选
	$('body').delegate('.right-radio #Radios1','click',function(){
		$('.list li input').prop('checked',true);
	})
	$('body').delegate('.right-radio #Radios2','click',function(){
		$('.list li input').prop('checked',false);
	})
	//获取权限选项列表
	function alertData(){
		$.ajax({
			type:"get",
			url:"/s_func_msgs.json",
			success:function(data){
				var html = template('msg',data);
				$('.left-list').html(html);
				if($('#manage .right-radio #optionsRadios1[type="radio"].checked')){
					$('#manage .list .che input[type="checkbox"]').prop('checked',true);
				}else{
				}
			}
		});
	}
	alertData();
	
//	分页+初始数据
	function originData(param){
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('self_role',data_hash);
			$('table .role_msg').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('#loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/s_role_msgs.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	success:function(data){
		    		$('#loading').hide()
		    		var htmlData = template('self_role',data);
					$('table .role_msg').html(htmlData);
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/s_role_msgs.json').done(function(data){
		$('#loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	})
	//每页显示更改
    $("#pagesize").change(function(){
    	$('#loading').show()
    	var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/s_role_msgs.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('#loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	//	角色名称表单验证
	function valid(){
		var roleName = document.getElementById("roleName");
		var roleName_Tip = document.getElementById("roleName_Tip");
		$('body').delegate("#manage #roleName","focus",function(){
			roleName_Tip.className = "control-default";
			roleName_Tip.firstChild.nodeValue = "请输入角色名称."
		})
		$('body').delegate("#manage #roleName","blur",function(){
			if(roleName.validity.valid){
				roleName_Tip.className = "control-success";
				roleName_Tip.firstChild.nodeValue = "角色名称正确.";
				$('#addSub').attr('disabled', false)
			}else if(roleName.validity.valueMissing){
				roleName_Tip.className = "control-error";
				roleName_Tip.firstChild.nodeValue = "角色名称不能为空.";
				$('#addSub').attr('disabled', true)
			}else if(roleName.validity.patternMismatch){
				roleName_Tip.className = "control-error";
				roleName_Tip.firstChild.nodeValue = "角色名称输入有误.";
				$('#addSub').attr('disabled', true)
			}
		})
	}
	
	function valids(){
		var roleNames = document.getElementById("role_name_edit");
		var roleName_Tip = document.getElementById("roleName_Tip_edit");
		$('body').delegate("#s_role_edit #role_name_edit","focus",function(){
			roleName_Tip.className = "control-default";
			roleName_Tip.firstChild.nodeValue = "请输入角色名称."
		})
		$('body').delegate("#s_role_edit #role_name_edit","blur",function(){
			if(roleNames.validity.valid){
				roleName_Tip.className = "control-success";
				roleName_Tip.firstChild.nodeValue = "角色名称正确.";
				$('#editSub').attr('disabled',false);
			}else if(roleNames.validity.valueMissing){
				roleName_Tip.className = "control-error";
				roleName_Tip.firstChild.nodeValue = "角色名称不能为空.";
				$('#editSub').attr('disabled',true);
				
			}else if(roleNames.validity.patternMismatch){
				roleName_Tip.className = "control-error";
				roleName_Tip.firstChild.nodeValue = "角色名称输入有误.";
				$('#editSub').attr('disabled',true);
				
			}
		})
	}
	
	
})