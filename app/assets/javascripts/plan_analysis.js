$(function(){
	//运维单位
	 $.getJSON('/search_region_codes').done(function(data){
		var stationTemp = template('editUnit',data);
		$('#search-unit .choose-site').html(stationTemp);
	})
	 	 var dataTime='';
	 	 var week_rand='';
	 	 var week_flag='';
	 function pageName_Data(){
	 	dataTime=$('#ops_type option:selected').val();
	 	week_rand=$('#ops_type option:selected').text();
	 	console.log(week_rand)
	 	week_flag=$('#ops_type option:selected').attr('title');
	 	console.log(week_flag)
	 }
	 $('#plan_analysis .public_search .search').click(function(){
	 	dataTime='';
	 	week_rand='';
	 	week_flag='';
	 	var time=$('#ops_type option:selected').val();
	 	console.log(time)
	 	pageName_Data()
	 	$.ajax({
	 		type:"get",
	 		url:"/plan_analysis/search_ops.json?week_time="+time,
	 		data:{
	 			week_time:dataTime,
	 			week_rand:week_rand,
	 			week_flag:week_flag
	 		},
	 		success:function(data){
				$('.loading').hide()
	   		 	var htmlData = template('analysisTable',data);
				$('#plan_analysis table .exception-list').html(htmlData);
		   		
			},
			error:function(err){
				console.log(err)
			}
	 	});
	 })
	 		//详细——查询
	var unit_ywID = '';
	var cityName='';
	function pageName_Data2(){
		unit_ywID = $('.public_search .unit_ywID').val();
		cityName=$('#ops_audit_detail #city').val();
	}
	$('#ops_audit_detail .public_search .search').click(function(){
		unit_ywID = '';
		cityName='';
		pageName_Data2()
		var week_times=$('#ops_audit_detail #weekTime').val()
		$.ajax({
			type:"get",
			url:"/plan_analysis.json?week_time="+week_times,
			data:{
				unit_name:unit_ywID,
				zone_name:cityName
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	$('body').delegate('.lookFor','click',function(){
		$('.loading').show()
		$('#ops_audit_detail').show();
		$('#plan_analysis').hide();
		var weekTime=$(this).parents('tr').find('.week_time').text();
		var randTime=$(this).parents('tr').find('.randTime').text();
		//	分页+初始数据
		function lookData(param){
			pageName_Data2()
			var pp = $('#ops_audit_detail #page .pageItemActive').html();
			var per=$('#ops_audit_detail #pagesize option:selected').val();
			if(parseInt(pp) == 1 && page_state == "init"){
				var htmlData = template('detail_Table',data_hash);
				$('#ops_audit_detail .public_table .exception-lis').html(htmlData);
				$('#ops_audit_detail .page_total span a').text(param);
			}else{
				$('.loading').show()
				page_state = "change";
	            data_hash = {};
				$.ajax({
					url: '/plan_analysis.json?week_time='+weekTime+'&page=' + pp+ '&per=' +per,
			    	type: 'get',
			    	data:{
						unit_name:unit_ywID,
						zone_name:cityName
					},
			    	success:function(data){
			    		$('.loading').hide()
			    		var total = data.list_size;
			    		var detailHtml = template('detail_Table',data)
						$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
						var weekTime=data.week_time;
						$('#ops_audit_detail #weekTime').val(weekTime)
						$('#ops_audit_detail #startDate').text(randTime)
						$('#ops_audit_detail .page_total span a').text(pp);
						$('#ops_audit_detail .page_total span small').text(total)
			    	},
			    	error:function(err){
			    		console.log(err)	
			    	}
				})
			}
		};

	var data_hash = {};
    var page_state = "init";
		$.ajax({
			type:"get",
			url:"/plan_analysis.json?week_time="+weekTime,
			success:function(data){
				$('.loading').hide()
				var total = data.list_size;
				data_hash=data;
			    $("#ops_audit_detail #page").initPage(total, 1, lookData)
			    $('#ops_audit_detail .page_total span small').text(total)

				var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
				var weekTime=data.week_time;
				$('#ops_audit_detail #weekTime').val(weekTime)
				$('#ops_audit_detail #startDate').text(randTime)
			},
			error:function(err){
				console.log(err)
			}
		});
		
		//	//每页显示更改
	    $("#ops_audit_detail #pagesize").change(function(){
	        $.ajax({
	            type: "get",
	            url: '/plan_analysis.json?week_time='+weekTime+'page=' + $('#ops_audit_detail #page .pageItemActive').html() + '&per=' + $('#ops_audit_detail #pagesize option:selected').val(),
	            async: true,
	            dataType: "json",
	            success: function(data) {
	                data_hash = data;
	                $('.loading').hide()
	                var total = data.list_size;
	                $("#ops_audit_detail #page").initPage(total, 1, lookData)
	                $('#ops_audit_detail .page_total span small').text(total)
	            },
	            error:function(err){
	                console.log(err)
	            }
	        })
	    })
		
	})
	$('body').delegate('#detilBack','click',function(){
		$('#plan_analysis').show();
		$('#ops_audit_detail').hide();
	})
	 //时间
	$.getJSON('/plan_analysis').done(function(data){
		var selectTime = template('searchTime',data);
		$('#plan_analysis .public_search #ops_type').html(selectTime);
	})
	$.getJSON('/plan_analysis.json').done(function(data){
		$('.loading').hide()
	    var htmlData = template('analysisTable',data);
		$('#plan_analysis table .exception-list').html(htmlData);
	    
	})
		
})
