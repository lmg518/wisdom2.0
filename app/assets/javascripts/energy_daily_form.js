$(function(){
	
var time = new Date();
var year = time.getFullYear();
var month = time.getMonth() + 1;
var day = time.getDate();
var newMonths = month < 10 ? '0' + month : month;
var newDates = day < 10 ? '0' + day : day;
var stringDate 
stringDate= year + "-" + newMonths + "-" + newDates;
$('#energy_daily_form .public_search #start_date_range').val(stringDate)
	//首页数据
	$('#loading').show()
	
	 	$.ajax({
			type:"get",
			url:"/energy_daily_form/energy_index",
			data:{
				time:stringDate
			},
			success:function(data){
				
				$('#energy_daily_form #powerCutRecords #public_table .public_table').html(data)
				$('#loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
	 
	//查询
   $('#energy_daily_form .public_search .search').click(function(){
   
   	$('#loading').show()
   		var time=$('#energy_daily_form .public_search #start_date_range').val();
   		$.ajax({
	   	type:"get",
	   	url:"/energy_daily_form/energy_index",
	   	data:{
	   		time:time
	   	},
	    success:function(data){
	    		
		   	$('#energy_daily_form #powerCutRecords #public_table .public_table').html(data)
		   	  $('#loading').hide()
		   },
		   error:function(err){
		   	alert("出错啦！！")
	    		console.log(err)
	    	}
	   });
   })
	$('body').delegate('#energy_daily_form #indexTable td .lookFor','click',function(){
		$('#energy_daily_form #powerCutRecords').hide()
		$('#loading').show()
		var regionCode=$(this).parents('tr').find('.code').text();
		var dateTime=$(this).parents('tr').find('.date_time').text();
		var url='';
		if(regionCode=="Biological_electricity_form"){
			//生物用电报表
			url="/energy_daily_form/energy_form_power"
		}else if(regionCode=="Biological_once_water_from"){
			//一次水
			url="/energy_daily_form/energy_form_once_water"
		}else if(regionCode=="Biological_water_from"){
			//循环水
			url="/energy_daily_form/energy_form_water"
		}else if(regionCode=="Biological_pollution_from"){
			url="/energy_daily_form/energy_form_pollution"
		}else if(regionCode=="Biological_steam_from"){
			url="/energy_daily_form/energy_form_steam"
		}
		$.ajax({
			type:"get",
			url:url,
			data:{
				code:regionCode,
				time:dateTime
			},
			success:function(data){
				$('#energy_daily_form #lookHtml').html(data)
//				$('#energy_daily_form #water_report .time').html(dateTime)
				//一次水合计计算开始
				var fieldSum=0;
				var monthSun=0;
				$('#lookHtml #public_table .once-water .fieldValue').each(function(){
					fieldSum += parseFloat($(this).html());
					
				})
				
				$('#lookHtml #public_table .once-water .monthValue').each(function(){
					monthSun += parseFloat($(this).html());
					
				})
				$('#lookHtml #public_table .once-water .field-sum').html(fieldSum)
				$('#lookHtml #public_table .once-water .month-sum').html(monthSun)
				
				//一次水合计计算结束
				$('#loading').hide()
			},
			error:function(err){
				alert("出错啦！！")
				console.log(err)
			}
		});
	})


	
})
//时间格式转换
Date.prototype.format = function (format) {
        var args = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
            "q+": Math.floor((this.getMonth() + 3) / 3), //quarter

            "S": this.getMilliseconds()
        };
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var i in args) {
            var n = args[i];

            if (new RegExp("(" + i + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
        }
        return format;
    };  