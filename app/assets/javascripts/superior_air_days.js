$(function(){
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	var placeId = ''
	$('.place_leval').change(function(){
		placeId = $(this).val()
	})
	$('.place_leval').click(function(){
		$('.control_station').val('')
	})
	$('.public_search .control_station').click(function(){
		if(placeId == ''){
			$('#search-station').hide()
			alert('请先选择区域级别')
	    }else{
	    	$('#search-station').show()
	    	$.ajax({
			type:'get',
			url:'/search_station_bindings.json',
			data:{
				region_level:placeId
			},
			success:function(data){
				var bindTemp = template('editStation',data);
		        $('#search-station .choose-site').html(bindTemp);
		        $('#search-station .choose-site').show();
			},
			error:function(err){
				console.log(err)
			}
		})
	    }
		
		
	})
	$(document).delegate('.public_search .control_station','blur',function(){
			var control_stationName=$('.public_search .control_station').val();
			if(control_stationName==""){
				$('.control_stationID').val("");
			}else{
				$('.control_stationID').val();
			}
	})
	
	$('.place_leval').click(function(){
		$('.date').hide()
	})
	$('.some_class').click(function(){
		$('.choose-site').hide()
	})
	$('.control_station').click(function(){
		$('.date').hide()
	})
	
	
	/*日期*/
//	查询
	var station_id;
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val().split(',');
		}
		if($('.public_search .control_stationID').val()==""){
			station_id="";
		}

		start_datetime = $('.public_search #start_datetime').val()+"-01";
	}
	$('.public_search .search').click(function(){
		$('.date').hide()
		$('.choose-site').hide()
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/superior_air_days/init_index.json',
			data:{
				search:{
					data_times:start_datetime,
					station_ids:station_id
				}
			},
			success:function(data){
				data_hash = data
		   		$('.page_total small').text(data.list_size);
				$('.loading').hide();
		    	$("#page").initPage(data.list_size, 1, originData);
			},
			error:function(err){
				console.log(err)
				alert("查询站点过多")
			}
		})
	})
	var time = new Date();
	var year = time.getFullYear();
	var myMonth = time.getMonth()+1;
	var myDate = time.getDate()-1;
	$('.month_rate span').text(myMonth=='1'?'1':('1-'+myMonth));
	$('.month_day span').text(myMonth=='1'?'1':('1-'+myMonth));
	$('.this_year').text(year);
	$('.last_year').text(year-1);
	$('#start_datetime').val(year+'-'+(myMonth<10?'0'+myMonth:myMonth));
	//自定义年视图
	$('#superior_air_days .date .year_time').text(time.getFullYear())
      
        $('body').delegate('.some_class','click',function(){
        	$('#superior_air_days .date').show();
        })
           $('#superior_air_days .date table td').eq(time.getMonth()).addClass('tdselect')
        $('.calindar-left').click(function(){
			var Year = Number($('.date .year_time').text());
			Year -= 1;
			$('.date .year_time').text(Year);
			$('#superior_air_days .date table td').removeClass('tdselect');
			if (Year == year) {
	        	$('#superior_air_days .date table td').eq(time.getMonth()).addClass('tdselect')
	        }
		})
		$('.calindar-right').click(function(){
			var Year = Number($('.date .year_time').text());
			Year += 1;
        	if (Year > time.getFullYear()) {
        		Year = time.getFullYear();
        	}
			$('.date .year_time').text(Year);
			$('#superior_air_days .date table td').removeClass('tdselect')
			if (Year == year) {
	        	$('#superior_air_days .date table td').eq(time.getMonth()).addClass('tdselect')
	        }
		})
		$('#superior_air_days .date table td').each(function(i){
        	i = Number(i)+1;
        	$(this).click(function(){
        		
        		if (i == 1) {
					$('.month_day').text("1月累计达标天数")
					$('.month_rate').text("1月累计达标率")
				} else if(i > myMonth && Number($('.date .year_time').text()) == year){
					$('.month_day').text("1"+'-'+Number(myMonth)+"月累计达标天数")
					$('.month_rate').text("1"+'-'+Number(myMonth)+"月累计达标率")
				}else{
					$('.month_day').text("1"+'-'+Number(i)+"月累计达标天数")
					$('.month_rate').text("1"+'-'+Number(i)+"月累计达标率")
				}
        		if (i < 10) {
        			i = "0" + Number(i)
        		}else{
        			i = i
        		}	
        		if (i > myMonth && Number($('.date .year_time').text()) == year) {
        			if (myMonth < 10) {
        				myMonth = '0' +Number(myMonth);
        			} else{
        				myMonth = myMonth
        			}
        			$('.some_class').val($('.date .year_time').text() + '-' + myMonth);
        		} else{
        			$('.some_class').val($('.date .year_time').text() + '-' + i);
        		}
        		$('.last_year').text(Number($('.date .year_time').text()) - 1);
				$('.this_year').text($('.date .year_time').text());
        		
        		$('#superior_air_days .date').hide();
        	})
        })
		
	if((myMonth == 2 ||myMonth == 4 ||myMonth == 6 ||myMonth == 8||myMonth == 9||myMonth == 11 || myMonth == 1) && (myDate == 0)){
				myDate = 31;
				myMonth -=1;
				if(myMonth == 0){
					year -= 1;
					myMonth =12
				}
	}else if((myMonth == 5||myMonth ==7||myMonth ==10||myMonth == 12) && myDate == 0){
				myDate = 30;
				myMonth -=1;
				if(myMonth == 0){
					year -= 1;
					myMonth =12
				}
	}else if(myMonth == 3 && myDate == 0){
			if( (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)){
				myDate = 29;
			}else{
				myDate = 28;
			}
			myMonth -=1;
			if(myMonth == 0){
				year -= 1;
				myMonth =12
			}
	}else{
			myDate = time.getDate()-1
		}
			var result = year+'-'+(myMonth<10?'0'+myMonth:myMonth);
			$('.some-none').val(result)
	
	
	
	
	$(document).delegate('.public_search .control_station','blur',function(){
			var control_stationName=$('.public_search .control_station').val();
			if(control_stationName==""){
				$('.control_stationID').val("");
			}else{
				$('.control_stationID').val();
			}
	})
	
	
//	分页
		function originData(param){
//		$('.loading').show()
		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originData = template('superiorTab',data_hash)
			$('.public_table .exception-list').html(originData)
			$('.page_total span a').text(pp);
		}else{
			$('.date').hide()
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            $.ajax({
				url: '/superior_air_days/init_index.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
				data:{
					search:{
						station_ids:station_id,
						data_times:data_times
					}
				},			   
		    	success:function(data){
                    $('.loading').hide()
		    		var originData = template('superiorTab',data)
					$('.public_table .exception-list').html(originData)
					$('.page_total span a').text(pp);
		    		$('.loading').hide()
		    	
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	
		
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/superior_air_days/init_index.json',
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
			var dataHtml = template('place_leval',data)
			$('.place_leval').html(dataHtml)
	        $("#page").initPage(total, 1, originData)
	        $('.page_total span small').text(total)
		},
		error:function(err){
			console.log(err)
		}
	})
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/superior_air_days/init_index.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	
	$('.putTable').click(function(){
		$('.date').hide()
		$('#search-station').hide()
    	$(this).attr('disabled',true)
		pageName_Data();
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("method","get");
		form.attr("action","/superior_air_days/export_file.csv");
		var input1=$("<input>");
		input1.attr("type","hidden");
		input1.attr("name","station_ids");
		input1.attr("value",station_id);
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","data_times");
		input2.attr("value",data_times);
		$("body").append(form);//将表单放置在web中
		form.append(input1);
		form.append(input2);
		form.submit();//表单提交 
    })
})
