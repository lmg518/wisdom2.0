$(function(){
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})

   
//  当你点击查询
	
	var searchSta;
	var time;
	var status;
    function pageName_Data(){
        //ruleName = $('.role_name').val()
        if($('.control_stationIDs').val()){
            searchSta = $('.control_stationIDs').val().split(',')
        }
        if($('.start_datetime').val()){
            time = $('.start_datetime').val();
        }
        status = $('.public_search .status option:selected').val();
        console.log('searchSta'+searchSta+'time'+time+'status'+status);
    }
    
    var alarms_arr = {};
	var alarm_lists;
     $('.search').click(function(){	
     	$('.loading').show()
     	/*var ruleName = $('.role_name').val()
     	if($('.control_stationIDs').val()){
             staStr = $('.control_stationIDs').val().split(',')
        }else{
        	 staStr = ''
        }
     	if($('#checkId').val()){
            checkId = $('#checkId').val().split(',')
        }
     	var status = $('.public_search .status option:selected').val();*/
     	if($('.control_stationIDs').val()){
     		var searchSta=$('.control_stationIDs').val().split(',');
     	}else{
     		var searchSta='';
     	}
     	
     	var time=$('.start_datetime').val();
     	var status=$('.public_search .status option:selected').val();
		var obj = {};
		obj.station_id= searchSta;
		obj.data_time = time;
		obj.status = status
		$.ajax({
				type: "get",
				url: "/data_view/air_data_analyze.json",
				dataType: "json",
				data:obj,
				cache:false,
				success: function(data) {
					$('.loading').hide();
					alarms_arr = data;
					data_hash = data;
			        $("#page").initPage(data.list_size, 1, originData);
    		        $('.page_total small').text(data.list_size);
				},
				error:function(err){
					console.log(err)
					alert("查询站点过多")
				}
			})
		
	})
     
//   查看
    var data_time;
    var station_id;
	$('body').delegate('#airDataAnalyze .public_table .lookfor','click',function(){
		data_time = $(this).siblings('.data_time').text();
		station_id=$(this).siblings('.id').text();
		$('#air_data_analyze_detail').show();
		$('#airDataAnalyze').hide();
		$('#air_data_analyze_detail .public_table .now_time').text(data_time);
		$.ajax({
			type:'get',
			url:'/data_view/air_data_analyze/min5_datas.json',
			dataType:'json',
			cache:false,
			data:{
				data_time:data_time,
				station_id:station_id
			},
			success:function(data){
				var htmlStr=template('detail_Table',data);
				$('#air_data_analyze_detail .exception-list').html(htmlStr);
//				var htmlStr2 = template('alarm_new_list',data)
//				$('.exception-lists').html(htmlStr2)
				
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	//查看页面详情
	$('body').delegate('#air_data_analyze_detail .public_table .exception-list .look_detail','click',function(){
		$('#air_data_analyze_detail .public_table .exception-list div').hide();
		console.log('点击');
		$(this).find('div').show();
	})
	
	//详情页面点击返回
	$('body').delegate('#air_data_analyze_detail #detilBack','click',function(){
		$('#air_data_analyze_detail').hide();
		$('#airDataAnalyze').show();
	})
     
   
       
    //分页
    function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlStr = template('alarmList',data_hash);
			$('.exception-list').html(htmlStr);
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/data_view/air_data_analyze.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	cache:false,
		    	data:{		    		
						//rule_name:ruleName,
						station_id:searchSta,
						data_time:time,
						status:status					
		    	},
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlStr = template('alarmList',data);
					$('.exception-list').html(htmlStr);
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url: "/data_view/air_data_analyze.json",
		cache:false,
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
			data_hash=data;
			//var newsInfo = template('allIfo',data)
			//$('.update').html(newsInfo)
		    $("#page").initPage(total, 1, originData)
		    $('.page_total span small').text(total)
		    
		},
		error:function(err){
			console.log(err)
		}
	})
  //每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/data_view/air_data_analyze.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            cache:false,
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })  
    
    
})