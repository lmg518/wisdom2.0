$(function(){
	
	//	运维单位
	$.getJSON('/search_region_codes').done(function(data){
			var stationTemp = template('editUnit',data);
			$('#search-unit .choose-site').html(stationTemp);
		})
	
	var id;
	$('body').delegate('#statistic_place table .lookFor','click',function(){
		id=$(this).parent().siblings('.id').text();
		$('#statistic_placeLook').show()
    	$('#statistic_place').hide()
    				//	分页+初始数据
		var month=$('#statistic_place .flag').val();
		console.log(month)
		function lookData(param){
//		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('lookTable',data_hash);
			$('#statistic_placeLook table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/statistic_place/'+ id +'.json?&page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
		    		month:month
		    	},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var lookHtml = template('lookTable',data);
					$('#statistic_placeLook table .exception-list').html(lookHtml);	
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
		
		$.ajax({
			type:"get",
			url:"/statistic_place/"+ id +".json",
			data:{
		    		month:month
		    	},
			success:function(data){
				var total = data.list_size;
				data_hash=data;
			    $("#page").initPage(total, 1, lookData)
			    $('.page_total span small').text(total)
			var lookHtml = template('lookTable',data);
			$('#statistic_placeLook table .exception-list').html(lookHtml);	
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	
	$('body').delegate('#lookBack','click',function(){
		$('#statistic_placeLook').hide()
    	$('#statistic_place').show()
	})
	


	//时间
	var now = new Date(); //当前日期
var nowDayOfWeek = now.getDay(); //今天本周的第几天
var nowDay = now.getDate(); //当前日
var nowMonth = now.getMonth(); //当前月
var nowYear = now.getYear(); //当前年
nowYear += (nowYear < 2000) ? 1900 : 0; //
var lastMonthDate = new Date(); //上月日期
lastMonthDate.setDate(1);
lastMonthDate.setMonth(lastMonthDate.getMonth() - 1);
var lastYear = lastMonthDate.getYear();
var lastMonth = lastMonthDate.getMonth();
//格式化日期：yyyy-MM-dd
function formatDate(date) {
    var myyear = date.getFullYear();
    var mymonth = date.getMonth() + 1;
//  var myweekday = date.getDate();
    if (mymonth < 10) {
        mymonth = "0" + mymonth;
    }
//  if (myweekday < 10) {
//      myweekday = "0" + myweekday;
//  }
//  return (myyear + "-" + mymonth + "-" + myweekday);
    return (myyear + "-" + mymonth);
}
//获得本月的开始日期
    var monthStartDate = new Date(nowYear, nowMonth);
//获得上月开始时间
    var lastMonthStartDate = new Date(nowYear, lastMonth);

$('#public_table .monthData span').html(formatDate(monthStartDate))
$('#statistic_place .flag').val("N")
var month;
//	上月
	$('#last_month').click(function(){
		$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
		$('.loading').show()
		$('#statistic_place .flag').val("L")
		month=$(this).val();
		$('#public_table .monthData span').html(formatDate(lastMonthStartDate))
		$.ajax({
			type:"get",
			url:"/statistic_place.json",
			data:{
				month:month
			},
			success:function(data){
				data_hash2 = data
				$('.loading').hide()
	   		 	$("#page2").initPage(data.list_size, 1, originData);
		   		$('.page_total2 small').text(data.list_size);

			},
			error:function(err){
				console.log(err)
			}
			
		});
	})
	//本月
	$('#this_month').click(function(){
		$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
		$('#statistic_place .flag').val("N")
		$('.loading').show()
		month=$(this).val();
		$('#public_table .monthData span').html(formatDate(monthStartDate))
		$.ajax({
			type:"get",
			url:"/statistic_place.json",
			data:{
				month:month
			},
			success:function(data){
				data_hash2 = data
				$('.loading').hide()
	   		 	$("#page2").initPage(data.list_size, 1, originData);
		   		$('.page_total2 small').text(data.list_size);

			},
			error:function(err){
				console.log(err)
			}
			
		});
	})
	//查询
	var unit_name_id ='';
	var name ='';
	$('.public_search .search').click(function(){
		$('#statistic_place .public_search .searchUnit').val($('.public_search .unit_ywID').val())
		if($('#statistic_place .public_search .searchUnit').val()){
            unit_name_id = $('#statistic_place .public_search .searchUnit').val().split(',')
        }else{
        	unit_name_id=''
        }
        $('#statistic_place .public_search .searchRen').val($('.public_search .unit_ry').val())
        name=$('#statistic_place .public_search .searchRen').val();
        month=$('#statistic_place .flag').val()
        $.ajax({
        	type:"get",
        	url:"/statistic_place.json",
        	data:{
        		month:month,
        		name:name,
        		s_region_code_info_id:unit_name_id
        	},
        	success:function(data){
        		data_hash2 = data
				$('.loading').hide()
	   		 	$("#page2").initPage(data.list_size, 1, originData);
		   		$('.page_total2 small').text(data.list_size);
        	},
        	error:function(err){
        		console.log(err)
        	}
        });
	})
	//首页分页+初始数据
	function originData(param){

		var pp = $('#page2 .pageItemActive').html();
		var per=$('#pagesize2 option:selected').val();
		if(parseInt(pp) == 1 && page_state2 == "init"){
			var htmlData = template('placeTable',data_hash2);
			$('#statistic_place table .exception-list').html(htmlData);
			$('.page_total2 span a').text(param);
		}else{
			$('.loading').show()
			page_state2 = "change";
            data_hash2 = {};
            var unit_name_id = $('#statistic_place .public_search .searchUnit').val();
            if(unit_name_id){
            	unit_name_id = unit_name_id.split(",");
            }else{
            	unit_name_id=''
            }
			$.ajax({
				url: '/statistic_place.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
	        		month:$('#statistic_place .flag').val(),
	        		name:$('#statistic_place .public_search .searchRen').val(),
	        		s_region_code_info_id:unit_name_id
	        	},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('placeTable',data);
					$('#statistic_place table .exception-list').html(htmlData);
					$('.page_total2 span a').text(pp);
					$('.page_total2 span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash2 = {};
    var page_state2 = "init";
    month=$('#statistic_place .flag').val()
    	$('.loading').show()
    	$.ajax({
    		type:"get",
    		url:"/statistic_place.json",
    		data:{
    			month:month
    		},
    		success:function(data){
    			console.log(data)
    			$('.loading').hide()
				var total = data.list_size;
				data_hash2=data;
			    $("#page2").initPage(total, 1, originData)
			    $('.page_total2 span small').text(total)
    		},
    		error:function(err){
    			console.log(err)
    		}
    	});
	
	
})
