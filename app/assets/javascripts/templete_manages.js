$(function(){
	
	
	
	$(document).ready(function() {
	    $('#add_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            name: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	     		title: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	            content: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            }
	        }
	    });
	});
		//添加提交
	$(document).delegate('#addModal .modal-footer .btn-sure','click',function(){
		var addValidator = $('#add_form').data('bootstrapValidator');
		addValidator.validate();
		var name=$('#add_form .form-group .project_name').val();
		var title=$('#add_form .form-group .project_title').val();
		var content=$('#add_form .form-group .content').val();
		var templete_type=$('#add_form .form-group #templete_type input[type="radio"]:checked').val();
		if (addValidator.isValid()){
			$.ajax({
				type:"post",
				url:"/templete_manages.json",
				data:{
					d_templete:{
						name:name,
						title:title,
						content:content,
						templete_type:templete_type
					}
					
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#addModal').modal('hide')
   					$('#add_form').data('bootstrapValidator').resetForm(true);
				},
				error:function(err){
					console.log(err)
				}
			});
		}

	
	})
	
		//修改
	var id;
	$('body').delegate('#templete_manages table tbody tr td .edit','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"get",
			url:'/templete_manages/'+id+'.json',
			success:function(data){
				var name=data.d_templete.name;
				var title=data.d_templete.title;
				var content=data.d_templete.content;
				$('#editModal .form-group .project_name').val(name)
				$('#editModal .form-group .project_title').val(title)
				$('#editModal .form-group .content').val(content)
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	$(document).ready(function() {
	    $('#editModal').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            name: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	     		title: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            },
	            content: {
	                validators: {
	                    notEmpty: {
	                        message: '不能为空'
	                    }
	                }
	            }
	        }
	    });
	});
	$(document).delegate('#editModal .modal-footer .btn-sure','click',function(){
		var editValidator = $('#editModal').data('bootstrapValidator');
		editValidator.validate();
		var name=$('#edit_form .form-group .project_name').val();
		var title=$('#edit_form .form-group .project_title').val();
		var content=$('#edit_form .form-group .content').val();
		var templete_type=$('#add_form .form-group #templete_type input[type="radio"]:checked').val();
		if (editValidator.isValid()){
			$.ajax({
				type:"put",
				url:'/templete_manages/'+id+'.json',
				data:{
					d_templete:{
						name:name,
						title:title,
						content:content,
						templete_type:templete_type
					}
					
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#editModal').modal('hide')
				},
				error:function(err){
					console.log(err)
				}
			});
		}		
	})
		//删除
	$("body").delegate("#templete_manages table tbody tr td .delete","click",function() {
		$('.delete_prop').hide();
		$(this).parent().children('div').show();	
	})
	$('body').delegate('#templete_manages table tbody tr td .delete_prop .btn-sure','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"delete",
			url:'/templete_manages/'+id+'.json',
			success:function(data){
				page_state = "change";
				originData();
				$('.delete_prop').hide();
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#templete_manages table tbody tr td .delete_prop .btn-cancel','click',function(){
		$('.delete_prop').hide();
	})
	
		//	分页+初始数据
	function originData(param){
//		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('temp_table',data_hash);
			$('table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/templete_manages.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('temp_table',data);
					$('table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/templete_manages.json').done(function(data){
		console.log(data)
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	})

//	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/templete_manages.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})
