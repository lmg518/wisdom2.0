$(function(){
	//	站点
	$.getJSON('/search_station_bindings').done(function(data){
		var stationTemp = template('editStations',data);
		$('#search-station .choose-site').html(stationTemp);
	})
	
	
	//	查询
	var station_id;
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val().split(',');
		}
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
	}
	$('.public_search .search').click(function(){
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/concentration_analysis/init_index.json',
			data:{
				search:{
					station_ids:station_id,
					data_times:data_times,
				}
			},
			success:function(data){
				data_hash = data
		   		$('.page_total small').text(data.list_size);
				$('.loading').hide();
		    	$("#page").initPage(data.list_size, 1, originData);
			},
			error:function(err){
				console.log(err)
				alert("查询站点过多")
			}
		})
	})

	function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originData = template('concenTab',data_hash)
			$('.public_table .exception-list').html(originData)
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            $.ajax({
				url: '/concentration_analysis/init_index.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
				data:{
					search:{
						station_ids:station_id,
						data_times:data_times,
					}
				},			   
		    	success:function(data){
		    		$('.loading').hide()
		    		var originData = template('concenTab',data)
					$('.public_table .exception-list').html(originData)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	
		
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/concentration_analysis/init_index.json',
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
	        $("#page").initPage(total, 1, originData)
	        $('.page_total span small').text(total)
		},
		error:function(err){
			console.log(err)
		}
	})
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/concentration_analysis/init_index.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    $('.putTable').click(function(){
    	$(this).attr('disabled',true)
    	pageName_Data();
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("method","get");
		form.attr("action","/concentration_analysis/export_file.csv");
		var input1=$("<input>");
		input1.attr("type","hidden");
		input1.attr("name","station_ids");
		input1.attr("value",station_id);
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","data_times");
		input2.attr("value",data_times);
		$("body").append(form);//将表单放置在web中
		form.append(input1);
		form.append(input2);
		form.submit();//表单提交 
    })
})
