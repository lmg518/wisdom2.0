$(function(){
	//	运维单位
    $.getJSON('/search_region_codes').done(function(data){
		var stationTemp = template('editUnit',data);
		$('#search-unit .choose-site').html(stationTemp);
	})
	$.getJSON('/search_region_codes').done(function(data){
		var stationTemp = template('editUnits',data);
		$('#search-units .choose-sites').html(stationTemp);
	})
	//	保管人
	$.getJSON('/get_users.json').done(function(data){
		var ownerTemp = template('ownerList',data);
		$('#owner .choose-owner').html(ownerTemp);
	})
	$('body').delegate('#add','click',function(){
//		$('#suppliesEdit').hide();
		$('#accsAdd').show();
		$('#accs').hide();
	})
	
	//查询
	
	var sypply_name='';
	var supply_no='';
	var brand='';
	var unit_name_id='';
	function pageName_Data(){
		$('#accs .public_search .searchUnit').val($('.public_search .unit_ywID').val())
		if($('#accs .public_search .searchUnit').val()){
            unit_name_id = $('#accs .public_search .searchUnit').val().split(',')
        }
		$('#accs .public_search .searchName').val($('.public_search .supply_name').val());
		$('#accs .public_search .searchNo').val($('.public_search .supply_no').val());
		var searchBrand=$('.public_search .brand option:selected').val();
		$('#accs .public_search .searchBrand').val(searchBrand);
		sypply_name=$('#accs .public_search .searchName').val();
		supply_no=$('#accs .public_search .searchNo').val();
		brand=$('#accs .public_search .searchBrand').val();
	}
	$('.public_search .search').click(function(){
		sypply_name='';
		supply_no='';
		brand='';
		unit_name_id='';
		pageName_Data();
		$.ajax({
			type:"get",
			url:"/accs.json",
			data:{
				sypply_name:sypply_name,
				brand:brand,
				supply_no:supply_no,
				unit_ids:unit_name_id
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, "1", originData);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				
			}
		});
	})
	
		//城市
	$('.city_name').focus(function(){
		$('#search-units .choose-sites').hide()
		$('#owner .choose-owner').hide()
		$('#citys .choose-city').show();
	})	
	$('body').delegate('#citys .station-btn .btn-cancel','click',function(){
		$('.choose-city').find('.city_list input').prop('checked',false)
		$('#citys .choose-city').hide()
	})	
	$('body').delegate('#accsAdd #citys .station-btn .btn-sure','click',function(){
			
		$('#accsAdd #citys .choose-city').hide()
		var list = $('#accsAdd input:radio[name="city"]:checked').val();
		var listName = $('#accsAdd #citys .city_list input:radio[name="city"]:checked').siblings('span').text()
        var listId = $('#accsAdd #citys .city_list input:radio[name="city"]:checked').siblings('small').text()	
        $('#accsAdd .city_name').val(listName)
		$('#accsAdd .cityId').val(listId)	
		
	})
	$('body').delegate('#accsEdit #citys .station-btn .btn-sure','click',function(){
			
		$('#accsEdit #citys .choose-city').hide()
		var list = $('#accsEdit input:radio[name="city"]:checked').val();
		var listName = $('#accsEdit #citys .city_list input:radio[name="city"]:checked').siblings('span').text()
        var listId = $('#accsEdit #citys .city_list input:radio[name="city"]:checked').siblings('small').text()	
        $('#accsEdit .city_name').val(listName)
		$('#accsEdit .cityId').val(listId)	
		
	})
	$('.unit_yws').focus(function(){
		$('#owner .choose-owner').hide();
		$('#citys .choose-city').hide()
	})	
	//保管人
	$('.owner').focus(function(){
		$('#owner .choose-owner').show();
		$('#search-units .choose-sites').hide()
		$('#citys .choose-city').hide()
	})	
	$('body').delegate('#owner .station-btn .btn-cancel','click',function(){
		$('.choose-owner').find('li input').prop('checked',false)
		$('#owner .choose-owner').hide()
	})	
	$('body').delegate('#accsAdd #owner .station-btn .btn-sure','click',function(){
			
		$('#accsAdd #owner .choose-owner').hide()
		var ownerId = $('#accsAdd #owner input:radio[name="users"]:checked').val();
		var ownerName = $('#accsAdd #owner .choose-owner input:radio[name="users"]:checked').siblings('span').text()
//      var listId = $('#citys .city_list input:radio[name="users"]:checked').siblings('small').text()	
        $('#accsAdd .owner').val(ownerName)
		$('#accsAdd .ownerId').val(ownerId)	
		
	})
	$('body').delegate('#accsEdit #owner .station-btn .btn-sure','click',function(){
			
		$('#accsEdit #owner .choose-owner').hide()
		var ownerId = $('#accsEdit #owner input:radio[name="users"]:checked').val();
		var ownerName = $('#accsEdit #owner .choose-owner input:radio[name="users"]:checked').siblings('span').text()
//      var listId = $('#citys .city_list input:radio[name="users"]:checked').siblings('small').text()	
        $('#accsEdit .owner').val(ownerName)
		$('#accsEdit .ownerId').val(ownerId)	
		
	})

	$('body').delegate('#citys .city_top .cityName','click',function(){
//		$(this).children('.city_list').toggle();
		var UL = $(this).siblings(".city_list"); 
		if(UL.css("display")=="none"){ 
			UL.css("display","block"); 
		} 
		else{ 
			UL.css("display","none"); 
		} 
		
	})
	$('body').delegate('#accsAdd #back','click',function(){
		$('#accsAdd').hide();
		$('#accs').show();
		$('#accsEdit').hide();
		$('#accsAdd #search-units .choose-sites').hide()
		$('#accsAdd #citys .choose-city').hide()
		$('#accsAdd #owner .choose-owner').hide()
		$('#accsEdit #search-units .choose-sites').hide()
		$('#accsEdit #citys .choose-city').hide()
		$('#accsEdit #owner .choose-owner').hide()
		
	})
	$('body').delegate('#accsEdit #back','click',function(){
		$('#accsEdit #search-units .choose-sites').hide()
		$('#accsEdit #citys .choose-city').hide()
		$('#accsEdit #owner .choose-owner').hide()
		$('#accsAdd #search-units .choose-sites').hide()
		$('#accsAdd #citys .choose-city').hide()
		$('#accsAdd #owner .choose-owner').hide()
		$('#accsAdd').hide();
		$('#accs').show();
		$('#accsEdit').hide();
	})
			$(document).ready(function() {
			    $('#add_form').bootstrapValidator({
			        message: '必输入项',
			        fields: {
			            unit_yws: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            },
			            city_name: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            },
			            supply_no: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            },
			            supply_name: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            },
			            supply_num: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            },
			            supply_unit: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            },
			            validity_date: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            },
			            owner: {
			                validators: {
			                    notEmpty: {
			                        message: '不能为空'
			                    }
			                }
			            }
			        }
			    });
			});
		//添加提交
	$(document).delegate('#accsAdd #addSave','click',function(){
		var addValidator = $('#add_form').data('bootstrapValidator');
		addValidator.validate();
		
		var unit_name=$('#accsAdd #add_form .form-group .unit_yws').val();
		var unit_ywIDs=$('#accsAdd #add_form .form-group .unit_ywIDs').val();
		var brand=$('#accsAdd #add_form .form-group .brand option:selected').val();
		var supply_no=$('#accsAdd #add_form .form-group .supply_no').val();
		var supply_num=$('#accsAdd #add_form .form-group .supply_num').val();
		var supply_unit=$('#accsAdd #add_form .form-group .supply_unit').val();
		var validity_date=$('#accsAdd #add_form .form-group .validity_date').val();
		var city_name=$('#accsAdd #add_form .form-group .city_name').val();
		var cityId=$('#accsAdd #add_form .form-group .cityId').val();
		var owner=$('#accsAdd #add_form .form-group .owner').val();
		var ownerId=$('#accsAdd #add_form .form-group .ownerId').val();
		var supply_name=$('#accsAdd #add_form .form-group .supply_name').val();
		
		if (addValidator.isValid()){
			$.ajax({
				type:"post",
				url:"/accs.json",
				data:{
					acc:{
						unit_name:unit_name,
						s_region_code_info_id:unit_ywIDs,
						brand:brand,
						supply_name:supply_name,
						supply_no:supply_no,
						supply_num:supply_num,
						supply_unit:supply_unit,
						validity_date:validity_date,
						city_name:city_name,
						s_administrative_area_id:cityId,
						owner:owner,
						d_login_msg_id:ownerId
						
					}
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#accsAdd').hide();
   					$('#accs').show();
					$('#accsEdit').hide()
					$('#add_form').data('bootstrapValidator').resetForm(true);
				},
				error:function(err){
					console.log(err)
				}
			});
		}
			
		
	})
			var id;
	$('body').delegate('#accs #public_table .edit','click',function(){
		$('#accs').hide();
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"get",
			url:'/accs/'+id+'.json',
			success:function(data){
					
					var brand=data.supply.brand;
			var unit_name=data.supply.unit_name;
			var unit_ywIDs=data.supply.s_region_code_info_id;
			var supply_no=data.supply.supply_no;
			var supply_num=data.supply.supply_num;
			var supply_unit=data.supply.supply_unit;
			var validity_date=data.supply.validity_date;
			var city_name=data.supply.city_name;
			var cityId=data.supply.s_administrative_area_id;
			var owner=data.supply.owner;
			var ownerId=data.supply.d_login_msg_id;
			$('#accsEdit .form-group .unit_yws').val(unit_name);
			$('#accsEdit .form-group .unit_ywIDs').val(unit_ywIDs)
			$('#accsEdit .form-group .city_name').val(city_name);
			$('#accsEdit .form-group .cityId').val(cityId)
			$('#accsEdit .form-group .supply_no').val(supply_no);
			$('#accsEdit .form-group .supply_num').val(supply_num)
			$('#accsEdit .form-group .supply_unit').val(supply_unit);
			$('#accsEdit .form-group .validity_date').val(validity_date)
			$('#accsEdit .form-group .owner').val(owner);
			$('#accsEdit .form-group .ownerId').val(ownerId)
				var brandTemp=template('brandList',data)
				$('#accsEdit .form-group .brand').html(brandTemp);
				$("#accsEdit .brand option").each(function(){  
			        if($(this).text() == brand){  
			            $(this).attr("selected","selected");  
			        }  
			    }); 
				$('#accsEdit').show();
			},
			error:function(err){
				console.log(err)
			}
		});
	})
		$('body').delegate('#accsEdit #editSave','click',function(){
		var unit_name=$('#accsEdit #edit_form .form-group .unit_yws').val();
		var unit_ywIDs=$('#accsEdit #edit_form .form-group .unit_ywIDs').val();
		var brand=$('#accsEdit #edit_form .form-group .brand option:selected').val();
		var supply_no=$('#accsEdit #edit_form .form-group .supply_no').val();
		var supply_num=$('#accsEdit #edit_form .form-group .supply_num').val();
		var supply_unit=$('#accsEdit #edit_form .form-group .supply_unit').val();
		var validity_date=$('#accsEdit #edit_form .form-group .validity_date').val();
		var city_name=$('#accsEdit #edit_form .form-group .city_name').val();
		var cityId=$('#accsEdit #edit_form .form-group .cityId').val();
		var owner=$('#accsEdit #edit_form .form-group .owner').val();
		var ownerId=$('#accsEdit #edit_form .form-group .ownerId').val();
		var supply_name=$('#accsEdit #edit_form .form-group .supply_name').val();
		$.ajax({
			type:"put",
			url:'/accs/'+id+'.json',
			data:{
					acc:{
						unit_name:unit_name,
						s_region_code_info_id:unit_ywIDs,
						brand:brand,
						supply_name:supply_name,
						supply_no:supply_no,
						supply_num:supply_num,
						supply_unit:supply_unit,
						validity_date:validity_date,
						city_name:city_name,
						s_administrative_area_id:cityId,
						owner:owner,
						d_login_msg_id:ownerId
						
					}
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#accsAdd').hide();
   					$('#accs').show();
					$('#accsEdit').hide();
				},
				error:function(err){
					console.log(err)
				}
		});
	})
	
		//删除
	$("body").delegate("#accs table tbody tr td .delete","click",function() {
		$('.delete_prop').hide();
		$(this).parent().children('div').show();	
	})
	$('body').delegate('#accs table tbody tr td .delete_prop .btn-sure','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"delete",
			url:'/accs/'+id+'.json',
			success:function(data){
				page_state = "change";
				originData();
				$('.delete_prop').hide();
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#accs table tbody tr td .delete_prop .btn-cancel','click',function(){
		$('.delete_prop').hide();
	})
	$('body').delegate('#allChk','click',function(){
//		$('#public_table table').find('.exception-list input').prop('checked',this.checked)
		var allCk = $(this).closest("table").find("input[type='checkbox'][disabled!='disabled']").not("[checkAll]");
        var checked = this.checked;
        allCk.each(function () {
            this.checked = checked;
        });
	});
	
	//批量删除
	
	$('body').delegate('#btnDelete','click',function(){
		$('.loading').show()
 			var ids=[];
			var id=$('.exception-list td input[type="checkbox"]');
			for(var i=0;i<id.length;i++){
				if(id[i].checked){
					ids.push($(id[i]).val());
				}
			}
			$('.ID').val(ids);
			var deleteId=$('.ID').val();
	})
	
	//	分页+初始数据
	function originData(param){
		// pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('accs_table',data_hash);
			$('table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
			data_hash = {};
			var unit_name_id = $('#accs .public_search .searchUnit').val();
            if(unit_name_id){
            	unit_name_id = station_ids.split(",");
            }
			$.ajax({
				url: '/accs.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					sypply_name:$('#accs .public_search .searchName').val(),
					brand:$('#accs .public_search .searchBrand').val(),
					supply_no:$('#accs .public_search .searchNo').val(),
					unit_ids:unit_name_id
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlData = template('accs_table',data);
					$('table .exception-list').html(htmlData);
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/accs.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	    var brandTemp=template('brandList',data)
		$('#accs .form-group .brand').html(brandTemp);
		$('#accsAdd .form-group .brand').html(brandTemp);
		var cityTemp=template('addCity',data)
		$('#citys .choose-city').html(cityTemp);
	})
	

	
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/accs.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
                
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})
