$(function(){
	
	//编辑
	var editId;
	var station_one=[];
	var station_two=[];
	$('body').delegate('table .ststion_regions_edit','click',function(){
		editId=$(this).siblings('.editId').text();
		station_two=[]
		$.ajax({
			url:"/handle_station_regions/"+editId+".json",
			type:'get',
			async:'false',
			success:function(data){
				var tmp=template('regionEdit',data)
				$('#editModal .editRegion').html(tmp);
//				$('#regions_edit').show();
				$.ajax({
					url:"/search_stations.json",
					type:'get',
					success:function(data){
						var stationTmp=template('stationAll',data)
						$('#regions_edit .station_content .station_list').html(stationTmp)
						$('#regions_edit .station_content .station_list input').each(function(i){
							station_one.push($('.station_content .station_list input').eq(i).val());
						});
						$('#regions_edit .station_list2 input').each(function(i){
							station_two.push($('#regions_edit .station_content .station_list2 input').eq(i).val());
						})
						station_two.map(function(ele,index,arr){
							var checkStation = station_one.indexOf(ele)
							$('#regions_edit .station_content .station_list  input[type="checkbox"]').eq(checkStation).prop('checked',true)
						});
						
					},
					error:function(err){
						console.log(err)
					}
				})
			},
			error:function(err){
				console.log(err)
			}
		})
		
		
		
	})
	//编辑提交
	$('body').delegate('#editModal .modal-footer .btn-sure','click',function(){
//		$('#regions_edit .content-outside .regionSubmit').val("提交中...");
//		if($('#regions_edit .content-outside .regionSubmit').val("提交中...")){
//			$('#regions_edit .content-outside .regionSubmit').attr('disabled',true)
//		}else{
//			$('#regions_edit .content-outside .regionSubmit').attr('disabled',false)
//		}
		var region_name=$('#regions_edit .place').val();
		var checkId=[];
		var phms=$('#regions_edit .station_content .station-two input[type="checkbox"]:checked');
		for (var s=0;s<phms.length;s++){
			if(phms[s].checked){
				checkId.push($(phms[s]).val());
			}
		}

	
		if(checkId.length==0){
			checkId=[""]
		}
			
		$.ajax({
			type:"PUT",
			url:'/handle_station_regions/'+editId+'.json',
			dataType: "json",
			data:{
				station_true_ids:checkId
			},
			success:function(data){
				page_state = "change";
				$('#editModal').modal('hide');
				alert("提交成功");
				originData();
				
			},
			error:function(err){
				console.log(err)
				alert("提交失败");
			}
		});
	})
	$('body').delegate('#regions_edit .content-outside h4 .close','click',function(){
		$('#regions_edit').hide();
	});
	$('body').delegate('.station-one  p input','click',function(){
		$(this).parents('.tab-station').find('.station-two p input').prop('checked',this.checked);
	})
	//全选
	$('body').delegate('#regions_edit .station_content  .checkall input','click',function(){
		$(this).parents('.station_content').find('.station_list p input').prop('checked',this.checked);
	})
	
	//查看
	
	$('body').delegate('#regions_look .content-outside h4 .close','click',function(){
		$('#regions_look').hide();
	});
		$('body').delegate('#regions_look .content-outside .btn-cancel','click',function(){
		$('#regions_look').hide();
	});
	//	分页+初始数据
	function originData(param){
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
            var htmlData = template('station_regions',data_hash);
            $('table .handle_msg').html(htmlData);
            $('.page_total span a').text(pp);
        }else{
        	$('.loading').show()
            page_state = "change";
            data_hash = {};
            $.ajax({
                url: '/handle_station_regions.json?page=' + pp+ '&per=' +per,
                type: 'get',
                success:function(data){
                	$('.loading').hide()
                    var htmlData = template('station_regions',data);
                    $('table .handle_msg').html(htmlData);
                    $('.page_total span a').text(pp);
                },
                error:function(err){
                    console.log(err)
                }
            })
        }
	};
	$('.loading').show();
    var data_hash = {};
    var page_state = "init";
	$.getJSON('/handle_station_regions.json').done(function(data){
		$('.loading').hide();
		var total = data.list_size;
        data_hash = data;
	    $("#page").initPage(total, 1, originData);
	    $('.page_total span small').text(total)
	});
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/handle_station_regions.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	
})