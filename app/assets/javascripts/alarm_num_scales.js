$(function(){
//	报警级别
    function plicBox(){
	//  报警级别
	    $.getJSON('/s_alarm_levels.json').done(function(data){
	    	var htmlStr=template('notice_leval',data);
	    	$('#scales .public_search .notice_leval').html(htmlStr)
	    })
    }
	plicBox();
	
	var placeId = ''
	$('.place_leval').change(function(){
		placeId = $(this).val()
		if(placeId == '5'){
			$('.control_stationPort').prop('disabled',true)
		}else{
			$('.control_stationPort').prop('disabled',false)
		}
	})
	
	$('.place_leval').click(function(){
		$('.control_station').val('')
	})
	
//	查询	
    
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	var alarm_level= '';
	function pageName_Data(){
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
		alarm_level = $('.public_search .notice_leval option:selected').val();
	}
	var alarm_count = ''
	var alarmNameArr = []
	var alarmArr = []
	var items = ''
	var alarmName = ''
	var max = ''
	var alarmTotal = ''
	var dAlarms = ''
	
	$('.public_search .search').click(function(){
	alarm_count = ''
	alarmNameArr = []
	alarmArr = []
	items = ''
	alarmName = ''
	max = ''
	alarmTotal = ''
	dAlarms = ''
		$('.loading').show()
	
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/alarm_num_scales.json',
			data:{
				search:{
					alarm_levels:alarm_level,
					data_times:data_times
				}
			},
			success:function(data){
				alarms_arr = data
				data_hash = data
				alarmTotal = data.alarm_total
				dAlarms = data.alarms
				dAlarms.map(function(item,index,arr){
					items = item
					alarm_count = items.alarm_count
					alarmName = items.region_name
					alarmNameArr.push(alarmName)
					alarmArr.push({value:alarm_count,name:alarmName})
				})
				$('.loading').hide();
		    	var originData = template('scalesTab',data)
				$('.public_table .exception-list').html(originData)
		    	/////////////////////////////////////////////////////////////////////////////
		    	
		    	
		    	var myChart = echarts.init(document.getElementById('scal'));
				option = {
				    tooltip: {
				        trigger: 'item',
				        formatter: "{a} <br/>{b}: {c} ({d}%)"
				    },
				    legend: {
				        orient: 'vertical',
				        x: 'left',
				        data:alarmNameArr
				    },
				    series: [
				       
				        {
				            name:'访问来源',
				            type:'pie',
				            radius: ['40%', '55%'],
				
				            data:alarmArr
				        }
				    ]
				};
                myChart.setOption(option);	
		    	
		
		    
		    	
		    	
		    	//////////////////////////////////////////////////////////////////////////
			},
			error:function(err){
				console.log(err)
			}
		})
	})
    
    
//  function originData(param){
//		pageName_Data();
//		var pp = $('#page .pageItemActive').html();
//		if(parseInt(pp) == 1 && page_state == "init"){
//			var originData = template('scalesTab',data_hash)
//			$('.public_table .exception-list').html(originData)
//			$('.page_total span a').text(pp);
//		}else{
//			page_state = "change";
//          data_hash = {};
//          $.ajax({
//				url: '/alarm_num_scales.json?page=' + pp,
//		    	type: 'get',
//				data:{
//					search:{
//						alarm_levels:alarm_level,
//						data_times:data_times
//					}
//				},			   
//		    	success:function(data){
//		    		var originData = template('scalesTab',data)
//					$('.public_table .exception-list').html(originData)
//					$('.page_total span a').text(pp);
//		    	},
//		    	error:function(err){
//		    		console.log(err)	
//		    	}
//			})
//		}
//	
//		
//	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/alarm_num_scales.json',
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var dataHtml = template('place_leval',data)
			$('.place_leval').html(dataHtml)
	        var originData = template('scalesTab',data)
			$('.public_table .exception-list').html(originData)
		},
		error:function(err){
			console.log(err)
		}
	})
	
	
//	导出表格
	
	$('.putTable').click(function(){
    	$(this).attr('disabled',true)
    	pageName_Data();
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("target","_parent");
		form.attr("method","get");
		form.attr("action","/alarm_num_scales/export_file.csv");
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","data_times");
		input2.attr("value",data_times);
		var input3=$("<input>");
		input3.attr("type","hidden");
		input3.attr("name","alarm_levels");
		input3.attr("value",alarm_level);
		$("body").append(form);//将表单放置在web中
		form.append(input2);
		form.append(input3);
		form.submit();//表单提交 
    })
    
    
    
})
