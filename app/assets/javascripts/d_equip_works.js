$(function(){

    //下发人员
    $('body').delegate('.handle_width .issue', 'click',function(){
    	var region_id = $(this).parents('tr').find('.region_id').text();
    	var area_id = $(this).parents('tr').find('.area_id').text();
        $.ajax({
            url: '/d_equip_works/get_login',
            type: 'get',
            data:{
            	region_id:region_id,
            	area_id:area_id
            },
            beforeSend: function () {
                $('#loading').show();
            },
            success:function(data){
                $('#loading').hide();
                var html = template('sendPerson',data);
                $('#issueModal #send_per').html(html);
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    })
//  部门
//$.ajax({
//	type:"get",
//	url:"/d_equip_works/get_equip_regions",
//	success:function(data){
//		var html = template('departmentadd',data);
//      $('#addModal #department').html(html);
//      $('#batchModal #department').html(html);
//	},
//	error:function(err){
//		console.log(err)
//	}
//	
//});
    //创建时间
    $('.createStart').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        startView: 2,
        minView: 2,
        forceParse: true,
        language: 'zh-CN' ,
        setEndDate:$('.day-end').val(),
    }).on('change',function(ev){
        var startDate = $('.createStart').val();
        $(".createEnd").datetimepicker('setStartDate',startDate);
        $(".createStart").datetimepicker('hide');
    });
    $('.createEnd').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        startView: 2,
        minView: 2,
        forceParse: true,
        language: 'zh-CN',
        setStartDate:$(".day-start").val(),
    }).on('change',function(ev){
        var endDate = $('.createEnd').val();
        $(".createStart").datetimepicker('setEndDate',endDate);
        $(".createEnd").datetimepicker('hide');
    });
    dataTime()
    //时间
    function dataTime(){
    	$('.dayStart').datetimepicker({
         format: 'yyyy-mm-dd hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 2,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setEndDate:$('.day-end').val(),
    }).on('change',function(ev){
        var startDate = $('.dayStart').val();
        $(".dayEnd").datetimepicker('setStartDate',startDate);
        $(".dayStart").datetimepicker('hide');
    });
    $('.dayEnd').datetimepicker({
         format: 'yyyy-mm-dd hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 2,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setStartDate:$(".day-start").val(),
    }).on('change',function(ev){
        var endDate = $('.dayEnd').val();
        $(".dayStart").datetimepicker('setEndDate',endDate);
        $(".dayEnd").datetimepicker('hide');
    });
    }
    
    $('#MaintenanceEditor .datetime').datetimepicker({
        format: 'yyyy-mm-dd',
        weekStart: 1,
        autoclose: true,
        startView: 2,
        minView: 2,
        forceParse: true,
        language: 'zh-CN' ,
    }).on('change',function(ev){
        var startDate = $('#MaintenanceEditor .datetime').val();
        // $(".dayEnd").datetimepicker('setStartDate',startDate);
        $("#MaintenanceEditor .datetime").datetimepicker('hide');
    });

  //查询功能
    $('body').delegate('#deviceName1', 'click', function(){
        if($('.navMain #workshop').val() == ''){
            alert("请先选择车间")
        }
    });
    $('body').delegate(".search", 'click', function(){
    	
        $.ajax({
            type: "get",
            url: "/d_equip_works.json",
            data: {
//              equip_ids: $('.navMain #deviceName1').val(),
                begin_time: $('.navMain .createStart').val(),
                end_time: $('.navMain .createEnd').val(),
                status: $('.navMain #CheckTheState').val(),
                equip_ids: $('.navMain #workshop option:selected').val(),
                page: 1,
                per: $('#pagesize option:selected').val()
            },
            beforeSend: function () {
                $('#loading').show();
            },
            success: function(data){
                $('#loading').hide()
                var html = template('d_equip_works',data);
                $('#public_table table').html(html);

                var total = data.equip_works_size;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', searchPage);
                $('#loading').hide();
                var roles=data.role_code
                    Roles(roles)
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    })

    //添加车间区域
    $('body').delegate('#addModal #workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('regionnameadd',data);
                $('#addModal .regionName').html(result);
                
        		
                // $('#addModal').modal('hide');

            },
            error:function(err){
                console.log(err)
            }
        });
    })
    //批量添加车间区域
    $('body').delegate('#batchModal #workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result2 = template('regionnamebatch',data);
                $('#batchModal .regionName').html(result2);
                
        		
                // $('#addModal').modal('hide');

            },
            error:function(err){
                console.log(err)
            }
        });
    })
    //查询车间设备
//  $('body').delegate('.navMain #workshop','change',function(){
//      $.ajax({
//          type:"get",
//          url:"/s_equips/get_equips",
//          data:{
//              region_ids:$(this).val()
//          },
//          success:function(data){
//              var result = template('devicename',data);
//              console.log(result);
//              $('.navMain #deviceName1').html(result);
//              $('#addModal').addClass('hide');
//          },
//          error:function(err){
//              console.log(err)
//          }
//      });
//  })
    //获取检查项
    function GetBoots() {
        $.ajax({
            type:"get",
            url:"/d_equip_works/get_boots",
            success:function(data){
                var result = template('ItemsChecked',data);
                $('#addModal .ItemsChecked').html(result);
                var result2 = template('ItemsChecked2',data);
                $('#batchModal .ItemsChecked2').html(result2);
            },
            error:function(err){
                console.log(err)
            }
        });
    }
    GetBoots();
    //	   初始数据
    function origin(){
        var per=$('#pagesize option:selected').val();
        $.ajax({
            url: '/d_equip_works.json?page=1&per=' +per,
            type: 'get',
            beforeSend: function () {
                $('#loading').show();
            },
            success:function(data){
                $('#loading').hide()
                var html = template('d_equip_works',data);
                $('#public_table table').html(html);

                var total = data.equip_works_size;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', Page);
                $('#loading').hide();
				var roles=data.role_code
                    Roles(roles)
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    }
    origin();



    // 添加维修
    function add_create(){
    	
        var BootArr = [];
        for (var i = 0; i < $(".ItemsChecked input[type='checkbox']").length; i++) {

            if ($(".checkYN").eq(i).is(':checked')) {
                var data = $(".checkYN").eq(i).val();
                BootArr.push(data);
            }
        }
        data = {
            s_region_code_id: $('#addModal #workshop').val(),
            region_name: $('#addModal #workshop').find("option:selected").text(),
            s_area_id:$('#addModal .regionName option:selected').val(),
//          s_equip_id: $('#addModal .DeviceName').val(),
//          equip_name: $('#addModal .DeviceName').find("option:selected").text(),
//          equip_code: $('#addModal .DeviceName').find("option:selected").attr('value1'),
            work_start_time: $('#addModal .dayStart').val(),
            work_end_time: $('#addModal .dayEnd').val(),
            work_desc: $('#addModal .DescriptionInspector').val(),
            s_region_code_info_id:$('#addModal #department option:selected').val()
        }
      		
      		if(data.work_start_time == ''){
      			alert("请输入开始时间")
      		}else if(data.work_end_time == ''){
      			alert("请输入结束时间")
      		}else{
      			$.ajax({
		            type:'post',
		            url:'/d_equip_works',
		            data:{
		                d_equip_work: data,
		                boot_ids: BootArr,
		            },
		            success:function(data){
		              
		                if(data.status == 200){
		                    alert("添加成功");
		                }else{
		                    alert("添加失败")
		                }
		                $('#addModal').modal('hide');
		                document.getElementById("add_station_form").reset()
		                origin();
		            },
		            error:function(err){
		                console.log(err)
		            }
		        })
      		}
       		
      
        
    }
    

    $('body').delegate('#addModal .btn-sure', 'click', function(){
        add_create();
    })
    //复制时间段选择div
    $('#batchModal #Multipletime').click(function(){
		var sourceNode = document.getElementById("time_0"); // 获得被克隆的节点对象 
		var clonedNode = sourceNode.cloneNode(true); // 克隆节点 
		var oId = 1
		
		clonedNode.setAttribute("id", "div-" +oId); // 修改一下id 值，避免id 重复 
		sourceNode.parentNode.appendChild(clonedNode); // 在父节点插入克隆的节点 
		dataTime()

	})
    //车间区域全选
    $('#batchModal .form-group .checkAll').click(function(){
    	$(this).parent().siblings('.regionName').find('input').prop('checked',this.checked)
    })
    //批量添加
    function batchAdd(){
    	
        var BootArr2 = [];
        for (var i = 0; i < $("#batchModal .ItemsChecked2 input[type='checkbox']").length; i++) {

            if ($(".checkYN2").eq(i).is(':checked')) {
                var checkvalue = $(".checkYN").eq(i).val();
                BootArr2.push(checkvalue);
            }
        }
        console.log(BootArr2)
        var regionArr=[];
        for (var i = 0; i < $("#batchModal .regionName input[type='checkbox']").length; i++) {

            if ($(".region").eq(i).is(':checked')) {
                var data = $(".region").eq(i).val();
                regionArr.push(data);
            }
        }
        var batchTime=[]
        $('#batchModal .Times').each(function(){
        
        	var work_start_time=$(this).children().find('.dayStart').val();
        	var work_end_time=$(this).children().find('.dayEnd').val()
        	
        	var list={
        		work_start_time:work_start_time,
        		work_end_time:work_end_time
        	}
        	
        	batchTime.push(list)
        })
      
        
        data2 = {
            s_region_code_id: $('#batchModal #workshop').val(),
            region_name: $('#batchModal #workshop').find("option:selected").text(),
            s_area_ids:regionArr,
			
            time:batchTime,
            work_desc: $('#batchModal .DescriptionInspector').val(),
            s_region_code_info_id:$('#batchModal #department option:selected').val()
        }
        
        console.log(data2.time)
     		$(data2.time).each(function(index,element){
     			console.log(element)
     			if(element.work_start_time==''){
     				alert("请输入开始时间")
     				return;
     			}else if(element.work_end_time==''){
     				alert("请输入结束时间")
     				return;
     			}else if(regionArr.length==0){
     				alert("请选择车间区域")
     				return;
     			}
     		})
     		$.ajax({
	            type:'post',
	            url:'/d_equip_works/batch_create.json',
	            data:{
	                d_equip_work: data2,
	                boot_ids: BootArr2,
	            },
	            success:function(data){
	              
	                if(data.status == 200){
	                    alert("添加成功");
	                }else{
	                    alert("添加失败")
	                }
	                $('#batchModal').modal('hide');
	                document.getElementById("add_station_form").reset()
	                
	                origin();
	                location.reload();
	            },
	            error:function(err){
	                console.log(err)
	            }
	        })
     		
        
    }
    $('body').delegate('#batchModal .btn-sure', 'click', function(){
  		batchAdd();
    })
    //  删除
//	$('body').delegate('.delete', 'click', function(){
//
//		$(this).parent().children('.delete_prop').show();
//  })
//	$('body').delegate('table tbody tr td .delete_prop .btn-sure','click',function(){
//		var id=$(this).parents('tr').find('.id').text();
//	
//		$.ajax({
//			type:"delete",
//			url:'/d_equip_works/' + id,
//			success:function(data){
//				origin();
//				$('.delete_prop').hide();
//			},
//			error:function(err){
//				console.log(err)
//			}
//		})
//	})
//	$('body').delegate('table tbody tr td .delete_prop .btn-cancel','click',function(){
//		$('.delete_prop').hide();
//	})
var id;
var name;
$("body").delegate("table tbody .delete","click",function() {
	id=Number($(this).parents('tr').find('.id').text())
	name=$(this).parents('tr').find('.region_name').text()
	$('#sureCannel').show();
	var sure_str = '<div class="point-out">确认删除 <span>"' + name + '"</span> 吗？</div>';
    $('#sureCannel .sureCannel-body').html(sure_str);

    var sureCannelFoot_str = '';
   
    sureCannelFoot_str += '<button class="btn-sure" id="equips_works_delete">确认</button><button onclick="resetPswd_cannel()" class="btn-cannel">取消</button>'

    $('#sureCannel .box-footer').html(sureCannelFoot_str);
})
$("body").delegate("#equips_works_delete","click",function() {
	$('#sureCannel').hide();
	$.ajax({
			type:"delete",
			url:'/d_equip_works/' + id,
			success:function(data){
				
				 $("#success").show().delay(3000).hide(300);
	            var successFoot_str = '';
	            successFoot_str += '<div class="resetPs1"><span class="resetPs1-span">" ' + name + ' "</span>，删除成功！</div>'
	
	            $('.success-footer').html(successFoot_str)
				origin();
			
			},
			error:function(err){
				console.log(err)
			}
		})

})
    
    
    // 修改维修 在还下发之后的时候才有此权限
    var editID,SEquipID;
    $('body').delegate('.editModal', 'click', function () {
        editID = $(this).parents('tr').find('.id').text();
        $.ajax({
            type: "get",
            url: "/d_equip_works/" + editID,
            success: function (data) {
                if(data.equip_work.work_status == "待下发"){
                    $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor')
                }else if(data.equip_work.work_status == "处理中"){
                    $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor')
                }else if(data.equip_work.work_status == "已完成"){
                    $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                }
                // SEquipID = data.equip_work.s_equip_id;
                var result = template('workNews', data);
                $('#MaintenanceEditor .MaintenanceIfo').html(result);

                var result2 = template('boots', data);
                $('#MaintenanceEditor .CheckProject').html(result2);


            },
            error: function (err) {
                console.log(err);
            }
        })
        $('#d_task_forms').addClass('hide');
        $('#MaintenanceEditor').removeClass('hide');
    })
    $('body').delegate('#MaintenanceEditor #editBack', 'click', function(){
        $('#MaintenanceEditor').addClass('hide');
        $('#d_task_forms').removeClass('hide');
    })

    //修改维修完成
 
    $('body').delegate('#MaintenanceEditor #editDone', 'click', function(){
        var This = $(this);
        var hashArr=[],hash;
        for(var i=0; i < $('#MaintenanceEditor .listNews2').length; i++){
            var boot_id = $('#MaintenanceEditor .listNews2').eq(i).find('.projectName').attr('value');
            var work_result = $('#MaintenanceEditor .listNews2').eq(i).find('input:radio:checked').val();
            var abnor_desc = $('#MaintenanceEditor .listNews2').eq(i).find('#abnormalReason').val();

            hash = {boot_id:boot_id,work_result:work_result,abnor_desc:abnor_desc};
            hashArr.push(hash);
        }
		var image_file= $('#MaintenanceEditor .CheckProject #prvid2 img').attr('src');

		if(image_file=="" || image_file==undefined){
			alert("请选择图片")
		}
		else{
			$.ajax({
	            type:"post",
	            url: "/d_equip_works/carried_out",
	            data: {
	                id:editID,
	                boot_hash: hashArr,
	                image_file:image_file
	            },
	            success:function(data){
	                alert("提交通过");
	                origin();
	                $('#MaintenanceEditor').addClass('hide');
	                $('#d_task_forms').removeClass('hide');
	            },
	            error:function(err){
	                alert("提交失败")
	            }
	        });
		}
        
    })


    //添加上传
    function add_update(){
        var data = {
            s_region_code_id: $('#editModal #workshop').val(),
            region_name: $('#editModal #workshop').find("option:selected").text(),
            s_equip_id: $('#editModal .DeviceName').val(),
            equip_name: $('#editModal .DeviceName').find("option:selected").text(),
            equip_code: $('#editModal .DeviceName').find("option:selected").attr('value1'),
            handle_start_time: $('#editModal .dayStart').val(),
            handle_end_time: $('#editModal .dayEnd').val(),
            snag_desc: $('#editModal .reconditionReason').val(),
            desc: $('#editModal .reconditionDesc').val(),
        };
        $.ajax({
            type:'put',
            url:'/s_equips/' + editID,
            data:{
                s_equips: data,
            },
            success:function(data){
             
                if(data.status == 200){
                    alert("修改成功");
                }else{
                    alert("修改失败")
                }
                $('#editModal').hide();
                origin();

            },
            error:function(err){
                console.log(err)
            }
        })
    }
    $('body').delegate('#editModal .btn-sure', 'click', function(){
        add_update();
    })
    //获取工单ID
    var workOrderID;
    $('body').delegate('.issue', 'click', function(){
        workOrderID = $(this).parents('tr').find('.id').text();
    })
    //下发操作
    $('body').delegate('#issueModal #paiSave', 'click', function(){
        if($('#issueModal input:radio:checked').length == 0){
            alert("请选择维修人员");
        }else {
            $.ajax({
                type: 'post',
                url: '/d_equip_works/issued',
                data: {
                    id: workOrderID,
                    work_person: $('#issueModal input:radio:checked').next().html(),
                },
                success: function (data) {
                  
                    origin();
                    $('#issueModal').hide();
                },
                error: function (err) {
                    console.log(err)
                }
            })
        }
    })


    //查看详情
    $('body').delegate('.watchModal', 'click', function(){
        var watchModalID = $(this).parents('tr').find('.id').text();
        $.ajax({
            type: "get",
            url: "/d_equip_works/" + watchModalID,
            success: function (data) {
                var result = template('DetailsworkNews', data);
                $('#DetailsMaintenance .MaintenanceIfo').html(result);

                var result2 = template('notes', data);
                $('#DetailsMaintenance .notes').html(result2);

                var result3 = template('bootsDetail', data);
                $('#DetailsMaintenance .CheckProjectDetail').html(result3);

				$('#DetailsMaintenance .listNewsDetail #prvid img').attr("src",data.img_path)
                if(data.equip_work.work_status == "待下发"){
                    $('#DetailsMaintenance .CheckProjectDetail input:radio').attr('checked','false');
                    $('#DetailsMaintenance #CheckProjectDetail').addClass('hide');
                  
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(0).find('p').html('待下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('待处理');

                }else if(data.equip_work.work_status == "处理中"){
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor')
					
                    $('#DetailsMaintenance ul li').eq(0).find('p').html('已下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('待处理');
                }else if(data.equip_work.work_status == "完成"){
                    $('#DetailsMaintenance #CheckProjectDetail').removeClass('hide')
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')

                    $('#DetailsMaintenance ul li').eq(0).find('p').html('已下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('已处理');


                    data.boots.forEach(function(ele,index,arr){
                        if(ele.work_result == '正常'){
                            $('#DetailsMaintenance .listNewsDetail').eq(index).find('.normal').attr('checked','true')
                        }else{
                            $('#DetailsMaintenance .listNewsDetail').eq(index).find('.abnormal').attr('checked','true');
                            $('#DetailsMaintenance .listNewsDetail').eq(index).find('.abnormalReason').removeClass('hide');
                            $('#DetailsMaintenance .listNewsDetail').eq(index).find('textarea').val(ele.abnor_desc);
                        }
                    })
                }

            },
            error: function (err) {
                console.log(err);
            }
        })
        $('#d_task_forms').addClass('hide');
        $('#DetailsMaintenance').removeClass('hide');
    })
    $('body').delegate('#DetailsMaintenance #editBack', 'click', function(){
        $('#DetailsMaintenance').addClass('hide');
        $('#d_task_forms').removeClass('hide');
    })

    //异常或者正常提交

    $('body').delegate('#MaintenanceEditor .abnormal', 'click', function(){
        $(this).parents('.listNews2').find('.abnormalReason').removeClass('hide');
    });
    $('body').delegate('#MaintenanceEditor .normal', 'click', function(){
        $(this).parents('.listNews2').find('.abnormalReason').addClass('hide');
    });


    $("#pagesize").change(function(){
        $('#loading').show()
        var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/d_equip_works.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            data: {
                equip_ids: $('.navMain #deviceName1').val(),
                begin_time: $('.navMain .createStart').val(),
                end_time: $('.navMain .createEnd').val(),
                status: $('.navMain #CheckTheState').val(),
            },
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('#loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
                var roles=data.role_code
                    Roles(roles)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    //查询页
    var page_state = "init";
    function searchPage(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;

            var urlStr = "/d_equip_works.json?&page=" + page + "&per=10";
            $.ajax({
                type: 'get',
                url: urlStr,
                data: {
//                  equip_ids: $('.navMain #deviceName1').val(),
                    begin_time: $('.navMain .createStart').val(),
                    end_time: $('.navMain .createEnd').val(),
                    status: $('.navMain #CheckTheState').val(),
                    equip_ids: $('.navMain #workshop option:selected').val(),
                    page: page,
                    per: $('#pagesize option:selected').val(),
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('d_equip_works',data);
                    $('#public_table table').html(html);

                    //分页
                    $('.page_total span a').text(page);
                    $('#loading').hide();
					var roles=data.role_code
                    Roles(roles)
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert("请求数据错误！！！")
                }
            })
        }
    }
    function Page(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;
            $.ajax({
                async: true,
                type: 'get',
                dataType: "json",
                url: '/d_equip_works.json?page=' + page + '&per=' + $('#pagesize option:selected').val(),
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('d_equip_works',data);
                    $('#public_table table').html(html);
                    $('.page_total span a').text(page);
                    $('#loading').hide();
                    var roles=data.role_code
                    Roles(roles)
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert('请求数据失败,请稍后重试');
                    $('#loading').hide();
                }

            })
        }
    }
    //添加缓存
    $('body').delegate('#add', 'click', function(){
        $('#addModal').find('input,textarea').val('');
        $('#addModal').find('.match_vlide').addClass('hide');
        $('#addModal').find('.match_vlide').removeClass('control-default');
        $('#addModal').find('.match_vlide').removeClass('control-error');
        $('#addModal').find('.match_vlide').removeClass('control-success');
        GetBoots();
    })
    //车间项目验证
    $('body').delegate("#addModal #workshop","focus",function(){
        $('#addModal #workshop1').addClass('control-default')
        $('#addModal #workshop1').removeClass('hide')
    });
    $('body').delegate("#addModal #workshop","blur",function(){
        if($('#addModal #workshop').val() != ''){
            $('#addModal #workshop1').removeClass('control-default')
            $('#addModal #workshop1').removeClass('control-error')
            $('#addModal #workshop1').addClass("control-success");
            $('#addModal #workshop1').html('车间名称已选择');
        }else if($('#addModal #workshop').val() == ''){
            $('#addModal #workshop1').removeClass('control-default')
            $('#addModal #workshop1').removeClass('control-success')
            $('#addModal #workshop1').addClass("control-error");
            $('#addModal #workshop1').html("车间名称不能为空")
        }
    });
    //设备名称验证
    $('body').delegate("#addModal .DeviceName","focus",function(){
        $('#addModal #DeviceName').addClass('control-default')
        $('#addModal #DeviceName').removeClass('hide')

    });
    $('body').delegate("#addModal .DeviceName","blur",function(){
        if($('#addModal .DeviceName').val() != ''){
            $('#addModal #DeviceName').removeClass('control-default')
            $('#addModal #DeviceName').removeClass('control-error')
            $('#addModal #DeviceName').addClass("control-success");
            $('#addModal #DeviceName').html('设备已选择');
        }else if($('#addModal .DeviceName').val() == ''){
            $('#addModal #DeviceName').removeClass('control-default')
            $('#addModal #DeviceName').removeClass('control-success')
            $('#addModal #DeviceName').addClass("control-error");
            $('#addModal #DeviceName').html("设备不能为空")
        }
    });
//  //检修计划开始时间验证
//  $('body').delegate("#addModal .beginTime input","focus",function(){
//      $('#addModal #beginTime').addClass('control-default')
//      $('#addModal #beginTime').removeClass('hide')
//
//  });
//  $('body').delegate("#addModal .beginTime input","blur",function(){
//      if($('#addModal .beginTime input').val() != ''){
//          $('#addModal #beginTime').removeClass('control-default')
//          $('#addModal #beginTime').removeClass('control-error')
//          $('#addModal #beginTime').addClass("control-success");
//          $('#addModal #beginTime').html('点检计划开始时间已输入');
//      }else if($('#addModal .beginTime input').val() == ''){
//          $('#addModal #beginTime').removeClass('control-default')
//          $('#addModal #beginTime').removeClass('control-success')
//          $('#addModal #beginTime').addClass("control-error");
//          $('#addModal #beginTime').html("点检计划开始时间不能为空")
//      }
//  });
//
//  //点检计划结束时间验证
//  $('body').delegate("#addModal .endTime input","focus",function(){
//      $('#addModal #endTime').addClass('control-default')
//      $('#addModal #endTime').removeClass('hide')
//  });
//  $('body').delegate("#addModal .endTime input","blur",function(){
//      if($('#addModal .endTime input').val() != ''){
//          $('#addModal #endTime').removeClass('control-default')
//          $('#addModal #endTime').removeClass('control-error')
//          $('#addModal #endTime').addClass("control-success");
//          $('#addModal #endTime').html('点检计划结束时间已输入');
//      }else if($('#addModal .endTime input').val() == ''){
//          $('#addModal #endTime').removeClass('control-default')
//          $('#addModal #endTime').removeClass('control-success')
//          $('#addModal #endTime').addClass("control-error");
//          $('#addModal #endTime').html("点检计划结束时间不能为空")
//      }
//  });


    //点检工单描述验证
    $('body').delegate("#addModal .DescriptionInspector","focus",function(){
        $('#addModal #DescriptionInspector').addClass('control-default')
        $('#addModal #DescriptionInspector').removeClass('hide')

    });
    $('body').delegate("#addModal .DescriptionInspector","blur",function(){
        if($('#addModal .DescriptionInspector').val() != ''){
            $('#addModal #DescriptionInspector').removeClass('control-default')
            $('#addModal #DescriptionInspector').removeClass('control-error')
            $('#addModal #DescriptionInspector').addClass("control-success");
            $('#addModal #DescriptionInspector').html('点检工单描述已输入');
        }else if($('#addModal .DescriptionInspector').val() == ''){
            $('#addModal #DescriptionInspector').removeClass('control-default')
            $('#addModal #DescriptionInspector').removeClass('control-success')
            $('#addModal #DescriptionInspector').addClass("control-error");
            $('#addModal #DescriptionInspector').html("点检工单描述不能为空")
        }
    });

})

//根据角色显示和隐藏新增
function Roles(roles){
	if(roles=="mechanic_repair"){
		$('#d_task_forms .public_search #add').hide();
		$('#d_task_forms #public_table .handle_width .editModal').show();
		
	}else{
		$('#d_task_forms .public_search #add').show();
		$('#d_task_forms #public_table .handle_width .editModal').hide();
	}
	if(roles=="division_level"){
		$('#d_task_forms #public_table table td .delete').show()
		$('#d_task_forms .public_search #batchAdd').show();
	}else{
		$('#d_task_forms #public_table table td .delete').hide()
		$('#d_task_forms .public_search #batchAdd').hide();
	}
}
//保存图片
function previewImage(file, prvid) {
    /* file：file控件
    * prvid: 图片预览容器
    */

    var tip = "Expect jpg or png or gif!"; // 设定提示信息
    var filters = {
        "jpeg" : "/9j/4",
        "gif" : "R0lGOD",
        "png" : "iVBORw"
    }
    var prvbox = document.getElementById(prvid);
    prvbox.innerHTML = "";
    if (window.FileReader) { // html5方案
        for (var i=0, f; f = file.files[i]; i++) {
            var fr = new FileReader();
            fr.onload = function(e) {
                var src = e.target.result;
                if (!validateImg(src)) {
                    alert(tip)
                } else {
                    showPrvImg(src);
                }
            }
            fr.readAsDataURL(f);
        }
    } else { // 降级处理

        if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) {
            alert(tip);
        } else {
            showPrvImg(file.value);
        }
    }

    function validateImg(data) {
        var pos = data.indexOf(",") + 1;
        for (var e in filters) {
            if (data.indexOf(filters[e]) === pos) {
                return e;
            }
        }
        return null;
    }

    function showPrvImg(src) {
        var img = document.createElement("img");
        img.src = src;
        prvbox.appendChild(img);
        $('.look_insp img').zoomify();
    }
}
