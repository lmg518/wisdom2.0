$(function () {
    //	站点
    $.getJSON('/search_station_bindings').done(function (data) {
        var stationTemp = template('editStations', data);
        $('#search-station .choose-site').html(stationTemp);

    })
    $('.loading').show()
    $.ajax({
        type: 'get',
        url: '/station_validity_data.json',
        dataType: 'json',
        success: function (data) {
            $('.loading').hide()
            var originData = template('origin', data)
            $('.public_table .exception-list').html(originData)
        },
        error: function (err) {
            console.log(err)
        }
    })
    //查询
    var start_datetime = '';
    var station_id = '';
    var Invalid;

    function pageName_Data() {

//		start_datetime = $('.public_search .start_datetime').val();
        if ($('.public_search .start_datetime').val()) {
            start_datetime = $('.public_search .start_datetime').val();
        }
        if ($('.public_search .control_stationID').val()) {
            station_id = $('.public_search .control_stationID').val().split(',');
        }
        Invalid = $('.public_search .Invalid option:selected').val()
        console.log(start_datetime)
        console.log(station_id)
        console.log(Invalid)
    }

    $('.loading').show()
    $('.public_search .search').click(function () {
        $('.choose-site').hide()
        $('.loading').show()
        pageName_Data()
        $.ajax({
            type: 'get',
            url: '/station_validity_data.json',
            data: {
                search: {
                    station_ids: station_id,
                    day_time: start_datetime,
                    vain_num: Invalid
                }
            },
            success: function (data) {
                $('.loading').hide()
                var originData = template('origin', data)
                $('.public_table .exception-list').html(originData)
            },
            error: function (err) {
                console.log(err)
            }
        })
    })
    $('.except_box h4 span').click(function () {
        $("#day_time_id").val();
        $("#station_name_id").val();
        $("#item_name_id").val();
        $('.perpro-table').empty();
        $('.except_box').hide();

    })
    $('body').delegate('#station_validity .public_table .click_count', 'click', function () {
        var id = $(this).parent('tr').find('.vaild_id').text();
        var item_name = $(this).children('span').text();
        $.ajax({
            type: "get",
            url: "/station_validity_data/" + id + ".json",
            async: true,
            dataType: "json",
            data: {item_name: item_name},
            success: function (data) {
                var htmlStr = '';
                var day_time = '';
                var station_name = '';
                var item_name = '';
                var item_day_data = '';
                htmlStr += '<table class="table table-bordered">';
                htmlStr += '<tr class="self-min5">';
                htmlStr += '<th>时间</th><th>值</th><th>时间</th><th>值</th><th>时间</th><th>值</th><th>时间</th><th>值</th>';
                htmlStr += '</tr>';
                if (data) {
                    day_time = data.day_time;
                    station_name = data.station_name;
                    item_name = data.item_name;
                    item_day_data = data.item_day_data;
                    $("#day_time_id").val(day_time);
                    $("#station_name_id").val(station_name);
                    $("#item_name_id").val(item_name);
                    for (var i = 0; i < 6; i++) {
                            htmlStr += '<tr>';
                            htmlStr += '<td>' + item_day_data[i + 1 - 1].DATA_TIME + '</td>';
                            htmlStr += '<td>' + item_day_data[i + 1 - 1].DATA_VALUE + '</td>';
                            htmlStr += '<td>' + item_day_data[i + 6].DATA_TIME + '</td>';
                            htmlStr += '<td>' + item_day_data[i + 6].DATA_VALUE + '</td>';
                            htmlStr += '<td>' + item_day_data[i + 12].DATA_TIME + '</td>';
                            htmlStr += '<td>' + item_day_data[i + 12].DATA_VALUE + '</td>';
                            htmlStr += '<td>' + item_day_data[i + 18].DATA_TIME + '</td>';
                            htmlStr += '<td>' + item_day_data[i + 18].DATA_VALUE +'</td>';
                            htmlStr += '</tr>';
                    }
                } else {
                    htmlStr += '<tr><td colspan="8">暂无数据.</td></td></tr>';
                }
                htmlStr += '</table>';
                $('.perpro-table').html(htmlStr);
            },
            error: function (err) {
                console.log(err)
            }
        })
        $('.except_box').show();
    })

})
