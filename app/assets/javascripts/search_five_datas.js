$(function(){
    
    //获取站点
	
	$.getJSON('/search_station_bindings').done(function(data){
		var bindTemp = template('editStations',data);
		$('#search-station .choose-site').html(bindTemp);
	});
    
        //	质控标识
	 $('#check').on('click',function(){
	 	$('.choose-site').hide()
         $('dl').show()
         $(this).val('')
         $('#checkId').val('')
     })
	 var allTextId
    $('#btnSure').on('click',function(){
        if($('dd .p input[type="checkbox"]:checked')){
            var text = []
            var textId = []
            $('dd .p input[type="checkbox"]:checked').next('label').each(function(){
                var news = $(this).text()
                 text.push(news)
            })
             $('dd .p input[type="checkbox"]:checked').each(function(){
                var newsId = $(this).val()
                 textId.push(newsId)
            })
            var allText = []
            allText=text.join(',')
            allTextId = []
            allTextId=textId.join(',')
            $('#checkId').val(allTextId)
            $('#check').val(allText)
            
        }else{
            $('#check').val('')
        }
       $('dl').hide()
       $('dd .p input').prop('checked',false)
       $('#checkAll').prop('checked',false)
    })
     $('#btnQv').on('click',function(){
       $('dd .p input').prop('checked',false)
       $('#checkAll').prop('checked',false)
       $('dl').hide()
    })
     	var items = document.querySelectorAll('dd input')
    $('#checkAll').on('click',function(e){
		for(var i=0;i<items.length;i++){
			items[i].checked = this.checked;
		}
	})
    $('dt a').on('click',function(){
		for(var i=0;i<items.length;i++){
			items[i].checked =! items[i].checked;
		}
		$('#checkAll').checked = isAllselect()    //或
	})
    function isAllselect(){
    	for(var i=0;i<items.length;i++){
    		if(items[i].checked == false){
    			return false;
    		}
    	}
    	return   true;
    }
    isAllselect();
    
		var startTime = '';
		var endTime = '';
		var staStr;
		var allTime = [];
        var item_id = '';
		var item_lable = '';
		var checkId;
    function public_search(){
		startTime = $('.start_time').val();
    	endTime = $('.end_time').val();
		if(startTime && endTime){
            allTime[0] = startTime
            allTime[1] = endTime
        }else if(startTime){
            allTime[0] = startTime
            allTime[1] = startTime
        }
		else if(endTime){
        	allTime[0] = endTime
            allTime[1] = endTime
        }
		else{
            allTime = []
        }
		if($('.control_stationID').val()){
            staStr = $('.control_stationID').val().split(',')
        }else{
        	staStr = ''
        }
        if($('#checkId').val()){
            checkId = $('#checkId').val().split(',')
        }
        item_id = $("#item_id").val()
//      item_lable = $("#item_name_id").val();
	}
//		点击查询的时候
    $('.search').click(function(){
    	$('.loading').show()
    	public_search()
    	var checkId = $('#checkId').val().split(',')
    	console.log(checkId)
		$.ajax({
			type: "get",
			url: "/search_five_datas.json",
			dataType: "json",
			data:{d_date:allTime,station_name:staStr, item_lable: checkId, item_id: item_id},
			success: function(data) {
				$('.loading').hide()
				data_hash = data
				$("#page").initPage(data.list_size, 1, origin)
				$('.page_total span small').text(data.list_size)
			},
			error:function(err){
				console.log(err)
			}
	    })
			
    })
    $('.putTable').click(function(){
    	$(this).attr('disabled',true)
    	public_search()
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
        form.attr("target","_parent");
		form.attr("method","get");
		form.attr("action","/search_five_datas/export_file.csv");
		var input1=$("<input>");
		input1.attr("type","hidden");
		input1.attr("name","station_name");
		input1.attr("value",staStr);
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","d_date");
		input2.attr("value",allTime);
		$("body").append(form);//将表单放置在web中
		form.append(input1);
		form.append(input2);
		form.submit();//表单提交 
    })
    
         //	分页+初始数据
	function origin(param){
        $('.loading').show()
		public_search()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlStr = template('score',data_hash);
			$('.exception-list').html(htmlStr);
			$('.page_total span a').text(pp);
            $('.loading').hide();
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: 'search_five_datas.json?page=' + pp + '&per=' +per,
		    	type: 'get',
		    	data:{d_date:allTime,station_name:staStr, item_lable: checkId, item_id: item_id},
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlStr = template('score',data);
					$('.exception-list').html(htmlStr);
					$('.page_total span a').text(pp);
                    $('.loading').hide();
		    	},
		    	error:function(err){
		    		console.log(err)
		    	}
			})
		}
	
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
    $.ajax({
			type: "get",
			url: "/search_five_datas.json",
			async: true,
			dataType: "json",
			success: function(data) {
				data_hash = data;
				$('.loading').hide()
				var total = data.list_size;
	    		$("#page").initPage(total, 1, origin)

                // $("#page").pagination(total, {
                //     num_edge_entries: 2,      //
                //     num_display_entries: 4,   //
                //     callback: origin,
                //     items_per_page: $('#pagesize option:selected').val()  //每页条数
                // });

	    		$('.page_total span small').text(total)
			},
			error:function(err){
				console.log(err)
			}
	})
//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/search_five_datas.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
        
})

