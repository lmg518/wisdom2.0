$(function () {
    var objId = '';
    $('#myModal').on('hide.bs.modal', function () {
        $('#myModalLabel').text("");
        $('.modal-body').empty();
        $('.modal-footer').empty();
    })
    indexInit();
})
function indexInit() {
    $('.body-infos').empty();
    $.ajax({
        url: '/form_module/table_infos',
        type: 'get',
        success: function (data) {
            $('.body-infos').html(data)
        }
    });

}
function modal_show(obj_id) {
    $('#myModalLabel').text("我是标题" + obj_id);
    get_func_show(obj_id);
    $('#myModal').modal({
        keyboard: true
    });
}

function get_func_show(obj_id) {
    objId = obj_id
    $.ajax({
        url: '/form_module/' + obj_id + "/show",
        type: 'get',
        success: function (data) {
            $('.modal-body').html(data);
            var foot_str = ''
            foot_str += '<button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button> '
            foot_str += '<button type="button" class="btn btn-primary" onclick="form_action(2)"> 提交更改  </button> '
            $('.modal-footer').html(foot_str);
            var checkedID=[]
            var list=document.getElementsByTagName("p");
		    for(var i=0;i<list.length;i++){
		        checkedID[i]=list[i].innerHTML;
		    
		       $('#fieldTitles li  input[type="checkbox"]').each(function(){
		    		var boxId=$(this).val()
		    		if(checkedID[i]==boxId){
		    			$(this).prop('checked',true)
		    		}
		       })
		    }
        }
    });
}
function create_func() {
    $('#myModal').modal({
        keyboard: true
    });
    $('#myModalLabel').text("添加一个检查项模板");
    $.ajax({
        url: '/form_module/new_form_module',
        type: 'get',
        success: function (data) {
            $('.modal-body').html(data);
            var foot_str = ''
            foot_str += '<button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button> '
            foot_str += '<button type="button" class="btn btn-primary" onclick="form_action(1)"> 提交更改  </button> '
            $('.modal-footer').html(foot_str);

        }
    });
}
function form_action(obj) {
    var url = '';
    var type = '';
    
    var form_arr = $(".form-horizontal").serializeArray();
 

    if (parseInt(obj) == 1) {
    	var form_hash = {};
	    $.each(form_arr, function () {
	        form_hash[this.name] = this.value;
	    });
        objId = ''
        url = "/form_module";
        type = "post";
        
    }
    if (parseInt(obj) == 2) {
    	var checkId = [];
    	var phms=$('#fieldTitles li  input[type="checkbox"]:checked');
		for (var s=0;s<phms.length;s++){
			if(phms[s].checked){
				checkId.push($(phms[s]).val());
			}
		}
		var form_hash={field_title_id:checkId}
        url = "/form_module/" + objId;
        type = "put"

    }
    
    $.ajax({
        url: url,
        type: type,
        data: form_hash,
        success: function (data) {
            indexInit();
            $('#myModal').modal('hide');
        },
    });
}
function func_show(obj_id) {

    $.ajax({
        url: '/form_module/new_form_title',
        type: 'get',
        data: {obj_id: obj_id},
        success: function (data) {
        	console.log(data)
            $('.body-infos').empty();
            $('.body-infos').html(data)
        },
    });
}
function delete_func(obj_id) {
	if(confirm("确认删除")){
    $.ajax({
        url: '/form_module/' + String(obj_id) + '.json',
        type: 'delete',
        dataType: 'json',
        success: function (data) {
            indexInit();
           
        },
    });
	}

}