$(function(){
//	站点
	function bindStation(){
		$.getJSON('/search_station_bindings').done(function(data){
			var bindTemp = template('bindStations',data);  //搜索站点
			var stationTemp = template('editStations',data);
			$('#search-station .choose-site').html(stationTemp);
			$('.add_example1 .station_content').html(bindTemp);
		});
	};
	bindStation();
	$('body').delegate('#unsuall .list-station .list-city > span a','click',function(){
		$(this).parents('.list-city').siblings('.list-city-station').toggle()
		.end().parents('.list-station').siblings().find('.list-city-station').hide();
	})
	
	$('.example_name').click(function(){
		$('.choose-site').hide()
	})
	$('.rule_code').click(function(){
		$('.choose-site').hide()
	})
	function alertData(){
		//	获取因子
		$.getJSON('/factors.json').done(function(data){
			var becase = template('becase',data);
			$('.factor_content .control_becase select').html(becase);
			var firstId=$('.factor_content .control_becase .becase').val();
		});
	//	根据因子获取模板
		$('.factor_content .control_becase select').change(function(){
			var becaseID=$(this).val();
			$.getJSON('/factors/'+becaseID+'.json').done(function(data){
				var unusualTemp = template('unusualTemple',data);
				$('.factor_content .unusual_temp select').html(unusualTemp);
//				根据模板获取条件
				$('.factor_content .unusual_temp select').change(function(){
					var unusual_temp_id = $(this).val();
					$.getJSON('/search_abnormal_rules/'+unusual_temp_id+'.json').done(function(data){
						var parameter_num1 = data.rule.parameter_num;
						var compare_code1=data.rule.compare_code;
						var compare_code2=data.rule.compare_code2;
						var compare_code3=data.rule.compare_code3;
						var rule_expression_des=data.rule.rule_expression_des;
						var condition = template('Condition',data);
						$('.add_example1 .factor_content .condition').html(condition);
						if(rule_expression_des==null){
							$('.add_example1 .factor_content .discribe p span').text('请选择异常模板')
						}else{
							$('.add_example1 .factor_content .discribe p span').text(rule_expression_des)
						}
						for(var i=0;i<parameter_num1;i++){
							$('.add_example1 .factor_content .condition .condition_number').append('<span>和</span><input type="text" />')
						}
						if(parameter_num1==0){
							$('.add_example1 .factor_content .condition input').eq(0).hide()
						}else{
							$('.add_example1 .factor_content .condition input').eq(0).show()
						}
						$('.add_example1 .factor_content .condition .condition_number span').eq(0).hide()
					})
				})
			});
		});
	}
//	查询
	var	stationid;
	var	example_name;
	var	rule_code;
	function pageName_Data(){
		example_name=$('.public_search .example_name').val();
		rule_code = $('.public_search .rule_code').val();
		stationid=$('.control_stationID').val().split(',');
	}
	$('#unsuall .public_search .search').click(function(){
		$('.choose-site').hide()
		$('.loading').show()
		stationid=$('.control_stationID').val().split(',');
		example_name=$('.public_search .example_name').val();
		rule_code = $('.public_search .rule_code').val();
		$.ajax({
			type: "get",
			url: "/exception_sets.json",
			contentType: "application/json; charset=utf-8",
			async: true,
			dataType: "json",
			data:{
				search:{
					instance_name:example_name,
					rule_name:rule_code,
					station_id:stationid
				}
			},
			success: function(data) {
				data_hash = data
				$('.loading').hide()
				var originTemp = template('addInfo',data);
				$('table tbody').html(originTemp);
	   		 	$("#page").initPage(data.list_size, 1, origin);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				console.log(err)
				alert("查询站点过多")
			}
		})
	})
//	创建模板
	$('#unsuall .public_search .btn_unusual').click(function(){
		$('.choose-site').hide()
		var data={};
		var addHtml =template('addTemp',data);
		$('.add_example1').html(addHtml);
		$('.add_example1').show();
		$('.public_station').hide();
		$('#public_box').show();
		bindStation();
		alertData();
		formValid();
	});
	$('body').delegate('#unsuall .add_factor h4 .right','click',function(){
		$('.add_example').hide();
		$('#public_box').hide();
	});
	$('body').delegate('#unsuall .add_factor .area_btn .btn-cancel','click',function(){
		$('#public_box').hide();
		$('.add_example').hide();
		$('.add_example').find('input').val('');
	});
	$('#unsuall').delegate('.exam_top .btn_station','click',function(){
		$('.add_example .station_content').show().siblings('.factor_content').hide();
		$('#unsuall .exam_top .btn_station').removeClass('btn-warning').addClass('btn-success');
		$('.btn_exam').removeClass('btn-success').addClass('btn-warning');
	});
	$('#unsuall').delegate('.exam_top .btn_exam','click',function(){
		$('#unsuall .exam_top .btn_exam').removeClass('btn-warning').addClass('btn-success');
		$('.btn_station').removeClass('btn-success').addClass('btn-warning');
		$('.add_example .factor_content').show().siblings('.station_content').hide();
	});

	$('body').delegate('.add_example .factor_content .tolerate input[type="checkbox"]','click',function(){
		if($('.add_example .factor_content .tolerate input').prop('checked')==true){
			$('#unsuall .add_example .factor_content .condition input').val();
		}else{
			$('#unsuall .add_example .factor_content .condition').find('input').eq(0).val('');
			$('#unsuall .add_example .factor_content .condition').find('input').eq(1).val('');
		};
	});
	$('body').delegate('.add_factor .station_content .station_content .tab-station .list-city span input','click',function(){
		$(this).parents('.list-city').siblings('.list-city-station').find('input').prop('checked',this.checked)
	})
	
	//	创建
	var status;
	$('body').delegate('.add_example1 .area_btn .btn-sure','click',function(){
		$('.add_example1 .area_btn .btn-sure').text('提交中...');
		if($('.add_example1 .area_btn .btn-sure').text('提交中...')){
			$('.add_example1 .area_btn .btn-sure').attr('disabled',true);
		}else{
			$('.add_example1 .area_btn .btn-sure').attr('disabled',false);
		}
		if($('.add_example1 .factor_content .tolerate input[type="checkbox"]').prop('checked')==true){
			status='是';
		}else{
			status='否';
		}
		var instance_name1 = $('.add_example1 .factor_content .exam_name input').val();  //实例名称
		var item_name1 = $('.add_example1 .factor_content .control_becase select option:selected').text();
		var s_abnormal_ltem_id1 = $('.add_example1 .factor_content .control_becase select option:selected').val();//因子ID
		var rule_code1 = $('.add_example1 .factor_content .unusual_temp select option:selected').text(); //模板名称
		var s_abnormmal_rule_id1 = $('.add_example1 .factor_content .unusual_temp select option:selected').val(); //异常模板
		var rule_name1 = $('.add_example1 .factor_content .condition .condition_number input').eq(0).val(); //判断条件
		var rule_name2 = $('.add_example1 .factor_content .condition .condition_number input').eq(1).val();//判断条件
		var note1 = $('.add_example1 .factor_content textarea').val();
		var textID1=[];
		$('.add_example1 .station_content .station_content .list-city-station input[type="checkbox"]:checked').next('small').each(function(){
			textID1.push(this.innerHTML);
		});
		$.ajax({
		    url: '/exception_sets.json',
		    type: 'post',
		    data: {
					s_abnormal_rule_instance:
					{
						
						instance_name: instance_name1,
						item_name: item_name1,
						rule_name: rule_code1,
						s_abnormal_ltem_id: s_abnormal_ltem_id1,
						s_abnormmal_rule_id: s_abnormmal_rule_id1,
						parameter_value: rule_name1,
						parameter_value2: rule_name2,
						status: status,
						note: note1
					},
					station_arr:textID1
			},
		    success:function(data) {
		    	page_state = "change";
		    	$('#public_box').hide();
				$('.add_example1').hide();
				alert('创建成功');
		    	origin();
		   },
		   error:function(err){
		   		console.log(err)
		   		alert('创建失败')
		   }
		});
	});
//	修改
	var editId;
	$('body').delegate(".edit","click",function(){
		$('.public_station').hide();
		$('#public_box').show();
		$('.add_example2').show();
	
		editId=$(this).parent().find('.EditId').text();
		
		var instance_name_Edit = $(this).parent().parent().find('.instance_name').text();
		var item_name_Edit = $(this).parent().parent().find('.item_name').text();
		var rule_name = $(this).parent().parent().find('.rule_name').text();
		$.getJSON('/exception_sets/'+ editId +'.json').done(function(data){
			var dataObj = {
				editData:data.exception_sets,
				stationData:data.area_stations
			};
			var editHtml =template('editTemp',dataObj);
			$('.add_example2').html(editHtml);
			formValids();
			var parameter_num2 = dataObj.editData.parameter_num;
			for(var i=0;i<parameter_num2;i++){
				$('.add_example2 .factor_content .condition .condition_number').append('<span>和</span><input type="text" />')
			}
			$('.add_example2 .factor_content .condition .condition_number span').eq(0).hide();
			$('.add_example2 .factor_content .condition .condition_number input').eq(0).val(dataObj.editData.parameter_value);
			$('.add_example2 .factor_content .condition .condition_number input').eq(1).val(dataObj.editData.parameter_value2);
			
			if(parameter_num2==0){
				$('.add_example2 .factor_content .condition input').eq(0).hide()
			}else{
				$('.add_example2 .factor_content .condition input').eq(0).show()
			}
			var station_list=[];
			var station_list2=[];
			$.getJSON('/search_station_bindings').done(function(data){
				var bindTemp = template('bindStations',data);
				$('.add_example2 .station_content .station_list').html(bindTemp);
				$('.add_example2 .station_list .tab-station input[type="checkbox"]').each(function(i){
					if($('.add_example2 .station_list .tab-station input[type="checkbox"]').val()){
						station_list.push($('.add_example2 .list-station input[type="checkbox"]').eq(i).val());
					}
				});
				$('.add_example2 .station_list2 input[type="checkbox"]').each(function(i){
					if($('.add_example2 .station_list2 input[type="checkbox"]').val()){
						station_list2.push($('.add_example2 .station_list2 input[type="checkbox"]').eq(i).val());
					}
				});
				station_list2.map(function(ele,index,arr){
					var checkStation = station_list.indexOf(ele)
					$('.add_example2 .station_list .station_content .list-station input[type="checkbox"]').eq(checkStation).prop('checked',true)
				});
					
			});
		});
	});
	$('body').delegate('.add_example2 .area_btn .btn-sure','click',function(){
		$('.add_example2 .area_btn .btn-sure').text('修改中...');
		if($('.add_example2 .area_btn .btn-sure').text('修改中...')){
			$('.add_example2 .area_btn .btn-sure').attr('disabled',true);
		}else{
			$('.add_example2 .area_btn .btn-sure').attr('disabled',true);
		}
		if($('.add_example2 .factor_content .tolerate input[type="checkbox"]').prop('checked')==true){
			status='是';
		}else{
			status='否';
		};
		var instance_name2 = $('.add_example2 .factor_content .exam_name input').val();  //实例名称
		var rule_name1 = $('.add_example2 .factor_content .condition .condition_number input').eq(0).val(); //判断条件
		var rule_name2 = $('.add_example2 .factor_content .condition .condition_number input').eq(1).val();//判断条件
		var note2 = $('.add_example2 .factor_content textarea').val();
		var textID2=[];
		$('.add_example2 .station_content .list-city-station input[type="checkbox"]:checked').next('small').each(function(){
			textID2.push(this.innerHTML);
		});
		if(textID2.length == 0){
			textID2=[""];
		}
		
		$.ajax({
		    url: '/exception_sets/'+ editId +'.json',
		    type: 'put',
		    async:'true',
		    data: {
					s_abnormal_rule_instance:
					{
						instance_name: instance_name2,
						parameter_value: rule_name1,
						parameter_value2: rule_name2,
						status: status,
						note: note2
					},
					station_arr:textID2
			},
		    success:function( data ) {
		    	page_state = "change";
		    	$('#public_box').hide();
				$('.add_example2').hide();
		    	alert('修改成功');
		    	origin();
		   },
		   error:function(err){
		   	alert('修改失败')
		   		console.log(err)
		   }
		});
	});
//	分页+初始数据
	function origin(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
    		var originTemp = template('addInfo',data_hash);
			$('table tbody').html(originTemp);
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/exception_sets.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
		    		search:{
						instance_name:example_name,
						rule_name:rule_code,
						station_id:stationid
					},
		    	},
		    	success:function(data){
		    		$('.loading').hide()
		    		var originTemp = template('addInfo',data);
					$('table tbody').html(originTemp);
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	$('.loading').show();
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/exception_sets.json').done(function(data){
		$('.loading').hide();
		var total = data.list_size;
		data_hash = data
	    $("#page").initPage(total, 1, origin)
	  	$('.page_total span small').text(total)
	})
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/exception_sets.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	
	
	
	
	
	
	
	function formValid(){
		//实例名称
		var exm_name = document.getElementById("exm_name");
		var exm_nameTip =document.getElementById("exm_nameTip");
		$("body").delegate("#unsuall #exm_name","focus",function(){
			$('#unsuall #exm_nameTip').text('请输入实例名称');
			$('#unsuall #exm_nameTip').addClass('control-default');
			$('#unsuall .area_btn .btn-sure').attr('disabled',true)
			
		})
		$("body").delegate("#unsuall #exm_name","blur",function(){
			if(exm_name.validity.valid){
				exm_nameTip.className = "control-success";
				exm_nameTip.firstChild.nodeValue = "格式正确.";
			$('#unsuall .area_btn .btn-sure').attr('disabled',false)
				
			}else if(exm_name.validity.valueMissing){
				exm_nameTip.className = "control-error";
				exm_nameTip.firstChild.nodeValue = "此项不能为空."
			$('#unsuall .area_btn .btn-sure').attr('disabled',true)
				
			}else if(exm_name.validity.patternMismatch){
				exm_nameTip.className = "control-error";
				exm_nameTip.firstChild.nodeValue = "格式有误."
			$('#unsuall .area_btn .btn-sure').attr('disabled',true)
				
			}
		})
		
	}
	function formValids(){
		//实例名称
		var exm_name = document.getElementById("exm_name");
		var exm_nameTip =document.getElementById("exm_nameTip");
		$("body").delegate("#unsuall #exm_name","focus",function(){
			$('#unsuall #exm_nameTip').text('请输入实例名称');
			$('#unsuall #exm_nameTip').addClass('control-default');
			$('#unsuall .area_btn .btn-sure').attr('disabled',true);
		})
		$("body").delegate("#unsuall #exm_name","blur",function(){
			if(exm_name.validity.valid){
				exm_nameTip.className = "control-success";
				exm_nameTip.firstChild.nodeValue = "格式正确.";
			$('#unsuall .area_btn .btn-sure').attr('disabled',false)
				
			}else if(exm_name.validity.valueMissing){
				exm_nameTip.className = "control-error";
				exm_nameTip.firstChild.nodeValue = "此项不能为空."
			$('#unsuall .area_btn .btn-sure').attr('disabled',true)
				
			}else if(exm_name.validity.patternMismatch){
				exm_nameTip.className = "control-error";
				exm_nameTip.firstChild.nodeValue = "格式有误."
			$('#unsuall .area_btn .btn-sure').attr('disabled',true)
				
			}
		})
		
	}
	
})
