$(function(){
	$.getJSON('/search_station_bindings').done(function(data){
		var bindTemp = template('editStations',data);
		$('#search-station .choose-site').html(bindTemp);
		
	});
	
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	
	$('.rule_name').click(function(){
		$('.choose-site').hide()
	})
	
//	报警级别
	 $('#check').on('click',function(){
         $('dl').show()
         $('.choose-site').hide()
         $(this).val('')
         $('#checkId').val('')
     })
	 var allTextId
    $('#btnSure').on('click',function(){
        if($('dd .p input[type="checkbox"]:checked')){
            var text = []
            var textId = []
            $('dd .p input[type="checkbox"]:checked').next('label').each(function(){
                var news = $(this).text()
                 text.push(news)
            })
             $('dd .p input[type="checkbox"]:checked').each(function(){
                var newsId = $(this).val()
                 textId.push(newsId)
            })
            var allText = []
            allText=text.join(',')
            allTextId = []
            allTextId=textId.join(',')
            $('#checkId').val(allTextId)
            $('#check').val(allText)
        }else{
            $('#check').val('')
        }
       $('dl').hide()
       $('dd .p input').prop('checked',false)
       $('#checkAll').prop('checked',false)
    })
     $('#btnQv').on('click',function(){
       $('dd .p input').prop('checked',false)
       $('#checkAll').prop('checked',false)
       $('dl').hide()
    })
	var items = document.querySelectorAll('dd input')
    $('#checkAll').on('click',function(e){
		for(var i=0;i<items.length;i++){
			items[i].checked = this.checked;
		}
	})
    $('dt a').on('click',function(){
		for(var i=0;i<items.length;i++){
			items[i].checked =! items[i].checked;
		}
		$('#checkAll').checked = isAllselect()    //或
	})
    function isAllselect(){
    	for(var i=0;i<items.length;i++){
    		if(items[i].checked == false){
    			return false;
    		}
    	}
    	return   true;
    }
    isAllselect();
	
//	表头的时间
	var time = new Date();
	var year = time.getFullYear()
	var myMonth = time.getMonth() + 1;
	var myDate = time.getDate() - 1;
	var nowTime = time.getHours();
	var dateArry = [];	
	function headeTitle() {
		for(var i = nowTime + 1; i <= 23; i++) {
			dateArry.push(i)
		}
		for(var i = 0; i <= nowTime; i++) {
			dateArry.push(i)
		}
		$('.self-date th').each(function(i) {
			$(this).html(dateArry[i] + '时');
		})
	}
	headeTitle();
	var today = year + '-' + (myMonth < 10 ? '0' + myMonth : myMonth) + '-' + ((myDate + 1) < 10 ? '0' + (myDate + 1) : (myDate + 1));
		function timeFormat(year,myMonth,myDate){
			if((myMonth == 2 ||myMonth == 4 ||myMonth == 6 ||myMonth == 8||myMonth == 9||myMonth == 11 || myMonth == 1) && (myDate == 0)){
				myDate = 31;
				myMonth -=1;
				if(myMonth == 0){
					year -= 1;
					myMonth =12
				}
			}else if((myMonth == 5||myMonth ==7||myMonth ==10||myMonth == 12) && myDate == 0){
					myDate = 30;
					myMonth -=1;
					if(myMonth == 0){
						year -= 1;
						myMonth =12
					}
			}else if(myMonth == 3 && myDate == 0){
				if( (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)){
					myDate = 29;
				}else{
					myDate = 28;
				}
				myMonth -=1;
				if(myMonth == 0){
					year -= 1;
					myMonth =12
				}
			}else{
				myDate = myDate;
			}
			return year+'-'+ (myMonth < 10 ? '0' + Number(myMonth) : myMonth)+'-' +  (myDate < 10 ? '0' + Number(myDate) : myDate);
		}
		var yesterday = timeFormat(year,myMonth,myDate);
		function yesToday(){
			$('.yesterday').attr('colspan', 23 - nowTime)
			$('.today').attr('colspan', Number(nowTime+1))
			$('.yesterday').html(yesterday)	
			$('.today .today-time').html(today);
		}
		yesToday()	
		$('.loading').show()
//		默认显示列表
        $.ajax({
			type: "get",
			url: "/alarm_monitorings/init_index.json",
			dataType: "json",
			success: function(data) {
				$('.loading').hide()
				var itm = {
					items:data.alarms,
					item:dateArry
				}
				var htmlStr = template('alarms',itm);
				$('.exception-list').html(htmlStr);
			},
			error:function(err){
				console.log(err)
			}
	})
//		点击查询的时候
        $('.search').click(function(){	
        	$('.choose-site').hide()
        	$('.loading').show()
        	$('.glyphicon-arrow-down').removeClass('blue')
        	$('.glyphicon-arrow-up').removeClass('blue')
            var ruleName = $('.rule_name').val()
            if($('.start_time').val()){
                var	startTime = $('.start_time').val();
            }
            if($('.control_stationID').val()){
                var staStr = $('.control_stationID').val().split(',')
            }
	        if($('#checkId').val()){
	            var checkId = $('#checkId').val().split(',')
	        }
            $('.yesterday').attr('colspan',6)
			$('.today').attr('colspan',18)
            $('.yesterday').text('');
			$('.today .today-time').html(startTime)
			var obj = {
				search:{}
			};
			obj.search.data_times = startTime;
			obj.search.rule_name = ruleName;
			obj.search.station_ids = staStr;
			obj.search.alarm_levels = checkId
			$.ajax({
					type: "get",
					url: "/alarm_monitorings/init_index.json",
					dataType: "json",
					data:obj,
					success: function(data) {
						$('.loading').hide()
						var itm = {
					        items:data.alarms,
					        item:dateArry
				        }
						if(startTime){
							var newDateArr = []
							$('.self-date th').each(function(i) {
								newDateArr.push(i)
								itm = {
						            items:data.alarms,
						            item:newDateArr
					            }
								
								$(this).html(i + '时');
							})
						}
						var htmlStr = template('alarms',itm);
						$('.exception-list').html(htmlStr);
					},
					error:function(err){
						console.log(err)
						alert("查询站点过多")
					}
				})
			
		})
//		当你点击站点升降排序时
        $('.sorts .glyphicon-arrow-down').click(function(){
        	$(this).addClass('blue').siblings('span').removeClass('blue')
        	var ruleName = $('.rule_name').val()
        	 if($('.start_time').val()){
                var	startTime = $('.start_time').val();
            }
            if($('.control_stationID').val()){
                var staStr = $('.control_stationID').val().split(',')
            }
	        if($('#checkId').val()){
	            var checkId = $('#checkId').val().split(',')
	        }
        	
			$('.yesterday').attr('colspan',6)
			$('.today').attr('colspan',18)
			$('.yesterday').text('');
			$('.today .today-time').html(startTime)
			var obj = {
				search:{},
				sort_by_station:'desc'
				
			};
			obj.search.data_times = startTime;
			obj.search.rule_name = ruleName;
			obj.search.station_ids = staStr;
			obj.search.alarm_levels = checkId
        	 $.ajax({
					type: "get",
					url: "/alarm_monitorings/init_index.json",
					dataType: "json",
					data:obj,
					success: function(data) {
                        var itm = {
					        items:data.alarms,
					        item:dateArry
				        }
						
					    if(startTime){
							var newDateArr = []
							$('.self-date th').each(function(i) {
								newDateArr.push(i)
								itm = {
						            items:data.alarms,
						            item:newDateArr
					            }
								
								$(this).html(i + '时');
							})
						}
                        var htmlStr = template('alarms',itm);
						$('.exception-list').html(htmlStr);
					},
					error:function(err){
						console.log(err)
					}
				})
        })
        $('.sorts .glyphicon-arrow-up').click(function(){
        	$(this).addClass('blue').siblings('span').removeClass('blue')
        	var ruleName = $('.rule_name').val()
        	 if($('.start_time').val()){
                var	startTime = $('.start_time').val();
            }
            if($('.control_stationID').val()){
                var staStr = $('.control_stationID').val().split(',')
            }
	        if($('#checkId').val()){
	            var checkId = $('#checkId').val().split(',')
	        }
			$('.yesterday').attr('colspan',6)
			$('.today').attr('colspan',18)
			$('.yesterday').text('');
			$('.today .today-time').html(startTime)
			var obj = {
				search:{},
				sort_by_station:'asc'
				
			};
			obj.search.data_times = startTime;
			obj.search.rule_name = ruleName;
			obj.search.station_ids = staStr;
			obj.search.alarm_levels = checkId
        	 $.ajax({
					type: "get",
					url: "/alarm_monitorings/init_index.json",
					dataType: "json",
					data:obj,
					success: function(data) {
                        var itm = {
					        items:data.alarms,
					        item:dateArry
				        }
					    if(startTime){
							var newDateArr = []
							$('.self-date th').each(function(i) {
								newDateArr.push(i)
								itm = {
						            items:data.alarms,
						            item:newDateArr
					            }
								
								$(this).html(i + '时');
							})
						}
					    var htmlStr = template('alarms',itm);
						$('.exception-list').html(htmlStr);
					},
					error:function(err){
						console.log(err)
					}
				})
        })
        //		当你点击报警升降排序时
          $('.sortsAlarm .glyphicon-arrow-down').click(function(){
          	$(this).addClass('blue').siblings('span').removeClass('blue')
          	var ruleName = $('.rule_name').val()
          	 if($('.start_time').val()){
                var	startTime = $('.start_time').val();
            }
            if($('.control_stationID').val()){
                var staStr = $('.control_stationID').val().split(',')
            }
	        if($('#checkId').val()){
	            var checkId = $('#checkId').val().split(',')
	        }
			$('.yesterday').attr('colspan',6)
			$('.today').attr('colspan',186)
			$('.yesterday').text('');
			$('.today .today-time').html(startTime)
			var obj = {
				search:{},
				sort_by_rule:'desc'
				
			};
			obj.search.data_times = startTime;
			obj.search.rule_name = ruleName;
			obj.search.station_ids = staStr;
			obj.search.alarm_levels = checkId
        	 $.ajax({
					type: "get",
					url: "/alarm_monitorings/init_index.json",
					dataType: "json",
					data:obj,
					success: function(data) {
						var itm = {
					        items:data.alarms,
					        item:dateArry
				        }
					    if(startTime){
							var newDateArr = []
							$('.self-date th').each(function(i) {
								newDateArr.push(i)
								itm = {
						            items:data.alarms,
						            item:newDateArr
					            }
								
								$(this).html(i + '时');
							})
						}
					    var htmlStr = template('alarms',itm);
						$('.exception-list').html(htmlStr);
					},
					error:function(err){
						console.log(err)
					}
				})
        })
        $('.sortsAlarm .glyphicon-arrow-up').click(function(){
        	$(this).addClass('blue').siblings().removeClass('blue')
        	var ruleName = $('.rule_name').val()
        	 if($('.start_time').val()){
                var	startTime = $('.start_time').val();
            }
            if($('.control_stationID').val()){
                var staStr = $('.control_stationID').val().split(',')
            }
	        if($('#checkId').val()){
	            var checkId = $('#checkId').val().split(',')
	        }
			$('.yesterday').attr('colspan',6)
			$('.today').attr('colspan',18)
			$('.yesterday').text('');
			$('.today .today-time').html(startTime)
			var obj = {
				search:{},
				sort_by_rule:'asc'
				
			};
			obj.search.data_times = startTime;
			obj.search.rule_name = ruleName;
			obj.search.station_ids = staStr;
			obj.search.alarm_levels = checkId
        	 $.ajax({
					type: "get",
					url: "/alarm_monitorings/init_index.json",
					dataType: "json",
					data:obj,
					success: function(data) {
						var itm = {
					        items:data.alarms,
					        item:dateArry
				        }
					    if(startTime){
							var newDateArr = []
							$('.self-date th').each(function(i) {
								newDateArr.push(i)
								itm = {
						            items:data.alarms,
						            item:newDateArr
					            }
								
								$(this).html(i + '时');
							})
						}
					    var htmlStr = template('alarms',itm);
						$('.exception-list').html(htmlStr);
					},
					error:function(err){
						console.log(err)
					}
				})
        })
//		单元格点击
        	$('body').delegate("tr .red,.yellow,.orange","click",function(){
//      		$('.alertProp').show()
        		$('.alertP').css('display','none').siblings().css('display','block')
        		var id = $(this).find('.sta_id').text()
        		var $ss = $(this)
        	    $.ajax({
					type: "get",
					url: "/alarm_monitorings/" + id + ".json",
					dataType: "json",
					success: function(data) {
						var htmlStr = template('alertpro',data);
						$('.alertProp').html(htmlStr);
						$ss.find('.disable').html(htmlStr).find('tr').html('')
					},
					error:function(err){
						console.log(err)
					}
				})
   		       
   		        
   	        })
            //点击tr弹窗消失
        	$('body').delegate('tr','click',function(){
        		$('.alertP').css('display','none')
        	})
})