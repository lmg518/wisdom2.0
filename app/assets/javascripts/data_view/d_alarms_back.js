$(function(){
	//	站点
	$.getJSON('/search_station_bindings').done(function(data){
		var stationTemp = template('editStations',data);
		$('#search-station .choose-site').html(stationTemp);
		
	})
	$('body').delegate('.add_factor h4 .right','click',function(){
		$('.lookforRules').hide();
		$('#public_box').hide();
	});
	$('body').delegate('.add_factor .area_btn .btn-cancel','click',function(){
		$('#public_box').hide();
		$('.lookforRules').hide();
		$('.lookforRules').find('input').val('');
		$('.lookforRules .factor_content').find('input').val('')
		
	});
	$('body').delegate('.notice_rules .close','click',function(){
		$('#public_box').hide();
	})
	$('body').delegate('.notice_rules .notice_btn .btn-cancel','click',function(){
		$('#public_box').hide();
	})
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	$('.notice_leval').click(function(){
		$('.choose-site').hide()
	})
	$('.search_bottom').click(function(){
		$('.choose-site').hide()
	})
	var time = new Date();
	var year = time.getFullYear();
	var myMonth = time.getMonth()+1;
	var myDate = time.getDate();
	var result = year+'-'+(myMonth<10?'0'+myMonth:myMonth)+'-'+(myDate<10?'0'+myDate:myDate)
	function plicBox(){
	//  报警级别
	    $.getJSON('/s_alarm_levels.json').done(function(data){
	    	var htmlStr=template('notice_leval',data);
	    	$('#d_alarms .public_search .notice_leval').html(htmlStr)
	    })
	//  状态
		$.ajax({
			type:'get',
			url:'/search_by_status.json',
			success:function(data){
				var htmlStr=template('alarmStatus',data);
	    		$('#d_alarms .public_search .status').html(htmlStr)
			},
			error:function(err){
				console.log(err)
			}
		})
    }
	plicBox();
//	查询
	var station_id;
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	var alarm_level;
	var status;
	function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val().split(',');
		}
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
		alarm_level = $('.public_search .notice_leval option:selected').val();
		status = $('.public_search .status option:selected').val();
	}
	var alarms_arr = {}
	var alarms
	$('.public_search .search').click(function(){
		$('.choose-site').hide()
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/d_alarms.json',
			data:{
				search:{
					station_ids:station_id,
					alarm_levels:alarm_level,
					data_times:data_times,
					status:status
				}
			},
			success:function(data){
				alarms_arr = data
				data_hash = data
		   		$('.page_total small').text(data.list_size);
				$('.loading').hide();
		    	$("#page").initPage(data.list_size, 1, originData);
			},
			error:function(err){
				console.log(err)
				alert("查询站点过多")
			}
		})
	})
//	查看
	var lookforID;
	$('body').delegate('#d_alarms .public_table .lookfor','click',function(){
		lookforID = $(this).siblings('.id').text();
		$('#public_box').show();
		$('#d_alarms .lookforRules').show();
		$.ajax({
			type:'get',
			url:'/d_alarms/'+lookforID+'.json',
			dataType:'json',
			success:function(data){
				var htmlStr=template('notice_rules',data);
				$('#d_alarms .lookforRules').html(htmlStr);
				var htmlStr2 = template('alarm_news_list',data)
				$('.exception-list').html(htmlStr2)
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originData = template('originData',data_hash)
			$('.public_table table tbody').html(originData)
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            $.ajax({
				url: '/d_alarms.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
				data:{
					search:{
						station_ids:station_id,
						alarm_levels:alarm_level,
						data_times:data_times,
						status:status
					}
				},			   
		    	success:function(data){
		    		$('.loading').hide()
		    		var originData = template('originData',data)
					$('.public_table table tbody').html(originData)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	
		
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/d_alarms.json',
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
		},
		error:function(err){
			console.log(err)
		}
	})
	
    //每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/d_alarms.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})
