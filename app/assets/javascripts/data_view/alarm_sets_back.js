$(function(){
	//	报警分类
	function alarm_type(){
		$.ajax({
			type:'get',
			url:'/s_alarm_types.json',
			dateType:'json',
			success:function(data){
				var createTemp = template('typeTemp',data)
				$('#alarm_sets .public_search select').html(createTemp)
				$('#alarm_sets .add_alarm1 .alarm_types select').html(createTemp)
			},
			error:function(err){
				console.log(err)
			}
		});
	}
	alarm_type();
	$('body').delegate('#alarm_sets .add_factor h4 .right','click',function(){
		$("#rule_nameTip").removeClass('control-success');
		$("#rule_nameTip").removeClass('control-error');
		$("#rule_sayTip").removeClass('control-success');
		
		$("#rule_numTip").removeClass('control-success');
		$("#rule_numTip").removeClass('control-error');
		$('.add_alarm').hide();
		$('#public_box').hide();
		$('.add_alarm .factor_content').find('input').val("");
	});
	$('body').delegate('#alarm_sets .add_factor .area_btn .btn-cancel','click',function(){
		$('#public_box').hide();
		$('.add_alarm').hide();
		$('.add_alarm').find('input').val('');
	});
	//查询
	var rule_name;
	var rule_instance;
	var rule_type;
	function pageName_Data(){
		rule_name = $('#alarm_sets .public_search input').eq(0).val(); //规则名称
		rule_instance = $('#alarm_sets .public_search input').eq(1).val();//异常实例
		rule_type = $('#alarm_sets .public_search select option:selected').val();
	}
	$('#alarm_sets .public_search .search').click(function(){
		$('.loading').show()
		rule_name = $('#alarm_sets .public_search input').eq(0).val(); //规则名称
		rule_instance = $('#alarm_sets .public_search input').eq(1).val();//异常实例
		rule_type = $('#alarm_sets .public_search select option:selected').val();
		$.ajax({
			type:'get',
			dataType:'json',
			url:'/alarm_sets.json',
			data:{
				search:{
					rule_name:rule_name,
					ab_rule_instance:rule_instance,
					rule_type_id:rule_type
				}
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, origin);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				console.log(err)
			}
		})
		
	})
//	创建
	$('#alarm_sets .public_search .btn_unusual').click(function(){
		
		$('.add_alarm1').show();
		$('#public_box').show();
		formValid();
		alarm_type();
	//	异常判断实例
		$.ajax({
			type:'get',
			url:'/search_abnormarl_instances.json',
			dateType:'json',
			success:function(data){
				var levesTemp = template('rulesType',data)
				$('#alarm_sets .add_alarm1 .alarm_rules select').html(levesTemp)
			},
			error:function(err){
				
			}
		});
//		严重级别
		$.ajax({
			type:'get',
			url:'/s_alarm_levels.json',
			dateType:'json',
			success:function(data){
				var levesTemp = template('levesAlarm',data)
				$('#alarm_sets .add_alarm1 table').html(levesTemp)
			},
			error:function(err){
				console.log(err)
			}
		})
	});
	$('.add_alarm1 .btn-sure').click(function(){
		$('.add_alarm1 .btn-sure').text('提交中...');
		if($('.add_alarm1 .btn-sure').text('提交中...')){
			$('.add_alarm1 .btn-sure').attr('disabled',true);
		}else{
			$('.add_alarm1 .btn-sure').attr('disabled',false);
		}
		var rule_code = $('.add_alarm1 .factor_content .rule_code input').val(); //规则代码
		var rule_name = $('.add_alarm1 .factor_content .rule_name input').val(); //规则名称
		var rule_desc = $('.add_alarm1 .factor_content .rule_desc input').val(); //规则描述
		var rule_type = $('.add_alarm1 .factor_content .alarm_types select option:selected').text(); //报警分类
		var rule_instance = $('.add_alarm1 .factor_content .alarm_rules select option:selected').text(); //异常实例
		var rule_type_id = $('.add_alarm1 .factor_content .alarm_types select option:selected').val();
		var rule_instance_id = $('.add_alarm1 .factor_content .alarm_rules select option:selected').val();
		var sample_num = $('.add_alarm1 .factor_content .sample_num input').val();  //判断次数
		var tableNum1 = $('.add_alarm1 .alarm_table tbody tr').eq(1).find('input').val();
		var tableNum2 = $('.add_alarm1 .alarm_table tbody tr').eq(2).find('input').val();
		var tableNum3 = $('.add_alarm1 .alarm_table tbody tr').eq(3).find('input').val();
		var tableNum1_id = $('.add_alarm1 .alarm_table tbody tr').eq(1).find('.alarm_id').text();
		var tableNum2_id = $('.add_alarm1 .alarm_table tbody tr').eq(2).find('.alarm_id').text();
		var tableNum3_id = $('.add_alarm1 .alarm_table tbody tr').eq(3).find('.alarm_id').text();
		var status;
		if($('.add_alarm1 .status input[type="radio"]').prop('checked')==true){
			status='1';
		}else{
			status='0'
		}
		$.ajax({
			type:'post',
			url:'/alarm_sets.json',
			dataType:'json',
			data:{
				s_alarm_rule:{
					rule_code:rule_code,
					rule_name:rule_name,
					rule_type:rule_type, 
					rule_desc:rule_desc,
					ab_rule_instance:rule_instance,
					sample_num:sample_num,
					status:status,
					rule_instance_id:rule_instance_id,
					rule_type_id:rule_type_id
				},
				alarm_levels:JSON.stringify([{
					level:tableNum1_id,
					ab_times:tableNum1
				},{
					level:tableNum2_id,
					ab_times:tableNum2
				},{
					level:tableNum3_id,
					ab_times:tableNum3
				}])
			},
			success:function(data){
				page_state = "change";
				alert('添加成功');
				$('#public_box').hide();
				$('.add_alarm').hide();
				$('.add_alarm1 .btn-sure').text('保存');
				origin();
			},
			error:function(err){
				alert('添加失败');
				$('.add_alarm1 .btn-sure').text('保存');
			}
		})
		$('.add_alarm1 .add_factor input').val('')
	})
//	编辑
	var editID;
	$('body').delegate('#alarm_sets .public_table table tr .edit','click',function(){
		editID = $(this).parent().siblings('.id').text();
		$.ajax({
			type:'get',
			url:'/alarm_sets/'+editID+'.json',
			dataType:'json',
			success:function(data){
				var editTemp = template('edit',data)
				$('.add_alarm2').html(editTemp);
				$('.add_alarm2').show();
				$('#public_box').show();
				editForm();
			},
			error:function(err){
				alert('获取数据失败');
			}
		});
	})
	$('body').delegate('.add_alarm2 .btn-sure','click',function(){
		$('.add_alarm2 .btn-sure').text('修改中...');
		if($('.add_alarm2 .btn-sure').text('修改中...')){
			$('.add_alarm2 .btn-sure').attr('disabled',true);
		}else{
			$('.add_alarm2 .btn-sure').attr('disabled',false);
		}
		var rule_code = $('.add_alarm2 .factor_content .rule_code input').val(); //规则代码
		var rule_name = $('.add_alarm2 .factor_content .rule_name input').val(); //规则名称
		var rule_desc = $('.add_alarm2 .factor_content .rule_desc input').val(); //规则描述
		var rule_type_id = $('.add_alarm2 .factor_content .alarm_types select option:selected').val();
		var rule_instance_id = $('.add_alarm2 .factor_content .alarm_rules select option:selected').val();
		var sample_num = $('.add_alarm2 .factor_content .sample_num input').val();  //判断次数
		var tableNum1 = $('.add_alarm2 .alarm_table tbody tr').eq(1).find('input').val();
		var tableNum2 = $('.add_alarm2 .alarm_table tbody tr').eq(2).find('input').val();
		var tableNum3 = $('.add_alarm2 .alarm_table tbody tr').eq(3).find('input').val();
		var tableNum1_id = $('.add_alarm2 .alarm_table tbody tr').eq(1).find('.level_id').text();
		var tableNum2_id = $('.add_alarm2 .alarm_table tbody tr').eq(2).find('.level_id').text();
		var tableNum3_id = $('.add_alarm2 .alarm_table tbody tr').eq(3).find('.level_id').text();
		var status;
		if($('.add_alarm2 .status input[type="radio"]').prop('checked')==true){
			status='1';
		}else{
			status='0'
		}
		$.ajax({
			type:'put',
			url:'/alarm_sets/'+editID+'.json',
			data:{
				s_alarm_rule:{
					rule_code:rule_code,
					rule_name:rule_name,
					rule_desc:rule_desc,
					sample_num:sample_num,
					status:status,
					rule_instance_id:rule_instance_id,
					rule_type_id:rule_type_id
				},
				alarm_levels:JSON.stringify([{
					level:tableNum1_id,
					ab_times:tableNum1
				},{
					level:tableNum2_id,
					ab_times:tableNum2
					
				},{
					level:tableNum3_id,
					ab_times:tableNum3
					
				}])
			},
			success:function(data){
				page_state = "change";
				alert('修改成功');
				$('#public_box').hide();
				$('.add_alarm').hide();
				$('.add_alarm2 .btn-sure').text('保存');
				origin();
			},
			error:function(err){
				alert('修改失败');
				$('.add_alarm2 .btn-sure').text('保存');
			}
		})
	})
	//	分页+初始数据
	function origin(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
    		var originData = template('originData',data_hash)
			$('.public_table table tbody').html(originData)
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/alarm_sets.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
		    		search:{
						rule_name:rule_name,
						ab_rule_instance:rule_instance,
						rule_type_id:rule_type
					}
		    	},
		    	success:function(data){
		    		$('.loading').hide()
		    		var originData = template('originData',data)
					$('.public_table table tbody').html(originData)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	$('.loading').show();
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/alarm_sets.json').done(function(data){
		$('.loading').hide();
		var total = data.list_size;
		data_hash = data;
	    $("#page").initPage(total, 1, origin);
	    $('.page_total span small').text(total);
	})
   //每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/alarm_sets.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
   
// 添加验证
    function formValid(){
		//	规则名称
		var rule_name = document.getElementById("rule_name");
		var rule_nameTip = document.getElementById("rule_nameTip");
		$('body').delegate("#alarm_sets #rule_name","focus",function(){
			rule_nameTip.className="control-default";
			rule_nameTip.firstChild.nodeValue = "可修改规则名称."
		})
		$('body').delegate("#alarm_sets #rule_name","blur",function(){
			if(rule_name.validity.valid){
				rule_nameTip.className = "control-success";
				rule_nameTip.firstChild.nodeValue = "规则名称正确.";
				$('.add_alarm1 .area_btn .btn-sure').attr('disabled',false);
			}else if(rule_name.validity.valueMissing){
				rule_nameTip.className = "control-error";
				rule_nameTip.firstChild.nodeValue = "规则名称不能为空.";
				$('.add_alarm1 .area_btn .btn-sure').attr('disabled',true);
			}
			
		})
//		规则描述
		var rule_say = document.getElementById("rule_say");
		var rule_sayTip1 = document.getElementById("rule_sayTip");
		$('body').delegate("#alarm_sets #rule_say","focus",function(){
			rule_sayTip.className="control-default";
			rule_sayTip.firstChild.nodeValue = "请输入规则描述."
		})
		$('body').delegate("#alarm_sets #rule_say","blur",function(){
			if(rule_say.validity.valid){
				rule_sayTip.className = "control-success";
				rule_sayTip.firstChild.nodeValue = "规则描述正确.";
				$('.add_alarm1 .area_btn .btn-sure').attr('disabled',false);
			}else if(rule_say.validity.valueMissing){
				rule_sayTip.className = "control-error";
				rule_sayTip.firstChild.nodeValue = "规则描述不能为空."
				$('.add_alarm1 .area_btn .btn-sure').attr('disabled',true);
			}
			
		});
//		判断次数
		var rule_num = document.getElementById("rule_num");
		var rule_numTip = document.getElementById("rule_numTip");
		$('body').delegate("#alarm_sets #rule_num","focus",function(){
			rule_numTip.className="control-default";
			rule_numTip.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#alarm_sets #rule_num","blur",function(){
			if(rule_num.validity.valid){
				rule_numTip.className = "control-success";
				rule_numTip.firstChild.nodeValue = "判断次数正确.";
				$('.add_alarm1 .area_btn .btn-sure').attr('disabled',false);
			}else if(rule_num.validity.valueMissing){
				rule_numTip.className = "control-error";
				rule_numTip.firstChild.nodeValue = "判断次数不能为空."
				$('.add_alarm1 .area_btn .btn-sure').attr('disabled',true);
			}
			
		})
	}
	
//	编辑验证
	function editForm(){
		//	规则名称
		var rule_name1 = document.getElementById("rule_name1");
		var rule_nameTip1 = document.getElementById("rule_nameTip1");
		$('body').delegate("#alarm_sets #rule_name1","focus",function(){
			rule_nameTip1.className="control-default";
			rule_nameTip1.firstChild.nodeValue = "可修改规则名称."
		})
		$('body').delegate("#alarm_sets #rule_name1","blur",function(){
			if(rule_name1.validity.valid){
				rule_nameTip1.className = "control-success";
				rule_nameTip1.firstChild.nodeValue = "规则名称正确.";
				$('.add_alarm2 .area_btn .btn-sure').attr('disabled',false);
			}else if(rule_name1.validity.valueMissing){
				rule_nameTip1.className = "control-error";
				rule_nameTip1.firstChild.nodeValue = "规则名称不能为空.";
				$('.add_alarm2 .area_btn .btn-sure').attr('disabled',true);
			}
			
		})
//		规则描述
		var rule_say1 = document.getElementById("rule_say1");
		var rule_sayTip1 = document.getElementById("rule_sayTip1");
		$('body').delegate("#alarm_sets #rule_say1","focus",function(){
			rule_sayTip1.className="control-default";
			rule_sayTip1.firstChild.nodeValue = "请输入规则描述."
		})
		$('body').delegate("#alarm_sets #rule_say1","blur",function(){
			if(rule_say1.validity.valid){
				rule_sayTip1.className = "control-success";
				rule_sayTip1.firstChild.nodeValue = "规则描述正确.";
				$('.add_alarm2 .area_btn .btn-sure').attr('disabled',false);
			}else if(rule_say1.validity.valueMissing){
				rule_sayTip1.className = "control-error";
				rule_sayTip1.firstChild.nodeValue = "规则描述不能为空."
				$('.add_alarm2 .area_btn .btn-sure').attr('disabled',true);
			}
			
		});
//		判断次数
		var rule_num1 = document.getElementById("rule_num1");
		var rule_numTip1 = document.getElementById("rule_numTip1");
		$('body').delegate("#alarm_sets #rule_num1","focus",function(){
			rule_numTip1.className="control-default";
			rule_numTip1.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#alarm_sets #rule_num1","blur",function(){
			if(rule_num1.validity.valid){
				rule_numTip1.className = "control-success";
				rule_numTip1.firstChild.nodeValue = "判断次数正确.";
				$('.add_alarm2 .area_btn .btn-sure').attr('disabled',false);
			}else if(rule_num1.validity.valueMissing){
				rule_numTip1.className = "control-error";
				rule_numTip1.firstChild.nodeValue = "判断次数不能为空."
				$('.add_alarm2 .area_btn .btn-sure').attr('disabled',true);
				
			}
			
		})
	}
    
})
