$(function(){
    function plicBox(){
	//  状态
		$.ajax({
			type:'get',
			url:'/search_by_status.json',
			success:function(data){
				var htmlStr=template('alarmStatus',data);
	    		$('#alarmDespatches .public_search .status').html(htmlStr)
			},
			error:function(err){
				console.log(err)
			}
		})
    }
	plicBox();
    
    
//  点击查询时
	var start_datetime = '';
	var createPer,dealPer;
	var status;
	function pageName_Data(){
		if($('.public_search .start_datetime').val()){
			start_datetime = $('.public_search .start_datetime').val();
		}else{
			start_datetime = ''
		}
		createPer = $('.public_search .create').val();
		dealPer = $('.public_search .deal').val();
		status = $('.public_search .status option:selected').val();
	}
	var datas
	$('.public_search .search').click(function(){
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/alarm_despatches.json',
			data:{
				search:{
					date:start_datetime,
					create_user:createPer,
					status:status,
					receive_user:dealPer
				}
			},
			success:function(data){
				datas = data
				data_hash = data
		   		$('.page_total small').text(data.list_size);
				$('.loading').hide();
		    	$("#page").initPage(data.list_size, 1, originData);
			},
			error:function(err){
				console.log(err)
			}
		})
	})
    
    
//  状态判断当我点击响应时
    var clickId
    $('body').delegate('.xiangying','click',function(){
        $(this).parent().children('.xiangyings').show();
        clickId = $(this).parent().find('.id').text()
    })
    var datasId,dataArr,results,result
    $('body').delegate('.xiangyings .btn-sure','click',function(){
    	$('.do').hide()
    	$.ajax({
			type:'put',
			url:'/alarm_despatches/'+ clickId+ '.json',
			data:{handle_answer:true},
			success:function(data){
				var time = new Date();
				var year = time.getFullYear();
				var myMonth = time.getMonth()+1;
				var myDate = time.getDate();
				var hours = time.getHours()
				var mint = time.getMinutes()
				var sec = time.getSeconds()
				results = year+'-'+(myMonth<10?'0'+myMonth:myMonth)+'-'+(myDate<10?'0'+myDate:myDate)+ ' ' + hours + ':' + mint + ':' + sec
				var statu = data.location.task_status
		   		$('table tbody tr').each(function(i){  			
		   			var now=$(this).index()
		   			if($('table tbody tr').eq(now).find('.id').text()==clickId){
			   			if(statu== 'answer'){
			   				$('table tbody tr').eq(now).find('.statusTest').text('待处理')	
			   				$('table tbody tr').eq(now).find('.zhuanfa').hide()
			   				$('table tbody tr').eq(now).find('.xiangying').hide()
			   				$('table tbody tr').eq(now).find('.xiangyingTime').text(results)
			   				$('table tbody tr').eq(now).find('.chuli').show()
			   				$('table tbody tr').eq(now).find('.wancheng').hide()
			   			}
		   			}
		   		})
			},
			error:function(err){
				console.log(err)
			}
		})

    	
    })
    
    $('body').delegate('.zhuanfa','click',function(){
    	$(this).parent().children('.zhuanfas').show();
    	clickId = $(this).parent().find('.id').text()
    })
    
    $('body').delegate('.zhuanfas .btn-sure','click',function(){
    	$('.do').hide()
    	$.ajax({
			type:'put',
			url:'/alarm_despatches/'+ clickId+ '.json',
			data:{handle_retweet:true},
			success:function(data){
		   		
			},
			error:function(err){
				console.log(err)
			}
		})
    	
    })
    
    $('body').delegate('.chuli','click',function(){
    	$(this).parent().children('.chulis').show();
    	clickId = $(this).parent().find('.id').text()
    })
    
    $('body').delegate('.chulis .btn-sure','click',function(){
    	$('.do').hide()
    	$.ajax({
			type:'put',
			url:'/alarm_despatches/'+ clickId+ '.json',
			data:{handle_do:true},
			success:function(data){
				var statu = data.location.task_status
		   		$('table tbody tr').each(function(i){  			
		   			var now=$(this).index()  			
		   			if($('table tbody tr').eq(now).find('.id').text()==clickId){								   
			   			if(statu== 'doing'){
			   				$('table tbody tr').eq(now).find('.statusTest').text('处理中')
			   				$('table tbody tr').eq(now).find('.zhuanfa').hide()
			   				$('table tbody tr').eq(now).find('.xiangying').hide()
			   				$('table tbody tr').eq(now).find('.chuli').hide()
			   				$('table tbody tr').eq(now).find('.wancheng').show()
			   			}
		   			}
		   		})
			},
			error:function(err){
				console.log(err)
			}
		})
    })
     $('body').delegate('.wancheng','click',function(){
    	$(this).parent().children('.wanchengs').show();
    	clickId = $(this).parent().find('.id').text()
    })
    
    $('body').delegate('.wanchengs .btn-sure','click',function(){
    	$('.do').hide()
    	$.ajax({
			type:'put',
			url:'/alarm_despatches/'+ clickId+ '.json',
			data:{handle_done:true},
			success:function(data){
		   		var statu = data.location.task_status
		   		
		   		var time = new Date();
				var year = time.getFullYear();
				var myMonth = time.getMonth()+1;
				var myDate = time.getDate();
				var hours = time.getHours()
				var mint = time.getMinutes()
				var sec = time.getSeconds()
				var result = year+'-'+(myMonth<10?'0'+myMonth:myMonth)+'-'+(myDate<10?'0'+myDate:myDate)+ ' ' + hours + ':' + mint + ':' + sec
		   		$('table tbody tr').each(function(i){  			
		   			var now=$(this).index()  
		   			if($('table tbody tr').eq(now).find('.id').text()==clickId){
		   				
			   			if(statu== 'done'){
			   				$('table tbody tr').eq(now).find('.statusTest').text('已完成')	
			   				$('table tbody tr').eq(now).find('.zhuanfa').hide()
//			   				$('table tbody tr').eq(now).find('.xiangyingTime').text(results)
		   				    $('table tbody tr').eq(now).find('.dealTime').text(result)
			   				$('table tbody tr').eq(now).find('.xiangying').hide()
			   				$('table tbody tr').eq(now).find('.chuli').hide()
			   				$('table tbody tr').eq(now).find('.wancheng').hide()
			   				
			   			}
		   			}
		   		})
			},
			error:function(err){
				console.log(err)
			}
		})
    	
    })
    
    
    $('body').delegate('.do .btn-cancel','click',function(){
    	$('.do').hide()
    })
    
    $('body').delegate('.lookNews','click',function(){
    	clickId = $(this).parent().find('.id').text()
    	$(".alarmDespatchesIndex").hide();
	    $(".public_title").hide();
	    $(".page").hide();
	    $(".page_total").hide();
	    $(".alarmDespatchesOther").show();
	    $('#page').hide()
	    $('.page_total').hide()
	    $.get("/alarm_despatches/despatch_infos",
	        {
	           id: clickId
	        },
	        function (data, status) {
	            $(".alarmDespatchesOther").html(data)
	        });

    })
    
    
    
    $('body').delegate('.back','click',function(){
    	$('.alarmDespatchesIndex').show()
    	$('.alarmDespatchesOther').hide()
    	$('#page').show()
    	$('.page_total').show()
    })
    
    
    
    
//  分页和初始化数据
    
    function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlStr = template('despatchesList',data_hash)
			        $('.exception-list').html(htmlStr)
					$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
				$.ajax({
				url: '/alarm_despatches.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
				data:{
					search:{
						date:start_datetime,
						create_user:createPer,
						status:status,
						receive_user:dealPer
				    }
				},			   
		    	success:function(data){
		    		$('.loading').hide()
		    		datas = data
		    		var htmlStr = template('despatchesList',data)
			        $('.exception-list').html(htmlStr)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
		
		
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/alarm_despatches.json',
		success:function(data){
			$('.loading').hide()
			var total = data.list_size;
			data_hash=data;
		    $("#page").initPage(total, 1, originData)
		    $('.page_total span small').text(total)
		},
		error:function(err){
			console.log(err)
		}
	})
		//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/alarm_despatches.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	
})
//function look_infos(obj){
//  $(".alarmDespatchesIndex").hide();
//  $(".public_title").hide();
//  $(".page").hide();
//  $(".page_total").hide();
//  $(".alarmDespatchesOther").show();
//  $('#page').hide()
//  $('.page_total').hide()
//  $.get("/alarm_despatches/despatch_infos",
//      {
//         id: clickIds
//      },
//      function (data, status) {
//          $(".alarmDespatchesOther").html(data)
//      });
//
//}

function despath_info_hide(){
    $(".alarmDespatchesOther").hide();
    $(".alarmDespatchesOther").empty();
    $(".alarmDespatchesIndex").show();
    $(".public_title").show();
    $(".page").show();
    $(".page_total").show();
}
