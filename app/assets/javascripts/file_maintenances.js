//= require load_kindeditor
$(function(){
	//增加类别
	$('body').delegate("#file_maintenances .add_cate","click",function(){
		var dataObj = {};
		var htmlStr = template('fileCategory',dataObj);
		$('.area_content .category').html(htmlStr)
		$('.public_box .category').show();
	})
	$('body').delegate(".addCategory .area_btn .btn-sure","click",function(){
		
		var category_name=$('.addCategory .category-name .col-sm-5  input').val();
		var parent_id=$('#parent-name option:selected').val();
		$.ajax({
			url:"/file_categories.json",
			type:"post",
			dataType:"json",
			data:{
			file_category:
				{
					category_name:category_name,
					parent_id:parent_id,
				}
			},
			success:function(data){
				$('.public_box .category').hide();
				page_state = "change";
				origin();
			},
			error:function(err){
				console.log(err)
			}
			
		});
	})
	//增加文章
	// $('body').delegate("#file_maintenances .add_article","click",function(){
	// 	var dataObj = {};
	// 	var htmlStr = template('fileArticle',dataObj);
	// 	$('.area_content .article').html(htmlStr)
	// 	$('.public_box .article').show();
	// })
	// $('body').delegate(".addArticle .area_btn .btn-sure","click",function(){
	// 	var title=$('.category-form .category-title .col-sm-5 input').val();
	// 	var contents=$('.category-form #parentContent').val();
	// 	var fileId=$('#classify option:selected').val();
	// 	$.ajax({
	// 		type:"post",
	// 		url:"/file_maintenances.json",
	// 		dataType:"json",
	// 		data:{
	// 			file_maintenance:
	// 				{
	// 					title:title,
	// 					content:contents,
	// 					file_category_id:fileId
	// 				}
	// 		},
	// 		success:function(data){
	// 			$('.public_box .article').hide();
	// 			page_state = "change";
	// 			origin();
	// 		},
	// 		error:function(err){
	// 			console.log(err)
	// 		}
	// 	});
	// })
	$('body').delegate('#file_maintenances h4 .close','click',function(){
		
		$('.public_box .category').hide();
		$('.public_box .article').hide();
		$('.file-look').hide();
		$(".file-edit").hide();
	})
	$('body').delegate('#file_maintenances .area_btn .btn-cancel','click',function(){
		
		$('.public_box .category').hide();
		$('.public_box .article').hide();
		$('.file-look').hide()
		$(".file-edit").hide();
	})
	
//	编辑
	var id;
//	$('body').delegate('#file_maintenances .table .fileEdit','click',function(){
//		
//		$(".file-edit").show();
//		id=$(this).find('.hide').text();
//		$.ajax({
//			type:"get",
//			url:"/file_maintenances/"+ id +".json",
//			dataType: "json",
//			success:function(data){
//				
//				var dataObj={
//					editData:data
//				}
//				
//				var htmlStr = template('fileEdits',dataObj);
//				$('.file-edit').html(htmlStr)
//				
//			},
//			error:function(err){
//				console.log(err)
//			}
//			
//		});
//	})
//	编辑提交
//	$('body').delegate("#sub_create_edit","click",function(){
//		var titles=$('.edit-form .category-title .col-sm-5 input').val();
//		var contents2=$('.edit-form #parentContents').val();
//		var fileIds=$('#classifys option:selected').val();
//		$.ajax({
//			type:"PUT",
//			url:"/file_maintenances/"+ id +".json",
//			dataType: "json",
//			data:{
//				file_maintenance:
//					{
//						title:titles,
//						content:contents2,
//						file_category_id:fileIds
//					}
//			},
//			success:function(data){
//				console.log(data)
//				var dataObj={
//					editData:data
//				}
//				console.log(dataObj)
//				$(".file-edit").hide();
//				var htmlStr = template('fileTable',dataObj);
//				$('.exception-list').html(htmlStr)
//				page_state = "change";
//				origin();
//				
//			},
//			error:function(err){
//				console.log(err)
//				alert('提交失败')
//			}
//			
//		});
//	})
//	查看
	$('body').delegate('#file_maintenances .table .fileLook','click',function(){
        var lookId = '';
		lookId=$(this).find('.hide').text();
        $('#showModal').modal({
            keyboard: true
        });
		$.ajax({
			type:"get",
			url:"/file_maintenances/"+ lookId,
			success:function(data){

                $('#show_title_id').empty();
                $('#show_modal_boday').empty();
                $('#show_modal_boday').html(data);
			},
			error:function(err){
				console.log(err)
			}
		});
	})
         //	分页+初始数据
	function origin(param){
        $('.loading').show()
	
		var pp = $('#page .pageItemActive').html();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlStr = template('fileTable',data_hash);
			$('.exception-list').html(htmlStr);
			$('.page_total span a').text(pp);
            $('.loading').hide();
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: 'file_maintenances.json?page=' + pp,
		    	type: 'get',
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlStr = template('fileTable',data);
					$('.exception-list').html(htmlStr);
					$('.page_total span a').text(pp);
                    $('.loading').hide();
		    	},
		    	error:function(err){
		    		console.log(err)
		    	}
			})
		}
	
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
    $.ajax({
			type: "get",
			url: "/file_maintenances.json",
			async: true,
			dataType: "json",
			success: function(data) {
				data_hash = data;
				$('.loading').hide()
				var total = data.list_size;
	    		$("#page").initPage(total, 1, origin)
	    		$('.page_total span small').text(total)
			},
			error:function(err){
				console.log(err)
			}
	})

//model2 关闭清空 init
    var objId = '';
    $('#myModal').on('hide.bs.modal', function () {
        $('#myModalLabel').text("");
        $("#parentContent").val("")
        my_content.html("");
    })
    //kindEditor
 // KindEditor.ready(function(K){
 //     var contentEditor = K.create("textarea[id='kind_edit']",{
 //         height:300,
 //         width:"100%",
 //         allowFileManager:true,
 //         uploadJson:"/kindeditor/upload",
 //         fileManagerJson:"/kindeditor/filemanager",
 //         "items":["fontname","fontsize","|","forecolor","hilitecolor","bold","italic","underline","removeformat","|","justifyleft","justifycenter","justifyright","insertorderedlist","insertunorderedlist","|","emoticons","image","link"],
 //         afterCreate: function(){ this.sync(); },
 //         afterBlur: function(){ this.sync(); }
 //     });`
 // });


})

//增加文章
function add_file() {
    $('#myModal').modal({
        keyboard: true
    });
    $('#myModalLabel').text("添加文章");
    KindEditor.ready(function(K){
        var contentEditor = K.create("textarea[id='kind_edit']",{
            allowFileManager:true,
            uploadJson:"/kindeditor/upload",
            fileManagerJson:"/kindeditor/filemanager",
            "items":["fontname","fontsize","|","forecolor","hilitecolor","bold","italic","underline","removeformat","|","justifyleft","justifycenter","justifyright","insertorderedlist","insertunorderedlist","|","emoticons","image","link"],
        });
    });
    my_content.html("");
}

function editmodel(data){
		$.ajax({
			type:"get",
			url:"/file_maintenances/"+ data +".json",
			dataType: "json",
			data:{edit_show: "edit"},
			success:function(data){
			$('#editModal').modal({
			        keyboard: true
			});
            $("#edit_form_id").val("");
    		$('#editModalLabel').text("修改文章");
    		if(data.location){
                $("#edit_form_id").val(data.location.id);
                $("#edit_title").val(data.location.title);
                my_editor_2.html(data.location.content);
            }
    		
			},
			error:function(err){
				console.log(err)
			}
			
		});
}

