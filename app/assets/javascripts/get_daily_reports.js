$(function(){



		//获取当前时间
	var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates;
    console.log(datestamp)
    $('#get_daily_reports .public_search #end_day').val(datestamp)
     $('#get_daily_reports .public_search .searchTime').val(datestamp)
    
	//	车间
	$.getJSON('/search_region_codes.json').done(function(data){
		console.log(data)
		var unitTemp = template('unitRegion',data);
		$('#ty_workshop .modal-body').html(unitTemp);
	})
//	查询
var region_id;
var end_day;
$('#get_daily_reports .public_search .search').click(function(){
	$('.loading').show()
	
	if($('#get_daily_reports .public_search .workshopName').val() == ''){

		$('#get_daily_reports .public_search .workshopID').val("")
		
	}
	$('#get_daily_reports .public_search .searchUnit').val($('#get_daily_reports .public_search .workshopID').val());
	if($('#get_daily_reports .public_search .searchUnit').val()){
		region_id=$('#get_daily_reports .public_search .searchUnit').val().split(",");
	}else{
		region_id=''
	}
	$('#get_daily_reports .public_search .searchTime').val($('#get_daily_reports .public_search #end_day').val());
	end_day=$('#get_daily_reports .public_search .searchTime').val();

	$.ajax({
			type:"get",
			url:"/get_daily_reports.json",
			data:{
				end_day:end_day,
				region_id:region_id
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		$("#page").initPage(data.list_size, 1, getOriginalData);
	   		$('.page_total small').text(data.list_size);
	   		
			},
			error:function(err){
				console.log(err)
			}
	});
})
	//	分页+初始数据
	function getOriginalData(param){
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
	    	var htmlStr = template('reportsTable',data_hash);
			$('table .exception-list').html(htmlStr)
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var region_ids = $('#get_daily_reports .public_search .searchUnit').val();
	        if(region_ids){
	        	region_ids = region_ids.split(",");
	        }
			$.ajax({
				url: "/get_daily_reports.json?page="+pp+ '&per=' +per,
		    	type: 'get',
				data:{
					end_day:$('#get_daily_reports .public_search .searchTime').val(),
					region_id:region_ids
				},
		    	success:function(data){
		    		console.log(data)
		    		$('.loading').hide()
		    		
		    		var htmlStr = template('reportsTable',data);
					$('table .exception-list').html(htmlStr)
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
					
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
    $.ajax({
    	type:"get",
    	url:"/get_daily_reports.json",
    	data:{
    		end_day:$('#get_daily_reports .public_search .searchTime').val()
    	},
    	success:function(data){
    		var total = data.list_size;
			data_hash = data;
		    $("#page").initPage(total, 1, getOriginalData);
		    $('.page_total span small').text(total)
    	},
    	error:function(err){
    		console.log(err)
    	}
    });
    //每页显示更改
    $("#pagesize").change(function(){
    	$('.loading').show()
    	var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/get_daily_reports.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            data:{
	    		end_day:$('#get_daily_reports .public_search .searchTime').val()
	    	},
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, getOriginalData)
                $('.page_total span small').text(total)
                
            },
            error:function(err){
                console.log(err)
            }
        })
    })

})
