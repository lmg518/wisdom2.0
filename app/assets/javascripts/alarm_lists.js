$(function() {
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data) {
		var stationTemp = template('stations', data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	//	$('.searchStatus').click(function(){
	//		$('.choose-site').hide()
	//	})
	//	$('.role_name').click(function(){
	//		$('.choose-site').hide()
	//	})
	//	报警级别
	$('#check').on('click', function() {
		$('.choose-site').hide()
		$('dl').show()
		$(this).val('')
		$('#checkId').val('')
	})
	var allTextId
	$('#btnSure').on('click', function() {
		if($('dd .p input[type="checkbox"]:checked')) {
			var text = []
			var textId = []
			$('dd .p input[type="checkbox"]:checked').next('label').each(function() {
				var news = $(this).text()
				text.push(news)
			})
			$('dd .p input[type="checkbox"]:checked').each(function() {
				var newsId = $(this).val()
				textId.push(newsId)
			})
			var allText = []
			allText = text.join(',')
			allTextId = []
			allTextId = textId.join(',')
			$('#checkId').val(allTextId)
			$('#check').val(allText)

		} else {
			$('#check').val('')
		}
		$('dl').hide()
		$('dd .p input').prop('checked', false)
		$('#checkAll').prop('checked', false)
	})
	$('#btnQv').on('click', function() {
		$('dd .p input').prop('checked', false)
		$('#checkAll').prop('checked', false)
		$('dl').hide()
	})
	var items = document.querySelectorAll('dd input')
	$('#checkAll').on('click', function(e) {
		for(var i = 0; i < items.length; i++) {
			items[i].checked = this.checked;
		}
	})
	$('dt a').on('click', function() {
		for(var i = 0; i < items.length; i++) {
			items[i].checked = !items[i].checked;
		}
		$('#checkAll').checked = isAllselect() //或
	})

	function isAllselect() {
		for(var i = 0; i < items.length; i++) {
			if(items[i].checked == false) {
				return false;
			}
		}
		return true;
	}
	isAllselect();

	//  状态

	function plicBox() {
		$.ajax({
			type: 'get',
			url: '/search_by_status.json',
			cache: false,
			success: function(data) {
				var htmlStr = template('alarmStatus', data);
				$('#alarmLists .public_search .status').html(htmlStr)
			},
			error: function(err) {
				console.log(err)
			}
		})
	}
	plicBox();

	//  当你点击查询
	var staStr
	var alarmLevel
	var checkId
	var ruleName
	var status;

	function pageName_Data() {
		ruleName = $('.role_name').val()
		if($('.control_stationIDs').val()) {
			staStr = $('.control_stationIDs').val().split(',')
		}
		if($('#checkId').val()) {
			checkId = $('#checkId').val().split(',')
		}
		status = $('.public_search .status option:selected').val();
	}

	var alarms_arr = {}
	var alarm_lists
	$('.search').click(function() {
		$('.loading').show()
		var ruleName = $('.role_name').val()
		if($('.control_stationIDs').val()) {
			staStr = $('.control_stationIDs').val().split(',')
		} else {
			staStr = ''
		}
		if($('#checkId').val()) {
			checkId = $('#checkId').val().split(',')
		}
		var status = $('.public_search .status option:selected').val();
		var obj = {
			search: {}
		};
		obj.search.rule_name = ruleName;
		obj.search.station_ids = staStr;
		obj.search.alarm_levels = checkId
		obj.search.status = status
		$.ajax({
			type: "get",
			url: "/alarm_lists.json",
			dataType: "json",
			data: obj,
			cache: false,
			success: function(data) {
				$('.loading').hide()
				alarms_arr = data
				data_hash = data
				$("#page").initPage(data.list_size, 1, originData)
				$('.page_total small').text(data.list_size);
			},
			error: function(err) {
				console.log(err)
				alert("查询站点过多")
			}
		})

	})

	//   查看
	var lookforID;
	$('body').delegate('#alarmLists .public_table .lookfor', 'click', function() {
		lookforID = $(this).siblings('.id').text();
//		$('#public_box').show();
//		$('#alarmLists .lookforRules').show();
		$.ajax({
			type: 'get',
			url: '/data_view/alarm_lists/' + lookforID + '.json',
			dataType: 'json',
			cache: false,
			success: function(data) {
				var htmlStr = template('alarm_rules', data);
				$('#lookModal .modal-body').html(htmlStr);
				//				var htmlStr2 = template('alarm_new_list',data)
				//				$('.exception-lists').html(htmlStr2)
			},
			error: function(err) {
				console.log(err)
			}
		})
	})

	$('body').delegate('.add_factor h4 .right', 'click', function() {
		$('.lookforRules').hide();
		$('#public_box').hide();
	});
	$('body').delegate('.add_factor .area_btn .btn-cancel', 'click', function() {
		$('#public_box').hide();
		$('.lookforRules').hide();
		$('.lookforRules').find('input').val('');
	});

	/*//分页
    function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlStr = template('alarmList',data_hash);
			$('.exception-list').html(htmlStr);
			$('.page_total span a').text(pp);
		}else{
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/alarm_lists.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	cache:false,
		    	data:{
		    		search:{
						rule_name:ruleName,
						station_ids:staStr,
		                alarm_levels:checkId,
		                status:status
					}
		    	},
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlStr = template('alarmList',data);
					$('.exception-list').html(htmlStr);
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};*/
	$('.loading').show()
	var data_hash = {};
	var page_state = "init";
	$.ajax({
		type: 'get',
		url: "/alarm_lists.json",
		cache: false,
		success: function(data) {
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
			data_hash = data;
			var newsInfo = template('allIfo', data)
			$('.update').html(newsInfo)
			$("#page").initPage(total, 1, originData)
			$('.page_total span small').text(total)

		},
		error: function(err) {
			console.log(err)
		}
	})
	//每页显示更改
	$("#pagesize").change(function() {
		$.ajax({
			type: "get",
			url: '/alarm_lists.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
			async: true,
			dataType: "json",
			cache: false,
			success: function(data) {
				data_hash = data;
				$('.loading').hide()
				var total = data.list_size;
				$("#page").initPage(total, 1, originData)
				$('.page_total span small').text(total)
			},
			error: function(err) {
				console.log(err)
			}
		})
	})

	//  新加分布任务

	var stationIdArr = []
	var alarmIdArr = []
	var stationId = ''
	var alarmId = ''
	$('body').delegate('.table .checkbox', 'click', function() {
		if($(this).prop('checked') == true) {
			stationId = $(this).parent().siblings().find('.stationID').text()
			stationIdArr.push(stationId)
			alarmId = $(this).parent().siblings().find('.alarmid').text()
			alarmIdArr.push(alarmId)
		} else {
			stationId = $(this).parent().siblings().find('.stationID').text()
			if(stationIdArr.indexOf(stationId) != -1) {
				stationIdArr.splice(stationIdArr.indexOf(stationId), 1)
			}
			alarmId = $(this).parent().siblings().find('.alarmid').text()
			if(alarmIdArr.indexOf(alarmId) != -1) {
				alarmIdArr.splice(alarmIdArr.indexOf(alarmId), 1)
			}
		}
	})
	$('.task').click(function() {
		if($("input[name=tasks]:checked").length <= 0) {
			alert('请先勾选')
			$('#taskModal').modal('hide');
		} else {
//			$('#public_box').show();
//			$('.public_config').show();
			$.ajax({
				type: 'get',
				url: '/search_station_logins.json',
				dataType: 'json',
				cache: false,
				data: { station_arr: stationIdArr },
				success: function(data) {
					var configTemplate = template('taskNews', data);
					$('#taskModal .modal-body').html(configTemplate);
					$('#taskModal').modal('show');
				},
				error: function(err) {
					console.log(err)
				}
			})
		}

	})

	$(document).delegate('.public_config .close', 'click', function() {
		$('#public_box').hide();
		$('.public_config').hide();
		$('.public_config .config_list input').prop('checked', false)
		$('.table').find('.checkbox').prop('checked', false)
		stationIdArr = []
		alarmIdArr = []
	})
	//	配置提交
	var ar
	$('body').delegate('#taskModal .modal-footer .btn-sure', 'click', function() {
//		$('.public_config .config_btn .btn-sure').text('配置中...');
//		if($('.public_config .config_btn .btn-sure').text('配置中...')) {
//			$('.public_config .config_btn .btn-sure').attr('disabled', true);
//		} else {
//			$('.public_config .config_btn .btn-sure').attr('disabled', false);
//		}

		var oIn_left = $('.config_list .left input[type="radio"]')
		$(oIn_left).each(function(i) {
			if(oIn_left[i].checked) {
				ar = $(this).parent().find('.renid').text();
				console.log(ar)
			}
		});
		var alarm_lists;
		$.ajax({
			url: '/search_station_logins/despatch_create',
			type: 'post',
			cache: false,
			data: { alarm_arr: alarmIdArr, login_arr: ar },
			success: function(data) {
				console.log(data)
//				$('#public_box').hide();
				alert("配置成功");
				$('.public_config .config_list input').prop('checked', false)
				$('.public_config .config_btn .btn-sure').attr('disabled', false);
				$('.public_config .config_btn .btn-sure').text('确定');
				$('#taskModal').modal('hide');
				originData();

				console.log('自己加的ajax');

				/*modified by  jinzhenkai 配置成功后清除数组  start*/
				stationIdArr = [];
				alarmIdArr = [];
				ar = '';
				/*modified by  jinzhenkai end*/

				//			    	var newID,newIds
				//			    	alarms_arr = data
				//			    	
				//			    	alarm_lists = alarms_arr.alarm_lists
				//			    	
				//			    	alarm_lists.map(function(item,index,arr){
				//			    		newID = item.id;
				//			    		newIds = newID.toString();
				//                     if (alarmIdArr.indexOf(newIds) > -1){
				//                         alarm_lists.splice(index,1)
				//                     }
				//                     return alarms_arr
				//			    	})
				//			    	var originData1 = template('originData',alarms_arr)
				//                   $('.public_table table tbody').html(originData1)
				//                  alarms_arr.list_size = alarms_arr.alarm_lists.length
				//                  var total = alarms_arr.list_size
				//                  $('.page_total small').text(total);
				//					data_hash = data
				//			        $("#page").initPage(data.list_size, 1, originData)
				//  		        $('.page_total small').text(data.list_size);
				//originData();
			},
			error: function(err) {
				console.log(err)
				alert("配置失败");
				$('.public_config .config_list input').prop('checked', false)
			}
		})
	})
	//	                       取消配置
	$('body').delegate('.public_config .config_btn .btn-cancel', 'click', function() {
		$('#public_box').hide();
		$('.public_config').hide();
		$('.public_config .config_list input').prop('checked', false)
		$('.table').find('.checkbox').prop('checked', false)
		stationIdArr = []
		alarmIdArr = []
	})

	//  新加分布任务    

	//分页
	function originData(param) {
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per = $('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init") {
			var htmlStr = template('alarmList', data_hash);
			$('.exception-list').html(htmlStr);
			$('.page_total span a').text(pp);
		} else {
			$('.choose-site').hide()
			$('.loading').show()
			page_state = "change";
			data_hash = {};
			$.ajax({
				url: '/alarm_lists.json?page=' + pp + '&per=' + per,
				type: 'get',
				cache: false,
				data: {
					search: {
						rule_name: ruleName,
						station_ids: staStr,
						alarm_levels: checkId,
						status: status
					}
				},
				success: function(data) {
					$('.loading').hide()
					var htmlStr = template('alarmList', data);
					$('.exception-list').html(htmlStr);
					$('.page_total span a').text(pp);
				},
				error: function(err) {
					console.log(err)
				}
			})
		}
	};

})