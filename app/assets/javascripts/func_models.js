
$(function () {
    var objId = '';
    $('#myModal').on('hide.bs.modal', function () {
        $('#myModalLabel').text("");
        $('.modal-body').empty();
        $('.modal-footer').empty();
    })
    indexInit();
})
function indexInit() {
    $('.body-infos').empty();
    $.ajax({
        url: '/func_models/table_infos',
        type: 'get',
        success: function (data) {
            $('.body-infos').html(data)
        }
    });

}
function modal_show(obj_id) {
    $('#myModalLabel').text("我是标题" + obj_id);
    get_func_show(obj_id);
    $('#myModal').modal({
        keyboard: true
    });
}
function get_func_show(obj_id) {
    objId = obj_id
    $.ajax({
        url: '/func_models/' + obj_id + "/modal_form",
        type: 'get',
        success: function (data) {
            $('.modal-body').html(data);
            var foot_str = ''
            foot_str += '<button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button> '
            foot_str += '<button type="button" class="btn btn-primary" onclick="form_action(2)"> 提交更改  </button> '
            $('.modal-footer').html(foot_str);
        }
    });
}
function create_func() {
    $('#myModal').modal({
        keyboard: true
    });
    $('#myModalLabel').text("添加一个菜单");
    $.ajax({
        url: '/func_models/new',
        type: 'get',
        success: function (data) {
            $('.modal-body').html(data);
            var foot_str = ''
            foot_str += '<button type="button" class="btn btn-default" data-dismiss="modal">关闭 </button> '
            foot_str += '<button type="button" class="btn btn-primary" onclick="form_action(1)"> 提交更改  </button> '
            $('.modal-footer').html(foot_str);

        }
    });
}
function form_action(obj) {
    var url = '';
    var type = '';
    var form_hash = {};
    var form_arr = $(".form-horizontal").serializeArray();
    $.each(form_arr, function () {
        form_hash[this.name] = this.value;
    });
    if (parseInt(obj) == 1) {
        objId = ''
        url = "/func_models";
        type = "post";
    }
    if (parseInt(obj) == 2) {
        url = "/func_models/" + objId;
        type = "put"
    }
    $.ajax({
        url: url,
        type: type,
        data: form_hash,
        success: function (data) {
            indexInit();
            $('#myModal').modal('hide');
        },
    });
}
function func_show(obj_id) {
    $.ajax({
        url: '/func_models/table_infos',
        type: 'get',
        data: {obj_id: obj_id},
        success: function (data) {
            $('.body-infos').empty();
            $('.body-infos').html(data)
        },
    });
}
function delete_func(obj_id) {
    $.ajax({
        url: '/func_models/' + String(obj_id) + '.json',
        type: 'delete',
        dataType: 'json',
        success: function (data) {
            indexInit();
            // $("#myButton").click(function() {
            //     $("#myDiv").fadeTo("slow", 0.01, function(){//fade
            //         $(this).slideUp("slow", function() {//slide up
            //             $(this).remove();//then remove from the DOM
            //         });
            //     });
            // });
        },
    });
}   

function init_data() {

}
