$(function(){
		//获取当前时间
	var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates;
    console.log(datestamp)
 
    //  首页获取数据

	
$('.loading').show()
   $.ajax({
   	type:"get",
   	url:"/steam_report/steam_report",
    success:function(data){
	   	$('#steam_report #public_table .excelTable').html(data)
	   	   $('#steam_report #public_table #water_report .time').html(datestamp)
	   	   $("#steam-table").rowspan(0);//合并单元格
	   	   $("#steam-table").rowspan(1);//合并单元格
	   	   $('.loading').hide()
	   	   //暂存
	   	   $('body').delegate('#steam_report #save','click',function(){
	   	   	$('.loading').show()
			var status='N'
				submit(status)
			})
	   	   var status=$('#steam_report #public_table #water_report .state').html();
	   	   if(status=="已上报"){
	   	   	$('#steam_report #public_table #water_report .state').css("color","#00923f")

			$('#steam_report #public_table .table_button').hide();
	   	   }
	   },
	   error:function(err){
    		console.log(err)
    	}
   }); 
// 上报
   $('body').delegate('#steam_report #report','click',function(){
   	$('.loading').show()
		var status='Y'
		submit(status)
   })
   var energy_params=[]
	var objname={}
	//暂存和上报所调用的方法
   function submit(status){
   	var date_time=$('#steam_report #public_table #water_report .time').html();
 
		var f_form_module_id=$('.excelTable .modalID').html()
		$('#steam_report #steam-table td input[type="text"]').each(function(){
			var field_code=$(this).attr('name');
			var field_value=this.value;
			var region_id=$(this).parents('tr').find('.id').html();
			var list={
					field_code:field_code,
					field_value:field_value,
					region_id:region_id
				}
				energy_params.push(list)
		})
			
			objname={energy_params,date_time:date_time,status:status,id:f_form_module_id}
			console.log(objname)
					
   		$.ajax({
			type:"POST",
			url:'/steam_report',
			data:objname,
			success:function(data){
				console.log(data)
				alert("成功")
				$('.loading').hide()
				window.location.reload();
			},
			error:function(err){
				console.log(err)
			}
		});
   }
})

//合并单元格
jQuery.fn.rowspan = function(colIdx) { //封装的一个JQuery小插件
    return this.each(function(){
        var that;
        $('tr', this).each(function(row) {
            $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                if (that!=null && $(this).html() == $(that).html()) {
                    rowspan = $(that).attr("rowSpan");
                    if (rowspan == undefined) {
                    $(that).attr("rowSpan",1);
                    rowspan = $(that).attr("rowSpan"); }
                    rowspan = Number(rowspan)+1;
                    $(that).attr("rowSpan",rowspan);
                    $(this).hide();
                } else {
                 that = this;
                }
            });
        });
    });
}