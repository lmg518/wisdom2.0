$(function(){


	//获取当前时间
	var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates;
    console.log(datestamp)
    $('#company_reports .public_search #end_date_range').val(datestamp)
     $('#company_reports .public_search .search_endTime').val(datestamp)
     var firstDate=year + "-" + newMonths + "-"+"01"//获取当月第一天
     $('#company_reports .public_search #start_date_range').val(firstDate)
     $('#company_reports .public_search .search_beginTime').val(firstDate)
//	查询
var region_id;
var begin_day;
var end_day;
$('#company_reports .public_search .search').click(function(){
	$('.loading').show()
	
	if($('#company_reports .public_search .workshopName').val() == ''){

		$('#company_reports .public_search .workshopID').val("")
		
	}
	$('#company_reports .public_search .searchUnit').val($('#company_reports .public_search .workshopID').val());
	if($('#company_reports .public_search .searchUnit').val()){
		region_id=$('#company_reports .public_search .searchUnit').val().split(",");
	}else{
		region_id=''
	}
	$('#company_reports .public_search .search_beginTime').val($('#company_reports .public_search #start_date_range').val());
	begin_day=$('#company_reports .public_search .search_beginTime').val();
	$('#company_reports .public_search .search_endTime').val($('#company_reports .public_search #end_date_range').val());
	end_day=$('#company_reports .public_search .search_endTime').val();
	
	$.ajax({
			type:"get",
			url:"/company_reports.json",
			data:{
				begin_day:begin_day,
				end_day:end_day,
				region_id:region_id
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		$("#page").initPage(data.list_size, 1, getOriginalData);
	   		$('.page_total small').text(data.list_size);
	   		chartNumber(data_hash)
			},
			error:function(err){
				console.log(err)
			}
	});
})
		//	车间
	$.getJSON('/search_region_codes.json').done(function(data){
		console.log(data)
		var unitTemp = template('unitRegion',data);
		$('#ty_workshop .modal-body').html(unitTemp);
	})
	//列表和趋势图的显示和隐藏
  $('#company_reports .btn-group #listTable').click(function(){
  	$(this).css({ "color": "#fff", "background": "#00923f" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
  	$('#company_reports .chartContent').hide();
  	$('#company_reports .tableContent').show();
  })
  $('#company_reports .btn-group #listChart').click(function(){
  	$(this).css({ "color": "#fff", "background": "#00923f" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
  	$('#company_reports .chartContent').show();
  	$('#company_reports .tableContent').hide();
  })
  //	分页+初始数据
	function getOriginalData(param){
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
	    	var htmlStr = template('reportsTable',data_hash);
			$('#company_reports #public_table table .exception-list').html(htmlStr)
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var region_ids = $('#company_reports .public_search .searchUnit').val();
	        if(region_ids){
	        	region_ids = region_ids.split(",");
	        }
			$.ajax({
				url: "/company_reports.json?page="+pp+ '&per=' +per,
		    	type: 'get',
				data:{
					begin_day:$('#company_reports .public_search .search_beginTime').val(),
					end_day:$('#company_reports .public_search .search_endTime').val(),
					region_id:region_ids
				},
		    	success:function(data){
		    		console.log(data)
		    		$('.loading').hide()
		    		chartNumber(data)
		    		var htmlStr = template('reportsTable',data);
					$('#company_reports #public_table table .exception-list').html(htmlStr)
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
					
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
    $.ajax({
    	type:"get",
    	url:'/company_reports.json',
    	data:{
    		begin_day:$('#company_reports .public_search .search_beginTime').val(),
    		end_day:$('#company_reports .public_search .search_endTime').val()
    	},
    	success:function(data){
    		console.log(data)
			var total = data.list_size;
			data_hash = data;
		    $("#page").initPage(total, 1, getOriginalData);
		    $('.page_total span small').text(total)
		    chartNumber(data_hash)
		    $('.loading').hide()
    	},
    	error:function(err){
    		console.log(err)
    	}
    });
 //每页显示更改
    $("#pagesize").change(function(){
    	var listcount=$(this).val()
    	$('.loading').show()
        $.ajax({
            type: "get",
            url: '/company_reports.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            data:{
	    		begin_day:$('#company_reports .public_search .search_beginTime').val(),
	    		end_day:$('#company_reports .public_search .search_endTime').val()
	    	},
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                console.log(total)
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, getOriginalData)
                $('.page_total span small').text(total)
                
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	
		//折线图
	function chartNumber(data_hash){
		console.log(data_hash)
		var xData=data_hash.months;
		var numbers=data_hash.month_days
		console.log(xData)
		console.log(numbers)
		var chart = Highcharts.chart('lineChart', {
		    title: {
		        text: '生产日趋势图'
		    },
		    credits: {
		        enabled: false//右下角的版权信息不显示
		    },
		    yAxis: {
		        title: {
		            text: 'ton（t）'
		        }
		    },
//		    tooltip: {
//		        crosshairs: true,
//		        shared: true
//		    },
		    legend: {
		        layout: 'horizontal',
		        align: 'center',
		        verticalAlign: 'bottom'
		    },
		    xAxis: {
	        
	            categories: xData
	        },
		    series:numbers,
		//  responsive: {
		//      rules: [{
		//          condition: {
		//              maxWidth: 500
		//          },
		//          chartOptions: {
		//              legend: {
		//                  layout: 'horizontal',
		//                  align: 'center',
		//                  verticalAlign: 'bottom'
		//              }
		//          }
		//      }]
		//  }
		});
	}

})
