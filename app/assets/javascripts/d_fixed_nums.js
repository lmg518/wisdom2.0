$(function(){
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	
	
	//	查询
	var station_id;
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
	var date = new Date();
	var year = date.getFullYear();
	$('.dateTimepickerH').datetimepicker({
		format:"Y-m-d H:i",
		minDate:'2017'+'-01'+'-01',
		maxDate: year+'-12'+'-31',
		timepicker:true,
		step:5
	  });
	//   jianceyinzi 
	  $.ajax({
		type:'get',
		url:"/d_fixed_nums/get_item_code",
		success:function(data){
			var Data = template('item_list',data)
			$('#item_id').html(Data)
		},
		error:function(err){
			console.log(err)
			alert("服务器报错")
		}
	})
	//   chakan
	$("body").delegate("#d_fixed_nums table a.look","click",function(){
		//$("#public_box").show();
//		$(".lookforRules").show();
		var id = $(this).parent("td").attr("value_id");
		$.ajax({
			type:'get',
			url:"/d_fixed_nums/"+id+".json",
			success:function(data){
				var Data = template('notice_rules',data);
				$('#lookModal .modal-body').html(Data)
			},
			error:function(err){
				console.log(err)
				alert("服务器报错")
			}
		})
	})
	$("body").delegate("#d_fixed_nums .lookforRules .close","click",function(){
		$(".lookforRules").hide();
	})
	function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val().split(',');
		}
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
	}
	var item_id;	
	$('.public_search .search').click(function(){
		$('.loading').show();
		item_id = $(".public_search #item_id option:selected").val();
		if(item_id.length == 0) {
			alert("请选择因子")
		}
		
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/d_fixed_nums.json',
			data:{
				
					station_ids:station_id,
					begin_time:data_times[0],
					end_time:data_times[1],
					item_id:item_id
				
			},
			success:function(data){
				data_hash = data
		   		$('.page_total small').text(data.list_size);
				$('.loading').hide();
		    	$("#page").initPage(data.list_size, 1, originData);
			},
			error:function(err){
				console.log(err)
				alert("服务器报错")
			}
		})
	})

	function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originData = template('tables',data_hash)
			$('.public_table .exception-list').html(originData)
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            $.ajax({
				url: 'd_fixed_nums.json?page=' + pp,
		    	type: 'get',
				data:{
					
						station_ids:station_id,
						begin_time:data_times[0],
						end_time:data_times[1],
						item_id:item_id
					
				},			   
		    	success:function(data){
		    		$('.loading').hide()
		    		var originData = template('tables',data)
					$('.public_table .exception-list').html(originData)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	
		
	};
	$('.loading').show()
	var data_hash = {};
    var page_state = "init";
	$.ajax({
		type:'get',
		url:'/d_fixed_nums.json',
		success:function(data){
			data_hash = data;
			$('.loading').hide()
			var total = data.list_size;
	        $("#page").initPage(total, 1, originData)
	        $('.page_total span small').text(total)
		},
		error:function(err){
			console.log(err)
		}
	})
	
    $('.putTable').click(function(){
    	$(this).attr('disabled',true)
    	pageName_Data();
		var form=$("<form>");//定义一个form表单
		form.attr("style","display:none");
		form.attr("method","get");
		form.attr("action","/d_fixed_nums.csv");
		var input1=$("<input>");
		input1.attr("type","hidden");
		input1.attr("name","station_ids");
		input1.attr("value",station_id);
		var input2=$("<input>");
		input2.attr("type","hidden");
		input2.attr("name","data_times");
		input2.attr("value",data_times);
		$("body").append(form);//将表单放置在web中
		form.append(input1);
		form.append(input2);
		form.submit();//表单提交 
    })
})
