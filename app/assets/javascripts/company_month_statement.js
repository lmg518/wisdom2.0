$(function(){
	var curDate = new Date();

	var year = curDate.getFullYear();
    var month = curDate.getMonth() + 1;
    var newMonths = month < 10 ? '0' + month : month;
    var inputmonth=year + "-" + newMonths
    var datestamp
    datestamp= year + "-" + newMonths+"-"+"01";//获取当前月

	$('#company_month_statement .public_search #beginMonth').val(inputmonth)

	$('#loading').show()
   $.ajax({
   	type:"get",
   	url:"/company_month_statement/index_table",
   	data:{
				time:datestamp
			},
    success:function(data){
 
	   	$('#company_month_statement #public_table .public_table').html(data)
	   	  $('#loading').hide()
	   },
	   error:function(err){
	   	alert("出错啦！！")
    		console.log(err)
    	}
   });
   
    //查询
   $('#company_month_statement .public_search .search').click(function(){
   	$('#loading').show()
   		var time
   		time=$('#company_month_statement .public_search #beginMonth').val();
   		time=time+"-"+"01"
   		$.ajax({
	   	type:"get",
	   	url:"/company_month_statement/index_table",
	   	data:{
	   		time:time
	   	},
	    success:function(data){
		   	$('#company_month_statement #public_table .public_table').html(data)
		   	  $('#loading').hide()
		   },
		   error:function(err){
		   	alert("出错啦！！")
	    		console.log(err)
	    	}
	   });
   })
})
