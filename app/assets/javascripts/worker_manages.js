$(function(){
	//所属组别
	$.ajax({
		type:'get',
		url:'/worker_manages/get_group_infos.json',
		success:function(data){
			$('.loading').hide()
			var groupTemp = template('groupTemp',data);
			$('#add_form #group .notice_group').html(groupTemp);
//			var groupTemp = template('groupTemp',data);
			$('#edit_form #group .notice_group').html(groupTemp);
		},
		error:function(err){
			console.log(err)
		}
	})
	
	
	
	$(document).delegate('#addModal .modal-footer .btn-default','click',function(){
		$('#add_form .form-group input').val("");
	})
	
	
		$(document).ready(function() {
	    $('#add_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            name: {
	                validators: {
	                    notEmpty: {
	                        message: '姓名不能为空'
	                    }
	                }
	            },
	            telphone: {
                 message: 'The phone is not valid',
                 validators: {
                     notEmpty: {
                         message: '手机号码不能为空'
                     },
                     stringLength: {
                         min: 11,
                         max: 11,
                         message: '请输入11位手机号码'
                     }
                 }
             },
	            email: {
                    validators: {
                        notEmpty: {
                            message: '邮箱不能为空'
                        },
                        emailAddress: {
                            message: '邮箱地址格式有误'
                        }
                    }
                }
	        }
	    });
	});
	//添加提交
	$(document).delegate('#addModal .modal-footer .btn-sure','click',function(){
		var addValidator = $('#add_form').data('bootstrapValidator');
		addValidator.validate();
		var name=$('#add_form .form-group .project_name').val();
		var telphone=$('#add_form .form-group .telphone').val();
		var wee_chat=$('#add_form .form-group .wee_chat').val();
		var qq_num=$('#add_form .form-group .qq_num').val();
		var email=$('#add_form .form-group .email').val();
		var work_status=$('#add_form .form-group #incumbency option:selected').val()
//		var checkId=$('#add_form #group .notice_group  input[type="checkbox"]:checked').val().split(',');
		var checkId=[];
		$('#add_form #group .notice_group  input[type="checkbox"]:checked').each(function(){
			checkId.push($(this).val());
		});
		if(checkId.length==0){
			checkId=[""]
		}
		console.log(checkId)
		if (addValidator.isValid()){
			$.ajax({
				type:"post",
				url:"/worker_manages.json",
				data:{
					d_notice_worker:{
						name:name,
						telphone:telphone,
						wee_chat:wee_chat,
						qq_num:qq_num,
						email:email,
						work_status:work_status
					},
					group_ids:checkId
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#addModal').modal('hide')
   					$('#add_form').data('bootstrapValidator').resetForm(true);
   					
				},
				error:function(err){
					console.log(err)
				}
			});
		}
	})
		//修改
	var id;
	$('body').delegate('#worker_manages table tbody tr td .work_edit','click',function(){
		id=$(this).parents('tr').find('.id').text();
		console.log(id)
		$.ajax({
			type:"get",
			url:'/worker_manages/'+id+'.json',
			success:function(data){
				var name=data.d_worker.name;
				var telphone=data.d_worker.telphone;
				var wee_chat=data.d_worker.wee_chat;
				var qq_num=data.d_worker.qq_num;
				var email=data.d_worker.email;
				var work_status=data.d_worker.work_status;
				$('#editModal .form-group .project_name').val(name);
				$('#editModal .form-group .telphone').val(telphone);
				$('#editModal .form-group .wee_chat').val(wee_chat);
				$('#editModal .form-group .qq_num').val(qq_num);
				$('#editModal .form-group .email').val(email);
				if(work_status=="Y"){
					$("#editModal #incumbency option[value='Y']").attr("selected","selected");
				}else{
					$("#editModal #incumbency option[value='N']").attr("selected","selected");
				}
//				var d_worker_g=data.d_worker_g;
//				$(d_worker_g).each(function(i){
//					console.log(i)
//					var s=$(this)[i].group_name;
//					console.log(s)
//				})
			},
			error:function(err){
				console.log(err)
			}
		});
	})
		$(document).ready(function() {
	    $('#editModal').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            name: {
	                validators: {
	                    notEmpty: {
	                        message: '姓名不能为空'
	                    }
	                }
	            },
	            telphone: {
                 message: 'The phone is not valid',
                 validators: {
                     notEmpty: {
                         message: '手机号码不能为空'
                     },
                     stringLength: {
                         min: 11,
                         max: 11,
                         message: '请输入11位手机号码'
                     }
                 }
             },
	            email: {
                    validators: {
                        notEmpty: {
                            message: '邮箱不能为空'
                        },
                        emailAddress: {
                            message: '邮箱地址格式有误'
                        }
                    }
                }
	        }
	    });
	});
	$(document).delegate('#editModal .modal-footer .btn-sure','click',function(){
		var editValidator = $('#editModal').data('bootstrapValidator');
		editValidator.validate();
		var name=$('#edit_form .form-group .project_name').val();
		var telphone=$('#edit_form .form-group .telphone').val();
		var wee_chat=$('#edit_form .form-group .wee_chat').val();
		var qq_num=$('#edit_form .form-group .qq_num').val();
		var email=$('#edit_form .form-group .email').val();
		var work_status=$('#edit_form .form-group #incumbency option:selected').val()
		var checkId=[];
		$('#edit_form #group .notice_group  input[type="checkbox"]:checked').each(function(i){
			checkId.push($(this).val());
		});
		
		if(checkId.length==0){
			checkId=[""]
		}
		console.log(checkId)
		if (editValidator.isValid()){
			$.ajax({
				type:"put",
				url:'/worker_manages/'+id+'.json',
				data:{
					d_notice_worker:{
						name:name,
						telphone:telphone,
						wee_chat:wee_chat,
						qq_num:qq_num,
						email:email,
						work_status:work_status
					},
					group_ids:checkId
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
   					originData()
   					$('#editModal').modal('hide')
   					
				},
				error:function(err){
					console.log(err)
				}
			});
		}
	})
	//删除
	$("body").delegate("#worker_manages table tbody tr td .work_delete","click",function() {
		$('.delete_prop').hide();
		$(this).parent().children('div').show();	
	})
	$('body').delegate('#worker_manages table tbody tr td .delete_prop .btn-sure','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"delete",
			url:'/worker_manages/'+id+'.json',
			success:function(data){
				page_state = "change";
				originData();
				$('.delete_prop').hide();
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#worker_manages table tbody tr td .delete_prop .btn-cancel','click',function(){
		$('.delete_prop').hide();
	})
		//	分页+初始数据
	function originData(param){
//		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('worker_table',data_hash);
			$('#worker_manages table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/worker_manages.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('worker_table',data);
					$('#worker_manages table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/worker_manages.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	})
})
