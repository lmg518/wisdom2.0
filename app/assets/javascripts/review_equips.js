$(function(){

    //	   初始数据
    function origin(){
        var per=$('#pagesize option:selected').val();
        $.ajax({
            url: '/review_equips.json?page=1&per=' +per,
            type: 'get',
            beforeSend: function () {
                $('#loading').show();
            },
            success:function(data){
                $('#loading').hide()
                var html = template('review_equips',data);
                $('#public_table table').html(html);

                var total = data.review_equips_list;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', Page);
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    }
    origin();

    //通过
    // var plan_audit_params=[];

    $('body').delegate('.btnPass', 'click', function(){
        var editID = $(this).parents('tr').find('.id').text();
        $.ajax({
            type:"put",
            url: "/review_equips/" + editID,
            data: {
                status:"carry_out",
            },
            success:function(data){
                alert("审核通过");
                origin();
            },
            error:function(err){
                alert("审核失败")
            }
        });
    })

    //不通过
    $('body').delegate('.btnUnpass', 'click', function(){
        var editID = $(this).parents('tr').find('.id').text();
        $.ajax({
            type:"put",
            url: "/review_equips/" + editID,
            data: {
                status:"not_carry",
            },
            success:function(data){
                alert("审核不通过");
                origin();
            },
            error:function(err){
                alert("审核失败")
            }
        });
    })


    //查看详情
    $('body').delegate('.watched', 'click', function(){
        var watchModalID = $(this).parent().parent().find('.id').html();
        $.ajax({
            type: "get",
            url: "/handle_equips/" + watchModalID + ".json",
            success: function (data) {
                if(data.handle_equip.status == "待下发"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(0).find('p').html('待下发');
                    $('#showRecord ul li').eq(1).find('p').html('待执行');
                    $('#showRecord ul li').eq(2).find('p').html('待维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                    $('#showRecord #MaintenanceToBePerformed').addClass('hide');
                    $('#showRecord #lookXun').addClass('hide');
                    $('#showRecord #RecordAfterMaintenance').addClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').addClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');
                }else if(data.handle_equip.status == "待执行"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').addClass('hide');
                    $('#showRecord #lookXun').addClass('hide');
                    $('#showRecord #RecordAfterMaintenance').addClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').addClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('待执行');
                    $('#showRecord ul li').eq(2).find('p').html('待维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待维修"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').addClass('hide');
                    $('#showRecord #RecordAfterMaintenance').addClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').addClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('待维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待试车"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').removeClass('hide');
                    $('#showRecord #RecordAfterMaintenance').removeClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').removeClass('hide');
                    $('#showRecord #TrialRunRecord').addClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('已维修');
                    $('#showRecord ul li').eq(3).find('p').html('待试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待审核"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').removeClass('hide');
                    $('#showRecord #RecordAfterMaintenance').removeClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').removeClass('hide');
                    $('#showRecord #TrialRunRecord').removeClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('已维修');
                    $('#showRecord ul li').eq(3).find('p').html('已试车');
                    $('#showRecord ul li').eq(4).find('p').html('待审核');

                }else if(data.handle_equip.status == "完成"){
                    $('#showRecord ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#showRecord ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(4).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#showRecord ul li').eq(5).find('.circle,.lineLeft').addClass('changeColor');
                    $('#showRecord #MaintenanceToBePerformed').removeClass('hide');
                    $('#showRecord #lookXun').removeClass('hide');
                    $('#showRecord #RecordAfterMaintenance').removeClass('hide');
                    $('#showRecord #ReplacementOfSpareParts').removeClass('hide');
                    $('#showRecord #TrialRunRecord').removeClass('hide');

                    $('#showRecord ul li').eq(0).find('p').html('已下发');
                    $('#showRecord ul li').eq(1).find('p').html('已执行');
                    $('#showRecord ul li').eq(2).find('p').html('已维修');
                    $('#showRecord ul li').eq(3).find('p').html('已试车');
                    $('#showRecord ul li').eq(4).find('p').html('已审核');

                }
                SEquipID = data.handle_equip.s_equip_id;
                var result = template('workNews2', data);
                $('#showRecord .MaintenanceIfo').html(result);




                //执行中
                $('#showRecord #prvid5 img').attr('src',data.handle_equip.img_path);
                //检修之前
                $('#showRecord #RecordBeforeMaintenance').val(data.start_note.note);
                $('#showRecord #prvid3 img').attr('src',data.start_note.img_path);
                $('#showRecord #PreServiceRemarks').val(data.start_note.overhaul_desc);
                $('#showRecord #PreTime').val(data.start_note.overhaul_time);
                //检修之后
                $('#showRecord #RecordAfter').val(data.end_note.note);
                $('#showRecord #prvid4 img').attr('src',data.end_note.img_path);
                $('#showRecord #PostOverhaulRemarks').val(data.end_note.overhaul_desc);
                $('#showRecord #AfterTime ').val(data.end_note.overhaul_time);

                //备件信息
                $('#showRecord #ReplacementParts ').val(data.spares.spare_name);
                $('#showRecord #RecordTimeAfterMaintenance').val(data.spares.change_time);

                getBoot();

                //checked默认选中
                data.equip_boots.boots.forEach(function(ele,index,arr){
                    for(var i=0; i < $('#showRecord .checkTI').length; i++){
                        if($('#showRecord .checkTI').eq(i).val() == ele.boot_check_id){
                            $('#showRecord .checkTI').eq(i).attr('checked','true')
                        }
                    }
                })

                //是否合格
                if(data.equip_boots.check_status == 'Y'){
                    $('#TestResults .hege').attr('checked',true)
                }else if(data.equip_boots.check_status == 'N'){
                    $('#TestResults .buhege').attr('checked',true);
                    $('#disqualificationResult').removeClass('hide');
                    $('#disqualificationResult textarea').val(data.equip_boots.exec_desc)
                }

                //是否整理
                if(data.end_note.sort_out == 'Y'){
                    $('#WhetherToArrange .yes').attr('checked',true)
                }else if(data.end_note.sort_out == 'N'){
                    $('#WhetherToArrange .no').attr('checked',true)
                }
            },
            error: function (err) {
                console.log(err);
            }
        })
        $('#d_task_forms').addClass('hide');
        $('#showRecord').removeClass('hide');

    })
    //获取检查项
    function getBoot(){
        $.ajax({
            type: "get",
            url: " /handle_equips/get_boots",
            async: false,
            success: function (data) {
                var result2 = template('TestItem2', data);
                $('#showRecord .TestItem').html(result2);
            },
            error: function (err) {
                console.log(err);
            }
        })
    }
    //查看详情返回
    $('body').delegate('#showRecord #editBack', 'click', function(){
        $('#d_task_forms').removeClass('hide');
        $('#showRecord').addClass('hide');
    })
    //每页显示更改
    $("#pagesize").change(function(){
        $('#loading').show()
        var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/review_equips.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('#loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    var page_state = "init";
    function Page(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;
            $.ajax({
                async: true,
                type: 'get',
                dataType: "json",
                url: '/review_equips.json?page=' + page + '&per=' + $('#pagesize option:selected').val(),
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('review_equips',data);
                    $('#public_table table').html(html);
                    $('.page_total span a').text(page);
                    $('#loading').hide();
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert('请求数据失败,请稍后重试');
                    $('#loading').hide();
                }

            })
        }
    }
})
