$(function(){
	
	//运维单位
	 $.getJSON('/search_region_codes').done(function(data){
		var stationTemp = template('editUnit',data);
		$('#search-unit .choose-site').html(stationTemp);
	})
	 //时间
	 $.getJSON('/ops_audit_manages').done(function(data){
		var selectTime = template('searchTime',data);
		$('#audit_manages .public_search #ops_type').html(selectTime);
	})
	 var dataTime='';
	 function pageName_Data(){
	 	dataTime=$('#ops_type option:selected').val();
	 }
	 $('#audit_manages .public_search .search').click(function(){
	 	dataTime='';
	 	var time=$('#ops_type option:selected').val();
	 	console.log(time)
	 	pageName_Data()
	 	$.ajax({
	 		type:"get",
	 		url:"/ops_audit_manages/search_ops.json?week_time="+time,
	 		data:{
	 			week_time:dataTime
	 		},
	 		success:function(data){
				$('.loading').hide()
	   		 	 var htmlData = template('audit_mangesTable',data);
				$('#audit_manages table .exception-list').html(htmlData);
		   		$('#audit_manages .public_table .edit_status').each(function(i){
					var s=$(this).text()
					if(s=="待审核"){
						$(this).css({ "color": "#fff", "background": "#f29a76" })
		//				$(this).siblings('td').find('input').attr('disabled',true)
					}else{
						$(this).css({ "color": "#fff", "background": "#558ad8" })
						$(this).siblings('td').find('input').attr('disabled','disabled')
					}
				})
		   		 var roles=data.role_name;
		        if(roles=="运维成员"){
		        	$('#btnBatchPass').attr("disabled","disabled")
					$('#btnBatchUnpass').attr("disabled","disabled")
		        }
			},
			error:function(err){
				console.log(err)
			}
	 	});
	 })
//	$('body').delegate('#detilBack','click',function(){
//		$('#audit_manages').show();
//		$('#ops_audit_detail').hide();
//		$('#opsmade').hide();
//	})
	//	全部
	var week_times;
	var edit_status;
$('body').delegate('#ops_audit_detail #whole','click',function(){
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	week_times=$('#ops_audit_detail #weekTime').val()
	edit_status=" ";
	$.ajax({
			type:"get",
			url:"/ops_audit_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status
			},
			success:function(data){
				var weekTime=data.week_time;
				var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
//				$('#plan_edit .navTop .tableName').text(ediytDate);
				var total = data.list_size;
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
})
//已定制
$('body').delegate('#ops_audit_detail #dingz','click',function(){
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	week_times=$('#ops_audit_detail #weekTime').val()
	edit_status=["Y","W"];
	$.ajax({
			type:"get",
			url:"/ops_audit_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status
			},
			success:function(data){
				console.log(data)
				var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
				var total = data.list_size;
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
})
//未定制
$('body').delegate('#ops_audit_detail #wed','click',function(){
	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	$('.loading').show()
	week_times=$('#ops_audit_detail #weekTime').val()
	edit_status=$(this).val();
	$.ajax({
			type:"get",
			url:"/ops_audit_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status
			},
			success:function(data){
				console.log(data)
				var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
				var total = data.list_size;
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
})

function ss(pa){
	week_times=$('#ops_audit_detail #weekTime').val()
	// pageName_Data2()
	$('#ops_audit_detail .public_search .searchUnit').val($('.public_search .unit_ywID').val())
	$('#ops_audit_detail .public_search .searchCity').val($('#ops_audit_detail #city').val())
	unit_ywID = $('#ops_audit_detail .public_search .searchUnit').val();
	cityName=$('#ops_audit_detail .public_search .searchCity').val();
	$.ajax({
			type:"get",
			url:"/ops_audit_manages.json?week_time="+week_times,
			data:{
				edit_status:edit_status,
				page:pa,
				unit_name:unit_ywID,
				zone_name:cityName
			},
			success:function(data){
				console.log(data)
				var total = data.list_size;
				var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$('.loading').hide()
			},
			error:function(err){
				console.log(err)
			}
		});
}
	//通过
	var plan_audit_params=[];
	$('#btnBatchPass').click(function(){
		var W=$(this).val();
		$('#audit_manages table td input[type="checkbox"]:checked').each(function(i){
			var weekTime=$('#audit_manages table td input[type="checkbox"]:checked').parents('tr').find('.week_time').eq(i).text();
			var han=[];
			var handle_stations=$(this).parents('tr').find('.hidden span');
			handle_stations.each(function(i){
				han.push(handle_stations.eq(i).text());
			})
			var param={param:{
				"week_time":weekTime,
				"handle_station":han
			}}
		plan_audit_params.push(param)
		})
		var objName={plan_audit_params,edit_status:W}
		$.ajax({
			type:"get",
			url:"/ops_audit_manages/plan_audit.json",
			data:objName,
			success:function(data){
				alert("提交成功")
				 var htmlData = template('audit_mangesTable',data);
				$('#audit_manages table .exception-list').html(htmlData);
				$('#audit_manages .public_table .edit_status').each(function(i){
					var s=$(this).text()
					if(s=="待审核"){
						$(this).css({ "color": "#fff", "background": "#f29a76" })
		//				$(this).siblings('td').find('input').attr('disabled',true)
					}else{
						$(this).css({ "color": "#fff", "background": "#558ad8" })
						$(this).siblings('td').find('input').attr('disabled','disabled')
					}
				})
			},
			error:function(err){
				alert("提交失败")
			}
		});
	})
	//不通过
	
	$('#btnBatchUnpass').click(function(){
		var Y=$(this).val();
		$('#audit_manages table td input[type="checkbox"]:checked').each(function(i){
			var weekTime=$('#audit_manages table td input[type="checkbox"]:checked').parents('tr').find('.week_time').eq(i).text();
			var han=[];
			var handle_stations=$(this).parents('tr').find('.hidden span');
			handle_stations.each(function(i){
				han.push(handle_stations.eq(i).text());
			})
			var param={param:{
				"week_time":weekTime,
				"handle_station":han
			}}
		plan_audit_params.push(param)
		})
		var objNames={plan_audit_params,edit_status:Y}
		$.ajax({
			type:"get",
			url:"/ops_audit_manages/plan_audit.json",
			data:objNames,
			success:function(data){
				alert("提交成功")
				 var htmlData = template('audit_mangesTable',data);
				$('#audit_manages table .exception-list').html(htmlData);
				$('#audit_manages .public_table .edit_status').each(function(i){
					var s=$(this).text()
					if(s=="待审核"){
						$(this).css({ "color": "#fff", "background": "#f29a76" })
		//				$(this).siblings('td').find('input').attr('disabled',true)
					}else{
						$(this).css({ "color": "#fff", "background": "#558ad8" })
						$(this).siblings('td').find('input').attr('disabled','disabled')
					}
				})
			},
			error:function(err){
				alert("提交失败")
			}
		});
	})
	$('body').delegate('#allChack','click',function(){
//		$('#public_table table').find('.exception-list input').prop('checked',this.checked)
		var allCk = $(this).closest("table").find("input[type='checkbox'][disabled!='disabled']").not("[checkAll]");
        var checked = this.checked;
        allCk.each(function () {
            this.checked = checked;
        });
	});
	
	$('body').delegate('.lookFor','click',function(){
		$('.loading').show()
		$('#ops_audit_detail').show();
		$('#audit_manages').hide();
		var weekTime=$(this).parents('tr').find('.week_time').text();
		var randTime=$(this).parents('tr').find('.randTime').text();
		//	分页+初始数据
		function lookData(param){
		// pageName_Data2()
		var pp = $('#ops_audit_detail #page .pageItemActive').html();
		var per=$('#ops_audit_detail #pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('detail_Table',data_hash);
			$('#ops_audit_detail .public_table .exception-lis').html(htmlData);
			$('#ops_audit_detail .page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/ops_audit_manages.json?week_time='+weekTime+'&page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					unit_name:$('#ops_audit_detail .public_search .searchUnit').val(),
					zone_name:$('#ops_audit_detail .public_search .searchCity').val(),
					edit_status:$('#plan_edit .public_search .searchStatus').val()
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var detailHtml = template('detail_Table',data)
					$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
					var weekTime=data.week_time;
					$('#ops_audit_detail #weekTime').val(weekTime)
					$('#ops_audit_detail #startDate').text(randTime)
					$('#ops_audit_detail .page_total span a').text(pp);
					$('#ops_audit_detail .page_total span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	
	var data_hash = {};
    var page_state = "init";
		$.ajax({
			type:"get",
			url:"/ops_audit_manages.json?week_time="+weekTime,
			success:function(data){
				console.log(data)
				$('.loading').hide()
				var total = data.list_size;
				data_hash=data;
			    $("#ops_audit_detail #page").initPage(total, 1, lookData)
			    $('#ops_audit_detail .page_total span small').text(total)

				var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
				var weekTime=data.week_time;
				$('#ops_audit_detail #weekTime').val(weekTime)
				$('#ops_audit_detail #startDate').text(randTime)
			},
			error:function(err){
				console.log(err)
			}
		});
		
		//	//每页显示更改
    $("#ops_audit_detail #pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/ops_audit_manages.json?week_time='+weekTime+'page=' + $('#ops_audit_detail #page .pageItemActive').html() + '&per=' + $('#ops_audit_detail #pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#ops_audit_detail #page").initPage(total, 1, lookData)
                $('#ops_audit_detail .page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
		
	})

	//详细——查询
	var unit_ywID = '';
	var cityName='';
	function pageName_Data2(){
		$('#ops_audit_detail .public_search .searchUnit').val($('.public_search .unit_ywID').val())
		$('#ops_audit_detail .public_search .searchCity').val($('#ops_audit_detail #city').val())
		unit_ywID = $('#ops_audit_detail .public_search .searchUnit').val();
		cityName=$('#ops_audit_detail .public_search .searchCity').val();
		console.log(cityName)
		edit_status=$('#plan_edit .public_search .searchStatus').val();
	}
	$('#ops_audit_detail .public_search .search').click(function(){
		$('#ops_audit_detail #whole').css({ "color": "#fff", "background": "#558ad8" })
			$('#ops_audit_detail #dingz').css({ "color": "#333", "background": "#fff" })
			$('#ops_audit_detail #wed').css({ "color": "#333", "background": "#fff" })
		unit_ywID = '';
		cityName='';
		pageName_Data2()
		var week_times=$('#ops_audit_detail #weekTime').val()
		$.ajax({
			type:"get",
			url:"/ops_audit_manages.json?week_time="+week_times,
			data:{
				unit_name:unit_ywID,
				zone_name:cityName,
				edit_status:edit_status
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	var detailHtml = template('detail_Table',data)
				$('#ops_audit_detail .public_table .exception-list').html(detailHtml)
				var total = data.list_size;
				var pp = $('#page .pageItemActive').html();
				$('.page_total span a').text(pp);
				$('.page_total span small').text(total)
				$("#page").initPage(total, 1, ss)
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	
	
	 var id;
	 $('body').delegate('#detailsTable .exception-list tr','click',function(){
	 	id=$(this).children('.id').text();
	 	console.log(id)
	 	$('#opsmade').show();
	 	$('#ops_audit_detail').hide();
		$('#audit_manages').hide();
		$.ajax({
			type:"get",
			url:"/ops_audit_manages/"+id+".json",
			success:function(data){
				var w=data.week_pro.length;
				var m=data.month_pro.length;
				var j=data.jd_pro.length;
				var y=data.year_pro.length;
				var dataObj={
					editData:data
				}
				var madHtml = template('madeTable',dataObj)
				$('#madeTables  .exception-list').html(madHtml)
				formDate()
				$('#opsmade #madeTables .week_pro').each(function(i){
					$('#opsmade #madeTables .week_pro').find('td').eq(0).attr("rowspan",w)
					$('#opsmade #madeTables .week_pro').eq(i+1).find('td').eq(0).remove();
				})
				$('#opsmade #madeTables .month_pro').each(function(i){
					$('#opsmade #madeTables .month_pro').find('td').eq(0).attr("rowspan",m)
					$('#opsmade #madeTables .month_pro').eq(i+1).find('td').eq(0).remove();
				})
				$('#opsmade #madeTables .year_pro').each(function(i){
					$('#opsmade #madeTables .year_pro').find('td').eq(0).attr("rowspan",y)
					$('#opsmade #madeTables .year_pro').eq(i+1).find('td').eq(0).remove();
				})
				$('#opsmade #madeTables .jd_pro').each(function(i){
					$('#opsmade #madeTables .jd_pro').find('td').eq(0).attr("rowspan",j)
					$('#opsmade #madeTables .jd_pro').eq(i+1).find('td').eq(0).remove();
				})
				$('#opsmade #madeTables .week_time').each(function(i){
					var weekTimes=$(this).text();
					var jobTime=$(this).parents('tr').find('td .job_time').text();
					if(weekTimes==jobTime){
						$(this).parents('.check-item').addClass('yChack')
					}
				})
			},
			error:function(err){
				console.log(err)
			}
		});
	 })
	 $('body').delegate('#madeBback','click',function(){
		$('#audit_manages').hide();
		$('#ops_audit_detail').show();
		$('#opsmade').hide();
	})

	
	$.getJSON('/ops_audit_manages.json').done(function(data){
		console.log(data)
		$('.loading').hide()
	    var htmlData = template('audit_mangesTable',data);
		$('#audit_manages table .exception-list').html(htmlData);
	    $('#audit_manages .public_table .edit_status').each(function(i){
			var s=$(this).text()
			if(s=="待审核"){
				$(this).css({ "color": "#fff", "background": "#f29a76" })
//				$(this).siblings('td').find('input').attr('disabled',true)
			}else{
				$(this).css({ "color": "#fff", "background": "#558ad8" })
				$(this).siblings('td').find('input').attr('disabled','disabled')
			}
		})
	    var roles=data.role_name;
        if(roles=="运维成员"){
        	$('#btnBatchPass').attr("disabled","disabled")
			$('#btnBatchUnpass').attr("disabled","disabled")
        }
	})
	 

function formDate(){
		var now = new Date();
		var nowTime = now.getTime() ; 
		var day = now.getDay();
		var oneDayLong = 24*60*60*1000 ; 
		var MondayTime = nowTime - (day-1)*oneDayLong  ; 
		var SundayTime =  nowTime + (7-day)*oneDayLong ; 

		var monday = new Date(MondayTime);
		var sunday = new Date(SundayTime);
		
		var myMonth=monday.getMonth() + 1;
		var myDate=monday.getDate();
		var myMonths=sunday.getMonth() + 1;
		var myDates=sunday.getDate();
		
		var newMonth=myMonth<10?'0'+myMonth:myMonth;
		var newDate=myDate<10?'0'+myDate:myDate;
		var newMonths=myMonths<10?'0'+myMonths:myMonths;
		var newDates=myDates<10?'0'+myDates:myDates;
		var m=monday.getFullYear() + '-' + newMonth + '-' + newDate;
		var s=sunday.getFullYear() + '-' + newMonths + '-' + newDates;
		
		
		$('#ops_audit_detail .public_search #startDate').text(m)
		$('#ops_audit_detail .public_search #endDate').text(s)
	}
	formDate();
})
