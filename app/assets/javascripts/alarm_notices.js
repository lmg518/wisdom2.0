$(function(){
	$('body').delegate('.add_factor h4 .right','click',function(){
		$('.editRules').hide();
		$('#public_box').hide();
	});
	$('body').delegate('.add_factor .area_btn .btn-cancel','click',function(){
		$('#public_box').hide();
		$('.editRules').hide();
		$('.editRules').find('input').val('');
	});
//	模态框隐藏
	$('body').delegate('.notice_rules .close','click',function(){
		$('#public_box').hide();
	})
	$('body').delegate('.notice_rules .notice_btn .btn-cancel','click',function(){
		$('#public_box').hide();
	})
	
	var time = new Date();
	var year = time.getFullYear();
	var myMonth = time.getMonth()+1;
	var myDate = time.getDate();
	var result = year+'-'+(myMonth<10?'0'+myMonth:myMonth)+'-'+(myDate<10?'0'+myDate:myDate)
	function plicBox(){
	//  报警级别
	    $.getJSON('/s_alarm_levels.json').done(function(data){
	    	var htmlStr=template('notice_leval',data);
	    	$('#alarm_notices .public_search .notice_leval').html(htmlStr)
	    })
	//  状态
//		$.ajax({
//			type:'get',
//			url:'/search_by_status.json',
//			success:function(data){
//				var htmlStr=template('alarmStatus',data);
//	    		$('#alarm_notices .public_search .status').html(htmlStr)
//			},
//			error:function(err){
//				console.log(err)
//			}
//		})
    }
	plicBox();
//	查询
	var infor_name;
	var alarm_level;
	var datetime;
	var status;
	
	function pageName_Data(){
		infor_name = $('#alarm_notices .public_search input').eq(0).val(); //通知人
		alarm_level = $('#alarm_notices .public_search .notice_leval option:selected').val();
		datetime = $('#alarm_notices .public_search input').eq(1).val();
//		status = $('#alarm_notices .public_search .status option:selected').val();//状态
		
	}
	$('#alarm_notices .public_search  .search').click(function(){
		$('.loading').show()
		infor_name = $('#alarm_notices .public_search input').eq(0).val(); //通知人
		alarm_level = $('#alarm_notices .public_search .notice_leval option:selected').val();
		datetime = $('#alarm_notices .public_search input').eq(1).val();
//		状态
//		status = $('#alarm_notices .public_search .status option:selected').val();
		$.ajax({
			type:'get',
			url:'/alarm_notices.json',
			data:{
				search:{
					name:infor_name,
					alarm_level:alarm_level,
					d_time:datetime,
//					状态
//					status:status 
				}
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData)
				$('.page_total small').text(data.list_size);
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	//	编辑
	var editID;
	$('body').delegate('#alarm_notices .public_table .edit','click',function(){
		editID = $(this).siblings('.id').text();
		$('#public_box').show();
		$('#alarm_notices .editRules').show();
		var data={};
		var htmlStr=template('notice_rules',data);
		$('#alarm_notices .editRules').html(htmlStr);
		plicBox();
		$.ajax({
			type:'get',
			url:'/alarm_notices/'+editID+'.json',
			dataType:'json',
			success:function(data){
				originData();
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#alarm_notices .notice_rules1 .notice_btn .btn-sure','click',function(){
		var alarm_level = $('#alarm_notices .notice_rules1 .alarm_level select option:selected').text();  //报警级别
		var alarm_level_id = $('#alarm_notices .notice_rules1 .alarm_level select option:selected').val();  //报警级别
		var alarm_inform = $('#alarm_notices .notice_rules1 .alarm_inform input').val();  //通知方式
		var alarm_config = $('#alarm_notices .notice_rules1 .alarm_config select option:selected').text();  //角色配置
		var alarm_config_id = $('#alarm_notices .notice_rules1 .alarm_config select option:selected').val();  //角色配置
		var alarm_belong = $('#alarm_notices .notice_rules1 .alarm_belong select option:selected').text();  //归属层级
		var alarm_belong_id = $('#alarm_notices .notice_rules1 .alarm_belong select option:selected').val();  //归属层级
		
		$.ajax({
			type:'put',
			url:'/alarm_notices/'+editID+'.json',
			dataType:'json',
			data:{
				alarm_notice:{
					alarm_level:alarm_level_id,
					notice_type:alarm_inform,
					role_id:alarm_config_id,
					up_region_level:alarm_belong_id
				}
			},
			success:function(data){
				originData();
			},
			error:function(err){
				console.log(err)
			}
		})
		
	})
	
	
	//	分页+初始数据
	function originData(param){
		pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var originData = template('originData',data_hash)
			$('.public_table table tbody').html(originData)
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/alarm_notices.json?page=' + pp + '&per=' +per ,
		    	type: 'get',
			    data:{
					search:{
						name:infor_name,
						alarm_level:alarm_level,
						d_time:datetime,
						status:status
					}
				},
		    	success:function(data){
		    		
		    		$('.loading').hide()
		    		var originData = template('originData',data)
					$('.public_table table tbody').html(originData)
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	$('.loading').show();
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/alarm_notices.json').done(function(data){
		$('.loading').hide();
		var total = data.list_size;
		data_hash = data;
	    $("#page").initPage(total, 1, originData);
	    $('.page_total span small').text(total);
	})
	
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/alarm_notices.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	
	
})