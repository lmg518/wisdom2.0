$(function(){
    //下发人员
    $('body').delegate('.handle_width .issue', 'click',function(){
    	var region_id = $(this).parents('tr').find('.region_id').text();
        $.ajax({
            url: '/handle_equips/get_login',
            type: 'get',
            beforeSend: function () {
                $('#loading').show();
            },
            data:{
            	region_id:region_id,
            	
            },
            success:function(data){
                $('#loading').hide();
                var html = template('sendPerson',data);
                $('#issueModal #send_per').html(html);
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    })

    //创建时间
    $('.createStart').datetimepicker({
        format: 'yyyy-mm-dd hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 2,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setEndDate:$('.day-end').val(),
    }).on('change',function(ev){
        var startDate = $('.createStart').val();
        $(".createEnd").datetimepicker('setStartDate',startDate);
        $(".createStart").datetimepicker('hide');
    });
    $('.createEnd').datetimepicker({
        format: 'yyyy-mm-dd hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 2,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setStartDate:$(".day-start").val(),
    }).on('change',function(ev){
        var endDate = $('.createEnd').val();
        $(".createStart").datetimepicker('setEndDate',endDate);
        $(".createEnd").datetimepicker('hide');
    });

    //时间
    $('.dayStart').datetimepicker({
         format: 'yyyy-mm-dd hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 2,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setEndDate:$('.day-end').val(),
    }).on('change',function(ev){
        var startDate = $('.dayStart').val();
        $(".dayEnd").datetimepicker('setStartDate',startDate);
        $(".dayStart").datetimepicker('hide');
    });
    $('.dayEnd').datetimepicker({
        format: 'yyyy-mm-dd hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 2,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setStartDate:$(".day-start").val(),
    }).on('change',function(ev){
        var endDate = $('.dayEnd').val();
        $(".dayStart").datetimepicker('setEndDate',endDate);
        $(".dayEnd").datetimepicker('hide');
    });
    $('#MaintenanceEditor .datetime').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',  
         weekStart: 1,  
         autoclose: true,  
         startView: 2,
         minView: 0,  
         forceParse: false,  
         language: 'zh-CN' ,
    }).on('change',function(ev){
        var startDate = $('#MaintenanceEditor .datetime').val();
        // $(".dayEnd").datetimepicker('setStartDate',startDate);
        $("#MaintenanceEditor .datetime").datetimepicker('hide');
    });


    //查询功能
    $('body').delegate('#deviceName1', 'click', function(){
        if($('.navMain #workshop').val() == ''){
            alert("请先选择车间")
        }
    });
    $('body').delegate(".search", 'click', function(){
        var per=$('#pagesize option:selected').val();
        $.ajax({
            type: "get",
            url: "/handle_equips.json",
            data: {
                equip_ids: $('.navMain #deviceName1').val(),
                begin_time: $('.navMain .createStart').val(),
                end_time: $('.navMain .createEnd').val(),
                status: $('.navMain #CheckTheState').val(),
              
            },
            beforeSend: function () {
                $('#loading').show();
            },
            success: function(data){
            	
                $('#loading').hide()
                var html = template('handle_equips',data);
                $('#public_table table').html(html);

                var total = data.handle_equips_list;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', searchPage);
        
                var roles=data.role_code
                    Roles(roles)
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    //添加车间设备
    $('body').delegate('.navMain #workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/s_equips/get_equips",
            data:{
                region_ids:$(this).val()
            },
            success:function(data){
                var result = template('devicename',data);
               
                $('.navMain #deviceName1').html(result);
                // $('#addModal').modal('hide');
            },
            error:function(err){
                console.log(err)
            }
        });
    })

    //添加车间设备
    $('body').delegate('#addModal #workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/s_equips/get_equips",
            data:{
                region_ids:$(this).val()
            },
            success:function(data){
                var result = template('devicenameadd',data);
                $('#addModal .DeviceName').html(result);
                // $('#addModal').modal('hide');
            },
            error:function(err){
                console.log(err)
            }
        });
    })

    //	   初始数据
    function origin(){
        var per=$('#pagesize option:selected').val();
        $.ajax({
            url: '/handle_equips.json?page=1&per=' +per,
            type: 'get',
            beforeSend: function () {
                $('#loading').show();
            },
            success:function(data){
                $('#loading').hide()
                var html = template('handle_equips',data);
                $('#public_table table').html(html);

                var total = data.handle_equips_list;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', Page);
                $('#loading').hide();
                var roles=data.role_code
                    Roles(roles)

            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    }
    origin();

    // 添加维修
    function add_create(){
        data = {
            s_region_code_id: $('#addModal #workshop').val(),
            region_name: $('#addModal #workshop').find("option:selected").text(),
            s_equip_id: $('#addModal .DeviceName').val(),
            equip_name: $('#addModal .DeviceName').find("option:selected").text(),
            equip_code: $('#addModal .DeviceName').find("option:selected").attr('value1'),
            handle_start_time: $('#addModal .dayStart').val(),
            handle_end_time: $('#addModal .dayEnd').val(),
            snag_desc: $('#addModal .reconditionReason').val(),
            desc: $('#addModal .reconditionDesc').val(),
        }
        $.ajax({
            type:'post',
            url:'/handle_equips',
            data:{
                handle_equip: data
            },
            success:function(data){
              
                if(data.status == 200){
                    alert("添加成功");
                }else{
                    alert("添加失败")
                }
                $('#addModal').modal('hide')
                document.getElementById("add_station_form").reset()
//              $('#addModal').addClass('hide');
                origin();
            },
            error:function(err){
                console.log(err)
            }
        })
    }
    $('body').delegate('#addModal .btn-sure', 'click', function(){
        add_create();
    })
//  删除

var id;
var name;
$("body").delegate("table tbody .delete","click",function() {
	id=Number($(this).parents('tr').find('.id').text())
	name=$(this).parents('tr').find('.equip_name').text()
	$('#sureCannel').show();
	var sure_str = '<div class="point-out">确认删除 <span>"' + name + '"</span>  吗？</div>';
    $('#sureCannel .sureCannel-body').html(sure_str);

    var sureCannelFoot_str = '';
   
    sureCannelFoot_str += '<button class="btn-sure" id="handle_delete">确认</button><button onclick="resetPswd_cannel()" class="btn-cannel">取消</button>'

    $('#sureCannel .box-footer').html(sureCannelFoot_str);
})
$("body").delegate("#handle_delete","click",function() {
	$('#sureCannel').hide();
	$.ajax({
			type:"delete",
			url:'/handle_equips/' + id,
			success:function(data){
				
				 $("#success").show().delay(3000).hide(300);
	            var successFoot_str = '';
	            successFoot_str += '<div class="resetPs1"><span class="resetPs1-span">" ' + name + ' "</span>，删除成功！</div>'
	
	            $('.success-footer').html(successFoot_str)
				origin();
			
			},
			error:function(err){
				console.log(err)
			}
		})

})

    //show页面获取参数
    function ShowModal(editID){
        $.ajax({
            type: "get",
            url: "/handle_equips/" + editID + ".json",

            success: function (data) {
                var result = template('workNews', data);
                $('#MaintenanceEditor .MaintenanceIfo').html(result);
                // if(data.handle_equip.status == "待下发"){
                //     $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                //     $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                //     $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                //     $('#MaintenanceEditor ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                //     $('#MaintenanceEditor ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                //     $('#MaintenanceEditor ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                // }else
                if(data.handle_equip.status == "待执行"){
                    $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor #lookXun').addClass('hide');
                    $('#MaintenanceEditor #RecordAfterMaintenance').addClass('hide');
                    $('#MaintenanceEditor #ReplacementOfSpareParts').addClass('hide');
                    $('#MaintenanceEditor #TrialRunRecord').addClass('hide');
                    $('#MaintenanceEditor #TrialRunRecord').addClass('hide');

                    //执行按钮清除
                    $('#MaintenanceEditor #MaintenanceToBePerformed input').removeClass('hide');
                    $('#MaintenanceEditor #PerformedSave').removeClass('hide');
                    // $('#MaintenanceEditor #lookXun').find('input,textarea').attr('disabled','false');
                    // $('#MaintenanceEditor #RecordAfterMaintenance').find('input,textarea').attr('disabled','false');
                    // $('#MaintenanceEditor #ReplacementOfSpareParts').find('input,textarea').attr('disabled','false');
                    // $('#MaintenanceEditor').find('.files').removeClass('hide');
                    // $('#MaintenanceEditor #OverhaulSave').removeClass('hide');

                    $('#MaintenanceEditor #MaintenanceToBePerformed').find('img').attr('src','');
                }else if(data.handle_equip.status == "待维修"){
                    $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor #lookXun').removeClass('hide');
                    $('#MaintenanceEditor #RecordAfterMaintenance').removeClass('hide');
                    $('#MaintenanceEditor #ReplacementOfSpareParts').removeClass('hide');
                    $('#MaintenanceEditor #TrialRunRecord').addClass('hide');

                    $('#MaintenanceEditor #PerformedSave').addClass('hide');
                    $('#MaintenanceEditor ul li').eq(1).find('p').html('已执行');
                    $('#MaintenanceEditor ul li').eq(2).find('p').html('待维修');
                    $('#MaintenanceEditor ul li').eq(3).find('p').html('待试车');
                    $('#MaintenanceEditor ul li').eq(4).find('p').html('待审核');
                    //隐藏执行上传按钮
                    //执行按钮清除


                    // $('#MaintenanceEditor #PerformedSave').addClass('hide');
                    $('#MaintenanceEditor #lookXun').find('input,textarea').removeAttr('disabled');

                    $('#MaintenanceEditor #ReplacementOfSpareParts').find('input,textarea').removeAttr('disabled');

                    $('#MaintenanceEditor #RecordAfterMaintenance').find('input,textarea').removeAttr('disabled');

                    $('#MaintenanceEditor').find('.files').removeClass('hide');
                    $('#MaintenanceEditor #OverhaulSave').removeClass('hide');

//添加隐藏
                    $('#MaintenanceEditor #MaintenanceToBePerformed input').addClass('hide');
                    $('#MaintenanceEditor #PerformedSave').addClass('hide');
                    $('#MaintenanceEditor #lookXun').find('img').attr('src','');
                    $('#MaintenanceEditor #RecordAfterMaintenance').find('img').attr('src','');

                }else if(data.handle_equip.status == "待试车"){
                    $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor #lookXun').removeClass('hide');
                    $('#MaintenanceEditor #RecordAfterMaintenance').removeClass('hide');
                    $('#MaintenanceEditor #ReplacementOfSpareParts').removeClass('hide');
                    $('#MaintenanceEditor #TrialRunRecord').removeClass('hide');

                    $('#MaintenanceEditor #PerformedSave').addClass('hide');
                    $('#MaintenanceEditor ul li').eq(1).find('p').html('已执行');
                    $('#MaintenanceEditor #OverhaulSave').addClass('hide');
                    $('#MaintenanceEditor ul li').eq(2).find('p').html('已维修');
                    $('#MaintenanceEditor ul li').eq(3).find('p').html('待试车');
                    $('#MaintenanceEditor ul li').eq(4).find('p').html('待审核');

                    //隐藏图片上传按钮
                    $('#MaintenanceEditor').find('.files').removeClass('hide');
                    $('#MaintenanceEditor #OverhaulSave').addClass('hide')
                    $('#MaintenanceEditor').find('.files').addClass('hide');
                    //input,textarea全部禁用
                    $('#MaintenanceEditor #lookXun').find('input,textarea').attr('disabled','true');
                    $('#MaintenanceEditor #RecordAfterMaintenance').find('input,textarea').attr('disabled','true');
                    $('#MaintenanceEditor #ReplacementOfSpareParts').find('input,textarea').attr('disabled','true');
                    $('#MaintenanceEditor #MaintenanceToBePerformed input').addClass('hide');
                    $('#MaintenanceEditor #PerformedSave').addClass('hide');
                    ;
                }
                else if(data.handle_equip.status == "待审核"){
                    $('#MaintenanceEditor ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(4).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#MaintenanceEditor ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#MaintenanceEditor #lookXun').removeClass('hide');
                    $('#MaintenanceEditor #RecordAfterMaintenance').removeClass('hide');
                    $('#MaintenanceEditor #ReplacementOfSpareParts').removeClass('hide');
                    $('#MaintenanceEditor #TrialRunRecord').removeClass('hide');

                    $('#MaintenanceEditor #PerformedSave').addClass('hide');
                    $('#MaintenanceEditor ul li').eq(1).find('p').html('已执行');
                    $('#MaintenanceEditor #OverhaulSave').addClass('hide');
                    $('#MaintenanceEditor ul li').eq(2).find('p').html('已维修');
                    $('#MaintenanceEditor #BootSave').addClass('hide');
                    $('#MaintenanceEditor ul li').eq(3).find('p').html('已试车');
                }





                SEquipID = data.handle_equip.s_equip_id;
                //执行中
                $('#MaintenanceEditor #prvid1 img').attr('src',data.handle_equip.img_path);
                //检修之前
                $('#MaintenanceEditor #RecordBeforeMaintenance').val(data.start_note.note);
                $('#MaintenanceEditor #prvid img').attr('src',data.start_note.img_path);
                $('#MaintenanceEditor #PreServiceRemarks').val(data.start_note.overhaul_desc);
                $('#MaintenanceEditor #PreTime').val(data.start_note.overhaul_time);
                //检修之后
                $('#MaintenanceEditor #RecordAfter').val(data.end_note.note);
                $('#MaintenanceEditor #prvid2 img').attr('src',data.end_note.img_path);
                $('#MaintenanceEditor #PostOverhaulRemarks').val(data.end_note.overhaul_desc);
                $('#MaintenanceEditor #AfterTime ').val(data.end_note.overhaul_time);

                //备件信息
                $('#MaintenanceEditor #ReplacementParts ').val(data.spares.spare_name);
                $('#MaintenanceEditor #RecordTimeAfterMaintenance').val(data.spares.change_time);

                getBoot();
                //是否整理
                if(data.end_note.sort_out == 'Y'){
                    $('#WhetherToArrange .yes').attr('checked',true)
                }else if(data.end_note.sort_out == 'N'){
                    $('#WhetherToArrange .no').attr('checked',true)
                }
            },
            error: function (err) {
                console.log(err);
            }
        })
    }
    // 修改维修 在还下发之后的时候才有此权限
    var editID,SEquipID;
    $('body').delegate('.editModal', 'click', function () {
        editID = $(this).parents('tr').find('.id').text();
        ShowModal(editID);
        $('#d_task_forms').addClass('hide');
        $('#MaintenanceEditor').removeClass('hide');
    })
    $('body').delegate('#MaintenanceEditor #editBack', 'click', function(){
        $('#MaintenanceEditor').addClass('hide');
        $('#d_task_forms').removeClass('hide');
    })

    //修改维修保存
    $('body').delegate('#MaintenanceEditor #PerformedSave', 'click', function(){
    	var image_file=$('#MaintenanceEditor #prvid1 img').attr('src')
  
    	if(image_file == undefined || image_file ==""){
    		alert("请选择图片")
  	}
else{
    		$.ajax({
	            type: 'post',
	            url: '/handle_equips/carried_out?id=' + editID,
	            data:{
	                //执行中
	                image_file:image_file,
	                status: "tamp_save",
	            },
	            success:function(data){
	                
	                if(data.status == 200){
	                    alert("提交成功");
	                }else{
	                    alert("提交失败")
	                }
	                ShowModal(editID);
	            },
	            error:function(err){
	                console.log(err)
	            }
	        })
    	}
        
    })

    $('body').delegate('#MaintenanceEditor #OverhaulSave', 'click', function(){
        $.ajax({
            type: 'post',
            url: '/handle_equips/carried_out?id=' + editID,
            data:{
                //维修
                start_note: $('#MaintenanceEditor #RecordBeforeMaintenance').val(),
                start_image_file: $('#MaintenanceEditor #prvid img').attr('src'),
                start_note_type: "overhaul_begin",
                start_overhaul_desc: $('#MaintenanceEditor #PreServiceRemarks').val(),
                start_overhaul_time: $('#MaintenanceEditor #PreTime').val(),
                status: 'boot_save',
                //检修后数据记录
                end_note: $('#MaintenanceEditor #RecordAfter').val(),
                end_image_file: $('#MaintenanceEditor #prvid2 img').attr('src') ,
                end_note_type: "overhaul_end",
                end_overhaul_desc: $('#MaintenanceEditor #PostOverhaulRemarks').val(),
                end_overhaul_time: $('#MaintenanceEditor #AfterTime').val(),
                end_sort_out: $('#MaintenanceEditor #WhetherToArrange input:radio:checked').val(),
                //更换备件数据
                spare_name: $('#MaintenanceEditor #ReplacementParts').val(),
                change_time: $('#MaintenanceEditor #RecordTimeAfterMaintenance').val(),
            },
            success:function(data){
                
                if(data.status == 200){
                    alert("提交成功");
                }else{
                    alert("提交失败")
                }
                ShowModal(editID);
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    $('body').delegate('#MaintenanceEditor #BootSave', 'click', function(){
        var bootIdArr=[];
        //试车检查项数组
        for (var i = 0; i < $("#MaintenanceEditor .TestItem input[type='checkbox']").length; i++) {
            if ($("#MaintenanceEditor .TestItem .checkTI").eq(i).is(':checked')) {
                var data = $(".checkTI").eq(i).val();
                bootIdArr.push(data);
            }
        }
        $.ajax({
            type: 'post',
            url: '/handle_equips/carried_out?id=' + editID,
            data:{
                boot_ids: bootIdArr,
                check_status: $('#MaintenanceEditor #TestResults input:radio:checked').val(),
                exec_desc: $('#MaintenanceEditor #DisqualificationResult').val(),
                Handle_time: $('#MaintenanceEditor #TestCompletionTime').val(),
                status: "wait_review",
            },
            success:function(data){
               
                if(data.status == 200){
                    alert("提交成功");
                }else{
                    alert("提交失败")
                }
                ShowModal(editID);

                $('#MaintenanceEditor').addClass('hide');
                $('#d_task_forms').removeClass('hide');
                origin();
            },
            error:function(err){
                console.log(err)
            }
        })
    })

    //修改维修完成
    $('body').delegate('#MaintenanceEditor #editDone', 'click', function(){
        var bootIdArr;
        //试车检查项数组
        for (var i = 0; i < $("#MaintenanceEditor .TestItem input[type='checkbox']").length; i++) {
            if ($(".checkTI").eq(i).is(':checked')) {
                var data = $(".checkTI").eq(i).val();
                bootIdArr.push(data);
            }
        }
        //图片路径
        $.ajax({
            type: 'post',
            url: '/handle_equips/carried_out?id=' + editID,
            data:{
                //执行中
                image_file:  $('#MaintenanceEditor #prvid1s img').attr('src'),
                //检修前记录数据
                start_note: $('#MaintenanceEditor #RecordBeforeMaintenance').val(),
                start_image_file: $('#MaintenanceEditor #prvid img').attr('src'),
                start_note_type: "overhaul_begin",
                start_overhaul_desc: $('#MaintenanceEditor #PreServiceRemarks').val(),
                start_overhaul_time: $('#MaintenanceEditor #PreTime').val(),
                //检修后数据记录
                end_note: $('#MaintenanceEditor #end_note').val(),
                end_image_file: $('#MaintenanceEditor #prvid2 img').attr('src') ,
                end_note_type: "overhaul_end",
                end_overhaul_desc: $('#MaintenanceEditor #PostOverhaulRemarks').val(),
                end_overhaul_time: $('#MaintenanceEditor #PostOverhaulRemarks').val(),
                end_sort_out: $('#MaintenanceEditor #WhetherToArrange input:radio:checked').val(),
                //更换备件数据
                spare_name: $('#MaintenanceEditor #ReplacementParts').val(),
                change_time: $('#MaintenanceEditor #RecordTimeAfterMaintenance').val(),
                //试车记录数据
                boot_ids: bootIdArr,
                check_status: $('#MaintenanceEditor #TestResults input:radio:checked').val(),
                exec_desc: $('#MaintenanceEditor #DisqualificationResult').val(),
                Handle_time: $('#MaintenanceEditor #TestCompletionTime').val(),

                status: "wait_review",
            },
            success:function(data){
                
                if(data.status == 200){
                    alert("编辑成功");
                }else{
                    alert("编辑失败")
                }
            },
            error:function(err){
                console.log(err)
            }
        })
    })

    //获取试车检查项
    function getBoot(){
        $.ajax({
            type: "get",
            url: " /handle_equips/get_boots",
            async: false,
            success: function (data) {
                var result = template('TestItem', data);
                $('#MaintenanceEditor .TestItem').html(result);

                var result2 = template('TestItem2', data);
                $('#DetailsMaintenance .TestItem').html(result2);
            },
            error: function (err) {
                console.log(err);
            }
        })
    }

    //获取工单ID
    var workOrderID;
    $('body').delegate('.issue', 'click', function(){
        workOrderID = $(this).parents('tr').find('.id').text();
    })
    //下发操作
    $('body').delegate('#issueModal #paiSave', 'click', function(){
        $.ajax({
            type:'post',
            url:'/handle_equips/issued',
            data:{
                id: workOrderID,
                handle_name: $('#issueModal input:radio:checked').next().html(),
            },
            success:function(data){
               
                origin();
                $('#issueModal').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    })

    //不合格隐藏项
    $('body').delegate('.buhege', 'click', function(){
        $('.disqualificationResult').removeClass('hide')
    })
    $('body').delegate('.hege', 'click', function(){
        $('.disqualificationResult').addClass('hide')
    })

    var watchModalID;
    //查看详情
    $('body').delegate('.watchModal', 'click', function(){
        watchModalID = $(this).parents('tr').find('.id').text();
        $.ajax({
            type: "get",
            url: "/handle_equips/" + watchModalID + ".json",
            success: function (data) {
                if(data.handle_equip.status == "待下发"){
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance #MaintenanceToBePerformed').addClass('hide');
                    $('#DetailsMaintenance #lookXun').addClass('hide');
                    $('#DetailsMaintenance #RecordAfterMaintenance').addClass('hide');
                    $('#DetailsMaintenance #ReplacementOfSpareParts').addClass('hide');
                    $('#DetailsMaintenance #TrialRunRecord').addClass('hide');
                    $('#DetailsMaintenance ul li').eq(0).find('p').html('待下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('待执行');
                    $('#DetailsMaintenance ul li').eq(2).find('p').html('待维修');
                    $('#DetailsMaintenance ul li').eq(3).find('p').html('待试车');
                    $('#DetailsMaintenance ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待执行"){
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance #MaintenanceToBePerformed').addClass('hide');
                    $('#DetailsMaintenance #lookXun').addClass('hide');
                    $('#DetailsMaintenance #RecordAfterMaintenance').addClass('hide');
                    $('#DetailsMaintenance #ReplacementOfSpareParts').addClass('hide');
                    $('#DetailsMaintenance #TrialRunRecord').addClass('hide');

                    $('#DetailsMaintenance ul li').eq(0).find('p').html('已下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('待执行');
                    $('#DetailsMaintenance ul li').eq(2).find('p').html('待维修');
                    $('#DetailsMaintenance ul li').eq(3).find('p').html('待试车');
                    $('#DetailsMaintenance ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待维修"){
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance #MaintenanceToBePerformed').removeClass('hide');
                    $('#DetailsMaintenance #lookXun').addClass('hide');
                    $('#DetailsMaintenance #RecordAfterMaintenance').addClass('hide');
                    $('#DetailsMaintenance #ReplacementOfSpareParts').addClass('hide');
                    $('#DetailsMaintenance #TrialRunRecord').addClass('hide');

                    $('#DetailsMaintenance ul li').eq(0).find('p').html('已下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('已执行');
                    $('#DetailsMaintenance ul li').eq(2).find('p').html('待维修');
                    $('#DetailsMaintenance ul li').eq(3).find('p').html('待试车');
                    $('#DetailsMaintenance ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待试车"){
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(3).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance #MaintenanceToBePerformed').removeClass('hide');
                    $('#DetailsMaintenance #lookXun').removeClass('hide');
                    $('#DetailsMaintenance #RecordAfterMaintenance').removeClass('hide');
                    $('#DetailsMaintenance #ReplacementOfSpareParts').removeClass('hide');
                    $('#DetailsMaintenance #TrialRunRecord').addClass('hide');

                    $('#DetailsMaintenance ul li').eq(0).find('p').html('已下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('已执行');
                    $('#DetailsMaintenance ul li').eq(2).find('p').html('已维修');
                    $('#DetailsMaintenance ul li').eq(3).find('p').html('待试车');
                    $('#DetailsMaintenance ul li').eq(4).find('p').html('待审核');
                }else if(data.handle_equip.status == "待审核"){
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(4).find('.circle,.lineRight,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(5).find('.circle,.lineLeft').removeClass('changeColor');
                    $('#DetailsMaintenance #MaintenanceToBePerformed').removeClass('hide');
                    $('#DetailsMaintenance #lookXun').removeClass('hide');
                    $('#DetailsMaintenance #RecordAfterMaintenance').removeClass('hide');
                    $('#DetailsMaintenance #ReplacementOfSpareParts').removeClass('hide');
                    $('#DetailsMaintenance #TrialRunRecord').removeClass('hide');

                    $('#DetailsMaintenance ul li').eq(0).find('p').html('已下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('已执行');
                    $('#DetailsMaintenance ul li').eq(2).find('p').html('已维修');
                    $('#DetailsMaintenance ul li').eq(3).find('p').html('已试车');
                    $('#DetailsMaintenance ul li').eq(4).find('p').html('待审核');

                }else if(data.handle_equip.status == "完成"){
                    $('#DetailsMaintenance ul li').eq(0).find('.circle,.lineRight').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(1).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(2).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(3).find('.circle,.lineRight,.lineLeft').addClass('changeColor')
                    $('#DetailsMaintenance ul li').eq(4).find('.circle,.lineRight,.lineLeft').addClass('changeColor');
                    $('#DetailsMaintenance ul li').eq(5).find('.circle,.lineLeft').addClass('changeColor')

                    $('#DetailsMaintenance #MaintenanceToBePerformed').removeClass('hide');
                    $('#DetailsMaintenance #lookXun').removeClass('hide');
                    $('#DetailsMaintenance #RecordAfterMaintenance').removeClass('hide');
                    $('#DetailsMaintenance #ReplacementOfSpareParts').removeClass('hide');
                    $('#DetailsMaintenance #TrialRunRecord').removeClass('hide');

                    $('#DetailsMaintenance ul li').eq(0).find('p').html('已下发');
                    $('#DetailsMaintenance ul li').eq(1).find('p').html('已执行');
                    $('#DetailsMaintenance ul li').eq(2).find('p').html('已维修');
                    $('#DetailsMaintenance ul li').eq(3).find('p').html('已试车');
                    $('#DetailsMaintenance ul li').eq(4).find('p').html('已审核');

                }
                SEquipID = data.handle_equip.s_equip_id;
                var result = template('workNews2', data);
                $('#DetailsMaintenance .MaintenanceIfo').html(result);




                //执行中
                $('#DetailsMaintenance #prvid5 img').attr('src',data.handle_equip.img_path);
                //检修之前
                $('#DetailsMaintenance #RecordBeforeMaintenance').val(data.start_note.note);
                $('#DetailsMaintenance #prvid3 img').attr('src',data.start_note.img_path);
                $('#DetailsMaintenance #PreServiceRemarks').val(data.start_note.overhaul_desc);
                $('#DetailsMaintenance #PreTime').val(data.start_note.overhaul_time);
                //检修之后
                $('#DetailsMaintenance #RecordAfter').val(data.end_note.note);
                $('#DetailsMaintenance #prvid4 img').attr('src',data.end_note.img_path);
                $('#DetailsMaintenance #PostOverhaulRemarks').val(data.end_note.overhaul_desc);
                $('#DetailsMaintenance #AfterTime ').val(data.end_note.overhaul_time);

                //备件信息
                $('#DetailsMaintenance #ReplacementParts ').val(data.spares.spare_name);
                $('#DetailsMaintenance #RecordTimeAfterMaintenance').val(data.spares.change_time);

                getBoot();

                //checked默认选中
                data.equip_boots.boots.forEach(function(ele,index,arr){
                    for(var i=0; i < $('#DetailsMaintenance .checkTI').length; i++){
                        if($('#DetailsMaintenance .checkTI').eq(i).val() == ele.boot_check_id){
                            $('#DetailsMaintenance .checkTI').eq(i).attr('checked','true')
                        }
                    }
                })
//                 是否整理
                if(data.end_note.sort_out == 'Y'){
                    $('#WhetherToArrange .yes').attr('checked',true)
                }else if(data.end_note.sort_out == 'N'){
                    $('#WhetherToArrange .no').attr('checked',true)
                }
                var check_status=data.equip_boots.check_status
				
                //是否合格
                if(check_status =="Y"){
                
                    $('#TestResults .hege').prop('checked',true)
                    $('#TestResults .buhege').prop('checked',false)
                    $('.disqualificationResult').addClass('hide');
                }
                else if(check_status == "N"){
                	$('#TestResults .hege').prop('checked',false)
                    $('#TestResults .buhege').prop('checked',true)
                    $('.disqualificationResult').removeClass('hide');
                    $('.disqualificationResult textarea').val(data.equip_boots.exec_desc)
                }

               

            
                
            },
            error: function (err) {
                console.log(err);
            }
        })
        $('#d_task_forms').addClass('hide');
        $('#DetailsMaintenance').removeClass('hide');
    })
    $('body').delegate('#DetailsMaintenance #editBack', 'click', function(){
        $('#DetailsMaintenance').addClass('hide');
        $('#d_task_forms').removeClass('hide');
        origin();
    })

    //每页显示更改
    $("#pagesize").change(function(){
        $('#loading').show()
        var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/handle_equips.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            data: {
                equip_ids: $('.navMain #deviceName1').val(),
                begin_time: $('.navMain .createStart').val(),
                end_time: $('.navMain .createEnd').val(),
                status: $('.navMain #CheckTheState').val(),
            },
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('#loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount);
                $("#page").initPage(total, 1, origin);
                $('.page_total span small').text(total)
                var roles=data.role_code
                    Roles(roles)
            },
            error:function(err){
                console.log(err)
            }
        })
    })



    //查询页
    var page_state = "init";
    function searchPage(page) {
    	var page = $('#page .pageItemActive').html();
        var per=$('#pagesize option:selected').val();
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;

            var urlStr = "/handle_equips.json";
            $.ajax({
                type: 'get',
                url: urlStr,
                data: {
                    equip_ids: $('.navMain #deviceName1').val(),
                    begin_time: $('.navMain .createStart').val(),
                    end_time: $('.navMain .createEnd').val(),
                    status: $('.navMain #CheckTheState').val(),
                    page: page,
                    per: per,
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('handle_equips',data);
                    $('#public_table table').html(html);

                    //分页
                    $('.page_total span a').text(page);
                    $('#loading').hide();
					var roles=data.role_code
                    Roles(roles)
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert("请求数据错误！！！")
                }
            })
        }
    }
    function Page(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;
            $.ajax({
                async: true,
                type: 'get',
                dataType: "json",
                url: '/handle_equips.json?page=' + page + '&per=' + $('#pagesize option:selected').val(),
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('handle_equips',data);
                    $('#public_table table').html(html);
                    $('.page_total span a').text(page);
                    $('#loading').hide();
                    var roles=data.role_code
                    Roles(roles)
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert('请求数据失败,请稍后重试');
                    $('#loading').hide();
                }

            })
        }
    }

    //添加缓存清除
    $('body').delegate('#add', 'click', function(){
        $('#addModal').find('input,textarea').val('');
        $('#addModal').find('.match_vlide').addClass('hide');
        $('#addModal').find('.match_vlide').removeClass('control-default');
        $('#addModal').find('.match_vlide').removeClass('control-error');
        $('#addModal').find('.match_vlide').removeClass('control-success');
    })
    //车间项目验证
    $('body').delegate("#addModal #workshop","focus",function(){
        $('#addModal #workshop1').addClass('control-default')
        $('#addModal #workshop1').removeClass('hide')

    });
    $('body').delegate("#addModal #workshop","blur",function(){
        if($('#addModal #workshop').val() != ''){
            $('#addModal #workshop1').removeClass('control-default')
            $('#addModal #workshop1').removeClass('control-error')
            $('#addModal #workshop1').addClass("control-success");
            $('#addModal #workshop1').html('车间名称已选择');
        }else if($('#addModal #workshop').val() == ''){
            $('#addModal #workshop1').removeClass('control-default')
            $('#addModal #workshop1').removeClass('control-success')
            $('#addModal #workshop1').addClass("control-error");
            $('#addModal #workshop1').html("车间名称不能为空")
        }
    });
    //设备名称验证
    $('body').delegate("#addModal .DeviceName","focus",function(){
        $('#addModal #DeviceName').addClass('control-default')
        $('#addModal #DeviceName').removeClass('hide')

    });
    $('body').delegate("#addModal .DeviceName","blur",function(){
        if($('#addModal .DeviceName').val() != ''){
            $('#addModal #DeviceName').removeClass('control-default')
            $('#addModal #DeviceName').removeClass('control-error')
            $('#addModal #DeviceName').addClass("control-success");
            $('#addModal #DeviceName').html('设备已选择');
        }else if($('#addModal .DeviceName').val() == ''){
            $('#addModal #DeviceName').removeClass('control-default')
            $('#addModal #DeviceName').removeClass('control-success')
            $('#addModal #DeviceName').addClass("control-error");
            $('#addModal #DeviceName').html("设备不能为空")
        }
    });
    //检修计划开始时间验证
    $('body').delegate("#addModal .beginTime input","focus",function(){
        $('#addModal #dayStart').addClass('control-default')
        $('#addModal #dayStart').removeClass('hide')

    });
    $('body').delegate("#addModal .beginTime input","blur",function(){
        if($('#addModal .beginTime input').val() != ''){
            $('#addModal #dayStart').removeClass('control-default')
            $('#addModal #dayStart').removeClass('control-error')
            $('#addModal #dayStart').addClass("control-success");
            $('#addModal #dayStart').html('计划开始时间已输入');
        }else if($('#addModal .beginTime input').val() == ''){
            $('#addModal #dayStart').removeClass('control-default')
            $('#addModal #dayStart').removeClass('control-success')
            $('#addModal #dayStart').addClass("control-error");
            $('#addModal #dayStart').html("计划开始时间不能为空")
        }
    });

    //检修计划结束时间验证
    $('body').delegate("#addModal .endTime input","focus",function(){
        $('#addModal #dayEnd').addClass('control-default')
        $('#addModal #dayEnd').removeClass('hide')

    });
    $('body').delegate("#addModal .endTime input","blur",function(){
        if($('#addModal .endTime input').val() != ''){
            $('#addModal #dayEnd').removeClass('control-default')
            $('#addModal #dayEnd').removeClass('control-error')
            $('#addModal #dayEnd').addClass("control-success");
            $('#addModal #dayEnd').html('计划结束时间已输入');
        }else if($('#addModal .endTime input').val() == ''){
            $('#addModal #dayEnd').removeClass('control-default')
            $('#addModal #dayEnd').removeClass('control-success')
            $('#addModal #dayEnd').addClass("control-error");
            $('#addModal #dayEnd').html("计划结束时间不能为空")
        }
    });

    //检修原因验证
    $('body').delegate("#addModal .reconditionReason","focus",function(){
        $('#addModal #reconditionReason').addClass('control-default')
        $('#addModal #reconditionReason').removeClass('hide')

    });
    $('body').delegate("#addModal .reconditionReason","blur",function(){
        if($('#addModal .reconditionReason').val() != ''){
            $('#addModal #reconditionReason').removeClass('control-default')
            $('#addModal #reconditionReason').removeClass('control-error')
            $('#addModal #reconditionReason').addClass("control-success");
            $('#addModal #reconditionReason').html('检修原因已输入');
        }else if($('#addModal .reconditionReason').val() == ''){
            $('#addModal #reconditionReason').removeClass('control-default')
            $('#addModal #reconditionReason').removeClass('control-success')
            $('#addModal #reconditionReason').addClass("control-error");
            $('#addModal #reconditionReason').html("检修原因不能为空")
        }
    });
    //检修描述验证
    $('body').delegate("#addModal .reconditionDesc","focus",function(){
        $('#addModal #reconditionDesc').addClass('control-default')
        $('#addModal #reconditionDesc').removeClass('hide')

    });
    $('body').delegate("#addModal .reconditionDesc","blur",function(){
        if($('#addModal .reconditionDesc').val() != ''){
            $('#addModal #reconditionDesc').removeClass('control-default')
            $('#addModal #reconditionDesc').removeClass('control-error')
            $('#addModal #reconditionDesc').addClass("control-success");
            $('#addModal #reconditionDesc').html('检修描述已输入');
        }else if($('#addModal .reconditionDesc').val() == ''){
            $('#addModal #reconditionDesc').removeClass('control-default')
            $('#addModal #reconditionDesc').removeClass('control-success')
            $('#addModal #reconditionDesc').addClass("control-error");
            $('#addModal #reconditionDesc').html("检修描述不能为空")
        }
    });

})

//保存图片
function previewImage(file, prvid) {
    /* file：file控件
    * prvid: 图片预览容器
    */

    var tip = "Expect jpg or png or gif!"; // 设定提示信息
    var filters = {
        "jpeg" : "/9j/4",
        "gif" : "R0lGOD",
        "png" : "iVBORw"
    }
    var prvbox = document.getElementById(prvid);
    prvbox.innerHTML = "";
    if (window.FileReader) { // html5方案
        for (var i=0, f; f = file.files[i]; i++) {
            var fr = new FileReader();
            fr.onload = function(e) {
                var src = e.target.result;
                if (!validateImg(src)) {
                    alert(tip)
                } else {
                    showPrvImg(src);
                }
            }
            fr.readAsDataURL(f);
        }
    } else { // 降级处理

        if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) {
            alert(tip);
        } else {
            showPrvImg(file.value);
        }
    }

    function validateImg(data) {
        var pos = data.indexOf(",") + 1;
        for (var e in filters) {
            if (data.indexOf(filters[e]) === pos) {
                return e;
            }
        }
        return null;
    }

    function showPrvImg(src) {
        var img = document.createElement("img");
        img.src = src;
        prvbox.appendChild(img);
        $('.look_insp img').zoomify();
    }
}

//根据角色显示和隐藏新增
function Roles(roles){
	if(roles=="mechanic_repair"){
		$('#d_task_forms .public_search #add').hide();
		$('#d_task_forms #public_table .handle_width .editModal').show();
	}else{
		$('#d_task_forms .public_search #add').show();
		$('#d_task_forms #public_table .handle_width .editModal').hide();
	}
	if(roles=="division_level"){
		$('#d_task_forms #public_table table td .delete').show()
	}else{
		$('#d_task_forms #public_table table td .delete').hide()
	}
}

  