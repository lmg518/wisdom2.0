$(function(){
	//增加
	$('body').delegate("#s_slas .new_add_btn","click",function(){
	
		var dataObj = {};
		var htmlStr = template('addSlas',dataObj);
		$('.a_slas_add').html(htmlStr)
			plicBox();
		$('#slas_add').show();
		addForm();
		
	})
	$('body').delegate("#s_slas h4 .close","click",function(){
 		$('#slas_add').hide();
 		$('#s_slas .a_slas_edit').hide();
 	})
	$('body').delegate("#s_slas .btn-cancel","click",function(){
 		$('#slas_add').hide();
 		$('#s_slas .a_slas_edit').hide();
 	})
	function plicBox(){
	//  报警级别
	    $.getJSON('/s_alarm_levels.json').done(function(data){
	    	var htmlStr=template('slas_leval',data);
	    	$('#slas_add .new_slas .slas_leval').html(htmlStr);
	    	$('#slas_edit .new_slas .slas_leval').html(htmlStr);
	    	$('#s_slas .slas_area2 .slas_leval option').each(function(i){
				var slasLevel_b = $('#slas_edit .slas_area2 .slas_leval option').eq(i).text();
				if(slasLevel_a==slasLevel_b){
					$('#s_slas .slas_area2 .slas_leval option').eq(i).attr('selected','selected')
				}
			})
	    })
	}
	plicBox();
//添加提交
$('body').delegate("#s_slas #slas_add .btn-sure","click",function(){
//	var slas_level1 = $('#slas_add .slas_area1 .slas_level option:selected').text();
	var slas_levell = $('#slas_add .slas_area1 .slas_level option:selected').text();  //报警级别
	var slas_level_id = $('#slas_add .slas_area1 .slas_content .slas_leval option:selected').val();  //报警级别
	var receive_minutes=$('#slas_add .slas_area1 .receive_minutes input').val();
	var arrive_minutes=$('#slas_add .slas_area1 .arrive_minutes input').val();
	var recovery_minutes=$('#slas_add .slas_area1 .recovery_minutes input').val();
	var deal_minutes=$('#slas_add .slas_area1 .deal_minutes input').val();
	$.ajax({
		type:"post",
		url:"/s_slas.json",
		async:true,
		data:{
			s_sla:
				{
					alarm_level:slas_level_id,
					receive_minutes:receive_minutes,
					arrive_minutes:arrive_minutes,
					recovery_minutes:recovery_minutes,
					deal_minutes:deal_minutes
			
				}
			},
			success: function(data) {
				$('#slas_add').hide();
				page_state = "change";
				originData();
			},
			error:function(err){
				console.log(err)
				alert("添加失败")
			}
	});
	
})
//编辑
var slasLevel_a;
var id;
$('body').delegate("#s_slas .slas_edit",'click',function(){

	$('#s_slas .a_slas_edit ').show();
	id = $(this).find('.hides').text();
	slasLevel_a = $(this).parents('tr').find('.levals').text();
	$.ajax({
		type:"get",
		url:"/s_slas/"+ id +".json",
		dataType: "json",
		success:function(data){
			var dataObj={
				editData:data
			}
			var htmlStr = template('editSlas',dataObj);
			$('.a_slas_edit').html(htmlStr)
			plicBox();
			editForm();
		},
		error:function(err){
			console.log(err)
		}
	});

})
//编辑提交
$('body').delegate('#s_slas #slas_edit .btn-sure','click',function(){
	var slas_level2 = $('#slas_edit .slas_area2 .slas_level option:selected').text();  //报警级别
	var slas_level_id2 = $('#slas_edit .slas_area2 .slas_content .slas_leval option:selected').val();  //报警级别
	var receive_minutes2=$('#slas_edit .slas_area2 .receive_minutes input').val();
	var arrive_minutes2=$('#slas_edit .slas_area2 .arrive_minutes input').val();
	var recovery_minutes2=$('#slas_edit .slas_area2 .recovery_minutes input').val();
	var deal_minutes2=$('#slas_edit .slas_area2 .deal_minutes input').val();
	$.ajax({
		type:"PUT",
		url: '/s_slas/'+ id +'.json',
			data:{
				s_sla:
					{
						alarm_level:slas_level_id2,
						receive_minutes:receive_minutes2,
						arrive_minutes:arrive_minutes2,
						recovery_minutes:recovery_minutes2,
						deal_minutes:deal_minutes2
				
					}
			},
			success: function(data) {
				$('#s_slas .a_slas_edit').hide();
				var htmlData = template('slas',data);
				$('#s_slas table .slas_msg').html(htmlData);
				page_state = "change";
				originData();
			},
			error:function(err){
				console.log(err)
				alert("添加失败")
			}
	});
})

function addForm(){
	//		响应时间
		var receive = document.getElementById("receive");
		var receive_Tip = document.getElementById("receive_Tip");
		$('body').delegate("#s_slas #receive","focus",function(){
			receive_Tip.className="control-default";
			receive_Tip.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #receive","blur",function(){
			if(receive.validity.valid){
				receive_Tip.className = "control-success";
				receive_Tip.firstChild.nodeValue = "响应时间正确.";
				$('#slas_add .btn-sure').attr('disabled', false)
			}else if(receive.validity.valueMissing){
				receive_Tip.className = "control-error";
				receive_Tip.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_add .btn-sure').attr('disabled', true)
			}
		});
		
		//		到达现场时间
		var arrive = document.getElementById("arrive");
		var arrive_Tip = document.getElementById("arrive_Tip");
		$('body').delegate("#s_slas #arrive","focus",function(){
			arrive_Tip.className="control-default";
			arrive_Tip.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #arrive","blur",function(){
			if(arrive.validity.valid){
				arrive_Tip.className = "control-success";
				arrive_Tip.firstChild.nodeValue = "响应时间正确.";
				$('#slas_add .btn-sure').attr('disabled', false)
			}else if(arrive.validity.valueMissing){
				arrive_Tip.className = "control-error";
				arrive_Tip.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_add .btn-sure').attr('disabled', true)
			}
		});
		
		//		故障恢复时间
		var recovery = document.getElementById("recovery");
		var recovery_Tip = document.getElementById("recovery_Tip");
		$('body').delegate("#s_slas #recovery","focus",function(){
			recovery_Tip.className="control-default";
			recovery_Tip.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #recovery","blur",function(){
			if(recovery.validity.valid){
				recovery_Tip.className = "control-success";
				recovery_Tip.firstChild.nodeValue = "响应时间正确.";
				$('#slas_add .btn-sure').attr('disabled', false)
			}else if(recovery.validity.valueMissing){
				recovery_Tip.className = "control-error";
				recovery_Tip.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_add .btn-sure').attr('disabled', true)
			}
		});
		
		//		解决时间
		var deal = document.getElementById("deal");
		var deal_Tip = document.getElementById("deal_Tip");
		$('body').delegate("#s_slas #deal","focus",function(){
			deal_Tip.className="control-default";
			deal_Tip.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #deal","blur",function(){
			if(deal.validity.valid){
				deal_Tip.className = "control-success";
				deal_Tip.firstChild.nodeValue = "响应时间正确.";
				$('#slas_add .btn-sure').attr('disabled', false)
			}else if(deal.validity.valueMissing){
				deal_Tip.className = "control-error";
				deal_Tip.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_add .btn-sure').attr('disabled', true)
			}
		});
		
}

function editForm(){
	//		响应时间
		var receive1 = document.getElementById("receive1");
		var receive_Tip1 = document.getElementById("receive_Tip1");
		$('body').delegate("#s_slas #receive1","focus",function(){
			receive_Tip1.className="control-default";
			receive_Tip1.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #receive1","blur",function(){
			if(receive1.validity.valid){
				receive_Tip1.className = "control-success";
				receive_Tip1.firstChild.nodeValue = "响应时间正确.";
				$('#slas_edit .btn-sure').attr('disabled', false)
			}else if(receive1.validity.valueMissing){
				receive_Tip1.className = "control-error";
				receive_Tip1.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_edit .btn-sure').attr('disabled', true)
			}
		});
		
		//		到达现场时间
		var arrive1 = document.getElementById("arrive1");
		var arrive_Tip1 = document.getElementById("arrive_Tip1");
		$('body').delegate("#s_slas #arrive1","focus",function(){
			arrive_Tip1.className="control-default";
			arrive_Tip1.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #arrive1","blur",function(){
			if(arrive1.validity.valid){
				arrive_Tip1.className = "control-success";
				arrive_Tip1.firstChild.nodeValue = "响应时间正确.";
				$('#slas_edit .btn-sure').attr('disabled', false)
			}else if(arrive1.validity.valueMissing){
				arrive_Tip1.className = "control-error";
				arrive_Tip1.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_edit .btn-sure').attr('disabled', true)
			}
		});
		
		//		故障恢复时间
		var recovery1 = document.getElementById("recovery1");
		var recovery_Tip1 = document.getElementById("recovery_Tip1");
		$('body').delegate("#s_slas #recovery1","focus",function(){
			recovery_Tip1.className="control-default";
			recovery_Tip1.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #recovery1","blur",function(){
			if(recovery1.validity.valid){
				recovery_Tip1.className = "control-success";
				recovery_Tip1.firstChild.nodeValue = "响应时间正确.";
				$('#slas_edit .btn-sure').attr('disabled', false)
			}else if(recovery1.validity.valueMissing){
				recovery_Tip1.className = "control-error";
				recovery_Tip1.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_edit .btn-sure').attr('disabled', true)
			}
		});
		
		//		解决时间
		var deal1 = document.getElementById("deal1");
		var deal_Tip1 = document.getElementById("deal_Tip1");
		$('body').delegate("#s_slas #deal1","focus",function(){
			deal_Tip1.className="control-default";
			deal_Tip1.firstChild.nodeValue = "请输入数字."
		})
		$('body').delegate("#s_slas #deal1","blur",function(){
			if(deal1.validity.valid){
				deal_Tip1.className = "control-success";
				deal_Tip1.firstChild.nodeValue = "响应时间正确.";
				$('#slas_edit .btn-sure').attr('disabled', false)
			}else if(deal1.validity.valueMissing){
				deal_Tip1.className = "control-error";
				deal_Tip1.firstChild.nodeValue = "响应时间不能为空."
				$('#slas_edit .btn-sure').attr('disabled', true)
			}
		});
		
}
//	分页+初始数据
	function originData(param){
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('slas',data_hash);
			$('#s_slas table .slas_msg').html(htmlData);
			$('.page_total span a').text(pp);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/s_slas.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlData = template('slas',data);
					$('#s_slas table .slas_msg').html(htmlData);
					$('.page_total span a').text(pp);
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/s_slas.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash = data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	})	
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/s_slas.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
	
})
