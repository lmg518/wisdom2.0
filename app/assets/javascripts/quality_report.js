$(function(){
		//获取当前时间
	var time = new Date();
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    var day = time.getDate();
    var newMonths = month < 10 ? '0' + month : month;
    var newDates = day < 10 ? '0' + day : day;
    var datestamp = year + "-" + newMonths + "-" + newDates;
    console.log(datestamp)
   	$('#loading').show()
    $.ajax({
	   	type:"get",
	   	url:"/quality_report/quality_report",
	    success:function(data){
		   	$('#quality_report  .excelTable').html(data)
		   	   $('#quality_report #public_table #water_report .time').html(datestamp)
		   	  var regioncode=$('#quality_report #public_table #water_report .region-code').html();
				if(regioncode=="TYSY_QUALITY"){
					$('#quality_report #public_table #water_report .exception-list .qualityReport').show()
					$('#quality_report #public_table #water_report .exception-list .qualityCenter').hide()
					
				}else if(regioncode=="TYSY_QCENTER"){
					$('#quality_report #public_table #water_report .exception-list .qualityReport').hide()
					$('#quality_report #public_table #water_report .exception-list .qualityCenter').show()
				}
		   	   var status=$('#quality_report #public_table #water_report .state').html();
		   	  
		   	   if(status=="已上报"){
		   	   	$('#quality_report #public_table #water_report .state').css("color","#00923f")
	
				$('#quality_report .navTop').hide();
		   	   }
		   	   $('#quality_report #public_table .table-title button').attr("disabled", true);
		   	  	$('#loading').hide() 
		   },
	    error:function(err){
    		console.log(err)
    	}
    });     
   	   	   //暂存
	   	   $('body').delegate('#quality_report #save','click',function(){
	   	   	$('#loading').show()
				
				var status='N';
				submit(status)
				$('#loading').hide()
	   	   })
	   	   //上传
	   	   $('body').delegate('#quality_report #report','click',function(){
	   	   		$('#loading').show()
	   	   	var status='Y'
	   	   	submit(status)
	   	   	$('#loading').hide()
	   	   })
	   	   function submit(status){
	   	   		var quality_params = [];
				var ta_arr = [];
				var ta_hash = {};
				var regioncode=$('#quality_report #public_table #water_report .region-code').html();
			if(regioncode=="TYSY_QUALITY"){
				
					$("#tf tr:gt(0)").each(function(i){
			    	var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });  
			    
				quality_params.push(ta_hash)
				ta_arr = []
				ta_hash = {}
			    $("#ty tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#fjy tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#shy tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#lj tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#cp tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#ycl tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#ecl tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#scpn tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#scps tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
				
			}else if(regioncode=="TYSY_QCENTER"){
				
				$("#na_nums tr:gt(0)").each(function(i){
					var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			    ta_arr = []
				ta_hash = {}
				$("#suan_nums tr:gt(0)").each(function(i){
				var item = {}
				    $(this).find('input').each(function() {
				        var $input = $(this)
				        item[$input.attr('name')] = $input.val()
				    })
				    ta_arr.push(item)
				    ta_hash["field_code"] =$(this).parents('td').siblings('.fieldCode').html()
					ta_hash['data'] = ta_arr
			    });
			    quality_params.push(ta_hash)
			}
				
			    
			    console.log(quality_params)
			    var date_time=$('#quality_report #public_table  .time').html();
			    
			    var objname={quality_params,date_time:date_time,status:status}
			    console.log(objname)
			    $.ajax({
			    	type:"post",
			    	url:"/quality_report",
			    	data:objname,
					success:function(data){
						console.log(data)
						alert("成功")
						$('.loading').hide()
						window.location.reload();
					},
					error:function(err){
						console.log(err)
					}
			    });
	   	   }
	//点击项目的事件
	$('body').delegate('#quality_report #quality_table tr .items','click',function(){
		if($(this).hasClass('qualityItemsChecked')){
			$(this).removeClass('qualityItemsChecked')
			$(this).siblings().css('border-color','')
			$('#quality_report #public_table .table-title button').attr("disabled", true);
		}else{
			$(this).addClass('qualityItemsChecked')
			$(this).siblings().css('border-color','#00923f')
			$('#quality_report #public_table .table-title button').attr("disabled", false);
		}
		$(this).parent().siblings('tr').find('.items').siblings().removeAttr("style")
//		$(this).siblings().removeAttr("style")
   		$(this).parent().siblings('tr').find('.items').removeClass('qualityItemsChecked')
   		$('#quality_report #quality_table tr .showDel').css('display','none')
    })	
    
    //增加行。删除行置顶 开始
    var winHeight = $(document).scrollTop();
 
    $(window).scroll(function() {
        var scrollY = $(document).scrollTop();// 获取垂直滚动的距离，即滚动了多少
 
        if (scrollY > 600){ //如果滚动距离大于550px则隐藏，否则删除隐藏类
            $('#quality_report #public_table .table-title2 ').css('display','block')
        } 
        else {
            $('#quality_report #public_table .table-title2 ').css('display','none')
        }
 
        if (scrollY > winHeight){ //如果没滚动到顶部，删除显示类，否则添加显示类
            $('#quality_report #public_table .table-title2 ').css('display','block')
        } 
        else {
             $('#quality_report #public_table .table-title2 ').css('display','none')
        }               
 
     });
       //增加行。删除行置顶 结束
       //关闭提示
       $('body').delegate('#quality_report .Tips span','click',function(){
       		$('#quality_report  .Tips').hide();
       })
       
})
//删除按钮的显示和隐藏
function ifShowDelBtn(){
	$('.qualityItemsChecked').siblings().children('table').find('.showDel').show()

}
//增加表格行
function addTr(){

	var tabId=$('.qualityItemsChecked').siblings().children('table').attr('id');
	console.log(tabId)
	var htmlText="<tr>" + $("#"+tabId+" tr").eq(-1).html()+ "</tr>"
    console.log(htmlText)
    $("#"+tabId+" tr").eq(-1).after(htmlText);
    $("#"+tabId+" tr").eq(-1).find('input').val('')
}

//删除表格的行
function qualityTableDel(obj,tab) {
    if ($(obj).parent().parent().children().length == 2) {
    	alert("最后一行不能删除！")
        return;
    }else{
     $(obj).parent().remove();		
    }
   
}