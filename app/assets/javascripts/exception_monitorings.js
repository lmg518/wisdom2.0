$(function(){
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	
	$('.loading').show()
	$.ajax({
			type: "get",
			url: "/exception_monitorings/init.json",
			dataType: "json",
			cache:false,
			success: function(data) {
				$('.loading').hide()
				var htmlStr = template('exception',data);
				$('.exception-list').html(htmlStr);
				var timeData = [];
				if (data.sources.length>0) {
					for (var i = 0;i<data.sources[0].d_sources.length;i++) {
						var time = data.sources[0].d_sources[i].d_time.substr(0,2)
						timeData.push(time);
					}
					$('.self-date th').each(function(i) {
						$(this).html(timeData[i] + '时');
					})
				}
				stationClick()
			},
			error:function(err){
				console.log(err)
			}
	})
	$('.except_box h4 span').click(function(){
		$('.except_box').hide();
		$('tr').find('.yellow').css('border','1px solid #fff')
	})
//	单击页面其他地方隐藏弹窗
	$(".except_box").click(function(){
        return false;
    });
	$(document).click(function(){
        $(".except_box").hide();
        $('tr').find('.yellow').css('border','1px solid #fff')
    });


	$('body').delegate("td","click",function(){
 		$('.public_box').show()
 	})
 	//表头时间
		var time = new Date();
		var year = time.getFullYear()
		var myMonth = time.getMonth() + 1;
		var myDate = time.getDate() - 1;
		var nowTime = time.getHours();
		var dateArry = [];
		function headeTitle(){
			for(var i = nowTime + 1; i <= 23; i++) {
				dateArry.push(i)
			}
			for(var i = 0; i <= nowTime; i++) {
				dateArry.push(i)
			}
			$('.self-date th').each(function(i) {
				$(this).html(dateArry[i] + '时');
			})
		}
		headeTitle()
		var today = year + '-' + (myMonth < 10 ? '0' + myMonth : myMonth) + '-' + ((myDate + 1) < 10 ? '0' + (myDate + 1) : (myDate + 1));
		function timeFormat(year,myMonth,myDate){
			if((myMonth == 2 ||myMonth == 4 ||myMonth == 6 ||myMonth == 8||myMonth == 9||myMonth == 11 || myMonth == 1) && (myDate == 0)){
				myDate = 31;
				myMonth -=1;
				if(myMonth == 0){
					year -= 1;
					myMonth =12
				}
			}else if((myMonth == 5||myMonth ==7||myMonth ==10||myMonth == 12) && myDate == 0){
					myDate = 30;
					myMonth -=1;
					if(myMonth == 0){
						year -= 1;
						myMonth =12
					}
			}else if(myMonth == 3 && myDate == 0){
				if( (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)){
					myDate = 29;
				}else{
					myDate = 28;
				}
				myMonth -=1;
				if(myMonth == 0){
					year -= 1;
					myMonth =12
				}
			}else{
				myDate = myDate;
			}
			return year+'-'+ (myMonth < 10 ? '0' + Number(myMonth) : myMonth)+'-' +  (myDate < 10 ? '0' + Number(myDate) : myDate);
		}
		var yesterday = timeFormat(year,myMonth,myDate);
		function yesToday(){
			$('.yesterday').attr('colspan', 23 - nowTime)
			$('.today').attr('colspan', Number(nowTime+1))
			$('.yesterday').html(yesterday)	
			$('.today .today-time').html(today);
		}
		yesToday()	
		
		//点击查询
		var start_datetime = '';
		var staStr;

		$('.search').click(function(){	
			$('.choose-site').hide()
			$('.loading').show()
//			pageName_Data()
//			var timeStr = $('.dateTimepicker').val();
//		    var staStr = $('.control_stationID').val().split(',')
			if($('.start_time').val()){
                var	timeStr = $('.start_time').val();
            }
            if($('.control_stationID').val()){
                var staStr = $('.control_stationID').val().split(',')
            }
			$('.yesterday').attr('colspan',18)
			$('.today').attr('colspan',6)
			$('.yesterday').text(timeStr);
			$('.today .today-time').html('')
			$('.self-date th').each(function(i) {
				$(this).html(i + '时');
			})
			var obj = {
				exception_infos:{}
			};
			obj.exception_infos.d_time = timeStr;
			obj.exception_infos.station_ids = staStr;
			$.ajax({
					type: "get",
					url: "/exception_monitorings/init.json",
					async: true,
					dataType: "json",
					cache:false,
					data:obj,
					success: function(data) {
						data_hash = data
						$('.loading').hide()
						
						var htmlStr = template('exception',data);
						$('.exception-list').html(htmlStr);
						$('.self-date th').each(function(i) {
							$(this).html(i + '时');
						})
						stationClick();
					},
					error:function(err){
						console.log(err)
						alert("查询站点过多")
					}
				})
		})
		
	function stationClick(){
		var stationId
		var staTime
		var factorId
		var data
		$('.list-parent').each(function(i) {
			var isCollapes = true;
			var $this = $(this);
			$(this).find('.station-list').click(function(e) {
				e.stopPropagation()
				$('.station-list').removeClass('td-selected');	
				$(this).addClass('td-selected')
				if(isCollapes) {
					$(this).find('.pull-right').attr('class','glyphicon glyphicon-minus pull-right')
					$('.tr-child'+i).show()
				} else {
					$(this).find('.pull-right').attr('class','glyphicon glyphicon-plus pull-right')
					$('.tr-child'+i).hide()
					$('.tr-child'+i).find('.yellow').css('border','1px solid #fff')
				}
				
				isCollapes = !isCollapes;
			})
			//点击单元格
			$('.tr-child'+i).find('.yellow').click(function(e){
				$(this).css('border','3px solid red').siblings('.yellow').css('border','1px solid #fff')
				stationId = $(this).find('.sta_id').text();
				staTime = $(this).find('.sta_time').text();
				factorId = $(this).find('.factor_id').text();
				e.stopPropagation();
				data = {
					station_infos:{}
				};
				data.station_infos.d_station_id = stationId;
				data.station_infos.item_id = factorId
				data.station_infos.d_time = staTime	
				$.ajax({
					type: "get",
					url: "/data_view/exception_monitorings/"+ stationId +".json",
					async: true,
					dataType: "json",
					data:data,
					cache:false,
					success: function(data) {
						var htmlStr = template('actor',data);
						$('.except_content').html(htmlStr);

					},
					error:function(err){
						console.log(err)
					}
				})
				$('.except_box').show();
//				return false;
			})

		})
		
	}
})