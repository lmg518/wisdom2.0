$(function(){
	//分配站点
	$('body').delegate("#add_form .choice","click",function(){
		allot()
		$('.single-station1').show();
		
	})
	$('body').delegate("#edit_form .choice","click",function(){
		allot()
		$('.single-station1').show();
		
		 var s = document.getElementsByName("stationId");
//		 for( var i = 0; i < s.length; i++ ){
//		 	 var str=$('#editModal .form-group #station_id').val();;
// 			strArray=str.split(","); 
// 			 for(var j = 0; j < strArray.length; j++ ){
// 			 	 if(s[i].value==strArray[j]){
//				   s[i].checked=true;
//				   break;
//				  }
// 			 }
//		 }
	})
	
	function allot(){
		$.ajax({
			type: "get",
			url: "/search_stations.json",
			async: true,
			dataType: "json",
			success:function(data){
				var htmlStr = template('allot',data);
				$('#single-station2 .content').html(htmlStr)
			}
		})
	}
	allot()
	$('body').delegate('.single-station .footer .btn-cancel','click',function(){
		$('.single-station').hide();
	})
	$('body').delegate(".single-station .clearfix span","click",function(){
		$('.single-station').hide();
	})	


	$('body').delegate('#single-station2  .checkall input','click',function(){
		$(this).parents('#single-station2').find('.tab-station p input').prop('checked',this.checked);
	})
	$('body').delegate('#single-station2 .station-one  p input','click',function(){
		$(this).parents('.tab-station').find('.station-two p input').prop('checked',this.checked);
	})
		$('body').delegate('#fenModal #fenSave','click',function(){
		
			if($('#single-station2 .station-two p input[type="checkbox"]:checked')){
				var text=[];
				var textid=[];
				$('#single-station2 .station-two p input[type="checkbox"]:checked').siblings('span').each(function(){
					text.push(this.innerHTML)
				})
				$('#single-station2 .station-two p input[type="checkbox"]:checked').siblings('small').each(function(){
					textid.push(this.innerHTML)
				})
				var allText = text.join(',')
				var textid=textid.join(',');
				
				$('#addModal #station').val(allText)
				$('#addModal #station_id').val(textid)
				$('#editModal #station').val(allText)
				$('#editModal #station_id').val(textid)
			}else{
				$('#addModal #station').val('')
				$('#addModal #station_id').val('')
				$('#editModal #station').val('')
				$('#editModal #station_id').val('')
			}
			$('#fenModal').modal('hide')
		})
	$('body').delegate("#add_form .choice2","click",function(){
		$.ajax({
			type: "get",
			url: "/search_region_codes_all.json",
			async: true,
			dataType: "json",
			success:function(data){
				var htmlStr = template('getSingleStation',data);
				$('#region_list .content').html(htmlStr)
				
				d_stations();
			}
		})
	
//		$('.single-station2').show();
	})


	
	
	$(document).delegate('#addModal .modal-footer .btn-default','click',function(){
		$('#add_form .form-group input[type="text"]').val("");
	})
	$(document).delegate('#addModal .modal-header .close','click',function(){
		$('#add_form .form-group input[type="text"]').val("");
	})
	
	
	$(document).ready(function() {
	    $('#add_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	        	unit_name: {
	                message: '项目编码无效',
	                validators: {
	                    notEmpty: {
	                        message: '单位名称不能为空'
	                    },
	                }
	           },
	            unit_code: {
	                validators: {
	                    notEmpty: {
	                        message: '单位编号不能为空'
	                    }
	                }
	            },
	            linkman: {
	                validators: {
	                    notEmpty: {
	                        message: '联系人员不能为空'
	                    }
	                }
	            },
	            areas: {
	                validators: {
	                    notEmpty: {
	                        message: '所属单位不能为空'
	                    }
	                }
	            },
	            linkman_tel: {
	                validators: {
	                    notEmpty: {
	                        message: '联系电话不能为空'
	                    }
	                }
	            },
//	            station: {
//	                validators: {
//	                    notEmpty: {
//	                        message: '站点不能为空'
//	                    }
//	                }
//	            },
	            unit_address: {
	                validators: {
	                    notEmpty: {
	                        message: '单位地址不能为空'
	                    }
	                }
	            }
	        }
	    });
	});
	//添加提交
	$(document).delegate('#addModal .modal-footer .btn-sure','click',function(){
		var addValidator = $('#add_form').data('bootstrapValidator');
		addValidator.validate();
		var unit_name=$('#add_form #unit_name').val();
		var unit_code=$('#add_form #unit_code').val();
		var linkman=$('#add_form #linkman').val();
		var linkman_tel=$('#add_form #linkman_tel').val();
		var unit_address=$('#add_form #unit_address').val();
		var status=$('#add_form #status option:selected').val();
//		var station_id=$('#add_form #station_id').val();
		var s_region_code_id=$('#add_form #s_region_code_id').val();
		if (addValidator.isValid()){
			$.ajax({
			type:"POST",
			url:"/s_region_code.json",
			async:true,
			data:{
				unit:{
					unit_name:unit_name,
					unit_code:unit_code,
					linkman:linkman,
					linkman_tel:linkman_tel,
					unit_address:unit_address,
					status:status,
//					station_id:station_id,
					s_region_code_id:s_region_code_id
				}
			},
			success:function(data){
				page_state = "change";
				originData();
				$('#addModal').modal('hide')
				$('#add_form').data('bootstrapValidator').resetForm(true);
			},
			error:function(err){
				
			}
		});
		}
		
	})
	//编辑
	var id;
	var s_region_code_id;
	$('body').delegate('#s_region_code table tbody tr td .work_edit','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"get",
			url:"/s_region_code/"+id+".json",
			success: function(data) {
				console.log(data)
				var unit_name=data.region.unit_name;
				var unit_code=data.region.unit_code;
				var linkman=data.region.linkman;
				var linkman_tel=data.region.linkman_tel;
				var unit_address=data.region.unit_address;
				var status=data.region.status;
				var station_name=data.region.station_name;
				var station_id=data.region.station_id;
				var s_region_code=data.region.s_region_code;
				s_region_code_id=data.region.s_region_code_id
				console.log(s_region_code_id)
				$('#editModal .form-group #unit_name').val(unit_name);
				$('#editModal .form-group #unit_code').val(unit_code);
				$('#editModal .form-group #linkman').val(linkman);
				$('#editModal .form-group #linkman_tel').val(linkman_tel);
				$('#editModal .form-group #unit_address').val(unit_address);
				$('#editModal .form-group .station').val(station_name);
				$('#editModal .form-group #station_id').val(station_id);
				$('#editModal .form-group .area ').val(s_region_code);
				$('#editModal .form-group #s_region_code_id').val(s_region_code_id);
				console.log($('#editModal .form-group #s_region_code_id').val())
				if(status=="Y"){
				$('#editModal #status #option1').prop('selected',true);
				}else{
					$('#editModal #status #option2').prop('selected',true);
				}
				var staId=data.region.station_id;
				var staIds=staId.split(',')
				$('#edit_form #single-station1 .content .station-two input[name="stationId"]').each(function(i,e){
					console.log($(this).val())
					for(var i=0;i<staIds.length;i++){
						if($(this).val()==staIds[i]){
							$(this).checked=true;
						}
					}
				})
			},
			error:function(err){
				
			}
		});
	})
		$('body').delegate("#edit_form .choice2","click",function(){
//		var textId=$('#s_region_code_id').val()
//		console.log(textId)
		$.ajax({
			type: "get",
			url: "/search_region_codes_all.json",
			async: true,
//			dataType: "json",
			success:function(data){
				console.log(data)
				var htmlStr = template('getSingleStation',data);
				$('#region_list .content').html(htmlStr)
				
				d_stations();
				$(' #edit_form .regionCode_content input').each(function(){
		    		var cId=$(this).val();
		
		    		if(cId==s_region_code_id){
		    		
		    			$(this).prop('checked',true)	
		    			return false;
		    		}
		    		
		    	})
			}
		})
		
		
//		$('.single-station2').show();
	})
		$(document).ready(function() {
	    $('#editModal').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	        	unit_name: {
	                message: '项目编码无效',
	                validators: {
	                    notEmpty: {
	                        message: '单位名称不能为空'
	                    },
	                }
	           },
	            unit_code: {
	                validators: {
	                    notEmpty: {
	                        message: '单位编号不能为空'
	                    }
	                }
	            },
	            linkman: {
	                validators: {
	                    notEmpty: {
	                        message: '联系人员不能为空'
	                    }
	                }
	            },
	            linkman_tel: {
	                validators: {
	                    notEmpty: {
	                        message: '联系电话不能为空'
	                    }
	                }
	            },
	            areas: {
	                validators: {
	                    notEmpty: {
	                        message: '所属单位不能为空'
	                    }
	                }
	            },
//	            station: {
//	                validators: {
//	                    notEmpty: {
//	                        message: '站点不能为空'
//	                    }
//	                }
//	            },
	            unit_address: {
	                validators: {
	                    notEmpty: {
	                        message: '单位地址不能为空'
	                    }
	                }
	            }
	        }
	    });
	});
	//编辑提交
	$(document).delegate('#editModal .modal-footer .btn-sure','click',function(){
		var editValidator = $('#editModal').data('bootstrapValidator');
		editValidator.validate();
		var unit_name=$('#edit_form #unit_name').val();
		var unit_code=$('#edit_form #unit_code').val();
		var linkman=$('#edit_form #linkman').val();
		var linkman_tel=$('#edit_form #linkman_tel').val();
		var unit_address=$('#edit_form #unit_address').val();
		var status=$('#edit_form #status option:selected').val();
//		var station_id=$('#edit_form #station_id').val();
		var s_region_code_id=$('#edit_form #s_region_code_id').val();
		if (editValidator.isValid()){
			$.ajax({
			type:"put",
			url:"/s_region_code/"+id+".json",
			async:true,
			data:{
				unit:{
					unit_name:unit_name,
					unit_code:unit_code,
					linkman:linkman,
					linkman_tel:linkman_tel,
					unit_address:unit_address,
					status:status,
//					station_id:station_id,
					s_region_code_id:s_region_code_id
				}
			},
			success:function(data){
				page_state = "change";
				originData();
				$('#editModal').modal('hide')
				
			},
			error:function(err){
				
			}
		});
		}
		
	})
	//删除
	$("body").delegate("#s_region_code table tbody tr td .work_delete","click",function() {
		$('.delete_prop').hide();
		$(this).parent().children('div').show();	
	})
	$('body').delegate('#s_region_code table tbody tr td .delete_prop .btn-sure','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"delete",
			url:'/s_region_code/'+id+'.json',
			success:function(data){
				page_state = "change";
				originData();
				$('.delete_prop').hide();
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#s_region_code table tbody tr td .delete_prop .btn-cancel','click',function(){
		$('.delete_prop').hide();
	})
	//	分页+初始数据
	function originData(param){
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('region_tables',data_hash);
			$('.public_table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/s_region_code.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('region_tables',data);
					$('.public_table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/s_region_code.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	})
	
//	//每页显示更改
    $("#pagesize").change(function(){
    	$('.loading').show()
    	var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/s_region_code.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    
    
    	function d_stations(){
		$('body').delegate("#regionModal .modal-footer #regionSave","click",function(){
            var chek_redie_this = $('input:radio[name="group_id"]:checked');
                var chkRadio = chek_redie_this.val();
               
                if (chkRadio == null) {
                    return false;
                } else {
                    var stationId = $(this).val();
                    var controllStation = chek_redie_this.next().html();
                    $('#add_form #control_station').val(controllStation);
                    $('#add_form #s_region_code_id').val(chkRadio);
                    $('#edit_form #control_station').val(controllStation);
                    $('#edit_form #s_region_code_id').val(chkRadio);
                }
			$('#regionModal').modal('hide')
		});
		//---------
//		$('body').delegate('.single-station .footer .btn-cancel','click',function(){
//			$('.single-station2').hide();
//		})
//		$('body').delegate(".single-station2 .clearfix span","click",function(){
//			$('.single-station2').hide();
//		})	
	}
	d_stations();
})
