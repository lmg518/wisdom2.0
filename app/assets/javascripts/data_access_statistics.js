$(function(){
	
	//	站点
    var placeId = ''
	$('.place_leval').change(function(){
		placeId = $(this).val()
	})
	
	$('.place_leval').click(function(){
		$('.control_station').val('')
	})
	
	$('.public_search .control_station').click(function(){
		if(placeId == ''){
			$('#search-station').hide()
			alert('请先选择区域级别')
	    }else{
	    	$('#search-station').show()
	    	$.ajax({
			type:'get',
			url:'/search_station_bindings.json',
			data:{
				region_level:placeId
			},
			success:function(data){
				var bindTemp = template('editStation',data);
		        $('#search-station .choose-site').html(bindTemp);
		        $('#search-station .choose-site').show();
			},
			error:function(err){
				console.log(err)
			}
		})
	    }
		
		
	})
	
	$(document).delegate('.public_search .control_station','blur',function(){
			var control_stationName=$('.public_search .control_station').val();
			if(control_stationName==""){
				$('.control_stationID').val("");
			}else{
				$('.control_stationID').val();
			}
	})
	
	
	var station_id='';
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];

	function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val().split(',');
		}
		if($('.public_search .control_stationID').val()==""){
			station_id="";
		}
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
		
	}
	$('.public_search .search').click(function(){
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/data_access_statistics/init_index.json',
			data:{
				search:{
					station_ids:station_id,
					data_times:data_times
				}
			},
			success:function(data){
				$('.loading').hide()
				var originData = template('accessTab',data)
				$('.public_table .exception-list').html(originData)
			},
			error:function(err){
				console.log(err)
			}
		})
	})

	
	
	$('.loading').show()
	$.ajax({
		type:'get',
		url:'/data_access_statistics/init_index.json',
		dataType:'json',
		success:function(data){
			$('.loading').hide()
			var dataHtml = template('place_leval',data)
			$('.place_leval').html(dataHtml)
			var originData = template('accessTab',data)
			$('.public_table .exception-list').html(originData)
		},
		error:function(err){
			console.log(err)
		}
	})
	
	
	
})
