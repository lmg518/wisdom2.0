$(function(){
	
	//查询
	var work_name='';
	var work_type='';
	var work_flag='';
	function pageName_Data(){
		$('#work_pros .public_search .searchName').val($('#work_pros .public_search #work_name').val());
		var searchType=$('#work_pros .public_search #work_type option:selected').val();
		var searchFlag=$('#work_pros .public_search #work_flag option:selected').val();
		$('#work_pros .public_search .searchType').val(searchType);
		$('#work_pros .public_search .searchFlag').val(searchFlag);
		work_name=$('#work_pros .public_search .searchName').val();
		work_type=$('#work_pros .public_search .searchType').val();
		work_flag=$('#work_pros .public_search .searchFlag').val();
	}
	$('.search').click(function(){
		work_name='';
	    work_type='';
        work_flag='';
		pageName_Data()
	
		$.ajax({
			type: "get",
			url: "/work_pros.json",
			dataType: "json",
			data:{
				work_name:work_name,
				work_type:work_type,
				work_flag:work_flag
			},
			success: function(data) {
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
		   		$('.page_total small').text(data.list_size);
	   		 	
			}
		})
	 
	})
	$(document).delegate('#addModal .modal-footer .btn-default','click',function(){
		$('#add_form .form-group input').val("");
	})
	
	
		$(document).ready(function() {
	    $('#add_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	        	project_code: {
	                message: '项目编码无效',
	                validators: {
	                    notEmpty: {
	                        message: '项目编码不能为空'
	                    },
	                   
                        stringLength: {
                            min: 2,
                            max: 16,
                            message: '长度必须在2到16位之间'
                        },
	                }
	           },
	            project_name: {
	                validators: {
	                    notEmpty: {
	                        message: '项目名称不能为空'
	                    }
	                }
	            }
	        }
	    });
	});
	//添加提交
	$(document).delegate('#addModal .modal-footer .btn-sure','click',function(){
		var addValidator = $('#add_form').data('bootstrapValidator');
		addValidator.validate();
		
		var project_name=$('#add_form .form-group .project_name').val();
		var project_code=$('#add_form .form-group .project_code').val();
		var project_type=$("#add_form .form-group .project_type input[type='radio']:checked").val();
		var project_iden=$("#add_form .form-group .project_iden input[type='radio']:checked").val();
		if (addValidator.isValid()){
			$.ajax({
				type:"POST",
				url:"/work_pros.json",
				async:true,
				data:{
					d_work_pro:{
						work_name:project_name,
						work_code:project_code,
						work_type:project_type,
						work_flag:project_iden
					}
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
					originData();
					
					$('#addModal').modal('hide')
					$('#add_form').data('bootstrapValidator').resetForm(true);
				},
				error:function(err){
					console.log(err);
				}
			});
		}
		
		
	
	})
	$(document).ready(function() {
	    $('#editModal').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	        	project_code: {
	                message: '项目编码无效',
	                validators: {
	                    notEmpty: {
	                        message: '项目编码不能为空'
	                    },
	                   
                        stringLength: {
                            min: 2,
                            max: 16,
                            message: '长度必须在2到16位之间'
                        },
	                }
	           },
	            project_name: {
	                validators: {
	                    notEmpty: {
	                        message: '项目名称不能为空'
	                    }
	                }
	            }
	        }
	    });
	});
	//修改
	var id;
	$('body').delegate('#work_pros table tbody tr td .work_edit','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"get",
			url:'/work_pros/'+id+'.json',
			success:function(data){
			var Data=data;
			var work_code=Data.work.work_code;
			var work_name=Data.work.work_name;
			$('#editModal .form-group .project_code').val(work_code);
			$('#editModal .form-group .project_name').val(work_name);
			
			var work_flag = Data.work.work_flag;
			
			if(work_flag=="Y"){
				$('#editModal .form-group #Radios1').prop('checked',true);
			}else{
				$('#editModal .form-group #Radios2').prop('checked',true);
			}
			var work_type=Data.work.work_type;
	    	$('#editModal .form-group .project_type input[type=radio]').each(function(){
	    		var types=$(this).val();
	    		if(work_type==types){
	    			$(this).prop('checked',true)	
	    			return false;
	    		}
	    		
	    	})
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	//修改提交
	$(document).delegate('#editModal .modal-footer .btn-sure','click',function(){
		var editValidator = $('#editModal').data('bootstrapValidator');
		editValidator.validate();
		var project_name=$('#edit_form .form-group .project_name').val();
		var project_code=$('#edit_form .form-group .project_code').val();
		var project_type=$("#edit_form .form-group .project_type input[type='radio']:checked").val();
		var project_iden=$("#edit_form .form-group .project_iden input[type='radio']:checked").val();
		if (editValidator.isValid()){
		 	$.ajax({
				type:"put",
				url:'/work_pros/'+id+'.json',
				data:{
					d_work_pro:{
						work_name:project_name,
						work_code:project_code,
						work_type:project_type,
						work_flag:project_iden
					}
				},
				success:function(data){
					page_state = "change";
					alert("提交成功");
					$('#editModal').modal('hide')
					originData();
				},
				error:function(err){
					console.log(err)
				}
			})	
		 }
	
	})
	
	
	//删除
	$("body").delegate("#work_pros table tbody tr td .work_delete","click",function() {
		$('.delete_prop').hide();
		$(this).parent().children('div').show();	
	})
	$('body').delegate('#work_pros table tbody tr td .delete_prop .btn-sure','click',function(){
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
			type:"delete",
			url:'/work_pros/'+id+'.json',
			success:function(data){
				page_state = "change";
				originData();
				$('.delete_prop').hide();
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	$('body').delegate('#work_pros table tbody tr td .delete_prop .btn-cancel','click',function(){
		$('.delete_prop').hide();
	})
	//查看详情

	$('body').delegate('#work_pros table tbody tr td .work_look','click',function(){
		id=$(this).parents('tr').find('.id').text();

		$.ajax({
			type:"get",
			url:'/work_pros/'+id+'.json',
			success:function(data){
			var Data=data;
			var swork_code=Data.work.work_code;
			var swork_name=Data.work.work_name;
			$('#showModal .form-group .project_code').val(swork_code);
			$('#showModal .form-group .project_name').val(swork_name);
			
			var swork_flag = Data.work.work_flag;
			
			if(swork_flag=="Y"){
				$('#showModal .form-group #Radios1').prop('checked',true);
			}else{
				$('#showModal .form-group #Radios2').prop('checked',true);
			}
			var swork_type=Data.work.work_type;
	    	$('#showModal .form-group .project_type input[type=radio]').each(function(){
	    		var s_types=$(this).val();
	    		if(swork_type==s_types){
	    			$(this).prop('checked',true)	
	    			return false;
	    		}
	    		
	    	})
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	//	分页+初始数据
	function originData(param){
//		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('work_table',data_hash);
			$('table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
			$.ajax({
				url: '/work_pros.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					work_name:$('#work_pros .public_search .searchName').val(),
					work_type:$('#work_pros .public_search .searchType').val(),
					work_flag:$('#work_pros .public_search .searchFlag').val()
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('work_table',data);
					$('table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/work_pros.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	})
	 init_index();
     function init_index(){
         $.getJSON('/work_pros.json').done(function(data){
             var total = data.list_size;
             data_hash = data;
             $("#page").initPage(total, 1, originData);
             $('.page_total span small').text(total)
         })
     }
//	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/work_pros.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})