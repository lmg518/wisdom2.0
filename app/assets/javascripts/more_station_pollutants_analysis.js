$(function(){
//	站点
    var placeId = ''
	$('.place_leval').change(function(){
		placeId = $(this).val()
	})
	$('.dateTimepicker').click(function(){
		$('.choose-site').hide()
	})
	$('.place_leval').click(function(){
		$('.control_station').val('')
	})
	
	$('.public_search .control_station').click(function(){
		if(placeId == ''){
			$('#search-station').hide()
			alert('请先选择区域级别')
	    }else{
	    	$('#search-station').show()
			$.ajax({
				type:'get',
				url:'/search_station_bindings.json',
				data:{
					region_level:placeId
				},
				success:function(data){
					var bindTemp = template('editStation',data);
			        $('#search-station .choose-site').html(bindTemp);
			        $('#search-station .choose-site').show();
				},
				error:function(err){
					console.log(err)
				}
			})
		}
	})
	
	$(document).delegate('.public_search .control_station','blur',function(){
			var control_stationName=$('.public_search .control_station').val();
			if(control_stationName==""){
				$('.control_stationID').val("");
			}else{
				$('.control_stationID').val();
			}
	})
	
	$('.loading').show()
	$.ajax({
		type:'get',
		url:'/more_station_pollutants_analysis.json',
		success:function(data){
			$('.loading').hide()
			var dataHtml = template('place_leval',data)
			$('.place_leval').html(dataHtml)
		},
		error:function(err){
			console.log(err)
		}
	})
	
	var station_id='';
	var start_datetime = '';
	var end_datetime = '';
	var data_times = [];
    function pageName_Data(){
		if($('.public_search .control_stationID').val()){
			station_id = $('.public_search .control_stationID').val().split(',');
		}
		if($('.public_search .control_stationID').val()==""){
			station_id="";
		}
		start_datetime = $('.public_search .start_datetime').val();
		end_datetime = $('.public_search .end_datetime').val();
		if(start_datetime && end_datetime){
			data_times[0]=start_datetime;
			data_times[1]=end_datetime;
		}else if(start_datetime){
			data_times[0]=start_datetime;
			data_times[1]=start_datetime;
			
		}else if(end_datetime){
			data_times[0]=end_datetime;
			data_times[1]=end_datetime;
		}else{
			data_times = []
		}
	}
	var stationName=''
	var station=''
	var datas=''
	var datasSo2=''
	var so2DataVal=''
	var so2DataTime= ''
	var so2DataVals=''
	var so2DataTimes=''
	var so2Da='';
	var so2DataStationArr = []
	var so2ValArr = []
	var so2TimeArr = []
	var so2Dat = []
	var datasCo=''
	var coDataVal=''
	var coDataTime=''
	var coDataVals=''
	var coDataTimes='';
	var coDa = ''
	var coDat = []
	var coDataStationArr = []
	var coValArr = []
	var coTimeArr = []
	var datasNo2=''
	var no2DataVal=''
	var no2DataTime=''
	var no2DataVals=''
	var no2DataTimes='';
	var no2Da = ''
	var no2Dat = []
	var no2DataStationArr = []
	var no2ValArr = []
	var no2TimeArr = []
	var datasO3=''
	var o3DataVal=''
	var o3DataTime=''
	var o3DataVals=''
	var o3DataTimes='';
	var o3Da = ''
	var o3Dat = []
	var o3DataStationArr = []
	var o3ValArr = []
	var o3TimeArr = []
	var datasPm25=''
	var pm25DataVal=''
	var pm25DataTime=''
	var pm25DataVals= ''
	var pm25DataTimes= '';
	var pm25Da = ''
	var pm25Dat = []
	var pm25DataStationArr = []
	var pm25ValArr = []
	var pm25TimeArr = []
	var datasPm10 = ''
	var pm10DataVal = ''
	var pm10DataTime = ''
	var pm10DataVals = ''
	var pm10DataTimes = '';
	var pm10Da = ''
	var pm10Dat = []
	var pm10DataStationArr = []
	var pm10ValArr = []
	var pm10TimeArr = []
	$('.public_search .search').click(function(){
		$('.choose-site').hide()
		$('.loading').show()
		pageName_Data()
		$.ajax({
			type:'get',
			url:'/more_station_pollutants_analysis.json',
			data:{
				search:{
					station_ids:station_id,
					data_times:data_times
				}
			},
			success:function(data){
				$('.loading').hide()
				station = data.station
				stationName = station.station_name
				datas = data.station_data
				datasSo2 = datas.so2
				datasSo2.map(function(item,index,arr){
					so2DataVal = item.station_name
					so2DataStationArr.push(so2DataVal)
					so2DataTime = item.data
					so2ValArr = []
					so2TimeArr = []
					so2DataTime.map(function(ite,ind,ar){
						so2DataVals = ite.val
						so2DataTimes = ite.data_time
						so2ValArr.push(so2DataVals)
					    so2TimeArr.push(so2DataTimes.substring(0,16))
					    
					})
					so2Dat[index]=({
				            name:so2DataVal,
				            type:'line',
				            data:so2ValArr
				        })
				})
				var myChart1 = echarts.init(document.getElementById('line1'));	
				option1 = {
				    title: {
				        text: 'SO2'
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:so2DataStationArr
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
				        feature: {
				            saveAsImage: {}
				        }
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data:so2TimeArr
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series:so2Dat
				};
				myChart1.setOption(option1);			
					
				datasNo2 = datas.no2
				datasNo2.map(function(item,index,arr){
					no2DataVal = item.station_name
					no2DataStationArr.push(no2DataVal)
					no2DataTime = item.data
					no2ValArr = []
				    no2TimeArr =[]
					no2DataTime.map(function(ite,ind,ar){
						no2DataVals = ite.val
						no2DataTimes = ite.data_time
						no2ValArr.push(no2DataVals)
					    no2TimeArr.push(no2DataTimes.substring(0,16))
					})
					no2Dat[index]=({
				            name:no2DataVal,
				            type:'line',
				            data:no2ValArr
				        })
				})
				
				var myChart2 = echarts.init(document.getElementById('line2'));	
				option2 = {
				    title: {
				        text: 'NO2'
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:no2DataStationArr
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
				        feature: {
				            saveAsImage: {}
				        }
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data:no2TimeArr
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series:no2Dat
				};
				myChart2.setOption(option2);
			
			    datasCo = datas.co
				datasCo.map(function(item,index,arr){
					coDataVal = item.station_name
					coDataStationArr.push(coDataVal)
					coDataTime = item.data
					coValArr = []
			        coTimeArr = []
					coDataTime.map(function(ite,ind,ar){
						coDataVals = ite.val
						coDataTimes = ite.data_time
						coValArr.push(coDataVals)
					    coTimeArr.push(coDataTimes.substring(0,16))
					})
					coDat[index]=({
				            name:coDataVal,
				            type:'line',
				            data:coValArr
				        })
				})
				var myChart3 = echarts.init(document.getElementById('line3'));	
				option3 = {
				    title: {
				        text: 'CO'
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:coDataStationArr
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
				        feature: {
				            saveAsImage: {}
				        }
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data:coTimeArr
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series:coDat
				};
				myChart3.setOption(option3);
		
		        datasPm10 = datas.pm_10
				datasPm10.map(function(item,index,arr){
					pm10DataVal = item.station_name
					pm10DataStationArr.push(pm10DataVal)
					pm10DataTime = item.data
					pm10ValArr = []
		            pm10TimeArr = []
					pm10DataTime.map(function(ite,ind,ar){
						pm10DataVals = ite.val
						pm10DataTimes = ite.data_time
						pm10ValArr.push(pm10DataVals)
					    pm10TimeArr.push(pm10DataTimes.substring(0,16))
					})
					pm10Dat[index]=({
				            name:pm10DataVal,
				            type:'line',
				            data:pm10ValArr
				        })
				})
				var myChart4 = echarts.init(document.getElementById('line4'));	
				option4 = {
				    title: {
				        text: 'PM10'
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:pm10DataStationArr
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
				        feature: {
				            saveAsImage: {}
				        }
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data:pm10TimeArr
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series:pm10Dat
				};
				myChart4.setOption(option4);
		
			    datasPm25 = datas.pm2_5
				datasPm25.map(function(item,index,arr){
					pm25DataVal = item.station_name
					pm25DataStationArr.push(pm25DataVal)
					pm25DataTime = item.data
					pm25ValArr = []
			        pm25TimeArr = []
					pm25DataTime.map(function(ite,ind,ar){
						pm25DataVals = ite.val
						pm25DataTimes = ite.data_time
						pm25ValArr.push(pm25DataVals)
					    pm25TimeArr.push(pm25DataTimes.substring(0,16))
					})
					pm25Dat[index]=({
				            name:pm25DataVal,
				            type:'line',
				            data:pm25ValArr
				        })
				})
				var myChart5 = echarts.init(document.getElementById('line5'));	
				option5 = {
				    title: {
				        text: 'PM25'
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:pm25DataStationArr
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
				        feature: {
				            saveAsImage: {}
				        }
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data:pm25TimeArr
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series: pm25Dat
				};
				myChart5.setOption(option5);
			    
			    datasO3 = datas.pm2_5
				datasO3.map(function(item,index,arr){
					o3DataVal = item.station_name
					o3DataStationArr.push(o3DataVal)
					o3DataTime = item.data
					o3ValArr = []
			        o3TimeArr = []
					o3DataTime.map(function(ite,ind,ar){
						o3DataVals = ite.val
						o3DataTimes = ite.data_time
						o3ValArr.push(o3DataVals)
					    o3TimeArr.push(o3DataTimes.substring(0,16))
					})
					o3Da = {
				            name:o3DataVal,
				            type:'line',
				            data:o3ValArr
				        }
					o3Dat.push(o3Da)
				})
				var myChart6 = echarts.init(document.getElementById('line6'));	
				option6 = {
				    title: {
				        text: 'O3'
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:o3DataStationArr
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
				        feature: {
				            saveAsImage: {}
				        }
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data:o3TimeArr
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series:o3Dat
				};
				myChart6.setOption(option6);
			    
			},
			error:function(err){
				console.log(err)
			}
		})
	})
	
	
})
