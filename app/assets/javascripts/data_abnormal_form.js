$(function(){
		//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	
		 	//查询
 	var station_id = '';
 	var created_time = '';
	var end_time = '';
	var job_no='';
	var author='';
	var handle_man='';
	var create_type='';
	var job_status='';
	   function public_search(){
			$('#data_abnormal .public_search .startTime1').val($('.create_datetime').val())
		    $('#data_abnormal .public_search .endTime1').val($('.end_datetime').val())
			created_time = $('#data_abnormal .public_search .startTime1').val();
	    	end_time = $('#data_abnormal .public_search .endTime1').val();
			if(created_time && end_time){
				created_time=created_time;
				end_time=end_time;
			}else if(created_time){
				end_time = created_time
			}else if(end_time){
				created_time = end_time
			}else{
				created_time = ''
		        end_time = ''
			}
			$('#data_abnormal .public_search .searchSta').val( $('.control_stationIDs').val())
			if($('#data_abnormal .public_search .searchSta').val()){
	             station_id = $('#data_abnormal .public_search .searchSta').val().split(',')
	        }else{
	        	 station_id = ''
			}
						
		$('#data_abnormal .public_search .searchNo').val($('.public_search .job_no').val())
        $('#data_abnormal .public_search .searchAuthor').val($('.public_search .author').val())
        $('#data_abnormal .public_search .searchHan').val($('.public_search .search_handle').val())			
		var searchCrea=$('.public_search .create_type option:selected').val();
		$('#data_abnormal .public_search .searchCrea').val(searchCrea);
		var searchStatus=$('.public_search .job_status option:selected').val();
		$('#data_abnormal .public_search .searchStatus').val(searchStatus)
		

		job_no=$('#data_abnormal .public_search .searchNo').val();
	   	author=$('#data_abnormal .public_search .searchAuthor').val();
	    handle_man= $('#data_abnormal .public_search .searchHan').val();
	    create_type=$('#data_abnormal .public_search .searchCrea').val()
	    job_status=$('#data_abnormal .public_search .searchStatus').val();
		}
	   $('.public_search .search').click(function(){
	   	
		 station_id = '';
	 	 created_time = '';
		 end_time = '';
		 job_no='';
		 author='';
		 handle_man='';
		 create_type='';
		  job_status='';
		 public_search()
//	   	var create_type=$('.public_search .create_type').val();
		$.ajax({
			type:"get",
			url:"/data_view/data_abnormal_form.json",
			cache:false,
			data:{
				created_time:created_time,
				end_time:end_time,
				station_id:station_id,
				job_no:job_no,
				author:author,
				handle_man:handle_man,
				job_status:job_status,
				create_type:create_type
				
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
				$('.page_total small').text(data.list_size);
					$('#data_abnormal .public_table .exception-list .job_status').each(function(i){
			        	var s=$(this).text()
			        	
			        	if(s=="工单待处理"){
							$(this).siblings().find('.fault_writing').show()
						}else{
							$(this).siblings().find('.fault_writing').hide()
						}
						if(s=="工单待审核"){
							$(this).siblings().find('.fault_audit').show()
						}else{
							$(this).siblings().find('.fault_audit').hide()
						}
			        	
				    })
			},
			error:function(err){
				console.log(err)
			}
		});
	   })
	
	
	//编辑
	var id;
	$('body').delegate('.fault_writing','click',function(){
		$('#data_abnormal').hide()
		$('#data_abnormal_look').hide();
		$('#data_abnormal_audits').hide()
		id=$(this).parents('tr').find('.id').text();
		$.ajax({
    		type:"get",
    		cache:false,
    		url:'/data_view/data_abnormal_form/'+id + '.json',
    		success:function(data){
    			var dataObj={
					editData:data
				}
    			console.log(dataObj)
    			var dTaskFormsNews = template('dataAbnormalEDit',dataObj)
 				$('#data_abnormal_edit').html(dTaskFormsNews)
 				$('#data_abnormal_edit').show();
 				var job_status=data.task_from.job_status;
 				var libg=data.task_from.flow;
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					$("#data_abnormal_edit .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				}
 				$('#data_abnormal_edit .dTaskTop li').each(function(i){
 				
   					var libg=$(this).find('.lbg-txt').text();
   					if(job_status==libg){
   						
   						var bgs=$(this).index()
   						var lis = $('#data_abnormal_edit .dTaskTop li');
   						
   						for(var j=0;j<=bgs;j++){
   						
							lis[j].className="on";
   						}
   					}
   				})
 				$('#data_abnormal_edit .dTaskTop li').eq(0).addClass("on")
 			
 				
    		},
    		error:function(err){
    			console.log(err)
    		}
    	});
	})
	//编辑保存
		$('body').delegate('#editSave','click',function(){
			
		var image_file=$('#data_abnormal_edit .seeTypes .look_insp #prvid img').attr("src")

		$.ajax({
			type:"put",
			cache:false,
			url:'/data_view/data_abnormal_form/'+id + '.json',
			data:{
				image_file:image_file
			},
			success:function(data){
				page_state = "change";
				originData();
				$('#data_abnormal').show()
				$('#data_abnormal_edit').hide()
				$('#data_abnormal_look').hide();
				$('#data_abnormal_audits').hide()
			},
			error:function(err){
				
			}
		});
	})
	$('body').delegate('#editBack','click',function(){
		$('#data_abnormal').show()
		$('#data_abnormal_edit').hide()
		$('#data_abnormal_look').hide()
		$('#data_abnormal_audits').hide()
	
	})
		//  //点击查看时
    $('body').delegate('.fault_look','click',function(){
    	

    	$('#data_abnormal').hide()
		$('#data_abnormal_edit').hide()
$('#data_abnormal_audits').hide()
    	id=$(this).parents('tr').find('.id').text();
    	$.ajax({
    		type:"get",
    		cache:false,
    		url:'/data_view/data_abnormal_form/'+id + '.json',
    		success:function(data){

    			var dataObj={
					editData:data
				}
				console.log(data)
    			var dTaskFormsNews = template('data_abnormalLook',dataObj)
 				$('#data_abnormal_look').html(dTaskFormsNews)
 				$('#data_abnormal_look').show();
 				var job_status=data.task_from.job_status;
 				var libg=data.task_from.flow;
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					$("#data_abnormal_look .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				}
 				$('#data_abnormal_look .dTaskTop li').each(function(i){
 				
   					var libg=$(this).find('.lbg-txt').text();
   					if(job_status==libg){
   						
   						var bgs=$(this).index()
   						var lis = $('#data_abnormal_look .dTaskTop li');
   						
   						for(var j=0;j<=bgs;j++){
   						
							lis[j].className="on";
   						}
   					}
   				})
 				$('#data_abnormal_look .dTaskTop li').eq(0).addClass("on")
				$('#lookXun .look_insp img').zoomify();
    		},
    		error:function(err){
    			console.log(err)
    		}
    	});


	})
	$('body').delegate('#lookBack','click',function(){
		$('#data_abnormal').show()
    	$('#data_abnormal_audits').hide()
    	$('#data_abnormal_look').hide()
    	$('#data_abnormal_edit').hide();
	})
	
		//审核
	var checkid;
	$('body').delegate('.fault_audit','click',function(){
		$('#data_abnormal').hide()
		$('#data_abnormal_edit').hide()
		$('#data_abnormal_look').hide()
		
	       checkid=$(this).parents('tr').find('.id').text();
	       $.ajax({
    			type:"get",
    			cache:false,
    			url:'/data_view/data_abnormal_form/'+checkid + '.json',
    			success:function(data){
    				var dataObj = {
						editData:data
					}
    				console.log(dataObj)
					var originData = template('dataAbnormalAudit',dataObj)
					$('#data_abnormal_audits').html(originData)
					$('#data_abnormal_audits').show();
					var status=data.task_from.job_status;
					var libg=data.task_from.flow;

 				
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					$("#data_abnormal_audits .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				}
 				$('#data_abnormal_audits .dTaskTop li').each(function(i){
// 				
   					var libg=$(this).find('.lbg-txt').text();
   					if(status==libg){
   						
   						var bgs=$(this).index()
   						var lis = $('#data_abnormal_audits .dTaskTop li');
   						
   						for(var j=0;j<=bgs;j++){
   						
							lis[j].className="on";
   						}
   					}
   				})
 				$('#lookXun .look_insp img').zoomify();
    			},
    			error:function(err){
    				console.log(err)
    			}
    		});
	})
		$('body').delegate('#data_abnormal_audits #ApproveY','click',function(){
			$('#data_abnormal_audits .audit_des').hide()
		})
		$('body').delegate('#data_abnormal_audits #ApproveN','click',function(){
			$('#data_abnormal_audits .audit_des').show()
		})
 		$('body').delegate('#data_abnormal_audits .saveback','click',function(){
			$('#data_abnormal').show()
	    	$('#data_abnormal_audits').hide()
	    	$('#data_abnormal_look').hide()
	    	$('#data_abnormal_edit').hide();
		})
		$('body').delegate('#data_abnormal_audits .save','click',function(){
			
			var audit_if=$('#data_abnormal_audits #audit_adopt .checkbox-radio input[name="Approve"]:checked').val();
			var audit_des=$('#data_abnormal_audits #audit_not').val();
			console.log(audit_if)
			console.log(audit_des)
			var audit_N=document.getElementById("audit_not")
			if(audit_if=="N" && audit_N.value==""){
				alert("请填写审核不通过原因")
			}else{
	   			$.ajax({
	   				type:"get",
	   				cache:false,
	   				url:'/data_view/data_abnormal_form/'+checkid + '/audit.json',
	   				data:{
	   					audit_if:audit_if,
	   					audit_des:audit_des
	   				},
	   				success:function(data){
	   					page_state = "change";
						alert("提交成功");
						originData();
						$('#data_abnormal').show()
					$('#data_abnormal_edit').hide()
					$('#data_abnormal_look').hide();
					$('#data_abnormal_audits').hide()
	   				},
	   				error:function(err){
	   					console.log(err)
	   				}
	   			});
			}

 		})
	
		//	分页+初始数据
	function originData(param){
		// public_search();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('Tables',data_hash);
			$('#data_abnormal table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
			data_hash = {};
			var station_ids = $('#data_abnormal .public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
			$.ajax({
				url: '/data_view/data_abnormal_form.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	cache:false,
		    	data:{
					created_time:$('#data_abnormal .public_search .startTime1').val(),
					end_time:$('#data_abnormal .public_search .endTime1').val(),
					station_id:station_ids,
					job_no:$('#data_abnormal .public_search .searchNo').val(),
					author:$('#data_abnormal .public_search .searchAuthor').val(),
					handle_man:$('#data_abnormal .public_search .searchCrea').val(),
					job_status:$('#data_abnormal .public_search .searchStatus').val(),
					create_type:$('#data_abnormal .public_search .searchCrea').val()
				
			},
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlData = template('Tables',data);
					$('#data_abnormal table .exception-list').html(htmlData);
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
					$('#data_abnormal .public_table .exception-list .job_status').each(function(i){
			        	var s=$(this).text()
			        	
			        	if(s=="工单待处理"){
							$(this).siblings().find('.fault_writing').show()
						}else{
							$(this).siblings().find('.fault_writing').hide()
						}
						if(s=="工单待审核"){
							$(this).siblings().find('.fault_audit').show()
						}else{
							$(this).siblings().find('.fault_audit').hide()
						}
						
			        	
				    })
					
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
    	$.getJSON('/data_view/data_abnormal_form.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
		$('#data_abnormal .public_table .exception-list .job_status').each(function(i){
        	var s=$(this).text()
        	
        	if(s=="工单待处理"){
				$(this).siblings().find('.fault_writing').show()
			}else{
				$(this).siblings().find('.fault_writing').hide()
			}
			if(s=="工单待审核"){
				$(this).siblings().find('.fault_audit').show()
			}else{
				$(this).siblings().find('.fault_audit').hide()
			}
        	
	    })

	})
})
function previewImage(file, prvid) { 
	/* file：file控件 
	* prvid: 图片预览容器 
	*/ 
	var tip = "Expect jpg or png or gif!"; // 设定提示信息 
	var filters = { 
	"jpeg" : "/9j/4", 
	"gif" : "R0lGOD", 
	"png" : "iVBORw" 
	} 
	var prvbox = document.getElementById(prvid); 
	prvbox.innerHTML = ""; 
	if (window.FileReader) { // html5方案 
		for (var i=0, f; f = file.files[i]; i++) { 
			var fr = new FileReader(); 
			fr.onload = function(e) { 
				var src = e.target.result; 
				if (!validateImg(src)) { 
					alert(tip) 
				} else { 
					showPrvImg(src); 
				} 
			} 
			fr.readAsDataURL(f); 
		} 
	} else { // 降级处理
	
		if ( !/\.jpg$|\.png$|\.gif$/i.test(file.value) ) { 
			alert(tip); 
		} else { 
			showPrvImg(file.value); 
		} 
	} 
	
		function validateImg(data) { 
			var pos = data.indexOf(",") + 1; 
			for (var e in filters) { 
				if (data.indexOf(filters[e]) === pos) { 
					return e; 
				} 
			} 
			return null; 
		} 
	
		function showPrvImg(src) { 
			var img = document.createElement("img"); 
			img.src = src; 
			prvbox.appendChild(img); 
			$('.look_insp img').zoomify();
		} 
}