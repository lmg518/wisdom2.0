$(function(){
	origin();//初始数据
	Time();//时间
	GetBoots()//检查项目
	//  部门
//$.ajax({
//	type:"get",
//	url:"/d_equip_work_rules/get_equip_regions",
//	success:function(data){
//		var html = template('departmentadd',data);
//      $('#addModal #department').html(html);
//   	$('#editModal #department').html(html);
//	},
//	error:function(err){
//		console.log(err)
//	}
//	
//});
 //添加车间区域
    $('body').delegate('#addModal #workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('regionnameadd',data);
                $('#addModal .regionName').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
     //修改车间区域
    $('body').delegate('#editModal #workshop','change',function(){
        $.ajax({
            type:"get",
            url:"/equips_area_manage/get_areas",
            data:{
                region_id:$(this).val()
            },
            success:function(data){
                var result = template('regionnameedit',data);
                $('#editModal .regionName').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
    
    //增加机修人员
    $('body').delegate('#addModal .regionName','change',function(){
    	var region_id=$('#addModal #workshop option:selected').val()
        $.ajax({
            type:"get",
            url:"/d_equip_work_rules/get_login",
            data:{
                region_id:region_id,
                area_id:$(this).val()
            },
            success:function(data){
                var result = template('sendPerson',data);
                $('#addModal .send_per').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
    //修改机修人员
    $('body').delegate('#editModal .regionName','change',function(){
    	var region_id=$('#editModal #workshop option:selected').val()
        $.ajax({
            type:"get",
            url:"/d_equip_work_rules/get_login",
            data:{
            	region_id:region_id,
                area_id:$(this).val()
            },
            success:function(data){
                var result = template('sendPerson2',data);
                $('#editModal .send_per').html(result);
            },
            error:function(err){
                console.log(err)
            }
        });
    })
    
    //查询
    $('body').delegate(".search", 'click', function(){
    	
        $.ajax({
            type: "get",
            url: "/d_equip_work_rules.json",
            data: {
//              equip_ids: $('.navMain #deviceName1').val(),
                begin_time: $('.navMain .createStart').val(),
                end_time: $('.navMain .createEnd').val(),
//              status: $('.navMain #CheckTheState').val(),
                equip_ids: $('.navMain #workshop option:selected').val(),
                page: 1,
                per: $('#pagesize option:selected').val()
            },
            beforeSend: function () {
                $('#loading').show();
            },
            success: function(data){
                $('#loading').hide()
                var html = template('d_equip_works',data);
                $('#public_table table').html(html);

                var total = data.equip_works_size;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', searchPage);
                $('#loading').hide();
              
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    
    
        //增加表单验证
    $(document).ready(function() {
	    $('#add_rules_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            
	            department: {
	                validators: {
	                    notEmpty: {
	                        message: '车间部门不能为空'
	                    },
	                    callback: {
                        message: '必须选择一个车间部门',
	                        callback: function(value, validator) {
	 
	                             if (value == '') {
	                                 return false;
	                             } else {
	                                 return true;
	                             }
	 
	                        }
	                    }
	                    
	                }
	            },
	            workshop: {
	                validators: {
	                    notEmpty: {
	                        message: '车间名称不能为空'
	                    },
	                    callback: {
                        message: '必须选择一个车间',
	                        callback: function(value, validator) {
	 
	                             if (value == '') {
	                                 return false;
	                             } else {
	                                 return true;
	                             }
	 
	                        }
	                    }
	                    
	                }
	            },
	            regionName: {
	                validators: {
	                    notEmpty: {
	                        message: '车间区域不能为空'
	                    },
	                    callback: {
                        message: '必须选择一个车间区域',
	                        callback: function(value, validator) {
	 
	                             if (value == '') {
	                                 return false;
	                             } else {
	                                 return true;
	                             }
	 
	                        }
	                    }
	                    
	                }
	            },
	            startTime: {
	                validators: {
	                    notEmpty: {
	                        message: '开始时间不能为空'
	                    }
	                }
	            },
	            endTime: {
	                validators: {
	                    notEmpty: {
	                        message: '结束时间不能为空'
	                    }
	                }
	            },
	            checkYN: {
	                validators: {
	                    notEmpty: {
	                        message: '请选择检查项目'
	                    }
	                }
	            }

	        }
	    });
	});
    //增加提交
    $('body').delegate('#addModal .btn-sure', 'click', function(){
        var addValidator = $('#add_rules_form').data('bootstrapValidator');
		addValidator.validate();
		var BootArr = [];
        for (var i = 0; i < $(".ItemsChecked input[type='checkbox']").length; i++) {

            if ($(".checkYN").eq(i).is(':checked')) {
                var data = $(".checkYN").eq(i).val();
                BootArr.push(data);
            }
        }
        data = {
            s_region_code_id: $('#addModal #workshop').val(),
            region_name: $('#addModal #workshop').find("option:selected").text(),
            s_area_id:$('#addModal .regionName option:selected').val(),

            work_start_time: $('#addModal .dayStart').val(),
            work_end_time: $('#addModal .dayEnd').val(),
            work_desc: $('#addModal .DescriptionInspector').val(),
            s_region_code_info_id:$('#addModal #department option:selected').val(),
            d_login_msg_id:$('#addModal .send_per input:radio:checked').val()
        }
      		
      	if (addValidator.isValid()){
      		$.ajax({
		            type:'post',
		            url:'/d_equip_work_rules',
		            data:{
		                d_equip_work_rule: data,
		                boot_ids: BootArr,
		            },
		            success:function(data){
		              
		                if(data.status == 200){
		                    alert("添加成功");
		                }else{
		                    alert("添加失败")
		                }
		                document.getElementById("add_rules_form").reset()
		                $('#addModal').modal('hide');
		                location.reload();
		                
		                origin();
		            },
		            error:function(err){
		                console.log(err)
		            }
		        })
      	}
      			
      		
    })

    //增加提交end
    
        //  删除

var id;
var name;
$("body").delegate("table tbody .delete","click",function() {
	id=Number($(this).parents('tr').find('.id').text())
	name=$(this).parents('tr').find('.region_name').text()
	$('#sureCannel').show();
	var sure_str = '<div class="point-out">确认删除 <span>"' + name + '"</span> 吗？</div>';
    $('#sureCannel .sureCannel-body').html(sure_str);

    var sureCannelFoot_str = '';
   
    sureCannelFoot_str += '<button class="btn-sure" id="work_rules_delete">确认</button><button onclick="resetPswd_cannel()" class="btn-cannel">取消</button>'

    $('#sureCannel .box-footer').html(sureCannelFoot_str);
})
$("body").delegate("#work_rules_delete","click",function() {
	$('#sureCannel').hide();
	$.ajax({
			type:"delete",
			url:'/d_equip_work_rules/' + id,
			success:function(data){
				
				 $("#success").show().delay(3000).hide(300);
	            var successFoot_str = '';
	            successFoot_str += '<div class="resetPs1"><span class="resetPs1-span">" ' + name + ' "</span>，删除成功！</div>'
	
	            $('.success-footer').html(successFoot_str)
				origin();
			
			},
			error:function(err){
				console.log(err)
			}
		})

})
    //删除end
    //修改显示数据
    var editID;
    $('body').delegate('.editModal', 'click', function () {
    	editID = $(this).parents('tr').find('.id').text();
    	$.ajax({
    		type:"get",
    		url:"/d_equip_work_rules/" + editID + ".json",
    		success:function(data){
    			$('#editModal .dayStart').val(data.equip_work.work_start_time);
    			$('#editModal .dayEnd').val(data.equip_work.work_end_time);
    			$('#editModal .DescriptionInspector').text(data.equip_work.work_desc);
    			var regionId=data.equip_work.s_region_code_info_id;
    			$("#editModal #department").find("option[value = '"+regionId+"']").attr("selected","selected");
				var regioncodeID=data.equip_work.s_region_code_id
				$("#editModal #workshop").find("option[value = '"+regioncodeID+"']").attr("selected","selected");
				var s_area_id=data.equip_work.s_area_id;
				
    			 data.boots.forEach(function(ele,index,arr){
                    for(var i=0; i < $('.ItemsChecked2 .checkYN2').length; i++){
                        if($('.ItemsChecked2 .checkYN2').eq(i).val() == ele.id){
                            $('.ItemsChecked2 .checkYN2').eq(i).attr('checked','true')
                        }
                    }
                })
    			 
    			 
    			 $.ajax({
		            type:"get",
		            url:"/d_equip_work_rules/get_login",
		            data:{
		                region_id:regioncodeID,
		                area_id:s_area_id
		            },
		            success:function(senddata){
		                var result = template('sendPerson2',senddata);
                		$('#editModal .send_per').html(result);
                		$('.send_per .Mechanic2 ').each(function(){
		    			 	var checkedID=$(this).val()
		    			 	
		    			 	if(checkedID==data.equip_work.d_login_msg_id){
		    			 		$(this).attr('checked','true')
		    			 	}
		    			 })
                		
		            },
		            error:function(err){
		                console.log(err)
		            }
		        });
		        
		        $.ajax({
		            type:"get",
		            url:"/equips_area_manage/get_areas",
		            data:{
		                region_id:regioncodeID
		            },
		            success:function(regiondata){
		                var result = template('regionnameedit',regiondata);
		                $('#editModal .regionName').html(result);
		                $("#editModal .regionName").find("option[value = '"+s_area_id+"']").attr("selected","selected");
		            },
		            error:function(err){
		                console.log(err)
		            }
		        });
    		},
    		error:function(err){
    			console.log(err)
    		}
    		
    	});
    })
    
    //编辑提交
    $('body').delegate('#editModal .btn-sure', 'click', function(){
    	var BootArr = [];
        for (var i = 0; i < $(".ItemsChecked2 input[type='checkbox']").length; i++) {

            if ($(".checkYN2").eq(i).is(':checked')) {
                var data = $(".checkYN2").eq(i).val();
                BootArr.push(data);
            }
        }
        data = {
            s_region_code_id: $('#editModal #workshop').val(),
            region_name: $('#editModal #workshop').find("option:selected").text(),
            s_area_id:$('#editModal .regionName option:selected').val(),

            work_start_time: $('#editModal .dayStart').val(),
            work_end_time: $('#editModal .dayEnd').val(),
            work_desc: $('#editModal .DescriptionInspector').val(),
            s_region_code_info_id:$('#editModal #department option:selected').val(),
            d_login_msg_id:$('#editModal .send_per input:radio:checked').val()
        }
        $.ajax({
            type:'put',
            url:'/d_equip_work_rules/'+ editID+ ".json",
            data:{
                d_equip_work_rule: data,
                boot_ids: BootArr,
            },
            success:function(data){
              
                if(data.status == 200){
                    alert("添加成功");
                }else{
                    alert("添加失败")
                }
                $('#editModal').modal('hide');
                document.getElementById("edit_rules_form").reset()
                origin();
            },
            error:function(err){
                console.log(err)
            }
        })
        
    })
    //查看
    $('body').delegate('.watchModal', 'click', function () {
    	editID = $(this).parents('tr').find('.id').text();
    	$.ajax({
    		type:"get",
    		url:"/d_equip_work_rules/" + editID + ".json",
    		success:function(data){
    			$('#detailsModal #department').val(data.equip_work.region_info_name);
    			$('#detailsModal #workshop').val(data.equip_work.region_name);
    			$('#detailsModal #regionName').val(data.equip_work.area_name);
    			$('#detailsModal .dayStart').val(data.equip_work.work_start_time);
    			$('#detailsModal .dayEnd').val(data.equip_work.work_end_time);
    			$('#detailsModal #sendPerson').val(data.equip_work.work_person);
    			$('#detailsModal .DescriptionInspector').text(data.equip_work.work_desc);
				
    			 data.boots.forEach(function(ele,index,arr){
                    for(var i=0; i < $('.ItemsChecked3 .checkYN3').length; i++){
                        if($('.ItemsChecked3 .checkYN3').eq(i).val() == ele.id){
                            $('.ItemsChecked3 .checkYN3').eq(i).attr('checked','true')
                        }
                    }
                })
    		
    		},
    		error:function(err){
    			console.log(err)
    		}
    		
    	});
    })
    
	////每页显示更改
    $("#pagesize").change(function(){
        $('#loading').show()
        var listcount=$(this).val()
        $.ajax({
            type: "get",
            url: '/d_equip_work_rules.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            data: {
                equip_ids: $('.navMain #deviceName1').val(),
                begin_time: $('.navMain .createStart').val(),
                end_time: $('.navMain .createEnd').val(),
//              status: $('.navMain #CheckTheState').val(),
            },
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('#loading').hide()
                var total = data.list_size;
                $("#page").attr('pagelistcount',listcount)
                $("#page").initPage(total, 1, origin)
                $('.page_total span small').text(total)
               
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})
//获取检查项
    function GetBoots() {
        $.ajax({
            type:"get",
            url:"/d_equip_work_rules/get_boots",
            success:function(data){
                var result = template('ItemsChecked',data);
                $('#addModal .ItemsChecked').html(result);
                var result2 = template('ItemsChecked2',data);
                $('#editModal .ItemsChecked2').html(result2);
                var result3 = template('ItemsChecked3',data);
                $('#detailsModal .ItemsChecked3').html(result3);
            },
            error:function(err){
                console.log(err)
            }
        });
    }
function Time(){
	//搜索时间
    $('.createStart').datetimepicker({
         format: 'hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 1,  
         minView: 1,  
         forceParse: false, 
        setEndDate:$('.day-end').val(),
    }).on('change',function(ev){
        var startDate = $('.createStart').val();
        $(".createEnd").datetimepicker('setStartDate',startDate);
        $(".createStart").datetimepicker('hide');
    });
    $('.createEnd').datetimepicker({
        format: 'hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 1,  
         minView: 1,  
         forceParse: false, 
        setStartDate:$(".day-start").val(),
    }).on('change',function(ev){
        var endDate = $('.createEnd').val();
        $(".createStart").datetimepicker('setEndDate',endDate);
        $(".createEnd").datetimepicker('hide');
    });
    //增加时间
    $('.dayStart').datetimepicker({
         format: 'hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 1,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setEndDate:$('.day-end').val(),
    }).on('change',function(ev){
        var startDate = $('.dayStart').val();
        $(".dayEnd").datetimepicker('setStartDate',startDate);
        $(".dayStart").datetimepicker('hide');
    });
    $('.dayEnd').datetimepicker({
         format: 'hh:00',  
         weekStart: 1,  
         autoclose: true,  
         startView: 1,  
         minView: 1,  
         forceParse: false,  
         language: 'zh-CN',
        setStartDate:$(".day-start").val(),
    }).on('change',function(ev){
        var endDate = $('.dayEnd').val();
        $(".dayStart").datetimepicker('setEndDate',endDate);
        $(".dayEnd").datetimepicker('hide');
    });
}
    //	   初始数据
    function origin(){
        var per=$('#pagesize option:selected').val();
        $.ajax({
            url: '/d_equip_work_rules.json?page=1&per=' +per,
            type: 'get',
            beforeSend: function () {
                $('#loading').show();
            },
            success:function(data){
                $('#loading').hide()
                var html = template('d_equip_works',data);
                $('#public_table table').html(html);

                var total = data.equip_works_size;
                $('.page_total span small').text(total);
                $("#page").initPage(total, '1', Page);
                $('#loading').hide();
				
            },
            complete: function () {
                $('#loading').hide();
            },
            error:function(err){
                console.log(err)
            }
        })
    }
    
    //查询页
    var page_state = "init";
    function searchPage(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;

            var urlStr = "/d_equip_work_rules.json?&page=" + page + "&per=10";
            $.ajax({
                type: 'get',
                url: urlStr,
                data: {
//                  equip_ids: $('.navMain #deviceName1').val(),
                    begin_time: $('.navMain .createStart').val(),
                    end_time: $('.navMain .createEnd').val(),
//                  status: $('.navMain #CheckTheState').val(),
                    equip_ids: $('.navMain #workshop option:selected').val(),
                    page: page,
                    per: $('#pagesize option:selected').val(),
                },
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('d_equip_works',data);
                    $('#public_table table').html(html);

                    //分页
                    $('.page_total span a').text(page);
                    $('#loading').hide();
					
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert("请求数据错误！！！")
                }
            })
        }
    }
    
    function Page(page) {
        if (parseInt(page) == 1 && page_state == "init") {
            $('input[name="pageNow"]').val(page);
            pageNow = page;
        } else {
            $('input[name="pageNow"]').val(page);
            page_state = "change";
            pageNow = page;
            $.ajax({
                async: true,
                type: 'get',
                dataType: "json",
                url: '/d_equip_work_rules.json?page=' + page + '&per=' + $('#pagesize option:selected').val(),
                beforeSend: function () {
                    $('#loading').show();
                },
                success: function (data) {
                    var html = template('d_equip_works',data);
                    $('#public_table table').html(html);
                    $('.page_total span a').text(page);
                    $('#loading').hide();
                    var roles=data.role_code
//                  Roles(roles)
                },
                complete: function () {
                    $('#loading').hide();
                },
                error: function (err) {
                    alert('请求数据失败,请稍后重试');
                    $('#loading').hide();
                }

            })
        }
    }
    