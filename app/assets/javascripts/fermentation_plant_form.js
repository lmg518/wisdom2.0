$(function(){
			//	车间
	$.getJSON('/search_region_codes.json').done(function(data){
		console.log(data)
		var unitTemp = template('unitRegion',data);
		$('#ty_workshop .modal-body').html(unitTemp);
	})

var time = new Date();
var year = time.getFullYear();
var month = time.getMonth() + 1;
var day = time.getDate();
var newMonths = month < 10 ? '0' + month : month;
var newDates = day < 10 ? '0' + day : day;
var stringDate 
stringDate= year + "-" + newMonths + "-" + newDates;

	$('#fermentation_plant_form .public_search #start_date_range').val(stringDate)
	//详情
	$('body').delegate('#fermentation_plant_form #indexTable td .lookFor','click',function(){
//		$('#fermentation_plant_form .public_search').hide()
		$('#fermentation_plant_form #powerCutRecords').hide()
		$('#loading').show()
		var regionCode=$(this).parents('tr').find('.code').text();
		var dateTime=$(this).parents('tr').find('.date_time').text();
		var url=''
		if(regionCode=="plant_form_01"){
			//发酵车间
			url="/fermentation_plant_form/fermentation_workshop"
		}else if(regionCode=="plant_form_02"){
			//合成车间
			url="/fermentation_plant_form/synthetic_workshop"
		}else if(regionCode=="plant_form_03"){
			//精制车间
			url="/fermentation_plant_form/refining_workshop"
		}else if(regionCode=="plant_form_04"){
			//烘包车间
			url="/fermentation_plant_form/bake_shop"
		}
		$.ajax({
			type:"get",
			url:url,
			data:{
				code:regionCode,
				time:dateTime
			},
			success:function(data){
				$('#fermentation_plant_form #lookHtml').html(data)
//				$('#fermentation_plant_form #public_table .public_table').html(data)
//				$('#fermentation_plant_form #excell-table .time').html(dateTime)
				$('#loading').hide()
			},
			error:function(err){
				alert("报错啦！！！")
				console.log(err)
			}
		});
	})
	
	$('#loading').show()
	$.ajax({
		type:"get",
		url:"/fermentation_plant_form/plant_form_index",
		   	data:{
				time:stringDate
			},
		success:function(data){
				
			$('#fermentation_plant_form #public_table .public_table').html(data)
			$('#loading').hide()
		},
		error:function(err){
			console.log(err)
		}
	});
	
	
	
	//查询
   $('#fermentation_plant_form .public_search .search').click(function(){
   
   	$('#loading').show()
   		var time=$('#fermentation_plant_form .public_search #start_date_range').val();
   		if($('#fermentation_plant_form .public_search .workshopID').val()){
			region_id=$('#fermentation_plant_form .public_search .workshopID').val().split(",");
		}else{
			region_id=''
		}
   		$.ajax({
	   	type:"get",
	   	url:"/fermentation_plant_form/plant_form_index",
	   	data:{
	   		time:time,
	   		region_id:region_id
	   	},
	    success:function(data){
	    		
		   	$('#fermentation_plant_form #public_table .public_table').html(data)
		   	  $('#loading').hide()
		   },
		   error:function(err){
		   	alert("出错啦！！")
	    		console.log(err)
	    	}
	   });
   })
   //	列表和折线图切换
   $('body').delegate('#fermentation_plant_form .plant_title_btn button','click',function(){
		$(this).siblings().removeClass('btn-chack');
    	$(this).addClass('btn-chack');
   })
   $('#fermentation_plant_form #listTable').click(function(){
// 	$(this).css({ "color": "#fff", "background": "#00923f" })
//	$(this).siblings().css({ "color": "#333", "background": "#fff" })
  	$('#fermentation_plant_form #public_table table').show();
  	$('#fermentation_plant_form .chartContent').hide();
   })
   $('#fermentation_plant_form #listChart').click(function(){
// 	$(this).css({ "color": "#fff", "background": "#00923f" })
//	$(this).siblings().css({ "color": "#333", "background": "#fff" })
  	$('#fermentation_plant_form .chartContent').show();
  	$('#fermentation_plant_form #public_table table').hide();
   })
   
   
   
   

})
//时间格式转换
Date.prototype.format = function (format) {
        var args = {
            "M+": this.getMonth() + 1,
            "d+": this.getDate(),
            "h+": this.getHours(),
            "m+": this.getMinutes(),
            "s+": this.getSeconds(),
            "q+": Math.floor((this.getMonth() + 3) / 3), //quarter

            "S": this.getMilliseconds()
        };
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var i in args) {
            var n = args[i];

            if (new RegExp("(" + i + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
        }
        return format;
    }; 