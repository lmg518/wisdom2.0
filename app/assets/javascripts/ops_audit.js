$(function(){
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	//首页input全选
	$('body').delegate('#allChk','click',function(){
//		$('#public_table table').find('.exception-list input').prop('checked',this.checked)
		var allCk = $(this).closest("table").find("input[type='checkbox'][disabled!='disabled']").not("[checkAll]");
        var checked = this.checked;
        allCk.each(function () {
            this.checked = checked;
        });
	});
	//查询
	var station_id = '';
	var job_no='';
	var author='';
	var created_time = '';
	var end_time = '';
	var handle_man='';
	var create_type='';
	var job_status='';
	var fault_type='';
	var polling_ops_type='';
	function pageName_Data(){
		$('#ops_audit .public_search .startTime1').val($('.create_datetime').val())
		$('#ops_audit .public_search .endTime1').val($('.end_datetime').val())
		created_time = $('#ops_audit .public_search .startTime1').val();
    	end_time = $('#ops_audit .public_search .endTime1').val();
		if(created_time && end_time){
			created_time=created_time;
			end_time=end_time;
		}else if(created_time){
			end_time = created_time
		}else if(end_time){
			created_time = end_time
		}else{
			created_time = ''
	        end_time = ''
		}
		$('#ops_audit .public_search .searchSta').val($('.control_stationIDs').val())
		if($('#ops_audit .public_search .searchSta').val()){
             station_id = $('#ops_audit .public_search .searchSta').val().split(',')
        }else{
        	 station_id = ''
        }
        $('#ops_audit .public_search .searchNo').val($('.public_search .job_no').val())
        $('#ops_audit .public_search .searchAuthor').val($('.public_search .author').val())
        $('#ops_audit .public_search .searchHan').val($('.public_search .search_handle').val())
        
        job_no=$('#ops_audit .public_search .searchNo').val();
	   	author=$('#ops_audit .public_search .searchAuthor').val();
	    handle_man=$('#ops_audit .public_search .searchHan').val();
	    
		var searchCrea=$('.public_search .create_type option:selected').val();
		$('#ops_audit .public_search .searchCrea').val(searchCrea);
		var searchStatus=$('.public_search .job_status option:selected').val();
		$('#ops_audit .public_search .searchStatus').val(searchStatus)
		var searchFault=$('.public_search .fault_type option:selected').val();
		$('#ops_audit .public_search .searchFault').val(searchFault)
		var searchOps=$('.public_search .ops_type option:selected').val();
		$('#ops_audit .public_search .searchOps').val(searchOps)
	    create_type=$('#ops_audit .public_search .searchCrea').val();
	    job_status=$('#ops_audit .public_search .searchStatus').val();
	    fault_type=$('#ops_audit .public_search .searchFault').val();
	    polling_ops_type=$('#ops_audit .public_search .searchOps').val();
	}
	 $('.public_search .search').click(function(){
	 	$('#ops_audit .btn-group #whole').css({ "color": "#fff", "background": "#558ad8" })
		$('#ops_audit .btn-group #whole').siblings().css({ "color": "#333", "background": "#fff" })
	 	 station_id = '';
		 job_no='';
		 author='';
		 created_time = '';
		 end_time = '';
		 handle_man='';
		 create_type='';
		 job_status='';
		 fault_type='';
		 polling_ops_type='';
		pageName_Data();
		$.ajax({
			type:"get",
			url:"/ops_audit.json",
			data:{
				d_station_id:station_id,
				job_no:job_no,
				author:author,
				created_time:created_time,
				end_time:end_time,
				handle_man:handle_man,
				create_type:create_type,
				job_status:job_status,
				fault_type:fault_type,
				polling_ops_type:polling_ops_type
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
		   		$('.page_total small').text(data.list_size);
		   		stucolor();
		   		var roles=data.role_name;
			        console.log(roles)
			        if(roles=="运维成员"){
			        	$('#btnBatchApprove').attr("disabled","disabled")
						$('#ops_audit .fault_audit').hide()
			        }
			        else if(roles=="运维复核人"){
			        	$('#btnBatchApprove').attr("disabled","disabled")
			        	$('#ops_audit .fault_audit').hide()
			        }
			},
			error:function(err){
				console.log(err)
			}
		});
	 })
	 //待审核
	 $('#ops_audit .btn-group #un_audit').click(function(){
	 	$(this).css({ "color": "#fff", "background": "#558ad8" })
	$(this).siblings().css({ "color": "#333", "background": "#fff" })
	 	var searchStatus=$(this).val()
	 	$('#ops_audit .public_search .searchStatus').val(searchStatus)
	 	var job_status=$('#ops_audit .public_search .searchStatus').val();
	 	var data={job_status:job_status}
	 	jobStatus(data)
	 })
	 //待复核
	 $('#ops_audit .btn-group #wait_review').click(function(){
	 	$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
	 	var searchStatus=$(this).val()
	 	$('#ops_audit .public_search .searchStatus').val(searchStatus)
	 	var job_status=$('#ops_audit .public_search .searchStatus').val();
	 	var data={job_status:job_status}
	 	jobStatus(data)
	 })
	 //全部
	 $('#ops_audit .btn-group #whole').click(function(){
	 	$(this).css({ "color": "#fff", "background": "#558ad8" })
		$(this).siblings().css({ "color": "#333", "background": "#fff" })
	 	var searchStatus=$(this).val()
	 	$('#ops_audit .public_search .searchStatus').val(searchStatus)
	 	var job_status=$('#ops_audit .public_search .searchStatus').val();
	 	var data={job_status:job_status}
	 	jobStatus(data)
	 })
	 function jobStatus(data){
	 	$.ajax({
			type:"get",
			url:"/ops_audit.json",
			data:data,
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, 1, originData);
		   		$('.page_total small').text(data.list_size);
		   		stucolor();
				$('.loading').hide()
		   		var roles=data.role_name;
			        console.log(roles)
			        if(roles=="运维成员"){
			        	$('#btnBatchApprove').attr("disabled","disabled")
						$('#ops_audit .fault_audit').hide()
			        }
			        else if(roles=="运维复核人"){
			        	$('#btnBatchApprove').attr("disabled","disabled")
			        	$('#ops_audit .fault_audit').hide()
			        }
			},
			error:function(err){
				console.log(err)
			}
		});
	 }
	 
	 
	//审核
	var id;
	var status
	$('body').delegate('.fault_audit','click',function(){
		
	       id=$(this).parents('tr').find('.id').text();
	     
	       $.ajax({
    			type:"get",
    			url:'/ops_audit/'+id + '.json',
    			success:function(data){
    				var dataObj = {
						editData:data
					}
    				console.log(dataObj)
					var originData = template('faultAudit',dataObj)
					$('#ops_audits').html(originData)
					
					
					
					status=data.ops_audit.job_status;
					var reviewIf=data.ops_audit.review_if;
					var libg=data.flow;
 				console.log(status)
 				
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					$("#ops_audits .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				}
 				$('#ops_audits .dTaskTop li').each(function(i){
// 				
   					var libg=$(this).find('.lbg-txt').text();
   				
   					if(status==libg){
   						
   						var bgs=$(this).index()
   						var lis = $('.dTaskTop li');
   						
   						for(var j=0;j<=bgs;j++){
   					
							lis[j].className="on";
   						}
   					}
   				})
 				$('#ops_audits .dTaskTop li').eq(0).addClass("on")
 				
 				if(reviewIf=="Y"){
						$('#ops_audits #review #audit_des').hide();
						$('#ops_audits #review_adopt #ApproveY').prop('checked',true);
					}else{
						$('#ops_audits #review #audit_des').show();
						$('#ops_audits #review_adopt #ApproveN').prop('checked',true);
					}
// 				$('#lookXun .look_insp img').zoomify();
					var imgs = document.getElementsByTagName("img");
					imgs[0].focus();
					for(var i = 0;i<imgs.length;i++){
						imgs[i].onclick = expandPhoto;
						imgs[i].onkeydown = expandPhoto;
					}
					imgSrc(status)
					$('#ops_looks').hide()
			       $('#ops_audit').hide();
			       $('#ops_review').hide();
			       $('#ops_audits').show();
    			},
    			error:function(err){
    				console.log(err)
    			}
    		});
	         
	})
	//审核保存
		$('body').delegate('#ops_audits .save','click',function(){
			
			var audit_if=$('#ops_audits #audit_adopt .checkbox-radio input[name="Approve"]:checked').val();
			var audit_des=$('#ops_audits #audit_not').val();
			console.log(audit_if)
			console.log(audit_des)
 			$.ajax({
 				type:"put",
 				url:'/ops_audit/'+id + '.json',
 				data:{
 					audit_if:audit_if,
 					audit_des:audit_des
 				},
 				success:function(data){
 					page_state = "change";
					alert("提交成功");
					originData();
					$('#ops_audit').show()
	    			$('#ops_audits').hide()
	    			$('#ops_looks').hide();
 				},
 				error:function(err){
 					console.log(err)
 				}
 			});
 		})
		//复核
		$('body').delegate('.fault_review','click',function(){
			id=$(this).parents('tr').find('.id').text();

	       $.ajax({
    			type:"get",
    			url:'/ops_audit/'+id + '.json',
    			success:function(data){
    				var dataObj = {
						editData:data
					}
    				console.log(dataObj)
					
					
					var originData = template('faultRevew',dataObj)
					$('#ops_review').html(originData)
					
					status=data.ops_audit.job_status;
					var libg=data.flow;
 				console.log(status)
 				
 				
 				for(var i = 0; i<libg.length; i++){
 					var industry = libg[i];
 					$("#ops_review .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
 				}
 				$('#ops_review .dTaskTop li').each(function(i){
   					var libg=$(this).find('.lbg-txt').text();
   				
   					if(status==libg){
   					
   						var bgs=$(this).index()
   						var lis = $('.dTaskTop li');
   						
   						for(var j=0;j<=bgs;j++){
   					
							lis[j].className="on";
   						}
   					}
   				})
 				$('#ops_review .dTaskTop li').eq(0).addClass("on")
					var imgs = document.getElementsByTagName("img");
					imgs[0].focus();
					for(var i = 0;i<imgs.length;i++){
						imgs[i].onclick = expandPhoto;
						imgs[i].onkeydown = expandPhoto;
					}
					imgSrc(status)
					$('#ops_looks').hide()
			        $('#ops_audit').hide();
				    $('#ops_audits').hide(); 
				    $('#ops_review').show(); 
    			},
    			error:function(err){
    				console.log(err)
    			}
    		});
	       
		})
		//复核保存
		$('body').delegate('#ops_review .save','click',function(){
			
			var audit_if=$('#ops_review #review_adopt .checkbox-radio input[name="Approve"]:checked').val();
			var audit_des=$('#ops_review #audit_not').val();
			console.log(audit_if)
			console.log(audit_des)
			if(audit_if=="N" && audit_des==''){
				alert("请填写复核不通过原因")
			}else{
				$.ajax({
	   				type:"get",
	   				url:'/ops_audit/'+id + '/review.json',
	   				data:{
	   					audit_if:audit_if,
	   					audit_des:audit_des
	   				},
	   				success:function(data){
	   					page_state = "change";
						alert("提交成功");
						originData();
						$('#ops_audit').show()
		    			$('#ops_audits').hide()
		    			$('#ops_looks').hide();
		    			$('#ops_review').hide();
	   				},
	   				error:function(err){
	   					console.log(err)
	   				}
	   			});	
			}
   			
 		})

		//点击检查项弹出电子表格
		var typeId;
		var typeName;
		$('body').delegate('.edit_jobs .seeTypes .handle_name','click',function(){
			
			typeId=$(this).siblings('.typesId').text();
			typeName=$(this).children('span').html()

			var Data_DZM = {
                        id:id,
                        name:typeName
                }
                cliclType(Data_DZM);
              
                $('.edit_jobs').hide();
				$('#sampling').hide();
				$('#weighing').hide();
                  $('#typeListBox').show();
               
		})
		$('body').delegate('#ops_review .seeTypes .handle_name','click',function(){
			
			typeId=$(this).siblings('.typesId').text();
			typeName=$(this).children('span').html()

			var Data_DZM = {
                        id:id,
                        name:typeName
                }
                cliclType(Data_DZM);
              
                $('#ops_review .edit_jobs').hide();
				$('#ops_review #sampling').hide();
				$('#ops_review #weighing').hide();
                  $('#ops_review #typeListBox').show();
               
		})
		//根据不同的code连接不同的表格
		function cliclType(Data_DZM){
			
			var url = '';
			//周
			if(typeId =="wk_003"){//站点巡检记录
				url = "/get_form_module/station_inspection_week";
			}else if(typeId =="wk_010"){//其他仪器、设备运行状况检查记录
				url = "/get_form_module/run_status_week";
			}else if(typeId =="wk_001"){//每周(次)巡检工作汇总表
				url = "/get_form_module/inspection_work_week";
			}else if(typeId =="wk_009" || typeId =="wk_008"){
//				颗粒物PM2.5自动监测分析仪运行状况检查记录\颗粒物PM10自动监测分析仪运行状况检查记录
				url = "/get_form_module/pm_check_week";
			}else if(typeId =="wk_002"){//长光程(SO2,NO2,O3)分析仪器运行状况检查记录表
				url = "/get_form_module/optical_distance_week";
			}else if(typeId =="wk_006"){//臭氧（O3）分析仪运行状况检查记录表
				url = "/get_form_module/analyser_status_week";
			}else if(typeId =="wk_005"){//氮氧化物（NOX）分析仪运行状况检查记录表
				url = "/get_form_module/nox_analyser_status_week";
			}else if(typeId =="wk_004"){//二氧化硫（SO2）分析仪运行状况检查记录表（每周）    
				url = "/get_form_module/so2_analyser_status_week";
			}else if(typeId =="wk_007"){//一氧化碳（CO）分析仪运行状况检查记录表
				url = "/get_form_module/co_analyser_status_week";
			}
			//月
			else if(typeId =="mh_001"){//站点设备维护记录
				url = "/get_form_module/site_plant_maintenance_month";
			}else if(typeId =="mh_002"){//气体分析仪流量检查记录表
				url = "/get_form_module/gas_analyzer_FlowCheck_month";
			}else if(typeId =="mh_003"){//多气体动态校准仪校准检查记录表
				url = "/get_form_module/multiGas_dynamic_calibrator_month";
			}
			//季度
			else if(typeId =="jd_001"|| typeId =="jd_002" || typeId =="jd_003" || typeId =="jd_004"){
				//气体分析仪（二氧化硫、臭氧、二氧化氮、一氧化碳）多点校准记录表（每季度）
				url = "/get_form_module/multipoint_calibration_quarter";
			}else if(typeId =="jd_005"|| typeId =="jd_008" || typeId =="jd_006" || typeId =="jd_007"){
				//（二氧化硫、臭氧、二氧化氮、一氧化碳)仪器精密度审核记录表（每季度）
				url = "/get_form_module/instrument_precision_review_quarter";
			}else if(typeId =="jd_009"){
//				站点设备清洁记录（每季度）
				url = "/get_form_module/site_equipment_cleaning_quarter";
			}else if(typeId =="jd_010"){
				//颗粒物温度、压力校准记录表
				url = "/get_form_module/grain_temperature_pressure_calibration_quarter";
			}else if (typeId =="jd_011" || typeId =="jd_012"){
				//颗粒物PM10、PM2.5自动监测分析仪运行状况检查记录（每季度）   
				url = "/get_form_module/automatic_monitoring_analyzer_quarter";
			}else if(typeId =="jd_013" || typeId =="jd_014"){
				//长光程SO2\NO2分析仪单点校准检查记录表（每季度）
				url = "/get_form_module/long_path_analyzer_quarter";
			}else if(typeId =="jd_015"){
				//长光程O3分析仪单点校准检查记录表（每季度）
				url = "/get_form_module/o3long_path_analyzer_quarter";
			}
			//半年
			else if(typeId =="yr_001"){
				//站点设备半年维护记录（每半年）
				url = "/get_form_module/site_equipment_maintain_year";
			}else if(typeId =="yr_002"){
				//氮氧化物分析仪钼炉转化率记录表（每半年）
				url = "/get_form_module/nox_analyzer_mofce_year";
			}else if(typeId =="yr_003"){
				//多气体动态校准仪校准检查记录表
				url = "/get_form_module/multi_gas_dynamic_calibrator_year";
			}else if(typeId =="yr_005"){
				//能见度分析仪校准记录表（每半年）  
				url = "/get_form_module/visibility_analyzer_calibration_year";
			}else if(typeId =="yr_004"){
				//臭氧（O3）校准仪（工作标准）量值传递记录表    
				url = "/get_form_module/o3_calibrator_value_transfer";
			}else if(typeId=="mh_004"){
				// 颗粒物手工比对采样记录表（每月）
				url="/get_form_module/manual_comparison_particulates_month"
			}
			
                $.ajax({
                    type:"get",
					url:url,
                    data:Data_DZM,
                    async:true,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
                    	$('#ops_audits #typeListBox').html(data)
					   $('#ops_looks #typeListBox').html(data)
					   $('#ops_review #typeListBox').html(data)
              
					   $('#typeListBox').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#typeListBox table input').attr("disabled",true)
						$('#typeListBox table textarea').attr("disabled",true)
						$('#typeListBox #Save').hide()
						$('#sampling #Save').hide()
						$('#sampling #Save').hide()
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
							todayButton:true,
							step:5
						});
						if(status=="工单待审核" || status=="工单已审核" || status=="工单待复核"){
							
							$('#typeListBox #supplyLook').show();
							$('#typeListBox #supplyEdit').hide();
						}else{
						
							$('#typeListBox #supplyEdit').show();
							$('#typeListBox #supplyLook').hide();
							
						}
						var imgs = document.getElementsByTagName("img");
						imgs[0].focus();
						for(var i = 0;i<imgs.length;i++){
							imgs[i].onclick = expandPhoto;
							imgs[i].onkeydown = expandPhoto;
						}
                       
                    },
                    error:function(err){
                        console.log(err);
                        $('.loading').hide();
                    }
                });
		}
		$('body').delegate('#typeListBox #Back','click',function(){
			$('.edit_jobs').show();
			$('#typeListBox').hide();
			$('#sampling').hide();
			$('#weighing').hide();
			$('.loading').hide();
		})
		$('body').delegate('#ops_review #typeListBox #Back','click',function(){
			$('#ops_review .edit_jobs').show();
			$('#ops_review #typeListBox').hide();
			$('#ops_review #sampling').hide();
			$('#ops_review #weighing').hide();
			$('.loading').hide();
		})
		//颗粒物手工比对采样记录表查看页面
		 $('body').delegate(' #typeListBox table .btn-sm','click',function(){
			$('.edit_jobs').hide();
			$('#typeListBox').hide();
			$('#weighing').hide();
				var typeName=$(this).find('i').attr('sampling_record');
				$.ajax({
                    type:"get",
					url:"/get_form_module/sampling_record_table",
                    data:{
                    	id:id,
                    	code:typeName
                    },
                    async:true,
                    cache:false,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
					   $('#sampling').html(data);
						  var fu=typeName.substring(16)
							var fujianID="weigh_record_"+fu
							console.log(fujianID)
						 $('#sampling .fujianID').val(fujianID)  
					   $('#sampling').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#sampling table input').attr("disabled",true)
						$('#sampling table textarea').attr("disabled",true)
						$('#sampling #Save').hide();
						$('#weighing #Save').hide();
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
							todayButton:true,
							step:5
						});
                       
                    },
                    error:function(err){
                        console.log(err);
                        $('.loading').hide();
                    }
				});
				$(' #sampling').show();

		 })	
		 $('body').delegate('#sampling #Back','click',function(){
			$('.edit_jobs').hide();
			$('#typeListBox').show();
			$('#sampling').hide();
			$('#weighing').hide();
			$('.loading').hide();
		})
//采样记录表附件表
 $('body').delegate(' #sampling input[name="fujian"]','click',function(){
	$(' .edit_jobs').hide();
			$('#typeListBox').hide();
			$(' #sampling').hide();
				var typeName=$('#sampling .fujianID').val();
				$.ajax({
                    type:"get",
					url:"/get_form_module/weighing_record_table",
                    data:{
                    	id:id,
                    	code:typeName
                    },
                    async:true,
                    cache:false,
                    beforeSend:function(){
                        $('.loading').show();
                    },
                    success:function(data){
					   $('#weighing').html(data);
						  var fu=typeName.substring(16)
						 $('#sampling .fujianID').val("weigh_record_"+fu)  
					   $('#sampling').append('<div id="loading" class="loading">'+'数据正在努力匹配加载中，请稍后...'+'</div>')
                       $('#weighing table input').attr("disabled",true)
						$('#weighing table textarea').attr("disabled",true)
						$('#sampling #Save').hide();
						$('#weighing #Save').hide();
						$('.dateTimepicker').datetimepicker({
							format:"Y-m-d",
							todayButton:true,
							timepicker:false
						});
                       	$('.dateTimepickerH').datetimepicker({
							format:"Y-m-d H:i",
							todayButton:true,
							step:5
						});
                       
                    },
                    error:function(err){
                        console.log(err);
						$('.loading').hide();
						
                    }
				});
				
				$('#weighing').show();
 })
 //采样记录表附件表返回
	$('body').delegate('#weighing #Back','click',function(){
		$('#sampling').show();
		$('#weighing').hide();
		$('.edit_jobs').hide();
		$('#typeListBox').hide();
		$('.loading').hide();
	})	
	//审核不通过的原因显示和隐藏
		$('body').delegate('#ApproveY','click',function(){
			$('.audit_des').hide()
		})
		$('body').delegate('#ApproveN','click',function(){
			$('.audit_des').show()
		})
 		$('body').delegate('#ops_audits .saveback','click',function(){
	        $('#ops_audit').show()
	        $('#ops_review').hide();
	    	$('#ops_audits').hide()
	    	$('#ops_looks').hide();
		})
 		$('body').delegate('#ops_review .saveback','click',function(){
	        $('#ops_audit').show()
	        $('#ops_review').hide();
	    	$('#ops_audits').hide()
	    	$('#ops_looks').hide();
		})
 		//批量审核
 		$('body').delegate('#ops_audit #btnBatchApprove','click',function(){
 			$('.loading').show()
 			var ids=[];
			var id=$('.exception-list td input[type="checkbox"]');
			for(var i=0;i<id.length;i++){
				if(id[i].checked){
					ids.push($(id[i]).val());
				}
			}
			$('.ID').val(ids);
			var auditId=$('.ID').val();
			console.log(ids)
			if(ids.length>0) {
				$.ajax({
				type:"get",
				url:"/ops_audit/batch_audit.json",
				data:{
					batch_audit_ids:ids
				},
				success:function(data){
					data_hash = data
					page_state = "change";
					originData();
				$('.loading').hide()
	   		 	$('#ops_audit .public_table .jobStu').each(function(i){
					var s=$(this).text()
					if(s=="工单待审核"){
		//				$(this).siblings('td').find('input').attr('disabled',true)
					}else{
						$(this).siblings('td').find('input').attr('disabled','disabled')
						$(this).siblings('td').find('.fault_audit').hide()
					}
				})
		   		
				},
				error:function(err){
					console.log(err)
				}
		
			});
			}else{
				alert('请至少选择一个工单');
				$('.loading').hide()
			}
			
 		})
 		//详情
 		$('body').delegate('#ops_audit .public_table .fault_look','click',function(){
 			
 			$('#ops_audit').hide();
 			$('#ops_audits').hide();
 			id=$(this).parents('tr').find('.id').text();
 			$.ajax({
				type:'get',
				url:'/ops_audit/'+id + '.json',
				success:function(data){
					var chekAudit=data.ops_audit.audit_if;
					var reviewIf=data.ops_audit.review_if;
					status=data.ops_audit.job_status;
					console.log(chekAudit)
					var dataObj = {
						editData:data
					}
					var originData = template('opsLook',dataObj)
					$('#ops_looks').html(originData)
					$('#ops_looks').show();
					var libg=data.flow;
 				
	 				for(var i = 0; i<libg.length; i++){
	 					var industry = libg[i];
	 					$("#ops_looks .dTaskTop ul").append(" <li>"+"<span class='round'></span>"
	 					+"<span class='line_bg lbg-l'></span>"+"<span class='line_bg lbg-r'></span>"
	 					+"<p class='lbg-txt'>"+industry+"</p>"+"</li>");
	 				}
	 				$('#ops_looks .dTaskTop li').each(function(i){
			
	   					var libg=$(this).find('.lbg-txt').text();
	   					if(status==libg){
	   						
	   						var bgs=$(this).index()
	   						var lis = $('#ops_looks .dTaskTop li');
	   						
	   						for(var j=0;j<=bgs;j++){
	   						
								lis[j].className="on";
	   						}
	   					}
	   				})
					if(chekAudit=="Y"){
						$('#ops_looks #audit #audit_des').hide();
						$('#ops_looks #audit_adopt #Approve_Y').attr('checked',true);
					}else{
						$('#ops_looks #audit #audit_des').show();
						$('#ops_looks #audit_adopt #Approve_N').attr('checked',true);
					}
					if(reviewIf=="Y"){
						$('#ops_looks #review #audit_des').hide();
						$('#ops_looks #review_adopt #ApproveY').prop('checked',true);
					}else{
						$('#ops_looks #review #audit_des').show();
						$('#ops_looks #review_adopt #ApproveN').prop('checked',true);
					}
					if(status=="工单待审核"){
						$('#ops_looks #audit').hide();
					}else if(status=="工单待复核"){
						$('#ops_looks #review').hide();
						$('#ops_looks #audit').hide();
					}

					var imgs = document.getElementsByTagName("img");
					imgs[0].focus();
					for(var i = 0;i<imgs.length;i++){
						imgs[i].onclick = expandPhoto;
						imgs[i].onkeydown = expandPhoto;
					}
					imgSrc(status)
				},
				error:function(err){
					console.log(err)
				}
			})
 		})
 		//检查项前显示的图片
	function imgSrc(job_status){
		if(job_status=="工单待审核" || job_status=="工单已审核"){
			$('.look_insp .handle_name img').attr('src','/images/com.png'); 
		}else{
			$('.look_insp .handle_name img').attr('src','/images/undo.png');
		}
	}
 		$('body').delegate('#ops_audit_look #look_back','click',function(){
 			$('#ops_looks').hide();
 			$('#ops_audits').hide();
 			$('#ops_audit').show();
 		})
 		//图片放大
 function expandPhoto(){
	var overlay = document.createElement("div");
	overlay.setAttribute("id","overlay");
	overlay.setAttribute("class","overlay");
	document.body.appendChild(overlay);

	var img = document.createElement("img");
	img.setAttribute("class","overlayimg");
	img.src = this.getAttribute("src");
	document.getElementById("overlay").appendChild(img);

	img.onclick = restore;
}
function restore(){
	document.body.removeChild(document.getElementById("overlay"));
	document.body.removeChild(document.getElementById("img"));
}
	//	分页+初始数据
	function originData(param){
//		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('opsTables',data_hash);
			$('table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var station_ids = $('#ops_audit .public_search .searchSta').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }
			$.ajax({
				url: '/ops_audit.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
				data:{
						d_station_id:station_ids,
						job_no:$('#ops_audit .public_search .searchNo').val(),
						author:$('#ops_audit .public_search .searchAuthor').val(),
						created_time:$('#ops_audit .public_search .startTime1').val(),
						end_time:$('#ops_audit .public_search .endTime1').val(),
						handle_man:$('#ops_audit .public_search .searchHan').val(),
						create_type:$('#ops_audit .public_search .searchCrea').val(),
						job_status:$('#ops_audit .public_search .searchStatus').val(),
						fault_type:$('#ops_audit .public_search .searchFault').val(),
						polling_ops_type:$('#ops_audit .public_search .searchOps').val()
					},
		    	success:function(data){
		    		console.log(data)
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('opsTables',data);
					$('table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
					stucolor();
					var roles=data.role_name;
			        console.log(roles)
			        if(roles=="运维成员"){
			        	$('#btnBatchApprove').attr("disabled","disabled")
						$('#ops_audit .fault_audit').hide()
			        }
			        else if(roles=="运维复核人"){
			        	$('#btnBatchApprove').attr("disabled","disabled")
			        	$('#ops_audit .fault_audit').hide()
			        }
					
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/ops_audit.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
		stucolor();
		var roles=data.role_name;
        console.log(roles)
        if(roles=="运维成员"){
        	$('#btnBatchApprove').attr("disabled","disabled")
			$('#ops_audit .fault_audit').hide()
        }
        else if(roles=="运维复核人"){
        	$('#btnBatchApprove').attr("disabled","disabled")
        	$('#ops_audit .fault_audit').hide()
        }
	})
	//首页工单状态显示的颜色和功能
	function stucolor(){
		$('#ops_audit .public_table .jobStu').each(function(i){
			var s=$(this).text()
			if(s=="工单待审核"){
				$(this).css({ "color": "#fff", "background": "#ff9d73" })
				$(this).siblings('td').find('.fault_review').hide()
//				$(this).siblings('td').find('input').attr('disabled',true)
			}else if(s=="工单已审核"){
				$(this).css({ "color": "#fff", "background": "#558ad8" })
				$(this).siblings('td').find('input').attr('disabled','disabled')
				$(this).siblings('td').find('.fault_audit').hide()
				$(this).siblings('td').find('.fault_review').hide()
		}
else if(s=="工单待复核"){
				$(this).css({ "color": "#fff", "background": "#7ecef4" })
				$(this).siblings('td').find('input').attr('disabled','disabled')
				$(this).siblings('td').find('.fault_audit').hide()
			}
		})
	}
//	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/ops_audit.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
})
