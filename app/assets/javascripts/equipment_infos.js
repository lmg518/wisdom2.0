 $(function(){
	
	//品牌
	$.getJSON('/supplies.json').done(function(data){
		var brandTemp=template('brandList',data)
		$('#equipment_infos .form-group .brand').html(brandTemp);
		$('#equipmentAdd .form-group .brand').html(brandTemp);
		$('#equipmentEdit .form-group .brand').html(brandTemp);
		
	})
	//站点
	$.ajax({
		type:"get",
		url:"/all_stations_area.json",
		success:function(data){
			var stationTemp = template('Addstations',data);
			$('#equipmentAdd #sq_radioStation .station_contents').html(stationTemp);
			
		},
		error:function(err){
			console.log(err)
		}
	});
	//查询
	var sypply_name='';
	var supply_no='';
	var brand='';
	
	function pageName_Data(){
		
		$('#equipment_infos .public_search .searchName').val($('.public_search #device_name option:selected').val());
		$('#equipment_infos .public_search .searchNo').val($('.public_search .supply_no').val());
		var searchBrand=$('.public_search .brand option:selected').val();
		$('#equipment_infos .public_search .searchBrand').val(searchBrand);	
		sypply_name=$('#equipment_infos .public_search .searchName').val();
		supply_no=$('#equipment_infos .public_search .searchNo').val();
		brand=$('#equipment_infos .public_search .searchBrand').val();
	}
	$('.public_search .search').click(function(){
		sypply_name='';
		supply_no='';
		brand='';
		
		pageName_Data();
		$.ajax({
			type:"get",
			url:"/equipment_infos.json",
			data:{
				equip_type:sypply_name,
				equip_brands:brand,
				equip_nums:supply_no,
				
			},
			success:function(data){
				data_hash = data
				$('.loading').hide()
	   		 	$("#page").initPage(data.list_size, "1", originData);
		   		$('.page_total small').text(data.list_size);
		   		equimentTypes()
			},
			error:function(err){
				
			}
		});
	})
	
	//增加
 	$('body').delegate('#add','click',function(){
		$('#equipmentEdit').hide();
		$('#equipmentAdd').show();
		$('#equipment_infos').hide();
	})
 	$('#equipmentAdd #Types_1').click(function(){
 		
 		$('#equipmentAdd #typeStation').hide()
 		$('#equipmentAdd .addControl_station').val('')
 		$('#equipmentAdd .addControl_stationID').val('')
 	})
 	$('#equipmentAdd #Types_2').click(function(){
 		$('#equipmentAdd #typeStation').show()
 	})
 	$('#equipmentEdit #Types_1').click(function(){
 	
 		$('#equipmentEdit #typeStation').hide()
 		$('#equipmentEdit .addControl_station').val('')
 		$('#equipmentEdit .addControl_stationID').val('')
 	})
 	$('#equipmentEdit #Types_2').click(function(){
 		$('#equipmentEdit #typeStation').show()
 	})
 		$(document).ready(function() {
	    $('#add_form').bootstrapValidator({
	        message: '必输入项',
	        fields: {
	            equip_code: {
	                validators: {
	                    notEmpty: {
	                        message: '设备型号不能为空'
	                    }
	                }
	            }
	           
	            
	        }
	    });
	});
 	//增加保存
 	$('body').delegate('#equipmentAdd #addSave','click',function(){
 		var addValidator = $('#add_form').data('bootstrapValidator');
		addValidator.validate();
		var equip_type=$('#equipmentAdd #add_form #device_name option:selected').val();
		var equip_brands=$('#equipmentAdd #add_form .brand option:selected').val();
		var equip_code=$('#equipmentAdd #add_form .equip_code').val();
		var equip_nums=$('#equipmentAdd #add_form .DeviceCode').val();
		var purchase_time=$('#equipmentAdd #add_form .purchase_time').val();
		var typs=$('#equipmentAdd #add_form #Types input[type="radio"]:checked').val();
		if($('#equipmentAdd #add_form #typeStation .addControl_stationID').val()){
			stationid=$('#equipmentAdd #add_form #typeStation .addControl_stationID').val();
		}else{
			stationid=''
		}
	if (addValidator.isValid()){
		$.ajax({
			type:"post",
			url:"/equipment_infos.json",
			data:{
				equipment_info:{
					equip_type:equip_type,
					equip_brands:equip_brands,
					equip_code:equip_code,
					equip_nums:equip_nums,
					purchase_time:purchase_time,
					types:typs,
					d_station_id:stationid
				}
			},
			success:function(){
				page_state = "change";
					alert("提交成功");
   					originData()
   					$('#equipmentAdd').hide();
   					$('#equipment_infos').show();
					$('#equipmentEdit').hide();
					document.getElementById("add_form").reset()
//					$('#add_form').data('bootstrapValidator').resetForm(true);
			},
			error:function(err){
				console.log(err)
			}
		});
	}
		
		
	})
 	$('#equipmentAdd #back').click(function(){
 		$('#equipmentAdd').hide();
		$('#equipment_infos').show();
		$('#equipmentEdit').hide();
 	})
 	//修改
 	var id;
	$('body').delegate('#equipment_infos #public_table .edit','click',function(){
		$('#equipment_infos').hide();
		$('#equipmentAdd').hide();
		id=$(this).parents('tr').find('.id').text();
		console.log(id)
		$.ajax({
			type:"get",
			url:'/equipment_infos/'+id+'.json',
			success:function(data){
				console.log(data)
				var equipType=data.equipment_info.equip_type;
				var equip_brands=data.equipment_info.equip_brands;
				var equip_code=data.equipment_info.equip_code;
				var equip_nums=data.equipment_info.equip_nums;
				var purchase_time=data.equipment_info.purchase_time;
				var typs=data.equipment_info.types;
				$("#equipmentEdit .equip_code").val(equip_code)
				$("#equipmentEdit .DeviceCode ").val(equip_nums)
				$("#equipmentEdit .purchase_time ").val(purchase_time)
				if(typs=="Y"){
					$("#equipmentEdit #Types #Types_2").attr("checked",true)
					$("#equipmentEdit  #typeStation").show();
				}else if(typs=="N"){
					$("#equipmentEdit #Types #Types_1").attr("checked",true)
					$("#equipmentEdit  #typeStation").hide();
				}
				$("#equipmentEdit #device_name option").each(function(){  
			        if($(this).text() == equipType){  
			            $(this).attr("selected","selected");  
			        }  
			    });
			    $("#equipmentEdit .brand option").each(function(){  
			        if($(this).text() == equip_brands){  
			            $(this).attr("selected","selected");  
			        }  
			    }); 
				
//				$("#equipmentEdit #device_name").find("option[value=equipType]").attr("selected","selected");  
 				
				$('#equipmentEdit').show();
			},
			error:function(err){
				console.log(err)
			}
		});
	})
	$('#equipmentEdit #back').click(function(){
 		$('#equipmentAdd').hide();
		$('#equipment_infos').show();
		$('#equipmentEdit').hide();
 	})
//	编辑保存
	$('body').delegate('#equipmentEdit #editSave','click',function(){
		var equip_type=$('#equipmentEdit #edit_form #device_name option:selected').val();
		var equip_brands=$('#equipmentEdit #edit_form .brand option:selected').val();
		var equip_code=$('#equipmentEdit #edit_form .equip_code').val();
		var equip_nums=$('#equipmentEdit #edit_form .DeviceCode').val();
		var purchase_time=$('#equipmentEdit #edit_form .purchase_time').val();
		
		$.ajax({
			type:"put",
			url:'/equipment_infos/'+id+'.json',
			data:{
				equipment_info:{
					equip_type:equip_type,
					equip_brands:equip_brands,
					equip_code:equip_code,
					equip_nums:equip_nums,
					purchase_time:purchase_time,
					
				}
			},
			success:function(){
				page_state = "change";
					alert("提交成功");
   					originData()
   					$('#equipmentAdd').hide();
   					$('#equipment_infos').show();
					$('#equipmentEdit').hide();
			},
			error:function(err){
				console.log(err)
			}
		});
		
	})
	//删除
	$('body').delegate("#equipment_infos .delete","click",function(){
		id=$(this).parents('tr').find('.id').text();
		if(confirm("确定要删除吗？")){
			$.ajax({
				type:"delete",
				url:"/equipment_infos/"+ id+".json",
				
				
				success:function(data){
					page_state = "change";
					originData();
					alert("删除成功")
				},
				error:function(err){
					console.log(err)
				}
			});
		}
	})
 	
 	
 		//	分页+初始数据
	function originData(param){
		// pageName_Data();
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('equipment_table',data_hash);
			$('table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
			data_hash = {};
			
			$.ajax({
				url: '/equipment_infos.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					sypply_name:$('#equipment_infos .public_search .searchName').val(),
					brand:$('#equipment_infos .public_search .searchBrand').val(),
					supply_no:$('#equipment_infos .public_search .searchNo').val(),
				},
		
		    	success:function(data){
		    		$('.loading').hide()
		    		var htmlData = template('equipment_table',data);
					$('table .exception-list').html(htmlData);
					var total = data.list_size;
					$('.page_total span small').text(total)
					$('.page_total span a').text(pp);
					equimentTypes()
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/equipment_infos.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	    equimentTypes()
	})
	//每页显示更改
    $("#pagesize").change(function(){
        $.ajax({
            type: "get",
            url: '/equipment_infos.json?page=' + $('#page .pageItemActive').html() + '&per=' + $('#pagesize option:selected').val(),
            async: true,
            dataType: "json",
            success: function(data) {
                data_hash = data;
                $('.loading').hide()
                var total = data.list_size;
                $("#page").initPage(total, 1, originData)
                $('.page_total span small').text(total)
            },
            error:function(err){
                console.log(err)
            }
        })
    })
    function equimentTypes(){
    	$('#equipment_infos .public_table .types').each(function(){
    		var types=$(this).html();
    		if(types=="正式"){
    			$(this).parents('tr').find('.delete').hide();
    		}else if(types=="备机"){
    			$(this).parents('tr').find('.delete').show();
    		}
    	})
    }
 })