$(function(){
	//	站点
	$.getJSON('/all_stations_area.json').done(function(data){
		var stationTemp = template('stations',data);
		$('#sq_station .station_contents').html(stationTemp);
	})
	//查询
	$(document).delegate('.public_search .search','click',function(){
		$('.public_search .ids').val($('.public_search .control_stationIDs').val());
		if($('.public_search .ids').val()){
			stationid=$('.public_search .ids').val().split(",");
		}else{
			stationid=''
		}
		$.ajax({
			type:"get",
			url:"/create_station_plan.json",
			async:true,
			dataType:'json',
			data:{
				station_ids:stationid
			},
			success:function (data){
				data_hash = data
				$('.loading').hide()
		   		$("#page").initPage(data.list_size, 1, originData);
		   		$('.page_total small').text(data.list_size);
			},
			error:function(err){
				
				console.log(err);
			}
		});
	})
	//生成本月计划
	var id;
	$('body').delegate(".buildPlan","click",function(){
		
		id=$(this).parents('tr').find('.id').text();
		var month=0;
		if(confirm("确定要生成本月计划？")){
			$.ajax({
				type:"get",
				url:"/create_station_plan/create_plan",
				
				data:{
						month:month,
						id:id
				},
				beforeSend:function(){
					$('.loading').show();
				},
				success:function(data){
					console.log(data)
					var status=data.status;
					page_state = "change";
					originData()
					alert(status)
				},
				complete:function(){
					$('.loading').hide();
				},
				error:function(err){
					console.log(err)
				}
			});
		}
	})
		//	分页+初始数据
	function originData(param){
//		pageName_Data()
		var pp = $('#page .pageItemActive').html();
		var per=$('#pagesize option:selected').val();
		if(parseInt(pp) == 1 && page_state == "init"){
			var htmlData = template('create_stations',data_hash);
			$('table .exception-list').html(htmlData);
			$('.page_total span a').text(param);
		}else{
			$('.loading').show()
			page_state = "change";
            data_hash = {};
            var station_ids = $('.public_search .ids').val();
            if(station_ids){
            	station_ids = station_ids.split(",");
            }else{
            	station_ids=''
            }
			$.ajax({
				url: '/create_station_plan.json?page=' + pp+ '&per=' +per,
		    	type: 'get',
		    	data:{
					station_ids:station_ids
				},
		    	success:function(data){
		    		$('.loading').hide()
		    		var total = data.list_size;
		    		var htmlData = template('create_stations',data);
					$('table .exception-list').html(htmlData);
					$('.page_total span a').text(pp);
					$('.page_total span small').text(total)
		    	},
		    	error:function(err){
		    		console.log(err)	
		    	}
			})
		}
	};
	var data_hash = {};
    var page_state = "init";
	$.getJSON('/create_station_plan.json').done(function(data){
		$('.loading').hide()
		var total = data.list_size;
		data_hash=data;
	    $("#page").initPage(total, 1, originData)
	    $('.page_total span small').text(total)
	})
})
