module WeekTimeHelper

  def self.month_days(month)
    month = month.to_i
    return 31 if [1, 3, 5, 7, 8, 10, 12].include? month
    return 29 if month == 2 && Date.julian_leap?(Time.now.year)
    return 28 if month ==2 && !Date.julian_leap?(Time.now.year)
    return 30
  end

  def self.week_plan_create(t_time)

    Rails.logger.info "---3------"

    @week_arr = []
    @time = t_time
    @month_days = month_days(@time.month)
    @month_end_day = @time.end_of_month
    @month = @time.month
    @stations = DStation.active.uniq.pluck(:id)
    @station_count = @stations.length
    10.times do |i|
      t_fitst_day = @time.beginning_of_month + i.week
      w_month_end_day = (t_time + (-1.month)).end_of_month.wday  #上个月月的最后一天是周几
      
     # Rails.logger.info "------week:#{w_month_end_day}---------"

      if w_month_end_day == 0  #如果是周日 从当前周开始
        Rails.logger.info "-----1---------"
        week_first_day = t_fitst_day.beginning_of_week
        week_end_day = t_fitst_day.end_of_week
      else
        Rails.logger.info "-----2---------"
        week_first_day = t_fitst_day.beginning_of_week + 1.week
        week_end_day = t_fitst_day.end_of_week + 1.week
      end

      #根据天数选择生成的周一日期  2018-1月不对
      # case @month_days
      #   when 30
      #     week_first_day = t_fitst_day.beginning_of_week + 1.week
      #     week_end_day = t_fitst_day.end_of_week + 1.week
      #   when 31
      #     week_first_day = t_fitst_day.beginning_of_week + 1.week
      #     week_end_day = t_fitst_day.end_of_week + 1.week
      #   when 29
      #     week_first_day = t_fitst_day.beginning_of_week
      #     week_end_day = t_fitst_day.end_of_week
      #   when 28
      #     week_first_day = t_fitst_day.beginning_of_week + 1.week
      #     week_end_day = t_fitst_day.end_of_week + 1.week
      # end

      # week_first_day = t_fitst_day.beginning_of_week   #生成的日期有重复
      # week_end_day = t_fitst_day.end_of_week
      #已制定的 包括 待审核 和审核通过的
      handle_station = DOpsPlanManage.where(:d_station_id => @stations, :week_begin_time => week_first_day,:edit_status => ["Y","W"]).pluck(:d_station_id).uniq
     
      #运维审核页面显示审核通过/不通过的记录
      handle_station_y = DOpsPlanManage.where(:d_station_id => @stations, :week_begin_time => week_first_day,:edit_status => "Y").pluck(:d_station_id).uniq
      #审核通过的
      handle_w = DOpsPlanManage.where(:d_station_id => @stations, :week_begin_time => week_first_day,:edit_status => "W").pluck(:d_station_id).uniq
      #待上报的
      handle_o = DOpsPlanManage.where(:d_station_id => @stations, :week_begin_time => week_first_day,:edit_status => "O").pluck(:d_station_id).uniq
      
      #审核时间
      handle_station_w = DOpsPlanManage.where(:week_begin_time => week_first_day,:edit_status => "W").order(:created_at => :desc).take

      #上报时间
      handle_station_r = DOpsPlanManage.where(:week_begin_time => week_first_day,:report_if => "Y").order(:created_at => :desc).take
     
      # handle_station =  @stations.select{|x| x.d_ops_plan_manages.s_by_week(week_first_day).present? }
      if week_end_day >= @month_end_day && week_first_day < @month_end_day
        #Rails.logger.info "-------if------"
        @week_arr << {week_flag: "#{@month}月第#{i + 1}周",
                      week_rand: "#{week_first_day.strftime("%d")}日至#{week_end_day.strftime("%d")}日",
                      station_count: @station_count,  #总站点数
                      station_handle_ok: handle_station.length,  #已做计划的站点数
                      station_handle_un: @station_count - handle_station.length,

                      status_y: handle_station_y.length,  #待审核状态的
                      status_w: handle_w.length,          #已审核状态的
                      status_o: handle_o.length,          #待上报状态的

                      week_time: week_first_day.strftime("%Y-%m-%d %H:%M:%S"),
                      unit_name: "河南鑫属",
                      #unit_id: 17,  #运维单位id
                      handle_station: handle_station_y,   #已精上报的站点id 
                      
                      edit_status: handle_station_y.length!=0 ? "待审核" : "待分配",

                      audit_time: handle_station_w.present? ? handle_station_w.updated_at.strftime("%Y-%m-%d %H:%M:%S") : "" ,       #审核时间
                      #report_time: handle_station_r.present? ? handle_station_r.updated_at.strftime("%Y-%m-%d %H:%M:%S") : "" ,      #上报时间
                      report_time: handle_station_r.present? ? handle_station_r.report_time.strftime("%Y-%m-%d %H:%M:%S") : "" ,      #上报时间
 
        } if week_first_day <= @month_end_day
        break
      else
        #Rails.logger.info "-------else------"
        @week_arr << {week_flag: "#{@month}月第#{i + 1}周",
                      week_rand: "#{week_first_day.strftime("%d")}日至#{week_end_day.strftime("%d")}日",
                      station_count: @station_count,
                      station_handle_ok: handle_station.length,
                      station_handle_un: @station_count - handle_station.length,

                      status_y: handle_station_y.length,  #待审核状态的
                      status_w: handle_w.length,          #已审核状态的
                      status_o: handle_o.length,          #待上报状态的

                      week_time: week_first_day.strftime("%Y-%m-%d %H:%M:%S"),
                      unit_name: "河南鑫属",
                      #unit_id: 17,
                      handle_station: handle_station_y,
                      edit_status: handle_station_y.length!=0 ? "待审核" : "待分配",

                      audit_time: handle_station_w.present? ? handle_station_w.updated_at.strftime("%Y-%m-%d %H:%M:%S") : ""  ,      #审核时间
                      #report_time: handle_station_r.present? ? handle_station_r.updated_at.strftime("%Y-%m-%d %H:%M:%S") : "" ,      #上报时间
                      report_time: handle_station_r.present? ? handle_station_r.report_time.strftime("%Y-%m-%d %H:%M:%S") : "" ,      #上报时间
        }
      end
    end
   

    return @week_arr
  end

end