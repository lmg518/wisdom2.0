module YjzhAirHourAlarmHelper

  def self.check_abnormal_datas
    @station_exceptions = []
    @data_count = 0
    @level1, @level2, @level3 = 0, 0, 0
    #@now_time = Time.now.utc
    @now_time = Time.now
    @stations = DStation.active
    Rails.logger.info "-----时间1---#{@now_time}----------"
    @stations.each do |station|
      #  站点该小时的所有异常
      Rails.logger.info "------------异常开始时间----#{@now_time.beginning_of_hour.strftime("%Y-%m-%d %H:%M:%S")}---------"
      Rails.logger.info "------------异常开始时间----#{@now_time.end_of_hour.strftime("%Y-%m-%d %H:%M:%S")}---------"
     
      begin_time=@now_time.beginning_of_hour.strftime("%Y-%m-%d %H:%M:%S")
      end_time=@now_time.end_of_hour.strftime("%Y-%m-%d %H:%M:%S")
     
      #@data_abnormals = station.d_abnormal_data_yyyymms.where(:create_acce => (@now_time.beginning_of_hour.strftime("%Y-%m-%d %H:%M:%S"))..(@now_time.end_of_hour.strftime("%Y-%d-%m %H:%M:%S")))
      @data_abnormals = station.d_abnormal_data_yyyymms.where(:create_acce => (begin_time)..end_time)
      #@data_abnormals = station.d_abnormal_data_yyyymms.where(create_acce: (begin_time)..end_time)
      
      next if @data_abnormals.blank?
      @data_abnormals.each do |abnormal|
        #站点的该时段的报警
        #puts ">>>>#{abnormal.create_acce}11"
        #puts ">>>>#{Time.parse(abnormal.create_acce).beginning_of_hour}22"
        #@d_alarms = station.d_alarms.where(:first_alarm_time => (Time.parse(abnormal.create_acce).beginning_of_hour)..(Time.parse(abnormal.create_acce).end_of_hour))
        @d_alarms = station.d_alarms.where(:first_alarm_time => (begin_time)..end_time)

        if @d_alarms.present?
          Rails.logger.info "--------更新报警信息---------"
          @d_alarms.each do |alarm|
            alarm.update(:last_alarm_time => abnormal.create_acce, :continuous_alarm_times => @d_alarms.length)
            alarm.d_alarm_details.first.update(:last_sample_time => abnormal.create_acce) if alarm.d_alarm_details.present?
          end
        else
          #  没有报警直接产生一条报警,根据异常判断报警的级别
          Rails.logger.info "--------创建报警信息---------"
          @d_alarms.create(alarm_rule: abnormal.rule_instance, alarm_level: 3,
                           first_alarm_time: abnormal.create_acce, continuous_alarm_times: 1, status: 'waitting', s_alarm_rule_id: '')
        end
      end
    end
  end

  private


end