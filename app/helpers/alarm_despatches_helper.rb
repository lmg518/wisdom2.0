module AlarmDespatchesHelper


  def step_html(info)
    # case info
    #   when "待响应"
    #     ''
    # end
  end

  def d_task_deal_record_create(despatch)
    despatch.d_task_deal_records.create!(task_id: despatch.id,
                                     flow_no: 1,
                                     task_status: despatch.task_status,
                                     deal_login: despatch.receive_login,
                                     note: "#{despatch.despatch_login}分配给#{despatch.receive_login}任务.",
                                     deal_time: Time.now,
                                     deal_accept: "RW" + Time.now.strftime("%Y%m%d%H%M%S") + despatch.id.to_s )
  end

  def handle_despath(despatch)
    @flow_no = 0
    @notes = ""
    case despatch.task_status
      when "answer"
        @flow_no = 2
        @notes = "#{despatch.receive_login}响应了任务."
      when "doing"
        @flow_no = 3
        @notes = "#{despatch.receive_login}任务处理中."
      when "done"
        @flow_no = 4
        @notes = "#{despatch.receive_login}完成任务."
      when 'retweet'
        @flow_no = 0
        @notes = "#{despatch.receive_login}将任务转发."
      # when 'completed'
      #   @flow_no = 4
      #   @notes = "#{despatch.receive_login}完成任务."
      when 'audit'
        @flow_no = 5
        @notes = "#{despatch.receive_login}审核了任务."
      when "finish"
        @flow_no = 6
        @notes = "#{despatch.receive_login}该任务已结束."
    end
    despatch.d_alarms.update_all(:status => despatch.task_status)
    despatch.d_task_deal_records.create!(task_id: despatch.id,
                                         flow_no: @flow_no,
                                         task_status: despatch.task_status,
                                         deal_login: despatch.receive_login,
                                         note: @notes,
                                         deal_time: Time.now,
                                         deal_accept: "RW" + Time.now.strftime("%Y%m%d%H%M%S") + despatch.id.to_s )
  end

end