module EnergyFormHelper
    #拓样实业  能源报表，不进行差值计算的逻辑
    #   EnergyFormHelper.js_method_energy
    def self.js_method_energy(time, d_report_form_id, item)
        @steam_values = 0
        @steam_month_sum = 0

        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        @form_values = SNenrgyValue.where(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)  
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:field_code => item, :d_report_form_id => d_report_form_id)      
        
        #计算日产量
        if @form_values.present? && @form_values.length >0
            @form_values.each do |m|
                @steam_values += m.field_value.to_f
              end
        else
                @steam_values = 0
        end
        #计算月累计
        if @form_values_month.present? && @form_values_month.length >0
            @form_values_month.each do |m|
                @steam_month_sum += m.field_value.to_f
                end
        else
                @steam_month_sum  = 0
        end
        return @steam_values.round(2), @steam_month_sum.round(2)
    end


    def self.js_method_by_item_form_id_time(item, d_report_form_id, time)
        @steam_values = 0
        @steam_month_sum = 0

        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        @form_values = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)  
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:field_code => item, :d_report_form_id => d_report_form_id)      
        
        #计算日产量
        if @form_values.present? && @form_values.field_value.present?
            @steam_values = @form_values.field_value.to_f
        else
            @steam_values = 0
        end
        #计算月累计
        if @form_values_month.present? && @form_values_month.length >0
            @form_values_month.each do |m|
                @steam_month_sum += m.field_value.to_f
                end
        else
                @steam_month_sum  = 0
        end
        return @steam_values.round(2), @steam_month_sum.round(2)
    end




    def self.js_method_one_v2(time, s_region_code_id, d_report_form_id, item)
        @steam_values = 0      #当天的日用量
        @steam_month_sum = 0   #累计用量

        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        @form_values = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)  
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:field_code => item, :d_report_form_id => d_report_form_id)      
        
        #计算日产量
        if @form_values.present? && @form_values.field_value.present?
            @steam_values = @form_values.field_value.to_f
        else
            @steam_values = 0
        end

        #计算月累计
        if @form_values_month.present? && @form_values_month.length >0
        @form_values_month.each do |m|
            @steam_month_sum += m.field_value.to_f
            end
        else
            @steam_month_sum  = 0
        end

        return @steam_values.round(2), @steam_month_sum.round(2)
    end

    #不进行差值计算的方法
    def self.js_method_by_time_region_id_form_id(time, s_region_code_id, d_report_form_id)
        @steam_values = 0
        @steam_month_sum = 0

        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => time)  
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id)      
        
        #计算日产量
        if @form_values.present? && @form_values.length >0
            @form_values.each do |m|
                @steam_values += m.field_value.to_f
                end
        else
                @steam_values = 0
        end

        #计算月累计
        if @form_values_month.present? && @form_values_month.length >0
        @form_values_month.each do |m|
            @steam_month_sum += m.field_value.to_f
            end
        else
            @steam_month_sum  = 0
        end
        #默认 返回的是  true
        return @steam_values.round(2), @steam_month_sum.round(2)
    end

    #进行差值计算的方法
    def self.js_method_by_time_region_id_form_id_v1(time, s_region_code_id, d_report_form_id)
        @steam_values = 0
        @steam_month_sum = 0
      
        #计算差值
        bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
      
        @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => time) 
        @form_values_bf = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => bf_time)  
          
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id)                   
        min_time = @form_values_month.minimum("datetime")   #获取最小日期
        max_time = @form_values_month.maximum("datetime")   #获取最大日期
        @form_values_min = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => min_time)                  
        @form_values_max = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => max_time)                  
      
        d1 = 0
        d2 = 0
        @form_values.each do |m|
          d1 += m.field_value.to_f
        end
        @form_values_bf.each do |m|
          d2 += m.field_value.to_f
        end
        @steam_values = (d1 - d2).round(2)   #日用量
      
        dd1 = 0
        dd2 = 0
        @form_values_max.each do |m|
          dd1 += m.field_value.to_f
        end
        @form_values_min.each do |m|
          dd2 += m.field_value.to_f
        end
        @steam_month_sum = (dd1 - dd2).round(2)   #累计用量

        #默认 返回的是  true
        return @steam_values.round(2), @steam_month_sum.round(2)
    end


    def self.js_method_many_v2(time, s_region_code_id, d_report_form_id, item)
        @steam_values = 0
        @steam_month_sum = 0
        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => time, :field_code => item)  
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :field_code => item)      
        #计算日产量
        if @form_values.present? && @form_values.length >0
            @form_values.each do |m|
                @steam_values += m.field_value.to_f
                end
        else
                @steam_values = 0
        end

        #计算月累计
        if @form_values_month.present? && @form_values_month.length >0
        @form_values_month.each do |m|
            @steam_month_sum += m.field_value.to_f
            end
        else
            @steam_month_sum  = 0
        end
        #默认 返回的是  true
        return @steam_values.round(2), @steam_month_sum.round(2)
    end



end
