module YjzhAirMinHelper

  require 'rest-client'

  def self.get_data(begin_time,end_time)
    url = "http://172.168.10.7:3000/api/v1/air_min_data/station_min_data"
    headers = {appkey: ApiAuth::AIR_KEY, Authorization: "Token token=#{ApiAuth::AIR_TOKEN}", params: {begin_time:begin_time, end_time:end_time}}
    response = RestClient.get(url, headers)
    json = JSON.parse(response.body)
    @min5_datas = json["data"]
    @stations=DStation.active
    if @min5_datas.present?
        @stations.each do |s|
            @station_min5_data=@min5_datas.select{|h| h["station_id"] == s.id }     #遍历站点获取每个站点一个月的数据
            if @station_min5_data.present?

              Rails.logger.info "---Station_ID--#{s.id}------"

                @day_str=Time.parse(begin_time)

                24.times do |t|
                    @hour_data=@day_str + 3600 * t
                    begin_num=@hour_data.strftime("%Y%m%d%H%M")
                    end_num=@hour_data.strftime("%Y%m%d%H") << "55"
                    
                    Rails.logger.info "---Begin_num--#{begin_num.to_i}------"
                    Rails.logger.info "---End_num--#{end_num.to_i}------"

                    @day_data=@station_min5_data.select{|d| (begin_num.to_i..end_num.to_i) === d["data_time"].to_i }   #获取站点一天的数据
                    if @day_data.present?
                        Rails.logger.info "---Num--#{@day_data.size}------"
                        hour_num=@day_data.size
                        Rails.logger.info "-------创建5min数据统计------"

                        @d_data_analyze=DDataAnalyze.new(:station_name => s.station_name, 
                                                        :hour_standard_num => 12, 
                                                        :data_type => 300,
                                                        :data_time => @hour_data.strftime("%Y%m%d%H"),
                                                        :hour_num => hour_num, 
                                                        :d_station_id => s.id)
                        @d_data_analyze.save
                    end
                end
            end
        end
    end


    


  end






end