module PostReportsHelper


  #将日期范围，转为周形式
  # PostReportsHelper.week_str()
  def self.week_str(arr_time)
    # arr_time = ['2018-04-01', '2018-04-02', '2018-04-03', '2018-04-04', '2018-04-05',
    #             '2018-04-06', '2018-04-07', '2018-04-08', '2018-04-09', '2018-04-10', '2018-04-11', '2018-04-12',
    #             '2018-04-13', '2018-04-14', '2018-04-15', '2018-04-16', '2018-04-17'
    # ]
    s_arr = []
    s_t = []
    arr_time.each do |time|
      w = Time.parse(time).wday
      #Rails.logger.info "---w-----#{w}----"
      s_t << {time: time, wday: w}
      if w == 4
        s_arr << s_t
        s_t = []
      end
    end
    s_arr << s_t
    Rails.logger.info "---s-----#{s_arr}----"
    return s_arr
  end


  #将周数字日期转为字符日期
  def self.week_srt_day
    arrs = [["0", "1", "2", "3", "4"], ["5", "6", "0", "1", "2", "3", "4"], ["5", "6", "0", "1", "2"]]
    day = '2018-07-01'
    @begin_time = Time.parse(day)
    @end_time = Time.parse(day) + (arrs[0].size - 1).day
    days_week=[]
    arrs.each do |a|
      week = []
      @end_time += (a.size - 1).day
      Rails.logger.info "---end_time-----#{@begin_time}----"
      Rails.logger.info "---a.size-----#{a.size - 1}----"
      Rails.logger.info "---end_time-----#{@end_time}----"
      # end_time = (Time.parse(begin_time) + (a.size - 1).day).strftime("%Y-%m-%d")
      # Rails.logger.info "---end_time-----#{end_time}----"
      # Rails.logger.info "---end_time-----#{a.size - 1}----"
      # week.push(begin_time)
      # week.push(end_time)
    end


  end

  #车间盘点报表 计算方法

  #头粉小计 计算 = 干粉时耗包数*单位 + 湿粉时耗单位包数*单位
  #干粉时耗包数 = 昨天下余干粉的包数 + 今天购进的干粉包数 - 今天下余的干粉包数
  #湿粉时耗包数 = 昨天下余的湿粉包数 + 今天购进的湿粉包数 - 今天下余的湿粉包数
  #头粉 时耗 小计计算
  # PostReportsHelper.tf_total('2018-07-02',1)
  def self.tf_total(date_time, s_material_id)
    before_time = (Time.parse(date_time) + (-1.day)).strftime("%Y-%m-%d")
    #按日期范围查询值
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time)
    daily_report_before = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => before_time)

    if daily_report_before.present? && daily_report_now.present?
      b_xy_gf = daily_report_before.select {|x| x.field_code == 'dryMiillLeft'} #昨天 下余 干粉数量
      b_xy_sf = daily_report_before.select {|x| x.field_code == 'wetMillLeft'} #昨天 下余 湿粉数量

      n_gj_gf = daily_report_now.select {|x| x.field_code == 'dryMiillBuy'} #今天 购进 干粉
      n_gj_sf = daily_report_now.select {|x| x.field_code == 'wetMillInBuy'} #今天 购进 湿粉

      n_xy_gf = daily_report_now.select {|x| x.field_code == 'dryMiillLeft'} #今天 下余 干粉
      n_xy_sf = daily_report_now.select {|x| x.field_code == 'wetMillLeft'} #今天 下余 湿粉
      Rails.logger.info "-----头粉 计算数据 ---b_xy_gf#{b_xy_gf[0].field_value}----b_xy_sf-#{b_xy_sf[0].field_value}------n_gj_gf-#{n_gj_gf[0].field_value}---n_gj_sf#{n_gj_sf[0].field_value}----------n_xy_gf-#{n_xy_gf[0].field_value}----n_xy_sf#{n_xy_sf[0].field_value}---------"
      if b_xy_gf[0].field_value.present? && n_gj_gf[0].field_value.present? && n_xy_gf[0].field_value.present? && b_xy_sf[0].field_value.present? && n_gj_sf[0].field_value.present? && n_xy_sf[0].field_value.present?
        gf_nums = b_xy_gf[0].field_value + n_gj_gf[0].field_value - n_xy_gf[0].field_value #干粉时耗包数
        sf_nums = b_xy_sf[0].field_value + n_gj_sf[0].field_value - n_xy_sf[0].field_value #湿粉时耗包数
        tf_sum = (gf_nums * 1 + sf_nums * 0.53).round(2)
        Rails.logger.info "-----头粉 时耗小计 ---#{tf_sum}----"
        return tf_sum
      end
    end
  end

  # PostReportsHelper.tf_method('2018-07-02',1)
  def self.tf_method(date_time, s_material_id)
    before_time = (Time.parse(date_time) + (-1.day)).strftime("%Y-%m-%d")
    #按日期范围查询值
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time)
    daily_report_before = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => before_time)

    if daily_report_before.present? && daily_report_now.present?
      b_xy_gf = daily_report_before.select {|x| x.field_code == 'dryMiillLeft'} #昨天 下余 干粉数量
      b_xy_sf = daily_report_before.select {|x| x.field_code == 'wetMillLeft'} #昨天 下余 湿粉数量

      n_gj_gf = daily_report_now.select {|x| x.field_code == 'dryMiillBuy'} #今天 购进 干粉
      n_gj_sf = daily_report_now.select {|x| x.field_code == 'wetMillInBuy'} #今天 购进 湿粉

      n_xy_gf = daily_report_now.select {|x| x.field_code == 'dryMiillLeft'} #今天 下余 干粉
      n_xy_sf = daily_report_now.select {|x| x.field_code == 'wetMillLeft'} #今天 下余 湿粉

      #if b_xy_gf[0].field_value.present? && n_gj_gf[0].field_value.present? && n_xy_gf[0].field_value.present? && b_xy_sf[0].field_value.present? && n_gj_sf[0].field_value.present? && n_xy_sf[0].field_value.present?
      if b_xy_gf[0].present? && n_gj_gf[0].present? && n_xy_gf[0].present? && b_xy_sf[0].present? && n_gj_sf[0].present? && n_xy_sf[0].present?
        gf_nums = b_xy_gf[0].field_value + n_gj_gf[0].field_value - n_xy_gf[0].field_value #干粉时耗包数
        sf_nums = b_xy_sf[0].field_value + n_gj_sf[0].field_value - n_xy_sf[0].field_value #湿粉时耗包数
        tf_sum = (gf_nums * 1 + sf_nums * 0.53).round(2)
        #湿粉比例 = 时耗湿粉 * 0.53 /(时耗干粉 + 时耗湿粉 * 0.53)
        sf_bl = ((sf_nums * 0.53 / (gf_nums + sf_nums * 0.53)) * 100).round(2)
        # Rails.logger.info "-----干粉时耗包数---#{gf_nums}----"
        # Rails.logger.info "-----湿粉时耗包数---#{sf_nums}----"
        Rails.logger.info "-----头粉 时耗小计 ---#{tf_sum}----"
        return gf_nums, sf_nums, sf_bl, tf_sum
      end
    end
  end

  #头粉 周盘点计算  按时间范围查询
  # PostReportsHelper.week_js('2018-07-02','2018-07-03',1)
  def self.week_js(begin_time, end_time, s_material_id)
    daily_reports = DTwoMenuValue.where(:s_material_id => s_material_id).by_datetime(begin_time, end_time)
    #下余小计 计算
    if daily_reports.present?
      xy_sum = [] #多天的 下余
      gj_sum = [] #多天的 购进
      sh_sum = [] #多天的 时耗
      js_sum = [] #多天的 结算
      datetimes = daily_reports.pluck(:datetime).uniq.sort
      datetimes.each do |date|
        #下余 计算
        daily_date = daily_reports.select {|x| x.datetime == date}
        daily_gf = daily_date.select {|x| x.field_code == 'dryMiillLeft'}
        daily_sf = daily_date.select {|x| x.field_code == 'wetMillLeft'}
        if daily_gf[0].present? && daily_sf[0].present?
          daily_gf_v = daily_gf[0].field_value * daily_gf[0].ratio
          daily_sf_v = daily_sf[0].field_value * daily_sf[0].ratio
          xy_gf = daily_gf_v + daily_sf_v
          Rails.logger.info "---每天下余---#{xy_gf}----"
          xy_sum.push(xy_gf)
        end

        #购进 计算
        daily_gj_gf = daily_date.select {|x| x.field_code == 'dryMiillBuy'}
        daily_gj_sf = daily_date.select {|x| x.field_code == 'wetMillInBuy'}
        Rails.logger.info "---购进1---#{daily_gj_gf[0]}----"
        Rails.logger.info daily_gj_gf[0].inspect
        Rails.logger.info "---购进2---#{daily_gj_sf[0]}----"
        if daily_gj_gf[0].present? && daily_gj_gf[0].field_value && daily_gj_sf[0].present? && daily_gj_sf[0].field_value.present?
          daily_gj_gf_v = daily_gj_gf[0].field_value * daily_gj_gf[0].ratio
          daily_gj_sf_v = daily_gj_sf[0].field_value * daily_gj_sf[0].ratio
          gj_s = daily_gj_gf_v + daily_gj_sf_v
          Rails.logger.info "---每天购进---#{gj_s}----"
          gj_sum.push(gj_s)
        end

        #时耗 计算
        tf_sum = PostReportsHelper.tf_total(date, s_material_id)
        Rails.logger.info "---每天时耗---#{tf_sum}----"
        if tf_sum.present?
          sh_sum.push(tf_sum)
        end

        #结算 计算
        daily_js = daily_date.select {|x| x.field_code == 'Balance'}
        daily_js_v = daily_js[0].field_value
        if daily_js_v.present?
          js_sum.push(daily_js_v)
        end

        Rails.logger.info "---每天结算---#{daily_js_v}----"
      end

      Rails.logger.info "---下余---#{xy_sum}----"
      Rails.logger.info "---购进---#{gj_sum}----"
      Rails.logger.info "---时耗---#{sh_sum}----"
      Rails.logger.info "---结算---#{js_sum}----"
      return xy_sum, gj_sum, sh_sum, js_sum
    end
  end

  #糖化 周盘点计算 V2.1
  # PostReportsHelper.week_th_js('2018-07-25','2018-07-25',2)
  def self.week_th_js(begin_time, end_time, s_material_id)
    daily_reports = DTwoMenuValue.where(:s_material_id => s_material_id).by_datetime(begin_time, end_time)
    if daily_reports.present?
      arr1 =[] #总结余
      arr2 =[] #实耗淀粉
      arr3 =[] #产糖（T）
      arr4 =[] #淀粉酶质量
      arr5 =[] #糖化酶质量
      datetimes = daily_reports.pluck(:datetime).uniq.sort
      datetimes.each do |date|
        th_daily_data = PostReportsHelper.th_method_v2(date, s_material_id) #返回 下余（总计） 产糖  时耗  收率 淀粉酶质量  糖化酶质量
        if th_daily_data.present?
          arr1.push(th_daily_data[0])
          arr2.push(th_daily_data[2])
          arr3.push(th_daily_data[1])
          arr4.push(th_daily_data[4])
          arr5.push(th_daily_data[5])
        end
      end
      arr4 = arr4.compact #去除nil元素
      arr5 = arr5.compact
      Rails.logger.info "---总结余---#{arr1}----"
      Rails.logger.info "---实耗淀粉---#{arr2}----"
      Rails.logger.info "---产糖---#{arr3}----"
      Rails.logger.info "---淀粉酶质量---#{arr4}----"
      Rails.logger.info "---糖化酶质量---#{arr5}----"
      return arr1, arr2, arr3,arr4,arr5
    end
  end

  #发酵 周盘点计算 V2.1
  # PostReportsHelper.week_fj_js('2018-07-25','2018-07-25',2)
  def self.week_fj_js(begin_time, end_time, s_material_id)
    daily_reports = DTwoMenuValue.where(:s_material_id => s_material_id).by_datetime(begin_time, end_time)
    if daily_reports.present?
      arr1 =[] #总结余
      arr2 =[] #实耗
      arr3 =[] #产量
      arr4 =[] #消沫剂
      datetimes = daily_reports.pluck(:datetime).uniq.sort
      datetimes.each do |date|
        th_daily_data = PostReportsHelper.fj_method_v2(date, s_material_id) #返回 下余（总计） 产糖  时耗  收率  消沫剂
        if th_daily_data.present?
          arr1.push(th_daily_data[0])
          arr2.push(th_daily_data[2])
          arr3.push(th_daily_data[1])
          arr4.push(th_daily_data[4])
        end
      end
      return arr1, arr2, arr3,arr4
    end
  end

  #酸化 周盘点计算 V2.1
  # PostReportsHelper.week_sh_js('2018-07-25','2018-07-25',2)
  def self.week_sh_js(begin_time, end_time, s_material_id)
    daily_reports = DTwoMenuValue.where(:s_material_id => s_material_id).by_datetime(begin_time, end_time)
    if daily_reports.present?
      arr1 =[] #总结余
      arr2 =[] #实耗
      arr3 =[] #产量
      datetimes = daily_reports.pluck(:datetime).uniq.sort
      datetimes.each do |date|
        th_daily_data = PostReportsHelper.fj_method_v2(date, s_material_id) #返回 下余（总计） 产量  时耗  收率  对淀粉收率
        if th_daily_data.present?
          arr1.push(th_daily_data[0])
          arr2.push(th_daily_data[2])
          arr3.push(th_daily_data[1])
        end
      end
      return arr1, arr2, arr3
    end
  end

  #糖化 淀粉酶单耗 糖化酶单耗 计算
  #（新增） 淀粉酶单耗 = 淀粉酶质量/投粉时耗小计  糖化酶单耗 = 糖化酶质量/投粉时耗小计
  def self.period_js_v2(date_time, s_material_id)
    add_datas = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['amylase','Glucoamylase'])
    d1 =add_datas.select {|x| x.field_code == 'amylase'}
    d2 =add_datas.select {|x| x.field_code == 'Glucoamylase'}
    if d1[0].present? && d2[0].present?
      dh1 = ((d1[0].fole_value / bg) * 100).round(2)  #淀粉酶单耗
      dh2 = ((d2[0].fole_value / bg) * 100).round(2)  #糖化酶单耗
    end
  end



  #糖化 计算
  #时耗淀粉 = 昨天 总结余 + 头粉时耗小计 - 今天 总结余
  #总结余 = 调pH罐(M)系数*每天的填入的值 / 96*33.3）+ （蒸前罐系数*每天值/96*33.3）
  #         + (1#体积*每天值 … 10#体积*每天值)
  #         + 淀粉池每天量+ 上料罐每天量+ 层流罐每天量 + 蒸发器每天量+ 折粉每天量

  #折粉 = 折糖（T）/ 92%
  #折糖（T） = （糖储v01高度 + 糖储v02高度）* 浓度% * 糖储v01高度底面积

  #产糖（T）= 浓 糖（m3）* 浓缩糖（%）
  #浓 糖（m3）=今天 糖表读数 - 昨天 糖表读数
  #收率 = 产糖（T）/ 时耗淀粉
  # PostReportsHelper.th_method('2018-07-02',2)
  def self.th_method(date_time, s_material_id)
    before_time = (Time.parse(date_time) + (-1.day)).strftime("%Y-%m-%d")
    #按日期查询值
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time)
    daily_report_before = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => before_time)
    if daily_report_before.present? && daily_report_now.present?
      #昨天 总结余
      b_zjy = PostReportsHelper.js_one(before_time, s_material_id)
      #今天 总结余
      n_zjy = PostReportsHelper.js_one(date_time, s_material_id)
      #头粉 时耗小计
      tf_sum = PostReportsHelper.tf_total(date_time, 1)
      #计算头粉 时耗时需要 今天 昨天的数据
      if tf_sum.present?

        Rails.logger.info "-b_zjy---#{b_zjy}----"
        Rails.logger.info "-今天 总结余---#{n_zjy}----"
        Rails.logger.info "-tf_sum---#{tf_sum}----"
        #时耗淀粉
        shdf = (b_zjy + tf_sum - n_zjy).round(2)
        #产糖（T）= 浓 糖（m3）* 浓缩糖（%） + 底 糖（%）* 底 糖（m3）
        #浓 糖（m3）=今天 糖表读数 - 昨天 糖表读数
        n_sugs = daily_report_now.select {|x| x.field_code == 'sugar_nums'}
        b_sugs = daily_report_before.select {|x| x.field_code == 'sugar_nums'}
        # Rails.logger.info "---糖表读数1---#{n_sugs}----"
        # Rails.logger.info "---糖表读数2---#{b_sugs}----"
        sugs = n_sugs[0].field_value - b_sugs[0].field_value #浓 糖（m3

        con_sugs = daily_report_now.select {|x| x.field_code == 'Concentrated_sugar'} # 浓缩糖（%）
        sugs1 = daily_report_now.select {|x| x.field_code == 'Bottom_sugar（%）'} # 底 糖（%）
        sugs2 = daily_report_now.select {|x| x.field_code == 'Bottom_sugar（m3）'} # 底 糖（m3）
        # 产糖（T）
        ct1 = sugs * con_sugs[0].field_value / 100
        ct2 = sugs1[0].field_value * sugs2[0].field_value / 100
        ct = (ct1 + ct2).round(2)

        #收率 = 产糖（T）/ 时耗淀粉
        sl = ((ct / shdf) * 100).round(2)
        # Rails.logger.info "---糖化产糖---#{ct}----"
        # Rails.logger.info "---糖化收率---#{sl}----"
        return n_zjy, shdf, ct, sl # n_zjy 今天 总结余  shdf 时耗淀粉  ct 产糖  sl 收率
      end
    end
  end

  #糖化 V2.1 计算 产糖 时耗 收率
  #产糖 = 交料产量小计
  # 时耗 = 上余+本购-下余  （本购 = 头粉时耗小计）
  #收率 = 产糖/时耗
  # PostReportsHelper.th_method_v2('2018-08-01',2)
  def self.th_method_v2(date_time, s_material_id)
    bef_time = (Time.parse(date_time) + (-1.day)).strftime("%Y-%m-%d")
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['Concentrated_sugar', 'Bottom_sugar（m3）'])
    if daily_report_now.present? && daily_report_now.length > 0
      #糖化 交料产量计算
      products_datas = daily_report_now.pluck(:fold_value)
      products_datas = products_datas.compact #去除nil 元素
      jlcl_sum = (products_datas.sum).round(2) #交料产量小计
      sy = PostReportsHelper.xy_method(bef_time, s_material_id) #上余=昨天的下余
      xy = PostReportsHelper.xy_method(date_time, s_material_id) #下余
      tf_datas = PostReportsHelper.tf_method(date_time, 1) #头粉数据
      if tf_datas.present? && sy.present? && xy.present?
        bg = tf_datas[3] #本购=头粉时耗小计
        Rails.logger.info "----sy-#{sy}--bg#{bg}----xy#{xy}---"
        sh = (sy + bg - xy).round(2) #时耗
        sl = ((jlcl_sum / sh) * 100).round(2) #收率

        Rails.logger.info "--TY-th_method_v2-sh-#{sh}--sl#{sl}----"


        # #（新增）  淀粉酶质量  糖化酶质量
        # add_datas = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['amylase','Glucoamylase'])
        # d1 =add_datas.select {|x| x.field_code == 'amylase'}
        # d2 =add_datas.select {|x| x.field_code == 'Glucoamylase'}
        # if d1[0].present? && d2[0].present?
        #   dh1 = d1[0].fold_value  #淀粉酶质量
        #   dh2 = d2[0].fold_value  #糖化酶质量
        # else
        #   dh1 = 0
        #   dh2 = 0
        # end

        Rails.logger.info "----th_method_v2--xy#{xy}-jlcl_sum#{jlcl_sum}-sh#{sh}-sl#{sl}----"
        return xy, jlcl_sum, sh, sl#返回 下余（总计） 产糖  时耗  收率
        #return xy, jlcl_sum, sh, sl,dh1,dh2 #返回 下余（总计） 产糖  时耗  收率 ||(新增) 淀粉酶质量  糖化酶质量
      end
    end
  end

  #发酵计算 V2.1
  # PostReportsHelper.fj_method_v2('2018-07-25',3)
  def self.fj_method_v2(date_time, s_material_id)
    bef_time = (Time.parse(date_time) + (-1.day)).strftime("%Y-%m-%d")
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['batch'])
    if daily_report_now.present? && daily_report_now.length > 0
      #交料产量计算
      products_datas = daily_report_now.pluck(:fold_value)
      products_datas = products_datas.compact #去除nil 元素
      jlcl_sum = (products_datas.sum).round(2) #交料产量小计

      sy = PostReportsHelper.xy_method(bef_time, s_material_id) #上余=昨天的下余
      xy = PostReportsHelper.xy_method(date_time, s_material_id) #下余
      th_datas = PostReportsHelper.th_method_v2(date_time, 2) #返回糖化  下余（总计） 产糖  时耗  收率
      Rails.logger.info "----发酵--th_datas-#{th_datas}--------sy-#{sy}------xy#{xy}-"
      if th_datas.present? && sy.present? && xy.present?
        bg = th_datas[1] #本购 = 糖化交料产量小计
        sh = (sy + bg - xy).round(2) #时耗
        sl = ((jlcl_sum / sh) * 100).round(2) #收率
        dfsl = ((sl * th_datas[3] * 100) / 10000).round(2) #对淀粉收率 = 收率 * 糖化收率

        # #新增 消沫剂 数据
        # add_datas = DTwoMenuValue.find_by(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['Foaming_agent'])
        # if add_datas.present?
        #   xmj = add_datas.fold_value
        # end

        return xy, jlcl_sum, sh, sl, dfsl   #返回 下余（总计） 产量  时耗  收率  对淀粉收率
        #return xy, jlcl_sum, sh, sl, dfsl,xmj   #返回 下余（总计） 产量  时耗  收率  对淀粉收率  消沫剂
      end
    end
  end

  #酸化计算 V2.1 计算 实耗  收率  总收率
  # PostReportsHelper.sh_method_v2('2018-07-25',4)
  def self.sh_method_v2(date_time, s_material_id)
    bef_time = (Time.parse(date_time) + (-1.day)).strftime("%Y-%m-%d")
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['batch'])
    if daily_report_now.present? && daily_report_now.length > 0
      #酸化 交料产量计算
      products_datas = daily_report_now.pluck(:fold_value)
      products_datas = products_datas.compact #去除nil 元素
      jlcl_sum = (products_datas.sum).round(2) #交料产量小计
      sy = PostReportsHelper.xy_method(bef_time, s_material_id) #上余=昨天的下余
      xy = PostReportsHelper.xy_method(date_time, s_material_id) #下余
      th_datas = PostReportsHelper.fj_method_v2(date_time, 3) #返回发酵  下余（总计） 产量  时耗  收率  对淀粉收率
      #Rails.logger.info "----酸化--th_datas-#{th_datas}--------sy-#{sy}------xy#{xy}-"
      if th_datas.present? && sy.present? && xy.present?
        bg = th_datas[1] #本购 = 发酵交料小计
        sh = (sy + bg - xy).round(2) #实耗
        sl = ((jlcl_sum / sh) * 100).round(2) #收率
        zsl = ((sl * th_datas[4]) / 100).round(2) #总收率 = 收率 * (发酵中 对淀粉收率)
        return xy, sh, sl, zsl #返回 下余 实耗  收率  总收率
      end
    end
  end

  # 酸化 交料产量小计/收率 （APP上报使用） 计算
  def self.shjl_method_v2(date_time, s_material_id)
    bef_time = (Time.parse(date_time) + (-1.day)).strftime("%Y-%m-%d")
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['batch'])
    if daily_report_now.present? && daily_report_now.length > 0
      #酸化 交料产量计算
      products_datas = daily_report_now.pluck(:fold_value)
      products_datas = products_datas.compact #去除nil 元素
      jlcl_sum = (products_datas.sum).round(2) #交料产量小计
      sy = PostReportsHelper.xy_method(bef_time, s_material_id) #上余=昨天的下余
      xy = PostReportsHelper.xy_method(date_time, s_material_id) #下余
      th_datas = PostReportsHelper.fj_method_v2(date_time, 3) #返回发酵  下余（总计） 产量  时耗  收率  对淀粉收率
      #Rails.logger.info "----酸化--th_datas-#{th_datas}--------sy-#{sy}------xy#{xy}-"
      if th_datas.present? && sy.present? && xy.present?
        bg = th_datas[1] #本购 = 发酵交料小计
        sh = (sy + bg - xy).round(2) #实耗
        sl = ((jlcl_sum / sh) * 100).round(2) #收率
        zsl = ((sl * th_datas[4]) / 100).round(2) #总收率 = 收率 * (发酵中 对淀粉收率)
        return jlcl_sum, xy, sh, sl, zsl #返回 交料产量  下余  实耗  收率  总收率
      end
    end
  end

  #糖化/发酵 / 酸化 下余计算 V2.1
  # PostReportsHelper.xy_method('2018-07-25',2)
  def self.xy_method(date_time, s_material_id)
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time)
    d_one_menu = DOneMenu.where(:s_material_id => s_material_id)
    s_material = SMaterial.find_by(:id => s_material_id)
    if 'SHY' == s_material.code #酸化液 计算 下余
      one_menu_left1 = d_one_menu.select {|x| x.code == 'left_fjy'}
      one_menu_left2 = d_one_menu.select {|x| x.code == 'left_ly'}
      codes1 = one_menu_left1[0].d_two_menus.pluck(:code)
      codes2 = one_menu_left2[0].d_two_menus.pluck(:code)
      Rails.logger.info "--------codes1---------#{codes1}----"
      Rails.logger.info "--------codes2---------#{codes2}----"
    end
    if ['TY', 'FJY'].include? s_material.code #糖化 发酵
      one_menu_left1 = d_one_menu.select {|x| x.code == 'left1'}
      one_menu_left2 = d_one_menu.select {|x| x.code == 'left2'}
      codes1 = one_menu_left1[0].d_two_menus.pluck(:code)
      codes2 = one_menu_left2[0].d_two_menus.pluck(:code)
    end

    if daily_report_now.present?
      xywl1 = daily_report_now.where(:field_code => codes1) #下余物料1 保存的值
      xywl2 = daily_report_now.where(:field_code => codes2) #下余物料2
      if ['TY', 'FJY'].include? s_material.code #糖液 发酵液 下余 取 nums_value
        xy_nums1 = xywl1.pluck(:nums_value)
        xy_nums2 = xywl2.pluck(:nums_value)
        xy_nums1 = xy_nums1.compact #去空
        xy_nums2 = xy_nums2.compact #去空
        Rails.logger.info "--------xy_nums1---------#{xy_nums1}----"
        Rails.logger.info "--------xy_nums2---------#{xy_nums2}----"
        return (xy_nums1.sum + xy_nums2.sum).round(2)
      end
      if ['SHY'].include? s_material.code #酸化液 下余 取 fold_value
        xy_nums1 = xywl1.pluck(:fold_value)
        xy_nums2 = xywl2.pluck(:fold_value)
        xy_nums1 = xy_nums1.compact #去空
        xy_nums2 = xy_nums2.compact #去空
        Rails.logger.info "--------xy_nums1---------#{xy_nums1}----"
        Rails.logger.info "--------xy_nums2---------#{xy_nums2}----"
        return (xy_nums1.sum + xy_nums2.sum).round(2)
      end
    end
  end

  # PostReportsHelper.js_one('2018-07-02',2)
  # 总结余 计算
  def self.js_one(date_time, s_material_id)
    #按日期查询值
    daily_report_now = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time)
    if daily_report_now.present?
      #b_evaporators = @daily_report_now.select {|x| ['Sugar_storageV01', 'Sugar_storageV02'].include?(x.field_code)} # 糖储v01 糖储v02

      b_evaporators_v1 = daily_report_now.select {|x| ['Sugar_storageV01'].include?(x.field_code)} # 糖储v01
      concentration = daily_report_now.select {|x| ['concentration'].include?(x.field_code)} # 浓度%
      b_evaporators = daily_report_now.where(:field_code => ['Sugar_storageV01', 'Sugar_storageV02'])
      #Rails.logger.info "-b_evaporators---#{b_evaporators}----"
      b_evaporators_v = b_evaporators.pluck(:field_value)

      # 折糖（T） = （糖储v01高度 + 糖储v02高度）* 浓度% * 糖储v01高度底面积
      zd = b_evaporators_v.sum * concentration[0].field_value * b_evaporators_v1[0].ratio / 100
      #折粉 = 折糖（T）/ 92%
      d = zd / 0.92

      Rails.logger.info "-d---#{d}----"

      ph_tank = daily_report_now.select {|x| x.field_code == 'PH_tank'} #调pH罐
      evaporation_tank = daily_report_now.select {|x| x.field_code == 'Evaporation_tank'} #蒸前罐

      a1 = (ph_tank[0].ratio * ph_tank[0].field_value) / 96 * 33.3
      a2 = (evaporation_tank[0].ratio * evaporation_tank[0].field_value) / 96 * 33.3
      a = a1 + a2

      # Rails.logger.info "-a1--#{a1}-----"
      # Rails.logger.info "-a2--#{a2}-----"

      arrs1=['1#', '2#', '3#', '4#', '5#', '6#', '7#', '8#', '9#', '10#']
      guans = daily_report_now.where(:field_code => arrs1)
      guans_r = guans.pluck(:ratio)
      guans_v = guans.pluck(:field_value)
      # Rails.logger.info "-guans_r--#{guans_r}-----"
      # Rails.logger.info "-guans_v--#{guans_v}-----"
      b = guans_v.sum * (guans_r.sum / guans_r.size)

      #Rails.logger.info "-b--#{b}-----"
      arrs2 = ['Starch_pond', 'Feeding_tank', 'laminar_flow_pot', 'evaporator'] #淀粉池 上料罐 层流罐 蒸发器
      starch_ponds = daily_report_now.where(:field_code => arrs2)
      starch_ponds_v = starch_ponds.pluck(:field_value)
      # Rails.logger.info "-starch_ponds_v--#{starch_ponds_v}-----"
      c = starch_ponds_v.sum

      #Rails.logger.info "-c--#{c}-----"

      zjy_sum = (a + b + c + d).round(2)
      Rails.logger.info "-总结余--#{zjy_sum}-----"
      return zjy_sum


    end
  end


end