module SRoleMsgsHelper
  def check_func_in_all(func_code)
      @son_funcs.each do |son_func|
        if func_code == son_func.func_code
          return true
        end
      end
    return false
  end
  
  def check_bar_in_all(func_code)
    @navi_bar.each do |navi_bar|
      if func_code == navi_bar.func_code
        return true
      end
    end
    return false
  end
  
end
