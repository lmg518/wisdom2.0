module AqiHandleHelper

  #laqi_handle(@laqi_so2,@laqi_co,@laqi_no2,@laqi_o3,@laqi_pm2_5,@laqo_pm10)
  def laqi_handle(*args)
    @AQI_arr = []
    so2, co, no2, o3, pm2_5, pm10 = args[0], args[1], args[2], args[3], args[4], args[5]
    handle_so2(so2) if so2.present?
    handle_co(co) if so2.present?
    handle_no2(no2) if so2.present?
    handle_o3(o3) if so2.present?
    handle_pm2_5(pm2_5) if so2.present?
    handle_pm10(pm10) if so2.present?
    aqi_days(@AQI_arr.max)
  end

  def month_days(month)
    month = month.to_i
    return 31 if [1,3,5,7,8,10,12].include? month
    return 29 if month == 2 && Date.julian_leap?(Time.now.year)
    return 28 if month ==2 && !Date.julian_leap?(Time.now.year)
    return 30
  end

  def days_between(date1, date2)
    d1 = Date.parse(date1)
    d2 = Date.parse(date2)
    (d1 - d2).to_i
  end

  def aqi_days(aqi_val)
    aqi = aqi_val.to_i
    return "否" if aqi == 0
    case aqi
      when 0..50
        "优"
      when 51..100
        "良"
      when 101..150
        "轻度污染"
      when 151..200
        "中度污染"
      when 201..300
        "重度污染"
      when 300..9999
        "严重污染"
      else
        "否"
    end
  end
  private
  def handle_so2(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 50, 0, data_val)
      when 51..100
        iaqi(100, 50, 150, 50, data_val)
      when 101..150
        iaqi(150, 100, 475, 150, data_val)
      when 151..200
        iaqi(200, 150, 800, 475, data_val)
      when 201..300
        iaqi(300, 200, 1600, 800, data_val)
      when 301..400
        iaqi(400, 300, 2100, 1600, data_val)
      when 401..500
        iaqi(500, 400, 2620, 2100, data_val)
    end
  end

  def handle_co(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 2, 0, data_val)
      when 51..100
        iaqi(100, 50, 4, 2, data_val)
      when 101..150
        iaqi(150, 100, 14, 4, data_val)
      when 151..200
        iaqi(200, 150, 24, 14, data_val)
      when 201..300
        iaqi(300, 200, 36, 24, data_val)
      when 301..400
        iaqi(400, 300, 500, 420, data_val)
      when 401..500
        iaqi(500, 400, 600, 500, data_val)
    end
  end

  def handle_no2(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 40, 0, data_val)
      when 51..100
        iaqi(100, 50, 80, 40, data_val)
      when 101..150
        iaqi(150, 100, 180, 80, data_val)
      when 151..200
        iaqi(200, 150, 280, 180, data_val)
      when 201..300
        iaqi(300, 200, 565, 280, data_val)
      when 301..400
        iaqi(400, 300, 750, 565, data_val)
      when 401..500
        iaqi(500, 400, 940, 750, data_val)
    end
  end

  def handle_o3(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 160, 0, data_val)
      when 51..100
        iaqi(100, 50, 200, 160, data_val)
      when 101..150
        iaqi(150, 100, 300, 200, data_val)
      when 151..200
        iaqi(200, 150, 400, 300, data_val)
      when 201..300
        iaqi(300, 200, 800, 400, data_val)
      when 301..400
        iaqi(400, 300, 1000, 800, data_val)
      when 401..500
        iaqi(500, 400, 1200, 1000, data_val)
    end
  end

  def handle_pm2_5(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 35, 0, data_val)
      when 51..100
        iaqi(100, 50, 75, 35, data_val)
      when 101..150
        iaqi(150, 100, 115, 75, data_val)
      when 151..200
        iaqi(200, 150, 150, 115, data_val)
      when 201..300
        iaqi(300, 200, 250, 150, data_val)
      when 301..400
        iaqi(400, 300, 350, 250, data_val)
      when 401..500
        iaqi(500, 400, 500, 350, data_val)
    end
  end

  def handle_pm10(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 50, 0, data_val)
      when 51..100
        iaqi(100, 50, 150, 50, data_val)
      when 101..150
        iaqi(150, 100, 250, 150, data_val)
      when 151..200
        iaqi(200, 150, 350, 250, data_val)
      when 201..300
        iaqi(300, 200, 420, 350, data_val)
      when 301..400
        iaqi(400, 300, 500, 240, data_val)
      when 401..500
        iaqi(500, 400, 600, 500, data_val)
    end
  end

  def iaqi(iaql_H, iaql_L, bp_H, bp_L, c_v)
    @second,@first,@fs = 0,0,0
    @fs = (iaql_H.to_i - iaql_L.to_i) / (bp_H.to_i - bp_L.to_i) if (bp_H.to_i - bp_L.to_i) > 0
    @first = @fs * (c_v.to_f - bp_L.to_i) if @fs.present?
    @second = @first + iaql_L.to_i
    @AQI_arr << @second
    @second
  end


end