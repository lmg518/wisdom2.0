module SAbnormalRuleInstancesHelper
  # H：有效数据不足 BB：连接不良 B：运行不良 W：等待数据恢复 HSp：数据超上限 LSp：数据超下限
  # PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
  # NU: 无数据（-99） TH1:突然高/低 TH2：突然高/低 （变化率）DBG1：对标高/低 DBG2：对标高/低 （变化率）
  # KDG：颗粒物倒挂
  ####
  #因子值：yz_val  第一个值： first_val  第二个值:second_val
  #----------

  def cycle_time(cycle)
    case cycle.to_i
      when 300
        @cycle_time = "5.minutes"
      when 3600
        @cycle_time = "1.hours"
    end
  end

  def minute_or_hour(time, cycle)
    @time = time
    case cycle.to_i
      when 300
        @cycle_time = " @time.beginning_of_minute - 5.minutes"
      when 3600
        @cycle_time = " @time.beginning_of_hour - 1.hours"
    end
  end

  #突然高   取当前数据X跟上一正常数据做差高于  X
  # 污染物突然高：
  # 当前小时数据的上一小时数据小于等于50，当前数据大于等于（上一数据 X 2）
  # 当前小时数据的上一小时数据大于50小于200，当前数据大于等于（上一数据 X 1）
  # 当前小时数据的上一小时数据大于等于200，当前数据大于等于（上一数据 X 0.5）
  # 对标高异常：  高于同城站点平均值50%
  #     产生异常，报警，通知到运维人员
  # 对标低异常：  低于同城站点平均值50%
  def handle_trg(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    case data_cycle.to_i
      when 3600
        pre_hour_date = DDataYyyymm.where(:data_time => (new_time - 1.hours ), :station_id => station_id, :item_code => item_code.id, :data_cycle => data_cycle)
        if pre_hour_date.present?
          @item_trg = false
          hour_val = pre_hour_date.data_value.to_f
          if (hour_val <= 50)
            @item_trg = true if item_val.to_f >=  (hour_val * 2)
          end
          if (50 < hour_val && hour_val < 200)
            @item_trg = true if item_val.to_f >=  (hour_val * 1)
          end
          if ( hour_val > 200)
            @item_trg = true if item_val.to_f >=  (hour_val * 0.5)
          end
          if @item_trg
            abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}突然高", abnormal_instabce.parameter_value, 0, 0)
          end
          # pre_item_val = pre_hour_date.data_value
          # abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
          # unless (item_val.to_f - pre_item_val.to_f) > abnormal_instabce.parameter_value
          #   abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}突然高", abnormal_instabce.parameter_value, 0, 0)
          # end
        end
    end
  end

  #突然低   取当前数据X跟上一正常数据做差低于  X
  def handle_trd(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    pre_hour_date = DDataYyyymm.where(:data_time => eval(minute_or_hour(new_time, data_cycle)), :station_id => station_id, :item_code => item_code.id, :data_cycle => data_cycle).first
    if pre_hour_date.present?
      pre_item_val = pre_hour_date.data_value
      abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
      unless (item_val.to_f - pre_item_val.to_f) < abnormal_instabce.parameter_value
        abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}突然低", abnormal_instabce.parameter_value, 0, 0)
      end
    end
  end

  #对标高 与同城站点数据差值，高于 X   逻辑： 当前站点数值跟同城站点值做差值比较（同城取值，排除当前站点值，剩下的最大值／最小值，然后求平均值）
  #高于同城站点平均值50%
  def handle_dbgg(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
    area = DStation.find(station_id).s_region_codes
    return  if area.blank?
    all_stations = area[0].d_stations.pluck(:id)
    station_id_arr = all_stations
    # station_id_arr = all_stations.map {|x| x.id}.delete_if {|x| x.to_i == station_id.to_i}
    province_datas = DDataYyyymm.where(:station_id => station_id_arr, :data_time => new_time, :item_code => item_code.id, :data_cycle => data_cycle)
    return if province_datas.blank?
    province_val_arr = province_datas.map {|x| x.data_value.to_f}
    arr_avg = province_val_arr.sum / province_val_arr.length

    if item_val.to_f > ( arr_avg + arr_avg * 0.5)
      abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}对标高", abnormal_instabce.parameter_value, 0, 0)
    end

    # @p_data_length = 0
    # @p_data_length = province_datas.length
    # if @p_data_length == 0
    #   cheack_val = item_val.to_f - province_datas.first.data_value.to_f
    # end
    #
    # if @p_data_length == 2
    #   province_data_avg = province_val_arr.sum / (province_datas.length == 0 ? 1 : province_datas.length)
    #   cheack_val = item_val.to_f - province_data_avg.to_f
    # end
    #
    # if @p_data_length > 2
    #   new_province_vals = province_val_arr.delete_if {|x| x == province_val_arr.max || x == province_val_arr.min}
    #   if new_province_vals.length == 0
    #     @new_province_val_sum = province_datas.first.data_value.to_f
    #   else
    #     @new_province_val_sum = new_province_vals.sum
    #   end
    #   if @p_data_length == 3
    #     new_province_avg = @new_province_val_sum
    #   else
    #     new_province_avg = new_province_vals.sum / (new_province_vals.length == 0 ? 1 : new_province_vals.length)
    #   end
    #   cheack_val = item_val.to_f - new_province_avg.to_f
    # end
    # unless cheack_val.to_f > abnormal_instabce.parameter_value
    #   abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}对标高", abnormal_instabce.parameter_value, 0, 0)
    # end
  end

  #对标低 与同城站点数据差值，低于 X   逻辑： 当前站点数值跟同城站点值做差值比较（同城取值，排除当前站点值，剩下的最大值／最小值，然后求平均值）
  def handle_dbgd(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
    area = DStation.find(station_id).s_administrative_area
    all_stations = area.d_stations
    station_id_arr = all_stations.map {|x| x.id}.delete_if {|x| x.to_i == station_id.to_i}
    province_datas = DDataYyyymm.where(:station_id => station_id_arr, :data_time => new_time, :item_code => item_code.id, :data_cycle => data_cycle)
    return if province_datas.blank?
    province_val_arr = province_datas.map {|x| x.data_value.to_f}
    arr_avg = province_val_arr.sum / province_val_arr.length

    if item_val.to_f > ( arr_avg + arr_avg * 0.5)
      abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}对标低", abnormal_instabce.parameter_value, 0, 0)
    end

    # p_data_length = province_datas.length
    #
    # if p_data_length == 0
    #   cheack_val = item_val.to_f - province_datas.first.data_value.to_f
    # end
    #
    # if p_data_length == 2
    #   province_data_avg = province_val_arr.sum / (province_datas.length == 0 ? 1 : province_datas.length)
    #   cheack_val = item_val.to_f - province_data_avg.to_f
    # end
    #
    # if p_data_length > 2
    #   new_province_vals = province_val_arr.delete_if {|x| x == province_val_arr.max || x == province_val_arr.min}
    #   if new_province_vals.length == 0
    #     @new_province_val_sum = province_datas.first.data_value.to_f
    #   else
    #     @new_province_val_sum = new_province_vals.sum
    #   end
    #   if p_data_length == 3
    #     new_province_avg = @new_province_val_sum
    #   else
    #     new_province_avg = new_province_vals.sum / (new_province_vals.length == 0 ? 1 : new_province_vals.length)
    #   end
    #   cheack_val = item_val.to_f - new_province_avg.to_f
    # end
    # unless cheack_val.to_f < abnormal_instabce.parameter_value
    #   abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}对标低", abnormal_instabce.parameter_value, 0, 0)
    # end
  end

  #定值
  def handle_ding(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    cycle_time(data_cycle)
    pre_val_arr = []
    case data_cycle.to_i
      when 3600
        @ding_time = new_time.beginning_of_hour
        begin
          2.times do |i|
            pre_val_arr << pre_hour_date = DDataYyyymm.find_by(:data_time => ((@ding_time - eval(@cycle_time) * i)), :station_id => station_id, :item_code => item_code.id, :data_cycle => data_cycle)
          end
          if pre_val_arr.map {|x| x.data_value}.uniq.size == 1
            abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}定值", item_val, 0, 0)
          end
          five_datas = DDataYyyymm.find_by(:data_time => (@ding_time - 1.hours + 1.minutes )..(@ding_time), :station_id => station_id, :item_code => item_code.id, :data_cycle => 300)
          if five_datas.map {|x| x.data_value}.uniq.size == 1
            abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}5分钟定值", item_val, 0, 0)
          end
        rescue Exception => e
          puts e.inspect
        end
    end
  end

  #
  def handle_lost(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)

  end

  #突然高/低 因子 取当前数据X跟上一正常数据Y做（X - Y）的值不在（-z，z）之间
  def handle_th1(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    pre_hour_date = DDataYyyymm.where(:data_time => eval(minute_or_hour(new_time, data_cycle)), :station_id => station_id, :item_code => item_code.id, :data_cycle => data_cycle).first
    if pre_hour_date.present?
      pre_item_val = pre_hour_date.data_value
      abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
      unless ((item_val.to_f - pre_item_val.to_f) > abnormal_instabce.parameter_value) && ((item_val.to_f - pre_item_val.to_f) < abnormal_instabce.parameter_value1)
        abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}突然高/低", abnormal_instabce.parameter_value, abnormal_instabce.parameter_value1, 0)
      end
    end
  end

  #突然高/低 （变化率）取当前数据X跟上一正常数据Y做（X - Y）/ X的值不在（-x=z%，x=z%）之间
  def handle_th2(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    pre_hour_date = DDataYyyymm.where(:data_time => eval(minute_or_hour(new_time, data_cycle)), :station_id => station_id, :item_code => item_code.id, :data_cycle => data_cycle).first
    if pre_hour_date.present?
      pre_item_val = pre_hour_date.data_value
      abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
      unless (((item_val.to_f - pre_item_val.to_f) / item_val.to_f) > abnormal_instabce.parameter_value) && (((item_val.to_f - pre_item_val.to_f) - item_val.to_f) < abnormal_instabce.parameter_value1)
        abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}突然高/低(变化率)", abnormal_instabce.parameter_value, abnormal_instabce.parameter_value1, 0)
      end
    end
  end

  #对标高/低 与同城站点数据差值，不在（-z，z）之间   逻辑： 当前站点数值跟同城站点值做差值比较（同城取值，排除当前站点值，剩下的最大值／最小值，然后求平均值）
  def handle_dbg1(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
    area = DStation.find(station_id).s_administrative_area
    all_stations = area.d_stations
    station_id_arr = all_stations.map {|x| x.id}.delete_if {|x| x.to_i == station_id.to_i}
    province_datas = DDataYyyymm.where(:station_id => station_id_arr, :data_time => new_time, :item_code => item_code.id, :data_cycle => data_cycle)
    return if province_datas.blank?
    province_val_arr = province_datas.map {|x| x.data_value.to_f}
    p_data_length = province_datas.length

    if p_data_length == 0
      cheack_val = item_val.to_f - province_datas.first.data_value.to_f
    end

    if p_data_length == 2
      province_data_avg = province_val_arr.sum / province_datas.length
      cheack_val = item_val.to_f - province_data_avg.to_f
    end

    if p_data_length > 2
      new_province_vals = province_val_arr.delete_if {|x| x == province_val_arr.max || x == province_val_arr.min}
      new_province_avg = new_province_vals.sum / new_province_vals.length
      cheack_val = item_val.to_f - new_province_avg.to_f
    end
    unless cheack_val > abnormal_instabce.parameter_value && cheack_val < abnormal_instabce.parameter_value
      abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}对标高/低", abnormal_instabce.parameter_value, abnormal_instabce.parameter_value1, 0)
    end
  end

  #对标高/低（变化率） 与同城站点数据比率，不在（-z%，z%）之间
  def handle_dbg2(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_instabce = SAbnormalRuleInstance.find_by_s_abnormmal_rule_id(abnormal_rule)
    area = DStation.find(station_id).s_administrative_area
    all_stations = area.d_stations
    station_id_arr = all_stations.map {|x| x.id}.delete_if {|x| x.to_i == station_id.to_i}
    province_datas = DDataYyyymm.where(:station_id => station_id_arr, :data_time => new_time, :item_code => item_code.id, :data_cycle => data_cycle)
    return if province_datas.blank?
    province_val_arr = province_datas.map {|x| x.data_value.to_f}
    p_data_length = province_datas.length

    if p_data_length == 0
      cheack_val = item_val.to_f / province_datas.first.data_value.to_f
    end

    if p_data_length == 2
      province_data_avg = province_val_arr.sum / province_datas.length
      cheack_val = item_val.to_f / province_data_avg.to_f
    end

    if p_data_length > 2
      new_province_vals = province_val_arr.delete_if {|x| x == province_val_arr.max || x == province_val_arr.min}
      new_province_avg = new_province_vals.sum / new_province_vals.length
      cheack_val = item_val.to_f / new_province_avg.to_f
    end
    unless cheack_val > abnormal_instabce.parameter_value && cheack_val < abnormal_instabce.parameter_value
      abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}对标高/低", abnormal_instabce.parameter_value, abnormal_instabce.parameter_value1, 0)
    end
  end

  # 出零出负  T =< 0 出零出付：  小时产生异常，报警，（分钟数据连续两个）
  def handle_zero(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    case data_cycle.to_i
      when 300
        seeion_num = 0
        DDataYyyymm.where(:data_time => (new_time - 5.minutes)..(new_time),
                            :station_id => station_id,
                            :item_code => item_code.id,
                            :data_cycle => data_cycle).each do |yy_mm|
          seeion_num += 1 if yy_mm.data_value.to_f <= 0
        end
        if seeion_num == 2
          abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}5分钟出零出负", item_val.to_f, 0, 0)
        end
      when 3600
        if item_val.to_f <= 0
          abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}小时出零出负", item_val.to_f, 0, 0)
        end
    end
  end

  #颗粒物倒挂
  def handle_kdg(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    if item_code.id == 107
      @pm2_5 = data_arr.select{|x| x["item_code"].to_i == 107 && x["station_id"].to_i == station_id.to_i}
      @pm10 = data_arr.select{|x| x["item_code"].to_i == 108 && x["station_id"].to_i == station_id.to_i}

      if @pm2_5.present? && @pm10.present?
         pm2_5 = @pm2_5[0]["data_value"].to_f
         pm10 = @pm10[0]["data_value"].to_f
         puts "#{new_time}>station_id>>#{station_id}--->#{@pm2_5}--->#{@pm10},#{pm2_5 > pm10} >>#{pm10 == 0}"
        if pm2_5 > pm10
         return if pm10 == 0
          abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}颗粒物倒挂", pm2_5, pm10, 0)
        end
         @pm2_5,@pm10 = [],[]
      end
    end
  end

  #6因子  连续两天平均值相差 1倍  报异常
  def handle_2_days_abnormal(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
     case data_cycle.to_i
       when 3600
        day_before_2 = DDataYyyymm.find_by(:data_time => new_time - 2.days,
                           :station_id => station_id,
                           :item_code => item_code.id,
                           :data_cycle => data_cycle)
        day_before_1 = DDataYyyymm.find_by(:data_time => new_time - 1.days,
                                         :station_id => station_id,
                                         :item_code => item_code.id,
                                         :data_cycle => data_cycle)
         if day_before_2.present? && day_before_1.present?
           val_2 = day_before_2.data_value.to_f
           val_1 = day_before_1.data_value.to_f
           if ((val_2 - val_1) > val_2) || ((val_2 - val_1) <  -val_2)
             abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}连续两天平均值超1倍", val_1, val_2, 0)
           end
         end
     end
  end

  #--------------------有标示判读方法
  # 离线  离线时长（1.hours），离线状态
  def handle_ofl_hours(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}1小时离线", 0, 0, 0)
  end

  # 离线  离线时长（5.minutes），离线状态
  def handle_ofl_minutes(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}5分钟离线", 0, 0, 0)
  end

  # 无数据 无数据（-99） 值判断
  def handle_null(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}无数据", 0, 0, 0)
  end

  # 有效数据不足
  def handle_h(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}有效数据不足", 0, 0, 0)
  end

  #链接不良
  def handle_bb(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    case data_cycle.to_i
      when 3600
        @ding_time = new_time.beginning_of_hour
        data_yyy_mms = DDataYyyymm.find_by(:data_time => (@ding_time - 4.hours)..(@ding_time), :station_id => station_id, :item_code => item_code.id, :data_cycle => data_cycle)
        if data_yyy_mms.length
          abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}连续4小时连接不良", 0, 0, 0)
        end
      else
        abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}5分钟连接不良", 0, 0, 0)
    end
  end

  #运行不良
  def handle_b(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}运行不良", 0, 0, 0)
  end

  #等待数据恢复
  def handle_w(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}等待数据恢复", 0, 0, 0)
  end

  #数据超上限
  def handle_hsp(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}数据超上限", 0, 0, 0)
  end

  #数据超下限
  def handle_lsp(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}数据超下限", 0, 0, 0)
  end

  #零点检查
  def handle_pz(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}零点检查", 0, 0, 0)
  end

  #跨度检查
  def handle_ps(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}跨度检查", 0, 0, 0)
  end

  #精度检查
  def handle_as(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}精度检查", 0, 0, 0)
  end

  #零点校准
  def handle_cz(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}零点校准", 0, 0, 0)
  end

  #跨度校准
  def handle_cs(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}跨度校准", 0, 0, 0)
  end

  #仪器回补数据
  def handle_re(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}仪器回补数据", 0, 0, 0)
  end

  #自动或人工审核为无效
  def handle_rm(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
    abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, "#{item_code.item_name}自动或人工审核为无效", 0, 0, 0)
  end

  private
  def abnormal_data_create(station_id, item_code, rule, new_time, create_time, item_val, abnormal_rule, des, val1, val2, val3)
    DAbnormalDataYyyymm.create!(abnormal_id: '',
                                station_id: station_id,
                                item_code_id: item_code.id,
                                rule_instance: rule.instance_name,
                                create_acce: new_time,
                                ab_lable: abnormal_rule.ab_lable, ab_data_time: create_time,
                                ab_value: item_val, compare_value: val1.present? ? val1 : 0,
                                compare_value2: val2.present? ? val2 : 0, compare_value3: val3.present? ? val3 : 0,
                                ab_desc: des, s_abnormal_rule_instance_id: rule.id)
  end

end