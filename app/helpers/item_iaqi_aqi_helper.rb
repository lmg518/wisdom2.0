module ItemIaqiAqiHelper

  def handle_so2(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 50, 0, data_val)
      when 51..100
        iaqi(100, 50, 150, 50, data_val)
      when 101..150
        iaqi(150, 100, 475, 150, data_val)
      when 151..200
        iaqi(200, 150, 800, 475, data_val)
      when 201..300
        iaqi(300, 200, 1600, 800, data_val)
      when 301..400
        iaqi(400, 300, 2100, 1600, data_val)
      when 401..500
        iaqi(500, 400, 2620, 2100, data_val)
      else
        0
    end
  end

  def handle_hour_so2(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 150, 0, data_val)
      when 51..100
        iaqi(100, 50, 500, 150, data_val)
      when 101..150
        iaqi(150, 100, 650, 500, data_val)
      when 151..200
        iaqi(200, 150, 800, 650, data_val)
      when 201..300
        iaqi(300, 200, 1600, 800, data_val)
      when 301..400
        iaqi(400, 300, 2100, 1600, data_val)
      when 401..500
        iaqi(500, 400, 2620, 2100, data_val)
      else
        0
    end
  end

  def handle_co(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 2, 0, data_val)
      when 51..100
        iaqi(100, 50, 4, 2, data_val)
      when 101..150
        iaqi(150, 100, 14, 4, data_val)
      when 151..200
        iaqi(200, 150, 24, 14, data_val)
      when 201..300
        iaqi(300, 200, 36, 24, data_val)
      when 301..400
        iaqi(400, 300, 500, 420, data_val)
      when 401..500
        iaqi(500, 400, 600, 500, data_val)
      else
        0
    end
  end

  def handle_hour_co(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 5, 0, data_val)
      when 51..100
        iaqi(100, 50, 10, 5, data_val)
      when 101..150
        iaqi(150, 100, 35, 10, data_val)
      when 151..200
        iaqi(200, 150, 60, 35, data_val)
      when 201..300
        iaqi(300, 200, 90, 60, data_val)
      when 301..400
        iaqi(400, 300, 120, 90, data_val)
      when 401..500
        iaqi(500, 400, 150, 120, data_val)
      else
        0
    end
  end

  def handle_no2(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 40, 0, data_val)
      when 51..100
        iaqi(100, 50, 80, 40, data_val)
      when 101..150
        iaqi(150, 100, 180, 80, data_val)
      when 151..200
        iaqi(200, 150, 280, 180, data_val)
      when 201..300
        iaqi(300, 200, 565, 280, data_val)
      when 301..400
        iaqi(400, 300, 750, 565, data_val)
      when 401..500
        iaqi(500, 400, 940, 750, data_val)
      else
        0
    end
  end

  def handle_hour_no2(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 100, 0, data_val)
      when 51..100
        iaqi(100, 50, 200, 100, data_val)
      when 101..150
        iaqi(150, 100, 700, 200, data_val)
      when 151..200
        iaqi(200, 150, 1200, 700, data_val)
      when 201..300
        iaqi(300, 200, 2340, 1200, data_val)
      when 301..400
        iaqi(400, 300, 3090, 2340, data_val)
      when 401..500
        iaqi(500, 400, 3840, 3090, data_val)
      else
        0
    end
  end

  #o3 1小时平均值
  def handle_o3(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 160, 0, data_val)
      when 51..100
        iaqi(100, 50, 200, 160, data_val)
      when 101..150
        iaqi(150, 100, 300, 200, data_val)
      when 151..200
        iaqi(200, 150, 400, 300, data_val)
      when 201..300
        iaqi(300, 200, 800, 400, data_val)
      when 301..400
        iaqi(400, 300, 1000, 800, data_val)
      when 401..500
        iaqi(500, 400, 1200, 1000, data_val)
      else
        0
    end
  end

  #o3 8小时滑动平均值
  def handle_hour8_o3(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 100, 0, data_val)
      when 51..100
        iaqi(100, 50, 160, 100, data_val)
      when 101..150
        iaqi(150, 100, 215, 160, data_val)
      when 151..200
        iaqi(200, 150, 265, 215, data_val)
      when 201..300
        iaqi(300, 200, 800, 265, data_val)
      when 301..400
        iaqi(400, 300, 1000, 800, data_val)
      when 401..500
        iaqi(500, 400, 1200, 1000, data_val)
      else
        0
    end
  end

  #24小时平均值
  def handle_24_pm2_5(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 35, 0, data_val)
      when 51..100
        iaqi(100, 50, 75, 35, data_val)
      when 101..150
        iaqi(150, 100, 115, 75, data_val)
      when 151..200
        iaqi(200, 150, 150, 115, data_val)
      when 201..300
        iaqi(300, 200, 250, 150, data_val)
      when 301..400
        iaqi(400, 300, 350, 250, data_val)
      when 401..500
        iaqi(500, 400, 500, 350, data_val)
      else
        0
    end
  end

  #24小时平均值
  def handle_24_pm10(obj_val)
    data_val = obj_val.to_i
    case data_val
      when 0..50
        iaqi(50, 0, 50, 0, data_val)
      when 51..100
        iaqi(100, 50, 150, 50, data_val)
      when 101..150
        iaqi(150, 100, 250, 150, data_val)
      when 151..200
        iaqi(200, 150, 350, 250, data_val)
      when 201..300
        iaqi(300, 200, 420, 350, data_val)
      when 301..400
        iaqi(400, 300, 500, 240, data_val)
      when 401..500
        iaqi(500, 400, 600, 500, data_val)
      else
        0
    end
  end

  def iaqi(iaql_H, iaql_L, bp_H, bp_L, c_v)
    @second,@first,@fs = 0,0,
    @fs = (iaql_H.to_i - iaql_L.to_i).to_f / (bp_H.to_i - bp_L.to_i).to_f
    @first = @fs * (c_v.to_f - bp_L.to_i)
    @second = @first + iaql_L
    @AQI_arr << @second
    @second
  end

  def aqi_days(aqi_val)
    aqi = aqi_val.to_i
    return "否" if aqi == 0
    case aqi
      when 0..50
        "优"
      when 51..100
        "良"
      when 101..150
        "轻度污染"
      when 151..200
        "中度污染"
      when 201..300
        "重度污染"
      when 300..9999
        "严重污染"
      else
        "否"
    end
  end


end