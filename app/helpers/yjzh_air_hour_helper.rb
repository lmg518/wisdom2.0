module YjzhAirHourHelper

  require 'rest-client'

  def self.get_data(data_time, station_id)
    #url = "http://localhost:3030/station_hourly_data.json"
    url = "http://172.168.10.7:3000/station_hourly_data.json"
    headers = {appkey: ApiAuth::AIR_KEY, Authorization: "Token token=#{ApiAuth::AIR_TOKEN}", params: {time_h: data_time, station_id: station_id}}
    response = RestClient.get(url, headers)
    json = JSON.parse(response.body)
    if json["data"].present?
      json["data"]
    else
      {}
    end
  end

  def self.is_exception?(data_arr)
      Rails.logger.info "------1------"
    if data_arr.present?
      Rails.logger.info "------#{data_arr}------"
      fixed_abnormal_rules(data_arr)
      diy_abnormal_rules(data_arr)
      item_avg(data_arr)
      zero_null_negative(data_arr)
      k_dao_gua(data_arr)
    end
  end

  # 固有的异常规则:
  # H：有效数据不足 BB：连接不良 B：运行不良 W：等待数据恢复 HSp：数据超上限 LSp：数据超下限
  # PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
  # NU: 无数据（-99）
  # 自定义:
  # TH1:突然高/低 TH2：突然高/低 （变化率）DBG1：对标高/低 DBG2：对标高/低 （变化率）
  # KDG：颗粒物倒挂
  def self.fixed_abnormal_rules(item_data)
    fixed_item_lable(item_data["avg_so2"], item_data["so2_label"], "SO2", item_data)
    fixed_item_lable(item_data["avg_no2"], item_data["no2_label"], "NO2", item_data)
    fixed_item_lable(item_data["avg_co"], item_data["co_label"], "CO", item_data)
    fixed_item_lable(item_data["avg_o3"], item_data["o3_label"], "O3", item_data)
    fixed_item_lable(item_data["avg_pm25"], item_data["pm25_label"], "PM2.5", item_data)
    fixed_item_lable(item_data["avg_pm10"], item_data["pm10_label"], "PM10", item_data)
  end

  # 自定义异常规则
  def self.diy_abnormal_rules(item_data)
    # ...
  end

  # 出零出负 无数据
  def self.zero_null_negative(item_data)
    Rails.logger.info "------4------"
    zero_item(item_data["avg_so2"], item_data["so2_label"], "SO2", item_data)
    zero_item(item_data["avg_no2"], item_data["no2_label"], "NO2", item_data)
    zero_item(item_data["avg_co"], item_data["co_label"], "CO", item_data)
    zero_item(item_data["avg_o3"], item_data["o3_label"], "O3", item_data)
    zero_item(item_data["avg_pm25"], item_data["pm25_label"], "PM2.5", item_data)
    zero_item(item_data["avg_pm10"], item_data["pm10_label"], "PM10", item_data)
  end

  # 当污染物小时浓度小于等于50微克/立方米，下一个小时浓度上升幅度达到2倍及以上时；
  # 当污染物浓度介于50至200微克/立方米之间，下一个小时浓度上升1倍及以上时；
  # 当小时浓度大于等于200微克/立方米，下一个小时浓度上升0.5倍以上时；
  def self.item_avg(item_data)
    Rails.logger.info "------3------"
    pre_data = get_data((Time.parse(item_data["data_time"]) - 1.hours).strftime("%Y%m%d%H"), item_data["station_id"])
    
    Rails.logger.info "------3-01------"
    Rails.logger.info "---当前时间小时数据---#{item_data["avg_so2"]}------"
    Rails.logger.info "------#{pre_data}------"
    Rails.logger.info "----前1小时数据--#{pre_data[0]["avg_so2"]}------"

    item_hour_data(item_data["avg_so2"], pre_data[0]["avg_so2"], "SO2", item_data,)
    item_hour_data(item_data["avg_no2"], pre_data[0]["avg_no2"], "NO2", item_data,)
    item_hour_data(item_data["avg_co"], pre_data[0]["avg_co"], "CO", item_data,)
    item_hour_data(item_data["avg_o3"], pre_data[0]["avg_o3"], "O3", item_data,)
    item_hour_data(item_data["avg_pm25"], pre_data[0]["avg_pm25"], "PM2.5", item_data,)
    item_hour_data(item_data["avg_pm10"], pre_data[0]["avg_pm10"], "PM10", item_data,)

  end

  # 颗粒物倒挂 PM10 小于 PM2.5 或者相反
  def self.k_dao_gua(item_data)
    Rails.logger.info "------5------"
    pm_25 = item_data["avg_pm25"]
    pm_10 = item_data["avg_pm10"]
    if pm_25 > pm_10
      create_abnormal(station_id,
                      107,
                      "颗粒物倒挂",
                      Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                      "KDG",
                      item_data["data_time"],
                      item_data["avg_pm10"],
                      item_data["avg_pm25"],
                      "颗粒物倒挂数据异常", ''
      )
    end
  end

  private

  # H：有效数据不足 BB：连接不良 B：运行不良 W：等待数据恢复 HSp：数据超上限 LSp：数据超下限
  # PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
  # NU: 无数据（-99）
  def self.fixed_item_lable(item_val, item_lable, item_name, item_data)
    item_flag = %w(B  BB  W  H  HSp  LSp  PZ  PS  AS  CZ  CS  Re RM NU)
    item_dec = %w(运行不良 连接不良 等待数据恢复 有效数据不足 数据超上限 数据超下限 零点检查 跨度检查 精度检查 零点校准 跨度校准 仪器回补数据 自动或人工审核为无效 无数据)
    if item_flag.include?(item_lable)
      index = item_flag.index(item_lable)
      item_des = item_dec[index]
      item_infos = SDataItem.find_by(:item_name => item_name)
      create_abnormal(item_data["station_id"],
                      item_infos.present? ? item_infos.item_code : '',
                      "#{item_name}--#{item_des}",
                      Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                      item_lable,
                      item_data["data_time"],
                      item_val,
                      "",
                      "#{item_name}--#{item_des}",
                      ''
      )
    end
  end

  def self.zero_item(item_val, item_label, item_name, item_data)
    if item_val.present?
      if item_val <= 0
        item_infos = SDataItem.find_by(:item_name => item_name)
        create_abnormal(item_data["station_id"],
                        item_infos.present? ? item_infos.item_code : '',
                        "#{item_name}出零出负",
                        Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                        "KZERO",
                        item_data["data_time"],
                        item_val,
                        "",
                        "#{item_name}出零出负",
                        ''
        )
      end
    end
  end

  def self.item_hour_data(current_data, pre_data, item_name, item_data)
    current_data = current_data.to_i
    pre_data = pre_data.to_i
    @flag = false
    if current_data <= 50
      @flag = true if current_data >= (pre_data * 3)
    end
    if current_data > 50 && current_data < 200
      @flag = true if current_data >= (pre_data * 2)
    end
    if current_data >= 200
      @flag = true if current_data >= (pre_data * 1.5)
    end
    Rails.logger.info "----异常数据创建条件--#{@flag}------"
    if @flag
      Rails.logger.info "------创建异常数据01------"
      item_info = SDataItem.find_by(:item_name => item_name)
      create_abnormal(item_data["station_id"],
                      item_info.present? ? item_info.item_code : '',
                      "#{item_name}数值突然高",
                      Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                      "KTRG",
                      item_data["data_time"],
                      current_data,
                      pre_data,
                      "#{item_name}数值突然高",
                      ''
      )
    end
  end

  def self.create_abnormal(station_id, item_code_id, instance_name, new_time, ab_lable, create_time, val1, val2, des, rule_instance_id)
    Rails.logger.info "------创建异常数据02------"
    DAbnormalDataYyyymm.create!(abnormal_id: '',
                                station_id: station_id,
                                item_code_id: item_code_id,
                                rule_instance: instance_name,
                                create_acce: new_time,
                                ab_lable: ab_lable,
                                ab_data_time: create_time,
                                ab_value: val1,
                                compare_value2: val2,
                                ab_desc: des,
                                s_abnormal_rule_instance_id: rule_instance_id)
  end

end