json.station @station do |s|

  json.region_code s.region_code  #站点城市
  
  json.station_name s.station_name  #站点名
  json.id s.id #站点id

  @regionCode=SRegionCodeInfo.find_by(:id => s.s_region_code_info_id)
  json.unit_name @regionCode.present? ? @regionCode.unit_name : ''  #运维单位

  @task_forms = DTaskForm.where(:d_station_id => s.id)  #找到该站点的所有工单
  @task_forms_p = DTaskForm.where(:d_station_id => s.id,:fault_type => 'polling_form')   #找到该站点的巡检单
  @task_forms_a = DTaskForm.where(:d_station_id => s.id,:fault_type => 'abnormal_form')  #找到该站点的异常巡检单
  @task_forms_f = DTaskForm.where(:d_station_id => s.id,:fault_type => 'fault_form')     #找到该站点的故障单

  @task_forms_d = DTaskForm.where(:d_station_id => s.id,:job_status => ["un_write","writing"])     #待处理的
  @task_forms_o = DTaskForm.where(:d_station_id => s.id,:job_status => ["wait_deal","deal_with","wait_maint","wait_calib"])     #处理中
  @task_forms_u = DTaskForm.where(:d_station_id => s.id,:job_status => 'un_audit')     #待审核
  @task_forms_w = DTaskForm.where(:d_station_id => s.id,:job_status => 'audit')        #已完成

  json.task_forms_num @task_forms.length  #总工单数
  json.task_forms_p @task_forms_p.length  #巡检单数
  json.task_forms_a @task_forms_a.length  #异常巡检单数
  json.task_forms_f @task_forms_f.length  #故障单数

  json.task_forms_d @task_forms_d.length  #待处理
  json.task_forms_o @task_forms_o.length  #处理中
  json.task_forms_u @task_forms_u.length  #待审核
  json.task_forms_w @task_forms_w.length  #已完成

end
 #分页
 #json.list_size @stations.present? ? @stations.total_count : 0

 
