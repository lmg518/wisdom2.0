json.d_task_forms @d_task_forms do |d|
    
        station = d.d_station          #关联查询 站点城市
        json.region_code station.present? ? station.region_code : '' #站点城市
    
        json.station_name d.station_name #站点名
        json.unit_name '河南鑫属'         #运维单位
        json.job_no d.job_no             #工单号
        json.polling_ops_type d.polling_ops_type_str     #任务周期  7 30 90
        json.fault_type d.fault_type_str     #工单类型
        json.author d.author                 #创建人
        json.created_at d.created_at.strftime("%Y-%m-%d %H:%M:%S") #创建时间
    
        json.polling_plan_time d.polling_plan_time.present? ? d.polling_plan_time.strftime("%Y-%m-%d") : ''   #计划完成日期
        json.urgent_level d.urgent_level_str #紧急程度
        json.job_status d.job_status_str     #工单状态

        @handle_man = DLoginMsg.find_by(:id => d.handle_man_id)

        json.handle_man d.handle_man             #运维人员
        json.telphone @handle_man.present? ? @handle_man.telphone : ''  #联系方式
        
    end
    
    json.list_size @d_task_forms.present? ? @d_task_forms.total_count : 0