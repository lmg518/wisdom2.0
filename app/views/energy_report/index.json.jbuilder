#表格名称
json.d_report_forms @d_report_forms do |d|
    json.id d.id
    json.code d.code
    json.nama d.name

    @form_headers_ids = DReportFormHeader.where(:d_report_form_id => d.id).pluck(:d_form_header_id)   #获取表头信息id
    @form_headers = DFormHeader.where(:id =>@form_headers_ids)
    #封装表头信息
    json.form_headers @form_headers do |f|
        json.id f.id
        json.code f.code
        json.name f.name
    end

    #封装项目信息
    @ids = DMaterialReginCode.where(:s_region_code_id => @region_code.id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids)   #从材料表中获取
    json.plants @plants do |p|

        # json.branch_name @region_code.region_name #部门名称  公司
        # json.id p.id
        # json.code p.code
        # json.name p.name   #用电
        # json.day_power '日用电量'
        # json.status 'input'
        #body 字段状态

        if d.code == 'Biological_electricity_form'   # 生物用电报表
            tr1=[]
            item1={}
            item1["branch_name"] = @region_code.region_name  #公司
            item1["status"] = 'show'
            tr1.push(item1)

            tr2=[]
            item2={}
            item2["name"] = p.name  #用电
            item2["status"] = 'show'
            tr2.push(item2)

            tr3=[]
            item3={}
            item3["day_power"] = '日用电量（度）'  #用电
            item3["status"] = 'input'
            tr3.push(item3)

            json.tr1 tr1
            json.tr2 tr2
            json.tr3 tr3
        end

    end

end





#车间项目信息
# json.plants @plants do |p|
#     json.id p.id
#     json.name p.name

#     #查询关系类型
#     @relation_id = DMaterialReginCode.find_by(:s_material_id => p.id)
#     @d_relation_type = DRelationType.find_by(:id => @relation_id.d_relation_type_id)
#     json.relation_type @d_relation_type.name  #消耗/产出

#     daily_report = @daily_report.find_by(:s_material_id => p.id)

#     json.nums daily_report.present? ? daily_report.nums : ''   #当天的日报数据
#     json.daily_yield daily_report.present? ? daily_report.daily_yield : ''   #日收率
# end

# #日报状态
# reports = @daily_report.where(:status => 'Y') if @daily_report.present?
# json.status reports.present? && reports.length >0 ? '已上报' : '未上报' 
