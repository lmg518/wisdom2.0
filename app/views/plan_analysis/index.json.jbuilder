if params[:week_time].present?
      json.week_time params[:week_time]  #查询使用
      work_pros = DWorkPro.active
      json.work_pros {
        json.week_pros work_pros.select{|x| x.work_type == 7}
        json.month_pros work_pros.select{|x| x.work_type == 30}
        json.jd_pros work_pros.select{|x| x.work_type == 90}
        json.year_pros work_pros.select{|x| x.work_type == 360}
      }

      @unit_name_id = params[:unit_name].split(",") if params[:unit_name].present?   #按运维单位id
      @zone_name = params[:zone_name]      #按城市
      @edit_status = params[:edit_status]  #按状态  Y 已定制   N 未定值
    
      ops_plan_week = DOpsPlanManage.where(:week_begin_time => params[:week_time]).by_unit_name_id(@unit_name_id).by_zone_name(@zone_name).by_edit_status(@edit_status).order(:d_station_id).page(params[:page]).per(params[:per])
     
      json.ops_plan_week ops_plan_week do |ops|
        unit = ops.s_region_code_info
        area = ops.s_administrative_area
        province = area.zone_name == "北京市" ? area.zone_name : area.parent.zone_name
        json.id ops.id
        json.procince province
        json.city_name area.zone_name
        json.station_id ops.d_station_id
        json.station_name ops.station_name.split("-")[1]
        json.unit_id unit.present? ? unit.id : ''
        json.unit_name unit.present? ? unit.unit_name : ''
        json.edit_status ops.edit_status_str          #Y 待审核 O 待上报
    
        ops_plan_details = ops.d_ops_plan_details
        #关联任务时间表中的数据
        json.plan_details ops_plan_details do |detail|
          json.id detail.id
          json.d_ops_plan_manage_id detail.d_ops_plan_manage_id
          json.job_name detail.job_name
          json.job_flag detail.job_flag
          json.d_work_pro_id detail.d_work_pro_id
          json.job_time detail.job_time.strftime("%m-%d") if detail.job_time.present?   #strftime("%Y-%m-%d %H:%M:%S") #创建时间
          json.job_checked detail.job_checked
        end
      end

       #分页
      json.list_size ops_plan_week.present? ? ops_plan_week.total_count : 0
      
    else
      json.week_plan_data index  #首页中的json

       #权限验证
       json.login_name @current_user.login_name 
       json.role_name @current_user.s_role_msg.role_name
       
    end
