s_item = SAbnormalLtem.find( @fixed.item_code)
json.fixeds @fi.each do |f|
  d_station = DStation.find(f.station_id)
  json.id f.id
  json.station_id f.station_id
  json.station_name d_station.present? ? d_station.station_name: ''
  json.data_time f.data_time.strftime("%Y-%m-%d %H:%M:%S")
  json.data_value f.data_value
  json.item_code s_item.present? ? s_item.item_name : ''
end

