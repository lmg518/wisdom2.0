json.alarms @alarms do |alarm|
  json.region_name alarm[:region_name]
  json.alarm_count alarm[:alarm_count]
  json.alarm_total @alarm_total
end
json.alarm_total @alarm_total
json.list_count 0
json.region_levels @region_leves