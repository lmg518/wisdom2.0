# json.partial! "s_role_msgs/s_role_msg", s_role_msg: @s_role_msg
# json.funcs @s_role_msg.s_func_msgs.where(:father_func_id => 9999) do |fun|
#   json.id fun.id
#   json.func_code fun.func_code
#   json.func_name fun.func_name
#   json.chil fun.mean_childern_path
# end


json.partial! "s_role_msgs/s_role_msg", s_role_msg: @s_role_msg
@role_funcs = @s_role_msg.s_role_funcs
func_root = SFuncMsg.where(:id => @role_funcs.pluck(:s_func_msg_id) , :father_func_id => 9999) #  一级目录
json.funcs func_root.each do |root|
  json.id root.id
  json.func_code root.func_code
  json.func_name root.func_name
  @chil_arr = []
  @role_funcs.each do |func|
    role_func = SFuncMsg.find_by(:id => func.s_func_msg_id,:valid_flag => 0 )
    next if role_func.blank?
    next if role_func.father_func_id != root.id
    @chil_arr << {id: role_func.id, func_name: role_func.func_name}
  end
  json.chil @chil_arr
end
