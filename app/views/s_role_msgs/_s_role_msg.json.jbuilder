json.extract! s_role_msg, :id, :role_name, :valid_flag, :root_distance, :queue_index, :created_at, :updated_at
json.url s_role_msg_url(s_role_msg, format: :json)