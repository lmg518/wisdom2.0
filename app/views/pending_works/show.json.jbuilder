polling_job_type = @task_work.polling_job_type.split(",")
# @works = DWorkPro.where(id: polling_job_type).select(:id,:work_name)
handle = @task_work.d_fault_handles.select(:id,:handle_type,:handle_time)
task_works = []
(polling_job_type.length).times do |i|
    task_works << { polling_ops_type: @task_work.polling_ops_type_str, #工作项类型
                   polling_plan_time: @task_work.polling_plan_time.strftime("%Y-%m-%d"), #计划完成时间
                   handle_type: handle.present? ? handle[i].handle_type : '', #工作项名称
                   handle_time: @task_work.job_status == 'un_audit' ? handle[i].handle_time.strftime("%Y-%m-%d") : '', #完成时间
                   dis_status: @task_work.handle_man.present? ? '已分配' : '未分配', #分配状态
                   task_status: @task_work.job_status == 'un_audit' ? '已完成' : '未完成', #完成状态
                   handle_man: @task_work.handle_man, #处理人
                } 
    json.task_works task_works 
end
json.s_region_code_info @task_work.s_region_code_info_id #单位id

#查看工单信息
region_code = @task_work.s_region_code_info
station= @task_work.d_station
json.task {
    json.id @task_work.id
    json.region_code region_code.present? ? region_code.unit_name : '' #运维单位
    json.city  station.present? ? "商丘市#{station.region_code}" : ''   #区域城市
    json.job_no @task_work.job_no #工单号
    json.station_name @task_work.station_name #站点
    json.job_status @task_work.job_status_str #工单状态
    json.fault_type @task_work.fault_type_str #工单类型
    json.polling_ops_type @task_work.polling_ops_type_str #运维类型
    json.created_at @task_work.created_at.strftime("%Y-%m-%d %H:%M:%S") #生成时间
    json.clraring_if @task_work.clraring_if == 'N'? "否" : "是"  #是否补录
    json.polling_plan_time @task_work.polling_plan_time.present? ? @task_work.polling_plan_time.strftime("%Y-%m-%d") : '' #计划完成时间
    json.urgent_level @task_work.urgent_level_str #紧急程度
    json.handle_man @task_work.handle_man #当前处理人
    json.create_type @task_work.create_type_str #创建类型
    json.send_type @task_work.send_type_str #发送方式
    json.finish_job "否"  #是否自动提交
}
