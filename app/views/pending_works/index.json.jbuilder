
#  if @task.present?
    #工作项合计
    json.works_size @task.length
    #未分配
    un_write = @task.select{|x| x.job_status == 'un_write' }
    json.un_write un_write.length
    #已分配  
    wait_deal = @task.select{|x| x.job_status == 'wait_deal'}
    json.wait_deal wait_deal.length
    #处理中
    deal_with = @task.select{|x| x.job_status == 'deal_with'}
    json.deal_with deal_with.length
    #已完成
    un_audit = @task.select{|x| x.job_status =='un_audit' }
    json.un_audit un_audit.length
    #超时
    over_time = @task.select{|x| x.job_status != 'un_audit' && (x.polling_plan_time - Time.now) < 0 }
    json.over_time  over_time.length
    #未完成
    not_size = @task.length - un_audit.length
    json.not_size not_size

    #计划完成时间段的工作项
    begin_time = Time.parse(params[:created_time])
    end_time = Time.parse(params[:end_time])
    region_time = (end_time - begin_time) /60 /60 /24
    region_times = region_time.to_i + 1
    day_works = []
    region_times.times do
        polling = @task.select{|x| x.polling_plan_time.strftime("%Y%m%d") == begin_time.strftime("%Y%m%d") } #每天
        un_write_day = polling.select{|x| x.job_status == 'un_write' } #未分配
        wait_deal_day = polling.select{|x| x.job_status == 'wait_deal'} #已分配
        deal_with_day = polling.select{|x| x.job_status == 'deal_with' } #处理中
        un_audit_day = polling.select{|x| x.job_status =='un_audit' } #已完成
        over_time_day = polling.select{|x| x.job_status != 'un_audit' }.select{|x| (x.polling_plan_time - Time.now) < 0 } #超时
        day_works << {day_time: begin_time.strftime("%Y-%m-%d"),un_write_day: un_write_day.length,wait_deal_day: wait_deal_day.length,
                        deal_with_day: deal_with_day.length,un_audit_day: un_audit_day.length,over_time_day: over_time_day.length}
        begin_time = begin_time + 1.day
    end
    json.day_works day_works
     #获取到所选天
    if params[:polling_time].present?
        day_polling = @task.select{|x| x.polling_plan_time.strftime("%Y-%m-%d") == params[:polling_time] }
        #根据状态获取到工单信息
        if params[:job_status].present?
            job_polling = day_polling.select{|x| x.job_status == params[:job_status] }
            json.polling DTaskForm.type_polling(job_polling)
        elsif params[:over_time].present? #获得超时状态
            job_polling = day_polling.select{|x| x.job_status != 'un_audit' && (x.polling_plan_time - Time.parse(params[:over_time])) < 0 }
            json.polling DTaskForm.type_polling(job_polling)
        else
            json.polling DTaskForm.type_polling(day_polling)
        end
    end 
# end

