json.smaterial @smaterials   #所有的岗位信息
json.menus_datas @menus_datas  #一级二级菜单数据

json.list_datas @list_datas  #列表数据
#json.list_datas @all_list_datas  #列表数据
#json.wk_datas @wk_datas  #周日期数据

if @smaterial.present?
  json.code @smaterial.code #列表数据添加标识
end

#json.ratios @daily_reports.pluck(:field_code,:datetime,:ratio)

#周盘点数据
json.titles @titles
json.check_data @check_data

#折线图数据
json.days @days
json.line_datas @line_datas

#分页数据
json.list_size @page_num
#json.list_size @daily_reports.present? ? @daily_reports.total_count : 0
