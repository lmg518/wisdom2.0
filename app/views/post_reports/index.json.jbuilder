father_region = SRegionCode.find_by(:id => @s_region_codes[0].father_region_id) if @s_region_codes[0].present?
father_region_name = father_region.present? ? father_region.region_name : ''
json.region_datas @s_region_codes do |s|
  json.id s.id
  json.region_name s.region_name
  json.father_region_name father_region_name

  if 'TYSYFGS01' == s.region_code
    json.status 'Y'
  else
    json.status 'N'
  end

end


#json.list_size @region_datas.present? ? @region_datas.total_count : 0