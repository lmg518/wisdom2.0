@task1 =  @task.select{|x| x.job_status == 'audit' }
json.task1_size  @task1.present? ? @task1.size : 0 #已完成
@task2 =  @task.select{|x| x.job_status != 'audit' }
json.task2_size @task2.present? ? @task2.size : 0  #未完成

json.task1 @task1 do |task|
    station = task.d_station
    unit = task.s_region_code_info
    json.id task.id #id
    json.unit_name unit.present? ? unit.unit_name : '' #运维单位
    json.author task.author #创建人
    json.handle_man task.handle_man #处理人
    json.job_no task.job_no #工单号
    json.job_status task.job_status_str #工单状态
    json.fault_type task.fault_type_str #工单类型
    json.create_type task.create_type_str #创建类型
    json.urgent_level task.urgent_level_str #紧急程度
    json.d_station_id task.d_station_id #站点Id
    json.station_name task.station_name #站点名称
    json.device_name task.device_name  #设备名称
    json.polling_plan_time task.polling_plan_time #计划完成时间
    json.polling_ops_type task.polling_ops_type_str #运维类型
    json.polling_job_type task.polling_job_type #检查类型
    # json.abnormal_begin_time task.abnormal_begin_time #异常开始时间
    # json.abnormal_end_time task.abnormal_end_time #异常结束时间
    # json.abnormal_notes task.abnormal_notes  #异常现象
    # json.fault_phenomenon task.fault_phenomenon #故障现象
    json.title task.title #标题
    json.content task.content #内容
    json.audit_if task.audit_if #是否审核
    json.audit_man task.audit_man #审核人
    json.audit_time task.audit_time #审核时间
    json.audit_des task.audit_des #审核不通过原因
    json.s_region_code_info_id task.s_region_code_info_id #单位id
    json.created_at task.created_at.strftime("%Y-%m-%d %H:%M:%S") #创建时间
end
json.task2 @task2 do |task|
    station = task.d_station
    unit = task.s_region_code_info
    json.id task.id #id
    json.unit_name unit.present? ? unit.unit_name : '' #运维单位
    json.author task.author #创建人
    json.handle_man task.handle_man #处理人
    json.job_no task.job_no #工单号
    json.job_status task.job_status_str #工单状态
    json.fault_type task.fault_type_str #工单类型
    json.create_type task.create_type_str #创建类型
    json.urgent_level task.urgent_level_str #紧急程度
    json.d_station_id task.d_station_id #站点Id
    json.station_name task.station_name #站点名称
    json.device_name task.device_name  #设备名称
    json.polling_plan_time task.polling_plan_time #计划完成时间
    json.polling_ops_type task.polling_ops_type_str #运维类型
    json.polling_job_type task.polling_job_type #检查类型
    # json.abnormal_begin_time task.abnormal_begin_time #异常开始时间
    # json.abnormal_end_time task.abnormal_end_time #异常结束时间
    # json.abnormal_notes task.abnormal_notes  #异常现象
    # json.fault_phenomenon task.fault_phenomenon #故障现象
    json.title task.title #标题
    json.content task.content #内容
    json.audit_if task.audit_if #是否审核
    json.audit_man task.audit_man #审核人
    json.audit_time task.audit_time #审核时间
    json.audit_des task.audit_des #审核不通过原因
    json.s_region_code_info_id task.s_region_code_info_id #单位id
    json.created_at task.created_at.strftime("%Y-%m-%d %H:%M:%S") #创建时间
end