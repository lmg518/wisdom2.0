json.extract! d_station, :id, :station_name, :region_code, :station_type, :object_id, :station_location, :longitude, :latitude, :station_ip, :station_mac, :service_no, :setup_date, :station_status, :is_online, :note, :created_at, :updated_at
json.url d_station_url(d_station, format: :json)
json.aa d_station.tap{|x| hash = {id: x.id, station_name: x.station_name, region_code: x.region_code,
                               station_type: x.station_type, object_id: x.object_id, station_location: x.station_location,
                               longitude: x.longitude, latitude: x.latitude, station_ip: x.station_ip, station_mac: x.station_mac,
                               service_no: x.service_no, setup_date: x.setup_date,station_status: x.station_status, is_online: x.is_online,
                               note: x.note, created_at: x.created_at, updated_at: x.updated_at, url: d_station_url(x.id, format: :json) }}