# json.all_statio @d_stations, partial: 'd_stations/d_station', as: :d_station
json.all_stations @d_stations do |station|
    json.id station.id
    json.station_name station.station_name
    json.region_code station.get_region
    json.station_type station.station_type
    json.station_location station.station_location
    json.longitude station.longitude
    json.latitude station.latitude
    json.station_ip station.station_ip
    json.station_mac station.station_mac
    json.service_no station.service_no
    json.setup_date station.setup_date
    json.station_status station.station_status
    json.is_online station.is_online
end
json.list_size @d_stations.present? ? @d_stations.total_count : 0
