json.templetes @templetes do |t|
    json.id t.id
    json.create_user t.create_user     #创建人
    json.name t.name                   #模板名称
    json.title t.title                 #标题
    json.templete_type t.templete_type #类型
    json.content t.content             #内容
    json.created_at t.created_at.strftime("%Y-%m-%d %H:%M:%S")       #创建时间
end

#分页
json.list_size @templetes.present? ? @templetes.total_count : ''