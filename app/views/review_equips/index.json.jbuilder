json.review_equips @review_equips do |h|
  json.id h.id
  json.s_region_code_id h.s_region_code_id
  json.region_name h.region_name
  json.equip_name h.equip_name
  json.s_equip_id h.s_equip_id
  json.equip_code h.equip_code
  json.handle_name h.handle_name
  json.handle_start_time h.handle_start_time.present? ? h.handle_start_time.strftime('%Y-%m-%d') : ''
  json.handle_end_time h.handle_end_time.present? ? h.handle_end_time.strftime('%Y-%m-%d') : ''
  json.snag_desc h.snag_desc
  json.status HandleEquip.set_status(h.status)
  json.desc h.desc
  json.founder h.founder
end
json.review_equips_list @review_equips.present? ? @review_equips.total_count : 0