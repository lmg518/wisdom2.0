json.task_from {
    json.id @d_task_form.id
    json.job_no @d_task_form.job_no #工单号
    json.station_name @d_task_form.station_name #来源
    json.job_status @d_task_form.job_status_str #工单状态
    json.fault_type @d_task_form.fault_type_str #工单类型
    json.polling_ops_type @d_task_form.polling_ops_type_str #运维类型
    json.created_at @d_task_form.created_at.strftime("%Y-%m-%d %H:%M:%S") #生成时间
    json.title @d_task_form.title #标题
    json.clraring_if @d_task_form.clraring_if == 'N'? "否" : "是"  #是否补录
    json.polling_plan_time @d_task_form.polling_plan_time.present? ? @d_task_form.polling_plan_time.strftime("%Y-%m-%d %H:%M:%S") : '' #计划完成时间
    json.audit_man @d_task_form.audit_man #审核人
    json.audit_time @d_task_form.audit_time #审核时间

    # #备机信息
    # @equilp = SEquipmentInfo.find_by(:d_station_id =>@d_task_form.d_station_id,:equip_type =>@d_task_form.device_name,
    #                                     :types => 'N',:d_task_form =>@d_task_form.id)
    # json.stand_num @equilp.present? ? @equilp.stand_num : '' #备机编号

    #处理表
    @handle = @d_task_form.d_fault_handles
    json.polling_job_type @handle do |handle| 
        json.id handle.id
        json.img_one handle.img_one
        json.handle_type handle.present? ? handle.handle_type : '' #处理方式

        @work_pro = DWorkPro.find_by(:work_name => handle.handle_type)
        json.d_work_pro_id @work_pro.present? ? @work_pro.id : ''     #工作项id
        json.work_code @work_pro.present? ? @work_pro.work_code : ''  #工作项编码

    end
    #检查类型
    json.abnormal_notes @d_task_form.abnormal_notes.present? ? @d_task_form.abnormal_notes : '' #异常现象
    json.abnormal_begin_time @d_task_form.abnormal_begin_time.present? ? @d_task_form.abnormal_begin_time.strftime("%Y-%m-%d %H:%M:%S") : ''#异常开始时间
    json.abnormal_end_time @d_task_form.abnormal_end_time.present? ? @d_task_form.abnormal_end_time.strftime("%Y-%m-%d %H:%M:%S") : ''#异常结束时间
    @details = @d_task_form.d_fault_job_details
    #日志表
    json.details @details do |details|
        json.job_status details.present? ? details.job_status_str : '' #步骤
        json.handle_man details.handle_man #处理人
        json.begin_time details.present? ? details.begin_time.strftime("%Y-%m-%d %H:%M:%S") : '' #开始时间
        json.end_time details.present? ? details.end_time.strftime("%Y-%m-%d %H:%M:%S") : '' #完成时间
        json.note details.note #说明
        json.handle_status details.handle_status #处理状态
        json.without_time_flag details.without_time_flag #超时状态
    end
    json.login_name @current_user.login_name
    json.s_region_code_info_id  @d_task_form.s_region_code_info_id  #单位ID
}

#流程图数据
flow=['创建工单','工单待处理','工单处理中','工单待复核','工单待审核','工单已审核']
json.flow flow

