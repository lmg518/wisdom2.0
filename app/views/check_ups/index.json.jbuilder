#运维人员登录时只显示他的工单
if @current_user.s_role_msg.role_name == '运维成员'
    @tasks=DTaskForm.where(:fault_type => 'polling_form')
                          .by_handle_man_id(@current_user.id)
                          .by_station_id(params[:station_id])
                          .by_job_no(params[:job_no])
                          .by_author(params[:by_author])
                          .by_create_type(params[:job_type])
                          .by_job_status_s(params[:job_status])
                          .by_time(params[:created_time],params[:end_time])
                          .by_fault_type(params[:polling_form])
                          .order(:created_at => "desc")

else
    @tasks = DTaskForm.where(:fault_type => 'polling_form')
                        .by_station_id(params[:station_id])
                        .by_job_no(params[:job_no])
                        .by_author(params[:by_author])
                        .by_create_type(params[:job_type])
                        .by_job_status_s(params[:job_status])
                        .by_time(params[:created_time],params[:end_time])
                        .by_fault_type(params[:polling_form])
                        .order(:created_at => "desc")
end


@d_task_forms = @tasks.page(params[:page]).per(params[:per])

json.task_forms @d_task_forms do |task|
  station = task.d_station
  unit = task.s_region_code_info
  json.id task.id
  json.unit_name unit.present? ? unit.unit_name : ''
  json.author task.author
  json.handle_man task.handle_man
  json.handle_man_old task.handle_man_old
  json.job_no task.job_no
  json.job_status task.job_status_str
  json.fault_type task.fault_type_str
  json.create_type task.create_type_str
  json.urgent_level task.urgent_level_str
  json.send_type task.send_type_str
  json.d_station_id task.d_station_id
  json.station_name task.station_name
  json.device_name task.device_name
  json.polling_plan_time task.polling_plan_time
  json.polling_ops_type task.polling_ops_type_str
  json.polling_job_type task.polling_job_type
  json.clraring_if task.clraring_if
  json.abnormal_begin_time task.abnormal_begin_time
  json.abnormal_end_time task.abnormal_end_time
  json.abnormal_notes task.abnormal_notes
  json.fault_phenomenon task.fault_phenomenon
  json.other_fault task.other_fault
  json.title task.title
  json.content task.content
  json.audit_if task.audit_if
  json.audit_man task.audit_man
  json.audit_time task.audit_time
  json.audit_des task.audit_des
  json.s_region_code_info_id task.s_region_code_info_id
  json.created_at task.created_at.strftime("%Y-%m-%d %H:%M:%S")
end
  #人员身份,权限
  json.login_name @current_user.login_name 
  json.role_name @current_user.s_role_msg.role_name
json.list_size @d_task_forms.present? ? @d_task_forms.total_count : 0
