json.extract! s_administrative_area, :id, :zone_name, :zone_rank, :parent_id, :des, :active_state, :sort_num, :created_at, :updated_at
json.url s_administrative_area_url(s_administrative_area, format: :json)
