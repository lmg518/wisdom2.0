# json.areas @s_administrative_areas, partial: 's_administrative_areas/s_administrative_area', as: :s_administrative_area
json.areas @s_administrative_areas do |area|
    json.id area.id
    json.name area.zone_name
    json.code area.zone_rank
    json.chil  area.children do |chil|
      json.id chil.id
      json.name chil.zone_name
      json.code chil.zone_rank
    end

end
