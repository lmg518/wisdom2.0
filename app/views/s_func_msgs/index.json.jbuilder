json.funcs @s_func_msgs do |fun|
  json.id fun.id
  json.func_code fun.func_code
  json.func_name fun.func_name
  json.chil fun.mean_childern_path
end