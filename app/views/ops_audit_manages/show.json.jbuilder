json.ops_plan {
    json.id @ops_plan.id
    json.d_station_id @ops_plan.d_station_id
    json.station_name @ops_plan.station_name
    json.s_administrative_area_id @ops_plan.s_administrative_area_id
    json.zone_name @ops_plan.zone_name
    json.s_region_code_info_id @ops_plan.s_region_code_info_id
    json.unit_name @ops_plan.unit_name
    json.edit_status @ops_plan.edit_status
    json.week_begin_time @ops_plan.week_begin_time
    json.week_end_time @ops_plan.week_end_time
    json.month_flag @ops_plan.month_flag
    json.week_flag @ops_plan.week_flag

    json.week_time_2 @ops_plan.week_begin_time+1.day
    json.week_time_3 @ops_plan.week_begin_time+2.day
    json.week_time_4 @ops_plan.week_begin_time+3.day
    json.week_time_5 @ops_plan.week_begin_time+4.day
    json.week_time_6 @ops_plan.week_begin_time+5.day
}
json.week_pro @week_pro
json.month_pro @month_pro
json.jd_pro @jd_pro
json.year_pro @year_pro