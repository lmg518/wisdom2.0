json.regions @units do |unit|
    json.id unit.id #序号
    json.unit_name unit.unit_name #单位名称
    json.unit_code unit.unit_code #单位编码
    json.region_code  unit.s_region_code.present? ? unit.s_region_code : '' #所属单位


    # station_ids=unit.station_id.split(",")   #查询运维单位下分配的站点
    # stations = DStation.where(:id => station_ids);  
    # station_name=''
    # json.stations stations do |s|
    #     station_name += (s.station_name+",")
    # end
    # json.station_name station_name



end
json.list_size @units.present? ? @units.total_count : 0