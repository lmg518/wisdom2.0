#json.region @unit


json.region {
    json.id @unit.id
    json.s_region_code_id @unit.s_region_code_id
    json.s_region_code @unit.s_region_code_id.present? && SRegionCode.find(@unit.s_region_code_id).present? ? SRegionCode.find(@unit.s_region_code_id).region_name : ''  #运维单位所属区域
    json.linkman @unit.linkman
    json.linkman_tel @unit.linkman_tel
    json.unit_address @unit.unit_address
    json.unit_name @unit.unit_name
    json.unit_code @unit.unit_code
    json.status @unit.status
    json.station_id @unit.station_id

    station_ids=@unit.station_id.split(",")

    stations = DStation.where(:id => station_ids);  #查询运维单位下分配的站点
    station_name=''
    json.stations stations do |s|
       station_name += (s.station_name+",")
    end
    json.station_name station_name

}