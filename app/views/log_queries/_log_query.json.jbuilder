json.extract! log_query, :id, :created_at, :updated_at
json.url log_query_url(log_query, format: :json)