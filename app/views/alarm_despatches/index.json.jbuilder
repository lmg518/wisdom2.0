json.despatches @despatches do |despatch|
  alarms = despatch.d_alarms
  station = alarms.map{|x|  x.d_station.station_name}
  json.stations station
  json.rule_name alarms.map{|x| x.alarm_rule}
  json.rule_leves alarms.map{|x| x.alarm_level_to_s}
  json.id despatch.id
  json.despatch_time despatch.despatch_time.strftime("%Y-%m-%d %H:%M:%S")
  json.despatch_login despatch.despatch_login
  json.task_status despatch.status_to_str
  json.receive_login despatch.receive_login
  json.receive_time despatch.receive_time.present? ? despatch.receive_time.strftime("%Y-%m-%d %H:%M:%S") : ''
  json.deal_time despatch.deal_time.present? ? despatch.deal_time.strftime("%Y-%m-%d %H:%M:%S") : ''
end
json.list_size @despatches.present? ? @despatches.total_count : 0