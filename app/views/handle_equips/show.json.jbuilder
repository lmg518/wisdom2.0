json.handle_equip {
  json.id @handle_equip.id
  json.s_region_code_id @handle_equip.s_region_code_id
  json.region_name @handle_equip.region_name
  json.equip_name @handle_equip.equip_name
  json.s_equip_id @handle_equip.s_equip_id
  json.equip_code @handle_equip.equip_code
  json.handle_name @handle_equip.handle_name
  json.handle_start_time @handle_equip.handle_start_time.present? ? @handle_equip.handle_start_time.strftime('%Y-%m-%d %H:%M') : ''
  json.handle_end_time @handle_equip.handle_end_time.present? ? @handle_equip.handle_end_time.strftime('%Y-%m-%d %H:%M') : ''
  json.snag_desc @handle_equip.snag_desc
  json.status HandleEquip.set_status(@handle_equip.status)
  json.desc @handle_equip.desc
  json.founder @handle_equip.founder
  json.img_path @handle_equip.img_path
}
json.start_note @start_note
json.end_note @end_note
json.spares @spares ? @spares : []
json.equip_boots @equip_boots
