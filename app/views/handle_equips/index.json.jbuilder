json.handle_equips @handle_equips do |h|
  colour = HandleEquip.handle_remind(h)
  json.id h.id
  json.s_region_code_id h.s_region_code_id
  json.region_name h.region_name
  json.equip_name h.equip_name
  json.s_equip_id h.s_equip_id
  json.equip_code h.equip_code
  json.handle_name h.handle_name
  json.handle_start_time h.handle_start_time.present? ? h.handle_start_time.strftime('%Y-%m-%d %H:%M') : ''
  json.handle_end_time h.handle_end_time.present? ? h.handle_end_time.strftime('%Y-%m-%d %H:%M') : ''
  json.snag_desc h.snag_desc
  json.status HandleEquip.set_status(h.status)
  json.desc h.desc
  json.founder h.founder
  json.img_path h.img_path
  json.colour colour ? colour : ''
end
json.handle_equips_list @handle_equips.present? ? @handle_equips.total_count : 0
#人员身份,权限
json.login_name @current_user.login_name 
json.role_name @current_user.s_role_msg.role_name
json.role_code @current_user.s_role_msg.role_code