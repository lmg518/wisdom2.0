#json.daily_reports @daily_reports

#折现图数据
#月份数量
json.months @months
json.month_days @month_days


json.daily_reports @daily_reports do |d|
    json.id d.id
    json.nums d.nums    #日产量
    json.month d.datetime
    json.name d.name    #项目
    #分公司
    #json.region_code_info @region_code_info.unit_name

    #判断是车间级  分公司级  还是总公司级
    code = SRegionCode.find_by(:id => d.s_region_code_id)
    @region_level = SRegionLevel.find_by(:id => code.s_region_level_id)
    if @region_level.level_name == '车间'
        region_name = SRegionCode.find_by(:id => code.father_region_id).region_name
        #分公司
        json.region_code_info region_name

    elsif @region_level.level_name == '分公司'

        
    elsif @region_level.level_name == '总公司'

    end

    #车间
    #json.region_code @region_code.region_name

    #根据 项目  查找 车间
    material = SMaterial.find_by(:name => d.name)
    relationcode = DMaterialReginCode.find_by(:s_material_id => material.id, :s_region_code_id => d.s_region_code_id) #查询当前车间中的项目
    #relationcode = DMaterialReginCode.find_by(:id => material.id)
    region_code = SRegionCode.find_by(:id => relationcode.s_region_code_id)
    json.region_code region_code.region_name

    #查询关系类型
    #@relation_id = DMaterialReginCode.find_by(:s_material_id => p.id)
    @d_relation_type = DRelationType.find_by(:id => relationcode.d_relation_type_id)
    json.relation_type @d_relation_type.name  #消耗/产出
    

    #计算累计量
    sum_nums = 0
    nums= @sum_dates.where(:name => d.name).pluck(:nums)
    if nums.length > 0
        nums.each do |n|
            sum_nums += n
        end
        json.sum_nums sum_nums   #累计产量
    else
        json.sum_nums sum_nums
    end
end

 #分页
 json.list_size @daily_reports.present? ? @daily_reports.total_count : 0