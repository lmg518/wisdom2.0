# json.array! @d_login_msgs, partial: 'd_login_msgs/d_login_msg', as: :d_login_msg
json.d_login_msgs @d_login_msgs do |login|
  #unit = login.s_region_code_info

  #查询人员所在的车间
  region_code = login.s_region_code
  json.region_code region_code.present? ? region_code.region_name : ''

  json.id login.id
  json.login_no login.login_no
  json.name login.login_name
  #json.region_code  unit.present? ? unit.unit_name  : ""
  json.role_code login.s_role_msg.present? ? login.s_role_msg.role_name : ""
  json.valid_flag login.valid_flag
  json.valid_begin login.valid_begin
  json.valid_end login.valid_end
  json.ip_address login.ip_address
  json.des login.describe
  json.telphone login.telphone
  json.id_card login.id_card
  json.email login.email
  json.work_num login.work_num
  json.work_status login.work_status
  #json.station login.d_stations.present? ? login.d_stations.each{|station| station.station_name} : ''

  #人员所在的车间区域
  area = SArea.find_by(:id => login.s_area_id)
  area_name = area.present? ? area.name : ''
  json.area_name area_name

end

json.list_size @d_login_msgs.total_count