json.exception_sets @exception_sets do |set|
  json.id set.id
  json.instance_name set.instance_name
  json.item_name set.s_abnormal_ltem.present? ? set.s_abnormal_ltem.item_name : ''
  json.rule_name set.s_abnormmal_rule.present? ? set.s_abnormmal_rule.rule_name : ''
  json.status set.status
end

json.list_size @exception_sets.present? ? @exception_sets.total_count : 0