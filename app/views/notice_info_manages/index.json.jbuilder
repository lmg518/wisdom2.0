json.notice_infos @notice_infos do |n|
    json.id n.id
    json.auther n.auther     #创建人
    json.title n.title       #标题
    json.send_time n.send_time.present? ? n.send_time.strftime("%Y-%m-%d %H:%M:%S") : ''   #发送时间
    json.send_type n.send_type_str      #下发方式  sms  app  email
    json.content n.content              #内容
    json.status n.status_str            #状态  N未发送  Y已发送
    json.created_at n.created_at.strftime("%Y-%m-%d %H:%M:%S")  #创建时间
end

#查询所有模板内的内容
templete_app=DTemplete.where(:templete_type => 'APP')
templete_sms=DTemplete.where(:templete_type => '短信')
templete_email=DTemplete.where(:templete_type => '邮件')
json.templete_types{
    json.templete_app templete_app
    json.templete_sms templete_sms
    json.templete_email templete_email
}



#分页
json.list_size @notice_infos.present? ? @notice_infos.total_count : 0