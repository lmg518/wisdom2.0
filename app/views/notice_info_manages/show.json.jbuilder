json.d_notice_info @d_notice_info 


# json.id n.id
# json.auther n.auther     #创建人
# json.title n.title       #标题
# json.send_time n.send_time.present? ? n.send_time.strftime("%Y-%m-%d %H:%M:%S") : ''   #发送时间
# json.send_type n.send_type_str      #下发方式  sms  app  email
# json.content n.content              #内容
# json.status n.status_str            #状态  N未发送  Y已发送
# json.created_at n.created_at.strftime("%Y-%m-%d %H:%M:%S")  #创建时间