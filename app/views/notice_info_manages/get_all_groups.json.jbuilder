json.groups @groups do |g|
    json.id g.id
    json.group_name g.group_name

    #查询组下的所有成员
    @group_workers=g.d_worker_groups
    json.group_workers @group_workers do |w|
        json.id DNoticeWorker.find(w.d_notice_worker_id).present? ? DNoticeWorker.find(w.d_notice_worker_id).id : ''
        json.name DNoticeWorker.find(w.d_notice_worker_id).present? ? DNoticeWorker.find(w.d_notice_worker_id).name : ''
    end

end




