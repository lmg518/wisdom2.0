json.equip_works @equip_works do |e|

  #colour = DEquipWork.handle_remind(e)
  #json.region_id e.s_region_code_info_id   #部门id 取消

  json.id e.id
  #json.s_region_code_id e.s_region_code_id  #车间id
  json.region_id e.s_region_code_id  #车间id

  json.region_name e.region_name

  s_area = SArea.find_by(:id => e.s_area_id)  #查询区域
  area_name = s_area.present? ? s_area.name : ''
  area_id = s_area.present? ? s_area.id : ''
  json.area_name area_name  #区域名称
  json.area_id area_id      #区域id

  json.work_person e.work_person
  json.work_start_time e.work_start_time ? e.work_start_time : ''
  json.work_end_time e.work_end_time ? e.work_end_time : ''
  json.work_desc e.work_desc
  json.founder e.fourder

  #json.colour colour ? colour : ''
end

json.equip_works_size @equip_works ? @equip_works.total_count : 0
#人员身份,权限
json.login_name @current_user.login_name 
json.role_name @current_user.s_role_msg.role_name
json.role_code @current_user.s_role_msg.role_code