json.equips @equips do |e|
  json.id e.id
  json.s_region_code_id e.s_region_code_id
  json.region_name e.region_name     #车间名称
  json.bit_code e.bit_code           #位号
  json.equip_code e.equip_code       #设备编号
  json.equip_name e.equip_name       #设备名称
  json.equip_location e.equip_location  #安装位置
  json.equip_norm e.equip_norm
  json.equip_nature e.equip_nature
  json.equip_material e.equip_material
  json.equip_num e.equip_num
  json.apper_code e.apper_code
  json.apper_time e.apper_time.present? ? e.apper_time.strftime("%Y-%m-%d") : ''
  json.equip_status e.equip_status.present? ? SEquip.set_status(e.equip_status) : ''
  json.factory e.factory
  json.equip_note e.equip_note


  s_area = SArea.find_by(:id => e.s_area_id)  #查询区域
  area_name = s_area.present? ? s_area.name : ''
  json.area_name area_name  #区域名称


end
json.equips_list @equips.present? ? @equips.total_count : 0
#人员身份,权限
json.login_name @current_user.login_name 
json.role_name @current_user.s_role_msg.role_name
json.role_code @current_user.s_role_msg.role_code