#json.faults
    handle = @faultJob.d_fault_handles.first
    @job_detail = @faultJob.d_fault_job_details
    @equip_calib = SEquipCalib.order(:created_at => :desc).find_by(:d_task_form_id => @faultJob.id)


    #设备信息
    #equipment_infos=@faultJob.s_equipment_infos

    equipment_infos_y=SEquipmentInfo.find(@faultJob.s_equipment_info_id)   #正在使用的设备
    equilp=SEquipmentInfo.find_by(:d_station_id => @faultJob.d_station_id,:d_task_form_id =>@faultJob.id,:types => 'O')  #备机
   
    json.equilp equipment_infos_y  #正在使用的设备
    # equipment_infos_n.each do |equilp|
    #json.stand_num equilp.present? ? equilp.stand_num : '' #备机编号
    json.stand_num equilp.present? ? equilp.equip_nums : '' #备机编号
    # end


    # if equipment_infos.length == 1
    #     json.equilp equipment_infos[0]  #故障设备信息
    # else
    #     equipment_infos.each do |equilp|
    #         if equilp.types == 'Y'
    #             json.equilp equilp  #故障设备信息
    #         else
    #             json.stand_num equilp.stand_num #备机编号
    #         end
    #     end
    # end

   

#站点信息
    #@station=@faultJob.d_station
    #json.station=@station

    #工单信息
    json.id @faultJob.id #id
    json.job_no @faultJob.job_no #工单号
    json.station_name @faultJob.station_name #来源(站点)
    json.created_at @faultJob.created_at.strftime("%Y-%m-%d %H:%M:%S") #生成时间
    json.job_status @faultJob.job_status_str #工单状态
    json.fault_type @faultJob.fault_type_str #工单类型
    json.title @faultJob.title #工单标题
    json.clraring_if @faultJob.clraring_if #是否补录
    json.content @faultJob.content #工单内容
    json.device_name @faultJob.device_name #设备名称
    json.fault_phenomenon @faultJob.fault_phenomenon #故障现象

    #维修项目信息
    json.maint_project {
        json.maint_name @maint_project.present? ? @maint_project.maint_name : ''  #维修项目
        json.maint_code @maint_project.present? ? @maint_project.maint_code : ''  #处理人
        json.maint_time @maint_project.present? ? @maint_project.created_at.strftime("%Y-%m-%d %H:%M:%S") : '' #处理时间
    }

    #工单中的审核信息
    json.audit_if @faultJob.audit_if      #是否审核
    json.audit_man @faultJob.audit_man    #审核人
    json.audit_time @faultJob.audit_time.present? ? @faultJob.audit_time.strftime("%Y-%m-%d %H:%M:%S") : '' #审核时间
    json.audit_des @faultJob.audit_des    #不通过原因
    json.work_flag @maint_project.present? ?  @maint_project.work_flag : ''   #审核通过时的处理方式


    #故障处理
    json.handle {
        json.handle_remote handle.present? ? handle.handle_remote : '' #远程处理(是： Y； 否： N)
        json.handle_type handle.present? ? handle.handle_type : '' #处理方式
        json.handle_des handle.present? ? handle.handle_des : '' #处理说明
        json.handle_forms handle.present? ? handle.handle_forms : '' #处理记录
        json.handle_man handle.present? ? handle.handle_man : '' #处理人
        json.handle_time handle.present? && handle.handle_time.present? ? handle.handle_time.strftime("%Y-%m-%d %H:%M:%S") : '' #处理时间
        json.img_one handle.present? ? handle.img_one : '' #维修图片
    }
    
    #校准信息
    json.equip_calib {
        json.calibs_man @equip_calib.present? ? @equip_calib.calibs_man : ''  #校准人
        json.calibs_if @equip_calib.present? ? @equip_calib.calibs_if : ''    #校准结果
        json.calibs_des @equip_calib.present? ? @equip_calib.calibs_des : ''  #校准不通过原因
        json.calibs_time @equip_calib.present? ? @equip_calib.calibs_time.strftime("%Y-%m-%d %H:%M:%S") : '' #校准时间
        json.img_one @equip_calib.present? ? @equip_calib.img_one : ''        #图片
        
    }


    #日志表信息
    json.detail @job_detail do |detail|
        json.job_status detail.present? ? detail.job_status_str : '' #步骤
        json.begin_time detail.present? ? detail.begin_time.strftime("%Y-%m-%d %H:%M:%S") : '' #开始处理时间
        json.end_time detail.present? ? detail.end_time.strftime("%Y-%m-%d %H:%M:%S") : '' #完成时间
        json.note detail.present? ? detail.note : '' #说明
        json.handle_man detail.present? ? detail.handle_man : '' #处理人
        json.handle_status detail.present? ? detail.handle_status : '' #处理状态
        json.without_time_flag detail.present? ? detail.without_time_flag : '' #超时状态
    end
    json.s_region_code_info_id  @faultJob.s_region_code_info_id  #单位ID
    #审核通过时的处理方式
   # json.maint_project{
        #json.work_flag @maint_project.present? ?  @maint_project.work_flag : ''
   # }

    #流程图数据
    flow=['创建工单','工单待处理','设备待维修','设备待校准','工单待审核','工单已审核']
    json.flow flow

        