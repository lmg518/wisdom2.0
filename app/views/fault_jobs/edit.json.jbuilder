#工单信息
json.faultJob{
    json.id @faultJob.id #id
    json.job_no @faultJob.job_no #工单号
    json.station_name @faultJob.station_name #来源(站点)
    json.created_at @faultJob.created_at.strftime("%Y-%m-%d %H:%M:%S") #生成时间
    json.job_status @faultJob.job_status_str #工单状态
    json.fault_type @faultJob.fault_type_str #工单类型
    json.title @faultJob.title #工单标题
    json.clraring_if @faultJob.clraring_if #是否补录
    json.content @faultJob.content #工单内容
    json.device_name @faultJob.device_name #设备名称
    json.fault_phenomenon @faultJob.fault_phenomenon #故障现象
}

#备机编号
json.stand_nums @equilp do |e|
    json.id e.id
    json.stand_num e.equip_nums
end

equipment_infos_y=SEquipmentInfo.find(@faultJob.s_equipment_info_id)   #正在使用的设备
json.equilp equipment_infos_y    #故障设备信息

# equipment_infos=@faultJob.s_equipment_infos
# equipment_infos.each do |equilp|
#     if equilp.types == 'Y'
#         json.equilp equilp  #故障设备信息
#     end
# end


@job_detail = @faultJob.d_fault_job_details
json.detail @job_detail do |detail|
    json.job_status detail.present? ? detail.job_status_str : '' #步骤
    json.begin_time detail.present? ? detail.begin_time.strftime("%Y-%m-%d %H:%M:%S") : '' #开始处理时间
    json.end_time detail.present? ? detail.end_time.strftime("%Y-%m-%d %H:%M:%S") : '' #完成时间
    json.note detail.present? ? detail.note : '' #说明
    json.handle_man detail.present? ? detail.handle_man : '' #处理人
    json.handle_status detail.present? ? detail.handle_status : '' #处理状态
    json.without_time_flag detail.present? ? detail.without_time_flag : '' #超时状态
end