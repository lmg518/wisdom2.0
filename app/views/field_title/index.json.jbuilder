json.field_titles @field_titles do |t|
    json.id t.id        #id
    json.name t.name    #名称
    json.code t.code    #编码
end