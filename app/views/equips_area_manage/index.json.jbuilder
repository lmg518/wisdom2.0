json.s_areas @s_areas do |e|

  region_code = SRegionCode.find_by(:id => e.s_region_code_id)
  s_area = SArea.find_by(:id => e.s_area_id)


  #s_equips = s_area.s_equips    #区域下的设备
  s_equips = SEquip.where(:s_region_code_id => e.s_region_code_id, :s_area_id => e.s_area_id)


  json.region_name region_code.present? ?region_code.region_name : ''  #车间名称
  json.region_id region_code.present? ?region_code.id : ''             #车间id
  json.area_name s_area.present? ? s_area.name : ''                    #区域名称
  json.area_id s_area.present? ? s_area.id : ''                        #区域id

  equip_name = ""
  if s_equips.present?
    s_equips.each do |s|
      equip_name += "#{s.equip_name},"
    end
  end

  json.equip_name  equip_name   #区域下的设备名称


end
json.list_size @s_areas.present? ? @s_areas.total_count : 0