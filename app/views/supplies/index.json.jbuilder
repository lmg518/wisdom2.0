json.supplies @supplies.each do |suppli|
  json.id suppli.id
  json.unit_name suppli.unit_name
  json.s_region_code_info_id suppli.s_region_code_info_id
  json.city_name suppli.city_name
  json.s_administrative_area_id suppli.s_administrative_area_id
  json.brand suppli.brand
  json.supply_name suppli.supply_name
  json.supply_no suppli.supply_no
  json.supply_num suppli.supply_num
  json.supply_unit suppli.supply_unit
  json.validity_date suppli.present? ? suppli.validity_date.strftime("%Y-%m-%d") : ''
  json.owner suppli.owner
  json.d_login_msg_id suppli.d_login_msg_id
  json.created_at suppli.present? ? suppli.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
end
json.brands SBrandMsg.pluck(:brand)

#json.province SAdministrativeArea.all_provinces  #查不出来

@provinces=SAdministrativeArea.select{|x| x["zone_name"] != "商丘市"}
json.province @provinces do |p|
    json.id p.id
    json.zone_name p.zone_name
end

json.list_size @supplies.present? ? @supplies.total_count : 0