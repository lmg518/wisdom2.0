json.extract! s_area_login, :id, :s_administrative_area_id, :d_station_id, :d_login_msg_id, :created_at, :updated_at
json.url s_area_login_url(s_area_login, format: :json)
