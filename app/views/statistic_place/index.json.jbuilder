codeInfos=SRegionCodeInfo.by_id(params[:s_region_code_info_id])   #查询运维单位
# code_info_ids=[];
# codeInfos.each do |s|
#   code_info_ids << s.id
# end

code_info_ids=codeInfos.pluck(:id)

  @handle_mans=DLoginMsg.where(:s_region_code_info_id => code_info_ids)  #根据运维单位找到所有的运维人员
                        .by_login_name(params[:name])  #按人员查询
                        .page(params[:page]).per(params[:per])
  
  json.handle_mans @handle_mans do |h|
    
 #根据运维人员找运维单位
 unit_code=SRegionCodeInfo.find_by(:id => h.s_region_code_info_id)
 
 json.unit_name unit_code.present? ? unit_code.unit_name : ''

 #json.unit_name SRegionCodeInfo.find(h.s_region_code_info_id).present? ? SRegionCodeInfo.find(h.s_region_code_info_id).unit_name : ''

  json.id h.id  
  json.handle_man h.login_name  #运维人名
  json.telphone h.telphone


  #上月下月查询
  polling_plan_time=params[:month]  #获取前台的月份  N 本月   L  上月
  if polling_plan_time.present? && polling_plan_time == 'N'
    begin_time=Time.new.beginning_of_month
    end_time=Time.new.end_of_month
  elsif polling_plan_time.present? && polling_plan_time == 'L'

    time=Time.now + (-1.month)
    begin_time=time.beginning_of_month
    end_time=time.end_of_month

    Rails.logger.info "---111-#{begin_time}------"
  end



  @task_forms=DTaskForm.by_polling_time(begin_time, end_time).where(:handle_man => h.login_name,:fault_type => ["polling_form","abnormal_form"])
  
  
  @station_num = @task_forms.length
  json.task_form_num  @station_num  #运维人员的工单数数  只统计巡检单和异常巡检单
  
  @task_form_ok=DTaskForm.by_polling_time(begin_time, end_time).where(:job_status => 'audit',:handle_man => h.login_name,:fault_type => ["polling_form","abnormal_form"]).length
  json.task_form_ok @task_form_ok   #运维人员已完成的工单数数 只统计巡检单和异常巡检单

  @hand_station_num=SAreaLogin.where(:d_login_msg_id => h.id)
  json.hand_station_num @hand_station_num.length  #运维人员属于的站点数

  end 

  #分页
  json.list_size @handle_mans.present? ? @handle_mans.total_count : 0

  Rails.logger.info "-----分页-----#{@handle_mans.total_count}---------"



# json.handle_mans @handle_mans do |h|
#   json.id h.id  
#   json.handle_man h.login_name  #运维人名
#   json.telphone h.telphone
#   json.unit_name @unit_name  
#   @task_forms=DTaskForm.where(:handle_man => h.login_name,:fault_type => ["polling_form","abnormal_form"])
#   @station_num = @task_forms.length
#   json.task_form_num  @station_num  #运维人员的工单数数  只统计巡检单和异常巡检单
#   @task_form_ok=DTaskForm.where(:job_status => 'audit',:handle_man => h.login_name,:fault_type => ["polling_form","abnormal_form"]).length
#   json.task_form_ok @task_form_ok   #运维人员已完成的工单数数 只统计巡检单和异常巡检单

#   @hand_station_num=SAreaLogin.where(:d_login_msg_id => h.id)
#   json.hand_station_num @hand_station_num.length  #运维人员属于的站点数
# end
