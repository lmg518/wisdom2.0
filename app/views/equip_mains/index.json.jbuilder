json.faults @faultJob do |job|
    region = job.s_region_code_info
    station = job.d_station
    
    json.id job.id #序号
    json.region_name region.present? ? region.unit_name : '' #运维单位
    json.station_location station.present? ? station.region_code : '' #站点城市
    json.station_name job.station_name #站点名称
    json.device_name job.device_name #设备名称
    json.job_no job.job_no #工单号
    json.fault_type job.fault_type_str #工单类型
    json.job_status job.job_status_str #工单状态
    json.create_type job.create_type_str #创建类型 (计划外plan_out ：；计划内：plan_in)
    json.author job.author #创建人
    json.created_at job.created_at.strftime("%Y-%m-%d %H:%M:%S") #创建时间
    json.urgent_level job.urgent_level_str #紧急程度 (I，II， III)
    json.title job.title          #工单标题
    json.handle_man job.handle_man #当前处理人
    json.handle  job.d_fault_job_details #日志信息
    json.send_type job.send_type_str #发送类型 (app, sms, email)
end
json.login_name @current_user.login_name.present? ? @current_user.login_name : '' #登陆人     
json.role_name @taskName.present? ? @taskName : '' #角色权限
json.list_size @faultJob.present? ? @faultJob.total_count : 0