json.alarm_notices @alarm_notices do |notice|
  json.id notice.id
  json.notice_accept notice.notice_accept
  json.notice_content notice.notice_content
  json.alarm notice.d_alarm.alarm_rule
  json.alarm_level notice.alarm_level
  json.notice_login notice.notice_login
  json.status notice.status_to_str
end
json.list_size @alarm_notices.present? ? @alarm_notices.total_count : 0