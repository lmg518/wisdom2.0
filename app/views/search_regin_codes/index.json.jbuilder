json.region_codes @region_codes do |code|
  json.id code.id
  json.name code.region_name
  json.chil code.get_son_region
end