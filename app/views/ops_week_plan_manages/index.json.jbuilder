@ops_plan = DOpsPlanManage.s_by_week(params[:begin_time])
                          .by_ops_info(params[:info_id])
                          .by_edit_status(params[:edit_status])
                          .by_zone_name(params[:zone_name])
                          .order(:created_at => "desc")
@ops_plan = @ops_plan.page(params[:page]).per(params[:per])                                  

json.ops_plan @ops_plan.each do |ops|
    @details = ops.d_ops_plan_details
    
    json.ops {
        json.id ops.id
        json.d_station_id ops.d_station_id
        json.station_name ops.station_name.present? ? ops.station_name : ''
        json.zone_name ops.zone_name
        json.unit_name ops.unit_name
        json.edit_status ops.edit_status.present? ? ops.edit_status : ''
        json.week_begin_time ops.week_begin_time
        json.week_end_time ops.week_end_time
        json.week_flag ops.week_flag       
    }
    json.details @details.each do |detail|
        json.id detail.id
        json.job_name detail.job_name
        json.job_flag detail.job_flag
        json.d_work_pro_id detail.d_work_pro_id
        json.job_time detail.job_time.present? ? detail.job_time.strftime("%m-%d") : ''
        json.created_at detail.created_at.present? ? detail.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
    end
end
json.list_size @ops_plan.present? ? @ops_plan.total_count : 0
