json.hour_datas @hour_datas do |hour|
    json.id hour.id
    json.data_time Time.parse(hour.data_time).strftime("%Y-%m-%d")
    json.station_name hour.station_name
    json.station_id hour.d_station_id
    json.standard_num hour.hour_standard_num
    json.hour_num hour.hour_num
    if hour.hour_num < hour.hour_standard_num
        @status='NG'
    else
        @status='OK'
    end
    json.status @status
end
json.list_size @hour_datas.present? ? @hour_datas.total_count : 0