if @min_datas.size>0
    @hours=["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"]
    json.station_name @min_datas[0].station_name
    json.station_id @min_datas[0].d_station_id

    json.hours @hours do |h|
        json.hour h
        json.min_datas @min_datas do |min|

                if Time.parse(min.data_time).strftime("%H") == h
                    json.data_time Time.parse(min.data_time).strftime("%H")
                    json.standard_num min.hour_standard_num
                    json.hour_num min.hour_num
                    if min.hour_num < min.hour_standard_num
                        @status='NG'
                    else
                        @status='OK'
                    end
                    json.status @status
                end
                
        end
    end
end

