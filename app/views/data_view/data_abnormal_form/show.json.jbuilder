json.task_from {
    json.id @d_task_form.id
    json.job_no @d_task_form.job_no #工单号
    json.station_name @d_task_form.station_name #站点
    json.job_status @d_task_form.job_status_str #工单状态
    json.fault_type @d_task_form.fault_type_str #工单类型
    json.created_at @d_task_form.created_at.strftime("%Y-%m-%d %H:%M:%S") #生成时间
    json.title @d_task_form.title #标题
    json.clraring_if @d_task_form.clraring_if == 'N'? "否" : "是"  #是否补录
    json.img_one @d_task_form.img_one     #图片
    json.urgent_level @d_task_form.urgent_level_str   #紧急程度
    json.audit_man @d_task_form.audit_man #审核人
    json.audit_time @d_task_form.audit_time #审核时间
    
    #日志表
    json.details @details do |details|
        json.job_status details.present? ? details.job_status_str : '' #步骤
        json.handle_man details.handle_man #处理人
        json.begin_time details.present? ? details.begin_time.strftime("%Y-%m-%d %H:%M:%S") : '' #开始时间
        json.end_time details.present? ? details.end_time.strftime("%Y-%m-%d %H:%M:%S") : '' #完成时间
        json.note details.note #说明
        json.handle_status details.handle_status #处理状态
        json.without_time_flag details.without_time_flag #超时状态
    end
    # json.login_name @current_user.login_name
    # json.s_region_code_info_id  @d_task_form.s_region_code_info_id  #单位ID

    #流程图数据
    flow=['创建工单','工单待处理','工单待审核','工单已审核']
    json.flow flow
}


