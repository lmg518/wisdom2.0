if params[:job_type] == "polling_form"
  @works = DWorkPro.all
  json.weak_jobs @works.select {|x| x.work_type == 7}
  json.month_jobs @works.select {|x| x.work_type == 30}
  json.ji_jobs @works.select {|x| x.work_type == 90}
  json.hal_year_jobs @works.select {|x| x.work_type == 360}
end
#分页显示数据  按创建时间降序
@works = DWorkPro.order(:created_at => :desc).page(params[:page]).per(params[:per])
#按条件查询
@work_name = params[:work_name]
@work_type = params[:work_type] # 7  30   90  360
@work_flag = params[:work_flag] # Y 启用   N 禁用
@works = DWorkPro.by_work_name(@work_name).by_work_type(@work_type).by_work_flag(@work_flag).order(:created_at => :desc).page(params[:page]).per(10)
json.works @works do |wk|
  json.id wk.id #id
  json.work_name wk.work_name #项目名
  json.work_code wk.work_code #项目编码
  json.work_flag wk.work_flag #项目标识
  #json.work_type wk.work_type  #项目类型
  json.work_type wk.work_type_str
end
#设置分页数据
json.list_size @works.present? ? @works.total_count : 0

