json.equipment_info {
    json.id @equipment_info.id
    json.equip_type @equipment_info.equip_type
    json.equip_brands @equipment_info.equip_brands
    json.equip_nums @equipment_info.equip_nums
    json.equip_code @equipment_info.equip_code
    json.types @equipment_info.types
    json.purchase_time @equipment_info.purchase_time.present? ? @equipment_info.purchase_time.strftime("%Y-%m-%d") : ''
    json.add_man @equipment_info.add_man
}