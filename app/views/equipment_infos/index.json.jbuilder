json.equipment_infos @equipment_infos.each do |e|

  if e.d_station_id.present?
    @statin=DStation.find(e.d_station_id)
  else
    @statin = DStation.find_by(:id => 'a')
  end

  json.station_name @statin.present? ? @statin.station_name : ''  #站点
  json.region_code @statin.present? ? @statin.region_code : ''    #城市
  json.id e.id
  json.equip_type e.equip_type       #设备类型
  json.equip_brands e.equip_brands   #品牌
  json.equip_code e.equip_code       #设备型号
  json.equip_nums e.equip_nums       #设备编号 
  #json.stand_num e.stand_num         #设备编号
  json.types e.types_str             #设备类型  Y正式  N  备机
  json.purchase_time e.purchase_time.present? ? e.purchase_time.strftime("%Y-%m-%d %H:%M:%S") : '' #购置日期
  json.created_at e.created_at.strftime("%Y-%m-%d %H:%M:%S")  #添加日期
  json.add_man e.add_man   #添加人
end

json.brands SBrandMsg.pluck(:brand)   #品牌信息

json.list_size @equipment_infos.present? ? @equipment_infos.total_count : 0