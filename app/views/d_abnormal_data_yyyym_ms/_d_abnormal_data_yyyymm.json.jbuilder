json.extract! d_abnormal_data_yyyymm, :id, :abnormal_id, :station_id, :rule_instance, :create_acce, :ab_lable, :ab_data_time, :ab_value, :compare_value, :compare_value2, :compare_value3, :ab_desc, :created_at, :updated_at
json.url d_abnormal_data_yyyymm_url(d_abnormal_data_yyyymm, format: :json)
