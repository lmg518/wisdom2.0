json.id @form_module.id       #模板id
json.name @form_module.name   #模板名

json.field_titles @field_titles do |f| 
    json.id f.id        #标题id
    json.code f.code    #标题编号
    json.name f.name    #标题名
end