json.extract! d_task_form, :id, :created_at, :updated_at
json.url d_task_form_url(d_task_form, format: :json)
