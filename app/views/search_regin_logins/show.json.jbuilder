json.region_code  @region_code do |region|
  json.id region.id
  json.region_code region.region_code
  json.region_name region.region_name
  json.chil region.d_login_msgs.where(:valid_flag => '1') do |login|
    json.id login.id
    json.login_name login.login_name
    json.flag login.s_area_logins.where(:d_station_id => @station.id).present? ? "true" : "false"
  end
end
