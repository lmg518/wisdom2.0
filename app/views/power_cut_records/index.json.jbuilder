#json.powerCutRecords @powerCutRecords.present? ? @powerCutRecords : []

#添加封装的状态 json信息   查询条件使用
json.status DPowerCutRecord.status
#添加 封装的来源 json信息  查询条件使用
json.origins DPowerCutRecord.origins

json.abnormals DPowerCutRecord.abnormals   #新增 中异常数据

json.fallbacks DPowerCutRecord.fallbacks   #回退原因



 #设置分页数据
json.list_size @powerCutRecords.present? ? @powerCutRecords.total_count : 0
#人员身份,权限
json.login_name @current_user.login_name 
json.role_name @current_user.s_role_msg.role_name

#转换日期格式
json.powerCutRecords @powerCutRecords do |p|

        json.id  p.id
    
        json.station_name  p.station_name
        json.unit_yw  p.unit_yw

        json.begin_time p.begin_time.strftime("%Y/%m/%d %H:%M:%S") if p.begin_time.present?
        json.end_time p.end_time.strftime("%Y/%m/%d %H:%M:%S") if p.end_time.present?
        json.time_range  p.time_range
        json.created_at  p.created_at.strftime("%Y/%m/%d %H:%M:%S")
        json.exception_name  p.exception_name
        json.origin  p.origin
        json.handle_man  p.handle_man
        json.audit_des  p.audit_des
        json.job_status  p.job_status_str
        json.img_path  p.img_path
    
    end