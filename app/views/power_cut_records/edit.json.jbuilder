#编辑页面的数据
json.powerCutRecord {
    json.id  @powerCutRecord.id
    json.station_name  @powerCutRecord.station_name
    json.d_station_id @powerCutRecord.d_station_id
    json.unit_yw  @powerCutRecord.unit_yw
    json.begin_time @powerCutRecord.begin_time.strftime("%Y/%m/%d %H:%M:%S") if @powerCutRecord.begin_time.present?
    json.end_time @powerCutRecord.end_time.strftime("%Y/%m/%d %H:%M:%S") if @powerCutRecord.end_time.present?
    json.time_range  @powerCutRecord.time_range
    json.created_at  @powerCutRecord.created_at.strftime("%Y/%m/%d %H:%M:%S")
    json.exception_name  @powerCutRecord.exception_name
    json.origin  @powerCutRecord.origin
    json.handle_man  @powerCutRecord.handle_man
    json.audit_des  @powerCutRecord.audit_des
    json.job_status  @powerCutRecord.job_status
    json.img_path  @powerCutRecord.img_path
}

json.origins DPowerCutRecord.origins       #封装的来源 json信息  
json.abnormals DPowerCutRecord.abnormals   #新增 中异常数据

