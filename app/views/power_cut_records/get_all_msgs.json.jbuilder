json.s_region_code_infos @s_region_code_infos do |s|
    json.id s.id
    json.unit_name s.unit_name

    #查询该运维单位下的所有运维人员
    @login_msgs=s.d_login_msgs
    json.login_msgs @login_msgs do |w|
        # json.id DLoginMsg.find_by(:s_region_code_info_id => s.id).present? ? DLoginMsg.find_by(:s_region_code_info_id => s.id).id : ''
        # json.name DLoginMsg.find_by(:s_region_code_info_id => s.id).present? ? DLoginMsg.find_by(:s_region_code_info_id => s.id).login_no : ''
        json.id w.id
        json.name w.login_name
    end

end