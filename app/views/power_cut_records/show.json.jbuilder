json.powerCutRecord {
        json.id  @powerCutRecord.id
        json.station_name  @powerCutRecord.station_name
        json.unit_yw  @powerCutRecord.unit_yw
        json.begin_time @powerCutRecord.begin_time.strftime("%Y/%m/%d %H:%M:%S") if @powerCutRecord.begin_time.present?
        json.end_time @powerCutRecord.end_time.strftime("%Y/%m/%d %H:%M:%S") if @powerCutRecord.end_time.present?
        json.time_range  @powerCutRecord.time_range
        json.created_at  @powerCutRecord.created_at.strftime("%Y/%m/%d %H:%M:%S")
        json.exception_name  @powerCutRecord.exception_name

        json.exception_device_type  @powerCutRecord.exception_device_type  #异常设备类型

        json.origin  @powerCutRecord.origin
        json.handle_man  @powerCutRecord.handle_man
        json.audit_des  @powerCutRecord.audit_des
        json.job_status  @powerCutRecord.job_status
        json.img_path  @powerCutRecord.img_path
}

#审核不通过日志信息
json.power_cut_audit_logs @power_cut_audit_logs do |log|
        json.id log.id
        json.audit_cause log.audit_cause
        json.note log.note
        #json.d_power_cut_record_id log.d_power_cut_record_id
        json.created_at log.created_at.strftime("%Y-%m-%d %H:%M:%S")
        json.updated_at log.updated_at.strftime("%Y-%m-%d %H:%M:%S")
end

json.fallbacks DPowerCutRecord.fallbacks   #回退原因




    