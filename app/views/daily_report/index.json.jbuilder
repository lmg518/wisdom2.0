#封装车间信息
json.region_code @region_code

#车间项目信息
json.plants @plants do |p|
    json.id p.id
    json.name p.name

    #查询关系类型
    @relation_id = DMaterialReginCode.find_by(:s_material_id => p.id)
    @d_relation_type = DRelationType.find_by(:id => @relation_id.d_relation_type_id)
    json.relation_type @d_relation_type.present? ? @d_relation_type.name : ''  #消耗/产出


    daily_report = @daily_report.find_by(:s_material_id => p.id)

    json.nums daily_report.present? ? daily_report.nums : ''   #当天的日报数据
    json.daily_yield daily_report.present? ? daily_report.daily_yield : ''   #日收率
end

#日报状态
reports = @daily_report.where(:status => 'Y') if @daily_report.present?
json.status reports.present? && reports.length >0 ? '已上报' : '未上报' 
