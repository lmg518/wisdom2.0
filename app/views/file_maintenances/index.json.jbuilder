json.files @file_maintenancees do |file|
  json.id file.id
  json.title file.title
  json.file_category_id file.file_category_id
  json.file_category_name file.file_category.category_name
  json.author file.author.present? ? file.author : ""
  json.created_at file.created_at.strftime("%Y-%m-%d %H:%M:%S")
end
json.list_size @file_maintenancees.present? ? @file_maintenancees.total_count : 0
