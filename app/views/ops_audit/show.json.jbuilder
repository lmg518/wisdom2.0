#单个工单信息
json.ops_audit {
    json.id @ops_audit.id

    json.station_name @ops_audit.station_name   #站点名称
    json.job_no @ops_audit.job_no                               #工单号
    json.fault_type @ops_audit.fault_type_str                   #工单类型 （转换了）
    json.title @ops_audit.title                                 #工单标题 
    json.created_at @ops_audit.created_at.strftime("%Y-%m-%d %H:%M:%S") #创建时间
    json.job_status @ops_audit.job_status_str                  #工单状态
    json.clraring_if @ops_audit.clraring_if                    #是否补录

    json.audit_man @ops_audit.audit_man             #审核人
    json.audit_if @ops_audit.audit_if               #审核信息
    json.audit_time @ops_audit.audit_time.strftime("%Y-%m-%d %H:%M:%S") if @ops_audit.audit_time.present?   #审核时间
    json.audit_des @ops_audit.audit_des if @ops_audit.audit_des.present?  #审核不通过原因
    
    json.handle_man @ops_audit.handle_man                      #当前处理人

    #复核内容信息
    json.review_man @ops_audit.review_man      #复核人
    json.review_time @ops_audit.review_time.strftime("%Y-%m-%d %H:%M:%S") if @ops_audit.review_time.present?    #复核时间
    json.review_if @ops_audit.review_if        #复核内容   Y 通过 N 不通过
    json.review_des @ops_audit.review_des if @ops_audit.review_des.present?     #复核不通过原因

}

#处理表
@handle = @ops_audit.d_fault_handles
json.polling_job_type @handle do |handle| 
    json.id handle.id
    json.img_one handle.img_one
    json.handle_type handle.present? ? handle.handle_type : '' #处理方式

    @work_pro = DWorkPro.find_by(:work_name => handle.handle_type)
    #json.d_work_pro_id @work_pro.present? ? @work_pro.id : ''     #工作项id
    json.work_code @work_pro.present? ? @work_pro.work_code : ''  #工作项编码
end


#封装的日志信息
json.d_fault_job_details @d_fault_job_details do |d|
    json.id d.id
    json.job_status d.job_status_str        #工单状态（步骤需要转换）

    json.handle_man d.handle_man
    json.begin_time d.begin_time.strftime("%Y/%m/%d %H:%M:%S") if d.begin_time.present?
    json.end_time d.end_time.strftime("%Y/%m/%d %H:%M:%S") if d.end_time.present?
    json.note d.note
    json.handle_status d.handle_status
    json.without_time_flag d.without_time_flag
end

#流程图数据
flow=['创建工单','工单待处理','工单处理中','工单待复核','工单待审核','工单已审核']
json.flow flow