#所有工单信息
json.ops_audits @ops_audits do |ops|

    station = ops.d_station          #关联查询 站点城市
    region = ops.s_region_code_info  #关联查询 运维单位表

    json.unit_name region.present? ? region.unit_name : ''          #运维单位
                                                                        #省  ??
    json.station_location station.present? ? station.station_location : '' #站点城市

    json.id ops.id
    json.station_name ops.station_name                   #站点名称
    json.polling_ops_type ops.polling_ops_type_str       #运维类型（任务周期  7 30 90 360）
    json.job_no ops.job_no                               #工单号
    json.fault_type ops.fault_type_str                   #工单类型 （转换了）
    json.urgent_level ops.urgent_level_str               #紧急程度  (I，II， III)
    json.author ops.author                               #创建人
    json.title ops.title                                 #工单标题     
    json.created_at ops.created_at.strftime("%Y-%m-%d %H:%M:%S") #创建时间
    json.job_status ops.job_status_str                  #工单状态
    json.create_type ops.create_type_str                #创建类型 (计划外plan_out ：；计划内：plan_in)
                                                        #当前节点 ??
                                                        #任务状态 ??
    json.handle_man ops.handle_man                      #当前处理人
    json.send_type ops.send_type_str                    #发送类型 (app, sms, email)

end


#权限验证
json.login_name @current_user.login_name 
json.role_name @current_user.s_role_msg.role_name


#分页
json.list_size @ops_audits.present? ? @ops_audits.total_count : 0
