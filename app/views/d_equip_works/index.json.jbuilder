json.equip_works @equip_works do |e|
  # #加一个部门字段，下发时，需要用到
  # region_code_info = SRegionCodeInfo.find_by(:s_region_code_id => e.s_region_code_id)
  # json.unit_name region_code_info.present? ? region_code_info.unit_name : ''
  # json.region_id region_code_info.present? ? region_code_info.id : ''

  colour = DEquipWork.handle_remind(e)
  #json.region_id e.s_region_code_info_id   #部门id

  json.id e.id
  #json.s_region_code_id e.s_region_code_id
  json.region_id e.s_region_code_id   #车间id

  json.region_name e.region_name
  json.s_equip_id e.s_equip_id

  s_area = SArea.find_by(:id => e.s_area_id)  #查询区域
  area_name = s_area.present? ? s_area.name : ''
  area_id = s_area.present? ? s_area.id : ''
  json.area_name area_name  #区域名称
  json.area_id area_id      #区域id

  json.equip_name e.equip_name
  json.equip_code e.equip_code
  json.work_person e.work_person
  json.work_start_time e.work_start_time ? e.work_start_time.strftime("%Y-%m-%d %H:%M:%S") : ''
  json.work_end_time e.work_end_time ? e.work_end_time.strftime("%Y-%m-%d %H:%M:%S") : ''
  json.work_status DEquipWork.set_work_status(e.work_status)
  json.work_desc e.work_desc
  json.handle_time e.handle_time ? e.handle_time.strftime("%Y-%m-%d %H:%M:%S") : ''
  json.founder e.founder
  json.colour colour ? colour : ''
end

json.equip_works_size @equip_works ? @equip_works.total_count : 0
#人员身份,权限
json.login_name @current_user.login_name 
json.role_name @current_user.s_role_msg.role_name
json.role_code @current_user.s_role_msg.role_code