json.alarms @alarms do |alarm|
  json.id alarm.id
  json.alarm_rule alarm.alarm_rule
  json.station_id alarm.station_id
  json.station_name alarm.d_station.station_name
  json.alarm_level alarm.alarm_level_to_s
  json.first_alarm_time alarm.first_alarm_time.present? ? alarm.first_alarm_time.strftime("%Y-%m-%d %H:%M:%S") : ''
  json.last_alarm_time alarm.last_alarm_time.present? ? alarm.last_alarm_time.strftime("%Y-%m-%d %H:%M:%S") : ''
  json.continuous_alarm_times alarm.continuous_alarm_times
  json.status alarm.status_to_str

end
json.list_size @alarms.present? ? @alarms.total_count : 0  if @params_status
