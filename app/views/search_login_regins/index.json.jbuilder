json.region_code @regin_codes do |code|
  json.id code.id
  json.region_name code.level_name
  json.chil code.s_region_codes.map{|x| {id: x.id,region_name: x.region_name, flag: x.id == @group.to_i ? 'checked' : 'false'}}
end