json.five_datas @data_yyyy_mms do |mm|
  json.id mm.id
  json.station_id mm.station_id
  json.station_name DStation.find(mm.station_id).station_name
  json.data_item SAbnormalLtem.find(mm.item_code).item_name
  json.data_cycle mm.data_cycle_to_s
  json.data_value mm.data_value
  json.data_label mm.data_label
  json.data_label_des mm.data_label_to_s
  json.created_at mm.data_time.strftime("%Y-%m-%d %H:%M:%S")
end
json.list_size @data_yyyy_mms.present? ? @data_yyyy_mms.total_count : 0