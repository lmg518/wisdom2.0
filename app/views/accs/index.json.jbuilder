json.accs @accs.each do |acc|
  json.id acc.id
  json.unit_name acc.unit_name
  json.s_region_code_info_id acc.s_region_code_info_id
  json.city_name acc.city_name
  json.s_administrative_area_id acc.s_administrative_area_id
  json.brand acc.brand
  json.supply_name acc.supply_name
  json.supply_no acc.supply_no
  json.supply_num acc.supply_num
  json.supply_unit acc.supply_unit
  json.validity_date acc.present? ? acc.validity_date.strftime("%Y-%m-%d") : ''
  json.owner acc.owner
  json.d_login_msg_id acc.d_login_msg_id
  json.created_at acc.present? ? acc.created_at.strftime("%Y-%m-%d %H:%M:%S") : ''
end

json.brands SBrandMsg.pluck(:brand)

#json.province SAdministrativeArea.all_provinces  查不出来
@provinces=SAdministrativeArea.select{|x| x["zone_name"] != "商丘市"}
json.province @provinces do |p|
    json.id p.id
    json.zone_name p.zone_name
end

json.list_size @accs.present? ? @accs.total_count : 0

