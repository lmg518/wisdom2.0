#所有登录人员
json.localizes @localizes do |l|
    json.login_name l.login_name         #登录人
    json.get_time l.get_time.present? ? l.get_time.strftime("%Y-%m-%d %H:%M:%S") : ''  #到位时间
    json.long_login l.long_login         #经度
    json.lat_login l.lat_login           #纬度

   #关联到d_login_msgs表查询电话
    @d_login_msg=l.d_login_msg
    json.telphone @d_login_msg.present? ? @d_login_msg.telphone : ''  #电话
    json.unit_name @d_login_msg.present? && @d_login_msg.s_region_code_info.present? ? @d_login_msg.s_region_code_info.unit_name : '' #运维单位

end

#所有站点信息
json.stations @stations do |s|
    json.id s.id                      #站点id
    json.station_name s.station_name  #站点名
    json.type 'yw'                    #站点类型
    json.longitude s.longitude        #经度
    json.latitude s.latitude          #纬度
end