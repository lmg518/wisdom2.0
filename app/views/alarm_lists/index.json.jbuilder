json.alarm_lists @alarms do |alarm|
     json.id alarm.id
     json.station_id alarm.station_id
     json.station_name DStation.find(alarm.station_id).station_name
     json.alarm_rule alarm.alarm_rule
     json.alarm_level alarm.alarm_level
     json.first_alarm_time  alarm.first_alarm_time.present? ? alarm.first_alarm_time.strftime('%Y-%m-%d %H:%M:%S') : ""
     json.last_alarm_time alarm.last_alarm_time.present? ? alarm.last_alarm_time.strftime('%Y-%m-%d %H:%M:%S') : ""
     json.continuous_alarm_times alarm.continuous_alarm_times
     json.status alarm.status_to_str
end
json.list_size @alarms.present? ? @alarms.total_count : 0
json.all_infos @all_infos