class StationLostJob < ActiveJob::Base
#站点离线
  def perform
    Resque.enqueue(StationLostWorker, "station_lost_crono")
  end
end