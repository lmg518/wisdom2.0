class StationItemAvgJob < ActiveJob::Base
  #站点 天 小时平均值
  #<StationItemAvg id: nil, d_station_id: nil, s02_avg: nil,
  # co_avg: nil, co2_avg: nil, o3_avg: nil,
  # pm10_avg: nil, pm2_5_avg: nil,
  # created_at: nil, updated_at: nil>

  def perform
    Resque.enqueue(StationItemAvgWoker, "station_item_avg_crono")
  end

end