class AqiDayJob < ActiveJob::Base

  def perform
    Resque.enqueue(AqiDayWorker, 'aqi_day_corno')
  end

end