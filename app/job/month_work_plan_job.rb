class MonthWorkPlanJob < ActiveJob::Base
  queue_as :default

  def perform(month,*args)
     @stations = DStation.active.uniq
     @stations.each do |station|
      @time = Time.now + month.to_i.month      #创建开始的月份  + 1.month
      @month_end_day = @time.end_of_month
      @month = @time.month
      @administrative_area = station.s_administrative_area
      @unit = station.s_region_code_info
      10.times do |i|
        t_fitst_day = @time.beginning_of_month + i.week


        w_month_end_day = (@time + (-1.month)).end_of_month.wday  #判断上个月月的最后一天是周几
        if w_month_end_day == 0  #如果是周日 从当前周开始
          #Rails.logger.info "-----1---------"
          week_first_day = t_fitst_day.beginning_of_week
          week_end_day = t_fitst_day.end_of_week
        else
          #Rails.logger.info "-----2---------"
          week_first_day = t_fitst_day.beginning_of_week + 1.week
          week_end_day = t_fitst_day.end_of_week + 1.week
        end

       #week_first_day = t_fitst_day.beginning_of_week 
       #week_end_day = t_fitst_day.end_of_week          

      # week_first_day = t_fitst_day.beginning_of_week + 1.week  #从下周开始创建  2018-1 不行
      # week_end_day = t_fitst_day.end_of_week + 1.week          #从下周开始创建
 

        if week_end_day >= @month_end_day && week_first_day < @month_end_day
          station.d_ops_plan_manages.create(
                                        station_name: station.station_name,
                                        s_administrative_area_id: @administrative_area.id,
                                        zone_name: @administrative_area.zone_name,
                                        s_region_code_info_id: @unit.present? ? @unit.id : 0,
                                        unit_name: @unit.present? ? @unit.unit_name : '',
                                        edit_status: "N",
                                        week_begin_time: week_first_day,
                                        week_end_time: week_end_day,
                                        month_flag: @month,
                                        week_flag: i + 1,
          )if week_first_day <= @month_end_day
          # @week_arr << {week_flag: "#{@month}月第#{i + 1}周",
          #               week_rand: "#{week_first_day.strftime("%d")}日至#{week_end_day.strftime("%d")}日",
          #               station_count: @station_count,
          #               station_handle_ok: handle_station.length,
          #               station_handle_un: @station_count - handle_station.length,

          # } if week_first_day <= @month_end_day
          break
        else
          station.d_ops_plan_manages.find_or_create_by(
              station_name: station.station_name,
              s_administrative_area_id: @administrative_area.id,
              zone_name: @administrative_area.zone_name,
              s_region_code_info_id: @unit.present? ? @unit.id : 0,
              unit_name: @unit.present? ? @unit.unit_name : '',
              edit_status: "N",
              week_begin_time: week_first_day,
              week_end_time: week_end_day,
              month_flag: @month,
              week_flag: i + 1,
          )
        end
      end
     end
  end



#根据 月份 站点 生成
  def perform2(month,stations)

    stations.each do |station|

     @time = Time.now + month.to_i.month      #创建开始的月份  + 1.month
     @month_end_day = @time.end_of_month
     @month = @time.month
     @administrative_area = station.s_administrative_area
     @unit = station.s_region_code_info
     10.times do |i|
       t_fitst_day = @time.beginning_of_month + i.week

       w_month_end_day = (@time + (-1.month)).end_of_month.wday  #判断上个月月的最后一天是周几
       if w_month_end_day == 0  #如果是周日 从当前周开始
         week_first_day = t_fitst_day.beginning_of_week
         week_end_day = t_fitst_day.end_of_week
       else
         week_first_day = t_fitst_day.beginning_of_week + 1.week
         week_end_day = t_fitst_day.end_of_week + 1.week
       end

      #week_first_day = t_fitst_day.beginning_of_week 
      #week_end_day = t_fitst_day.end_of_week          

     # week_first_day = t_fitst_day.beginning_of_week + 1.week  #从下周开始创建  2018-1 不行
     # week_end_day = t_fitst_day.end_of_week + 1.week          #从下周开始创建


       if week_end_day >= @month_end_day && week_first_day < @month_end_day
         station.d_ops_plan_manages.create(
                                       station_name: station.station_name,
                                       s_administrative_area_id: @administrative_area.id,
                                       zone_name: @administrative_area.zone_name,
                                       s_region_code_info_id: @unit.present? ? @unit.id : 0,
                                       unit_name: @unit.present? ? @unit.unit_name : '',
                                       edit_status: "N",
                                       week_begin_time: week_first_day,
                                       week_end_time: week_end_day,
                                       month_flag: @month,
                                       week_flag: i + 1,
         )if week_first_day <= @month_end_day
         # @week_arr << {week_flag: "#{@month}月第#{i + 1}周",
         #               week_rand: "#{week_first_day.strftime("%d")}日至#{week_end_day.strftime("%d")}日",
         #               station_count: @station_count,
         #               station_handle_ok: handle_station.length,
         #               station_handle_un: @station_count - handle_station.length,

         # } if week_first_day <= @month_end_day
         break
       else
         station.d_ops_plan_manages.find_or_create_by(
             station_name: station.station_name,
             s_administrative_area_id: @administrative_area.id,
             zone_name: @administrative_area.zone_name,
             s_region_code_info_id: @unit.present? ? @unit.id : 0,
             unit_name: @unit.present? ? @unit.unit_name : '',
             edit_status: "N",
             week_begin_time: week_first_day,
             week_end_time: week_end_day,
             month_flag: @month,
             week_flag: i + 1,
         )
       end
     end
    end
 end






end