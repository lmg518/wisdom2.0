class DayHourAverageJob < ActiveJob::Base

  # PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
  def perform
    Resque.enqueue(DayHourAverageWorker, "day_hour_average_crono")
  end

end