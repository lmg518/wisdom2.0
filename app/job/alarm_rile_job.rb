class AlarmRuleJob < ActiveJob::Base

  def perform
    Resque.enqueue(AlarmRuleWorker, "alarm_rule_crono")
  end

end