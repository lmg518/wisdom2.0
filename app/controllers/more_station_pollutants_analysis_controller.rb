class MoreStationPollutantsAnalysisController < ApplicationController
  before_action :current_user
  def index
    search_parmas
    handle_table
  end

  private
  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @station_id = @search[:station_ids]
      @data_times = @search[:data_times]
      @grading_analysis = @search[:grading_analysis]
    end
  end

  def handle_table
    @yy_h = []
    @region_leves = SRegionLevel.select(:id, :level_name)
    @so2_arr,@co_arr,@no2_arr,@o3_arr,@pm2_5_arr,@pm10_arr, = [],[],[],[],[],[],[],[]
    begin
      @stations = DStation.where(:id => @station_id)
      @stations.each do |station|
        next if station.blank?
        @so2,@co,@no2,@o3,@pm2_5,@pm10, = [],[],[],[],[],[],[]
        @hour_data = station.d_data_yyyymms.by_time_arr(@data_times)
        @hour_data.map{|x| @so2 << { val: x.data_value, data_time: x.data_time.strftime("%Y-%m-%d %H:%M:%S")} if x.item_code == 101 }
        @hour_data.map{|x| @co << { val: x.data_value, data_time: x.data_time.strftime("%Y-%m-%d %H:%M:%S")} if x.item_code == 105 }
        @hour_data.map{|x| @no2 << { val: x.data_value, data_time: x.data_time.strftime("%Y-%m-%d %H:%M:%S")} if x.item_code == 102 }
        @hour_data.map{|x| @o3 << { val: x.data_value, data_time: x.data_time.strftime("%Y-%m-%d %H:%M:%S")} if x.item_code == 106 }
        @hour_data.map{|x| @pm2_5 << { val: x.data_value, data_time: x.data_time.strftime("%Y-%m-%d %H:%M:%S")} if x.item_code == 107 }
        @hour_data.map{|x| @pm10 << { val: x.data_value, data_time: x.data_time.strftime("%Y-%m-%d %H:%M:%S")} if x.item_code == 108 }
        @so2_arr << {station_name: station.station_name, data:@so2}
        @co_arr << {station_name: station.station_name, data:@co}
        @no2_arr << {station_name: station.station_name, data:@no2}
        @o3_arr << {station_name: station.station_name, data:@o3}
        @pm2_5_arr << {station_name: station.station_name, data:@pm2_5}
        @pm10_arr << {station_name: station.station_name, data:@pm10}
      end
      @yy_h = { so2: @so2_arr, co: @co_arr,
                 no2: @no2_arr, o3: @o3_arr,
                 pm2_5: @pm2_5_arr, pm_10: @pm10_arr}
    # rescue Exception => e
    end

  end

end
