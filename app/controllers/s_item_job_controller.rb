class SItemJobController < ApplicationController

  def index
     @item_jobs =  SItemJob.return_job(params[:item])
     @item_jobs =   @item_jobs.present? ? @item_jobs.split(",") : []
     render json: {item_jobs: @item_jobs}
  end

end


