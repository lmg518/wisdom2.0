class StatisticPlaceController < ApplicationController
    #到位统计
    # GET /statistic_place.json
    def index
      # codeInfos=SRegionCodeInfo.all   #查询有的的运维单位
      # codeInfos.each do |s|
      #   Rails.logger.info "---#{s.id}----"
      #   @handle_mans=DLoginMsg.where(:s_region_code_info_id => s.id)  #根据运维单位找到所有的运维人员
      #   @unit_name=s.unit_name
      # end
    end

    # GET /statistic_place/1.json
    def show
      #上月下月查询
      polling_plan_time=params[:month]  #获取前台的月份  N 本月   L  上月
      if polling_plan_time.present? && polling_plan_time == 'N'
        begin_time=Time.new.beginning_of_month
        end_time=Time.new.end_of_month
      elsif polling_plan_time.present? && polling_plan_time == 'L'
        time=Time.now + (-1.month)
        begin_time=time.beginning_of_month
        end_time=time.end_of_month
        Rails.logger.info "---111-#{begin_time}------"
      end

     @d_task_forms=DTaskForm.by_polling_time(begin_time, end_time).by_fault_type(params[:polling_form])
                             .order(:created_at => "desc")
                             .where(:handle_man_id => params[:id])

     @d_task_forms = @d_task_forms.page(params[:page]).per(params[:per])
     
     @handle_man=DLoginMsg.find_by(:id => params[:id])
    end
   

end
