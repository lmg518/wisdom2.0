class OpsAuditManagesController < ApplicationController
  before_action :get_ops_pnal, only: [:show,:edit,:update,:destroy]
  before_action :authenticate_user # 转存公共参数 获取登录信息

    #运维计划审核
    def index
    end

    def month_days(month)
      month = month.to_i
      return 31 if [1, 3, 5, 7, 8, 10, 12].include? month
      return 29 if month == 2 && Date.julian_leap?(Time.now.year)
      return 28 if month ==2 && !Date.julian_leap?(Time.now.year)
      return 30
    end
    #查询的路由 ops_audit_manages/search_ops.json
    def search_ops
        @week_arr = []
        week_time = DateTime.parse(params[:week_time]) if !params[:week_time].blank?
        if !week_time.blank?
            @month_days = month_days(week_time.month)
            @month_end_day = week_time.end_of_month
            @month = week_time.month
            @stations = DStation.active.uniq.pluck(:id)
            @station_count = @stations.length  #总站点数
            handle_station = DOpsPlanManage.where(:d_station_id => @stations, :week_begin_time => week_time,:edit_status => "Y").pluck(:d_station_id).uniq
            #@ops_plans = DOpsPlanManage.where(:week_begin_time => week_time, :s_region_code_info_id => 17 ) if week_time.present?
            @week_arr << {#week_flag: "#{@month}月第#{@week}周",
                          #week_rand: "#{week_first_day.strftime("%d")}日至#{week_end_day.strftime("%d")}日",
                          station_count: @station_count,  #总站点数
                          station_handle_ok: handle_station.length,  #已做计划的站点数
                          #week_time: week_first_day.strftime("%Y-%m-%d %H:%M:%S"),
                          week_time: week_time,
                          unit_name: "河南鑫属",
                          unit_id: 17,                      #运维单位id
                          handle_station: handle_station,   #已做计划的站点id 
                          edit_status: handle_station.length!=0 ? "待审核" : "待分配",
            }
            render json: {week_plan_data: {currnt_week_plan_date: @week_arr}}
        else
          return render :index
        end

        
    end



    #点击站点后显示的json数据
    def show
      @week_pro = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "7", :job_checked => "Y")
      @month_pro = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "30", :job_checked => "Y")
      @jd_pro = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "90", :job_checked => "Y")
      @year_pro = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "360", :job_checked => "Y")
      #@week_pro = @work_pro.select{|x| x.job_flag == 7}
      #@month_pro = @work_pro.select{|x| x.job_flag == 30}
      #@jd_pro = @work_pro.select{|x| x.job_flag == 90}
      #@year_pro = @work_pro.select{|x| x.job_flag == 360}
    end

  
  #审核 ops_audit_manages/plan_audit.json
  def plan_audit
      plan_audit_params = params[:plan_audit_params]  #是一个数组
      @edit_if = params[:edit_status]                 #审核内容
      plan_audit_params.each do |param|
        week_time = param[1]['param']['week_time']
        d_station_id = param[1]['param']['handle_station']
        @ops_plans = DOpsPlanManage.where(d_station_id: d_station_id, week_begin_time: week_time)
        create_plan(@ops_plans,@edit_if) if @ops_plans.present? && @edit_if.present?
      end
      return render :index
  end

#多个审核时封装的方法
def create_plan(ops_plans,edit_if)
    #判断如果有多个站点时
    if ops_plans.length>1 
      ops_plans.each do |ops_plan|
      if edit_if == 'W' && ops_plan.edit_status=='Y'
        ops_plan.edit_status='W'  #状态修改为W
        ops_plan.save
        #根据运维类型创建不同的巡检工单 周 月 季度 年
        @plan_details_w=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plan.id,job_flag: "7")
        @plan_details_m=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plan.id,job_flag: "30")
        @plan_details_j=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plan.id,job_flag: "90")
        @plan_details_y=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plan.id,job_flag: "360")

        get_task_form(ops_plan,@plan_details_w) if @plan_details_w.present?  #创建周计划
        get_task_form(ops_plan,@plan_details_m) if @plan_details_m.present?  #创建月计划
        get_task_form(ops_plan,@plan_details_j) if @plan_details_j.present?  #创建季度计划
        get_task_form(ops_plan,@plan_details_y) if @plan_details_y.present?  #创建年计划
      else
        #return render json: {status: 'false'}
        Rails.logger.info "----多个站点-----"
        ops_plan.edit_status='O'       #审核不通过时 状态修改为O   
        ops_plan.save
      end
    end
    # 只有一个站点时
    else
    if edit_if == 'W' && ops_plans[0].edit_status=='Y'
      ops_plans[0].edit_status='W'  #状态修改为W
      ops_plans[0].save
      #根据运维类型创建不同的巡检工单 周 月 季度 年
      @plan_details_w=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plans[0].id,job_flag: "7")
      @plan_details_m=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plans[0].id,job_flag: "30")
      @plan_details_j=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plans[0].id,job_flag: "90")
      @plan_details_y=DOpsPlanDetail.where(job_checked: "Y",d_ops_plan_manage_id: ops_plans[0].id,job_flag: "360")

      get_task_form(ops_plans[0],@plan_details_w) if @plan_details_w.present?  #创建周计划
      get_task_form(ops_plans[0],@plan_details_m) if @plan_details_m.present?  #创建月计划
      get_task_form(ops_plans[0],@plan_details_j) if @plan_details_j.present?  #创建季度计划
      get_task_form(ops_plans[0],@plan_details_y) if @plan_details_y.present?  #创建年计划
    else
      Rails.logger.info "----单个站点-----"
      #return render json: {status: 'false'}
      ops_plans[0].edit_status='O'       #审核不通过时 状态修改为O   
      ops_plans[0].save
    end
 end

end









  private
  def get_ops_pnal
    @ops_plan = DOpsPlanManage.find(params[:id])
  end

  #创建巡检工单
  def get_task_form(obj,plan_details)
    @d_task_form = DTaskForm.new()
    @d_task_form.job_no = "GD"+crc(Time.now.strftime("%H%M%S")).to_s << rand(9999).to_s
    @d_task_form.create_type = @d_task_form.fault_type =="fault_form" ? "plan_out" : "plan_in"
    #@d_task_form.author = @current_user.login_name
    @d_task_form.author = '系统'
    @d_task_form.job_status = "un_write"     #工单状态  工单待分配
    @d_task_form.fault_type = "polling_form"
    @d_task_form.urgent_level = 'I'
    @d_task_form.title = '任务检查单'
    @d_task_form.clraring_if = @d_task_form.clraring_if.present? ? @d_task_form.clraring_if : "N"
    @d_task_form.d_station_id = obj.d_station_id.present? ? obj.d_station_id : ''               #站点id
    @d_task_form.station_name = obj.station_name.present? ? obj.station_name : ''               #站点名称
    #@d_task_form.polling_plan_time = obj.week_end_time.present? ? obj.week_end_time : ''        #计划完成时间
    #@d_task_form.polling_plan_time = obj.week_end_time.present? ? obj.week_end_time : ''        #计划完成时间
    
    d_station = DStation.find_by(:id => @d_task_form.d_station_id)
    @d_task_form.s_region_code_info_id = d_station.s_region_code_info_id
    work = ""     #存polling_job_type 字段信息 
    if plan_details.length>1
      plan_details.each do |detail|
        work+="#{detail.d_work_pro_id},"
        @d_task_form.polling_ops_type = detail.job_flag

        @d_task_form.polling_plan_time = detail.job_time   #计划完成时间
        
        end
        @d_task_form.polling_job_type = work
    else
        @d_task_form.polling_job_type = plan_details[0].d_work_pro_id
        @d_task_form.polling_ops_type = plan_details[0].job_flag

        @d_task_form.polling_plan_time = plan_details[0].job_time   #计划完成时间
    end
    if @d_task_form.save
      #创建巡检处理单
      @polling = DWorkPro.where(id: @d_task_form.polling_job_type.split(","))
      @polling.each do |polling|
          @d_task_form.d_fault_handles.create(handle_man: @current_user.login_name,handle_type: polling.work_name,
                                            handle_time: Time.now)
      end

      #创建日志
      #@details = @d_task_form.d_fault_job_details.new(:job_status =>@d_task_form.job_status,
      @details = @d_task_form.d_fault_job_details.new(:job_status => 'writing',     #日志中记录  创建工单
      :handle_man =>"系统",:begin_time =>Time.now,
      :end_time =>Time.now,:handle_status =>"完成",:without_time_flag =>"无")
      @details.save


    end
  end

end
