class SearchStationsController < ApplicationController

  def index
    station_arr = []
    father_areas = SAdministrativeArea.where(:parent_id => nil )
    father_areas.each do |f_a|
      f_a.children.each do |chil|
        station_arr << {id: chil.id,city_name: chil.zone_name,
                        d_stations: chil.d_stations.map{|x|{id:x.id,station_name:x.station_name.split("-")[1]}}}
      end
      bj_arr = []
      if f_a.zone_name == "北京市"
         DStation.where(:region_code => "北京市").each do |station|
           bj_arr << {id:station.id,station_name:station.station_name.split("-")[1]}
         end
         station_arr <<  {id: f_a.id,city_name: f_a.zone_name,
                          d_stations: bj_arr}
      end
    end
    render json: {all_station: station_arr}
  end

end