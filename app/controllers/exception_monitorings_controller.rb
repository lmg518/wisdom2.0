class ExceptionMonitoringsController < ApplicationController
  before_action :current_user

#exception_infos：{station_ids:[],d_time: 2017/04/05}
  def index
  end

  def init
    @index = []
    @d_stations = []
    @role = @current_user.s_role_msg
    case @role.role_name
      when '运维人员'
        @stations = @current_user.d_stations.by_station_ids(params[:exception_infos].present? ? params[:exception_infos][:station_ids] : "").active
      else
        @region = @current_user.s_region_code
        @stations = @region.d_stations.by_station_ids(params[:exception_infos].present? ? params[:exception_infos][:station_ids] : "").active
    end
    @stations.find_in_batches do |station|
      @d_stations << station
    end
    return render json: {sources: @index} if @d_stations.blank?
    @station_ids = DStation.all.pluck(:id)
    #按站点和时间查询
    if params[:exception_infos].present?
      @station_arr = []
      @exception_infos = params[:exception_infos]
      @exception_time = @exception_infos['d_time'].present? ? Time.parse(@exception_infos['d_time']) : Time.now
      @d_stations[0].each do |station|
        @t_arr = []
        @i_arr = []
        @abnormals = []
        station.d_abnormal_data_yyyymms.where("create_acce  BETWEEN ? AND ?", @exception_time.beginning_of_day,
                                              @exception_time.end_of_day).find_in_batches(batch_size: 100) do |abnormal|
          @abnormals << abnormal
        end
        next if @abnormals.blank?
        @normal_hours_data = @abnormals[0].map {|x| {d_day: x.create_acce.strftime("%Y/%m/%d"),
                                                     flag: 'false',
                                                     d_time: x.create_acce.strftime("%H:00:00")}}
        @normal_hours_data = @normal_hours_data.uniq
        24.times do |i|
          y_time = (@exception_time.beginning_of_day + i.hours).strftime("%Y/%m/%d")
          h_time = (@exception_time.beginning_of_day + i.hours).strftime("%H:00:00")
          @d_hash = {d_day: y_time, flag: 'false', d_time: h_time}
          if @normal_hours_data.present?
            if @normal_hours_data.include?(@d_hash)
              @t_arr << {d_day: y_time, flag: 'false', d_time: h_time}
            else
              @t_arr << {d_day: y_time, flag: 'true', d_time: h_time}
            end
          end
        end
        abnormal_item_ids = @abnormals[0].map {|x| x.item_code_id}.uniq
        abnormal_item_ids.each do |item_ids|
          next if item_ids == 9999
          @abnoormal_item_arr = []
          item = DAbnormalDataYyyymm.item_hash(item_ids)
          @abnormal_item_hash = @abnormals[0].select {|x| x.item_code_id == item_ids}.map {|x| {d_day: x.create_acce.strftime("%Y/%m/%d"),
                                                                                                flag: 'false',
                                                                                                d_time: x.create_acce.strftime("%H:00:00")}}
          @abnormal_item_hash = @abnormal_item_hash.uniq
          24.times do |i|
            y_time = (@exception_time.beginning_of_day + i.hours).strftime("%Y/%m/%d")
            h_time = (@exception_time.beginning_of_day + i.hours).strftime("%H:00:00")
            @item_hash = {d_day: y_time, flag: 'false', d_time: h_time}
            if @abnormal_item_hash.present?
              if @abnormal_item_hash.include?(@item_hash)
                @abnoormal_item_arr << {d_day: y_time, flag: 'false', d_time: h_time}
              else
                @abnoormal_item_arr << {d_day: y_time, flag: 'true', d_time: h_time}
              end
            end
          end
          @i_arr << {id: item[:id], name: item[:name], d_sources: @abnoormal_item_arr}

        end
        #
        # @abnormals[0].each do |x|
        #   next if x.item_code_id == 9999
        #   item = DAbnormalDataYyyymm.item_hash(x.item_code_id)
        #
        #   @i_arr << {id: item[:id], name: item[:name], d_sources: @t_arr}
        # end

        @index << {id: station.id, name: station.station_name, d_sources: @t_arr, factors: @i_arr.uniq}
      end

      @index #= Kaminari.paginate_array(@index.html.erb).page(params[:page]).per(10)
      return render json: {sources: @index}
    end
    #初始数据
    @d_stations[0].each do |station|
      @time_arr =[]
      @item_arr = []
      @i_arr = []
      @abnormals = []
      station.d_abnormal_data_yyyymms.where("create_acce  BETWEEN ? AND ?", ((Time.now - 24.hours).beginning_of_hour), Time.now.end_of_hour).order(:create_acce => :desc)
          .find_in_batches(batch_size: 100) do |abnormal|
        @abnormals << abnormal
      end
      next if @abnormals.blank?
      @normal_hours_data = @abnormals[0].map {|x| {d_day: x.create_acce.strftime("%Y/%m/%d"),
                                                   flag: 'false',
                                                   d_time: x.create_acce.strftime("%H:00:00")}}
      @normal_hours_data = @normal_hours_data.uniq
      24.times do |i|
        y_time = (Time.now - i.hours).strftime("%Y/%m/%d")
        h_time = (Time.now - i.hours).strftime("%H:00:00")
        @d_hash = {d_day: y_time, flag: 'false', d_time: h_time}
        if @normal_hours_data.present?
          if @normal_hours_data.include?(@d_hash)
            @time_arr << {d_day: y_time, flag: 'false', d_time: h_time}
          else
            @time_arr << {d_day: y_time, flag: 'true', d_time: h_time}
          end
        end
      end

      abnormal_item_ids = @abnormals[0].map {|x| x.item_code_id}.uniq
      abnormal_item_ids.each do |item_ids|
        next if item_ids == 9999
        @abnoormal_item_arr = []
        item = DAbnormalDataYyyymm.item_hash(item_ids)
        @abnormal_item_hash = @abnormals[0].select {|x| x.item_code_id == item_ids}.map {|x| {d_day: x.create_acce.strftime("%Y/%m/%d"),
                                                                                              flag: 'false',
                                                                                              d_time: x.create_acce.strftime("%H:00:00")}}
        @abnormal_item_hash = @abnormal_item_hash.uniq
        24.times do |i|
          y_time = (Time.now - i.hours).strftime("%Y/%m/%d")
          h_time = (Time.now - i.hours).strftime("%H:00:00")
          @item_hash = {d_day: y_time, flag: 'false', d_time: h_time}
          if @abnormal_item_hash.present?
            if @abnormal_item_hash.include?(@item_hash)
              @abnoormal_item_arr << {d_day: y_time, flag: 'false', d_time: h_time}
            else
              @abnoormal_item_arr << {d_day: y_time, flag: 'true', d_time: h_time}
            end
          end
        end
        @i_arr << {id: item[:id], name: item[:name], d_sources: @abnoormal_item_arr.reverse}
      end

      # @abnormals[0].each do |x|
      #   begin
      #     next if x.item_code_id == 9999
      #     # item = SAbnormalLtem.find(x)
      #     item = DAbnormalDataYyyymm.item_hash(x.item_code_id)
      #     @item_arr << {id: item[:id],name: item[:name],d_sources:@time_arr.reverse}
      #   # rescue Exception => e
      #   end
      # end

      @index << {id: station.id, name: station.station_name, d_sources: @time_arr.reverse, factors: @i_arr.uniq}
    end

    @index #= Kaminari.paginate_array(@index).page(params[:page]).per(10)
    return render json: {sources: @index}
  end

# params[:id] 站点id params[:data_time]
#params[:factor_id]  因子详情
# params[:station_infos]  单元格数据  d_station_id  item_id d_time 
  def show
    @time_arr =[]
    @item_arr = []
    @d_stations = DStation.find(params[:id])
    @data_time = Time.parse(params[:d_time]) if params[:d_time].present?
    if @data_time.present?
      @abnormals = @d_stations.d_abnormal_data_yyyymms.where("create_acce  BETWEEN ? AND ?", @data_time.beginning_of_day.strftime("%Y-%m-%d %H:%M:%S"), @data_time.end_of_day.strftime("%Y-%m-%d %H:%M:%S")).order(:create_acce)
    else
      @abnormals = @d_stations.d_abnormal_data_yyyymms.where("create_acce  BETWEEN ? AND ?", (Time.now - 24.hours).beginning_of_hour.strftime("%Y-%m-%d %H:%M:%S"), Time.now.end_of_hour.strftime("%Y-%m-%d %H:%M:%S")).order(:create_acce)
    end

    if params[:station_infos].present?
      puts "因子5分钟数据"
      station_infos = params[:station_infos]
      @d_time = Time.parse(station_infos['d_time'])
      @abnormals = @d_stations.d_abnormal_data_yyyymms.where("create_acce  BETWEEN ? AND ?", @d_time.beginning_of_hour.strftime("%Y-%m-%d %H:%M:%S"), @d_time.end_of_hour.strftime("%Y-%m-%d %H:%M:%S")).order(:create_acce)
      if station_infos["d_station_id"].present? && station_infos['item_id'].present?
        item_abnormals = @abnormals.where(:item_code_id => station_infos['item_id'].to_i)
        item_arr = []
        item_abnormals.each do |abnormal|
          item = DAbnormalDataYyyymm.item_hash(abnormal.item_code_id)
          item_arr << {item_id: item[:id], item_name: item[:name],
                       abnormal_name: abnormal.s_abnormal_rule_instance.instance_name,
                       item_val: abnormal.ab_value,
                       item_lable: abnormal.ab_lable,
                       item_time: abnormal.create_acce.strftime("%Y-%m-%d %H:%M:%S"),
                       contrast_val: abnormal.compare_value2,
          }
        end
        # @begin_time = @d_time.beginning_of_hour
        # @end_time = @d_time.end_of_hour
        # @abnormals.pluck(:item_code_id).each do |x|
        #   @f_data_arr = []
        #   @f_arr= []
        #       @item = SAbnormalLtem.find(x)
        #       @d_5_datas = DAbnormalDataYyyymm.where(" s_abnormal_rule_instance_id = ? and create_acce BETWEEN ? AND ?",SAbnormalRuleInstance.find_by_s_abnormal_ltem_id(x).id,@begin_time.strftime("%Y-%m-%d %H:%M:%S"),@end_time.strftime("%Y-%m-%d %H:%M:%S"))
        #       @d_5_datas.uniq.each do |f_d|
        #       puts  @d_5_hash = {d_day: Time.parse(f_d.create_acce).strftime("%Y/%m/%d"),flag:'false',d_time: Time.parse(f_d.create_acce).strftime("%H:%M:00")}
        #         12.times do |i|
        #           y_time = @begin_time.strftime("%Y/%m/%d")
        #           h_time = (@begin_time + (5 * i).minutes).strftime("%H:%M:00")
        #           time_data = {d_day: y_time,flag:'true',d_time:h_time}
        #           if @d_5_hash[:d_time] >= h_time && @d_5_hash[:d_time] <= (@begin_time + (10 * i).minutes).strftime("%H:%M:00")
        #             @f_data_arr << @d_5_hash
        #           else
        #             @f_data_arr << time_data
        #           end
        #         end
        #
        #       end
        #       @f_arr << {id: @item.id,name: @item.item_name,d_sources:@f_data_arr.uniq}
        # end
        #  @index.html.erb = {d_station:{id: @d_stations.id,name: @d_stations.station_name,factors: @f_arr.present? ? @f_arr.uniq : [] }}
        @index = {d_station: {id: @d_stations.id, name: @d_stations.station_name, abnormals: item_arr.uniq}}
      else
        item1_arr = []
        @abnormals.each do |abnormal|
          item = abnormal.s_abnormal_ltem
          item1_arr << {item_id: item.id, item_name: item.item_name, abnormal_name: abnormal.s_abnormal_rule_instance.instance_name, item_val: abnormal.ab_value, item_lable: abnormal.ab_lable, item_time: abnormal.create_acce}
        end
      end
      return render json: @index
    end

    if params[:factor_id].present?
      puts "********************因子小时数据"
      @abnormals.where(:item_code_id => params[:factor_id])
      @normal_hours_data = @abnormals.map {|x| {d_day: Time.parse(x.create_acce).strftime("%Y/%m/%d"), flag: 'false', d_time: Time.parse(x.create_acce).strftime("%H:00:00")}}
      @abnormals.where(:item_code_id => params[:factor_id]).each do |abnormal|
        @abnormal_item = abnormal.s_abnormal_ltem
        24.times do |i|
          y_time = (@data_time + i.hours).strftime("%Y/%m/%d")
          h_time = (@data_time + i.hours).strftime("%H:00:00")
          @d_hash = {d_day: y_time, flag: 'false', d_time: h_time}
          if @normal_hours_data.present?
            if @normal_hours_data.include?(@d_hash)
              @time_arr << {d_day: y_time, flag: 'false', d_time: h_time}
            else
              @time_arr << {d_day: y_time, flag: 'true', d_time: h_time}
            end
          end
        end
      end
      puts ">>>>>>>因子小时数据#{@time_arr.uniq}"
      return render json: {actors: {id: @abnormal_item.id, name: @abnormal_item.item_name, d_sources: @time_arr.uniq}}
    else
      puts "********************站点小时数据"
      puts @abnormals.inspect

      @normal_hours_data = @abnormals.map {|x| {d_day: Time.parse(x.create_acce).strftime("%Y/%m/%d"), flag: 'false', d_time: Time.parse(x.create_acce).strftime("%H:00:00")}}
      @normal_hours_data.uniq!
      puts @normal_hours_data.inspect
      24.times do |i|
        y_time = (@data_time.beginning_of_day + i.hours).strftime("%Y/%m/%d")
        h_time = (@data_time.beginning_of_day + i.hours).strftime("%H:00:00")
        @d_hash = {d_day: y_time, flag: 'false', d_time: h_time}
        if @normal_hours_data.present?
          if @normal_hours_data.include?(@d_hash)
            @time_arr << {d_day: y_time, flag: 'false', d_time: h_time}
          else
            @time_arr << {d_day: y_time, flag: 'true', d_time: h_time}
          end
        end
      end

      @abnormals.pluck(:item_code_id).each do |x|
        item = SAbnormalLtem.find(x)
        @item_arr << {id: item.id, name: item.item_name, d_sources: @time_arr.reverse}
      end

      @index = {d_station: {id: @d_stations.id, name: @d_stations.station_name, d_sources: @time_arr.reverse, factors: @item_arr.uniq!}}
      render json: @index
    end

  end

end


