class DLoginMsgsController < ApplicationController
  before_filter :authenticate_user
  before_action :set_d_login_msg, only: [:show, :edit, :update, :destroy]

  # GET /d_login_msgs
  # GET /d_login_msgs.json
  def index
    @role_id      = params[:role_id]
    @role_name    = params[:role_name]
    @login_name   = params[:login_name]
    @s_role_msgs  = SRoleMsg.all
    if @login_name.present? || @role_id.present?
      @d_login_msgs = DLoginMsg.by_role_ids(@role_id).by_login_name(@login_name).order(:created_at => :desc).page(params[:page]).per(10)
     return
    end
    @d_login_msgs = DLoginMsg.order(:created_at => :desc).page(params[:page]).per(params[:per])
  end

  # post 测试
  def get_datas
    render json:{RETCODE: 'R20', RETMSG: 'OK'}
  end


  # 获取所有的总公司、分公司
  def get_branchs
    @region_codes = SRegionCode.where(:s_region_level_id => [1,2]).map{|x| {id: x.id, region_name: x.region_name}}
    render json: @region_codes
  end

  # 获取分公司下的所有车间 部门
  def get_region_codes
    region_id = params[:id]
    @region_codes = SRegionCode.where(:father_region_id => region_id)
    render json: @region_codes
  end

  # GET /d_login_msgs/1
  # GET /d_login_msgs/1.json
  def show
  end

  # GET /d_login_msgs/new
  def new
    @d_login_msg    = DLoginMsg.new
    @s_region_codes = SRegionCode.where( "is_leaf = ?", "0" )
    @son_regions    = SRegionCode.where( "is_leaf = ?", "1" )
    @s_role_msgs    = SRoleMsg.all
    # session[:name] = "催小培"
  end

  # GET /d_login_msgs/1/edit
  def edit
    @s_region_codes = SRegionCode.where( "is_leaf = ?", "0" )
    @son_regions    = SRegionCode.where( "is_leaf = ?", "1" )
    @s_role_msgs    = SRoleMsg.all
    
    if params[:reset_passwd] == "Yes"
      render :edit_passwd
    else
      render :edit
    end
  end

  # POST /d_login_msgs
  # POST /d_login_msgs.json
  def create
    retCode = true
    unit_id = params[:d_login_msg][:s_region_code_info_id]
    # region = SRegionCodeInfo.find_by(:id => unit_id)
    #              .s_region_code
    params[:d_login_msg][:group_id] = 0
  
    @d_login_msg = DLoginMsg.new(d_login_msg_params)

    #group_id 人员属于那个车间
    # code_info = SRegionCodeInfo.find(@d_login_msg.s_region_code_info_id)
    # @d_login_msg.group_id = code_info.s_region_code_id
    @d_login_msg.group_id = unit_id

    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_login_msgs|create"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = current_user.login_no
    wLoginOpr.ip_addr   = current_user.ip_address
    wLoginOpr.op_note   = "增加工号"
    wLoginOpr.op_describe = "增加工号，工号名称：" + d_login_msg_params[:login_name]

    @d_login_msg.transaction do
      retCode = @d_login_msg.save
      retCode = wLoginOpr.save if retCode
    end

    respond_to do |format|
      if retCode
        format.html { redirect_to "/d_login_msgs" }
        format.json { render json: {location: @d_login_msg ,status: :created} }
      else
        format.html { render :new }
        format.json { render json: @d_login_msg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /d_login_msgs/1
  # PATCH/PUT /d_login_msgs/1.json
  def update
    retCode = true

     # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_login_msgs|update"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = current_user.login_no
    wLoginOpr.ip_addr   = current_user.ip_address
    wLoginOpr.op_note   = "编辑工号"
    wLoginOpr.op_describe = "编辑工号，" + "工号编码：" + @d_login_msg.login_no + ", 工号名称：" + @d_login_msg.login_name

    @d_login_msg.transaction do
      retCode = @d_login_msg.update(d_login_msg_params)

      #更新group_id
      @d_login_msg.group_id = params[:d_login_msg][:s_region_code_info_id]
      @d_login_msg.save
      
      retCode = wLoginOpr.save if retCode
    end
    
    respond_to do |format|
      if retCode
        format.html { }
        format.json { render json:{status: :ok, location: @d_login_msg}  }
      else
        format.html { render :edit }
        format.json { render json: @d_login_msg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /d_login_msgs/1
  # DELETE /d_login_msgs/1.json
  def destroy
    @d_login_msg.destroy
    respond_to do |format|
      format.html { redirect_to d_login_msgs_url, notice: 'D login msg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # #当前用户信息
  # def user_info
  #   render json: {userinfo: current_user}   if current_user.present?
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_d_login_msg
      @d_login_msg = DLoginMsg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def d_login_msg_params
      params.require(:d_login_msg).permit(:login_no, :login_name, :password, :telphone,
                                          :password_confirmation, :password_digest, :group_id,
                                          :s_role_msg_id, :valid_flag, :valid_begin, :valid_end,
                                          :ip_address, :describe, :id_card, :email ,:work_num,
                                          :work_status,:s_region_code_info_id,:s_area_id)
    end
end
