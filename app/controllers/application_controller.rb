class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :get_test

  before_filter :add_xframe
  def add_xframe
    headers['X-Frame-Options'] = 'ALLOWALL'
    # headers['Access-Control-Allow-Origin'] = "*"
    # headers['Access-Control-Allow-Methods'] =["GET, POST, PATCH, PUT, DELETE, OPTIONS"]
    # headers['Access-Control-Allow-Headers'] = 'x-requested-with,content-type'
  end

  # around_filter :record_memory
  #
  # def record_memory
  #   process_status = File.open("/proc/#{Process.pid}/status")
  #   13.times {process_status.gets}
  #   rss_before_action = process_status.gets.split[1].to_i
  #   process_status.close
  #   yield
  #   process_status= File.open("/proc/#{Process.pid}/status")
  #   13.times {process_status.gets}
  #   rss_after_action=process_status.gets.split[1].to_i
  #   process_status.close
  #   logger.info("CONSUME　MEMORY:　#{rss_after_action - rss_before_action}　\
  #     KB\tNow:　#{rss_after_action}　KB\t#{request.url}")
  # end


  # around_filter :log_rss      #具有before_filter和after_filter的功能
  # def log_rss
  #   before_rss,before_rss_t = _worker_rss    #action前该进程的内存
  #   yield                                                     #执行action
  #   after_rss,after_rss_t = _worker_rss        #action后该进程的内存
  #   after_rss_t ||= 0
  #   before_rss_t ||= 0
  #   if after_rss_t - before_rss_t > 100000000  #大于100M（大概）
  #     logger.info "#{controller_name}_#{action_name} rss info #{Process.pid} VmRSS: #{before_rss}----#{after_rss}"
  #   end
  # end
  # def _worker_rss
  #   proc_status = "/proc/#{Process.pid}/status"
  #   if File.exists? proc_status
  #     open(proc_status).each_line { |l|
  #       if l.include? 'VmRSS'
  #         ls = l.split
  #         if ls.length == 3
  #           value = ls[1].to_i
  #           unit = ls[2]
  #           val = case unit.downcase
  #                   when 'kb'
  #                     value*(1024**1)
  #                   when 'mb'
  #                     value*(1024**2)
  #                   when 'gb'
  #                     value*(1024**3)
  #                 end
  #           return ["#{value} #{unit}",val]
  #         end
  #       end
  #     }
  #     ["0",0]
  #   end
  #   ["0",0]
  # end
  


  def get_test
    # DDataYyyymm.get_five_minutes_data
    # DDataHourYyyy.get_hour_data
  end

  def w_log(op_code,login_num,op_note,op_describe)
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = op_code
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = login_num
    wLoginOpr.ip_addr   = request.ip.to_s
    wLoginOpr.op_note   = op_note
    wLoginOpr.op_describe = op_describe
    wLoginOpr.save!
  end
  
  def set_error_msg( error_msg )
    @error_msg = error_msg
    flash[:notice] = error_msg
  end
  
  def current_user
    Rails.logger.debug( "当前用户的SessionCode: " + session[:session_code].to_s )
    @current_user ||= DLoginMsg.find_by( "session" => params[:session].present? ? params[:session] : session[:session_code].to_s )
    if @current_user.present?
      session[:session_code] = @current_user.session
    end
    @current_user
  end
  
  def signed_in?
    !current_user.nil?
  end

  def authenticate_user
    ## 转存公共参数
    @curr_page      = params[:curr_page]
    @total_page     = params[:total_page]
    @size_pre_page  = params[:size_pre_page]

    Rails.logger.debug( "当前用户信息: #{current_user}" )
    if current_user.nil?
      redirect_to "/sessions/new", notice: '用户信息不存在，请注册？'
    else
      handle_request
    end
  end

  def handle_request
    # Rails.logger.info '-------------------=='
    # Rails.logger.info request.fullpath
    # Rails.logger.info '-------------------=='
    # Rails.logger.info controller_path
    @flag = false
    url_str = request.fullpath
    puts @current_user.login_name
    if @current_user
      begin
          # render_error(401)
      rescue Exception => e
            puts e
      end
    end
  end

  def render_error(status)
    render template: "/errors/#{status}", format: [:html],
           handler: [:erb], status: status, layout: 'home_iframe'
  end


  def send_sms(mobile)
    ChinaSMS.use :yunpian, password: ProductConfig::YUN_PIAN_KEY
    ChinaSMS.to mobile, "【鑫属运维系统】尊敬的#{current_user.login_name},您上次的登录时间为#{Time.now.strftime('%Y-%m-%d %H:%M:%S')}"
  end

  def crc(ftp_str)
    @crc_reg = 0xFFFF
    @num = 0xA001
    @inum = 0
    str_bytes = ftp_str.bytes
    str_bytes.length.times do |i|
      @inum = str_bytes[i]
      @crc_reg = (@crc_reg >> 8) & 0x00FF
      @crc_reg ^= @inum
      8.times do |i|
        flag = @crc_reg % 2
        @crc_reg = @crc_reg >> 1
        if flag ==1
          @crc_reg = @crc_reg ^ @num
        end
      end
    end
    return @crc_reg
  end

end
