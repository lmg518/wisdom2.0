class AlarmDespatchesController < ApplicationController
  before_action :current_user

  def index
    if params[:search].present?
      @search = params[:search]
      @time = @search[:date]
      @create_user = @search[:create_user]
      @status = @search[:status]
      @receive_user = @search[:receive_user]
    end
    @despatches = DAlarmDespatch.by_create_user(@create_user)
                                .by_status(@status)
                                .by_date_time(@time)
                                .by_receive_user(@receive_user)
                                .order(:created_at => "desc")
    # @despatches = Kaminari.paginate_array(@despatches).page(params[:page]).per(10)
    @despatches = @despatches.page(params[:page]).per(params[:per])
  end

  def despatch_infos
    deal_arr = []
    @despath = DAlarmDespatch.find_by(:id => params[:id])
    return render json:{ status: :unprocessable_entity ,location: "访问的数据不存在！" }  if @despath.blank?
    d_task_deal_records = @despath.d_task_deal_records.order(:created_at => :desc)
    d_task_deal_records.each do |record|
      deal_arr << {id: record.id, flow_no: record.flow_no, task_status: record.status_to_str, deal_login: record.deal_login,
                   note: record.note, deal_time: record.deal_time.strftime("%Y-%m-%d %H:%M:%S"), deal_accept: record.deal_accept}
    end
    @task_deal_infos = deal_arr
    render :partial => "despatch_infos", :layout => false
  end

  def show
    deal_arr = []
    @despath = DAlarmDespatch.find_by(:id => params[:id])
    return render json:{ status: :unprocessable_entity ,location: "访问的数据不存在！" }  if @despath.blank?
    d_task_deal_records = @despath.d_task_deal_records
    d_task_deal_records.each do |record|
      deal_arr << {id: record.id, flow_no: record.flow_no, task_status: record.status_to_str, deal_login: record.deal_login,
                   note: record.note, deal_time: record.deal_time.strftime("%Y-%m-%d %H:%M:%S"), deal_accept: record.deal_accept}
    end
    return render json: {task_deal_infos: deal_arr}
  end

  def update

    @despath = DAlarmDespatch.find_by(:id => params[:id])
    return render json:{ status: :unprocessable_entity ,location: "访问的数据不存在！" }  if @despath.blank?

    if params[:handle_answer].present?    #响应
      @react =  @despath.update!(:task_status => 'answer', :receive_login => @current_user.login_name, :receive_time => Time.now)
      handle_despath
      return
    end

    if params[:handle_retweet].present?   #转发
      @react =  @despath.update!(:task_status => 'retweet', :receive_login => @current_user.login_name)
      handle_despath
      return
    end

    if params[:handle_do].present?  #处理
      @react =  @despath.update!(:task_status => "doing", :receive_login => @current_user.login_name)
      handle_despath
      return
    end

    if params[:handle_done].present?  #完成
      @react =  @despath.update!(:task_status => 'done', :receive_login => @current_user.login_name, :deal_time => Time.now)
      handle_despath
      return
    end

  end

  def handle_despath
    respond_to do |format|
      if @react
        format.json { render json:{status: :ok, location: @despath}  }
      else
        format.json { render json: @despath.errors, status: :unprocessable_entity }
      end
    end
  end

end