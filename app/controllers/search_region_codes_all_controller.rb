class SearchRegionCodesAllController < ApplicationController
  #before_action :authenticate_user

  #查询所有的车间 部门
  #  search_region_codes_all
  def index
   @regioncodes = SRegionCode.select(:id,:region_name).all
   render json: {unit_region: @regioncodes}
  end

end