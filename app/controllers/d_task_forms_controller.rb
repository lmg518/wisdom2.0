class DTaskFormsController < ApplicationController
  before_action :set_d_task_form, only: [:show, :edit, :update, :destroy, :sign_in]
  before_action :authenticate_user
  # GET /d_task_forms
  # GET /d_task_forms.json
  def index
     #获取创建开始时间
     @created_time=params[:created_time].to_s + " 00:00:00" if params[:created_time].present?
     #获取结束时间
     @end_time=params[:end_time].to_s + " 23:59:59" if params[:end_time].present?
  end

  # GET /d_task_forms/1
  # GET /d_task_forms/1.json
  def show
  end

  # GET /d_task_forms/new
  def new
    @d_task_form = DTaskForm.new
  end

  # GET /d_task_forms/1/edit
  def edit
  end

  # GET  /d_task_forms/35/get_form_module.json
  #  根据工作项id 获取模板信息
  # def get_form_module
  #   @form_module=FFormModule.find_by(:d_work_pro_id => params[:id])
  #   title_ids=@form_module.field_title_id.split(",")
  #   @field_titles=FFieldTitle.where(:id => title_ids)
  # end

  #签到
  def sign_in
    @station=DStation.find(@d_task_form.d_station_id)
    @station_ip=@station.station_ip
    @user_ip=@current_user.ip_address

    @task_line=DLogionTaskLine.new
    @task_line.d_task_form_id = params[:id]
    @task_line.get_time=Time.now
    @task_line.login_name=@current_user.login_name

    respond_to do |format|
      if @station_ip == @user_ip && @task_line.save
        format.html { render html: "<h3>OK</h3>"}
        format.json { render json: {status: :ok} }
      else
        #format.json { render json{status: :ng} }
        format.json { render json: {status: :ng} }
      end
    end
  end






  # POST /d_task_forms
  # POST /d_task_forms.json
  def create
    @d_task_form = DTaskForm.new(d_task_form_params)
    @d_task_form.job_no = "GD"+crc(Time.now.strftime("%H%M%S")).to_s << rand(9999).to_s
    @d_task_form.create_type = @d_task_form.fault_type =="fault_form" ? "plan_out" : "plan_in"
    @d_task_form.author = @current_user.login_name
    @d_task_form.job_status = "un_write"
    @d_task_form.clraring_if = @d_task_form.clraring_if.present? ? @d_task_form.clraring_if : "N"
    @d_task_form.polling_plan_time = @d_task_form.polling_plan_time.present? ? @d_task_form.polling_plan_time.strftime("%Y-%m-%d")+" 23:59:59" : ''
    d_station = DStation.find_by(:id => @d_task_form.d_station_id)
    @d_task_form.s_region_code_info_id = d_station.s_region_code_info_id

    #异常图片上传
    image_file = params[:d_task_form][:image_file]
    if image_file.present?
      img_flag = "jpeg"  if image_file[0,15] == "data:image/jpeg"
      img_flag = "png"  if image_file[0,14] == "data:image/png"
      png      = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
      time = Time.now
      file_path = "#{Rails.root}/public/test/#{time.year}/#{time.month}/#{time.day}/"
      picture = "#{time.strftime("%H%M%S")}.#{img_flag}"
      FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
      #向dir目录写入文件
      File.open(Rails.root.join("#{file_path}", picture ), 'wb') { |f| f.write(png) }
      #存储图片
      @d_task_form.img_one = "/test/#{time.year}/#{time.month}/#{time.day}/"+"#{picture}"
    end

    respond_to do |format|
      if @d_task_form.save
          #创建巡检处理单
          @polling = DWorkPro.where(id: @d_task_form.polling_job_type.split(","))
          @polling.each do |polling|
              @d_task_form.d_fault_handles.create(handle_man: @current_user.login_name,handle_type: polling.work_name)
          end
        #创建日志
        details_create(@d_task_form)
        format.html { redirect_to @d_task_form, notice: 'D task form was successfully created.' }
        format.json { render :show, status: :created, location: @d_task_form }
      else
        format.html { render :new }
        format.json { render json: @d_task_form.errors, status: :unprocessable_entity }
      end
    end
  end

  
  
  # PATCH/PUT /d_task_forms/1
  # PATCH/PUT /d_task_forms/1.json
  def update
    job_status = params[:job_status] #工单状态
    d_task_form_id = params[:id] #工单ID
    tasks = params[:task][:tasks1]
    #检查项图片上传
    if tasks.present?
      upload_img(tasks,d_task_form_id) 
    end

    respond_to do |format|
      if @d_task_form.update(:job_status =>job_status)
        details_create(@d_task_form) #创建日志
        format.html { redirect_to @d_task_form, notice: 'D task form was successfully updated.' }
        format.json { render :show, status: :ok, location: @d_task_form }
      else
        format.html { render :edit }
        format.json { render json: @d_task_form.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /d_task_forms/1
  # DELETE /d_task_forms/1.json
  def destroy
    @d_task_form.destroy
    respond_to do |format|
      format.html { redirect_to d_task_forms_url, notice: 'D task form was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_d_task_form
      @d_task_form = DTaskForm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def d_task_form_params
      params.require(:d_task_form).permit( :author ,:handle_man ,:handle_man_old ,:job_no ,
                                          :job_status ,:fault_type ,:create_type ,:urgent_level ,
                                          :send_type ,:d_station_id ,:station_name ,:device_name ,
                                          :polling_plan_time ,:polling_ops_type ,:polling_job_type ,
                                          :clraring_if ,:abnormal_begin_time ,:abnormal_end_time ,
                                          :abnormal_notes ,:fault_phenomenon ,:other_fault ,:title ,
                                          :content ,:audit_if ,:audit_man ,:audit_time ,:audit_des)
    end

    #创建工单日志
    def details_create(d_task_form)
      @details = @d_task_form.d_fault_job_details.new(:job_status =>@d_task_form.job_status,
                                                  :handle_man =>@d_task_form.handle_man,:begin_time =>Time.now,
                                                  :end_time =>Time.now,:handle_status =>"完成",:without_time_flag =>"无")
      @details.save
    end

    #上传图片
    def upload_img(tasks,d_task_form_id)
      @task = DTaskForm.find(d_task_form_id)
        tasks.each_value do |task| 
          handle_type = task['handle_type']
          image_file = task['image_file']
          img_flag = "jpeg"  if image_file[0,15] == "data:image/jpeg"
          img_flag = "png"  if image_file[0,14] == "data:image/png"
          png      = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
          @handle = DFaultHandle.find_by(:d_task_form_id =>d_task_form_id,:handle_type=> handle_type)
          time = Time.now
          file_path = "#{Rails.root}/public/test/#{time.year}/#{time.month}/#{@task.id}/"
          picture = "#{@handle.id}.#{img_flag}"
          FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
          #向dir目录写入文件
          File.open(Rails.root.join("#{file_path}", picture ), 'wb') { |f| f.write(png) }
          #存储图片
          @handle.update(:img_one => "/test/#{time.year}/#{time.month}/#{@task.id}/"+"#{picture}",:handle_time =>Time.now)
          end
      end
end
