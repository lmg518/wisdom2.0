class SearchUnitsController < ApplicationController

  def index
     station = DStation.find_by(:id => params[:station_id])
     region = station.s_region_codes.select{|x| x.region_code != "全国"}
     units = region.present? ? region[0].s_region_code_infos : []
     render json:{units: units}
  end

end