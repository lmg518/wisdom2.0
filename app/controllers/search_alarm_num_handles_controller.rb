class SearchAlarmNumHandlesController < ApplicationController
  before_action :current_user

  def index
    search_parms
    handle_table
    @region_arr
  end

  def export_file
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["区域名称", "任务数量", "响应数量", "未响应数量", "响应比率", "未响应比率"]
      @region_arr.each do |alarm|
        csv << [alarm[:region_name], alarm[:despatch_counts], alarm[:answer_times], alarm[:unanswer_times],
                alarm[:despatch_counts].to_i == 0 ? "0.00" :  (alarm[:answer_times].to_f / alarm[:despatch_counts].to_f).round(2) * 100,
                alarm[:despatch_counts].to_i == 0 ? "0.00" : (alarm[:unanswer_times].to_f / alarm[:despatch_counts].to_f).round(2) * 100 ]
      end
    end
    if  @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "报警反馈查询"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "报警反馈查询"
    end
    respond_to do |format|
      format.csv{
        send_data @scv,
                  :type=>'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private

  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids]
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
  end

  def search_parms
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @data_times = @search[:data_times]
      @despatch_status = @search[:status]
    end
  end

  def handle_table
    search_parms
    @region_arr = []
    @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
#-----part one
    @region_codes.each do |region|
      stations = region.d_stations
      next if stations.blank?
      @despatch_total = 0
      @answer_times, @doing_times = 0,0
      stations.each do |station|
        alarms = station.d_alarms
        next if alarms.blank?
        alarms.each do |alarm|
          alarm_despatches = alarm.d_alarm_despatches.where(:task_status => ['assign','answer'])
                                 .by_create_time(@data_times)
          next if alarm_despatches.blank?
          @despatch_total += alarm_despatches.length
          @answer_times, @doing_times = 0,0
          alarm_despatches.each do |despatch|
            answer_date = despatch.created_at
            deal_date = despatch.deal_time
            alarm = despatch.d_alarms.first
            alarm_level = alarm.alarm_level
            sla = SSla.find_by(:alarm_level => alarm_level.to_i)
            task_deal_records = despatch.d_task_deal_records
            next if task_deal_records.blank?
            @answer_times = despatch.d_task_deal_records.where(:task_status => "answer").length
            # .where("deal_time <= ?", answer_date + (sla.receive_minutes).hours ).length
            @doing_times = despatch.d_task_deal_records.where(:task_status => "assign").length
            # .where("deal_time <= ?", answer_date + (sla.receive_minutes).hours ).length
          end
        end
      end
      @region_arr << {region_name: region.region_name,
                      despatch_counts: @despatch_total,
                      answer_times: @answer_times,
                      unanswer_times: @doing_times}
    end
    @region_arr
  end

end
