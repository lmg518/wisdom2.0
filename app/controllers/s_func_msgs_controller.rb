class SFuncMsgsController < ApplicationController
  before_action :set_s_func_msg, only: [:show, :edit, :update, :destroy]

  def index
    @s_func_msgs = SFuncMsg.where(:father_func_id => 9999,:valid_flag =>"1").order(:queue_index)
  end

  def new
    @s_func_msg = SFuncMsg.new
  end

  def show
  end

  def create
    @s_func_msg = SFuncMsg.new(s_func_msg_params)

    respond_to do |format|
      if @s_func_msg.save
        format.html { redirect_to @s_func_msg, notice: 'Test iframe was successfully created.' }
        format.json { render :show, status: :created, location: @s_func_msg }
      else
        format.html { render :new }
        format.json { render json: @s_func_msg.errors, status: :unprocessable_entity }
      end
    end

  end

  def destroy
    @s_func_msg.destroy
    respond_to do |format|
      format.html { redirect_to s_func_msgs_url, notice: 'S func msg was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_s_func_msg
    @s_func_msg = SFuncMsg.find(params[:id])
  end

  def s_func_msg_params
    params.require(:s_func_msg).permit(:func_code,:func_name,:valid_flag,
                                      :root_distance,:queue_index,:func_url,
                                      :new_window_flag,:func_note)
  end

end
