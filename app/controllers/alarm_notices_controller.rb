class AlarmNoticesController < ApplicationController
  before_action :set_alarm_notice, only: [:show,:edit,:update,:destroy]
  before_action :current_user

  def index
    if params[:search].present?
      search = params[:search]
      @name = search[:name]
      @d_time = search[:d_time]
      @status = search[:status]
      @level = search[:alarm_level]
    end
    @alarm_notices = DAlarmNotice.by_name(@name)
                                 .by_level(@level)
                                  .by_d_time(@d_time)
                                  .by_status(@status).order(:created_at => :desc)
    @alarm_notices = @alarm_notices.page(params[:page]).per(params[:per])
  end

  def show

  end

  def edit

  end

  def create
    @alarm_notice = DAlarmNotice.new(get_alarm_notice_params)
    respond_to do |format|
      if @alarm_notice.save

        format.html {}
        format.json { render json: {location: @alarm_notice ,status: :created} }
      else
        format.html {}
        format.json { render json: @alarm_notice.errors, status: :unprocessable_entity }
      end
    end
  end

  def update

  end

  private

  def set_alarm_notice
    @alarm_notice = DAlarmNotice.find(params[:id])
  end

  def get_alarm_notice_params
    params.require(:alarm_notice).permit(:alarm_id, :alarm_level, :notice_type, :notice_login, :notice_content, :notice_time, :notice_accept)
  end

end