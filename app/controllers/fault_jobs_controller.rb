class FaultJobsController < ApplicationController
    before_action :get_fault_job, only: [:show, :update, :edit, :audit, :destory]
    before_action :authenticate_user

    #GET /fault_jobs
    #GET /fault_jobs.json
    def index
        @taskName = @current_user.s_role_msg.role_name
        #按条件查询
        @station_ids = params[:d_station_id] #站点id
        @job_no = params[:job_no]   #工单号
        @author = params[:author]   #创建人
        @created_time = params[:created_time].to_s + " 00:00:00" if params[:created_time].present? #创建起始时间  
        @end_time = params[:end_time].to_s + " 23:59:59" if params[:end_time].present?  #创建结束时间 
        @handle_man = params[:handle_man] #当前处理人
        @create_type = params[:create_type] #创建类型
        @job_status = params[:job_status] #工单状态

        # @faultJob = DTaskForm.by_job_no(@job_no).by_author(@author)
        #                     .by_time(@created_time,@end_time).by_station_id(@station_ids)
        #                     .by_handle_man(@handle_man).by_create_type(@create_type).by_job_status_s(@job_status)
        #                     .by_fault_type('fault_form')   #只显示故障单
        #                     .order(:created_at => :desc)
        #                     .page(params[:page]).per(params[:per])


      #运维人员登录时只显示他的工单
      if @current_user.s_role_msg.role_name == '运维成员'

        @faultJob = DTaskForm.by_handle_man_id(@current_user.id)
                            .by_job_no(@job_no).by_author(@author)
                            .by_time(@created_time,@end_time).by_station_id(@station_ids)
                            .by_handle_man(@handle_man).by_create_type(@create_type).by_job_status_s(@job_status)
                            .by_fault_type('fault_form')   #只显示故障单
                            .order(:created_at => :desc)
                            .page(params[:page]).per(params[:per])
      else
        @faultJob = DTaskForm.by_job_no(@job_no).by_author(@author)
                            .by_time(@created_time,@end_time).by_station_id(@station_ids)
                            .by_handle_man(@handle_man).by_create_type(@create_type).by_job_status_s(@job_status)
                            .by_fault_type('fault_form')   #只显示故障单
                            .order(:created_at => :desc)
                            .page(params[:page]).per(params[:per])
      end



    end

    #GET /fault_jobs/id
    #GET /fault_jobs/1.json
    def show
    end

    #GET /fault_jobs/new
    def new
        @faultJob = DTaskForm.new()
    end

    #POST /fault_jobs
    #POST /fault_jobs.json
    def create
        @faultJob = DTaskForm.new(params_fault_job)
        @faultJob.job_no = "GD"+crc(Time.now.strftime("%H%M%S")).to_s << rand(9999).to_s
        @faultJob.create_type = @faultJob.fault_type =="fault_form" ? "plan_out" : "plan_in"
        @faultJob.author = @current_user.login_name
        @faultJob.job_status = "un_write"
        @faultJob.clraring_if = @faultJob.clraring_if.present? ? @faultJob.clraring_if : "N"

        d_station = DStation.find_by(:id => @faultJob.d_station_id)
        @faultJob.s_region_code_info_id = d_station.s_region_code_info_id

        respond_to do |format|
            if @faultJob.save

                equip_info_y = SEquipmentInfo.find_by(:d_station_id =>@faultJob.d_station_id,:types => 'Y', :equip_type =>@faultJob.device_name)  #正在使用的设备
                @faultJob.update(:s_equipment_info_id => equip_info_y.id) if equip_info_y.present?
                
                #设备信息表
                # @equip = SEquipmentInfo.find_by(:d_station_id =>@faultJob.d_station_id,:equip_type =>@faultJob.device_name,
                #                                 :types => 'Y')
                # @equip.update(:d_task_form_id => @faultJob.id) if @equip.present?

                #创建日志
                #新建工单时不存在
                 if !@audit_if.present?
                    @audit_if='Y'
                end 
                details_create(@faultJob,@audit_if)
                format.html{redirect_to fault_jobs_path}
                format.json{render json: {status: "success",location: @faultJob}}
            else
                format.html{redirect_to new_fault_jobs_path}
                format.json{render json:{status: "false",location: @faultJob.errors}}
            end
        end
    end

    #GET /fault_jobs/id/edit
    def edit
        @handle = @faultJob.d_fault_handles
        #@equilp = SEquipmentInfo.where(:d_station_id =>@faultJob.d_station_id,:equip_type =>@faultJob.device_name, :types => 'N')
        @equilp = SEquipmentInfo.where(:equip_type =>@faultJob.device_name, :types => 'N')   #编辑页面 选择备机编号
                                         
    end

    #PUT /fault_jobs/id
    #PUT /fault_jobs/id.json
    def update
        equilp_id = params[:d_fault_handle][:equilp_id]
        @handle = DFaultHandle.find_by(:d_task_form_id =>@faultJob.id)

        #第一次时 没有处理记录表 需要新增
        if @handle.blank?
            #创建故障处理表
            @handle = DFaultHandle.new(params_handle)
        end

        #审核不通过时  已经存在了 更新原来的处理信息
        if @handle.present?
            @handle.update(params_handle)
        end

        @handle.handle_man = @current_user.login_name
        @handle.handle_time = Time.now
        @handle.d_task_form_id = @faultJob.id
        respond_to do |format|
            if @handle.save
                if @handle.handle_type == '更换备机'
                    @faultJob.update(:job_status =>'wait_maint', :pro_type =>'Y')   #更换备机工单的标识
                else
                    @faultJob.update(:job_status =>'un_audit', :pro_type =>'N')     #不更换备机的标识
                end
                
                #选择备机
                if equilp_id.present?
                    @eqruilp = SEquipmentInfo.find(equilp_id)
                    @eqruilp.update(:d_task_form_id =>@faultJob.id,:types => 'O',:d_station_id => @faultJob.d_station_id)  # O 代表备机已经使用了,将更换的站点id存到设备信息表中
                end
                
                #创建日志
                #新建工单时不存在
                 if !@audit_if.present?
                    @audit_if='Y'
                end 
                details_create(@faultJob, @audit_if)  

                format.html{redirect_to fault_jobs_path}
                format.json{render json:{status: "success",location: @faultJob}}
            else
                format.html{redirect_to edit_fault_jobs_path}
                format.json{render json:{status: "false",location: @faultJob.errors}}
            end
        end
    end

    # post  /fault_jobs/id/audit.json
    #审核
    def audit
        @audit_if = params[:audit_if]
        audit_man = @current_user.login_name
        audit_time = Time.now
        #audit_des = audit_if=="N" ? params[:audit_des] : ''

        job_status = @faultJob.job_status  #获取工单状态
        pro_type = @faultJob.pro_type      #获取工单标识  Y 更换备机   N 没更换备机
        @faultJob.audit_man = audit_man
        @faultJob.audit_time = audit_time

        if @audit_if == 'N' && job_status == 'un_audit' && pro_type == 'N'
            audit_des = params[:audit_des]
            @faultJob.audit_if='N'
            @faultJob.job_status='writing'
            @faultJob.audit_des = audit_des
            @faultJob.save
        #更换备机 工单  审核不通过时，工单改变为  待校准
        elsif @audit_if == 'N' && job_status == 'un_audit' && pro_type == 'Y'
            audit_des = params[:audit_des]
            @faultJob.audit_if='N'
            @faultJob.job_status='wait_calib'
            @faultJob.audit_des = audit_des
            @faultJob.save
        elsif @audit_if == 'Y' && job_status == 'un_audit'
            @faultJob.audit_if='Y'
            @faultJob.job_status='audit'
            @faultJob.save

            work_flag = params[:work_flag]    #审核通过时  处理方式存到维修项目表中 更换备机时
            #@maint_project=SMaintProject.find_by(:d_task_form_id => params[:id])  #关联维修项目
            if @maint_project.present?
                @maint_project.work_flag = work_flag
                @maint_project.save
            end
            #render :index
        else 
            return render json: {status: 'false'}
        end

        respond_to do |format|
            if @faultJob.save

                #新建工单时不存在
                if !@audit_if.present?
                    @audit_if='Y'
                end 

                details_create(@faultJob,@audit_if)
                format.html{}
                format.json{render json:{status: "success",location: @faultJob}}

                #极光推送
                

            else
                format.html{}
                format.json{render json:{status: "false",location: @faultJob.errors}}
            end
        end    
    end

    #转发
    #get /fault_jobs/id/forword.json   
    def forword
        @fault = DTaskForm.find(params[:id])
        @fault.handle_man_old = @fault.handle_man
        @fault.handle_man = ''
        @fault.handle_man_id = ''
        @fault.job_status = 'un_write'
        if @fault.save
            render json:{status: "success", location: @fault}
        end
    end

    def destory
        respond_to do |format|
            if @faultJob.destory
                format.html{}
                format.json{render json:{status: "success",location: @faultJob}}
            else
                format.html{}
                format.json{render json:{status: "false",location: @faultJob.errors}}
            end
        end 
    end

    private

    def details_create(faultJob,audit_if)
        @details = faultJob.d_fault_job_details.new(:job_status =>faultJob.job_status,:note =>faultJob.audit_des,
                                                    :handle_man =>current_user.login_name,:begin_time =>Time.now,
                                                    :end_time =>Time.now,:without_time_flag =>"无")

        if audit_if == 'Y'
            @details.handle_status='完成'   #修改日志中的处理状态
        elsif audit_if == 'N'
            @details.handle_status='审核不通过'
        end                                            
        @details.save
    end

    def get_fault_job
        @faultJob = DTaskForm.find(params[:id])
        @maint_project=SMaintProject.find_by(:d_task_form_id => params[:id])  #关联维修项目
    end

    def params_fault_job
        params.require(:d_fault_job).permit(:fault_type,:urgent_level,:send_type,:d_station_id,
                                            :station_name,:device_name,:fault_phenomenon,:other_fault,
                                            :title,:content,:job_no,:create_type,:author,:job_status,
                                            :clraring_if)
    end

    def params_handle
        params.require(:d_fault_handle).permit(:handle_remote,:handle_type,:handle_des)
    end

    # def params_handle
    #     params.require(:d_fault_handle).permit(:id,:handle_remote,:handle_type,:handle_des,:handle_man,:handle_time,
    #                                             :handle_forms,:d_task_form_id)
    # end

end