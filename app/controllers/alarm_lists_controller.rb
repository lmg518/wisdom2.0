class AlarmListsController < ApplicationController

  def index
    @first_counts = 0
    @second_counts = 0
    @third_counts = 0
      @now_time = Time.now
      if params[:search].present?
        @search = params[:search]
        @station_ids = @search[:station_ids]
        @data_times = @search[:data_times]
        @alarm_levels = @search[:alarm_levels]
        @rule_name = @search[:rule_name]
        @status = @search[:status]
      end
      @alarms = DAlarm.by_arr_ids(@station_ids)
                            .by_search_times(@data_times)
                            .by_rule_levels(@alarm_levels)
                            .by_rule_name(@rule_name)
                            .by_status(@status)
                            .order(:last_alarm_time => :desc)
      @alarms.each do |alarm|
        @third_counts += 1   if alarm.alarm_level == 3
        @second_counts += 1   if alarm.alarm_level == 2
        @first_counts += 1   if alarm.alarm_level == 1
      end
      @alarms = @alarms.page(params[:page]).per(params[:per])
      @all_infos = {update_time: @alarms.present? ? @alarms.first.created_at.strftime('%Y-%m-%d %H:%M:%S') : "",
                  all_counts: @alarms.present? ? @alarms.total_count : 0,
                  first_counts: @first_counts,second_counts: @second_counts,
                  third_counts:  @third_counts }
  end

  def show
    @alarm_arr = []
    @alarms = DAlarm.find(params[:id])
    @alarm_details = @alarms.d_alarm_details
    @station = @alarms.d_station
    @station_noemal_data = @station.d_abnormal_data_yyyymms.where()
    @alarm_details.each do |detail|


      @alarm_arr << {station_name: "", item_name: "", item_val: "",
                     item_lable: "", alarm_name: "", alarm_level: "",
                     alarm_num: "", data_time: ""}
    end
    render json: {all_infos: @alarm_arr}
  end

end   