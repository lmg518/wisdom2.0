class SearchUnitManysController < ApplicationController
    before_action :authenticate_user
    def index
        unit = SRegionCodeInfo.find(params[:s_region_code_info_id])
        h = {}
        user = DLoginMsg.where(:s_region_code_info_id => unit.id) if unit.present?
        h = {"unit_id" =>unit.id,"unit_name" =>unit.unit_name,"user" => user.select(:id,:login_name)}
        render json:{units: h.present? ? h : {}}
    end

    def update
        @unit = DTaskForm.find_by(:id =>params[:id])
        @unit.s_region_code_info_id = params[:unit_id]
        @unit.handle_man_id = params[:handle_man_id]
        @unit.handle_man = params[:handle_man]
        @unit.job_status = "wait_deal"

        @user = @current_user  #发送人 是登录人

        respond_to do |format|
            if @unit.save
                format.json{render json:{status: "success",location:@unit}}

                #发送邮件
                #@user = DLoginMsg.find(params[:handle_man_id])
                #UserMailer.send_mail(@user,@unit).deliver_later

                #极光推送  JpushSend.send_msg("张三你好","140fe1da9e9010a3725")
                @user=DLocalize.find_by(:d_login_msg_id => params[:handle_man_id])
                if @user.present?
                    @infos= "你好：" + @user.login_name + "，您有新工单了, 工单号：" + @unit.job_no + "，请及时处理！！"
                    JpushSend.send_msg(@infos, @user.uuid)
                end
                

            else
                format.json{render json:{status: "false",location:@unit.errors}}
            end
        end
    end

end
