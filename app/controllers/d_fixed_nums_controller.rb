class DFixedNumsController < ApplicationController
  before_action :get_fixed, only: [:show,:destroy]

  def index
      begin_time = params[:begin_time]
      end_time = params[:end_time]
      item_id = params[:item_ids]
      station_ids = params[:station_ids]
      per = params[:per]
      unless per.present?
        per = 10
      end
      fixeds = hour_fixed_num(begin_time,end_time,station_ids,item_id)
      @fixed = Kaminari.paginate_array(fixeds).page(params[:page]).per(per)

  end

  def show
      time = @fixed.data_time
      time1 = (time - 3600).strftime("%Y-%m-%d %H:00:00")
      time2 = (time - 2 * 3600).strftime("%Y-%m-%d %H:00:00")
      @fi = DDataYyyymm.select(:id,:station_id,:item_code,:data_value,:data_time,:created_at)
                   .where(:data_cycle => 3600,:station_id =>@fixed.station_id,:item_code =>@fixed.item_code,data_time: time2..time.strftime("%Y-%m-%d %H:00:00") )
                    .order(:data_time => 'desc')
  end

  def get_item_code
    @item_code = SAbnormalLtem.select(:id,:item_name).where(id: [107,108])
    render json: {'item'=> @item_code}
  end


  # def export_file
  #   export_file_params
  #   handle_table
  #   @scv = CSV.generate(encoding: "GB18030") do |csv|
  #     csv << ["区域", "数量", "占比"]
  #     @alarms.each do |alarm|
  #       csv << [alarm[:region_name], alarm[:alarm_count], (alarm[:alarm_count].to_f / @alarm_total.to_f).round(2) ]
  #     end
  #   end
  #   if  @name =@data_times.present?
  #     @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "报警数量占比统计"
  #   else
  #     @name = Time.now.strftime("%Y-%m-%d-") + "报警数量占比统计"
  #   end
  #   respond_to do |format|
  #     format.csv{
  #       send_data @scv,
  #                 :type=>'text/csv; charset=GB18030; header=present',
  #                 :disposition => "attachment; filename=#{@name}.csv"
  #     }
  #   end
  # end


  private

  def get_fixed
    @fixed = DDataYyyymm.find(params[:id])
  end

  #获取定值数据
  def hour_fixed_num(begin_time,end_time,station_ids,item_id)
    if  begin_time.present? && end_time.present?
      t1 = Time.parse(begin_time).strftime("%Y-%m-%d %H:00:00")
      t2 = Time.parse(end_time).strftime("%Y-%m-%d %H:00:00")
      datas = DDataYyyymm.select(:id,:station_id,:item_code,:data_value,:data_time,:created_at)
                  .where(:data_cycle => 3600,item_code: [107,108],data_time: t1..t2 )
                  .by_d_station_ids(station_ids)
                  .by_item_id(item_id)
                  .order(:data_time =>'desc')
      arr = []
      datas.each do |d1|
        time = d1.data_time
        d_time1 = (time - 3600).strftime("%Y-%m-%d %H:00:00")
        d_time2 = (time - 2 * 3600).strftime("%Y-%m-%d %H:00:00")
        #前一个小时的数据
        d2 = DDataYyyymm.select(:id,:station_id,:item_code,:data_value,:data_time,:created_at)
                 .find_by(:station_id =>d1.station_id,:data_cycle => 3600,:item_code =>d1.item_code,:data_time => d_time1)
        #前两个小时的数据
        d3 = DDataYyyymm.select(:id,:station_id,:item_code,:data_value,:data_time,:created_at)
                 .find_by(:station_id =>d1.station_id,:data_cycle => 3600,:item_code =>d1.item_code,:data_time => d_time2)
        if d2.present? && d3.present?
          if d1.data_value == d2.data_value && d1.data_value == d3.data_value
            arr << d1
          end
        end
      end
      Rails.logger.info "arr: #{arr.length}"
      arr
    else
      time1 = (Time.now - 3600).strftime("%Y-%m-%d %H:00:00")
      data2 = DDataYyyymm.select(:id,:station_id,:item_code,:data_value,:data_time,:created_at)
                        .where(:data_cycle => 3600,item_code: [107,108],:data_time =>time1)
                        .by_d_station_ids(station_ids)
                        .by_item_id(item_id)
                        .order(:data_time =>'desc')
      arr = []
      data2.each do |d1|
        time = d1.data_time
        time2 = (time - 3600).strftime("%Y-%m-%d %H:00:00")
        time3 = (time - 2 * 3600 ).strftime("%Y-%m-%d %H:00:00")
        d2 = DDataYyyymm.select(:id,:station_id,:item_code,:data_value,:data_time,:created_at)
                 .find_by(:station_id =>d1.station_id,:data_cycle => 3600,:item_code =>d1.item_code,:data_time =>time2)
        d3 = DDataYyyymm.select(:id,:station_id,:item_code,:data_value,:data_time,:created_at)
                 .find_by(:station_id =>d1.station_id,:data_cycle => 3600,:item_code =>d1.item_code,:data_time =>time3)
        #if d1.present? && d2.present?
        if d1.present? && d2.present? && d3.present?
          if d1.data_value == d2.data_value && d1.data_value == d3.data_value
            arr << d1
          end
        end
      end
      Rails.logger.info "arr: #{arr.length}"
      arr
    end
  end


  # def export_file_params
  #   @station_ids = params[:station_ids]
  #   @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
  #   @alarm_levels = params[:alarm_levels]
  # end
  #
  # def search_parmas
  #   if params[:search].present?
  #     @search = params[:search]
  #     @region_leve = @search[:region_level]
  #     @data_times = @search[:data_times]
  #     @alarm_levels = @search[:alarm_levels]
  #   end
  # end
  #
  # def handle_table
  #   @alarms = []
  #   @alarm_total = 0
  #   @region_leves = @region_leves = SRegionLevel.select(:id, :level_name)
  #   @region_codes = SRegionCode.by_region_level(@region_leve)
  #   @region_codes.each do |region|
  #     begin
  #       @alarm_count = 0
  #       stations = region.d_stations
  #       stations.each do |station|
  #         @alarm_count += station.d_alarms.by_alarm_times(@data_times).by_rule_levels(@alarm_levels).length
  #       end
  #       next if @alarm_count == 0
  #       @alarm_total += @alarm_count
  #       @alarms << {region_name: region.region_name, alarm_count: @alarm_count}
  #       # rescue Exception => e
  #       #   next
  #     end
  #   end
  # end



end

