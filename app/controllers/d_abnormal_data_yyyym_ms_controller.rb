class DAbnormalDataYyyymMsController < ApplicationController
  before_action :set_d_abnormal_data_yyyymm, only: [:show, :edit, :update, :destroy]

  # GET /d_abnormal_data_yyyymms
  # GET /d_abnormal_data_yyyymms.json
  def index
    @d_abnormal_data_yyyymms = DAbnormalDataYyyymm.all
  end

  # GET /d_abnormal_data_yyyymms/1
  # GET /d_abnormal_data_yyyymms/1.json
  def show
  end

  # GET /d_abnormal_data_yyyymms/new
  def new
    @d_abnormal_data_yyyymm = DAbnormalDataYyyymm.new
  end

  # GET /d_abnormal_data_yyyymms/1/edit
  def edit
  end

  # POST /d_abnormal_data_yyyymms
  # POST /d_abnormal_data_yyyymms.json
  def create
    @d_abnormal_data_yyyymm = DAbnormalDataYyyymm.new(d_abnormal_data_yyyymm_params)

    respond_to do |format|
      if @d_abnormal_data_yyyymm.save
        format.html { redirect_to @d_abnormal_data_yyyymm, notice: 'D abnormal data yyyymm was successfully created.' }
        format.json { render :show, status: :created, location: @d_abnormal_data_yyyymm }
      else
        format.html { render :new }
        format.json { render json: @d_abnormal_data_yyyymm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /d_abnormal_data_yyyymms/1
  # PATCH/PUT /d_abnormal_data_yyyymms/1.json
  def update
    respond_to do |format|
      if @d_abnormal_data_yyyymm.update(d_abnormal_data_yyyymm_params)
        format.html { redirect_to @d_abnormal_data_yyyymm, notice: 'D abnormal data yyyymm was successfully updated.' }
        format.json { render :show, status: :ok, location: @d_abnormal_data_yyyymm }
      else
        format.html { render :edit }
        format.json { render json: @d_abnormal_data_yyyymm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /d_abnormal_data_yyyymms/1
  # DELETE /d_abnormal_data_yyyymms/1.json
  def destroy
    @d_abnormal_data_yyyymm.destroy
    respond_to do |format|
      format.html { redirect_to d_abnormal_data_yyyymms_url, notice: 'D abnormal data yyyymm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_d_abnormal_data_yyyymm
      @d_abnormal_data_yyyymm = DAbnormalDataYyyymm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def d_abnormal_data_yyyymm_params
      params.require(:d_abnormal_data_yyyymm).permit(:abnormal_id, :station_id, :rule_instance, :create_acce, :ab_lable, :ab_data_time, :ab_value, :compare_value, :compare_value2, :compare_value3, :ab_desc)
    end
end
