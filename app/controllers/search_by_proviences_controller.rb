class SearchByProviencesController < ApplicationController

  def index
    @admin_proviences=  SAdministrativeArea.roots
    render json:{ proviences: @admin_proviences.select("id","zone_name")}
  end
end
