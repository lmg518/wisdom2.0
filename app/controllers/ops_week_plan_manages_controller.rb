class OpsWeekPlanManagesController < ApplicationController
  before_action :get_ops_params, only:[:show]

  #GET /ops_week_plan_manages
  #GET /ops_week_plan_manages.json
  def index
  end

  #GET /ops_week_plan_manages/id
  #GET /ops_week_plan_manages/id.json
  def show
    @details = @ops_plan.d_ops_plan_details.where(:job_flag =>'7')
  end

  private
    def get_ops_params
      @ops_plan = DOpsPlanManage.find(params[:id])
    end
end
