class DevelopingController < ApplicationController


  #临时 首页  开发中提示

  def index
  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  private
  def set_work
    @work = DWorkPro.find(params[:id])
  end

  def work_params
    params.require(:d_work_pro).permit(:id, :work_name, :work_code, :work_type, :work_flag)
  end

end
