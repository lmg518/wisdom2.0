class HandleEquipsController < ApplicationController
  before_action :get_handle_equip, only: [:show, :edit, :update, :issued, :carried_out, :destroy]
  before_action :authenticate_user
  #设备检修管理

  def index
    #车间级的人员只看自己车间的设备信息
    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    if @region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      region_z = SRegionCode.where(:s_region_level_id => 3)
      region_code_ids = region_z.pluck(:id)  #查询所有车间
    end

    if @region_code.s_region_level_id == 2  #2 分公司   1 总公司   3 车间
      region_f = SRegionCode.where(:father_region_id => @region_code.id, :s_region_level_id => 3)
      region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
    end

    if @region_code.s_region_level_id == 3  #2 分公司   1 总公司   3 车间
      region_code_ids = @region_code.id
    end

    get_region

    #机修人员只看自己的工单
    if @current_user.s_role_msg.role_code == 'mechanic_repair'
      @handle_equips = HandleEquip.where(:s_region_code_id => region_code_ids,:handle_name => @current_user.login_name).by_equip(params[:equip_ids])
                                .by_handle_time(params[:begin_time], params[:end_time])
                                .by_handle_status(params[:status])
                                .order(:created_at => 'desc')
                                .page(params[:page]).per(params[:per])
    else
      @handle_equips = HandleEquip.where(:s_region_code_id => region_code_ids).by_equip(params[:equip_ids])
                                .by_handle_time(params[:begin_time], params[:end_time])
                                .by_handle_status(params[:status])
                                .order(:created_at => 'desc')
                                .page(params[:page]).per(params[:per])
    end

  end

  def new
    get_equip
    @handle_equip = HandleEquip.new()
  end

  def create
    handle_equip = HandleEquip.new(handle_equip_params)
    handle_equip.status = 'wait_issued'
    handle_equip.founder = @current_user.login_name
    respond_to do |format|
      if handle_equip.save
        # format.html {redirect_to handle_equips_path}
        format.json {render json: {status: 200}}
      else
        format.html {}
        format.json {render json: {status: 'error'}}
      end
    end
  end

  #get /handle_equips/id
  def show
    #填写检修记录，更换备件什么都在show页面，看有没有检修前、后记录和更换备件记录
    h_overhaul_notes = @handle_equip.h_overhaul_notes
    if h_overhaul_notes
      start_note = h_overhaul_notes.find_by(:note_type => 'overhaul_begin')
      if start_note.present?
        @start_note = {id: start_note.id, note: start_note.note, img_path: start_note.img_path,
                       overhaul_desc: start_note.overhaul_desc, note_type: start_note.note_type,
                       overhaul_time: start_note.overhaul_time ? start_note.overhaul_time.strftime("%Y-%m-%d %H:%M:%S") : ''
        }
      else
        @start_note = {}
      end
      end_note = h_overhaul_notes.find_by(:note_type => 'overhaul_end')
      if end_note.present?
        @end_note = {id: end_note.id, note: end_note.note, img_path: end_note.img_path, sort_out: end_note.sort_out,
                     overhaul_desc: end_note.overhaul_desc, note_type: end_note.note_type,
                     overhaul_time: end_note.overhaul_time ? end_note.overhaul_time.strftime("%Y-%m-%d %H:%M:%S") : ''
        }
      else
        @end_note = {}
      end
    end
    #更换的备件信息
    spare = @handle_equip.h_equip_spare
    if spare.present?
      @spares = {id: spare.id, spare_name: spare.spare_name, change_time: spare.change_time ? spare.change_time.strftime("%Y-%m-%d %H:%M:%S") : ''}
    end
    #试车检查项
    boot_s = @handle_equip.h_equip_boots
    boots = []
    if boot_s.present?
      boot_s.each{|b| boots << {id: b.id,handle_equip_id: b.handle_equip_id, boot_check_id: b.boot_check_id, check_project: b.check_project}}
      @equip_boots = {boots: boots,check_status: boot_s[0].check_status,exec_desc: boot_s[0].exec_desc }
    else
      @equip_boots = {boots: [],check_status: '',exec_desc: ''}
    end

    
  end

  def edit

  end

  def update
    if @handle_equip.status == 'wait_issued'
      respond_to do |format|
        if @handle_equip.update(handle_equip_params)
          format.html {redirect_to handle_equips_path}
          format.json {render json: {status: 200}}
        else
          format.html {}
          format.json {render json: {status: 'error'}}
        end
      end
    end
  end

  #下发操作
  #post /handle_equips/issued
  def issued
    if @handle_equip.status == 'wait_issued'
      if @handle_equip.update(:handle_name => params[:handle_name], :status => 'wait_carried')
        #极光推送 提醒机修人员
        content = '你有新的检修记录要处理，请查看'
        delay_time = ((@handle_equip.handle_start_time - Time.now) / 60).to_i  #延迟时间
        d_login_msg = DLoginMsg.find_by(:login_name => params[:handle_name])
        if d_login_msg.present? && d_login_msg.uuid.present?
          #SidekiqJpushSingleWorker.perform_async(content,d_login_msg.uuid,'JX')
          SidekiqJpushSingleWorker.perform_in(delay_time.minutes, content,d_login_msg.uuid,'JX')  #延迟推送
        end

        render json: {status: 200}
      else
        render json: {status: 'error'}
      end
    end
  end

  #执行操作
  #post /handle_equips/carried_out
  def carried_out
    #-------填写检修前记录----------
    if params[:start_note_type]
      note = params[:start_note].present? ? params[:start_note] : ''
      note_type = params[:start_note_type]
      overhaul_desc = params[:start_overhaul_desc] ? params[:start_overhaul_desc] : ''
      overhaul_time = params[:start_overhaul_time]
      image_file = params[:start_image_file]
      create_overhaul(@handle_equip, note, note_type, overhaul_desc, overhaul_time, '', image_file)
    end
    #--------填写检修后记录---------
    if params[:end_note_type]
      note = params[:end_note].present? ? params[:end_note] : ''
      note_type = params[:end_note_type]
      overhaul_desc = params[:end_overhaul_desc] ? params[:end_overhaul_desc] : ''
      overhaul_time = params[:end_overhaul_time]
      sort_out = params[:end_sort_out]
      image_file = params[:end_image_file]
      create_overhaul(@handle_equip, note, note_type, overhaul_desc, overhaul_time, sort_out, image_file)
    end
    #更换备件
    spare = @handle_equip.h_equip_spare
    if spare
      if params[:spare_name] && params[:change_time]
        spare.update(:spare_name => params[:spare_name], :change_time => params[:change_time])
      end
    else
      HEquipSpare.create(:handle_equip_id => @handle_equip.id, :spare_name => params[:spare_name],
                         :change_time => params[:change_time])
    end
    #试车记录
    boots = @handle_equip.h_equip_boots
    if boots
      if boots.destroy_all
        boot_ids = params[:boot_ids]
        if boot_ids
          boot_ids.each do |b|
            boot = BootCheck.find(b.to_i).check_project
            HEquipBoot.create(:handle_equip_id => @handle_equip.id, :boot_check_id => b.to_i,
                              :check_project => boot, :check_status => params[:check_status], :exec_desc => params[:exec_desc],
                              :handle_time => params[:handle_time])
          end
        end
      end
    end
    #改变设备检修单状态
    if params[:image_file]
      image_file = params[:image_file]
      img_flag = "jpeg" if image_file[0, 15] == "data:image/jpeg"
      img_flag = "png" if image_file[0, 14] == "data:image/png"
      img_file = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
      img_name = "#{@handle_equip.id}.#{img_flag}"
      file_path = "#{Rails.root}/public/test/handle/"
      FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
      #向dir目录写入文件
      File.open(Rails.root.join("#{file_path}", img_name), 'wb') {|f| f.write(img_file)}
      img_path = "test/handle/#{img_name}"
      @handle_equip.update(:img_path => img_path)
    end
    if params[:status]
      @handle_equip.update(:status => params[:status])
    end
    render json: {status: 200}
  end

  #获取备件信息
  def get_spare
    @spares = SSpare.select(:id, :spare_name, :spare_code).where(:s_equip_id => params[:id])
    render json: {spares: @spares}
  end

  #获取试车检查项
  def get_boots
    @boots = BootCheck.select(:id, :check_project)
    render json: {boots: @boots}
  end
#  handle_equips/1
#  handle_equips/1.json    
  def destroy
    
    if @handle_equip.destroy
      render json: {status: 200}
    else
      render json: {status: 'error'}
    end
    # respond_to do |format|
    #   format.html {}
    #   format.json {render json: {status: :ok} }
    # end
  end


  #获取人员
  #get /handle_equips/get_login  @current_user.s_role_msg.role_code == 'mechanic_repair'
  def get_login
    #按车间查询人员
    region_id = params[:region_id]
    role_msg = SRoleMsg.find_by(:role_code => 'mechanic_repair')
    @logins = DLoginMsg.select(:id, :login_name).where(:s_role_msg_id => role_msg.id,:group_id => region_id) #只查当前车间下的机修人员
    render json: {login: @logins}
  end

  private

  def get_handle_equip
    @handle_equip = HandleEquip.find(params[:id])
  end

  def handle_equip_params
    params.require(:handle_equip).permit(:s_region_code_id, :region_name, :s_equip_id, :equip_name, :equip_code,
                                         :handle_start_time, :handle_end_time, :snag_desc, :desc)
  end


  #上传图片
  def upload_img(image_file, equip)
    if image_file.present?
      img_flag = "jpeg" if image_file[0, 15] == "data:image/jpeg"
      img_flag = "png" if image_file[0, 14] == "data:image/png"
      img_file = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
      img_name = "#{equip.id}.#{img_flag}"
      file_path = "#{Rails.root}/public/test/equip/"
      FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
      #向dir目录写入文件
      File.open(Rails.root.join("#{file_path}", img_name), 'wb') {|f| f.write(img_file)}
      return "test/equip/#{img_name}"
    end
  end

  #创建检修记录
  def create_overhaul(handle_equip, *arg)
    note = arg[0]
    note_type = arg[1]
    overhaul_desc = arg[2]
    overhaul_time = arg[3]
    sort_out = arg[4] ? arg[4] : ''
    image_file = arg[5]
    h_overhaul = handle_equip.h_overhaul_notes.find_by(:note_type => note_type)
    #判断是否存在检修记录
    if h_overhaul.blank?
      note1 = HOverhaulNote.create(:handle_equip_id => handle_equip.id, :note => note,
                                   :note_type => note_type, :overhaul_desc => overhaul_desc,
                                   :overhaul_time => overhaul_time, :sort_out => sort_out)
      #获取到上传的图片路径
      img_path = upload_img(image_file, note1)
      note1.img_path = img_path
      note1.save
    else
      #先删除原来的图片
      delete_img(h_overhaul)
      #获取到上传的图片路径
      img_path = upload_img(image_file, h_overhaul)
      h_overhaul.update(:note => note, :overhaul_desc => overhaul_desc,
                        :overhaul_time => overhaul_time, :sort_out => sort_out,
                        :img_path => img_path)
    end
  end

  #查询接口数据------获取车间
  def get_region
    # puts "group_id: #{@current_user.group_id}"
    # region_code = SRegionCode.find_by(:id => @current_user.group_id)
    # father_region = SRegionCode.find_by(:father_region_id == 99999)
    # if region_code.father_region_id == 99999
    #   @region_code = SRegionCode.select(:id, :region_name).where("father_region_id != 99999", :s_region_level_id =>3)
    # elsif region_code.father_region_id == father_region.id
    #   @region_code = SRegionCode.select(:id, :region_name).where(:father_region_id => region_code.id, :s_region_level_id =>3)
    # else
    #   @region_code = SRegionCode.select(:id, :region_name).where(:father_region_id => region_code.father_region_id, :s_region_level_id =>3)
    # end

    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    if region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      @region_code = SRegionCode.select(:id, :region_name).where(:s_region_level_id =>3)
    elsif region_code.s_region_level_id == 2
      @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id =>region_code.id, :s_region_level_id =>3)
    elsif region_code.s_region_level_id == 3
      @region_code = SRegionCode.select(:id,:region_name).where(:id =>region_code.id, :s_region_level_id =>3)
    else
      @region_code = ''
    end

  end

  #对上传的图片和记录进行删除
  def delete_img(arg)
    if arg.img_path.present?
      File::unlink("#{Rails.root}/public/#{arg.img_path}")
    end
  end

end