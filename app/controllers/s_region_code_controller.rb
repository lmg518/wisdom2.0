class SRegionCodeController < ApplicationController
  before_action :get_region_code, only: [:show, :update, :edit, :destroy]

  #GET /s_region_code
  #GET /s_region_code.json
  def index
    @units = SRegionCodeInfo.page(params[:page]).per(params[:per])

    #按条件查询
    @unit_name = params[:unit_name]
    @region_code = params[:region_code]
    if @unit_name.present? || @region_code.present?
      @units = SRegionCodeInfo.by_unit_name(@unit_name).by_region_code(@region_code).page(params[:page]).per(params[:per])

    end
  end

  #GET /s_region_code/new
  def new
    @unit = SRegionCodeInfo.new()
  end

  #POST /s_region_code
  #POST /s_region_code.json
  def create
    @unit = SRegionCodeInfo.new(params_units)
    respond_to do |format|
      if @unit.save
        station_ids = @unit.station_id.present? ? @unit.station_id.split(",") : []
        station_ids.each do |id|
         station = DStation.find_by(:id => id)
         next if station.blank?
         station.update(:s_region_code_info_id => @unit.id)
        end
        format.html {redirect_to "/s_region_code"}
        format.json {render json: {status: "success", location: @unit}}
      else
        format.html {render :new}
        format.json {render json: {status: "false", location: @unit.errors}}
      end
    end
  end

  #DELETE /s_region_code/id
  #DELETE /s_region_code/id.json
  def destroy
    respond_to do |format|
      if @unit.destroy
        format.html {redirect_to '/s_region_code'}
        format.json {render json: {status: "success"}}
      else
        format.html {redirect_to "/s_region_code"}
        format.json {render json: {status: "false", location: @unit.errors}}
      end
    end
  end

  #GET /s_region_code/id/edit
  def edit
  end

  #PUT /s_region_code/id
  #PUT /s_region_code/id.json
  def update
    respond_to do |format|
      if @unit.update(params_units)
        station_ids = @unit.station_id.present? ? @unit.station_id.split(",") : []
        station_ids.each do |id|
          station = DStation.find_by(:id => id)
          next if station.blank?
          station.update(:s_region_code_info_id => @unit.id)
        end
        format.html {redirect_to "/s_region_code"}
        format.json {render json: {status: "success", location: @unit}}
      else
        format.html {render :edit}
        format.json {render json: {status: "success", location: @unit.errors}}
      end
    end
  end

  #GET /s_region_code/56
  #GET /s_region_code/56.json
  def show
  end

  private

  def get_region_code
    @unit = SRegionCodeInfo.find(params[:id])
  end

  def params_units
    params.require(:unit).permit(:id, :unit_name, :unit_code, :s_region_code_id, :linkman,
                                 :linkman_tel, :unit_address, :status, :station_id)
  end
end