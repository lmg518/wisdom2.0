class CompanyMonthReportsController < ApplicationController
  before_action :authenticate_user
  
  #公司生产月报表

  def index
    #@daily_reports=DDailyReport.all

    #获取查询参数
    #params_month = params[:month]

    params_region_ids = params[:region_id]
    @month_begin1 = params[:month_begin]  
    @month_end1 = params[:month_now]

    @page1 = params[:page]
    @per1 = params[:per]


    # @page = 1
    # @per = 10


    if @page1.present? && @per1.present?
      @page = @page1
      @per = @per1
    else
      @page = 1
      @per = 10
    end

    Rails.logger.info "-------#{@page}-----"
    Rails.logger.info "-------#{@per}-----"


    if @month_begin1.present? && @month_end1.present?
      month_begin = @month_begin1 + '-1'
      dd1 = Time.parse(@month_end1 + '-1')
      month_end = dd1.end_of_month.strftime("%Y-%m-%d")
      @time_begion = month_begin #列表显示的月份
      time = month_end
    else
      @time_begion=Time.now.beginning_of_month.strftime("%Y-%m")
      time = Time.now.strftime("%Y-%m-%d")
    end


    Rails.logger.info "---month_begin--#{@time_begion}-----"   # 2018-01-1
    Rails.logger.info "---month_begin--#{time}-----"      # 2018-03-31



    #根据登录人员 获取人员所在的车间 或公司
    @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id)  #获取人员所在车间 或公司


    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1   #2 分公司   1 总公司   3 车间
      if params_region_ids.present?   #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id)  #查询分公司下的的所有车间
      end

    elsif @region_code1.s_region_level_id == 2
      if params_region_ids.present?   #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_f = SRegionCode.where(:father_region_id => @region_code1.id)
        region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 3
      if params_region_ids.present?   #使用查询的ids
        region_code_ids = params_region_ids
      else
        @region_code = @region_code1
        region_code_ids = @region_code.id
      end
    end

    Rails.logger.info "-region_code_ids----#{region_code_ids}-------"


    #折线图数据
    @daily_reports2 = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                                    .by_datetime(@time_begion, time)
                                    .order(:created_at => :desc)


    #@sum_dates = DDailyReport.by_datetime(@time_begion, time).where(:s_region_code_id =>region_code_ids, :status => 'Y')

    #获取当前月份
    @months=[]
    @m2 =[]
    @m22 =[]   #折现图使用
    @months = @daily_reports2.pluck(:datetime).uniq
    Rails.logger.info "-months----#{@months}-------"
    @months.each do |m|
      t1 = Time.parse(m).strftime("%Y-%m") +'-1'
      @m2.push(t1)
    end
    @months.each do |m|
      t1 = Time.parse(m).strftime("%Y-%m")
      @m22.push(t1)
    end
    @m3 = @m2.uniq.sort   #带 日的
    @m33 = @m22.uniq.sort   #折现图使用  不带 日的

    Rails.logger.info "-----#{@m3}-------"    #["2018-03-1", "2018-04-1"]
    Rails.logger.info "-----#{@m33}-------"   #["2018-03", "2018-04"]

    @ids = DMaterialReginCode.where(:s_region_code_id => region_code_ids).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids)   #从材料表中获取

    @date2 =[]   #自己封装多个月的数据
    @m33.each do |m|
      @plants.each do |p|
      item={}
      item["month"] = m
      code1 = DMaterialReginCode.find_by(:s_material_id => p.id)
      code = SRegionCode.find_by(:id => code1.s_region_code_id)
      #判断是车间级  分公司级  还是总公司级
      @region_level = SRegionLevel.find_by(:id => code.s_region_level_id)
      if @region_level.level_name == '车间'
          region_name = SRegionCode.find_by(:id => code.father_region_id).region_name
          region_code = code
      elsif @region_level.level_name == '分公司'
          
      elsif @region_level.level_name == '总公司'
  
      end
      item["region_code_info"] = region_name  #分公司

      #根据 项目  查找 车间
      relationcode = DMaterialReginCode.find_by(:s_material_id => p.id)
      region_code = SRegionCode.find_by(:id => relationcode.s_region_code_id)
      item["region_code"] = region_code.region_name  #车间
      item["name"] = p.name  #车间
      
      #查询关系类型
      #@relation_id = DMaterialReginCode.find_by(:s_material_id => p.id)
      @d_relation_type = DRelationType.find_by(:id => relationcode.d_relation_type_id)
      item["relation_type"] = @d_relation_type.name  #消耗/产出


      #计算累计量  要使用@m33中月份的最后一天作为结束条件
      @m_end = Time.parse(m + '-1').end_of_month.strftime("%Y-%m-%d")
      nums= DDailyReport.by_datetime(m, @m_end).where(:s_region_code_id => region_code_ids, :name => p.name).pluck(:nums)

      sum_nums = 0
      if nums.length > 0
          nums.each do |n|
              sum_nums += n
          end
      end
      item["sum_nums"] = sum_nums  #累计产量
      @date2.push(item)
     end
    end

    @date2 = @date2.sort_by{|d| d["region_code_info"]}
    
    #-----------------------------------

    #列表数据 需要分页
    # @daily_reports = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y').uniq
    #                               .by_region_ids(params_region_ids)     #按车间查询
    #                               .by_datetime(month_begin,month_end)  #按月查询
    #                               .order(:created_at => :desc)
    #                               .page(params[:page]).per(params[:per])


#-----------------------------

    #折现图数据
    # time=Time.now
    # day_begin = time.beginning_of_month.strftime("%Y-%m-%d")
    # day_now = time.strftime("%Y-%m-%d")

    #查询当前车间  各个项目的 月数据
    #根据登录人员 获取人员所在的车间 或公司
    @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    #车间项目 信息
    @month_days=[]
    #获取最小查询日期
    min_day = @m3.min

      # time1 =Time.now   #查询 1-3月分时有问题
      # time2 =time1.end_of_month.strftime("%Y-%m-%d")


      @plants.each do |p|
        item={}
        item["name"] = p.name
        @data=[]  #存3 月  4月的总量
        @m3.each do |m|

          #要使用@m3中月份的最后一天作为结束条件
          m_end = Time.parse(m).end_of_month.strftime("%Y-%m-%d")
          nums= DDailyReport.by_datetime(m, m_end).where(:s_region_code_id => region_code_ids, :name => p.name).pluck(:nums)
          
          Rails.logger.info "-----m_end-----#{m_end}-----"
          Rails.logger.info "---nums-------#{nums}-----"

          sum_nums = 0
          if nums.length > 0
            nums.each do |n|
              sum_nums += n
            end
          else
            sum_nums = nil
          end
          @data.push(sum_nums)
        end
        item["data"] = @data   #必须是数组
        @month_days.push(item)
      end




    #分页
    #数据长度
    @data_length = @date2.length
    #分页数据
    @date2 = Kaminari.paginate_array(@date2).page(@page).per(@per)
    #页数
    @page_num = (@data_length%(@per.to_i)) == 0 ? @data_length/(@per.to_i) : @data_length/(@per.to_i) + 1


    Rails.logger.info "------@date2----#{@date2}----"

    #end


  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    @work=DWorkPro.new
  end

  #成功跳转到show页面
  def create
    @work = DWorkPro.new(work_params)
    respond_to do |format|
      if @work.save
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Successfully create!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :new}
      end
    end
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @work.destroy
        format.html {redirect_to work_pros_path, notice: 'Successfully destroy!'}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @work.update(work_params)
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    @work = DWorkPro.find(params[:id])
  end

  def work_params
    params.require(:d_work_pro).permit(:id, :work_name, :work_code, :work_type, :work_flag)
  end

end
