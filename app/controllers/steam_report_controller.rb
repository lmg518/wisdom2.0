class SteamReportController < ApplicationController
  before_action :authenticate_user

  #蒸汽上报
  def steam_report
    get_datas
    get_form_values
    render :partial => "steam_report", :layout => false
  end

  #获取表格项目数据
  def get_datas
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => @region_code.id) #表格信息

    #表格头信息
    @form_name = @d_report_forms.name #表格名称
    @form_headers_ids = DReportFormHeader.where(:d_report_form_id => @d_report_forms.id).pluck(:d_form_header_id) #获取表头信息id
    Rails.logger.info @form_headers_ids.inspect
    #@form_headers = DFormHeader.where(:id =>@form_headers_ids)  #表头信息  无法排序
    @form_headers = []
    @form_headers_ids.each do |id|
      @fd = DFormHeader.find_by(:id => id)
      name = @fd.present? ? @fd.name : ''
      @form_headers.push(name)
    end
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",") #报表类型id
    @ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_material_id) #根据类型id查找
    @form_body1 = SMaterial.where(:id => @ids) #从材料表中获取
    @form_body=[]
    @form_body1.each do |f|
      item ={}
      item["code"] = f.code
      item["name"] = f.name
      steam_region_code = DSteamRegionCode.find_by(:s_material_id => f.id) #蒸汽项目与车间 类型的关联
      region_code_id = steam_region_code.s_region_code_id
      region_code = SRegionCode.find_by(:id => region_code_id)
      item["region_name"] = region_code.region_name #车间信息
      item["region_id"] = region_code.id #车间id
      nenrgy_type_id = steam_region_code.s_nenrgy_form_type_id #表格类型id
      nenrgy_type = SNenrgyFormType.find_by(:id => nenrgy_type_id)
      item["type_name"] = nenrgy_type.name #类型
      @form_body.push(item)
    end
    # Rails.logger.info "------@form_body------#{@form_body}-------"
    # Rails.logger.info @form_body.inspect
    # Rails.logger.info "------@form_type------#{@form_type}-------"
    # Rails.logger.info "------@d_report_forms------#{@d_report_forms}-------"
    # Rails.logger.info "------@d_report_forms------#{@type_name}-------"
  end


  def get_form_values
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    time = Time.now.strftime("%Y-%m-%d")
    #根据人员车间 找报表id
    @d_report_form = DReportForm.find_by(:s_region_code_id => @region_code.id)
    report_form_id = @d_report_form.id #报表id
    @form_values = SNenrgyValue.where(:d_report_form_id => report_form_id, :datetime => time)
    #日报状态
    reports = @form_values.where(:status => 'Y') if @form_values.present?
    @status = reports.present? && reports.length >0 ? '已上报' : '未上报'
    # Rails.logger.info '=========1111111111=========='
    # Rails.logger.info @form_values.inspect
  end


  def index
  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
  end

  #成功跳转到show页面
  #保存 操作  还能修改

  # POST  steam_report
  def create
   # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id) #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    @d_daily_reports = params[:energy_params]
    status = params[:status] #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:id] #报表id
    @d_daily_reports.each do |d|
      #判断是否已经存在当天的日报
      @day_report = SNenrgyValue.find_by(:s_region_code_id => d[1]["region_id"], :datetime => date, :field_code => d[1]["field_code"])
      if @day_report.present?
        @day_report.update(:field_value => d[1]["field_value"].to_f, :status => status)
      else
        @d_daily_report = SNenrgyValue.new
        @d_daily_report.field_value = d[1]["field_value"].to_f
        @d_daily_report.field_code = d[1]["field_code"]
        @d_daily_report.datetime = date
        @d_daily_report.status = status
        #@d_daily_report.s_region_code_id = @region_code.id   #车间id
        @d_daily_report.s_region_code_id = d[1]["region_id"] #车间id  要存到各个车间下，不能存到登录人员的region_id
        @d_daily_report.d_report_form_id = d_report_form_id #报表id
        @d_daily_report.save
      end
    end
    render json: {status: 'success'}
  end

  #上报   不能再修改
  def report
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work

  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
