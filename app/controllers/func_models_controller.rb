class FuncModelsController < ApplicationController
  before_action :current_user
  before_action :get_func_msgs, only: [:modal_form,:show,:update,:destroy]

  def index
    # @funcs = SFuncMsg.where(:father_func_code => 9999).order(:queue_index)
  end

  def table_infos
    # @funcs = SFuncMsg.order(:created_at => :desc)
    @father = []
    @father_id = params[:obj_id]
    if @father_id.present?
      s_func_msg = SFuncMsg.find_by(:id => @father_id)
      @father << s_func_msg if s_func_msg.present? && s_func_msg.father_func_id == 9999
    end
    @funcs = SFuncMsg.s_by_father_id(@father_id).order(:queue_index => :desc)
    render :partial => 'table_infos', :layout => false
  end

  def show
    @funcs = SFuncMsg.where(:father_func_code => @func_msg.func_code)
    render :partial => "show_infos", :layout => false
  end

  def modal_form
    render :partial => "modal_form", :layout => false
  end

  def new
    render :partial => "new_form", :layout => false
  end

  def update
    respond_to do |format|
      if @func_msg.update(func_model_params)
        if @func_msg.father_func_id == 9999
          @func_msg.update(:valid_flag => '1')
        else
          @func_msg.update(:valid_flag => '0')
        end
        format.json { render json: {status: :ok, location: @func_msg} }
      else
        format.json { render json: @func_msg.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @func_msg = SFuncMsg.new(func_model_params)
    if @func_msg.father_func_id == 9999
       @func_msg.valid_flag = '1'
    else
      @func_msg.valid_flag = '0'
    end
    respond_to do |format|
      if @func_msg.save
        format.json { render json: {status: :ok, location: @func_msg} }
      else
        format.json { render json: @func_msg.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @func_msg.destroy
        format.json { render json:{ status: :ok } }
      else
        format.json { render json: @func_msg.errors, status: :unprocessable_entity }
      end
    end
  end


  private
  def get_func_msgs
    @func_msg = SFuncMsg.find(params[:id])
  end

  def func_model_params
    params.require(:s_func_msg).permit(:func_code, :func_name, :father_func_id, :func_url, :root_distance, :queue_index)
  end

end
