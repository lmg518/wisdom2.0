class CreateStationPlanController < ApplicationController

  #新增站点 时 手动创建计划
  def index
    t_fitst_day = Time.now.beginning_of_month  #
    w_month_end_day=(Time.now + (-1.month)).end_of_month.wday   #上月最后一天是周几

    if w_month_end_day == 0  #如果是周日 从当前周开始
      week_first_day = t_fitst_day.beginning_of_week
      week_end_day = t_fitst_day.end_of_week
    else
      week_first_day = t_fitst_day.beginning_of_week + 1.week
      week_end_day = t_fitst_day.end_of_week + 1.week
    end

    Rails.logger.info "-----3--#{week_first_day}-------"
    Rails.logger.info "-----4--#{week_end_day}-------"

    #已生成计划的站点id
    ops_plan_station_ids = DOpsPlanManage.where(:week_begin_time => week_first_day, :week_end_time => week_end_day).pluck(:d_station_id)
    Rails.logger.info "-----5--#{ops_plan_station_ids}-------"
    @stations=DStation.active.where.not(:id => ops_plan_station_ids).by_station_ids(params[:station_ids]).page(params[:page]).per(params[:per])
  end

  def show
  end


  def create_plan
    month=params[:month]   #0  本月   1 下月
    station=DStation.where(:id => params[:id])
    flag=false

    if month == '0'
      MonthWorkPlanJob.new.perform2('0', station)   #创建本月计划
      MonthWorkPlanJob.new.perform2('1', station)   #创建下月计划
      flag=true
    # elsif  month == '1'
    #   MonthWorkPlanJob.new.perform2(month, station)
    #   flag=true
    end

    respond_to do |format|
      if flag
        format.json {render json: {status: '本月计划、下月计划已生成完毕'}}
        format.html {}
      else
        format.json {render json: {status: '计划生成失败'}}
        format.html {}
      end
    end

  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @work.update(work_params)
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :edit}
      end
    end

  end


end
