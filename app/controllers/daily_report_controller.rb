class DailyReportController < ApplicationController
  before_action :authenticate_user
  #车间生产上报

  def index
    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司

    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    #车间项目 信息
    #@plants = @region_code.d_plants  DMaterialReginCode
    @ids = DMaterialReginCode.where(:s_region_code_id => @region_code.id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids)   #从材料表中获取

    #当天的 日报表
    time = Time.now.strftime("%Y-%m-%d")
    @daily_report = DDailyReport.where(:datetime => time, :s_region_code_id => @region_code.id)

  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    @d_daily_report=DDailyReport.new
  end

  #成功跳转到show页面
  #保存 操作  还能修改

  # POST  daily_report
  def create

    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    @d_daily_reports = params[:d_daily_reports]
    status = params[:status]   #  Y 上报   N 保存
    date = params[:date_time]
    #date_time = date.strftime("%Y%m%d")

    @d_daily_reports.each do |d|

    # Rails.logger.info "-------#{d}-------"
    # Rails.logger.info "-------#{d[0]}-------"
    # Rails.logger.info "-------#{d[1]}-------"

    #判断是否已经存在当天的日报
    @day_report = DDailyReport.find_by( :s_region_code_id => @region_code.id, :datetime => date, :name => d[1]["name"] )
    if @day_report.present?
      @day_report.update(:nums => d[1]["nums"], :status => status, :daily_yield => d[1]["daily_yield"] )
    else
      @d_daily_report = DDailyReport.new
      @d_daily_report.name = d[1]["name"]
      @d_daily_report.nums = d[1]["nums"]
      @d_daily_report.s_region_code_id = @region_code.id
      @d_daily_report.datetime = date
      @d_daily_report.status = status

      #根据id 获取code
      s_material = SMaterial.find_by(:id => d[1]["id"])
      @d_daily_report.s_material_id = s_material.id  #物料项目id
      @d_daily_report.code = s_material.code         #物料项目code
      
      @d_daily_report.daily_yield = d[1]["daily_yield"] #日收率
      @d_daily_report.save
    end

  end
    render json: {status: 'success'}
    
    # respond_to do |format|
    #   if @d_daily_report.save
    #     format.json {render json: {status: 'success', location: @work}}
    #     format.html {}
    #   else
    #     format.json {render json: {status: 'false', location: @work.errors}}
    #     format.html {render :new}
    #   end
    # end
  end

  #上报   不能再修改
  def report
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    @d_daily_reports = params[:d_daily_reports]
    date = params[:date_time]
    #date_time = date.strftime("%Y%m%d")

    @d_daily_reports.each do |d|

    # Rails.logger.info "-------#{d}-------"
    # Rails.logger.info "-------#{d[0]}-------"
    # Rails.logger.info "-------#{d[1]}-------"

    #判断是否已经存在当天的日报
    @day_report = DDailyReport.find_by( :s_region_code_id => @region_code.id, :datetime => date, :name => d[1]["name"] )
    if @day_report.present?
      @day_report.update(:nums => d[1]["nums"].to_i)
    else
      @d_daily_report = DDailyReport.new
      @d_daily_report.name = d[1]["name"]
      @d_daily_report.nums = d[1]["nums"].to_i
      @d_daily_report.s_region_code_id = @region_code.id
      @d_daily_report.datetime = date
      @d_daily_report.status = 'Y'
      @d_daily_report.save
    end
    render :index
  end
end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    
  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
