class SRoleMsgsController < ApplicationController
  before_filter :authenticate_user
  before_action :set_s_role_msg, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token,:only => :update  #跳过安全令牌验证

  # GET /s_role_msgs
  # GET /s_role_msgs.json
  def index
    case params[:all]
      when "1"
        @s_role_msgs = SRoleMsg.all
      when "sqyw"
        #@s_role_msgs = SRoleMsg.where(:role_name => ["运维成员","运维单位负责人","运维复核人"])
       # @s_role_msgs = SRoleMsg.where(:role_name => ["运维成员","运维审核人","运维复核人"])
        @s_role_msgs = SRoleMsg.where(:valid_flag => 1)
      else
        @s_role_msgs = SRoleMsg.order(:created_at => :desc).page(params[:page]).per(params[:per])
    end

    # if params[:all].present?
    #   @s_role_msgs = SRoleMsg.all
    # else
    #   @s_role_msgs = SRoleMsg.order(:created_at => :desc).page(params[:page]).per(params[:per])
    # end
  end

  # GET /s_role_msgs/1
  # GET /s_role_msgs/1.json
  def show
  end

  # GET /s_role_msgs/new
  def new
    @s_role_msg = SRoleMsg.new
    @navi_all = SFuncMsg.where("root_distance" => 1).order("queue_index")
  end

  # GET /s_role_msgs/1/edit
  def edit
    i = 0
    role_id = params[:id]
    @son_funcs = Array.new
    @navi_bar = @s_role_msg.s_func_msgs.where("root_distance = 1").order(:queue_index)
    @navi_all = SFuncMsg.where("root_distance" => 1).order("queue_index")
    ### 角色所属权限功能
    @navi_bar.each do |navi_bar|
      navi_bar.son_funcs.where("id in (select s_func_msg_id from s_role_funcs where s_role_msg_id = ? )", role_id).each do |son_func|
        @son_funcs[i] = son_func
        i = i + 1
      end
    end
  end

  # POST /s_role_msgs
  # POST /s_role_msgs.json
  def create
    @s_role_msg = SRoleMsg.new(s_role_msg_params)
    func_code_trues = params[:func_code_true]
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code = "s_role_msgs|create"
    wLoginOpr.op_time = Time.now.to_s
    wLoginOpr.login_no = current_user.login_no
    wLoginOpr.ip_addr = current_user.ip_address
    wLoginOpr.op_note = "增加角色权限"
    wLoginOpr.op_describe = current_user.login_name + "：增加角色权限"

    # 新增角色时增加角色功能表
    retCode = true
    @s_role_msg.transaction do
      retCode = @s_role_msg.save!
      for i in 0 ... func_code_trues.length
        s_role_func_data = SRoleFunc.new
        s_role_func_data.s_func_msg_id = func_code_trues[i]
        s_role_func_data.s_role_msg_id = @s_role_msg.id

        retCode = s_role_func_data.save!
        break unless retCode
      end
      retCode = wLoginOpr.save! if retCode
    end

    respond_to do |format|
      if retCode
        self.index
        format.html {render 'index'}
        format.json {render :json => {status: :created, location: @s_role_msg}}
      else
        format.html {render :new}
        format.json {render json: @s_role_msg.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /s_role_msgs/1
  # PATCH/PUT /s_role_msgs/1.json
  def update
    role_id = params[:id]
    valid_flag = params[:valid_flag]
    func_code_trues = params[:func_code_true].present? ? params[:func_code_true] : []
    func_code_falses = params[:func_code_false].present? ? params[:func_code_false] : []
    s_role_funcs = SRoleFunc.all
    Rails.logger.debug("初始化: " + func_code_trues.to_s)
    ### 插入新增的权限功能
    if func_code_trues.length != 0
      for i in 0 ... func_code_trues.length
        is_has = false
        s_role_funcs.each do |s_role_func|
          if func_code_trues[i].to_i == s_role_func.s_func_msg_id && role_id.to_i == s_role_func.s_role_msg_id
            is_has = true
            break
          else
            next
          end
        end
        if !is_has
          s_role_func_data = SRoleFunc.new
          s_role_func_data.s_func_msg_id = func_code_trues[i]
          s_role_func_data.s_role_msg_id = role_id
          s_role_func_data.save
        end
      end
    end
    #### 删除取消的权限功能
    if func_code_falses.length != 0
      s_role_funcs.each do |s_role_func|
        for i in 0 ... func_code_falses.length
          if func_code_falses[i].to_i == s_role_func.s_func_msg_id && role_id.to_i == s_role_func.s_role_msg_id
            s_role_funcs.find_by("s_func_msg_id = ? and s_role_msg_id = ?", func_code_falses[i], role_id).destroy
          end
        end
      end
    end
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code = "s_role_msgs|update"
    wLoginOpr.op_time = Time.now.to_s
    wLoginOpr.login_no = current_user.login_no
    wLoginOpr.ip_addr = current_user.ip_address
    wLoginOpr.op_note = "编辑角色权限"
    wLoginOpr.op_describe = current_user.login_name + "：编辑角色权限"

    respond_to do |format|
      if @s_role_msg.update(s_role_msg_params)
        wLoginOpr.save
        self.index
        format.html {render 'index'}
        format.json {render json: {status: :ok, location: @s_role_msg}}
      else
        format.html {render :edit}
        format.json {render json: @s_role_msg.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /s_role_msgs/1
  # DELETE /s_role_msgs/1.json
  def destroy
    @s_role_msg.destroy
    respond_to do |format|
      format.html {redirect_to s_role_msgs_url, notice: 'S role msg was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_s_role_msg
    @s_role_msg = SRoleMsg.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def s_role_msg_params
    params.require(:s_role_msg).permit(:role_name, :valid_flag, :root_distance, :queue_index)
  end
end
