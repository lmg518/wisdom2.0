class SuperiorAirDaysController < ApplicationController
  before_action :current_user
  include AqiHandleHelper

  def index
  end

  def init_index
    search_parmas
    handle_table
  end

  def export_file
    export_file_params
    @expot_file = true
    handle_table
    @csv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["站点名称", "当月", "上月", "", "去年同期", "", "1-6月累计达标率", "", "", "1-6月达标天数", '', '', '']
      csv << ["", "达标率(%)", "达标率(%)", "比较", "达标率(%)", "比较", "2017", "2016", "比较(百分点)", "2017", '2016', '比较天']
      @yy_h.each do |info|
        info_lj = (info[:current_month_aqi_days].to_f / info[:current_days].to_f).round(2)
        info_lj_per = (info[:last_year_month_aqi_days].to_f / info[:last_days].to_f).round(2)
        csv << [info[:station], info[:current_month], info[:pre_month], info[:pre_month].to_f - info[:current_month].to_f,
                info[:last_year], info[:last_year].to_f - info[:current_month].to_f, info_lj,
                info_lj_per, info_lj - info_lj_per,
                info[:current_month_aqi_days], info[:last_year_month_aqi_days], info[:current_month_aqi_days].to_i - info[:last_year_month_aqi_days].to_i]
      end
    end
=begin
    # Create a new workbook and add a worksheet
         xlsx = StringIO.new
         workbook = WriteXLSX.new(xlsx)
    # workbook  = WriteXLSX.new('merge3.xlsx')
    worksheet = workbook.add_worksheet()
    # Increase the cell size of the merged cells to highlight the formatting.
    [0, 1].each { |row| worksheet.set_row(row, 30) }
    # worksheet.set_column('B:D', 20)
    format = workbook.add_format(
        :border    => 1,
        :align     => 'center',
        :valign    => 'vcenter',
        :color     => 'blue'
    )
    format_center= workbook.add_format(
        :border    => 1,
        :align     => 'center',
        :valign    => 'vcenter',
        :color     => 'blue'
    )
    # Merge
    worksheet.merge_range('A1:A2', '站点名称', format)
    worksheet.write('B1', "当月",format_center)
    worksheet.merge_range('C1:D1', '上月', format)
    worksheet.merge_range('E1:F1', '去年同期', format)
    worksheet.merge_range('G1:I1', '1-6月累计达标率', format)
    worksheet.merge_range('J1:L1', '1-6月累计达标天数', format)
    worksheet.write('B2', "达标率(%)",format_center)
    worksheet.write('C2', "达标率(%)",format_center)
    worksheet.write('D2', "比较",format_center)
    worksheet.write('E2', "达标率(%)",format_center)
    worksheet.write('F2', "比较",format_center)
    worksheet.write('G2', "2017",format_center)
    worksheet.write('H2', "2016",format_center)
    worksheet.write('I2', "比较(百分点)",format_center)
    worksheet.write('J2', "2017",format_center)
    worksheet.write('K2', "2016",format_center)
    worksheet.write('L2', "比较天",format_center)
    @data_times = "2017-06-14"

=end
      @name = @data_times.strftime("%Y-%m-%d") + "优良天数对比"
    respond_to do |format|
      format.csv {
        send_data @csv,
                  :type => 'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end

  end

  private
  def export_file_params
    @region_leve = params[:region_level]
    @station_id = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @station_id = @search[:station_ids]
      @data_times = @search[:data_times]
      @grading_analysis = @search[:grading_analysis]
    end
  end

  def handle_table
    @region_leves = SRegionLevel.select(:id, :level_name)
    if @data_times.present?
      @data_times = Time.parse(@data_times)
    else
      @data_times = Time.now
    end

    @yy_h, @so2, @co, @no2, @o3, @pm2_5, @pm10, = [], [], [], [], [], [], [], []
    @stations = DStation.by_station_ids(@station_id)
    @stations = @stations.page(params[:page]).per(10) unless @expot_file
    @stations.each do |station|
      next if station.blank?
      # @hour_data = DDataYyyymm.where(:station_id => station.id).by_hour_month_data(@data_times)
      @now_item_avgs = StationItemAvg.where(:d_station_id => station.id).by_hour_month_data(@data_times)
      @pre_item_avgs = StationItemAvg.where(:d_station_id => station.id).by_hour_month_data(@data_times - 1.months)
      @per_year_item_avgs = StationItemAvg.where(:d_station_id => station.id).by_hour_month_data(@data_times - 1.years)
      @current_month_item_avgs = StationItemAvg.where(:d_station_id => station.id).by_month_between_and(@data_times)
      @pre_month_item_avgs = StationItemAvg.where(:d_station_id => station.id).by_month_between_and(@data_times - 1.years)
      #AQI优良  0～100
      @aqi_days = handle_aqi(@now_item_avgs)
      #上个月
      @pre_aqi_days = handle_aqi(@pre_item_avgs)
      #上年同期
      @pre_year_aqi_days = handle_aqi(@per_year_item_avgs)
      #本年度累计
      @current_month_aqi_days = handle_aqi(@current_month_item_avgs)
      #去年度累计
      @pre_month_aqi_days = handle_aqi(@pre_month_item_avgs)
      now_month_days = month_days(@data_times.month)
      pre_month_days = month_days((@data_times - 1.months).month)
      pre_yar_month_days = month_days((@data_times - 1.years).month)
      current_month_days = days_between(@data_times.strftime("%Y-%m-%d"), (@data_times.beginning_of_year).strftime("%Y-%m-%d"))
      pre_month_days = days_between((@data_times - 1.year).strftime("%Y-%m-%d"), ((@data_times - 1.years).beginning_of_year).strftime("%Y-%m-%d"))
      @yy_h << {station: station.station_name,
                current_month: (((@aqi_days.to_f / now_month_days.to_f) * 100 ).round(2)) ,
                pre_month: ((@pre_aqi_days.to_f / pre_month_days.to_f) * 100 ).round(2),
                last_year: ((@pre_year_aqi_days.to_f / pre_yar_month_days.to_f) * 100 ).round(2),
                current_month_aqi_days: @current_month_aqi_days,
                last_year_month_aqi_days: @pre_month_aqi_days,
                current_days: current_month_days,
                last_days: pre_month_days}
    end
    @yy_h
  end

  def handle_aqi(item_arrs)
    aqi_days = 0
    item_arrs.each do |pre_item_avg|
      aqi = laqi_handle(pre_item_avg.so2_avg,
                        pre_item_avg.co_avg,
                        pre_item_avg.no2_avg,
                        pre_item_avg.o3_avg,
                        pre_item_avg.pm2_5_avg,
                        pre_item_avg.pm10_avg)
      if aqi == "优" || aqi == "良"
        aqi_days += 1
      else
        next
      end
    end
    aqi_days
  end

end
