class TestIframesController < ApplicationController
  before_action :set_test_iframe, only: [:show, :edit, :update, :destroy]

  # GET /test_iframes
  # GET /test_iframes.json
  def index

    @test_iframes = TestIframe.all

    # @stations = DStation.all
    # @info = []
    # @stations.each do |station|
    #   logins = station.d_login_msgs
    #
    #   if logins.blank?
    #     @info << {id: station.id, station_name: station.station_name, login_name: "", telphone: ''}
    #   else
    #     logins.map {|x|  @info << {id: station.id, station_name: station.station_name, login_name: x.login_name, telphone: ""}  if x.telphone.blank? }
    #   end
    # end
    # @scv = CSV.generate(encoding: "GB18030") do |csv|
    #   csv << ["站点ID", "站点名称", "人员姓名", "电话"]
    #   @info.each do |info|
    #     csv << [info[:id], info[:station_name], info[:login_name],info[:telphone]]
    #   end
    # end
    #
    # @name = Time.now.strftime("%Y-%m-%d-") + "站点人员补漏"
    # respond_to do |format|
    #   format.html{}
    #   format.csv {
    #     send_data @scv,
    #               :type => 'text/csv; charset=GB18030; header=present',
    #               :disposition => "attachment; filename=#{@name}.csv"
    #   }
    # end

    # #render layout: false
    # respond_to do |format|
    #   format.html { render :index, status: :ok }
    #   format.json { render :index, status: :ok }
    # end
  end

  def download_explorer
    file_apth = "#{Rails.root}/public/app/android-app.apk"
    send_file(file_apth)
  end

  # GET /test_iframes/1
  # GET /test_iframes/1.json
  def show
  end

  # GET /test_iframes/new
  def new
    @test_iframe = TestIframe.new
  end

  # GET /test_iframes/1/edit
  def edit
  end

  # POST /test_iframes
  # POST /test_iframes.json
  def create
    @test_iframe = TestIframe.new(test_iframe_params)

    respond_to do |format|
      if @test_iframe.save
        format.html {redirect_to @test_iframe, notice: 'Test iframe was successfully created.'}
        format.json {render :show, status: :created, location: @test_iframe}
      else
        format.html {render :new}
        format.json {render json: @test_iframe.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /test_iframes/1
  # PATCH/PUT /test_iframes/1.json
  def update
    respond_to do |format|
      if @test_iframe.update(test_iframe_params)
        format.html {redirect_to @test_iframe, notice: 'Test iframe was successfully updated.'}
        format.json {render :show, status: :ok, location: @test_iframe}
      else
        format.html {render :edit}
        format.json {render json: @test_iframe.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /test_iframes/1
  # DELETE /test_iframes/1.json
  def destroy
    @test_iframe.destroy
    respond_to do |format|
      format.html {redirect_to test_iframes_url, notice: 'Test iframe was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_test_iframe
    @test_iframe = TestIframe.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def test_iframe_params
    params.require(:test_iframe).permit(:mark, :yangna)
  end
end
