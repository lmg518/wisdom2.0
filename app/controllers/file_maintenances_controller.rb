class FileMaintenancesController < ApplicationController
  before_action :current_user
  before_action :get_file_maines, only: [:show,:update,:destroy,:edit_new,:edit_model]

  def index
    @file_maintenancees = FileMaintenance.order(:created_at => :desc)
    @file_maintenancees = @file_maintenancees.page(params[:page]).per(10)
    @files = FileMaintenance.new
  end

  def new
    @file = FileMaintenance.new
  end

  def show
    if params[:edit_show].present?
      render json:{location: @file}
    else
      render :partial => "fileLook", layout: false
    end
  end

  def edit_new
    respond_to do |format|
      if @file.update(params_file)
        format.html {}
        format.js{}
        format.json{ render json:{status: :ok, localtion: @file } }
      else
        format.json{ render json:{status: :unprocessable_entity, localtion: @file.errors } }
      end
    end
  end

  def update
    respond_to do |format|
      if @file.update(params_file)
        format.html {}
        format.js{}
        format.json{ render json:{status: :ok, localtion: @file } }
      else
        format.json{ render json:{status: :unprocessable_entity, localtion: @file.errors } }
      end
    end
  end

  def create
    @file = FileMaintenance.new(params_file)
    @file.author = @current_user.login_name
    @file.d_login_msg_id = @current_user.id
    @file.content = params[:content] if params[:content]
    respond_to do |format|
      if @file.save
        format.html{ redirect_to '/file_maintenances' }
        format.js{}
        format.json{ render json: {status: :ok, location: @file} }
      else
        format.html{ render :new }
        format.json{ render json:{status: :unprocessable_entity, localtion: @file.errors } }
      end
    end
  end

  def destroy
    @file.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private

  def get_file_maines
    @file = FileMaintenance.find(params[:id])
  end

  def params_file
    params.require(:file_maintenance).permit(:title, :content, :flag, :author, :file_category_id, :d_login_msg_id )
  end

end
