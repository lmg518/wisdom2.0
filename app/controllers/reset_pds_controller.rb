class ResetPdsController < ApplicationController

  def update
    @d_login_msg = DLoginMsg.find(params[:id])
    retCode = true
    d_login_msg_params = {password: "123456",password_confirmation: "123456" }
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_login_msgs|update"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = current_user.login_no
    wLoginOpr.ip_addr   = current_user.ip_address
    wLoginOpr.op_note   = "编辑工号"
    wLoginOpr.op_describe = "编辑工号，" + "工号编码：" + @d_login_msg.login_no + ", 工号名称：" + @d_login_msg.login_name


    @d_login_msg.transaction do
      retCode = @d_login_msg.update(d_login_msg_params)
      retCode = wLoginOpr.save if retCode
    end

    respond_to do |format|
      if retCode
        format.html {}
        format.json { render json:{status: :ok, location: @d_login_msg}  }
      else
        format.html { render :edit }
        format.json { render json: @d_login_msg.errors, status: :unprocessable_entity }
      end
    end

  end

end