class DStationsController < ApplicationController
  before_filter :authenticate_user
  before_action :set_d_station, only: [:show, :edit, :update, :destroy,:area_logins,:handle_station]
  before_action :current_user



  # GET /d_stations
  # GET /d_stations.json
  def index
    # code_arr = []
    code_hash = {}
    region_code = @current_user.s_region_code

    @s_station_code = @current_user.d_stations.page(params[:page]).per(params[:per])
    @regin_stations = @s_station_code.present? ? @s_station_code : []

    code_hash['chil'] = {region_chil: region_code ,d_stations: @s_station_code}
    @station_hash =[{region_chil: region_code ,d_stations: @s_station_code}]

    @station_id     = params[:station_id]
    @station_name   = params[:station_name]
    # @d_station_region_code = DStation.where( :region_code => @current_user.s_region_code.region_code ).page(params[:page]).per(params[:per])
    if @station_id.nil?
      @d_stations = @s_station_code
    # elsif @station_id.length == 0
    #   @d_stations = @d_station_region_code
    else
      @d_stations = @s_station_code.where( "id" => @station_id.split(",") )
    end
  end

  def all_stations
    if params[:station_ids].present?
      @d_stations = DStation.by_station_ids(params[:station_ids]).order(:created_at => :desc).page(params[:page]).per(10)
      return
    end

    @d_stations =  DStation.order(:created_at => :desc).page(params[:page]).per(params[:per])

  end
  # get /d_stations/:id/area_logins
  def area_logins
     arr = []
     # zone_ids = @d_station.s_area_logins.pluck
     area_code = @d_station.region_code
     # login_arr = @d_station.d_login_msgs.order(:id)

     @areas = SRegionCode.where(:region_code => area_code)
     @areas.each_with_index do |area,index|
       area_login_arr =  area.d_login_msgs.order(:id)
       # zone_login_arr <<  area_login_arr
       arr   <<  {area: area, logins: area_login_arr}
     end

       render json: {areas: arr}

  end

  # post /d_stations/:id/handle_station
  def handle_station
    flag = false
      if params[:login_arr].present?
        arr = params[:login_arr]
        arr.each do |login|
          if @d_station.s_area_logins.where(:d_login_msg_id => login).present?
            flag = true
             next
          else
            if @d_station.s_area_logins.where(:d_login_msg_id => login).present?
              @d_station.s_area_logins.where(:d_login_msg_id => login).first.delete
              flag = true
              next
            end

            if  @d_station.s_area_logins.create(:d_login_msg_id => login)
              flag = true
            else
              flag = false
            end
          end


        end
      end
      if flag
        render json: {location: "success", status: :ok}
      else
        render json: {location: "error",status: :unprocessable_entity}
      end
  end

  # put /d_stations/:id/update_station
  def update_station
    flag = false
    if params[:login_arr].present?
      arr = params[:login_arr]
      arr.each do |login|
        if  @d_station.s_area_logins.where(:d_login_msg_id => login).first.delete
          flag = true
        else
          flag = false
        end
      end
    end
    if flag
      render json: {location: "success", status: :ok}
    else
      render json: {location: "error",status: :unprocessable_entity}
    end
  end

  # GET /d_stations/1
  # GET /d_stations/1.json
  def show 
  end

  # GET /d_stations/new
  def new
    @d_station           = DStation.new
    @s_station_types     = SStationType.all
    @s_software_versions = SSoftwareVersion.all
    @s_region_codes      = SRegionCode.all
  end

  # GET /d_stations/1/edit
  def edit
    @s_station_types     = SStationType.all
    @s_software_versions = SSoftwareVersion.all
    @s_region_codes      = SRegionCode.all
  end

  # POST /d_stations
  # POST /d_stations.json
  def create
    retCode = true
    
    @d_station = DStation.new(d_station_params)
    # @area = SAdministrativeArea.find_by(:zone_rank =>@d_station.region_code)
    # @d_station.region_code = @area.zone_name
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code    = "d_stations|create"
    wLoginOpr.op_time    = Time.now.to_s
    wLoginOpr.login_no   = @current_user.login_no
    wLoginOpr.ip_addr    = @current_user.ip_address
    wLoginOpr.op_note    = "站点配置"
    wLoginOpr.op_describe = "新增站点配置信息"
    s_administrative_area = SAdministrativeArea.where(:zone_name => @d_station.region_code).first
    @d_station.region_code = s_administrative_area.zone_name if s_administrative_area.present?
    @d_station.s_administrative_area_id = s_administrative_area.id if s_administrative_area.present?
  
    #站点名
   @d_station.station_name = "#{@d_station.region_code}-#{@d_station.station_name}"
    
  retCode = wLoginOpr.save! if retCode
    respond_to do |format|
      if @d_station.save
        format.html { redirect_to  "/d_stations" }
        format.json { render json: {location: @d_station, status: :created  } }
      else
        format.html { render :new }
        format.json { render json: @d_station.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /d_stations/1 
  # PATCH/PUT /d_stations/1.json
  def update
    retCode   = true
    @onoff_flag     = params[:onoff_flag]
    ## 判断
    if @onoff_flag == "on"
      station_status = '1'
    else
      station_status = '0'    
    end
    if @onoff_flag
      retCode = @d_station.update( "station_status" => station_status )
    else
      s_administrative_area = SAdministrativeArea.where(:zone_name=> params[:d_station]["region_code"]).first
      params[:d_station]["region_code"] = s_administrative_area.zone_name  if s_administrative_area.present?
      params[:d_station]['s_administrative_area_id'] = s_administrative_area.id if s_administrative_area.present?
      retCode = @d_station.update( d_station_params )
    end
    respond_to do |format|
      if retCode
        format.html {  }
        format.json { render json:{ status: :ok, location: @d_station} }
      else
        format.html { render :edit }
        format.json { render json: @d_station.errors, status: :unprocessable_entity }
      end
    end
    
    
    # 写日志
    wLoginOpr = WLoginOpr.new
    wLoginOpr.op_code   = "d_stations|update"
    wLoginOpr.op_time   = Time.now.to_s
    wLoginOpr.login_no  = @current_user.login_no
    wLoginOpr.ip_addr   = @current_user.ip_address
    wLoginOpr.op_note   = "站点信息配置修改"
    wLoginOpr.op_describe = "站点信息配置修改"
    wLoginOpr.save
  end

  # DELETE /d_stations/1
  # DELETE /d_stations/1.json
  def destroy
    @d_station.destroy
    respond_to do |format|
      format.html { redirect_to d_stations_url, notice: 'D station was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_d_station
      @d_station = DStation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def d_station_params
      params.require(:d_station).permit(:station_name, :region_code, :station_type,
                                        :wc_station_id, :object_id, :match_string, :station_location, :longitude,
                                        :latitude, :station_ip, :station_mac, :service_no, :setup_date,:created_at,
                                        :station_status, :is_online, :note, :soft_version_no,:s_administrative_area_id,
      :distance, :arrive_time )
    end
end
