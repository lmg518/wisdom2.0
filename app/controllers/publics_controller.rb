class PublicsController < ApplicationController

  #解决账户被顶页面刷新问题
  # publics/status.json
  def status
    Rails.logger.debug( '当前用户的SessionCode: ' + session[:session_code].to_s )
    @current_user ||= DLoginMsg.find_by('session' => params[:session].present? ? params[:session] : session[:session_code].to_s)
    if @current_user.blank?
      return render json:{msg: "操作超时，请重新登录", status:"401" }
    else
      return render json:{msg: "success"}
    end
  end

end