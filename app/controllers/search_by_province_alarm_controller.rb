class SearchByProvinceAlarmController < ApplicationController
  before_action :current_user
  def index
    region_code = @current_user.s_region_code
    d_stations = region_code.d_stations.by_province(params[:province])
    if d_stations.present?
      @alarms =  DAlarm.by_station_id(d_stations.pluck(:id)).by_today_data
      @alarms = @alarms.page(params[:page]).per(5)
      render json: {alarms: @alarms.map{|x| {id:x.id ,station_id: x.station_id,station_name: x.d_station.station_name,
                                             alarm_rule: x.alarm_rule,alarm_level: x.alarm_level_to_s,
                                             first_alarm_time: x.first_alarm_time.strftime("%Y-%m-%d %H:%M:%S") ,
                                             last_alarm_time: x.last_alarm_time.present? ? x.last_alarm_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                                             continuous_alarm_times: x.continuous_alarm_times,
                                             status: x.status_to_str} },
                    list_size: @alarms.present? ? @alarms.total_count : 0 }
    else
      render json: {alarms: {}, list_size:0}
    end

  end

end