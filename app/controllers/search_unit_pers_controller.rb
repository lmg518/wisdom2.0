class SearchUnitPersController < ApplicationController

  def index
    unit = SRegionCodeInfo.find(params[:s_region_code_info_id])
    user = DLoginMsg.where(:s_region_code_info_id => unit.id) unless unit.blank?
    h = {"unit_id" =>unit.id,"unit_name" =>unit.unit_name,"user" => user.select(:id,:login_name)}
    render json:{units: h.present? ? h : {}}
  end

  def update
    @unit = DTaskForm.find_by(:id =>params[:id])
    @unit.s_region_code_info_id = params[:unit_id]
    @unit.handle_man_id = params[:handle_man_id]
    @unit.handle_man_old = @unit.handle_man
    @unit.handle_man = params[:handle_man]
    respond_to do |format|
      if @unit.save
        format.json{render json:{status: "success",location:@unit}}
      else
        format.json{render json:{status: "false",location:@unit.errors}}
      end
    end
  end

end