class EquipsAreaManageController < ApplicationController
  before_action :get_equip, only: [:show, :edit, :update, :destroy,:set_status,:get_handle_equip]
  before_action :authenticate_user
  # skip_before_filter :verify_authenticity_token  #跳过安全令牌验证

  #设备区域管理
  def index
    get_region
    @region_codes_ids = SRegionCode.by_region_id(params[:region_id]).where(:s_region_level_id => 3).pluck(:id)
    @s_areas = SRegionCodeArea.where(:s_region_code_id => @region_codes_ids).page(params[:page]).per(params[:per])
  
  
  end

  def new
    get_region
    render partial: 'new', layout: false
  end

  #车间下的区域跟设备关联   保存操作
  #POST  /equips_area_manage
  def create

    region_id = params[:region_id]  #车间id
    area_id = params[:area_id]      #区域id
    equip_ids = params[:equip_ids]  #设备id

    #车间下的区域 设备 先清除关联
    s_equips = SEquip.where(:s_area_id => area_id, :s_region_code_id => region_id)


    Rails.logger.info "------222222---------"
    Rails.logger.info s_equips.inspect


    if s_equips.present? && s_equips.length > 0
      s_equips.each do |s|
        Rails.logger.info "------#{s.s_area_id}----------"
        s.update(:s_area_id => nil)
      end
    end


    flag = false
    if equip_ids.present? && equip_ids.length > 0
      equip_ids.each do |id|
        s_equip = SEquip.find_by(:id => id)  #找到配置区域的设备
        s_equip.update(:s_area_id => area_id)
      end
      flag = true
    end

    respond_to do |format|
      if flag
        format.json {render json: {status: 200}}
      else
        format.html {}
        format.json {render json: {status: 'error'}}
      end
    end
  end

  def show
    @s_spares = @equip.s_spares

    # render json: {equip: @equip, s_spare: @s_spares,handle_equip: @handle_equips }
  end

  #修改操作
  #get /equips_area_manage/area_edit
  def area_edit
    region_id =  params[:region_id]    #车间id
    area_id =  params[:area_id]        #区域id

    region_code = SRegionCode.find_by(:id => region_id)
    s_area = SArea.find_by(:id => area_id)
    s_equips = s_area.s_equips    #区域下的设备

    region_name = region_code.present? ? region_code.region_name : ''
    area_name = s_area.present? ? s_area.name : ''
    equips = s_equips.present? ? s_equips : ''

    render json: {region_name: region_name, area_name: area_name,  equips: equips }

  end


  def edit
    render json: {equip: @equip}
  end



  def update
    respond_to do |format|
      if @equip.update(get_equip_params)
        format.json {render json: {status: 200}}
      else
        format.html {}
        format.json {render json: {status: 'error'}}
      end
    end
  end

  def destroy
    spares = SSpare.where(s_equip_id: @equip.id)
    if @equip.destroy && spares.destroy_all
      render json: {status: 200}
    else
      render json: {status: 'error'}
    end
  end

  #禁用启用
  #get /s_equips/set_status
  def set_status
    if @equip
      @equip.update(:equip_status => params[:equip_status])
      render json:{status: 200}
    end
  end

#----------所属备件信息管理-------------
#POSt /s_equips/create_spare
  def create_spare
    get_spare_params
    respond_to do |format|
      if @spare.save
        format.json {render json: {status: 200}}
      else
        format.json {render json: {status: 'error'}}
      end
    end
  end

  def edit_spare
    @s_spare = SSpare.find(params[:id])
  end

#post /s_equips/update_spare
  def update_spare
    @s_spare = SSpare.find(params[:id])
    if @s_spare.update(:spare_name=>params[:spare_name], :spare_code=>params[:spare_code], :spare_desc=>params[:spare_desc], :spare_material=>params[:spare_material],
                      :spare_num=>params[:spare_num])
      render json: {status: 200}
    else
      render json: {status: 'error'}
    end
  end

#delete /s_equips/delete_spare
  def delete_spare
    @s_spare = SSpare.find(params[:id])
    if @s_spare.nil?
      render json: {status: '数据不存在'}
    else
      if @s_spare.destroy
        render json: {status: '删除成功'}
      else
        render json: {status: 'error'}
      end
    end
  end

  #get /s_equips/get_handle_equip
  #获取到所属维修记录
  def get_handle_equip
      handle_equips = @equip.handle_equips
      han = []
      handle_equips.each do |h|
        han << {id:h.id,region_name:h.region_name,equip_name:h.equip_name,equip_code:h.equip_code,
                handle_name:h.handle_name,handle_start_time: h.handle_start_time.present? ? h.handle_start_time.strftime('%Y-%m-%d') : '',
                handle_end_time:h.handle_end_time.present? ? h.handle_end_time.strftime('%Y-%m-%d') : '',snag_desc:h.snag_desc,
                status: HandleEquip.set_status(h.status),desc:h.desc,founder:h.founder}
      end
      render json: {handle_equips: han,handle_size: han.length}
  end

  #根据车间获取到车间下的区域
  #get /equips_area_manage/get_areas
  def get_areas
    region_id = params[:region_id]
    area_ids = SRegionCodeArea.where(:s_region_code_id => region_id).pluck(:s_area_id)
    @areas = SArea.select(:id,:code,:name).where(id: area_ids)

    #封装所有设备信息
    @s_equips = SEquip.select(:id,:equip_name,:equip_code).all
    render json: {areas: @areas, equips: @s_equips}
  end

  private
  def get_equip
    @equip = SEquip.find(params[:id])
  end

#接收设备创建参数
  def get_equip_params
    params.require(:s_equips).permit(:s_region_code_id, :region_name, :bit_code, :equip_code,
                                     :equip_name, :equip_location, :equip_norm, :equip_nature, :equip_material,
                                     :equip_num, :apper_code, :apper_time,:equip_status,:factory, :equip_note)
  end

#获取车间
  def get_region
    puts "group_id: #{@current_user.group_id}"
    region_code = SRegionCode.find_by(:id => @current_user.group_id)
    father_region = SRegionCode.find_by(:father_region_id == 99999)
    if region_code.father_region_id == 99999
      @region_code = SRegionCode.select(:id, :region_name).where("father_region_id != 99999")
    elsif region_code.father_region_id == father_region.id
      @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id =>region_code.id)
    else
      @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id => region_code.father_region_id)
    end

  end

#接受备件创建参数
  def get_spare_params
    @spare = SSpare.new(:s_equip_id =>params[:s_equip_id],
                        :equip_code=>params[:equip_code],
                        :spare_name=>params[:spare_name],
                        :spare_code=>params[:spare_code],
                        :spare_desc=>params[:spare_desc],
                        :spare_material=>params[:spare_material],
                        :spare_num=>params[:spare_num])
  end



end