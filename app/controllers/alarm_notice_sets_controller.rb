class AlarmNoticeSetsController < ApplicationController
  before_action :get_notice_rule, only: [:show, :update, :edit, :destroy]

  def index
    @notice_riles = SNoticeRule.order(:created_at => :desc).page(params[:page]).per(params[:per])
  end

  def show
    render json: {notice_rule: @notice_rule.json_has}
  end

  def create
    @notice_rules = SNoticeRule.new(set_notice_rule_params)
    respond_to do |format|
      if @notice_rules.save
        if params[:role_arr].present?
          params[:role_arr].each do |arr|
            @notice_rules.notice_rule_roles.create!(:s_role_msg_id => arr.to_i)
          end
        end
        format.html {}
        format.json {render json: {location: @notice_rules.json_has, status: :created}}
      else
        format.html {}
        format.json {render json: @notice_rules.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    respond_to do |format|
      if @notice_rule.update(set_notice_rule_params)
        if params[:role_arr].present?
          role_arr = params[:role_arr].map {|x| x.to_i}
          @notice_roles = @notice_rule.notice_rule_roles
          puts ">>>>#{role_arr}"
          if role_arr.length == 1 && role_arr[0] == ''
            @notice_roles.delete_all
            format.html {}
            format.json {render json: {status: :ok, location: @notice_rule.json_has}}
            return
          end
          @role_ids = @notice_roles.map {|x| x.s_role_msg_id}
          if @notice_roles.present?
            if @role_ids.present?
              role_arr.each_with_index do |arr, index|
                @notice_roles.each do |x|
                  x.delete unless role_arr.include? x.s_role_msg_id
                end
                @notice_roles.create(:s_role_msg_id => arr.to_i) if !@role_ids.include?(arr.to_i)
              end
            end
          else
            role_arr.map {|x| @notice_roles.create(:s_role_msg_id => x)}
          end
        else
          @notice_rule.notice_rule_roles.destroy_all if @notice_rule.notice_rule_roles.present?
        end
        format.html {}
        format.json {render json: {location: @notice_rule.json_has, status: :ok}}
      else
        format.html {}
        format.json {render json: @notice_rule.errors, status: :unprocessable_entity}
      end
    end
  end

  private

  def get_notice_rule
    @notice_rule = SNoticeRule.find(params[:id])
  end

  def set_notice_rule_params
    params.require(:s_notice_rule).permit(:alarm_level, :notice_type, :role_id, :up_region_level, :s_notice_template_id, :title, :content)
  end

end