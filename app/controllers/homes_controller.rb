class HomesController < ApplicationController
  before_filter :authenticate_user
  # layout 'home_iframe'
  layout 'root_application'

  def index
    @current_user = current_user
    @index, @chil_arr = [],[]
    @father_func = []
    @func_msgs = @current_user.s_role_msg.s_func_msgs.order('queue_index asc')
    @func_msgs.map{|x| @father_func << {func_name: x.func_name, func_url: x.func_url,func_code: x.id} if x.father_func_id == 9999 }
    @father_func.each do |father|
      @func_msgs.each do |func|
        next if func.father_func_id == 9999
        @chil_arr << {func_name: func.func_name, func_url: func.func_url, func_code: func.father_func_id} if func.father_func_id == father[:func_code]
      end
      @index <<  {func_name: father[:func_name], func_url: father[:func_url], func_code: father[:func_code],
                  img_url: img_url_str(father[:func_name]), chil: @chil_arr}
      @chil_arr = []
    end

    # @current_user.s_role_msg.s_func_msgs.order('queue_index asc').each do |m|
    #   next if m.father_func_code != '9999'

      # m.mean_childern_path.each do |c|
      #   @chil_arr << {func_name: c.func_name, func_url: c.func_url, func_code: c.func_code}
      # end

    #   @index <<  {func_name:m.func_name, func_url: m.func_url, func_code: m.func_code, img_url: img_url_str(m.func_name), chil: @chil_arr}
    #   @chil_arr = []
    # end
  end

  def video_help
    send_file( Rails.public_path.to_s.concat('/WebComponentsKit.exe'), type: 'application/x-msdownload' ) #, disposition: 'inline' )
  end

  private
  def img_url_str(fun_name)
    case fun_name
      when '首页'
        "/images/left-00.png"
      when '异常监控'
        "/images/left-01.png"
      when '报警监控'
        "/images/left-02.png"
      when '报警通知'
        "/images/left-03.png"
      when '历史查询'
        "/images/left-04.png"
      when '系统配置'
        "/images/left-05.png"
      when '报警任务'
        "/images/left-06.png"
      when '运维报表'
        "/images/left-07.png"
      when '运维管理'
        "/images/left-08.png"
      when '统计分析'
        "/images/left-09.png"
      when '公告通知'
        "/images/left-10.png"
    end
  end
end
