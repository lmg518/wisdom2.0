class GetObjTypesController < ApplicationController

  # GET /get_obj_types.json
  def index
    obj_types = SMonitorObjectType.all
    render json: {obj_types: obj_types}
  end


end