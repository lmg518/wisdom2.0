class AlarmNumStatisticsController < ApplicationController

  def index
  end

  def init_index
    search_parmas
    handle_table
    @alarm_totle #= Kaminari.paginate_array(@alarm_totle).page(params[:page]).per(10)
  end

  def export_file
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["日期", "区域", "站点",	"报警级别",	"次数"]
      @alarm_totle.each do |alarm|
        case alarm[:alarm_level].to_i
          when 3
           @alarm_level_str = "严重"
          when 2
            @alarm_level_str =  "较重"
          when 1
            @alarm_level_str =  "一般"
        end
        csv << [alarm[:data_time], alarm[:region_zone], alarm[:station_name], @alarm_level_str , alarm[:alarm_num]]
      end
    end
    if  @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "报警数量统计"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "报警数量统计"
    end
    respond_to do |format|
      format.csv{
        send_data @scv,
                  :type=>'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private
  
  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
    @alarm_levels = params[:alarm_levels]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @station_ids = @search[:station_ids]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
    end
  end

  def handle_table
    @alarm_totle = []
    @region_leves = SRegionLevel.select(:id, :level_name)
    @stations = DStation.by_station_ids(@station_ids).select {|x| x.d_alarms.present?}
    @stations.each do |station|
      @alarm_level_1, @alarm_level_2, @alarm_level_3 = 0, 0, 0
      next if station.d_alarms.blank?
      next if station.s_region_codes.blank?
      region_zones = station.s_region_codes.by_region_level(@region_leve)
      next if region_zones.blank?
      region_zone = region_zones.first
      alarm_arr = station.d_alarms.by_search_times(@data_times)
                         .by_rule_levels(@alarm_levels)
      alarm_handle(alarm_arr,region_zone,station)
    end
  end

  def alarm_handle(alarm_arr,region_zone,station)
    @alarm_level_1,@alarm_level_2,@alarm_level_3 = 0,0,0
    alarm_1 = alarm_arr.where(:alarm_level => 1)
    alarm_1.map {|x| @alarm_level_1 += x.continuous_alarm_times}
    alarm_2 = alarm_arr.where(:alarm_level => 2)
    alarm_2.map {|x| @alarm_level_2 += x.continuous_alarm_times}
    alarm_3 = alarm_arr.where(:alarm_level => 3)
    alarm_3.map {|x| @alarm_level_3 += x.continuous_alarm_times}
    if alarm_1.present?
      @alarm_totle << {data_time: alarm_1.first.first_alarm_time.strftime("%Y-%m-%d"), region_zone: region_zone.region_name,
                       station_id: station.id, station_name: station.station_name, alarm_level: 1,
                       alarm_num: @alarm_level_1}
    end
    if alarm_2.present?
      @alarm_totle << {data_time: alarm_2.first.first_alarm_time.strftime("%Y-%m-%d"), region_zone: region_zone.region_name,
                       station_id: station.id, station_name: station.station_name, alarm_level: 2,
                       alarm_num: @alarm_level_2}
    end
    if alarm_3.present?
      @alarm_totle << {data_time: alarm_3.first.first_alarm_time.strftime("%Y-%m-%d"), region_zone: region_zone.region_name,
                       station_id: station.id, station_name: station.station_name, alarm_level: 3,
                       alarm_num: @alarm_level_3}
    end
  end

end