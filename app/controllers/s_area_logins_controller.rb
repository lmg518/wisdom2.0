class SAreaLoginsController < ApplicationController
  before_action :set_s_area_login, only: [:show, :edit, :update, :destroy]

  # GET /s_area_logins
  # GET /s_area_logins.json
  def index
    @s_area_logins = SAreaLogin.all
  end

  # GET /s_area_logins/1
  # GET /s_area_logins/1.json
  def show
  end

  # GET /s_area_logins/new
  def new
    @s_area_login = SAreaLogin.new
  end

  # GET /s_area_logins/1/edit
  def edit
  end

  # POST /s_area_logins
  # POST /s_area_logins.json
  def create
    @s_area_login = SAreaLogin.new(s_area_login_params)

    respond_to do |format|
      if @s_area_login.save
        format.html { redirect_to @s_area_login, notice: 'S area login was successfully created.' }
        format.json { render :show, status: :created, location: @s_area_login }
      else
        format.html { render :new }
        format.json { render json: @s_area_login.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /s_area_logins/1
  # PATCH/PUT /s_area_logins/1.json
  def update
    respond_to do |format|
      if @s_area_login.update(s_area_login_params)
        format.html { redirect_to @s_area_login, notice: 'S area login was successfully updated.' }
        format.json { render :show, status: :ok, location: @s_area_login }
      else
        format.html { render :edit }
        format.json { render json: @s_area_login.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /s_area_logins/1
  # DELETE /s_area_logins/1.json
  def destroy
    @s_area_login.destroy
    respond_to do |format|
      format.html { redirect_to s_area_logins_url, notice: 'S area login was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_s_area_login
      @s_area_login = SAreaLogin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def s_area_login_params
      params.require(:s_area_login).permit(:s_administrative_area_id, :d_station_id, :d_login_msg_id)
    end
end
