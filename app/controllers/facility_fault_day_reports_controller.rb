class FacilityFaultDayReportsController < ApplicationController
  before_action :current_user

  def index
  end

  def init_index
    search_parmas
    handle_table
  end

  def export_file
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ['序号', '时间', "省份", "城市", "站点名称", "质控因子", "故障标识","开始时间","结束时间"]
      @data_arr.each_with_index do |data,index|
        csv << [index + 1, data[:day_time],data[:province],data[:city], data[:station_name], data[:item_name], data[:item_label] ,
        data[:begin_time],data[:end_time]]
      end
    end
    if @data_times.present?
      @name = Time.parse(@data_times).strftime("%Y-%m-%d-")  + "设备运行故障日报"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "设备运行故障日报"
    end
    respond_to do |format|
      format.csv {
        send_data @scv,
                  :type => 'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private
  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times]
    @alarm_levels = params[:alarm_levels]
    @provience = params[:provience]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @station_ids = @search[:station_ids]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
      @provience = @search[:provience]
    end
  end

  def handle_table
    @data_arr = []
    @day_datas = DayHourFalgData.by_get_data(@data_times).by_province(@provience).order(:d_station_id)
    # @day_datas = @day_datas.page(params[:page]).per(10)
    @day_datas.each do |day_data|
      @data_arr << {day_time: day_data.acce_data.strftime("%Y-%m-%d"), province: day_data.province,
                    city: day_data.city, station_name: day_data.station_name, item_name: day_data.item_name,
                    item_label: day_data.item_flag, begin_time: day_data.acce_data.strftime("%Y-%m-%d %H:%M:%S"),
                    end_time: day_data.acce_data.strftime("%Y-%m-%d %H:%M:%S") }

    end


  end

end