class ContaminantLevesController < ApplicationController
  before_action :current_user
  include AqiHandleHelper

  def index
  end

  def init_index
    search_parmas
    handle_table
  end

  def export_file
    export_file_params
    @search_status = true
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ['站点名称', "优", "", "良", "", "轻度污染", "", "中度污染", "", "重度污染", "", "严重污染", "", "有效天数"]
      csv << ['', "天数", "比例", "天数", "比例", "天数", "比例", "天数", "比例", "天数", "比例", "天数", "比例", "有效天数"]
      @yy_h.each do |yy|
        puts yy
        csv << [yy[:station],
                yy[:aqi1],
                yy[:aqi_days].to_i != 0 ? (yy[:aqi1].to_f / yy[:aqi_days].to_f).round(2) : "0.00",
                yy[:aqi2],
                yy[:aqi_days].to_i != 0 ? (yy[:aqi2].to_f / yy[:aqi_days].to_f).round(2) : "0.00",
                yy[:aqi3],
                yy[:aqi_days].to_i != 0 ? (yy[:aqi3].to_f / yy[:aqi_days].to_f).round(2) : "0.00",
                yy[:aqi4],
                yy[:aqi_days].to_i != 0 ? (yy[:aqi4].to_f / yy[:aqi_days].to_f).round(2) : "0.00",
                yy[:aqi5],
                yy[:aqi_days].to_i != 0 ? (yy[:aqi5].to_f / yy[:aqi_days].to_f).round(2) : "0.00",
                yy[:aqi6],
                yy[:aqi_days].to_i != 0 ? (yy[:aqi6].to_f / yy[:aqi_days].to_f).round(2) : "0.00",
                yy[:aqi_days]
                ]
      end
    end
    if @data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "污染天气级别分布"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "污染天气级别分布"
    end
    respond_to do |format|
      format.csv {
        send_data @scv,
                  :type => 'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private

  def export_file_params
    @station_id = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
    @grading_analysis = params[:grading_analysis]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @station_id = @search[:station_ids]
      @data_times = @search[:data_times]
      @grading_analysis = @search[:grading_analysis]
    end
  end

  def handle_table
    @region_leves = SRegionLevel.select(:id, :level_name)
    @yy_h, @so2, @co, @no2, @o3, @pm2_5, @pm10, = [], 0, 0, 0, 0, 0, 0
    @stations = DStation.by_station_ids(@station_id)
    @stations = @stations.page(params[:page]).per(10) unless @search_status
    @stations.each do |station|
      next if station.blank?
      @now_item_avgs = StationItemAvg.where(:d_station_id => station.id).by_get_time(@data_times)
      #AQI
      @aqi_days = handle_aqi(@now_item_avgs)
      @yy_h << {station: station.station_name,
                aqi1: @aqi_days[:aqi1],
                aqi2: @aqi_days[:aqi2],
                aqi3: @aqi_days[:aqi3],
                aqi4: @aqi_days[:aqi4],
                aqi5: @aqi_days[:aqi5],
                aqi6: @aqi_days[:aqi6],
                aqi_days: @aqi_days[:aqi1] + @aqi_days[:aqi2] + @aqi_days[:aqi3] + @aqi_days[:aqi4] + @aqi_days[:aqi5] + @aqi_days[:aqi5] + @aqi_days[:aqi6]
      }
    end
  end

  def handle_aqi(item_arrs)
    aqi_1, aqi_2, aqi_3, aqi_4, aqi_5, aqi_6 = 0, 0, 0, 0, 0, 0
    item_arrs.each do |pre_item_avg|
      aqi = laqi_handle(pre_item_avg.so2_avg,
                        pre_item_avg.co_avg,
                        pre_item_avg.no2_avg,
                        pre_item_avg.o3_avg,
                        pre_item_avg.pm2_5_avg,
                        pre_item_avg.pm10_avg)
      if aqi == "优"
        aqi_1 += 1
      elsif aqi == "良"
        aqi_2 += 1
      elsif aqi == "轻度污染"
        aqi_3 += 1
      elsif aqi == "中度污染"
        aqi_4 += 1
      elsif aqi == "重度污染"
        aqi_5 += 1
      elsif aqi == "严重污染"
        aqi_6 += 1
      else
        next
      end
    end
    {aqi1: aqi_1, aqi2: aqi_2, aqi3: aqi_3, aqi4: aqi_4, aqi5: aqi_5, aqi6: aqi_6}
  end


end
