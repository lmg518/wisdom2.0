class PowerCutRecordsController < ApplicationController
  before_action :set_work, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user # 转存公共参数

  def index
    #按条件查询
    @unit_yw = params[:unit_yw] # 按运维单位
    #@station_name = params[:station_name] # 按站点
    @job_status = params[:job_status] # 按工单状态
    @station_id = params[:station_id] # 按站点id

    @origin = params[:origin] # 按来源
    @audit_des = params[:audit_des] # 按回退原因

    #获取创建开始时间
    @created_at=params[:created_at].to_s + " 00:00:00" if params[:created_at].present?
    #获取结束时间
    @end_at=params[:end_at].to_s + " 23:59:59" if params[:end_at].present?

    # @powerCutRecords = DPowerCutRecord.by_unit_yw(@unit_yw).by_station_ids(@station_id).by_job_status(@job_status).by_origin(@origin).by_audit_des(@audit_des).by_created_at(@created_at,@end_at).order(:created_at => :desc)
    # @powerCutRecords = @powerCutRecords.page(params[:page]).per(params[:per])

    #运维人员登录时只能查看自己的运维工单
    if @current_user.s_role_msg.role_name == '运维成员'
       @powerCutRecords = DPowerCutRecord.by_handle_man(@current_user.login_name)
                                         .by_unit_yw(@unit_yw)
                                         .by_station_ids(@station_id)
                                         .by_job_status(@job_status)
                                         .by_origin(@origin)
                                         .by_audit_des(@audit_des)
                                         .by_created_at(@created_at,@end_at)
                                         .order(:created_at => :desc)
                                         .page(params[:page]).per(params[:per])

    else
        @powerCutRecords = DPowerCutRecord.by_unit_yw(@unit_yw)
        .by_station_ids(@station_id)
        .by_job_status(@job_status)
        .by_origin(@origin)
        .by_audit_des(@audit_des)
        .by_created_at(@created_at,@end_at)
        .order(:created_at => :desc)
        .page(params[:page]).per(params[:per])
    end







  end

  #从set_user 获取数据
  def show
  end

  #根据运维单位获取所有运维人员
  #power_cut_records/get_all_msgs.json
  def get_all_msgs
    @s_region_code_infos=SRegionCodeInfo.all
  end




  # GET /power_cut_records/1/edit
  def edit
  end

  def new
    @powerCutRecord=DPowerCutRecord.new
  end

  #审核 
  # GET  /power_cut_records/89/power_audit
  def power_audit
    
    @powerCutRecord = DPowerCutRecord.find(params[:id])
    audit_cause = params[:d_power_cut_record][:audit_des]  #退回原因
    audit_if= params[:d_power_cut_record][:audit_if]       #根据审核条件判断是否生成 审核不通过日志

    job_status=@powerCutRecord.job_status   #获取工单状态
    @powerCutRecord.audit_man=@current_user.login_name     #获取当前的审核人
    @powerCutRecord.audit_time=Time.now                    #更新审核时间

    if audit_if == 'W' && job_status =='Y'
      @powerCutRecord.job_status='W'
      @powerCutRecord.audit_if='W'
      @powerCutRecord.save
      render :index
    elsif audit_if == 'N' && audit_cause.present? && job_status =='Y'
      @powerCutRecord.job_status='N'
      @powerCutRecord.audit_des = audit_cause
      @powerCutRecord.save

      #保存关联数据到另一张表 创建不通过日志
      @power_cut_audit_log=DPowerCutAuditLog.new(:audit_cause => audit_cause, :note => "", :d_power_cut_record_id => @powerCutRecord.id)
      @power_cut_audit_log.save
      render :index
    elsif audit_if == 'N' && !audit_cause.present? && job_status =='Y'  #不通过时没有不通过原因
      render json: {status: 'false'}
    end

  end

  #上报  取消了跟APP一致
  # GET  /power_cut_records/89/report
  def report
    @powerCutRecord = DPowerCutRecord.find(params[:id])
    #@operation=params[:d_power_cut_record][:job_status]  #获取操作内容
    @powerCutRecord.job_status='Y'
    @powerCutRecord.save
    render :index
  end

  #图片上传方法
  def upload_img(image_file,powerCutRecord)

    img_flag = "jpeg"  if image_file[0,15] == "data:image/jpeg"
    img_flag = "png"  if image_file[0,14] == "data:image/png"


    new_name = "power_img#{Time.now.strftime('%Y%m%d%H%M%S')}.#{img_flag}"  #新名字

    png = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])   #解析文件

    time = Time.now
    #new_name = "#{powerCutRecord.id}.jpg"

    file_path = "#{Rails.root}/public/test/#{time.year}/#{time.month}/#{time.day}"
    FileUtils.mkdir_p(file_path) unless File.exist?(file_path)

    File.open(Rails.root.join("#{file_path}", new_name ), 'wb') { |f| f.write(png) }
   # File.open(Rails.root.join("public","task_app_images", new_name ), 'wb') { |f| f.write(png) }

    #powerCutRecord.img_path="task_app_images/#{new_name}"  #将路径保存到数据库中
    powerCutRecord.img_path = "/test/#{time.year}/#{time.month}/#{time.day}"+'/'+"#{new_name}"


    #自动计算时间跨度  /3600
    time_rang=(powerCutRecord.end_time - powerCutRecord.begin_time).to_i
    powerCutRecord.time_range=sprintf("%.1f",time_rang/3600.0)  #保留一位小数点 

    powerCutRecord.d_station_id=params[:d_power_cut_record][:d_station_id]
    powerCutRecord.audit_if="N"    # N 未审核   W 已审核
    powerCutRecord.job_status="Y"  # 工单状态  C待上报 Y待审核 W审核通过  N审核不通过
  end


  #创建，新增
  def create
    note = params[:d_power_cut_record][:note]               #备注
    @image_file = params[:d_power_cut_record][:image_file]  #获取图片
    if @image_file.present?
      @powerCutRecord = DPowerCutRecord.new(work_params)
      upload_img(@image_file,@powerCutRecord)
    else
      return render json: {status: 'false'}
    end
    respond_to do |format|
      if @powerCutRecord.save
        format.json {render :index}
        format.html {redirect_to power_cut_record_path(@powerCutRecord), notice: 'Successfully create!'}
      else
        format.json {render json: {status: 'false', location: @powerCutRecord.errors}}
        format.html {render :new}
      end
    end
  end

  #编辑修改数据
  # PATCH/PUT /power_cut_records/1
  def update
    job_status = params[:d_power_cut_record][:job_status]          #工单状态 
    @image_file = params[:d_power_cut_record][:image_file]         #获取图片

    respond_to do |format|
      if @powerCutRecord.update(work_params) && @image_file.present?
        upload_img(@image_file,@powerCutRecord)
        @powerCutRecord.job_status="Y" #修改工单状态(审核不通过 的需要修改,Y:待审核，取消上报操作) 
        @powerCutRecord.save
        format.json {render :index}
        format.html {}
      else
        format.json {render json: {status: 'false'}}
        format.html {}
      end
    end
  end

  #删除后跳转到首页  # DELETE   /power_cut_records/1
  def destroy
    respond_to do |format|
      if @powerCutRecord.destroy
        format.json {render :index}
        format.html {redirect_to power_cut_records_path, notice: 'Successfully destroy!'}
      else
        format.json {render json: {status: 'false'}}
        format.html{}
      end
    end
  end

  #给:show, :edit, :update, :destroy  提供数据
  private
  def set_work
    @powerCutRecord = DPowerCutRecord.find(params[:id])
    @power_cut_audit_logs = @powerCutRecord.d_power_cut_audit_logs  #关联到日志
  end

  #获取表单中的参数
  def work_params
    params.require(:d_power_cut_record).permit(:id, :station_name,:d_station_id, :unit_yw, :begin_time, :end_time, :exception_name, :exception_device_type, :handle_man, :origin, :note, :audit_des,:audit_if)
  end


end
