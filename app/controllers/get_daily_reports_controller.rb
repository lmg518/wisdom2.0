class GetDailyReportsController < ApplicationController
  before_action :authenticate_user
  
  #日生产报表

  def index


    #获取查询参数
    end_day1 = params[:end_day]
    params_region_ids = params[:region_id]


    #根据登录人员 获取人员所在的车间 或公司
    @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    

    Rails.logger.info "-------所在车间 或分公司---#{@region_code1.region_name}----"

    #当天的 日报表
    time_begion=Time.now.beginning_of_month.strftime("%Y-%m-%d")

    if end_day1.present?
      end_day = end_day1
    else
      end_day = Time.now.end_of_month.strftime("%Y-%m-%d")
    end
    #time = Time.now.strftime("%Y-%m-%d")



      #判断 是 车间级 还是  分公司级
      if @region_code1.s_region_level_id == 1   #2 分公司   1 总公司   3 车间
        if params_region_ids.present?   #使用查询的ids
          region_code_ids = params_region_ids
        else
          region_z = SRegionCode.where(:s_region_level_id => 3)
          region_code_ids = region_z.pluck(:id)  #查询分公司下的的所有车间
        end
        elsif @region_code1.s_region_level_id == 2
          if params_region_ids.present?   #使用查询的ids
            region_code_ids = params_region_ids
          else
            region_f = SRegionCode.where(:father_region_id => @region_code1.id)
            region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
          end
        elsif @region_code1.s_region_level_id == 3
          if params_region_ids.present?   #使用查询的ids
            region_code_ids = params_region_ids
          else
            @region_code = @region_code1
            region_code_ids = @region_code.id
          end
        end
  

      Rails.logger.info "-region_code_ids----#{region_code_ids}-------"


    #  :created_at => :desc
    @daily_reports = DDailyReport.where(:s_region_code_id => region_code_ids)
                                  .by_datetime(end_day,end_day)  #按日期查询
                                  .order(:s_region_code_id => :desc, :created_at => :desc)
                                  .page(params[:page]).per(params[:per])

        
   @sum_dates = DDailyReport.by_datetime(time_begion, end_day).where(:s_region_code_id => region_code_ids, :status => 'Y')

  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    @work=DWorkPro.new
  end

  #成功跳转到show页面
  def create
    @work = DWorkPro.new(work_params)
    respond_to do |format|
      if @work.save
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Successfully create!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :new}
      end
    end
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @work.destroy
        format.html {redirect_to work_pros_path, notice: 'Successfully destroy!'}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @work.update(work_params)
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    @work = DWorkPro.find(params[:id])
  end

  def work_params
    params.require(:d_work_pro).permit(:id, :work_name, :work_code, :work_type, :work_flag)
  end

end
