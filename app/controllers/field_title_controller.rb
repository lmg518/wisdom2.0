class FieldTitleController < ApplicationController
    before_action :current_user
    before_action :get_field_title, only: [:show,:update,:destroy]


    # def index
    #     @field_titles=FFieldTitle.all.order(:created_at => :desc)
    # end

     #首页信息
     def table_infos
        @funcs=FFieldTitle.all.order(:created_at => :desc)
        @funcs=@funcs.page(params[:page]).per(params[:per])

        @list_size = @funcs.total_count

        render :partial => 'table_infos', :layout => false
    end

    #新建字段页面
    def new
        render :partial => "new", :layout => false
    end

    def show
        render :partial => "show", :layout => false
    end

    #保存模板
    def create
        retCode = true
        @field_title = FFieldTitle.new(field_title_params)
        respond_to do |format|

        # 写日志
        wLoginOpr = WLoginOpr.new
        wLoginOpr.op_code   = "field_title|create"
        wLoginOpr.op_time   = Time.now.to_s
        wLoginOpr.login_no  = current_user.login_no
        wLoginOpr.ip_addr   = current_user.ip_address
        wLoginOpr.op_note   = "创建人：" + current_user.login_name
        wLoginOpr.op_describe = "新增模板字段，字段名：" + @field_title.name

        @field_title.transaction do
          retCode = @field_title.save
          retCode = wLoginOpr.save if retCode
        end

        if retCode
          format.json { render json: {status: :ok, location: @field_title} }
        else
          format.json { render json: @field_title.errors, status: :unprocessable_entity }
        end
      end
    end

    def update
        retCode = true
        respond_to do |format|

        # 写日志
        wLoginOpr = WLoginOpr.new
        wLoginOpr.op_code   = "field_title|update"
        wLoginOpr.op_time   = Time.now.to_s
        wLoginOpr.login_no  = current_user.login_no
        wLoginOpr.ip_addr   = current_user.ip_address
        wLoginOpr.op_note   = "修改人:" + current_user.login_name
        wLoginOpr.op_describe = "修改模板字段，修改后字段名：" + @field_title.name

        @field_title.transaction do
          retCode = @field_title.update(field_title_params)
          retCode = wLoginOpr.save if retCode
        end

          if retCode
            format.json { render json: {status: :ok, location: @field_title} }
          else
            format.json { render json: @field_title.errors, status: :unprocessable_entity }
          end
        end
    end

    def destroy
        retCode = true
        respond_to do |format|

        # 写日志
        wLoginOpr = WLoginOpr.new
        wLoginOpr.op_code   = "field_title|destroy"
        wLoginOpr.op_time   = Time.now.to_s
        wLoginOpr.login_no  = current_user.login_no
        wLoginOpr.ip_addr   = current_user.ip_address
        wLoginOpr.op_note   = "删除人：" + current_user.login_name
        wLoginOpr.op_describe = "删除模板字段，字段名：" + @field_title.name

        @field_title.transaction do
          retCode = @field_title.destroy
          retCode = wLoginOpr.save if retCode
        end

        if retCode
          format.json { render json:{ status: :ok } }
        else
          format.json { render json: @field_title.errors, status: :unprocessable_entity }
        end
      end
    end

    private
    def get_field_title
      @field_title = FFieldTitle.find(params[:id])
    end

    def field_title_params
        params.require(:f_field_title).permit(:name, :code, :title_if_value)
    end

end