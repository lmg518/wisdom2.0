class SuppliesController < ApplicationController
  before_action :get_supply, only: [:update, :show, :edit, :destroy]
  #运维配件管理
  def index
    @supplies = DSupply.s_by_name(params[:sypply_name])
                    .s_by_no(params[:supply_no])
                    .s_by_brand(params[:brand])
                    .s_by_unit_ids(params[:unit_ids])
    @supplies = @supplies.page(params[:page]).per(params[:per])
  end

  def show
    render json:{supply: {:unit_name =>@supply.unit_name,:s_region_code_info_id =>@supply.s_region_code_info_id,
                          :city_name =>@supply.city_name,:s_administrative_area_id =>@supply.s_administrative_area_id,
                          :brand =>@supply.brand,:supply_name =>@supply.supply_name,:supply_no =>@supply.supply_no,
                          :supply_num =>@supply.supply_num,:supply_unit =>@supply.supply_unit,
                          :validity_date =>@supply.validity_date.strftime("%Y-%m-%d"),:owner =>@supply.owner,
                          :d_login_msg_id =>@supply.d_login_msg_id },
                 brands: SBrandMsg.pluck(:brand), provinces: SAdministrativeArea.all_provinces}
  end

  def update
    respond_to do |format|
      if @supply.update(params_to_supply)
        format.html {}
        format.json {render json: {status: "success", location: @supply}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @supply.errors}}
      end
    end
  end

  def create
    @supply = DSupply.new(params_to_supply)
    respond_to do |format|
      if @supply.save
        format.html {}
        format.json {render json: {status: "success", location: @supply}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @supply.errors}}
      end
    end
  end

  def destroy
    respond_to do |format|
      if @supply.destroy
        format.html {}
        format.json {render json: {status: "success"}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @supply.errors}}
      end
    end
  end

  private
  def get_supply
    @supply = DSupply.find(params[:id])
  end

  def params_to_supply
    params.require(:supply).permit(:unit_name, :s_region_code_info_id, :brand,
                                   :city_name,
                                   :s_administrative_area_id,
                                   :supply_name, :supply_no, :supply_num,
                                   :supply_unit, :validity_date, :owner,
                                   :d_login_msg_id)
  end

end