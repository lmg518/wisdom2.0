class DGetYearsController < ApplicationController
include AqiHandleHelper

#GET   /d_get_years
def index
    citys = params[:citys]
    begin_time = params[:begin_time]
    end_time = params[:end_time]
    if citys.present? && begin_time.present? && end_time.present?
        stations = SRegionCode.find(citys).d_stations.select(:id)
        station_ids = []
        stations.each{|x|  station_ids << x[:id] }
        dailys = DDataDaily.where(station_id: station_ids)
                            .by_data_time(begin_time,end_time)
                            .order(:data_time => 'asc')	
        t1 = Time.parse(begin_time)
        @aqi_count = []
        time = ((Time.parse(end_time) - Time.parse(begin_time))/3600/24).round()
        time.times do |t|
            daily = dailys.select{|x| x.data_time == t1}
            if daily.length == 0
                @aqi_count << {"data_time" =>t1,"aqi_avg" =>'',"aqi_colour" => '#ffffff'}
            else
                aqi_avg = get_aqi_avg(daily)
                @aqi_count << {"data_time" =>t1,"aqi_avg" =>aqi_avg,"aqi_colour" => get_aqi_level(aqi_avg)} 
            end
            t1 += (3600 * 24)
        end
        #页面aqi级别统计
        @count_num = { :excellent_num =>"#{@aqi_count.select{|x| x['aqi_avg'] == "优"}.length }",
                        :good_num => "#{@aqi_count.select{|x| x['aqi_avg'] == "良"}.length }",
                        :mild_num => "#{@aqi_count.select{|x| x['aqi_avg'] == "轻度污染"}.length }",
                        :moder_num => "#{@aqi_count.select{|x| x['aqi_avg'] == "中度污染"}.length }",
                        :severe_num => "#{@aqi_count.select{|x| x['aqi_avg'] == "重度污染"}.length }",
                        :serious_num => "#{@aqi_count.select{|x| x['aqi_avg'] == "严重污染"}.length }"
                    }
        #Rails.logger.info ">>>#{@aqi_count[0]["aqi_level"]}"
    else
        @aqi_count = []
        @count_num = {:excellent_num => 0 ,
                        :good_num => 0,
                        :mild_num => 0,
                        :moder_num => 0,
                        :severe_num => 0,
                        :serious_num => 0 }
     end

end

#获取区域
#GET /d_get_years/get_region_code
def get_region_code
    @region_code = SRegionCode.select(:id,:region_code,:region_name,:father_region_id)
    render json: {"region_code": @region_code}
end

#获取年份
#GET /d_get_years/get_year
def get_year
    years = []
    year = Time.now
    #需要包括当前年份的前5个年份，循环求出年份，存入数组中
    5.times do |t|
        time = (year - t.year).strftime("%Y")
        years << time
    end
    render json: {"year": years}
end




private

#计算区域每天的aqi平均值
def get_aqi_avg(daily)
    aqi_avg = []
    # add_by_guoyunpeng+<2018-01-02 10:34> begin
    #先把匹配时间区域下的各个站点中的监测物的aqi相加，然后再除以区域的站点数量求平均
    aqi_so2,aqi_no2,aqi_co,aqi_1h_o3,aqi_ma8_o3,aqi_pm10,aqi_pm25 = 0,0,0,0,0,0,0
    arg_len = daily.length
    daily.each do |a|
        aqi_so2 += a.avg_so2
        aqi_co += a.avg_co
        aqi_no2 += a.avg_no2
        aqi_1h_o3 += a.max_avg_1h_o3
        aqi_pm25 += a.avg_pm25
        aqi_pm10 += a.avg_pm10
        aqi_ma8_o3 += a.max_ma8_o3
    end
    so2 = aqi_so2 >0 ? aqi_so2 / arg_len : 0
    co = aqi_co >0 ? aqi_co/arg_len : 0
    no2 = aqi_no2 >0 ? aqi_no2 / arg_len : 0
    h1_o3 = aqi_1h_o3 >0 ? aqi_1h_o3 / arg_len : 0
    pm25 = aqi_pm25 >0 ? aqi_pm25/arg_len : 0
    pm10 = aqi_pm10 >0 ? aqi_pm10 / arg_len : 0
    h8_o3 = aqi_ma8_o3 >0 ? aqi_ma8_o3 / arg_len : 0
    #调用laqi_handle方法来得出区域AQI的指数质量级别
    laqi_handle(so2,co,no2,h1_o3,pm25,pm10,h8_o3)
    # add_by_guoyunpeng+<2018-01-02 10:34> add_end
end

#匹配aqi级别返回所属颜色
def get_aqi_level(aqi)
    case aqi
    when '优' 
        return '#00e400'  #优
    when '良'
        return '#ffff00'  #良
    when '轻度污染'
        return '#ff7e00'  #轻度污染
    when '中度污染'
        return '#ff0000'  #中度污染
    when '中度污染'
        return '#99004c'  #重度污染
    when '严重污染'
        return '#7e0023'  #严重污染
    else
        return '#ffffff'
    end
end


# def get_aqi_avg(daily)
#     aqis = 0
#     aqi_len = arg.length
#     # daily.each do |x| 
#     #     aqis += x.aqi
#     # end
#     daily.each{|x| aqis += x.aqi}
#     aqi_avg = (aqis / aqi_len)
#     return aqi_avg
# end

# #匹配aqi级别返回所属颜色
# def get_aqi_level(aqi)
#     case aqi
#     when 0..50 
#         return '#00e400'  #优
#     when 51..100
#         return '#ffff00'  #良
#     when 101..150
#         return '#ff7e00'  #轻度污染
#     when 151..200
#         return '#ff0000'  #中度污染
#     when 201..300
#         return '#99004c'  #重度污染
#     else
#         return '#7e0023'  #严重污染
#     end
# end



end