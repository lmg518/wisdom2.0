class EnergyDailyFormController < ApplicationController
  include EnergyFormHelper
  before_action :authenticate_user
  #能源日报表   用电  循环水  一次水

  $flag = false    #差值计算和不差值计算的 标识  true 差值计算   false 不差值计算


  #计算 公司 总产量 
  def get_company_finished(before_time)
    time  = before_time
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
    @company_day_total = 1    #日用量      计算日单耗时  除数不能为0
    @company_month_total = 1  #累计用量

    @na_form_values = DDailyReport.find_by(:s_region_code_id => 8 , :code => 'na_input', :datetime => time)  # 8 烘包车间  na_input 钠入库
    @sour_form_values = DDailyReport.find_by(:s_region_code_id => 8 , :code => 'sour_input', :datetime => time)  # 8 烘包车间  sour_input 酸入库

    @na_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8 , :code => 'na_input') 
    @sour_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8 , :code => 'sour_input') 
    
    if @na_form_values.present? && @sour_form_values.present?
      @company_day_total = (@na_form_values.nums.to_f + @sour_form_values.nums.to_f / 0.73).round(2)
    end

    d1 = 0
    d2 = 0
    @na_form_values_month.each do |m|
      d1 += m.nums.to_f
    end
    @sour_form_values_month.each do |m|
      d2 += m.nums.to_f
    end
    @company_month_total = (d1 + d2 / 0.73).round(2)  ##累计用量
  end

#差值计算能源新加  根据 s_region_code_id  d_report_form_id
def js_method_by_region_id_form_id(time, s_region_code_id, d_report_form_id)
  @steam_values = 0
  @steam_month_sum = 0

   #计算差值
  bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
  month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期

  @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => time) 
  @form_values_bf = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => bf_time)  
    
  @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id)                   
  min_time = @form_values_month.minimum("datetime")   #获取最小日期
  max_time = @form_values_month.maximum("datetime")   #获取最大日期
  @form_values_min = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => min_time)                  
  @form_values_max = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => max_time)                  

  d1 = 0
  d2 = 0
  @form_values.each do |m|
    d1 += m.field_value.to_f
  end
  @form_values_bf.each do |m|
    d2 += m.field_value.to_f
  end
  @steam_values = (d1 - d2).round(2)   #日用量

  dd1 = 0
  dd2 = 0
  @form_values_max.each do |m|
    dd1 += m.field_value.to_f
  end
  @form_values_min.each do |m|
    dd2 += m.field_value.to_f
  end
  @steam_month_sum = (dd1 - dd2).round(2)   #累计用量
end

#差值计算方法1 计算多个项目的，参数有项目的
def js_method_many(time, s_region_code_id, d_report_form_id, item)
  @steam_values = 0
  @steam_month_sum = 0

   #计算差值
  bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
  month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期

  @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => time, :field_code => item) 
  @form_values_bf = SNenrgyValue.where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :datetime => bf_time, :field_code => item)  
    
  @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id , :d_report_form_id => d_report_form_id, :field_code => item)                   
  min_time = @form_values_month.minimum("datetime")   #获取最小日期
  max_time = @form_values_month.maximum("datetime")   #获取最大日期
  @form_values_min = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => min_time, :field_code => item)                  
  @form_values_max = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => max_time, :field_code => item)                  

  d1 = 0
  d2 = 0
  @form_values.each do |m|
    d1 += m.field_value.to_f
  end
  @form_values_bf.each do |m|
    d2 += m.field_value.to_f
  end
  @steam_values = (d1 - d2).round(2)   #日用量

  dd1 = 0
  dd2 = 0
  @form_values_max.each do |m|
    dd1 += m.field_value.to_f
  end
  @form_values_min.each do |m|
    dd2 += m.field_value.to_f
  end
  @steam_month_sum = (dd1 - dd2).round(2)   #累计用量
end

  #差值计算方法 2  计算单个项目的  item 项目参数名
  def js_method_one(time, s_region_code_id, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期

    @form_values = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)  
    @form_values_bf = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id,:field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)  
   
    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id,:field_code => item, :d_report_form_id => d_report_form_id)  
    min_time = @form_values_month.minimum("datetime")   #获取最小日期
    max_time = @form_values_month.maximum("datetime")   #获取最大日期

    @form_values_min = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)                  
    @form_values_max = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)                  
    
    if @form_values.present? && @form_values.field_value.present?
      d1 = @form_values.field_value.to_f
    else
      d1 = 0
    end

    if @form_values_bf.present? && @form_values_bf.field_value.present?
      d2 = @form_values_bf.field_value.to_f
    else
      d2 = 0
    end

    #添加判断
    if @form_values_min.present? && @form_values_min.field_value.present?
      min = @form_values_min.field_value.to_f
    else
      min = 0
    end
    if @form_values_max.present? && @form_values_max.field_value.present?
      max = @form_values_max.field_value.to_f
    else
      max = 0
    end
    @steam_month_sum = max - min

    @steam_values = d1 - d2  #昨天的 - 前天的
    #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f   #月累计用量
  end

  #差值计算方法 3  计算单个项目的  能源使用的
  def js_method_energy(time, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期

    @form_values = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)  
    @form_values_bf = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)  
   
    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:field_code => item, :d_report_form_id => d_report_form_id)  
    min_time = @form_values_month.minimum("datetime")   #获取最小日期
    max_time = @form_values_month.maximum("datetime")   #获取最大日期

    @form_values_min = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)                  
    @form_values_max = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)                  
    
    if @form_values.present? && @form_values.field_value.present?
      d1 = @form_values.field_value.to_f
    else
      d1 = 0
    end

    if @form_values_bf.present? && @form_values_bf.field_value.present?
      d2 = @form_values_bf.field_value.to_f
    else
      d2 = 0
    end

     #添加判断
     if @form_values_min.present? && @form_values_min.field_value.present?
      min = @form_values_min.field_value.to_f
    else
      min = 0
    end
    if @form_values_max.present? && @form_values_max.field_value.present?
      max = @form_values_max.field_value.to_f
    else
      max = 0
    end
    @steam_month_sum = max - min

    @steam_values = d1 - d2  #昨天的 - 前天的
    #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f   #月累计用量
  end




  #用电
  def energy_form_power
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
    end
    @time = before_time
    get_company_finished(before_time)   #计算成品入库量  #返回  @company_day_total 日的  @company_month_total累计的
    get_datas_power(before_time)
    render :partial => "energy_form_power", :layout => false
  end

   #循环水
   def energy_form_water
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
    end
    @time = before_time
    get_company_finished(before_time)   #计算成品入库量
    get_datas_water(before_time)
    render :partial => "energy_form_water", :layout => false
   end

   #一次水
   def energy_form_once_water
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
    end
    @time = before_time
    get_company_finished(before_time)   #计算成品入库量
    get_datas_once_water(before_time)
    render :partial => "energy_form_once_water", :layout => false
   end

   #排污报表
   def energy_form_pollution
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
    end
    @time = before_time
    get_company_finished(before_time)   #计算成品入库量
    get_datas_pollution(before_time)
    render :partial => "energy_form_pollution", :layout => false
   end

   #蒸汽报表
   def energy_form_steam
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
    end
    @time = before_time
    get_company_finished(before_time)   #计算成品入库量
    get_datas_steam(before_time)
    render :partial => "energy_form_steam", :layout => false
   end


  #蒸汽报表   EnergyDailyFormController.new.get_datas_steam
  def get_datas_steam(before_time)
    form_code = params[:code]  #表单code
    #time = Time.now.strftime("%Y-%m-%d")
    time = before_time
    
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => current_user.group_id)  #获取人员所在车间 或公司

    @d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
    report_form_id = @d_report_forms.id   #报表id

    #表格头信息
    @form_name = @d_report_forms.name  #表格名称
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")  #报表类型id
    @region_datas = []  #第一层  存类型  蒸汽 甲醇母液
    @form_values = SNenrgyValue.where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :datetime => time)

    #蒸汽报表合成车间 合计计算 日单耗 = 日蒸汽/ 合成车间日报表的日产量粗品     月单耗 = 月蒸汽/ 合成车间日报表的月产量粗品
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => 5, :datetime => time, :code => 'CP')   # CP  在 s_region_code_id = 5
    @daily_report_cp_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 5, :code => 'CP')
    
    @cp_day_nums = @daily_report_cp.present? ? @daily_report_cp.nums : 1    #合成车间日报表的日产量粗品

    @cp_month_nums = 0          #粗品月累计
    if @daily_report_cp_month.present? && @daily_report_cp_month.length >0
      @daily_report_cp_month.each do |d1|
        @cp_month_nums += d1.nums.to_f
      end
    else
      @cp_month_nums = 1   #数据不存在是 默认值为 1  除数不能为0
    end
    #----------------
    #精制车间日产量=钠包装量+酸包装量*0.73
    #精制车间 计算 日单耗 = 日蒸汽/ 精制车间日产量     月单耗 = 月蒸汽/ 精制车间月产量
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
    @daily_report_na = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'na_nums')   # na_nums  在 s_region_code_id = 8
    @daily_report_na_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'na_nums')
    @daily_report_suan = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'suan_nums')
    @daily_report_suan_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'suan_nums')

    na_day_nums = @daily_report_na.present? ? @daily_report_na.nums : 1        #钠日产量
    suan_day_nums = @daily_report_suan.present? ? @daily_report_suan.nums : 1  #酸日产量
    @nums_day_jz =  (na_day_nums + suan_day_nums * 0.73).round(2)    #精制车间日产量
    dd1= 0
    dd2 =0
    @daily_report_na_month.each do |d1|
      dd1 += d1.nums.to_f
    end
    @daily_report_suan_month.each do |d1|
      dd2 += d1.nums.to_f
    end
    @nums_month_jz = (dd1 + dd2 * 0.73).round(2)    #精制车间 月累计产量
    #----------------



    @region_datas = []  #存类型  蒸汽 甲醇母液
    if type_ids.length >0
      type_ids.each do |id|
        item1 = {}  #存类型  
        nenrgy_type = SNenrgyFormType.find_by(:id => id)
        item1[:type_name] = nenrgy_type.name   #类型
        region_ids =  steam_region_codes = DSteamRegionCode.where(:s_nenrgy_form_type_id => id).pluck(:s_region_code_id).uniq  # 要去重（车间）   蒸汽项目与车间 类型的关联
        @s2 = []  #存车间 
        region_ids.each do |region_id|
          item2 = {}  #存车间
          region_code = SRegionCode.find_by(:id => region_id)
          item2[:region_name] = region_code.region_name
          item2[:region_id] = region_code.id  #车间id
          @form_values = SNenrgyValue.where(:s_region_code_id => region_code.id, :d_report_form_id => report_form_id, :datetime => time)
          @ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => id, :s_region_code_id => region_id).pluck(:s_material_id)
         
          #找车间下的项目
          s_materials = SMaterial.where(:id => @ids) 
          @s3 = []  #存车间下的 岗位
          s_materials.each do |m|  #车间下的项目
            item3 = {}
            item3[:name] = m.name
            item3[:field_code] = m.code
            if $flag
              js_method_one(time, region_code.id, report_form_id, m.code)  
              #返回  @steam_values 日的  @steam_month_sum 累计的
            else
              #不差值计算
              @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, region_code.id, report_form_id, m.code)
            end

            item3[:field_value] = @steam_values.round(2)    #日用量
            item3[:month_sum] = @steam_month_sum.round(2)   #月累计

            # 判断车间显示附加信息
            if 'fermentation' == m.code # 发酵车间

              #--------------v2------------
              #发酵车间蒸汽  总收率  数据
              @daily_report_shy = DDailyReport.find_by(:s_region_code_id => region_code.id, :datetime => time, :code => 'SHY')
              
              if $flag
                js_method_one(time, region_code.id, 3, 'fermentation')
                #返回  @steam_values 日的  @steam_month_sum 累计的
              else
                #不差值计算
                @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, region_code.id, report_form_id, m.code)
              end

              @day_steam_fj = @steam_values
              @steam_month_sum_fj = @steam_month_sum
              month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #查询日期月的开始日期
              #发酵车间计算 日单耗 月单耗
              @shy_nums= @daily_report_shy.present? ? @daily_report_shy.nums : 1   #酸化液的 日产量
              @shy_month_nums = 1   #酸化液的 月产量
              @shy_months = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => region_code.id, :code => 'SHY')  #酸化液的 月累计
              @shy_months.each do |s|
                @shy_month_nums += s.nums.to_f 
                @shy_month_nums = @shy_month_nums -1
              end
              @day_expend_fj = (@day_steam_fj.to_f / @shy_nums.to_f).round(2)  #发酵车间日单耗
              @month_expend_fj = (@steam_month_sum_fj.to_f / @shy_month_nums.to_f).round(2)  #发酵车间月单耗

              item3[:day_expend] = @day_expend_fj         #日单耗
              item3[:month_expend] = @month_expend_fj     #月单耗
              item3[:day_totle] = @shy_nums.round(2)               #日产量
              item3[:month_totle] = @shy_month_nums.round(2)       #月产量
            end

            if ['steam_4',"steam_7",'steam_8', 'steam_9'].include? m.code  # 甲醇塔 烘包  核糖  稀甲醇

              if 'steam_4' == m.code  #甲醇塔 日单耗 = 甲醇塔 steam_4 / 蒸精甲醇 steam_10
                if $flag
                  js_method_one(time, region_code.id, 3, 'steam_4')
                  #返回  @steam_values 日的  @steam_month_sum 累计的
                  d1= @steam_values
                  d2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, region_code.id, 3, 'steam_4')
                  d1= @steam_values
                  d2 = @steam_month_sum
                end


                if $flag
                  js_method_one(time, region_code.id, 3, 'steam_10')
                  dd1 = @steam_values
                  dd2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, region_code.id, 3, 'steam_10')
                  dd1 = @steam_values
                  dd2 = @steam_month_sum
                end

                item3[:day_expend] = (d1.to_f / dd1.to_f).round(2)    #日单耗
                item3[:month_expend] = (d2.to_f / dd2.to_f).round(2)  #月单耗
              end

              if 'steam_7' == m.code  #烘包 日单耗 = 烘包 steam_7 / 公司合计 @company_day_total
                if $flag
                  js_method_one(time, region_code.id, 3, 'steam_7')
                  #返回  @steam_values 日的  @steam_month_sum 累计的
                  s1= @steam_values
                  s2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, region_code.id, 3, 'steam_7')
                  s1= @steam_values
                  s2 = @steam_month_sum
                end

              #   #计算公司合计
              #   #-------------
              #   steam_values = 0
              #   steam_month_sum = 0
              #   if $flag
              #   #计算差值
              #   bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
              #   @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => time)  #   3 蒸汽报表
              #   @form_values_bf = SNenrgyValue.where(:d_report_form_id => 3, :datetime => bf_time)  #   3 蒸汽报表
              #   @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 3)     # 3 蒸汽报表
              #   min_time = @form_values_month.minimum("datetime")   #获取最小日期
              #   max_time = @form_values_month.maximum("datetime")   #获取最大日期
              #   @form_values_min = SNenrgyValue.where(:d_report_form_id => 3, :datetime => min_time)                  
              #   @form_values_max = SNenrgyValue.where(:d_report_form_id => 3, :datetime => max_time)                  
              #   d1 = 0
              #   d2 = 0
              #   @form_values.each do |m|
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     d1 += m.field_value.to_f
              #   end
              #   @form_values_bf.each do |m|
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     d2 += m.field_value.to_f
              #   end
              #   steam_values = (d1 - d2).round(2)   #日用量
              #   dd1 = 0
              #   dd2 = 0
              #   @form_values_max.each do |m|  #不包括  steam_9稀甲醇  steam_10蒸精甲醇  steam_11浓缩母液
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     dd1 += m.field_value.to_f
              #   end
              #   @form_values_min.each do |m|
              #     if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #       next
              #     end
              #     dd2 += m.field_value.to_f
              #   end
              #   steam_month_sum = (dd1 - dd2).round(2)   #累计用量

              # else
              #   #不差值计算
              #   @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => time)  #   3 蒸汽报表
              #   @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 3)     # 3 蒸汽报表
               
              #   #日用量计算
              #   if @form_values.present? && @form_values.length > 0
              #     @form_values.each do |m|
              #       if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #         next
              #       end
              #       steam_values += m.field_value.to_f   #日用量
              #     end
              #   else
              #       steam_values = 1  #数据不存在时  除数默认为1
              #   end

              #   #月累计计算
              #   if @form_values_month.present? && @form_values_month.length > 0
              #     @form_values_month.each do |m|
              #       if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              #         next
              #       end
              #       steam_month_sum += m.field_value.to_f   #日用量
              #     end
              #   else
              #       steam_month_sum = 1  #数据不存在时  除数默认为1
              #   end
              # end


              # item3[:day_expend] = (s1.to_f / steam_values.to_f).round(2)       #日单耗
              # item3[:month_expend] = (s2.to_f / steam_month_sum.to_f).round(2)  #月单耗

              item3[:day_expend] = (s1.to_f /  @company_day_total).round(2)       #日单耗
              item3[:month_expend] = (s2.to_f / @company_month_total).round(2)    #月单耗
            end

              if 'steam_9' == m.code  #稀甲醇 日单耗 = 稀甲醇 steam_9 / 合成车间 合计
                if $flag
                  js_method_one(time, region_code.id, 3, 'steam_9')
                  #返回  @steam_values 日的  @steam_month_sum 累计的
                  d1= @steam_values
                  d2 = @steam_month_sum
                else
                  #不差值计算
                  @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, region_code.id, 3, 'steam_9')
                  d1= @steam_values
                  d2 = @steam_month_sum
                end

                item3[:day_expend] = (d1.to_f / @day_total_hc.to_f).round(2)      #日单耗
                item3[:month_expend] = (d2.to_f / @month_total_hc.to_f).round(2)  #月单耗
              end
            end
           
            @s3.push(item3)
          end


          if region_code.region_code == 'TYSYFGS02'  && nenrgy_type.code == 'zq' #蒸汽 合成车间  添加一个 合计
            item4 = {}
            item4[:name] = '合计'
            item = ['steam_1', 'steam_2', 'steam_3', 'steam_4']

            if $flag
              js_method_many(time, 5, 3, item)  #返回 @steam_values   @steam_month_sum 
            else
              #不差值计算
              @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 5, 3, item)
            end

            @day_total_hc = @steam_values.round(2)
            @month_total_hc = @steam_month_sum.round(2)


            Rails.logger.info "-------!!!!!-----#{@cp_day_nums}---"
            Rails.logger.info "-------!!!!!-----#{@steam_values}---"

            item4[:field_value] = @steam_values
            item4[:month_sum] = @steam_month_sum
            item4[:day_expend] = (@steam_values.to_f / @cp_day_nums).round(2)    #合成车间日单耗
            item4[:month_expend] = (@steam_month_sum / @cp_month_nums).round(2)  #合成车间月单耗
            item4[:day_totle] = @cp_day_nums              #等于 粗品 日产量
            item4[:month_totle] = @cp_month_nums.round(2) #等于 粗品 月累计
            @s3.push(item4)
          end

          if region_code.region_code == 'TYSYFGS05' && nenrgy_type.code == 'zq'  #蒸汽 精制车间  添加一个 合计
            item4 = {}
            item4[:name] = '合计'

            item = ['steam_6', 'steam_5']
            if $flag
              js_method_many(time, 7, 3, item)  #返回 @steam_values   @steam_month_sum 
            else
              #不差值计算
              @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 7, 3, item)
            end

            item4[:field_value] =@steam_values.round(2)
            item4[:month_sum] = @steam_month_sum.round(2)
            item4[:day_expend] = (@steam_values.to_f / @nums_day_jz ).round(2)
            item4[:month_expend] = (@steam_month_sum / @nums_month_jz).round(2)
            item4[:day_totle] = @nums_day_jz
            item4[:month_totle] = @nums_month_jz

            @s3.push(item4)
          end
          item2[:data] = @s3  #车间下的项目
          @s2.push(item2)
        end


        if nenrgy_type.code == 'zq'   # 蒸汽 下 添加一个 公司 公司合计
          item5 = {}
          item5[:region_name] = '公司'

          #-------------
          steam_values = 0
          steam_month_sum = 0

          if $flag
          #计算差值
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
          @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => time)  #   3 蒸汽报表
          @form_values_bf = SNenrgyValue.where(:d_report_form_id => 3, :datetime => bf_time)  #   3 蒸汽报表
    
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 3)     # 3 蒸汽报表
          min_time = @form_values_month.minimum("datetime")   #获取最小日期
          max_time = @form_values_month.maximum("datetime")   #获取最大日期
          @form_values_min = SNenrgyValue.where(:d_report_form_id => 3, :datetime => min_time)                  
          @form_values_max = SNenrgyValue.where(:d_report_form_id => 3, :datetime => max_time)                  
    
          d1 = 0
          d2 = 0
          @form_values.each do |m|
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            d1 += m.field_value.to_f
          end
          @form_values_bf.each do |m|
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            d2 += m.field_value.to_f
          end
          steam_values = (d1 - d2).round(2)   #日用量
          dd1 = 0
          dd2 = 0
          @form_values_max.each do |m|  #不包括  steam_9稀甲醇  steam_10蒸精甲醇  steam_11浓缩母液
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            dd1 += m.field_value.to_f
          end
          @form_values_min.each do |m|
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            dd2 += m.field_value.to_f
          end
          steam_month_sum = (dd1 - dd2).round(2)   #累计用量

        else
          #不差值计算
          @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => time)  #   3 蒸汽报表
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 3)     # 3 蒸汽报表

          #日用量计算
          if @form_values.present? && @form_values.length > 0
            @form_values.each do |m|
              if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
                next
              end
              steam_values += m.field_value.to_f   #日用量
            end
          else
              steam_values = 1  #数据不存在时  除数默认为1
          end

          #月累计计算
          if @form_values_month.present? && @form_values_month.length > 0
            @form_values_month.each do |m|
              if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
                next
              end
              steam_month_sum += m.field_value.to_f   #日用量
            end
          else
              steam_month_sum = 1  #数据不存在时  除数默认为1
          end

        end

        day_expend = (steam_values / @company_day_total).round(2)          #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2)   #月单耗

          # item1[:day_expend]= day_expend        #日单耗
          # item1[:month_expend]= month_expend    #月单耗
          # item1[:steam_values]= steam_values             #日用量
          # item1[:steam_month_sum]= steam_month_sum       #月累计
          #-------------

          item5[:data] = [{:name => '公司合计', :field_value => steam_values.round(2), :month_sum => steam_month_sum.round(2), :day_expend => day_expend, :month_expend => month_expend, :day_totle => @company_day_total, :month_totle => @company_month_total }]
          @s2.push(item5)
        end

        item1[:data] = @s2
        @region_datas.push(item1)
      end
    end
  

  end
   

  #用电报表
  def get_datas_power(before_time)

    form_code = params[:code]  #表单code
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名

    time = before_time

    report_form_id = @d_report_forms.id   #报表id
    @region_datas = []
    
    #当天的值
    #@form_values = SNenrgyValue.where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :datetime => time)
    
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")
    @form_type = SNenrgyFormType.where(:id => type_ids)  #报表类型数据
    @type_name = @form_type[0].name      #公司

    #计算成品入了量
    # @finished_products1 = 58    #日的
    # @finished_products2 = 100   #月累计的

    @finished_products1 = @company_day_total      #日的
    @finished_products2 =  @company_month_total   #月累计的

    #封装项目信息
    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    
    @plants = SMaterial.where(:id => @ids)   #从材料表中获取
    @plants.each do |s|
      item = {}
      item[:name] = s.name
      if $flag
        js_method_one(time, @d_report_forms.s_region_code_id, report_form_id, s.code)  
        #返回  @steam_values 日的  @steam_month_sum 累计的
      else
        #不差值计算
        @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, @d_report_forms.s_region_code_id, report_form_id, s.code)
      end

      item[:field_value] = @steam_values  #日用电量
      item[:month_value] = @steam_month_sum   #月累计用电量
      daily_piece = (@steam_values.to_f / @finished_products1.to_f ).round(2)  #保留二位精度
      month_piece = (@steam_month_sum.to_f / @finished_products2.to_f ).round(2)  #保留二位精度
      item[:daily_piece] = daily_piece  #日单耗
      item[:month_piece] = month_piece  #月单耗

      @region_datas.push(item)
    end
    
  end

  #循环水  报表 数据
  def get_datas_water(before_time)
    form_code = params[:code]  #表单code

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名

    time = before_time

    report_form_id = @d_report_forms.id   #报表id
    @region_datas = []

    
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")
    @form_type = SNenrgyFormType.where(:id => type_ids)  #报表类型数据
    @type_name = @form_type[0].name      #公司

    #计算成品入了量
    @finished_products1 = @company_day_total      #日的
    @finished_products2 =  @company_month_total   #月累计的
    

    #合计的  数据
    if $flag
      js_method_by_region_id_form_id(time, @d_report_forms.s_region_code_id, report_form_id)
      #返回  @steam_values 日的  @steam_month_sum 累计的
    else
      #不差值计算
      @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id(time, @d_report_forms.s_region_code_id, report_form_id)
    end

    @daily_sum = @steam_values
    @month_sum = @steam_month_sum
    @daily_piece = ( @daily_sum / @finished_products1 ).round(2)
    @month_piece = ( @month_sum / @finished_products2 ).round(2)


    #封装项目信息
    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids)   #从材料表中获取
    @plants.each do |s|
      item = {}
      item[:name] = s.name

      if $flag
        js_method_one(time, @d_report_forms.s_region_code_id, report_form_id, s.code)  
        #返回  @steam_values 日的  @steam_month_sum 累计的
      else
        #不差值计算
        @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, @d_report_forms.s_region_code_id, report_form_id, s.code)
      end

      item[:field_value] = @steam_values     #日用量
      item[:month_value] = @steam_month_sum  #月累计
      @region_datas.push(item)
    end
    
  end


  #一次水报表  
  def get_datas_once_water(before_time)
    form_code = params[:code]  #表单code

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名
    time = before_time

    report_form_id = @d_report_forms.id   #报表id
    @region_datas = []
    @form_values = SNenrgyValue.where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => report_form_id, :datetime => time)
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")

    @form_type = SNenrgyFormType.where(:id => type_ids)  #报表类型数据
    @type_name = @form_type[0].name      #公司

    #计算成品入了量
    @finished_products1 = @company_day_total      #日的
    @finished_products2 =  @company_month_total   #月累计的


    #封装项目信息
    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids)   #从材料表中获取
    @plants.each do |s|
      item = {}
      item[:name] = s.name

      if $flag
        js_method_one(time, @d_report_forms.s_region_code_id, report_form_id, s.code)  
        #返回  @steam_values 日的  @steam_month_sum 累计的
      else
        #不差值计算
        @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, @d_report_forms.s_region_code_id, report_form_id, s.code)
      end

      item[:field_value] = @steam_values      #日用量
      item[:month_value] =  @steam_month_sum  #月累计用量

      @region_datas.push(item)
    end
    
  end

  #排污报表
  def get_datas_pollution(before_time)
      form_code = params[:code]  #表单code
      time = before_time
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期

      @d_report_forms = DReportForm.find_by(:code=> form_code)  #根据表格code 来找

      #表格头信息
      @form_name = @d_report_forms.name  #表格名称
      report_form_id = @d_report_forms.id      #表格id

      type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")  #报表类型id

      @form_type = SNenrgyFormType.where(:id => type_ids)  #报表类型数据
      @type_name = @form_type[0].name      #生产排污

      #计算成品入了量
      @finished_products1 = @company_day_total      #日的
      @finished_products2 =  @company_month_total   #月累计的


    #------------------------------------
    if $flag   #差值计算

    #计算低浓度水 日的
    #--------v2----------
    #低浓度水计算
    steam_values = 0    #日的
    steam_month_sum = 0 #月的

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d")   #前天的日期
    @form_values = SNenrgyValue.where(:d_report_form_id => 5, :datetime => time)  #   5 排污报表
    @form_values_bf = SNenrgyValue.where(:d_report_form_id => 5, :datetime => bf_time)  
    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 5)     
    min_time = @form_values_month.minimum("datetime")   #获取最小日期
    max_time = @form_values_month.maximum("datetime")   #获取最大日期
    @form_values_min = SNenrgyValue.where(:d_report_form_id => 5, :datetime => min_time)                  
    @form_values_max = SNenrgyValue.where(:d_report_form_id => 5, :datetime => max_time)                  
    d1 = 0
    d2 = 0
    @form_values.each do |m|
      if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
        next
      end
      d1 += m.field_value.to_f
    end
    @form_values_bf.each do |m|
      if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
        next
      end
      d2 += m.field_value.to_f
    end

    steam_values = (d1 - d2).round(2)   #日用量

    dd1 = 0
    dd2 = 0
    @form_values_max.each do |m|  ##去除二个  pollution_6甲醇废水    pollution_7洗效水
      if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
        next
      end
      dd1 += m.field_value.to_f
    end
    @form_values_min.each do |m|
      if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
        next
      end
      dd2 += m.field_value.to_f
    end
    steam_month_sum = (dd1 - dd2).round(2)   #累计用量
    @low_water = steam_values
    @low_month_sum = steam_month_sum

  else
    #-------不差值计算-----------
    @low_water = 0   #低浓度水日用量
    @low_water_values = SNenrgyValue.where(:d_report_form_id => '5', :datetime => time)  #要去除二个  甲醇废水 洗效水
    if @low_water_values.present? && @low_water_values.length > 0
      @low_water_values.each do |low|
        if low.field_code == 'pollution_6' || low.field_code == 'pollution_7' then
          next
        end
        @low_water += low.field_value.to_f
      end
    end
    #计算低浓度 月累计
    @low_month_sum = 0
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
    @low_month_values = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => report_form_id)
    if @low_month_values.present? && @low_month_values.length > 0
      @low_month_values.each do |m|
        if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then # pollution_6 ==甲醇废水 pollution_7=洗效水
          next
        end
        @low_month_sum += m.field_value.to_f
      end
    end

  end




    #---------v2--------
    if $flag
      js_method_energy(time, 5, 'pollution_6')  #d_report_form_id  5  item  高浓度水
      #返回  @steam_values 日的  @steam_month_sum 累计的
    else
      #不差值计算
      @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_item_form_id_time('pollution_6', 5, time)
    end


    @high_water = @steam_values
    @hig_month_sum = @steam_month_sum
    @low_water_piece = ( @low_water / @finished_products1 ).round(2)   #低浓度水 日单耗
    @hig_water_piece = ( @high_water / @finished_products1 ).round(2)  #高浓度水 日单耗
    @day_low_water_piece = ( @low_month_sum / @finished_products2 ).round(2)   #低浓度水 月单耗
    @month_hig_water_piece = ( @hig_month_sum / @finished_products2 ).round(2) #高浓度水 月单耗

      
      #  车间和排污项目关联表
      @region_code_ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_region_code_id).uniq  #去重处理

      @region_datas = []
      @region_code_ids.each do |id|
        item = {} 
        region_code = SRegionCode.find_by(:id => id)
        item[:region_name] = region_code.region_name   #车间名称
        item[:region_id] = region_code.id              #车间id
        
        #根据车间 取值
        @form_values = SNenrgyValue.where(:s_region_code_id => region_code.id, :d_report_form_id => '5', :datetime => time)

        @steam_code = DSteamRegionCode.where(:s_region_code_id => id, :s_nenrgy_form_type_id => '5')  
        s_material_ids = @steam_code.pluck(:s_material_id)  #一个车间的项目id
        plants1 = SMaterial.where(:id => s_material_ids)

        plants_datas = []  #存车间下的项目信息
        plants1.each do |p|
          item1 = {}
          item1[:name] = p.name

          if $flag
            js_method_one(time, region_code.id, 5, p.code)
            #返回  @steam_values 日的  @steam_month_sum 累计的
          else
            #不差值计算
            @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, region_code.id, 5, p.code)
          end

          item1[:field_value] = @steam_values      #日用量
          item1[:month_value] = @steam_month_sum   #月累计
          daily_piece = ( @steam_values.to_f / @finished_products1.to_f ).round(2)
          month_piece = ( @steam_month_sum.to_f / @finished_products2.to_f ).round(2)
          item1[:daily_piece] = daily_piece  #日单耗
          item1[:month_piece] = month_piece  #月单耗

          plants_datas.push(item1)
        end
        item[:data] = plants_datas
        @region_datas.push(item)
      end

  end

  
  def index
  end


  #首页
  def energy_index

    #获取查询参数
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d")  #查询昨天的数据
    end


    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    time = before_time
    #time = Time.now
    @unit_info = []
    region_level = @region_code.s_region_level

    # #  总公司级别的权限
    # if "总公司" == region_level.level_name
    #   #分公司
    #   all_info = SRegionCode.where(:s_region_level_id => 2, :father_region_id => @region_code.id)
    #   info_h = {}
    #   info_h[:title] = "总报表"
    #   info_h[:time] = time
    #   @z_arr = []
    #   all_info.each do |info|
    #     forms = DReportForm.where(:s_region_code_id => info.id)
    #     forms.map {|x| @z_arr << {form_code: x.code, form_name: x.name,region_code:info.region_code,region_name: info.region_name }}
    #   end
    #   info_h[:unit_data] = @z_arr
    #   @unit_info << info_h
    # end

    #  分公司级别的权限
    if "分公司" == region_level.level_name
      # 能源
      all_info = SRegionCode.where(:s_region_level_id => 4, :father_region_id => @region_code.id)
      @z_arr = []
      all_info.each do |info|
        forms = DReportForm.where(:s_region_code_id => info.id)
        forms.map {|x| @z_arr << {code: x.code, form_name: x.name, date_time: time, region_code:info.region_code,region_name: info.region_name}}
      end
    end


    #  能源级别的权限
    if "能源" == region_level.level_name
      # 能源
      all_info = SRegionCode.where(:id => @region_code.id)
      @z_arr = []
      all_info.each do |info|
        forms = DReportForm.where(:s_region_code_id => info.id)
        forms.map {|x| @z_arr << {code: x.code, form_name: x.name, date_time: time, region_code:info.region_code,region_name: info.region_name}}
      end
    end
    @region_datas = @z_arr
    Rails.logger.info "-----region_datas--------#{@region_datas}-------"






    
    render :partial => "energy_index", :layout => false
  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    #@d_daily_report=DDailyReport.new
  end

  #成功跳转到show页面
  #保存 操作  还能修改

  # POST  energy_report
  def create

    region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司

    @d_daily_reports = params[:energy_params]
    status = params[:status]   #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:id]  #报表id

    # Rails.logger.info "----1---#{@d_daily_reports}-------"
    # Rails.logger.info "----1---#{status}-------"
    # Rails.logger.info "----1---#{date}-------"

    @d_daily_reports.each do |d|

    # Rails.logger.info "-------#{d}-------"
    # Rails.logger.info "-------#{d[0]}-------"
    # Rails.logger.info "-------#{d[1]}-------"

    #判断是否已经存在当天的日报
    @day_report = SNenrgyValue.find_by(:s_region_code_id => @region_code.id, :datetime => date, :field_code => d[1]["field_code"] )
    if @day_report.present?
      @day_report.update(:field_value => d[1]["field_value"].to_f, :status => status )
      #@day_report.update(:field_value => d[1]["field_code"].to_f)
    else
      @d_daily_report = SNenrgyValue.new
      #@d_daily_report.name = d[1]["name"]
      @d_daily_report.field_value = d[1]["field_value"].to_f
      @d_daily_report.field_code = d[1]["field_code"]
      @d_daily_report.datetime = date
      @d_daily_report.status = status

      @d_daily_report.s_region_code_id = @region_code.id  #车间id
      @d_daily_report.d_report_form_id = d_report_form_id  #报表id
      @d_daily_report.save
    end

  end
    render json: {status: 'success'}
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    
  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
