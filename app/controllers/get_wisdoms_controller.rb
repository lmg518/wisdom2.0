class GetWisdomsController < ApplicationController
    skip_before_filter :verify_authenticity_token,:only => :update  #跳过安全令牌验证
    def index
        #计划内
        day_time = Time.parse(params[:day_time]+ " 23:59:59") if params[:day_time].present?
        @tasks = DTaskForm.where(:create_type =>'plan_in')
                          .by_get_day(day_time)
                          .by_polling_time(params[:created_time],params[:end_time])
        @task = @tasks.page(params[:page]).per(params[:per])
    end

end