class AlarmNumTimeAnalyseController < ApplicationController

  #默认 分析粒度 2
  def index
    search_parmas
    handle_table
  end

  def export_file
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["时段", "数量", "占比"]
      @alarm_arr.each do |alarm|
        csv << [alarm[:grading_time], alarm[:grading_num], (alarm[:grading_num].to_f / @alarm_total.to_f).round(2)]
      end
    end
    if  @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "报警时段分析统计"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "报警时段分析统计"
    end
    respond_to do |format|
      format.csv{
        send_data @scv,
                  :type=>'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private
  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids]
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
    @alarm_levels = params[:alarm_levels]
    @grading_analysis = params[:grading_analysis]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
      @grading_analysis = @search[:grading_analysis]
    end
  end

  def handle_table
    @alarm_arr = []
    @region_leves = SRegionLevel.select(:id, :level_name)
    @alarms = DAlarm.by_station_regin_leve(@region_leve).by_alarm_times(@data_times).by_rule_levels(@alarm_levels)
    @alarm_total = @alarms.length
    if @grading_analysis.present?
      grading_step = @grading_analysis.to_i
    else
      grading_step = 2
    end
    begin
      if @data_times.present?
        @time1 = Time.parse(@data_times[0])
        @time2 = Time.parse(@data_times[1])
      else
        @time1 = Time.now
        @time2 = Time.now
      end
    rescue Exception => e
      @data_times = @data_times.split(",")
      @time1 = Time.parse(@data_times[0])
      @time2 = Time.parse(@data_times[1])
    end

    grading_num = 24 / grading_step
    first_num, sed_num = 0,0
    grading_num.times do |i|
      if i == 0
        first_num = 0
      end
      sed_num += grading_step
      @alarm_arr << {grading_time: first_num.to_s + "~" + sed_num.to_s,
                     grading_num: DAlarm.where(:first_alarm_time => (@time1 + first_num.hours)..(@time2 + sed_num.hours)).length}
      first_num += grading_step
    end
    @alarm_arr
  end

end