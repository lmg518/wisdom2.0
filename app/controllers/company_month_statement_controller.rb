class CompanyMonthStatementController < ApplicationController
  include EnergyFormHelper
  before_action :authenticate_user

  #公司总报表

  def index
  end

  #首页
  def index_table
    $flag = false #差值计算和不差值计算的 标识  true 差值计算   false 不差值计算
    get_login_infos
    render :partial => "index_table", :layout => false
  end


  #获取登录人信息
  def get_login_infos

    time = params[:time]
    params_region_ids = params[:region_id]
    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code1= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code1= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1 #2 分公司   1 总公司   3 车间

      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 2
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_f = SRegionCode.where(:father_region_id => @region_code1.id, :s_region_level_id => 3)
        region_code_ids = region_f.pluck(:id) #查询分公司下的的所有车间

      end
    elsif @region_code1.s_region_level_id == 3
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        @region_code = @region_code1
        #region_code_ids = @region_code.id
        region_code_ids = [@region_code.id]
      end
    end

    #设置查询的日期
    if time.present?
      before_time = params[:time]
      end_time = Time.parse(time).end_of_month.strftime("%Y-%m-%d") #查询月份的最后一天
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @form_values_month1 = SNenrgyValue.by_datetime(month_begin, end_time)
      if @form_values_month1.present?
        max_time = @form_values_month1.maximum("datetime") #获取最大日期
        before_time = max_time
      else
        before_time = Time.now.strftime("%Y-%m-%d") #查询当天的数据  #当月没数据时 加一个默认时间
      end
    else
      before_time = Time.now.strftime("%Y-%m-%d") #查询当天的数据
    end

    #计算 公司 总产量
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    time = before_time
    @company_day_total = 1 #日用量      计算日单耗时  除数不能为0
    @company_month_total = 1 #累计用量

    @na_form_values = DDailyReport.find_by(:s_region_code_id => 8, :code => 'na_input', :datetime => time) # 8 烘包车间  na_input 钠入库
    @sour_form_values = DDailyReport.find_by(:s_region_code_id => 8, :code => 'sour_input', :datetime => time) # 8 烘包车间  sour_input 酸入库

    @na_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'na_input')
    @sour_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'sour_input')

    if @na_form_values.present? && @sour_form_values.present?
      @company_day_total = (@na_form_values.nums.to_f + @sour_form_values.nums.to_f / 0.73).round(2)
    end

    d1 = 0
    d2 = 0
    @na_form_values_month.each do |m|
      d1 += m.nums.to_f
    end
    @sour_form_values_month.each do |m|
      d2 += m.nums.to_f
    end
    @company_month_total = (d1 + d2 / 0.73).round(2) ##累计用量

    #@company_day_total = 58.91    #公司 日用量
    #@company_month_total = 100    #公司  累计用量


    #分公司下的车间
    region_code_ids.each do |region_id|
      region = SRegionCode.find_by(:id => region_id)
      if 'TYSYFGS01' == region.region_code #发酵车间
        get_datas_fj(region_id, before_time)
      end

      if 'TYSYFGS02' == region.region_code #合成车间
        get_datas_hc(region_id, before_time)
      end

      if 'TYSYFGS05' == region.region_code #精制车间
        get_datas_jz(region_id, before_time)
      end

      if 'TYSYFGS03' == region.region_code #烘包车间
        get_datas_hb(region_id, before_time)
      end
    end


    #计算总收率 = 发酵总收率*合成总收率*精制总收率
    @fj_yield = @total_yield_fj #发酵车间总收率
    @hc_yield = @total_yield_hc #合成车间总收率
    @jz_yield = @total_yield_jz #精制车间总收率
    if @fj_yield.present? && @hc_yield.present? && @jz_yield.present?
      @company_total_yield = ((@total_yield_fj * @total_yield_hc * @total_yield_jz) / 10000).round(2) #总收率
    else
      @company_total_yield = 0
    end
    # @company_total_yield = ((@total_yield_fj * @total_yield_hc * @total_yield_jz) / 10000).round(2)  #总收率

    #公司 能源  废水  数据
    energy_datas(before_time)
    water_datas(before_time)

  end

  #---------------------
  #差值计算方法1 计算多个项目的，参数有项目的
  def js_method_many(time, s_region_code_id, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

    @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => time, :field_code => item)
    @form_values_bf = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => bf_time, :field_code => item)

    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :field_code => item)
    min_time = @form_values_month.minimum("datetime") #获取最小日期
    max_time = @form_values_month.maximum("datetime") #获取最大日期
    @form_values_min = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => min_time, :field_code => item)
    @form_values_max = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => max_time, :field_code => item)

    d1 = 0
    d2 = 0
    @form_values.each do |m|
      d1 += m.field_value.to_f
    end
    @form_values_bf.each do |m|
      d2 += m.field_value.to_f
    end
    @steam_values = (d1 - d2).round(2) #日用量

    dd1 = 0
    dd2 = 0
    @form_values_max.each do |m|
      dd1 += m.field_value.to_f
    end
    @form_values_min.each do |m|
      dd2 += m.field_value.to_f
    end
    @steam_month_sum = (dd1 - dd2).round(2) #累计用量
  end

  #差值计算方法 2  计算单个项目的  item 项目参数名
  def js_method_one(time, s_region_code_id, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

    @form_values = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)
    @form_values_bf = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)

    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id)
    min_time = @form_values_month.minimum("datetime") #获取最小日期
    max_time = @form_values_month.maximum("datetime") #获取最大日期

    @form_values_min = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)
    @form_values_max = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)


    if @form_values.present? && @form_values.field_value.present?
      d1 = @form_values.field_value.to_f
    else
      d1 = 0
    end

    if @form_values_bf.present? && @form_values_bf.field_value.present?
      d2 = @form_values_bf.field_value.to_f
    else
      d2 = 0
    end

    #添加判断
    if @form_values_min.present? && @form_values_min.field_value.present?
      min = @form_values_min.field_value.to_f
    else
      min = 0
    end
    if @form_values_max.present? && @form_values_max.field_value.present?
      max = @form_values_max.field_value.to_f
    else
      max = 0
    end
    @steam_month_sum = max -min

    @steam_values = d1 - d2 #昨天的 - 前天的
    #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f   #月累计用量
  end

  #差值计算方法 3  计算单个项目的  能源使用的
  def js_method_energy(time, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

    @form_values = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)
    @form_values_bf = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)

    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:field_code => item, :d_report_form_id => d_report_form_id)
    min_time = @form_values_month.minimum("datetime") #获取最小日期
    max_time = @form_values_month.maximum("datetime") #获取最大日期

    @form_values_min = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)
    @form_values_max = SNenrgyValue.find_by(:field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)

    if @form_values.present? && @form_values.field_value.present?
      d1 = @form_values.field_value.to_f
    else
      d1 = 0
    end

    if @form_values_bf.present? && @form_values_bf.field_value.present?
      d2 = @form_values_bf.field_value.to_f
    else
      d2 = 0
    end

    #添加判断
    if @form_values_min.present? && @form_values_min.field_value.present?
      min = @form_values_min.field_value.to_f
    else
      min = 0
    end
    if @form_values_max.present? && @form_values_max.field_value.present?
      max = @form_values_max.field_value.to_f
    else
      max = 0
    end
    @steam_month_sum = max -min

    @steam_values = d1 - d2 #昨天的 - 前天的
    #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f   #月累计用量
  end

  #-----------------------------

  #能源 
  def energy_datas(before_time)
    time = before_time
    @steam_datas_energys = []
    steam_names1 = [{:name => '一次水', :code => 'once_water'},
                    {:name => '电', :code => 'power'},
                    {:name => '循环水', :code => 'water'},
                    {:name => '甲醇塔蒸汽', :code => 'jct_steam'},
                    {:name => '总蒸汽', :code => 'all_steam'}
    ]
    steam_names1.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

      if 'once_water' == s[:code] #一次水计算
        steam_values = 0
        steam_month_sum = 0

        if $flag #差值计算 和不差值计算的 标识
          #计算差值的逻辑
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期

          @form_values = SNenrgyValue.where(:s_region_code_id => 11, :d_report_form_id => 4, :datetime => time) # 11 一次水统计部门  4 一次水报表
          @form_values_bf = SNenrgyValue.where(:s_region_code_id => 11, :d_report_form_id => 4, :datetime => bf_time)

          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => 11, :d_report_form_id => 4) # 11 一次水统计部门  4 一次水报表
          min_time = @form_values_month.minimum("datetime") #获取最小日期
          max_time = @form_values_month.maximum("datetime") #获取最大日期
          @form_values_min = SNenrgyValue.where(:s_region_code_id => 11, :d_report_form_id => 4, :datetime => min_time)
          @form_values_max = SNenrgyValue.where(:s_region_code_id => 11, :d_report_form_id => 4, :datetime => max_time)

          d1 = 0
          d2 = 0
          @form_values.each do |m|
            d1 += m.field_value.to_f
          end
          @form_values_bf.each do |m|
            d2 += m.field_value.to_f
          end
          steam_values = (d1 - d2).round(2) #日用量

          dd1 = 0
          dd2 = 0
          @form_values_max.each do |m|
            dd1 += m.field_value.to_f
          end
          @form_values_min.each do |m|
            dd2 += m.field_value.to_f
          end
          steam_month_sum = (dd1 - dd2).round(2) #累计用量

          #不进行 差值的计算逻辑
        else

          @form_values = SNenrgyValue.where(:s_region_code_id => 11, :d_report_form_id => 4, :datetime => time) # 11 一次水统计部门  4 一次水报表
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => 11, :d_report_form_id => 4) # 11 一次水统计部门  4 一次水报表
          @form_values.each do |m|
            steam_values += m.field_value.to_f
          end
          @form_values_month.each do |m|
            steam_month_sum += m.field_value.to_f
          end

        end


        item1[:steam_values]= steam_values #日用量
        item1[:steam_month_sum]= steam_month_sum #月累计
        day_expend = (steam_values / @company_day_total).round(2) #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'power' == s[:code] #电  计算

        if $flag

          steam_values = 0 #电 的日用量
          steam_month_sum = 0 #电 的月累计
          #计算差值
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期

          @form_values = SNenrgyValue.find_by(:s_region_code_id => 9, :d_report_form_id => 1, :datetime => time) # 9 电统计部门  1 电报表
          @form_values_bf = SNenrgyValue.find_by(:s_region_code_id => 9, :d_report_form_id => 1, :datetime => bf_time) # 9 电统计部门  1 电报表

          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => 9, :d_report_form_id => 1)
          min_time = @form_values_month.minimum("datetime") #获取最小日期
          max_time = @form_values_month.maximum("datetime") #获取最大日期

          @form_values_min = SNenrgyValue.find_by(:s_region_code_id => 9, :d_report_form_id => 1, :datetime => min_time)
          @form_values_max = SNenrgyValue.find_by(:s_region_code_id => 9, :d_report_form_id => 1, :datetime => max_time)

          if @form_values.present? && @form_values.field_value.present?
            d1 = @form_values.field_value.to_f
          else
            d1 = 0
          end

          if @form_values_bf.present? && @form_values_bf.field_value.present?
            d2 = @form_values_bf.field_value.to_f
          else
            d2 = 0
          end

          #添加判断
          if @form_values_min.present? && @form_values_min.field_value.present?
            min = @form_values_min.field_value.to_f
          else
            min = 0
          end
          if @form_values_max.present? && @form_values_max.field_value.present?
            max = @form_values_max.field_value.to_f
          else
            max = 0
          end
          steam_month_sum = max -min
          steam_values = d1 - d2 #昨天的 - 前天的

        else
          #不差值计算的逻辑
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id(time, 9, 1)
          steam_values = @steam_values
          steam_month_sum = @steam_month_sum
        end

        # Rails.logger.info "1111111111111---------#{steam_values}"
        # Rails.logger.info "1111111111111---------#{steam_month_sum}"

        item1[:steam_values]= steam_values #日用量
        item1[:steam_month_sum]= steam_month_sum #月累计
        day_expend = (steam_values / @company_day_total).round(2) #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'water' == s[:code] #循环水  计算

        if $flag

          steam_values = 0
          steam_month_sum = 0
          #计算差值
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期

          @form_values = SNenrgyValue.where(:s_region_code_id => 10, :d_report_form_id => 2, :datetime => time) # 11 一次水统计部门  4 一次水报表
          @form_values_bf = SNenrgyValue.where(:s_region_code_id => 10, :d_report_form_id => 2, :datetime => bf_time)

          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => 10, :d_report_form_id => 2) # 11 一次水统计部门  4 一次水报表
          min_time = @form_values_month.minimum("datetime") #获取最小日期
          max_time = @form_values_month.maximum("datetime") #获取最大日期
          @form_values_min = SNenrgyValue.where(:s_region_code_id => 10, :d_report_form_id => 2, :datetime => min_time)
          @form_values_max = SNenrgyValue.where(:s_region_code_id => 10, :d_report_form_id => 2, :datetime => max_time)

          d1 = 0
          d2 = 0
          @form_values.each do |m|
            d1 += m.field_value.to_f
          end
          @form_values_bf.each do |m|
            d2 += m.field_value.to_f
          end
          steam_values = (d1 - d2).round(2) #日用量

          dd1 = 0
          dd2 = 0
          @form_values_max.each do |m|
            dd1 += m.field_value.to_f
          end
          @form_values_min.each do |m|
            dd2 += m.field_value.to_f
          end
          steam_month_sum = (dd1 - dd2).round(2) #累计用量

        else
          #不差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id(time, 10, 2)
          steam_values = @steam_values
          steam_month_sum = @steam_month_sum

        end

        item1[:steam_values]= steam_values #日用量
        item1[:steam_month_sum]= steam_month_sum #月累计
        day_expend = (steam_values / @company_day_total).round(2) #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'jct_steam' == s[:code] #甲醇塔蒸汽  计算

        if $flag

          steam_values = 0
          steam_month_sum = 0
          #计算差值
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期

          @form_values = SNenrgyValue.find_by(:field_code => 'steam_4', :d_report_form_id => 3, :datetime => time) # steam_4 --> 甲醇塔  3 蒸汽报表
          @form_values_bf = SNenrgyValue.find_by(:field_code => 'steam_4', :d_report_form_id => 3, :datetime => bf_time) # steam_4 --> 甲醇塔  3 蒸汽报表

          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:field_code => 'steam_4', :d_report_form_id => 3)
          min_time = @form_values_month.minimum("datetime") #获取最小日期
          max_time = @form_values_month.maximum("datetime") #获取最大日期

          @form_values_min = SNenrgyValue.find_by(:field_code => 'steam_4', :d_report_form_id => 3, :datetime => min_time)
          @form_values_max = SNenrgyValue.find_by(:field_code => 'steam_4', :d_report_form_id => 3, :datetime => max_time)

          if @form_values.present? && @form_values.field_value.present?
            d1 = @form_values.field_value.to_f
          else
            d1 = 0
          end

          if @form_values_bf.present? && @form_values_bf.field_value.present?
            d2 = @form_values_bf.field_value.to_f
          else
            d2 = 0
          end

          #添加判断
          if @form_values_min.present? && @form_values_min.field_value.present?
            min = @form_values_min.field_value.to_f
          else
            min = 0
          end
          if @form_values_max.present? && @form_values_max.field_value.present?
            max = @form_values_max.field_value.to_f
          else
            max = 0
          end
          steam_month_sum = max -min
          steam_values = d1 - d2 #昨天的 - 前天的

        else
          #不差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_item_form_id_time('steam_4', 3, time)
          steam_values = @steam_values
          steam_month_sum = @steam_month_sum

        end

        item1[:steam_values]= steam_values.round(2) #日用量
        item1[:steam_month_sum]= steam_month_sum.round(2) #月累计
        day_expend = (steam_values / @company_day_total).round(2) #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'all_steam' == s[:code] #总蒸汽  计算
        steam_values = 0
        steam_month_sum = 0

        if $flag

          #计算差值
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期

          @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => time) #   3 蒸汽报表
          @form_values_bf = SNenrgyValue.where(:d_report_form_id => 3, :datetime => bf_time) #   3 蒸汽报表

          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 3) # 3 蒸汽报表
          min_time = @form_values_month.minimum("datetime") #获取最小日期
          max_time = @form_values_month.maximum("datetime") #获取最大日期
          @form_values_min = SNenrgyValue.where(:d_report_form_id => 3, :datetime => min_time)
          @form_values_max = SNenrgyValue.where(:d_report_form_id => 3, :datetime => max_time)

          d1 = 0
          d2 = 0
          @form_values.each do |m|
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            d1 += m.field_value.to_f
          end
          @form_values_bf.each do |m|
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            d2 += m.field_value.to_f
          end
          steam_values = (d1 - d2).round(2) #日用量
          dd1 = 0
          dd2 = 0
          @form_values_max.each do |m| #不包括  steam_9稀甲醇  steam_10蒸精甲醇  steam_11浓缩母液
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            dd1 += m.field_value.to_f
          end
          @form_values_min.each do |m|
            if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
              next
            end
            dd2 += m.field_value.to_f
          end
          steam_month_sum = (dd1 - dd2).round(2) #累计用量

        else
          #不进行 差值计算
          @form_values = SNenrgyValue.where(:d_report_form_id => 3, :datetime => time) #   3 蒸汽报表
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 3) # 3 蒸汽报表

          #计算日产量
          if @form_values.present? && @form_values.length > 0
            @form_values.each do |m|
              if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
                next
              end
              steam_values += m.field_value.to_f
            end
          else
            steam_values = 0
          end
          #计算月累计
          if @form_values_month.present? && @form_values_month.length >0
            @form_values_month.each do |m|
              if m.field_code == 'steam_9' || m.field_code == 'steam_10' || m.field_code == 'steam_11' then
                next
              end
              steam_month_sum += m.field_value.to_f
            end
          else
            steam_month_sum = 0
          end
        end

        item1[:steam_values]= steam_values.round(2) #日用量
        item1[:steam_month_sum]= steam_month_sum.round(2) #月累计
        day_expend = (steam_values / @company_day_total).round(2) #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      @steam_datas_energys.push(item1)
    end
  end

  #废水
  def water_datas(before_time)
    time = before_time
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

    @steam_datas_water = []
    steam_names1 = [{:name => '低浓度水', :code => 'low_water'},
                    {:name => '高浓度水', :code => 'hig_water'},
                    {:name => '洗效水', :code => 'pollution_7'},
                    {:name => 'D核糖', :code => 'D_hd'},
                    {:name => '废母液', :code => 'pollution_8'}
    ]
    steam_names1.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]

      if 'low_water' == s[:code] #低浓度水  计算
        steam_values = 0
        steam_month_sum = 0
        if $flag

          #计算差值
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
          @form_values = SNenrgyValue.where(:d_report_form_id => 5, :datetime => time) #   5 排污报表
          @form_values_bf = SNenrgyValue.where(:d_report_form_id => 5, :datetime => bf_time)
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 5)
          min_time = @form_values_month.minimum("datetime") #获取最小日期
          max_time = @form_values_month.maximum("datetime") #获取最大日期
          @form_values_min = SNenrgyValue.where(:d_report_form_id => 5, :datetime => min_time)
          @form_values_max = SNenrgyValue.where(:d_report_form_id => 5, :datetime => max_time)
          d1 = 0
          d2 = 0
          @form_values.each do |m|
            if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
              next
            end
            d1 += m.field_value.to_f
          end
          @form_values_bf.each do |m|
            if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
              next
            end
            d2 += m.field_value.to_f
          end

          steam_values = (d1 - d2).round(2) #日用量

          dd1 = 0
          dd2 = 0
          @form_values_max.each do |m| ##去除二个  pollution_6甲醇废水    pollution_7洗效水
            if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
              next
            end
            dd1 += m.field_value.to_f
          end
          @form_values_min.each do |m|
            if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
              next
            end
            dd2 += m.field_value.to_f
          end

          steam_month_sum = (dd1 - dd2).round(2) #累计用量

        else
          #不进行差值计算
          @form_values = SNenrgyValue.where(:d_report_form_id => 5, :datetime => time) #  5 排污报表
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => 5)
          @form_values.each do |m| #去除二个  pollution_6甲醇废水    pollution_7洗效水
            if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
              next
            end
            steam_values += m.field_value.to_f
          end
          @form_values_month.each do |m|
            if m.field_code == 'pollution_6' || m.field_code == 'pollution_7' then
              next
            end
            steam_month_sum += m.field_value.to_f
          end
        end

        item1[:steam_values]= steam_values #日用量
        item1[:steam_month_sum]= steam_month_sum #月累计
        day_expend = (steam_values / @company_day_total).round(2) #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'hig_water' == s[:code] #高浓度水  计算

        if $flag

          js_method_energy(time, 5, 'pollution_6') #d_report_form_id  5  item

        else
          #不进行差值计算
          @steam_values = 0
          @steam_month_sum = 0
          @form_values = SNenrgyValue.where(:d_report_form_id => '5', :datetime => time, :field_code => 'pollution_6') #  5 排污报表
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => '5', :field_code => 'pollution_6')
          @form_values.each do |m|
            @steam_values += m.field_value.to_f
          end
          @form_values_month.each do |m|
            @steam_month_sum += m.field_value.to_f
          end
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月累计
        day_expend = (@steam_values / @company_day_total).round(2) #日单耗
        month_expend = (@steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'pollution_7' == s[:code] #洗效水  计算


        if $flag
          js_method_energy(time, 5, 'pollution_7')
        else
          #不进行差值计算
          @steam_values = 0
          @steam_month_sum = 0
          @form_values = SNenrgyValue.where(:d_report_form_id => '5', :datetime => time, :field_code => 'pollution_7') #  5 排污报表
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => '5', :field_code => 'pollution_7')
          @form_values.each do |m| #去除二个  pollution_6甲醇废水    pollution_7洗效水
            @steam_values += m.field_value.to_f
          end
          @form_values_month.each do |m|
            @steam_month_sum += m.field_value.to_f
          end
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月累计
        day_expend = (@steam_values / @company_day_total).round(2) #日单耗
        month_expend = (@steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'D_hd' == s[:code] #D核糖  计算
        steam_values = 0
        steam_month_sum = 0

        if $flag
          #计算差值
          bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
          @form_values = SNenrgyValue.find_by(:d_report_form_id => 5, :s_region_code_id => 15, :datetime => time) # 5 排污报表   15 D核糖车间
          @form_values_bf = SNenrgyValue.find_by(:d_report_form_id => 5, :s_region_code_id => 15, :datetime => bf_time)
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => 15, :d_report_form_id => 5)
          min_time = @form_values_month.minimum("datetime") #获取最小日期
          max_time = @form_values_month.maximum("datetime") #获取最大日期
          @form_values_min = SNenrgyValue.find_by(:d_report_form_id => 5, :s_region_code_id => 15, :datetime => min_time)
          @form_values_max = SNenrgyValue.find_by(:d_report_form_id => 5, :s_region_code_id => 15, :datetime => max_time)

          if @form_values.present? && @form_values.field_value.present?
            d1 = @form_values.field_value.to_f
          else
            d1 = 0
          end

          if @form_values_bf.present? && @form_values_bf.field_value.present?
            d2 = @form_values_bf.field_value.to_f
          else
            d2 = 0
          end

          #添加判断
          if @form_values_min.present? && @form_values_min.field_value.present?
            min = @form_values_min.field_value.to_f
          else
            min = 0
          end
          if @form_values_max.present? && @form_values_max.field_value.present?
            max = @form_values_max.field_value.to_f
          else
            max = 0
          end
          steam_month_sum = max -min

          steam_values = d1 - d2 #昨天的 - 前天的

        else
          # 不进行差值计算
          @form_values = SNenrgyValue.where(:d_report_form_id => '5', :datetime => time, :s_region_code_id => 15) #  5 排污报表   15 D核糖车间
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => '5', :s_region_code_id => 15)
          @form_values.each do |m|
            steam_values += m.field_value.to_f
          end
          @form_values_month.each do |m|
            steam_month_sum += m.field_value.to_f
          end
        end

        item1[:steam_values]= steam_values #日用量
        item1[:steam_month_sum]= steam_month_sum #月累计
        day_expend = (steam_values / @company_day_total).round(2) #日单耗
        month_expend = (steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      if 'pollution_8' == s[:code] #废母液  计算

        if $flag
          js_method_energy(time, 5, 'pollution_8')
        else
          #不进行差值运算
          @steam_values = 0
          @steam_month_sum = 0
          @form_values = SNenrgyValue.where(:d_report_form_id => '5', :datetime => time, :field_code => 'pollution_8') #  5 排污报表
          @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:d_report_form_id => '5', :field_code => 'pollution_8')
          @form_values.each do |m| #去除二个  pollution_6甲醇废水    pollution_7洗效水
            @steam_values += m.field_value.to_f
          end
          @form_values_month.each do |m|
            @steam_month_sum += m.field_value.to_f
          end
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月累计
        day_expend = (@steam_values / @company_day_total).round(2) #日单耗
        month_expend = (@steam_month_sum / @company_month_total).round(2) #月单耗

        item1[:day_expend]= day_expend #日单耗
        item1[:month_expend]= month_expend #月单耗
      end

      @steam_datas_water.push(item1)
    end
  end


  #精制车间
  def get_datas_jz(region_id, before_time)
    time = before_time

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => region_id) #根据id 来找
    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas3 = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)

    # #精制车间 客户要看 湿成品钠、湿成品酸、粗品
    # @ids.push(9,11,6)


    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.now.beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计用电量

      #质量信息
      data2 = []

      if 'YCL' == plant.code #一次料
        data2.push({:technic_01 => '批号', :technic_17 => '比重', :technic_18 => '料浆含量', :technic_19 => '加炭量', :technic_20 => '一次母液含量', :technic_21 => '溶解温度', })
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'ECL' == plant.code #二次料
        data2.push({:technic_01 => '批号', :technic_21 => '溶解温度', :technic_12 => 'PH', :technic_22 => '加EDTA量', :technic_23 => '加草酸量'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      # #精制车间 客户要看 湿成品钠、湿成品酸、粗品   
      # if 'SPCN' == plant.code  #湿成品钠
      #   data2.push({:technic_01 => '批号' , :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
      #   @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
      #   data2.push({:data => @s_teachnics})
      # end

      # if 'SCPS' == plant.code  #湿成品酸
      #   data2.push({:technic_01 => '批号' , :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
      #   @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
      #   data2.push({:data => @s_teachnics})
      # end

      # if 'CP' == plant.code  #粗品
      #   data2.push({:technic_01 => '批号' , :technic_13 => '含量', :technic_12 => 'PH', :technic_14 => '水份', :technic_15 => '料浆比重',:technic_16 => '母液含量',})
      #   @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
      #   data2.push({:data => @s_teachnics})
      # end


      item[:data] = data2
      @region_datas3.push(item)
    end


    #------------------------
    #蒸汽  总收率  数据
    #计算精制车架总收率 = 一次料 * 二次料
    @daily_report_yc = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'YCL')
    @daily_report_ec = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'ECL')

    yc_yield= @daily_report_yc.present? ? @daily_report_yc.daily_yield : 0
    ec_yield= @daily_report_ec.present? ? @daily_report_ec.daily_yield : 0
    @total_yield_jz = ((yc_yield.to_f * ec_yield.to_f) / 100).round(2) #精制车间总收率

    #精制车间日产量=钠包装量+酸包装量*0.73
    #精制车间 计算 日单耗 = 日蒸汽/ 精制车间日产量     月单耗 = 月蒸汽/ 精制车间月产量
    month_begin = Time.now.beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    @daily_report_na = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'na_nums') # na_nums  在 s_region_code_id = 8
    @daily_report_na_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'na_nums')
    @daily_report_suan = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'suan_nums')
    @daily_report_suan_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'suan_nums')

    na_day_nums = @daily_report_na.present? ? @daily_report_na.nums : 1 #钠日产量
    suan_day_nums = @daily_report_suan.present? ? @daily_report_suan.nums : 1 #酸日产量
    @nums_day_jz = na_day_nums + suan_day_nums * 0.73 #精制车间日产量
    dd1= 0
    dd2 =0
    @daily_report_na_month.each do |d1|
      dd1 += d1.nums.to_f
    end
    @daily_report_suan_month.each do |d1|
      dd2 += d1.nums.to_f
    end
    @nums_month_jz = dd1 + dd2 * 0.73 #精制车间 月累计产量


    #精制车间 蒸汽 表格数据
    @steam_datas3 = []
    steam_names = [{:name => '浓缩', :code => 'steam_6'},
                   {:name => '溶解', :code => 'steam_5'},
                   {:name => '合计', :code => 'total'}
    ]
    steam_names.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]

      #计算单个 项目 蒸汽的日用量  月累计
      if s[:code] == 'steam_6' #浓缩

        if $flag
          js_method_one(time, 7, 3, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 3, s[:code])
        end
        # @steam_values = SNenrgyValue.find_by(:s_region_code_id => 7 , :d_report_form_id => 3 , :datetime => time, :field_code => s[:code])  # 3 蒸汽报表  7 精制车间
        # @day_steam = @steam_values.present? && @steam_values.field_value.present? ? @steam_values.field_value : ''
        # @steam_month_sum = 0  #蒸汽月累计  单个项目的
        # month_begin = Time.now.beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        # @steam_month_values = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => 7 , :d_report_form_id => 3, :field_code => s[:code])
        # @steam_month_values.each do |data|
        #   @steam_month_sum += data.field_value.to_f
        # end

        item1[:steam_values]= @steam_values.round(2) #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end

      if s[:code] == 'steam_5' #溶解
        if $flag
          js_method_one(time, 7, 3, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 3, s[:code])
        end
        # @steam_values = SNenrgyValue.find_by(:s_region_code_id => 7 , :d_report_form_id => 3 , :datetime => time, :field_code => s[:code])  # 3 蒸汽报表  7 精制车间
        # @day_steam = @steam_values.present? && @steam_values.field_value.present? ? @steam_values.field_value : ''
        # @steam_month_sum = 0  #蒸汽月累计  单个项目的
        # month_begin = Time.now.beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        # @steam_month_values = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => 7 , :d_report_form_id => 3, :field_code => s[:code])
        # @steam_month_values.each do |data|
        #   @steam_month_sum += data.field_value.to_f
        # end

        item1[:steam_values]= @steam_values.round(2) #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end

      if s[:code] == 'total' #合计

        item = ['steam_6', 'steam_5']
        if $flag
          js_method_many(time, 7, 3, item) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 7, 3, item)
        end

        item1[:steam_values]= @steam_values.round(2) #合计日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end
      @steam_datas3.push(item1)


    end
  end

  #合成车间
  def get_datas_hc(region_id, before_time)

    time = before_time

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => region_id) #根据车间id 来找

    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas2 = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计

      #质量信息
      data2 = []

      if 'LJ' == plant.code #离交
        data2.push({:technic_01 => '批号', :technic_09 => '交料旋光', :technic_10 => '交料透光', :technic_11 => 'Ca2+', :technic_12 => 'PH', })
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'CP' == plant.code #粗品
        data2.push({:technic_01 => '批号', :technic_13 => '含量', :technic_12 => 'PH', :technic_14 => '水份', :technic_15 => '料浆比重', :technic_16 => '母液含量', })
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas2.push(item)
    end

    #-------------v2--------------
    #合成车间 蒸汽  总收率  数据
    #合成车间 总收率= 离交*粗品*三效
    @daily_report_lj = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'LJ')
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'CP')
    @daily_report_sx = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'steam_1')

    lj_yield= @daily_report_lj.present? ? @daily_report_lj.daily_yield : 0
    cp_yield= @daily_report_cp.present? ? @daily_report_cp.daily_yield : 0
    sx_yield= @daily_report_sx.present? ? @daily_report_sx.daily_yield : 0

    @total_yield_hc = ((lj_yield.to_f * cp_yield.to_f * sx_yield.to_f) / 10000).round(2) #合成车间 总收率

    #合成车间日产量= 粗品
    #合成车间 计算 日单耗 = 日蒸汽/ 合成车间日产量     月单耗 = 月蒸汽/ 合成车间月产量
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => 5, :datetime => time, :code => 'CP') # CP  在 s_region_code_id = 5
    @daily_report_cp_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 5, :code => 'CP')
    @cp_day_nums = @daily_report_cp.present? ? @daily_report_cp.nums : 1 #粗品日产量

    @cp_month_nums = 0 #粗品月累计
    if @daily_report_cp_month.present? && @daily_report_cp_month.length >0
      @daily_report_cp_month.each do |d1|
        @cp_month_nums += d1.nums.to_f
      end
    else
      @cp_month_nums = 1 #数据不存在是 默认值为 1  除数不能为0
    end


    #蒸汽 表格数据
    @steam_datas2 = []
    steam_names = [{:name => '酯转化', :code => 'steam_3'},
                   {:name => '三浓', :code => 'steam_2'},
                   {:name => '三效', :code => 'steam_1'},
                   {:name => '甲醇塔', :code => 'steam_4'},
                   {:name => '合计', :code => 'total'}
    ]
    steam_names.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]

      #合成车间 计算单个 项目 蒸汽的日用量  月累计
      if s[:code] == 'steam_3' #酯转化

        if $flag
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values.round(2) #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'steam_2' #三浓
        if $flag
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values.round(2) #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'steam_1' #三效
        if $flag
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values.round(2) #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'steam_4' #甲醇塔
        if $flag
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values.round(2) #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量

        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end


      if s[:code] == 'total' #合计

        item = ['steam_1', 'steam_2', 'steam_3', 'steam_4']
        if $flag
          js_method_many(time, 5, 3, item) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 5, 3, item)
        end
        item1[:steam_values]= @steam_values.round(2) #合计日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量

        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end
      @steam_datas2.push(item1)


    end

  end


  #烘包车间
  def get_datas_hb(region_id, before_time)

    time = before_time
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => region_id) #根据表格code 来找

    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas4 = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      #month_begin = Time.now.beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #c查询日期月的开始日期

      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计用电量

      #烘包车间 质量信息
      data2 = []

      if 'SPCN' == plant.code #湿成品钠
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'SCPS' == plant.code #湿成品酸
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'na_nums' == plant.code #钠包装量
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_26 => '2小时白度', :technic_27 => '透光度', :technic_14 => '水份', :technic_28 => '等级'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'suan_nums' == plant.code #酸包装量
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_26 => '2小时白度', :technic_27 => '透光度', :technic_14 => '水份', :technic_28 => '等级'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas4.push(item)
    end

    # 烘包车间 蒸汽  总收率  数据
    #日产量
    @steam_values = SNenrgyValue.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => 3, :datetime => time) # 3 蒸汽报表
    @steam_month_sum_hb = 0 #烘包车间  蒸汽月累计
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    @steam_month_values = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => 3)
    @steam_month_values.each do |data|
      @steam_month_sum_hb += data.field_value.to_f
    end
    @steam_month_sum_hb = @steam_month_sum_hb.round(2) #烘包车间  蒸汽月累计
    @day_steam_hb = @steam_values.present? ? @steam_values.field_value : '' #烘包车间 蒸汽日用量

    #计算 日单耗  月单耗
    #烘包车间计算 日单耗 = 烘包蒸汽日用量 / 公司日产量    月单耗 = 烘包蒸汽月用量 / 公司月产量
    #@company_day_total = 58.91    #公司 日产量 
    #@company_month_total = 100    #公司  累计用量
    @day_expend_hb = (@day_steam_hb.to_f / @company_day_total.to_f).round(2) #烘包车间日单耗
    @month_expend_hb = (@steam_month_sum_hb.to_f / @company_month_total.to_f).round(2) #烘包车间月单耗

  end


  #发酵车间
  def get_datas_fj(region_id, before_time)
    time = before_time

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => region_id) #根据表格code 来找

    #@form_name = @d_report_forms.present? ? @d_report_forms.name : ''   #表名
    @form_name = '郑州拓洋生物工程有限公司生产报表' #表名
    @data_time = before_time #日期

    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计用电量

      #质量信息
      data2 = []

      if 'TF' == plant.code #头粉
        data2.push({:technic_01 => '批号', :technic_02 => '波美度', :technic_03 => '淀粉酶量'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'TY' == plant.code #糖液
        data2.push({:technic_01 => '批号', :technic_04 => '糖值', :technic_05 => 'DE值', :technic_06 => '周期'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'FJY' == plant.code #发酵液
        data2.push({:technic_01 => '批号', :technic_07 => '放罐旋光', :technic_08 => '滤速', :technic_06 => '周期'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'SHY' == plant.code #酸化液
        data2.push({:technic_01 => '批号', :technic_09 => '交料旋光', :technic_10 => '交料透光', :technic_11 => 'Ca2+', :technic_12 => 'PH'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas.push(item)
    end

    #--------------v2------------
    #发酵车间蒸汽  总收率  数据
    # @daily_report_shy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'SHY')
    # @daily_report_fjy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'FJY')
    # shy_yield= @daily_report_shy.present? ? @daily_report_shy.daily_yield : 0   #酸化液日收率
    # fjy_yield= @daily_report_fjy.present? ? @daily_report_fjy.daily_yield : 0   #发酵液日收率
    # @total_yield_fj = (shy_yield.to_f * fjy_yield.to_f).round(2)  #发酵车间总收率
    # js_method_one(time, @d_report_forms.s_region_code_id, 3, 'fermentation')
    # #返回  @steam_values 日的  @steam_month_sum 累计的
    # @day_steam_fj = @steam_values
    # @steam_month_sum_fj = @steam_month_sum

    # month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #查询日期月的开始日期
    # #发酵车间计算 日单耗 月单耗
    # @shy_nums= @daily_report_shy.present? ? @daily_report_shy.nums : 1   #酸化液的 日产量
    # @shy_month_nums = 1   #酸化液的 月产量
    # @shy_months = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => 'SHY')  #酸化液的 月累计
    # @shy_months.each do |s|
    #   @shy_month_nums += s.nums.to_f
    #   @shy_month_nums = @shy_month_nums -1
    # end
    # @day_expend_fj = (@day_steam_fj.to_f / @shy_nums.to_f).round(2)  #发酵车间日单耗
    # @month_expend_fj = (@steam_month_sum_fj.to_f / (@shy_month_nums).to_f).round(2)  #发酵车间月单耗

    #--------------v2------------
    #发酵车间蒸汽  总收率  数据
    @daily_report_shy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'SHY')
    @daily_report_fjy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'FJY')
    shy_yield= @daily_report_shy.present? ? @daily_report_shy.daily_yield : 0 #酸化液日收率
    fjy_yield= @daily_report_fjy.present? ? @daily_report_fjy.daily_yield : 0 #发酵液日收率

    @total_yield_fj = ((shy_yield.to_f * fjy_yield.to_f) / 100).round(2) #发酵车间总收率

    #蒸汽 日产量
    @steam_values = SNenrgyValue.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => 3, :datetime => time)

    @steam_month_sum_fj = 0 #蒸汽 月累计
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #查询日期月的开始日期
    @steam_month_values = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :d_report_form_id => 3)
    @steam_month_values.each do |data|
      @steam_month_sum_fj += data.field_value.to_f
    end
    @day_steam_fj = @steam_values.present? ? @steam_values.field_value : '' #发酵车间 蒸汽日用量
    @steam_month_sum_fj = @steam_month_sum_fj.round(2) #蒸汽 月累计

    #发酵车间计算 日单耗 月单耗
    @shy_nums= @daily_report_shy.present? ? @daily_report_shy.nums : 1 #酸化液的 日产量
    @shy_month_nums = 1 #酸化液的 月产量
    @shy_months = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => 'SHY') #酸化液的 月累计
    @shy_months.each do |s|
      @shy_month_nums += s.nums.to_f
    end
    @day_expend_fj = (@day_steam_fj.to_f / @shy_nums.to_f).round(2) #发酵车间日单耗
    @month_expend_fj = (@steam_month_sum_fj.to_f / @shy_month_nums.to_f).round(2) #发酵车间月单耗

  end


  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
  end

  #成功跳转到show页面
  # POST  steam_report
  def create
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work

  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
