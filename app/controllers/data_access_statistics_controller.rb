class DataAccessStatisticsController < ApplicationController

  def index
  end

  def init_index
    search_parmas
    new_handle_table
  end

  def export_file
    export_file_params
    new_handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ['站点名称', "SO2", "NO2", "CO", "O3", "PM2.5", "PM10"]
      @yy_h.each do |yy|
        csv << [yy[:station_name].to_s, yy[:so2], yy[:no2], yy[:co], yy[:o3], yy[:pm2_5], yy[:pm10]]
      end
      csv << ['站点平均', @all_infos[:so2], @all_infos[:no2], @all_infos[:co], @all_infos[:o3], @all_infos[:pm2_5], @all_infos[:pm10]]
    end
    if @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "站点数据有效性统计"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "站点数据有效性统计"
    end
    respond_to do |format|
      format.csv {
        send_data @scv,
                  :type => 'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private
  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
    @alarm_levels = params[:alarm_levels]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @station_ids = @search[:station_ids]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
    end
  end


  def new_handle_table
    @yy_h, @so2, @co, @no2, @o3, @pm2_5, @pm10, = [], 0, 0, 0, 0, 0, 0, 0
    @region_leves = @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
    @day_datas = DayStationHourLength.by_get_time(@data_times).by_station_ids(@station_ids)
    @station_id_arr = @day_datas.pluck(:d_station_id).uniq
    station_arr = @station_id_arr.length.to_f * 24.00
    @all_infos = {so2: (@day_datas.sum("so2").to_f / station_arr).round(2) * 100,
                  no2: (@day_datas.sum("no2").to_f / station_arr).round(2) * 100,
                  co: (@day_datas.sum("co").to_f / station_arr).round(2) * 100,
                  o3: (@day_datas.sum("o3").to_f / station_arr).round(2) * 100,
                  pm2_5: (@day_datas.sum("pm2_5").to_f / station_arr).round(2) * 100,
                  pm10: (@day_datas.sum("pm10").to_f / station_arr).round(2) * 100,
    }
    @day_datas.each do |data|
      @yy_h << {station_name: DStation.find(data.d_station_id).station_name.split("-")[1],
                so2: ((data.so2).to_f / 24.to_f).round(2) * 100,
                so2_avg: @all_infos[:so2],
                co: ((data.co).to_f / 24.to_f).round(2) * 100,
                co_avg: @all_infos[:co],
                no2: ((data.no2).to_f / 24.to_f).round(2) * 100,
                no2_avg: @all_infos[:no2],
                o3: ((data.o3).to_f / 24.to_f).round(2) * 100,
                o3_avg: @all_infos[:o3],
                pm2_5: ((data.pm2_5).to_f / 24.to_f).round(2) * 100,
                pm2_5_avg: @all_infos[:pm2_5],
                pm10:((data.pm10).to_f / 24.to_f).round(2) * 100,
                pm10_avg: @all_infos[:pm10]
      }
    end
  end


  def handle_table
    @yy_h, @so2, @co, @no2, @o3, @pm2_5, @pm10, = [], 0, 0, 0, 0, 0, 0, 0
    @region_leves = @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
    @data_yyyy_mms = DDataYyyymm.by_station_statistics(@station_ids).by_get_time(@data_times).unactive
    @station_id_arr = @data_yyyy_mms.pluck(:station_id).uniq
    if @station_ids.present?
      @station_ids.each do |station_id|
        station_length(station_id.to_i)
      end
    else
      @station_id_arr.map {|x| station_length(x.to_i)}
    end
    station_arr = @station_id_arr.length.to_f * 24.00
    @all_infos = {so2: (@data_yyyy_mms.select {|x| x.item_code == 101}.length.to_f / station_arr).round(2) * 100,
                  no2: (@data_yyyy_mms.select {|x| x.item_code == 102}.length.to_f / station_arr).round(2) * 100,
                  co: (@data_yyyy_mms.select {|x| x.item_code == 105}.length.to_f / station_arr).round(2) * 100,
                  o3: (@data_yyyy_mms.select {|x| x.item_code == 106}.length.to_f / station_arr).round(2) * 100,
                  pm2_5: (@data_yyyy_mms.select {|x| x.item_code == 107}.length.to_f / station_arr).round(2) * 100,
                  pm10: (@data_yyyy_mms.select {|x| x.item_code == 108}.length.to_f / station_arr).round(2) * 100, }
  end

  def station_length(station_id)
    @so2 = @data_yyyy_mms.select {|x| x.station_id == station_id && x.item_code == 101}.length.to_f / 24.00
    @co = @data_yyyy_mms.select {|x| x.station_id == station_id && x.item_code == 105}.length.to_f / 24.00
    @no2 = @data_yyyy_mms.select {|x| x.station_id == station_id && x.item_code == 102}.length.to_f / 24.00
    @o3 = @data_yyyy_mms.select {|x| x.station_id == station_id && x.item_code == 106}.length.to_f / 24.00
    @pm2_5 = @data_yyyy_mms.select {|x| x.station_id == station_id && x.item_code == 107}.length.to_f / 24.00
    @pm10 = @data_yyyy_mms.select {|x| x.station_id == station_id && x.item_code == 108}.length.to_f / 24.00
    @yy_h << {station_name: DStation.find(station_id).station_name.split("-")[1],
              so2: @so2.round(2) * 100,
              co: @co.round(2) * 100,
              no2: @no2.round(2) * 100,
              o3: @o3.round(2) * 100,
              pm2_5: @pm2_5.round(2) * 100,
              pm10: @pm10.round(2) * 100
    }
  end

end
