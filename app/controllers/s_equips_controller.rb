class SEquipsController < ApplicationController
  before_action :get_equip, only: [:show, :edit, :update, :destroy, :set_status, :get_handle_equip]
  before_action :authenticate_user
  # skip_before_filter :verify_authenticity_token  #跳过安全令牌验证

  #设备台账管理
  def index
    #车间级的人员只看自己车间的设备信息
    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    if @region_code.s_region_level_id == 1 #2 分公司   1 总公司   3 车间
      region_z = SRegionCode.where(:s_region_level_id => 3)
      region_code_ids = region_z.pluck(:id) #查询所有车间
    end

    if @region_code.s_region_level_id == 2 #2 分公司   1 总公司   3 车间
      region_f = SRegionCode.where(:father_region_id => @region_code.id, :s_region_level_id => 3)
      region_code_ids = region_f.pluck(:id) #查询分公司下的的所有车间
    end

    if @region_code.s_region_level_id == 3 #2 分公司   1 总公司   3 车间
      region_code_ids = @region_code.id
    end

    Rails.logger.info "-----region_code_ids-------#{region_code_ids}----------"


    get_region #查询数据 获取车间
    # @equips = SEquip.where(:s_region_code_id => region_code_ids).by_region(params[:region_ids])
    #              .by_equip_ids(params[:equip_ids])
    #              .order(:created_at => 'desc')
    #              .page(params[:page]).per(params[:per])


    #机修人员只看自己区域的的设备
    if @current_user.s_role_msg.role_code == 'mechanic_repair'
      @equips = SEquip.where(:s_region_code_id => region_code_ids).by_s_area_id(@current_user.s_area_id).by_region(params[:region_ids])
                    .by_equip_ids(params[:equip_ids])
                    .order(:created_at => 'desc')
                    .page(params[:page]).per(params[:per])
    else
      @equips = SEquip.where(:s_region_code_id => region_code_ids).by_region(params[:region_ids])
                    .by_equip_ids(params[:equip_ids])
                    .order(:created_at => 'desc')
                    .page(params[:page]).per(params[:per])
    end
  end


  #从excel导入台账信息
  # post  s_equips/excel_import
  def excel_import
    s_region_code_id = params[:s_region_code_id]
    excel = params[:excel]
    filename = excel.original_filename

    Rails.logger.info "----------filename------#{filename}-------"
    names = filename.split(".")
    file_type = names[1]

    if file_type == 'xlsx'
      region_code = SRegionCode.find_by(:id => s_region_code_id)
      region_name = region_code.present? ? region_code.region_name : ''
      @excel_datas = SEquipsHelper.read_excel(excel) #从excel读取信息

      @excel_datas.each do |e|
        e.push(s_region_code_id.to_i)
        e.push(region_name)
      end

      # Rails.logger.info "----------@excel_datas------#{@excel_datas}-------"
      # Rails.logger.info "----------0------#{@excel_datas[0]}-------"
      # Rails.logger.info "----------1------#{@excel_datas[1]}-------"

      status = SEquipsHelper.save_hash(@excel_datas)  #保存读取到的信息

      # SEquipsHelper.save_excel(@excel_datas) #保存读取到的信息
      #SEquip.save_excel(@excel_datas)  #保存读取到的信息
      render json: {status: 200}
    end
  end

  def new
    get_region
    render partial: 'new', layout: false
  end

  def create
    equip = SEquip.new(get_equip_params)
    respond_to do |format|
      if equip.save
        format.json {render json: {status: 200}}
      else
        format.html {}
        format.json {render json: {status: 'error'}}
      end
    end
  end

  def show
    @s_spares = @equip.s_spares

    # render json: {equip: @equip, s_spare: @s_spares,handle_equip: @handle_equips }
  end

  def edit

    area = SArea.find_by(:id => @equip.s_area_id)
    area_name = area.present? ? area.name : ''
    if @equip.apper_time.present?
      apper_time = @equip.apper_time.strftime("%Y-%m-%d")
    else
      apper_time = ''
    end
    equip ={
        #apper_code: @equip.apper_code,
        apper_code: @equip.apper_code,
        apper_time: apper_time,
        bit_code: @equip.bit_code,
        equip_code: @equip.equip_code,
        equip_location: @equip.equip_location,
        equip_material: @equip.equip_material,
        equip_name: @equip.equip_name,
        equip_nature: @equip.equip_nature,
        equip_norm: @equip.equip_norm,
        equip_note: @equip.equip_note,
        equip_num: @equip.equip_num,
        equip_status: @equip.equip_status,
        factory: @equip.factory,
        id: @equip.id,
        s_region_code_id: @equip.s_region_code_id,
        region_name: @equip.region_name,
        area_name: area_name
    }
    render json: {equip: equip}
  end

  def update
    respond_to do |format|
      if @equip.update(get_equip_params)
        format.json {render json: {status: 200}}
      else
        format.html {}
        format.json {render json: {status: 'error'}}
      end
    end
  end

  def destroy
    spares = SSpare.where(s_equip_id: @equip.id)
    if @equip.destroy && spares.destroy_all
      render json: {status: 200}
    else
      render json: {status: 'error'}
    end
  end

  #禁用启用
  #get /s_equips/set_status
  def set_status
    if @equip
      @equip.update(:equip_status => params[:equip_status])
      render json: {status: 200}
    end
  end

  #----------所属备件信息管理-------------
  #POSt /s_equips/create_spare
  def create_spare
    get_spare_params
    respond_to do |format|
      if @spare.save
        format.json {render json: {status: 200}}
      else
        format.json {render json: {status: 'error'}}
      end
    end
  end

  def edit_spare
    @s_spare = SSpare.find(params[:id])
  end

  #post /s_equips/update_spare
  def update_spare
    @s_spare = SSpare.find(params[:id])
    if @s_spare.update(:spare_name => params[:spare_name], :spare_code => params[:spare_code], :spare_desc => params[:spare_desc], :spare_material => params[:spare_material],
                       :spare_num => params[:spare_num])
      render json: {status: 200}
    else
      render json: {status: 'error'}
    end
  end

  #delete /s_equips/delete_spare
  def delete_spare
    @s_spare = SSpare.find(params[:id])
    if @s_spare.nil?
      render json: {status: '数据不存在'}
    else
      if @s_spare.destroy
        render json: {status: '删除成功'}
      else
        render json: {status: 'error'}
      end
    end
  end

  #get /s_equips/get_handle_equip
  #获取到所属维修记录
  def get_handle_equip
    #handle_equips = @equip.handle_equips
    #只显示已完成的维修记录
    handle_equips = HandleEquip.where(:s_equip_id => @equip.id, :status => 'carry_out')

    han = []
    handle_equips.each do |h|
      han << {id: h.id, region_name: h.region_name, equip_name: h.equip_name, equip_code: h.equip_code,
              handle_name: h.handle_name, handle_start_time: h.handle_start_time.present? ? h.handle_start_time.strftime('%Y-%m-%d') : '',
              handle_end_time: h.handle_end_time.present? ? h.handle_end_time.strftime('%Y-%m-%d') : '', snag_desc: h.snag_desc,
              status: HandleEquip.set_status(h.status), desc: h.desc, founder: h.founder}
    end
    render json: {handle_equips: han, handle_size: han.length}
  end

  #根据车间获取到设备
  #get /s_equips/get_equips
  def get_equips
    region_ids = params[:region_ids]
    @equips = SEquip.select(:id, :equip_name, :equip_code).where(s_region_code_id: region_ids)
    render json: {equips: @equips}
  end

  private
  def get_equip
    @equip = SEquip.find(params[:id])
  end

  #接收设备创建参数
  def get_equip_params
    params.require(:s_equips).permit(:s_region_code_id, :s_area_id, :region_name, :bit_code, :equip_code,
                                     :equip_name, :equip_location, :equip_norm, :equip_nature, :equip_material,
                                     :equip_num, :apper_code, :apper_time, :equip_status, :factory, :equip_note)
  end

  #获取车间
  def get_region

    # puts "group_id: #{@current_user.group_id}"
    # region_code = SRegionCode.find_by(:id => @current_user.group_id)
    # father_region = SRegionCode.find_by(:father_region_id == 99999)
    # if region_code.father_region_id == 99999
    #   @region_code = SRegionCode.select(:id, :region_name).where("father_region_id != 99999", :s_region_level_id =>3)
    # elsif region_code.father_region_id == father_region.id
    #   @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id =>region_code.id, :s_region_level_id =>3)
    # else
    #   @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id => region_code.father_region_id, :s_region_level_id =>3)
    # end

    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    if region_code.s_region_level_id == 1 #2 分公司   1 总公司   3 车间
      @region_code = SRegionCode.select(:id, :region_name).where(:s_region_level_id => 3)
    elsif region_code.s_region_level_id == 2
      @region_code = SRegionCode.select(:id, :region_name).where(:father_region_id => region_code.id, :s_region_level_id => 3)
    elsif region_code.s_region_level_id == 3
      @region_code = SRegionCode.select(:id, :region_name).where(:id => region_code.id, :s_region_level_id => 3)
    else
      @region_code = ''
    end

  end

  #接受备件创建参数
  def get_spare_params
    @spare = SSpare.new(:s_equip_id => params[:s_equip_id],
                        :equip_code => params[:equip_code],
                        :spare_name => params[:spare_name],
                        :spare_code => params[:spare_code],
                        :spare_desc => params[:spare_desc],
                        :spare_material => params[:spare_material],
                        :spare_num => params[:spare_num])
  end


end