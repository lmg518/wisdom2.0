class OrderCompletionController < ApplicationController
  
  #工单完成率
  def index

    regionCode_ids=SRegionCodeInfo.where(:id => params[:s_region_code_info_id]).pluck(:id) #运维单位

    @stations=DStation.active.by_unit_ids(regionCode_ids).by_station_ids(params[:station_id])
                             .by_province(params[:region_code])       #按区域模糊查询
                             .page(params[:page]).per(params[:per])   #查询所有的站点
  end

  #智慧门户接口显示单个站点的所有工单
  # GET /order_completion/task_forms.json
  def task_forms
    id=params[:id]
    if id.present?
      @station=DStation.active.by_station_ids(id)
    end
    
  end

  # GET /order_completion/4
  # GET /order_completion/4.json   查询当前站点下的所有工单
  def show
    @d_task_forms=DTaskForm .order(:created_at => "desc").where(:d_station_id => params[:id])
    @d_task_forms = @d_task_forms.page(params[:page]).per(params[:per])
  end


  def export_file

    #export_file_params

    @expot_file = true

    handle_table

    @csv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["城市", "站点名", "运维单位", "总工单数", "巡检单数", "异常检查单", "故障单数", "待处理", "处理中", "待审核", "已完成", '完成率']
      #csv << ["", "达标率(%)", "达标率(%)", "比较", "达标率(%)", "比较", "2017", "2016", "比较(百分点)", "2017", '2016', '比较天']
      @yy_h.each do |info|
        # info_lj = (info[:current_month_aqi_days].to_f / info[:current_days].to_f).round(2)
        # info_lj_per = (info[:last_year_month_aqi_days].to_f / info[:last_days].to_f).round(2)

        csv << [info[:region_code], info[:station_name], info[:unit_name],
                info[:task_forms_num], info[:task_forms_p], info[:task_forms_a],
                info[:task_forms_f], info[:task_forms_d], info[:task_forms_o],
                info[:task_forms_u], info[:task_forms_w], info[:task_complete]]
      end
    end

      @name = @data_times.strftime("%Y-%m-%d") + "工单完成率统计"
      respond_to do |format|
        format.csv {
          send_data @csv,
                  :type => 'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end

  end



  private
  #查询条件的参数
  def export_file_params
    @region_leve = params[:region_level]
    @station_id = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times]
  end

  def handle_table
    if @data_times.present?
      @data_times = Time.parse(@data_times)
    else
      @data_times = Time.now
    end

   #封装数据
   @stations=DStation.all
   @yy_h=[]
   @stations.each do |s|
   
     region_code = s.region_code    #站点城市
     station_name = s.station_name  #站点名
   
     @regionCode=SRegionCodeInfo.find_by(:id => s.s_region_code_info_id)
     unit_name = @regionCode.present? ? @regionCode.unit_name : ''  #运维单位
   
     @task_forms = DTaskForm.where(:d_station_id => s.id)  #找到该站点的所有工单
     @task_forms_p = DTaskForm.where(:d_station_id => s.id,:fault_type => 'polling_form')   #找到该站点的巡检单
     @task_forms_a = DTaskForm.where(:d_station_id => s.id,:fault_type => 'abnormal_form')  #找到该站点的异常巡检单
     @task_forms_f = DTaskForm.where(:d_station_id => s.id,:fault_type => 'fault_form')     #找到该站点的故障单
   
     @task_forms_d = DTaskForm.where(:d_station_id => s.id,:job_status => ["un_write","writing"])     #待处理的
     @task_forms_o = DTaskForm.where(:d_station_id => s.id,:job_status => ["wait_deal","deal_with","wait_maint","wait_calib"])     #处理中
     @task_forms_u = DTaskForm.where(:d_station_id => s.id,:job_status => 'un_audit')     #待审核
     @task_forms_w = DTaskForm.where(:d_station_id => s.id,:job_status => 'audit')        #已完成
   
     task_forms_num = @task_forms.length  #总工单数
     task_forms_p = @task_forms_p.length  #巡检单数
     task_forms_a = @task_forms_a.length  #异常巡检单数
     task_forms_f = @task_forms_f.length  #故障单数
   
     task_forms_d = @task_forms_d.length  #待处理
     task_forms_o = @task_forms_o.length  #处理中
     task_forms_u = @task_forms_u.length  #待审核
     task_forms_w = @task_forms_w.length  #已完成

      if task_forms_w == 0
        task_complete=0
      else
        task_complete = ((task_forms_w.to_f / task_forms_num.to_f)*100).round(2).to_s << "%"  #完成率
      end
    
     @yy_h << {
        region_code: region_code,
        station_name: station_name,
        unit_name: unit_name,
        task_forms_num: task_forms_num,
        task_forms_p: task_forms_p,
        task_forms_a: task_forms_a,
        task_forms_f: task_forms_f,
        task_forms_d: task_forms_d,
        task_forms_o: task_forms_o,
        task_forms_u: task_forms_u,
        task_forms_w: task_forms_w,
        task_complete: task_complete,
     }
    end
    @yy_h
  end

  

end
