class ConsultsController < ApplicationController

  def index
    s_abnormal_compares = SAbnormalCompare.all
    render json: {compares_types: s_abnormal_compares}
  end

end