class NextMmmWorkJobsController < ApplicationController
    #自动生成运维计划管理
    #参数为0时生成本月计划，参数为1生成下月计划
    #Get next_mmm_work_jobs?month=0     
    def index
        month = params[:month]
        if month.present?
            MonthWorkPlanJob.new.perform(month)
            render html: "运维计划生成完毕"
        else
            render html: "运维计划生成失败"
        end
        
    end
    
end