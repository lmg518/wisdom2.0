class AlarmNumHandlesController < ApplicationController

  def index
    search_parms
    handle_table
    @region_arr #= Kaminari.paginate_array(@region_arr).page(params[:page]).per(10)
  end

  def export_file
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["办事处", "任务数量", "按时响应次数", "按时解决次数", "响应比率", "按时解决响应比率"]
      @region_arr.each do |alarm|
        csv << [alarm[:region_name], alarm[:despatch_counts], alarm[:answer_times], alarm[:doing_times],
                alarm[:despatch_counts].to_i == 0 ? "0.00" :  alarm[:answer_times].to_i / alarm[:despatch_counts].to_i,
                alarm[:despatch_counts].to_i == 0 ? "0.00" : alarm[:doing_times].to_i / alarm[:despatch_counts].to_i ]
      end
    end
    if  @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "报警处理及时性统计"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "报警处理及时性统计"
    end
    respond_to do |format|
      format.csv{
        send_data @scv,
                  :type=>'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private

  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids]
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
    @alarm_levels = params[:alarm_levels]
  end


  def search_parms
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
    end
  end

  def handle_table
    search_parms
    @region_arr = []
    @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
#-----part one
    @region_codes.each do |region|
      stations = region.d_stations
      next if stations.blank?
      @despatch_total = 0
      stations.each do |station|
        alarms = station.d_alarms.by_rule_levels(@alarm_levels)
        next if alarms.blank?
        alarms.each do |alarm|
          alarm_despatches = alarm.d_alarm_despatches.where(:task_status => 'done').by_create_time(@data_times)
          next if alarm_despatches.blank?
          @despatch_total += alarm_despatches.length
          @answer_times, @doing_times = 0,0
          alarm_despatches.each do |despatch|
            answer_date = despatch.created_at
            deal_date = despatch.deal_time
            alarm = despatch.d_alarms.first
            alarm_level = alarm.alarm_level
            sla = SSla.find_by(:alarm_level => alarm_level.to_i)
            task_deal_records = despatch.d_task_deal_records
            next if task_deal_records.blank?
            @answer_times = despatch.d_task_deal_records.where(:task_status => "answer")
                                .where("deal_time <= ?", answer_date + (sla.receive_minutes).hours ).length
            @doing_times = despatch.d_task_deal_records.where(:task_status => "done")
                               .where("deal_time <= ?", deal_date + (sla.deal_minutes).hours).length
          end
        end
      end
      @region_arr << {region_name: region.region_name,
                      despatch_counts: @despatch_total,
                      answer_times: @answer_times,
                      doing_times: @doing_times}
    end
#-----part two
    # @region_codes.each do |region|

    #   logins = region.d_login_msgs
    #   next if logins.blank?
    #   logins.each do |login|
#   @despatch_total = 0
    #     login_despatches = login.d_alarm_despatches.where(:task_status => 'done')
    #     next if login_despatches.blank?
    #     @despatch_total += login_despatches.length
    #     @answer_times, @doing_times = 0,0
    #     login_despatches.each do |despatch|
    #       answer_date = despatch.created_at
    #       deal_date = despatch.deal_time
    #       alarm = despatch.d_alarms.first
    #       alarm_level = alarm.alarm_level
    #       sla = SSla.find_by(:alarm_level => alarm_level.to_i)
    #       task_deal_records = despatch.d_task_deal_records
    #       next if task_deal_records.blank?
    #       @answer_times = task_deal_records.where(:task_status => "answer")
    #                           .where("deal_time <= ?", answer_date + (sla.receive_minutes).hours ).length
    #       @doing_times = task_deal_records.where(:task_status => "done")
    #                          .where("deal_time <= ?", deal_date + (sla.deal_minutes).hours).length
    #     end
    #
    #   end
    #   @region_arr << {region_name: region.region_name,
    #                   despatch_counts: @despatch_total,
    #                   answer_times: @answer_times,
    #                   doing_times: @doing_times}
    # end

    @region_arr
  end

end