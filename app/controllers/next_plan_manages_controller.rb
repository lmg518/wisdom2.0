class NextPlanManagesController < ApplicationController
  before_action :get_ops_pnal, only: [:show,:edit,:update,:destroy]

  def index
  end

  #点击站点后显示的json数据
  def show

    @week_pro_checked = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "7", :job_checked => "Y")
    @month_pro_checked = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "30", :job_checked => "Y")
    @jd_pro_checked = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "90", :job_checked => "Y")
    @year_pro_checked = DOpsPlanDetail.where(:d_ops_plan_manage_id => params[:id],:job_flag => "60", :job_checked => "Y")

    @work_pro = DWorkPro.all.order(:work_type)
    week_time = @ops_plan.week_begin_time
    @week_pro = @work_pro.select{|x| x.work_type == 7}
    @month_pro = @work_pro.select{|x| x.work_type == 30}
    @jd_pro = @work_pro.select{|x| x.work_type == 90}
    @year_pro = @work_pro.select{|x| x.work_type == 360}
    #render json: {ops_plan: @ops_plan ,week_pro: week_pro, month_pro: month_pro, jd_pro: jd_pro, year_pro: year_pro }
  end

  def create
  end

  #上报操作  将状态由已制定--》待审核
  def report
    week_time = params[:week_time]
    d_station_id = params[:d_station_id]
    @ops_plan = DOpsPlanManage.find_by(d_station_id: d_station_id, week_begin_time: week_time)
    if @ops_plan.edit_status == 'O'
      @ops_plan.edit_status='Y'

      @ops_plan.report_if='Y'         #已上报
      @ops_plan.report_time=Time.now  #更新上报时间

      @ops_plan.save
      render :index
    else
      render json:{status: "false"} 
    end

  end


  #保存操作
  def update
    #获取参数  id 时间
    if params[:plan_details_params][:params].present?

        plan_details_params=params[:plan_details_params][:params]  #获取到参数
        plan_details_params.each do |params|
        @ops_plan_detail=@ops_plan.d_ops_plan_details.find_by(:d_work_pro_id =>params[1]['id'])
        @ops_plan_detail.job_time=params[1]['job_time'].to_s + " 23:59:59"   #计划完成时间
        @ops_plan_detail.job_checked='Y'  #当前任务设为 已选择
        @ops_plan_detail.save
      end
      @ops_plan.edit_status='O'  #当前站点以分配
      @ops_plan.save


      #批量操作相同的计划
      plan_ids=params[:station_ids]         #获取所有要计划的站点id
      week_begin_time=params[:week_time]   #获取当前的周开始时间
        plan_ids.each do |id|
          @plan = DOpsPlanManage.find_by(:d_station_id =>id, :week_begin_time => week_begin_time, :edit_status => 'N')
          if @plan.present?
              plan_details_params.each do |params|
              @ops_plan_detail=@plan.d_ops_plan_details.find_by(:d_work_pro_id =>params[1]['id'])
              @ops_plan_detail.job_time=params[1]['job_time'].to_s + " 23:59:59"   #计划完成时间
              @ops_plan_detail.job_checked='Y'  #当前任务设为 已选择
              @ops_plan_detail.save
            end
            @plan.edit_status='O'  #当前站点以分配
            @plan.save
          end
        end

      render :index
      #render json: {location: "success", status: :ok}
    else
      render json: {location: "false", status: :ng}
    end

  end

  private
  def get_ops_pnal
    @ops_plan = DOpsPlanManage.find(params[:id])
  end

end