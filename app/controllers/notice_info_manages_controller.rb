class NoticeInfoManagesController < ApplicationController
  before_action :get_notice_info, only: [:update, :show, :edit, :destroy, :send_message]
  before_action :authenticate_user # 转存公共参数 获取登录信息
  # GET /notice_info_manages
  # GET /notice_info_manages.json
  def index
    @notice_infos=DNoticeInfo.order(:created_at => :desc).page(params[:page]).per(params[:per])
  end

  # GET /notice_info_manages/1
  # GET /notice_info_manages/1.json
  def show
  end

  # 获取所有组的 成员
  # notice_info_manages/get_all_groups.json
  def get_all_groups
    @groups=DGroup.all
  end

  #发送操作
  # GET notice_info_manages/send_message/2.json
  def send_message

    #发送邮件  单个人时
    #@user=DNoticeWorker.find(params[:worker_ids])
    @ids = params[:worker_ids].split(",")
    send_type=params[:send_type]    
    #Rails.logger.info "------------#{@ids}"
    if @ids.length > 0
      @ids.each do |id|
        @user = DNoticeWorker.find(id)

        if send_type == '邮件'
            #邮件推送
           UserMailer.send_mail_msgs(@user,@d_notice_info).deliver_later
        elsif send_type == 'app推送'
            #极光推送  JpushSend.send_msg("张三你好","140fe1da9e9010a3725")
            @local=DLocalize.find_by(:d_login_msg_id => @user.d_login_msg_id)
            if @local.present?
                @infos= "你好：" + @local.login_name + ","+@d_notice_info.content
                JpushSend.send_msg(@infos, @local.uuid)
            end
        else
            #短信发送方式

        end

      end
    end
    @d_notice_info.send_time=Time.now
    @d_notice_info.d_notice_person_id=params[:worker_ids]
    @d_notice_info.status='Y'
    respond_to do |format|
      if @d_notice_info.save
        format.json {render :show}
        format.html {}
      else
        format.json {render json: {status: 'false'}}
        format.html {}
      end
    end
  end



  # POST /notice_info_manages
  # POST /notice_info_manages.json
  def create

    @d_notice_info = DNoticeInfo.new(info_params)
    @d_notice_info.auther=@current_user.login_name  #登录人作为创建人
    @d_notice_info.status='N'
    
    respond_to do |format|
      if @d_notice_info.save
        format.json {render json: {status: 'success', location: @d_notice_info}}
        format.html {}
      else
        format.json {render json: {status: 'false', location: @d_notice_info.errors}}
        format.html {}
      end
    end



  end

  # PATCH/PUT /notice_info_manages/1
  # PATCH/PUT /notice_info_manages/1.json
  def update
    respond_to do |format|
      if @d_notice_info.update(info_params)
        format.json {render :show}
        format.html {}
      else
        format.json {render json: {status: 'false'}}
        format.html {}
      end
    end
  end

  # DELETE /notice_info_manages/1
  # DELETE /notice_info_manages/1.json
  def destroy
    @d_notice_info.destroy
    respond_to do |format|
      format.html { }
      format.json { render :index }
    end
  end

  private 
  def get_notice_info
     @d_notice_info=DNoticeInfo.find(params[:id])
  end
 
  def info_params
    params.require(:d_notice_info).permit(:id, :title, :send_type,
                                  :content)
  end  


  
end
