class SAlarmLevelsController < ApplicationController

  def index 
       @s_alarm_levels = SAlarmLevel.order(:alarm_level => :desc)
       render json: {alarm_levels: @s_alarm_levels}
  end     

end    