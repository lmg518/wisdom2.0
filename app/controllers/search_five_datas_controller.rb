class SearchFiveDatasController < ApplicationController

  def index
    @data_yyyy_mms = DDataYyyymm.by_get_time(params[:d_date])
                                .by_station_ids(params[:station_name] )
                                .by_item_lable(params[:item_lable])
                                .by_item_name(params[:item_id])
                                .active
                                .order(:data_time => :desc)
    @data_yyyy_mms = @data_yyyy_mms.page(params[:page]).per(params[:per])
  end

  def export_file

    @data_yyyy_mms = DDataYyyymm.by_get_time(params[:d_date])
                         .by_station_ids(params[:station_name].present? ? params[:station_name].split(",") : '' )
                         .active
                         .order(:data_time => :desc)
    @scv =  CSV.generate(encoding: "GB18030") do |csv|
      csv << ["站点名称",	"数据周期",	"监控因子",	"因子数值",	"监控标识",	"标识描述",	"时间"]
      @data_yyyy_mms.each do |data|
        csv << [DStation.find(data.station_id).station_name, data.data_cycle_to_s,SAbnormalLtem.find(data.item_code).item_name,
        data.data_value, data.data_label,data.data_label_to_s,data.data_time.strftime("%Y-%m-%d %H:%M:%S")]
      end
    end
    @name = Time.now.strftime("%Y%m%d%H%M%S")
    respond_to do |format|
      format.csv{
        send_data @scv,
                  :type=>'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end


end