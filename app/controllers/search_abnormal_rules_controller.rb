class SearchAbnormalRulesController < ApplicationController
  before_action :set_abnormal_rule, only: [:show]

  def show
    render json: {rule: @abnormal_rule}
  end

  private

  def set_abnormal_rule
     @abnormal_rule = SAbnormmalRule.find(params[:id])
  end

end