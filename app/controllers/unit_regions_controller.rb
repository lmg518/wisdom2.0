class UnitRegionsController < ApplicationController
  before_action :authenticate_user

  def index
    if params[:unit_id].present?
      regioninfo = SRegionCodeInfo.find_by(:id => params[:unit_id])
      return render json: {data: []} if regioninfo.blank?
      render json: {data: regioninfo.d_login_msgs.select("id", "login_name")}
    else
      region_infos = SRegionCodeInfo.select("id", "unit_name").active
      render json: {data: region_infos}
    end
  end

end
