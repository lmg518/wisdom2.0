class LogQueriesController < ApplicationController
  before_filter :authenticate_user
  before_action :set_log_query, only: [:show, :edit, :update, :destroy]

  # GET /log_queries
  # GET /log_queries.json
  def index
    @dStations = DStation.where( "d_stations.region_code" => current_user.region_codes ).order( "id asc" )
	  #根据工号获得所在区域
    @s_region_code = current_user.s_region_code
    #所在区域的下属区域
    # @s_region_code_son = current_user.s_region_code.son_region
    # #区域下属的站点
    # @s_region_code_stations = current_user.s_region_code.son_region.d_stations

    @log_query_type = params[:log_query_type]
    @start_datetime = params[:start_datetime]
    @end_datetime   = params[:end_datetime]
	  @station_id     = params[:station_id]
    @station_name   = params[:station_name]
    
    if @log_query_type == "WLoginOpr"
      @wLoginOprs = WLoginOpr.all
    elsif @log_query_type == "WUploadTaskLog"
      if @station_id.length == 0
        @wUploadTaskLogs = WUploadTaskLog.where( "upload_time < ? and upload_time > ? ", @end_datetime, @start_datetime ).joins( "inner join d_stations on d_stations.id = w_upload_task_logs.station_id" ).where( "d_stations.region_code" => current_user.region_codes )
      else
        @wUploadTaskLogs = WUploadTaskLog.where( "upload_time < ? and upload_time > ? ", @end_datetime, @start_datetime ).joins( "inner join d_stations on d_stations.id = w_upload_task_logs.station_id" ).where( "d_stations.region_code" => current_user.region_codes ).where( "station_id" => @station_id.split(",") )
      end 
    elsif @log_query_type == "WStationUpgradeLog"
      if @station_id.length == 0
        @wStationUpgradeLogs = WStationUpgradeLog.where( "upgrade_date < ? and upgrade_date > ? ", @end_datetime, @start_datetime ).joins( "inner join d_stations on d_stations.id = w_station_upgrade_logs.station_id" ).where( "d_stations.region_code" => current_user.region_codes )
      else 
        @wStationUpgradeLogs = WStationUpgradeLog.where( "upgrade_date < ? and upgrade_date > ? ", @end_datetime, @start_datetime ).joins( "inner join d_stations on d_stations.id = w_station_upgrade_logs.station_id" ).where( "d_stations.region_code" => current_user.region_codes ).where( "station_id" => @station_id.split(",") )
      end
    elsif @log_query_type == "WDataCorrectLog"  
      if @station_id.length == 0
        @wDataCorrectLogs = WDataCorrectLog.where( "w_data_correct_logs.updated_at < ? and w_data_correct_logs.updated_at > ? ", @end_datetime, @start_datetime ).joins( "inner join d_stations on d_stations.id = w_data_correct_logs.station_id" ).where( "d_stations.region_code" => current_user.region_codes )
      else
        @wDataCorrectLogs = WDataCorrectLog.where( "w_data_correct_logs.updated_at < ? and w_data_correct_logs.updated_at > ? ", @end_datetime, @start_datetime ).joins( "inner join d_stations on d_stations.id = w_data_correct_logs.station_id" ).where( "d_stations.region_code" => current_user.region_codes ).where( "station_id" => @station_id.split(",") )
      end
    else
      @log_queries = WLoginOpr.all
    end
  end

  # GET /log_queries/1
  # GET /log_queries/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_log_query
      #@wLoginOpr = WLoginOpr.find(params[:id])
    end

end
