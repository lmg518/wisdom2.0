class AllStationsAreaController < ApplicationController
  #根据区域显示站点的接口  商丘东   商丘西   微型站  扬尘站
  # all_stations_area.json
  def index
    stations=[]
    @stations_d = DStation.active.where(:s_administrative_area_id => [5,6]).map {|x| {id: x.id, station_name: x.station_name}}       #商丘东  虞城 夏邑
    @stations_x = DStation.active.where(:s_administrative_area_id => [3,4,2,7]).map {|x| {id: x.id, station_name: x.station_name}}   #商丘西  宁陵  民权 睢县 柘城

    @stations_w = DStation.active.where(:station_type => '微型站').map {|x| {id: x.id, station_name: x.station_name}}  #微型站     
    @stations_y = DStation.active.where(:station_type => '扬尘站').map {|x| {id: x.id, station_name: x.station_name}}  #扬尘站     
    stations << {name: '商丘东', station:@stations_d}
    stations << {name: '商丘西', station:@stations_x}
    stations << {name: '微型站', station:@stations_w}
    stations << {name: '扬尘站', station:@stations_y}

    return render json:{stations:stations}
  end
   
end
