class SearchNoticeTemplatesController < ApplicationController

  def index
    @index = SNoticeTemplate.all
    render json: {notice_template: @index}
  end

end