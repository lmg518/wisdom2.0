class GetFormModuleController < ApplicationController
  before_action :authenticate_user

  #  get_form_module.json?id=35
  def index
     @form_module=FFormModule.find_by(:d_work_pro_id => params[:id])
     title_ids=@form_module.field_title_id.split(",")
     @field_titles=FFieldTitle.where(:id => title_ids)

     @titles=Hash.new
     @field_titles.each do |f|
      @titles[f.code] = f.name
     end
  end


  #颗粒物手工比对采样记录表  每月
  def manual_comparison_particulates_month
    get_datas
    get_values2
    get_values_klw   #首页数据  所有采样记录表
    render :partial => "manual_comparison_particulates_month", :layout => false
  end

  #采样记录表
  def sampling_record_table
    get_datas2
    get_values3
    render :partial => "sampling_record_table", :layout => false
  end

  #称量记录表
  def weighing_record_table
    get_datas2
    get_values3
    render :partial => "weighing_record_table", :layout => false
  end



  #PM2.5检查 校准记录表
  def pm25_check_calibration_record
    get_datas
    get_values2
    render :partial => "pm25_check_calibration_record", :layout => false
  end

  #PM10检查 校准记录表
  def pm10_check_calibration_record
    get_datas
    get_values2
    render :partial => "pm10_check_calibration_record", :layout => false
  end

  #氮氧化物监测仪器 校准数据报告
  def nox_monitoring_instrument_calibration
    get_datas
    get_values2
    render :partial => "nox_monitoring_instrument_calibration", :layout => false
  end

  # 二氧化硫监测仪器 校准数据报告
  def so2_monitoring_instrument_calibration
    get_datas
    get_values2
    render :partial => "so2_monitoring_instrument_calibration", :layout => false
  end


  #臭氧监测仪器 校准数据报告
  def o3_monitoring_instrument_calibration
    get_datas
    get_values2
    render :partial => "o3_monitoring_instrument_calibration", :layout => false
  end

  #一氧化碳监测仪器 校准数据报告
  def co_monitoring_instrument_calibration
    get_datas
    get_values2
    render :partial => "co_monitoring_instrument_calibration", :layout => false
  end

#---------------------------------


 # 备机更换记录表
 def replacement_recorder
  get_datas
   get_values
   render :partial => "replacement_recorder", :layout => false
 end 
  # 空气自动监测仪器设备检修记录表
 def atmosphere_automatic_monitoring_instrument
  get_datas
   get_values
   render :partial => "atmosphere_automatic_monitoring_instrument", :layout => false
 end 


  #臭氧（O3）校准仪（工作标准）量值传递记录表（每半年）
  def o3_calibrator_value_transfer
    get_datas
    get_values
    render :partial => "o3_calibrator_value_transfer", :layout => false
  end

  #能见度分析仪校准记录表（每半年）  
  def visibility_analyzer_calibration_year
    get_datas
    get_values
    render :partial => "visibility_analyzer_calibration_year", :layout => false
  end

  #多气体动态校准仪校准检查记录表（每半年）
  def multi_gas_dynamic_calibrator_year
    get_datas
    get_values
    render :partial => "multi_gas_dynamic_calibrator_year", :layout => false
  end

  #氮氧化物分析仪钼炉转化率记录表（每半年） 
  def nox_analyzer_mofce_year
    get_datas
    get_values
    render :partial => "nox_analyzer_mofce_year", :layout => false
  end

  #站点设备半年维护记录（每半年）
  def site_equipment_maintain_year
    get_datas
    get_values
    render :partial => "site_equipment_maintain_year", :layout => false
  end

  #长光程O3分析仪单点校准检查记录表（每季度）
  def o3long_path_analyzer_quarter
    get_datas
    get_values
    render :partial => "o3long_path_analyzer_quarter", :layout => false
  end

  #长光程SO2分析仪单点校准检查记录表（每季度）
  #长光程NO2分析仪单点校准检查记录表（每季度）
  #长光程O3分析仪单点校准检查记录表（每季度）
  def long_path_analyzer_quarter
    get_datas
    get_values
    render :partial => "long_path_analyzer_quarter", :layout => false
  end

  #颗粒物PM10自动监测分析仪运行状况检查记录（每季度）
  #粒物PM2.5自动监测分析仪运行状况检查记录（每季度）   
  def automatic_monitoring_analyzer_quarter
    get_datas
    get_values
    render :partial => "automatic_monitoring_analyzer_quarter", :layout => false
  end

  #颗粒物温度、压力校准记录表
  def grain_temperature_pressure_calibration_quarter
    get_datas
    get_values
    render :partial => "grain_temperature_pressure_calibration_quarter", :layout => false
  end

  #站点设备清洁记录（每季度）
  def site_equipment_cleaning_quarter
    get_datas
    get_values
    render :partial => "site_equipment_cleaning_quarter", :layout => false
  end

  #(二氧化硫)仪器精密度审核记录表（每季度）  
  #(氮氧化物)仪器精密度审核记录表（每季度）                  
  #(一氧化碳)仪器精密度审核记录表（每季度） 
  #(臭氧)仪器精密度审核记录表（每季度）
  def instrument_precision_review_quarter
    get_datas
    get_values
    render :partial => "instrument_precision_review_quarter", :layout => false
  end

  #气体分析仪（二氧化硫）多点校准记录表（每季度）
  #气体分析仪（臭氧）多点校准记录表（每季度）
  #气体分析仪（二氧化氮）多点校准记录表（每季度）
  #气体分析仪（一氧化碳）多点校准记录表（每季度）
  def multipoint_calibration_quarter
    get_datas
    get_values
    render :partial => "multipoint_calibration_quarter", :layout => false
  end

  #多气体动态校准仪校准检查记录表
  def multiGas_dynamic_calibrator_month
    get_datas
    get_values
    render :partial => "multiGas_dynamic_calibrator_month", :layout => false
  end

  #气体分析仪流量检查记录表（每月）
  def gas_analyzer_FlowCheck_month
    get_datas
    get_values
    render :partial => "gas_analyzer_FlowCheck_month", :layout => false
  end

  #站点设备维护记录（每月）
  def site_plant_maintenance_month
    get_datas
    get_values
    render :partial => "site_plant_maintenance_month", :layout => false
  end

  #一氧化碳（CO）分析仪运行状况检查记录表
  def co_analyser_status_week
    get_datas
    get_values
    render :partial => "co_analyser_status_week", :layout => false
  end

  #二氧化硫（SO2）分析仪运行状况检查记录表
  def so2_analyser_status_week
    get_datas
    get_values
    render :partial => "so2_analyser_status_week", :layout => false
  end

  #氮氧化物（NOX）分析仪运行状况检查记录表
  def nox_analyser_status_week
    get_datas
    get_values
    render :partial => "nox_analyser_status_week", :layout => false
  end

  #臭氧(O3)分析仪运行状况检查记录表
  def analyser_status_week
    get_datas
    get_values
    render :partial => "analyser_status_week", :layout => false
  end

  #长光程（SO2、NO2、O3）分析仪运行状况检查记录表（每周）
  def optical_distance_week
    get_datas
    get_values
    render :partial => "optical_distance_week", :layout => false
  end

  #颗粒物PM2.5自动监测分析仪运行状况检查记录（每周）
  def pm_check_week
    get_datas
    get_values
    render :partial => "pm_check_week", :layout => false
  end

  #每周（次）巡检工作汇总表
  def inspection_work_week
    get_datas
    get_values
    render :partial => "inspection_work_week", :layout => false
  end

  #其他仪器、设备运行状况检查记录（每周）
  def run_status_week
    get_datas
    get_values
    render :partial => "run_status_week", :layout => false
  end

  #get_form_module/station_inspection_week?id=237&name=站点巡检记录
  #站点巡检记录（每周）
  def station_inspection_week
    get_datas
    get_values
    render :partial => "station_inspection_week", :layout => false
  end


#根据工单id   模板code   获取模板数据
def get_datas2
  @d_task_form=DTaskForm.find(params[:id])            #获取工单

  region = @d_task_form.s_region_code_info
  if region.present?    
    @unit_name =region.unit_name               #运维单位
  else
    @unit_name = ""
  end
  station = @d_task_form.d_station
  @station_name = station.station_name         #站点名称
  @region_code = station.region_code           #城市

  @form_module=FFormModule.find_by(:code => params[:code])      #根据模板 code 获取模板
  if @form_module.present?
    title_ids=@form_module.field_title_id.split(",")
    @field_titles=FFieldTitle.where(:id => title_ids)           #获取模板中的字段
    #将数组转为一个Hash
    @titles=Hash.new
    @field_titles.each do |f|
     @titles[f.code] = f.name
    end
  else
    @titles=Hash.new
  end
end




  #根据工单id  检查项名name 获取模板数据
  def get_datas
    @d_task_form=DTaskForm.find(params[:id])            #获取工单

    region = @d_task_form.s_region_code_info

    @unit_name = ""
    @unit_name =region.unit_name if region.present? 
   
    # if region.present?    
    #   @unit_name =region.unit_name               #运维单位
    # else
    #   @unit_name = ""
    # end

    station = @d_task_form.d_station
    @station_name = station.station_name         #站点名称
    @region_code = station.region_code           #城市
    #@login_name = @current_user.login_name       #当前登录人

    @audit_man = @d_task_form.audit_man            #审核人
    @handle_man = @d_task_form.handle_man          #处理人
    @review_man = @d_task_form.review_man          #复核人


    #关联校准表信息(校准不通过时，有多条取最近的)
    @equip_calib=SEquipCalib.order(:created_at => :desc).find_by(:d_task_form_id => params[:id])  
    @calibs_man = @equip_calib.present? ? @equip_calib.calibs_man : ''
    @calibs_time = @equip_calib.present? ? @equip_calib.calibs_time.strftime("%Y-%m-%d %H:%M:%S") : '' #校准时间


    @form_module=FFormModule.find_by(:name => params[:name])   #根据检查项名获取模板
    if @form_module.present?

      #耗材信息,返回当前工单，当前模板的
      @f_module_images=FModuleImage.where(:d_task_form_id => params[:id], :f_form_module_id =>@form_module.id)

      title_ids=@form_module.field_title_id.split(",")
      @field_titles=FFieldTitle.where(:id => title_ids)            #获取模板中的字段
      #将数组转为一个Hash
      @titles=Hash.new
      @field_titles.each do |f|
       @titles[f.code] = f.name
      end
    else
      @titles=Hash.new
    end
  end


 #返回 称量记录   保存的结果
 def get_values3
  #@d_task_form=DTaskForm.find(params[:id])                   #获取工单
   @form_module=FFormModule.find_by(:code => params[:code])   #根据模板code 获取模板

   @f_field_values=FFieldValue.where(:d_task_form_id => params[:id], :f_form_module_id => @form_module.id) if @form_module.present?

   if @f_field_values.length >0
     @field_values=Hash.new

     @f_field_values.each do |v|
       #title=FFieldTitle.find(v.f_field_title_id)
       @field_values[v.field_title] = v.field_value
     end
   end
 end

 #返回 颗粒物 首页  的结果
 def get_values_klw

  #   code='sampling_record_01' 
  #   @field_values_01 = Hash.new
  #   @field_values_02 = Hash.new
  #   @field_values_03 = Hash.new


  #  #获取 采样记录表1 模板
  #  @form_module=FFormModule.find_by(:code => 'sampling_record_01')   
  #  if @form_module.present?
  #   @f_field_values=FFieldValue.where(:d_task_form_id => params[:id], :f_form_module_id => @form_module.id) 
  #   if @f_field_values.length >0
  #     @field_values_01=Hash.new
  #     @f_field_values.each do |v|
  #       @field_values_01[v.field_title] = v.field_value
  #     end
  #   end
  #  end
   
  #获取 采样记录表2 模板
  # @form_module=FFormModule.find_by(:code => 'sampling_record_02')   
  # if @form_module.present?
  # @f_field_values=FFieldValue.where(:d_task_form_id => params[:id], :f_form_module_id => @form_module.id) 
  # if @f_field_values.length >0
  #   @field_values_02=Hash.new
  #   @f_field_values.each do |v|
  #     @field_values_02[v.field_title] = v.field_value
  #   end
  # end
  # end

 # //...
  form_module=FFormModule.select("id","code").where(:code => ['sampling_record_1','sampling_record_2','sampling_record_3','sampling_record_4','sampling_record_5',
                                                              'sampling_record_6','sampling_record_7','sampling_record_8','sampling_record_9','sampling_record_10'])
  #render json:{ item_data:  do_hash(form_module,params[:id]) }
  do_hash(form_module,params[:id]) 
 end

  def do_hash(form_module,form_id)
    @form_hash = []
    form_module.each_with_index do |mod,index|
      f_field_values=FFieldValue.where(:d_task_form_id => form_id, :f_form_module_id => mod.id)
      @file_hash = {}
      f_field_values.map{|x| @file_hash[x.field_title] = x.field_value }
      @form_hash << {item_index: mod.code[-2,2],item: @file_hash}
    end
    return  @form_hash.sort{|x,y| x[:item_code] <=> y[:item_code] }
  end

 
#  def get_values_by_code(code)
#   #获取 采样记录表2 模板
#   @form_module=FFormModule.find_by(:code => 'sampling_record_02')   
#   if @form_module.present?
#   @f_field_values=FFieldValue.where(:d_task_form_id => params[:id], :f_form_module_id => @form_module.id) 
#   if @f_field_values.length >0
#     @field_values=Hash.new
#     @f_field_values.each do |v|
#       @field_values[v.field_title] = v.field_value
#     end
#   end
#   end
#  end

  #返回 校准模板 保存的结果
  def get_values2
   #@d_task_form=DTaskForm.find(params[:id])                   #获取工单
    @form_module=FFormModule.find_by(:name => params[:name])   #根据检查项名获取模板

    @f_field_values=FFieldValue.where(:d_task_form_id => params[:id], :f_form_module_id => @form_module.id) if @form_module.present?

    if @f_field_values.length >0
      @field_values=Hash.new

      @f_field_values.each do |v|
        #title=FFieldTitle.find(v.f_field_title_id)
        @field_values[v.field_title] = v.field_value
      end
    end
  end



  #返回保存的结果
  def get_values
     @form_module=FFormModule.find_by(:name => params[:name])   #根据检查项名获取模板
     @f_field_values=FFieldValue.where(:d_task_form_id => params[:id], :f_form_module_id => @form_module.id)
 
     if @f_field_values.length >0
       @field_values=Hash.new
       @f_field_values.each do |v|
         title=FFieldTitle.find(v.f_field_title_id)
         @field_values[title.code] = v.field_value
       end
     end
   end


  #校准模板保存
  #GET /get_form_module/save/id
  def save
    respond_to do |format|

      @datas=params[:datas]
      if @datas.length >0
        @datas.each do |d|
          # @f_field_title=FFieldTitle.find_by(:code => d[1]["check_name"])
          # 
          # @f_value=FFieldValue.find_by(:d_task_form_id => params[:id], 
          #                               :f_form_module_id => params[:f_form_module_id], 
          #                               :f_field_title_id =>@f_field_title.id)

          #判断是否已经存过
          @f_value=FFieldValue.find_by(:d_task_form_id => params[:id], 
                                        :f_form_module_id => params[:f_form_module_id], 
                                        :field_title =>d[1]["check_name"])
          if @f_value.present?
            @f_value.field_value = d[1]["check_value"]
            @f_value.save
          else
            @f_field_value=FFieldValue.new
            @f_field_value.d_task_form_id = params[:id]
            @f_field_value.f_form_module_id = params[:f_form_module_id]
            @f_field_value.field_title = d[1]["check_name"]
            @f_field_value.field_value = d[1]["check_value"]
            @f_field_value.f_field_title_id = @f_field_title.id if @f_field_title.present?
  
            # if d[1]["check_value"] != ''
            #   @f_field_value.save
            # end
            @f_field_value.save
          end
        end
  
      end
      #@form_module=FFormModule.find(params[:f_form_module_id])  #月 颗粒物手工   保存  跳转所需数据
      format.json { render json: {status: :ok, id: 39, code: 'mh_004', name: '颗粒物手工比对采样记录表'}}
    end
  end


  #保存表格数据
  def update
    respond_to do |format|
    # @d_task_form=DTaskForm.find(params[:id])                        #获取工单
    # @form_module=FFormModule.find(params[:f_form_module_id])        #根据模板id获取模板
    @datas=params[:datas]
    if @datas.length >0
      @datas.each do |d|
        @f_field_title=FFieldTitle.find_by(:name => d[1]["check_name"])
        #判断是否已经存过
        @f_value=FFieldValue.find_by(:d_task_form_id => params[:id], 
                                      :f_form_module_id => params[:f_form_module_id], 
                                      :f_field_title_id =>@f_field_title.id)

        if @f_value.present?
          @f_value.field_value = d[1]["check_value"]
          @f_value.save
        else
          @f_field_value=FFieldValue.new
          @f_field_value.d_task_form_id = params[:id]
          @f_field_value.f_form_module_id = params[:f_form_module_id]
          @f_field_value.field_title = d[1]["check_name"]
          @f_field_value.field_value = d[1]["check_value"]
          @f_field_value.f_field_title_id = @f_field_title.id if @f_field_title.present?

          # if d[1]["check_value"] != ''
          #   @f_field_value.save
          # end
          @f_field_value.save
        end
      end
    end


    #耗材图片上传
    d_task_form_id = params[:id] 
    tasks = params[:task][:tasks]  if params[:task].present?  #获取多个图片
    if tasks.present?
      upload_img(tasks,d_task_form_id) 
    end
    

    #  image_file = params[:d_task_form][:image_file]
    #  if image_file.present?
    #    img_flag = "jpeg"  if image_file[0,15] == "data:image/jpeg"
    #    img_flag = "png"  if image_file[0,14] == "data:image/png"
    #    png      = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
    #    time = Time.now
    #    file_path = "#{Rails.root}/public/test/module/#{time.year}/#{time.month}/#{time.day}/"
    #    picture = "#{time.strftime("%H%M%S")}.#{img_flag}"
    #    FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
    #    #向dir目录写入文件
    #    File.open(Rails.root.join("#{file_path}", picture ), 'wb') { |f| f.write(png) }
    #    #存储图片
    #    @f_module_image=FModuleImage.new
    #    @f_module_image.img_one = "/test/#{time.year}/#{time.month}/#{time.day}/"+"#{picture}"
    #    @f_module_image.f_form_module_id = params[:f_form_module_id]
    #    @f_module_image.d_task_form_id = params[:id]
    #    @f_module_image.save
    #  end




   format.json { render json: {status: :ok} }
   end
  end



  #上传多个图片
  def upload_img(tasks,d_task_form_id)
    #@task = DTaskForm.find(d_task_form_id)
      tasks.each_value do |task| 
        cons_name = task['cons_name']    #耗材名称
        num = task['num']                #耗材数量
        image_file = task['image_file']
        img_flag = "jpeg"  if image_file[0,15] == "data:image/jpeg"
        img_flag = "png"  if image_file[0,14] == "data:image/png"
        png      = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
        #@handle = DFaultHandle.find_by(:d_task_form_id =>d_task_form_id,:handle_type=> handle_type)

        time = Time.now
        #picture = "#{@f_module_image.id}.#{img_flag}"
        file_path = "#{Rails.root}/public/test/module/#{time.year}/#{time.month}/#{time.day}/"
        
        @f_module_image=FModuleImage.new
        #@f_module_image.img_one = "/test/#{time.year}/#{time.month}/#{time.day}/"+"#{picture}"
        @f_module_image.f_form_module_id = params[:f_form_module_id]
        @f_module_image.d_task_form_id = d_task_form_id
        @f_module_image.cons_name = cons_name
        @f_module_image.num = num
        @f_module_image.save

        #time = Time.now
        #file_path = "#{Rails.root}/public/test/#{time.year}/#{time.month}/#{@task.id}/"
        #file_path = "#{Rails.root}/public/test/module/#{time.year}/#{time.month}/#{time.day}/"
        picture = "#{@f_module_image.id}.#{img_flag}"
        FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
        #向dir目录写入文件
        File.open(Rails.root.join("#{file_path}", picture ), 'wb') { |f| f.write(png) }
        
        #存储图片url
        #@f_module_image.update(:img_one => "/test/#{time.year}/#{time.month}/#{@f_module_image.id}/"+"#{picture}",:handle_time =>Time.now)
        @f_module_image.update(:img_one => "/test/module/#{time.year}/#{time.month}/#{time.day}/"+"#{picture}")
        end
    end












end