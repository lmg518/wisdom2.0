class SearchAbnormarlInstancesController < ApplicationController
   
   def index
     @s_abnormal_rule_instances =  SAbnormalRuleInstance.where(:status => '是')
     render json: {rule_instances: @s_abnormal_rule_instances.map{|x| {id: x.id,instance_name: x.instance_name} }}
   end    

end    