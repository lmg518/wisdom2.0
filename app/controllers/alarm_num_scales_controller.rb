class AlarmNumScalesController < ApplicationController

  def index
    search_parmas
    handle_table
    @alarms.map{|x| puts x.inspect}
    @alarms #= Kaminari.paginate_array(@alarms).page(params[:page]).per(10)

  end

  def export_file
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["区域", "数量", "占比"]
      @alarms.each do |alarm|
        csv << [alarm[:region_name], alarm[:alarm_count], (alarm[:alarm_count].to_f / @alarm_total.to_f).round(2) ]
      end
    end
    if  @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "报警数量占比统计"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "报警数量占比统计"
    end
    respond_to do |format|
      format.csv{
        send_data @scv,
                  :type=>'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private
  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids]
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
    @alarm_levels = params[:alarm_levels]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
    end
  end

  def handle_table
    @alarms = []
    @alarm_total = 0
    @region_leves = @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
    @region_codes.each do |region|
      begin
        @alarm_count = 0
        stations = region.d_stations
        stations.each do |station|
          @alarm_count += station.d_alarms.by_alarm_times(@data_times).by_rule_levels(@alarm_levels).length
        end
        next if @alarm_count == 0
        @alarm_total += @alarm_count
        @alarms << {region_name: region.region_name, alarm_count: @alarm_count}
      # rescue Exception => e
      #   next
      end
    end
  end

end