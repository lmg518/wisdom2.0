class PostReportsController < ApplicationController
  include PostReportsHelper
  before_action :set_work, only: [:show, :update, :destroy]
  before_action :authenticate_user
  ##车间岗位盘点报表
  #首页
  def index
    get_login_region_code_ids
    @s_region_codes = SRegionCode.where(:id => @region_code_ids).order(:created_at => :asc)
  end

  #获取登录人所在的所有车间id
  def get_login_region_code_ids
    #获取查询参数
    params_region_ids = params[:region_id]
    #根据登录人员 获取人员所在的车间 或公司
    @region_code1= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1 #2 分公司   1 总公司   3 车间
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 2
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_f = SRegionCode.where(:father_region_id => @region_code1.id, :s_region_level_id => 3)
        region_code_ids = region_f.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 3
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_code_ids = @region_code1.id
      end
    end
    @region_code_ids = region_code_ids
    return @region_code_ids
  end

  #发酵、酸化交料产量详细信息接口
  # post_reports/get_product.json?s_material_id=3&date_time=2018-07-26
  def get_product
    s_material_id = params[:s_material_id] #物料id
    s_material = SMaterial.find_by(:id => s_material_id)
    date_time = params[:date_time]
    daily_batchs = DTwoMenuValue.where(:s_material_id => s_material_id, :datetime => date_time, :field_code => ['batch'])
    render json: {daily_batchs: daily_batchs, code: s_material.code}
  end

  #期盘点 接口
  #post_reports/period_inventory.json?s_material_id=2&begin_time=2018-07-01&end_time=2018-07-30
  def period_inventory
    s_material_id = params[:s_material_id] #物料id
    @smaterial = SMaterial.find_by(:id => s_material_id)

    if params[:begin_time].present? && params[:end_time].present?
      begin_time = params[:begin_time]
      end_time = params[:end_time]
    else
      begin_time = (Time.now + (-7.day)).strftime("%Y-%m-%d")
      end_time = Time.now.strftime("%Y-%m-%d")
    end

    d_two_menu_values = DTwoMenuValue.where(:s_material_id => s_material_id).by_datetime(begin_time, end_time)
    week_times = d_two_menu_values.pluck(:datetime).uniq.sort

    #头粉 期盘点
    if @smaterial.present? && 'TF' == @smaterial.code #头粉
      @titles= ['开始日期', '结束日期', '本期余量', '本期购进总量', '实际结算量', '本期时耗量']
      @check_data=[]
      week_datas = PostReportsHelper.week_js(begin_time, end_time, @smaterial.id)
      if week_datas.present?
        item={}
        item[:time1] = begin_time
        item[:time2] = end_time
        item[:b_yl] =week_datas[0].sum.round(2) #本期余量
        item[:b_gj] =week_datas[1].sum.round(2) #本期购进
        item[:b_sh] =week_datas[2].sum.round(2) #本期时耗量
        item[:b_js] = week_datas[3].sum.round(2) #实际结算
        @check_data.push(item)
      end
    end

    #糖化 期盘点
    if @smaterial.present? && 'TY' == @smaterial.code #糖化
      @titles= ['开始日期', '结束日期', '总结余', '时耗淀粉', '产糖（T）', '收率%','淀粉酶单耗(%)','糖化酶单耗(%)']
      @check_data=[]
      week_datas = PostReportsHelper.week_th_js(begin_time, end_time, @smaterial.id)
      if week_datas.present?
        item={}
        item[:time1] = begin_time
        item[:time2] = end_time
        item[:zjy] = week_datas[0].sum.round(2) #总结余
        item[:shdy] = week_datas[1].sum.round(2) #时耗淀粉
        item[:ct] = week_datas[2].sum.round(2) #产糖
        if week_datas[1].present? && week_datas[1].sum != 0
          item[:sl] = ((week_datas[2].sum / week_datas[1].sum) * 100).round(2) #收率
          item[:df_dh] = ((week_datas[3].sum / week_datas[1].sum) * 100).round(2) #淀粉酶单耗
          item[:th_dh] = ((week_datas[4].sum / week_datas[1].sum) * 100).round(2) #糖化酶单耗
        else
          item[:sl] = ''
          item[:df_dh] = ''
          item[:th_dh] = ''
        end
        @check_data.push(item)
      end
    end

    #发酵 期盘点
    if @smaterial.present? && 'FJY' == @smaterial.code #发酵
      @titles= ['开始日期', '结束日期', '总结余', '时耗', '产量', '收率%','消沫剂']
      @check_data=[]
      week_datas = PostReportsHelper.week_fj_js(begin_time, end_time, @smaterial.id)
      if week_datas.present?
        item={}
        item[:time1] = begin_time
        item[:time2] = end_time
        item[:zjy] = week_datas[0].sum.round(2) #总结余
        item[:shdy] = week_datas[1].sum.round(2) #时耗
        item[:ct] = week_datas[2].sum.round(2) #产量
        item[:xmj] = week_datas[3].sum.round(2)  #消沫剂
        if week_datas[1].present? && week_datas[1].sum != 0
          item[:sl] = ((week_datas[2].sum / week_datas[1].sum) * 100).round(2) #收率
        else
          item[:sl] = ''
        end
        @check_data.push(item)
      end
    end

    #酸化 期盘点
    if @smaterial.present? && 'SHY' == @smaterial.code #酸化
      @titles= ['开始日期', '结束日期', '总结余', '时耗', '产量', '收率%']
      @check_data=[]
      week_datas = PostReportsHelper.week_sh_js(begin_time, end_time, @smaterial.id)
      if week_datas.present?
        item={}
        item[:time1] = begin_time
        item[:time2] = end_time
        item[:zjy] = week_datas[0].sum.round(2) #总结余
        item[:shdy] = week_datas[1].sum.round(2) #时耗
        item[:ct] = week_datas[2].sum.round(2) #产量
        if week_datas[1].present? && week_datas[1].sum != 0
          item[:sl] = ((week_datas[2].sum / week_datas[1].sum) * 100).round(2) #收率
        else
          item[:sl] = ''
        end
        @check_data.push(item)
      end
    end
    render json: {check_data: @check_data, titles: @titles, code: @smaterial.code}
  end


  #从set_user 获取数据
  def show
    s_material_id = params[:s_material_id] #物料id  默认头粉的
    #从权限报表配置s_role_msg_region_codes 获取关联数据
    #get_login_region_code_ids #获取登录人员所在的车间id @region_code_ids

    #详细页面 显示一个车间的信息
    @SRoleMsgs = SRoleMsgRegionCode.where(:s_region_code_id => params[:id], :s_role_msg_id => @current_user.s_role_msg_id)
    if @SRoleMsgs.present?
      ids = @SRoleMsgs.pluck(:s_material_id)
      @smaterials = SMaterial.where(:id => ids) #从材料表中获取

      if params[:begin_time].present? && params[:end_time].present?
        s_time = params[:begin_time]
        end_time = params[:end_time]
      else
        #s_time = Time.now.beginning_of_month.strftime("%Y-%m-%d")
        s_time = ((Time.now + (-7.day)).beginning_of_week + 3.day).strftime("%Y-%m-%d") #上周四
        end_time = Time.now.strftime("%Y-%m-%d")
      end

      #添加查询开始日期的前一天的数据，计算使用
      begin_time = (Time.parse(s_time) + (-1.day)).strftime("%Y-%m-%d")
      #begin_time = s_time
      Rails.logger.info "---------查询日期--------#{begin_time}--#{end_time}--"

      #按日期范围查询值
      @daily_reports = DTwoMenuValue.where(:s_material_id => s_material_id).by_datetime(begin_time, end_time)
      #查询物料下的一级 二级菜单数据  需要传 s_material_id
      @smaterial = SMaterial.find_by(:id => s_material_id)
      #根据 岗位id 找岗位下的一级菜单数据、二级菜单数据
      @d_one_menus = DOneMenu.where(:s_material_id => s_material_id)

      @menus_datas=[] #封装菜单数据
      @d_one_menus.each do |one|
        one_item ={} #一级菜单数据
        data=[]
        one_item[:name] = one.name
        one_item[:code] = one.code
        two_menus = one.d_two_menus
        two_menus.each do |two|
          two_item={}
          two_item[:name] = two.name
          data.push(two_item)
        end

        #头粉 下余、购进 都添加一个 小计
        if @smaterial.present? && 'TF' == @smaterial.code
          data.push({name: '小计（T）'})
        end

        #糖化 交料产量  添加额外字段
        if @smaterial.present? && 'TY' == @smaterial.code
          if one.code == 'product' #交料产量
            data.push({name: '总计（T）'})
          end
        end

        #糖化 下余物料1 /2   添加额外字段
        if @smaterial.present? && 'TY' == @smaterial.code
          if ['left1', 'left2'].include? one.code
            data.push({name: '小计'})
          end
        end

        #发酵 交料产量   添加二级菜单数据
        if @smaterial.present? && 'FJY' == @smaterial.code
          if ['product'].include? one.code
            #交料产量 二级菜单 返回一个交料产量小计
            data.push({name: '交料产量小计'})
          end
          if ['left1', 'left2'].include? one.code
            data.push({name: '小计'})
          end
        end

        #酸化 添加额外 二级菜单数据
        if @smaterial.present? && 'SHY' == @smaterial.code
          if ['product'].include? one.code
            data.push({name: '交料产量小计'})
          end
          if ['left_fjy', 'left_ly'].include? one.code
            data.push({name: '小计'})
          end
        end

        one_item[:data] = data
        @menus_datas.push(one_item)
      end

      if @smaterial.present? && 'TF' == @smaterial.code #头粉 添加额外的字段
        item ={name: '实耗', data: [{name: '干粉（包）'},
                                  {name: '湿粉（包）'},
                                  {name: '湿粉比例'},
                                  {name: '小计（T）'}
        ]}
        @menus_datas.push(item)
      end

      if @smaterial.present? && 'TY' == @smaterial.code #糖液 添加额外的字段
        item ={name: '综合', data: [{name: '下余总计'},
                                  {name: '产糖'},
                                  {name: '时耗'},
                                  {name: '收率'}
        ]}
        @menus_datas.push(item)
      end

      if @smaterial.present? && 'FJY' == @smaterial.code #发酵 添加额外的字段
        item ={name: '综合', data: [{name: '总计'},
                                  {name: '产量'},
                                  {name: '时耗'},
                                  {name: '收率'},
                                  {name: '对淀粉收率'}
        ]}
        @menus_datas.push(item)
      end

      if @smaterial.present? && 'SHY' == @smaterial.code #酸化 添加额外的字段
        item ={name: '综合', data: [{name: '总计'},
                                  {name: '时耗'},
                                  {name: '收率'},
                                  {name: '总收率'}
        ]}
        @menus_datas.push(item)
      end


      #封装查询到的数据
      if @daily_reports.present?
        #封装折线图数据
        one_menus_ids = @d_one_menus.pluck(:id)
        two_menus_datas = DTwoMenu.where(:d_one_menu_id => one_menus_ids)
        get_datas_lines(@daily_reports, two_menus_datas)

        #列表数据
        get_list_data_v2(@daily_reports, @d_one_menus, @smaterial)

        #周盘点数据
        week_times = @daily_reports.pluck(:datetime).uniq.sort

        #头粉 周盘点
        if @smaterial.present? && 'TF' == @smaterial.code #头粉
          @titles= ['开始日期', '结束日期', '上期余量', '本期余量', '本期购进总量', '实际结算量', '本期时耗量']
          @check_data=[]
          s_arr = PostReportsHelper.week_str(week_times)
          s_arr.each do |s|
            if s == [] then #跳过空数组
              next
            end
            nums = s.size - 1
            time1 =s[0][:time]
            time2 =s[nums][:time]
            week_datas = PostReportsHelper.week_js(time1, time2, @smaterial.id)
            #计算上期余量
            item={}
            item[:time1] = time1
            item[:time2] = time2
            item[:s_yl] = '' #上期余量
            item[:b_yl] =week_datas[0].sum.round(2) #本期余量
            item[:b_gj] =week_datas[1].sum.round(2) #本期购进
            item[:b_sh] =week_datas[2].sum.round(2) #本期时耗量
            item[:b_js] = week_datas[3].sum.round(2) #实际结算
            @check_data.push(item)
          end
          if @check_data.present?
            #更新上期余量
            nums = @check_data.length - 1
            nums.times do |i|
              @check_data[i + 1][:s_yl] = @check_data[i][:b_yl]
            end
          end
        end

        #糖化 周盘点
        if @smaterial.present? && 'TY' == @smaterial.code #糖化
          @titles= ['开始日期', '结束日期', '总结余', '时耗淀粉', '产糖（T）', '收率%','淀粉酶单耗(%)','糖化酶单耗(%)']
          @check_data=[]
          s_arr = PostReportsHelper.week_str(week_times)
          Rails.logger.info "-----糖化 周盘点--s_arr-----#{s_arr}------"
          s_arr.each do |s|
            if s == [] then #跳过空数组
              next
            end
            nums = s.size - 1
            time1 =s[0][:time]
            time2 =s[nums][:time]
            week_datas = PostReportsHelper.week_th_js(time1, time2, @smaterial.id)
            item={}
            item[:time1] = time1
            item[:time2] = time2
            item[:zjy] = week_datas[0].sum.round(2) #总结余
            item[:shdy] = week_datas[1].sum.round(2) #时耗淀粉
            item[:ct] = week_datas[2].sum.round(2) #产糖
            if week_datas[1].present? && week_datas[1].sum != 0
              item[:sl] = ((week_datas[2].sum / week_datas[1].sum) * 100).round(2) #收率
              item[:df_dh] = ((week_datas[3].sum / week_datas[1].sum) * 100).round(2) #淀粉酶单耗
              item[:th_dh] = ((week_datas[4].sum / week_datas[1].sum) * 100).round(2) #糖化酶单耗
            else
              item[:sl] = ''
              item[:df_dh] = ''
              item[:th_dh] = ''
            end
            @check_data.push(item)
          end
        end

        #发酵 周盘点
        if @smaterial.present? && 'FJY' == @smaterial.code #发酵
          @titles= ['开始日期', '结束日期', '总结余', '时耗', '产量', '收率%','消沫剂']
          @check_data=[]
          s_arr = PostReportsHelper.week_str(week_times)
          s_arr.each do |s|
            if s == [] then #跳过空数组
              next
            end
            nums = s.size - 1
            time1 =s[0][:time]
            time2 =s[nums][:time]
            week_datas = PostReportsHelper.week_fj_js(time1, time2, @smaterial.id)
            item={}
            item[:time1] = time1
            item[:time2] = time2
            item[:zjy] = week_datas[0].sum.round(2) #总结余
            item[:shdy] = week_datas[1].sum.round(2) #时耗
            item[:ct] = week_datas[2].sum.round(2) #产量
            item[:xmj] = week_datas[3].sum.round(2)  #消沫剂 新增
            if week_datas[1].present? && week_datas[1].sum != 0
              item[:sl] = ((week_datas[2].sum / week_datas[1].sum) * 100).round(2) #收率
            else
              item[:sl] = ''
            end
            @check_data.push(item)
          end
        end

        #酸化 周盘点
        if @smaterial.present? && 'SHY' == @smaterial.code #酸化
          @titles= ['开始日期', '结束日期', '总结余', '时耗', '产量', '收率%']
          @check_data=[]
          s_arr = PostReportsHelper.week_str(week_times)
          s_arr.each do |s|
            if s == [] then #跳过空数组
              next
            end
            nums = s.size - 1
            time1 =s[0][:time]
            time2 =s[nums][:time]
            week_datas = PostReportsHelper.week_sh_js(time1, time2, @smaterial.id)
            item={}
            item[:time1] = time1
            item[:time2] = time2
            item[:zjy] = week_datas[0].sum.round(2) #总结余
            item[:shdy] = week_datas[1].sum.round(2) #时耗
            item[:ct] = week_datas[2].sum.round(2) #产量
            if week_datas[1].present? && week_datas[1].sum != 0
              item[:sl] = ((week_datas[2].sum / week_datas[1].sum) * 100).round(2) #收率
            else
              item[:sl] = ''
            end
            @check_data.push(item)
          end
        end

        #@list_datas.push({code: @smaterial.code,name:@smaterial.name})  #列表数据添加标识
        # ###数据长度
        # @data_length = @list_datas.length
        # ###分页数据
        # @d_ay_zwx_datas = Kaminari.paginate_array(@list_datas).page(@page).per(@per)
        # # #页数
        # @page_num = (@data_length % (@per.to_i)) == 0 ? @data_length/(@per.to_i) : @data_length/(@per.to_i) + 1
      end

    end
  end


  #周日期转换
  def week_date_str(week_data)
    case week_data
      when 0
        '周日'
      when 1
        '周一'
      when 2
        '周二'
      when 3
        '周三'
      when 4
        '周四'
      when 5
        '周五'
      when 6
        '周六'
      else
        ''
    end
  end

  #根据二级菜单获取值
  def get_field_value(daily_reports, field_code)
    @days = daily_reports.pluck(:datetime).uniq.sort #折现图使用
    @field_values=[]
    @days.each do |d|
      s_datas = daily_reports.select {|x| x.datetime == d}
      s_datas = s_datas.select {|x| x.field_code == field_code}
      item={}
      if s_datas.present? && s_datas[0].present?
        field_value = s_datas[0].field_value
        ratio = s_datas[0].ratio
      else
        field_value = nil
        ratio = nil
      end
      item[:field_value] = field_value
      item[:ratio] = ratio
      @field_values.push(item)
    end
  end

  #日期竖向显示 列表数据
  def get_list_data_v2(daily_reports, one_menus_datas, smaterial)
    @days = daily_reports.pluck(:datetime).uniq.sort #折现图使用
    @list_datas=[]
    @days.each do |d|
      item={}
      item[:datetime] = d
      item[:week_time] = week_date_str(Time.parse(d).wday)
      day_datas = daily_reports.select {|x| x.datetime == d} #每天所有项目的数据

      #one_menus = [] #每天单个一级菜单的数据
      one_menus = {} #每天单个一级菜单的数据 封装到一个hash中
      one_menus_datas.each do |one|
        two_item = {}
        two_item[:name] = one.name
        two_datas = day_datas.select {|x| x.d_one_menu_id == one.id}

        if 'TY' == smaterial.code && 'left2' == one.code #糖化 下余物料2 列表数据 去掉 系数
          two_datas = two_datas.select {|x| x.field_code != 'ratio'}
        end
        #------V2.1需求----
        if 'TY' == smaterial.code && 'product' == one.code #糖化 交料产量 列表数据 去掉 糖板框
          two_datas = two_datas.select {|x| x.field_code != 'sugar_plate'}
        end

        if 'FJY' ==smaterial.code && 'left2' == one.code #发酵 下余物料2  补充数据
          field_code1 = [] #已经保存的
          field_code_all = DTwoMenu.where(:d_one_menu_id => one.id).pluck(:code)
          two_datas.each do |two|
            field_code1.push(two.field_code)
          end
          field_code_s = field_code_all - field_code1 #需要存的field_code
          field_code_s.each do |field_code|
            o_d_two = DTwoMenuValue.new({:field_code => field_code})
            two_datas.push(o_d_two)
          end
          two_datas = two_datas.sort_by {|u| u[:field_code]}
        end

        if 'SHY' ==smaterial.code && 'left_fjy' == one.code #酸化 下余发酵液/酸化液  补充数据
          field_code1 = [] #已经保存的
          field_code_all = DTwoMenu.where(:d_one_menu_id => one.id).pluck(:code)
          two_datas.each do |two|
            field_code1.push(two.field_code)
          end
          field_code_s = field_code_all - field_code1 #需要存的field_code
          field_code_s.each do |field_code|
            o_d_two = DTwoMenuValue.new({:field_code => field_code})
            two_datas.push(o_d_two)
          end
          two_datas = two_datas.sort_by {|u| u[:field_code]}
        end
        if 'SHY' ==smaterial.code && 'left_ly' == one.code #酸化 下余滤液  补充数据
          field_code1 = [] #已经保存的
          field_code_all = DTwoMenu.where(:d_one_menu_id => one.id).pluck(:code)
          two_datas.each do |two|
            field_code1.push(two.field_code)
          end
          field_code_s = field_code_all - field_code1 #需要存的field_code
          field_code_s.each do |field_code|
            o_d_two = DTwoMenuValue.new({:field_code => field_code})
            two_datas.push(o_d_two)
          end
          two_datas = two_datas.sort_by {|u| u[:field_code]}
        end

        if 'TY' ==smaterial.code && 'left1' == one.code #糖液 下余物料1 补充数据
          field_code_all = DTwoMenu.where(:d_one_menu_id => one.id).pluck(:code)
          arr =[]
          field_code_all.each do |f|
            datas = two_datas.select {|x| x.field_code == f}
            if datas[0].present?
              arr.push(datas[0])
            else
              o_d_two = DTwoMenuValue.new({:field_code => f, :nums_value => 0})
              arr.push(o_d_two)
            end
          end
          two_datas = arr
        end

        #发酵液 交料产量 返回的数据  = 折产品累加
        # if 'FJY' == smaterial.code && 'product' == one.code
        #   product_total_sum =0
        #   two_datas.each do |t|
        #     product_total_sum += t.fold_value
        #   end
        #   two_datas = product_total_sum.round(2)
        # end

        # V2 合并写法 #发酵 酸化 合并写
        if ['FJY', 'SHY'].include? smaterial.code
          if 'product' == one.code
            product_total_sum =0
            two_datas.each do |t|
              product_total_sum += t.fold_value
            end
            two_datas = product_total_sum.round(2)
          end
        end

        two_item[:data] = two_datas #以对象的形式 返回数据
        one_menus[one.code] = two_item
        #one_menus.push(two_item)
      end

      #添加综合 计算的数据  糖化
      if 'TY' == smaterial.code
        ty_data = PostReportsHelper.th_method_v2(d, smaterial.id)
        Rails.logger.info "-----TY--ty_data----#{ty_data}----"
        if ty_data.present? && ty_data.size >0
          data = [ty_data[0],ty_data[1],ty_data[2],ty_data[3]]  #取前四个元素
        else
          data = []
        end
        one_menus['zh'] = {name: '综合', data: data}
      end
      #添加综合 计算的数据  发酵
      if 'FJY' == smaterial.code
        ty_data = PostReportsHelper.fj_method_v2(d, smaterial.id)
        if ty_data.present?
          data = [ty_data[0],ty_data[1],ty_data[2],ty_data[3],ty_data[4]]  #取前5个元素
        else
          data = []
        end
        one_menus['zh'] = {name: '综合', data: data}
      end
      #添加综合 计算的数据  酸化
      if 'SHY' == smaterial.code
        ty_data = PostReportsHelper.sh_method_v2(d, smaterial.id)
        one_menus['zh'] = {name: '综合', data: ty_data}
      end

      item[:one_menus] = one_menus
      @list_datas.push(item)
    end
  end

  #封装列表数据 横向的（已弃用）
  def get_list_data(daily_reports, two_menus_datas)
    #获取日期数据
    @days = daily_reports.pluck(:datetime).uniq.sort #折现图使用
    @list_datas=[]
    two_menus_datas.each do |t|
      item={}
      item[:name] = t.name
      item[:code] = t.code
      z_data =[] #存所有的值
      @days.each do |d|
        s_datas = daily_reports.select {|x| x.datetime == d}
        s_datas = s_datas.select {|x| x.field_code == t.code}
        hash={}
        if s_datas.present? && s_datas[0].present?
          field_value = s_datas[0].field_value
          ratio = s_datas[0].ratio
        else
          field_value = nil
          ratio = nil
        end
        hash[:field_value] = field_value
        hash[:ratio] = ratio

        z_data.push(hash)
        item[:data] = z_data
      end
      @list_datas.push(item)
    end
  end

  #获取折线图数据
  #返回 @days 日期  @line_datas数据
  def get_datas_lines(daily_reports, two_menus_datas)
    #获取日期数据
    @days = daily_reports.pluck(:datetime).uniq.sort #折现图使用
    @line_datas=[]
    two_menus_datas.each do |t|
      item={}
      item[:name] = t.name
      z_data =[] #存所有的值
      @days.each do |d|
        s_datas = daily_reports.select {|x| x.datetime == d}
        s_datas = s_datas.select {|x| x.field_code == t.code}
        if s_datas.present? && s_datas[0].present?
          field_value = s_datas[0].field_value
        else
          field_value = nil
        end
        z_data.push(field_value)
        item[:data] = z_data
      end
      @line_datas.push(item)
    end
  end

  # GET /users/1/edit
  def edit
  end


  #成功跳转到show页面
  # POST  steam_report
  def create
  end


  #删除后调转到首页  /work_pros
  # def destroy
  #   respond_to do |format|
  #     if @d_daily_report.destroy
  #       format.html {}
  #       format.json {render json: {status: 'success'}}
  #     else
  #       format.json {render json: {status: 'false'}}
  #     end
  #   end
  # end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    @region_code = SRegionCode.find_by(:id => params[:id])
  end

  def work_params
    #params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
