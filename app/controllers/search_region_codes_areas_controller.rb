class SearchRegionCodesAreasController < ApplicationController
  #before_action :authenticate_user

  #根据车间级的部门id，找到车间下的区域
  #  search_region_codes_areas
  def index

  region_code_id = params[:region_id]

  if region_code_id.present?
    area_ids = SRegionCodeArea.where(:s_region_code_id => region_code_id).pluck(:s_area_id)
    @areas = SArea.select(:id,:code,:name).where(id: area_ids)
  end
  
   render json: {unit_region: @areas}
  end

end