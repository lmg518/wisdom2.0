class EquipmentInfosController < ApplicationController
  before_action :get_equipment_info, only: [:update, :show, :edit, :destroy]
  before_action :authenticate_user
  #设备信息管理
  def index
    @equipment_infos=SEquipmentInfo.by_equip_brands(params[:equip_brands])
                                  .by_equip_nums(params[:equip_nums])
                                  .by_equip_type(params[:equip_type])
                                  .order(:created_at => :desc)
                                  .page(params[:page]).per(params[:per])

  end

  def show
    # render json:{supply: {:unit_name =>@supply.unit_name,:s_region_code_info_id =>@supply.s_region_code_info_id,
    #                       :city_name =>@supply.city_name,:s_administrative_area_id =>@supply.s_administrative_area_id,
    #                       :brand =>@supply.brand,:supply_name =>@supply.supply_name,:supply_no =>@supply.supply_no,
    #                       :supply_num =>@supply.supply_num,:supply_unit =>@supply.supply_unit,
    #                       :validity_date =>@supply.validity_date.strftime("%Y-%m-%d"),:owner =>@supply.owner,
    #                       :d_login_msg_id =>@supply.d_login_msg_id },
    #              brands: SBrandMsg.pluck(:brand), provinces: SAdministrativeArea.all_provinces}
  end

  def update
    respond_to do |format|
      if @equipment_info.update(params_to_equipment_info)
        format.html {}
        format.json {render json: {status: "success", location: @equipment_info}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @equipment_info.errors}}
      end
    end
  end

  def create
    @equipment_info = SEquipmentInfo.new(params_to_equipment_info)
    @equipment_info.add_man = @current_user.login_name   #添加人
    respond_to do |format|
      if @equipment_info.save
        format.html {}
        format.json {render json: {status: "success", location: @equipment_info}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @equipment_info.errors}}
      end
    end
  end

  def destroy
    respond_to do |format|
      if @equipment_info.destroy
        format.html {}
        format.json {render json: {status: "success"}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @equipment_info.errors}}
      end
    end
  end

  private
  def get_equipment_info
    @equipment_info = SEquipmentInfo.find(params[:id])
  end

  def params_to_equipment_info
    params.require(:equipment_info).permit(:equip_type, :equip_brands, :equip_code,
                                            :equip_nums,:types,:d_station_id,
                                            :purchase_time)
                                         
  end

end