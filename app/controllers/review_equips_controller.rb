class ReviewEquipsController < ApplicationController
  before_action :get_review_equip, only: [:show, :update]

  def index
    review_equips = HandleEquip.where(:status => 'wait_review')
                        .order(:created_at => 'desc')
    @review_equips = review_equips.page(params[:page]).per(params[:per])
  end

  def show
    #填写检修记录，更换备件什么都在show页面，看有没有检修前、后记录和更换备件记录
    @h_overhaul_notes = @handle_equip.h_overhaul_notes
    @spare = @handle_equip.h_equip_spare
    #试车检查项
    get_boot
  end

  def update
    if @review_equip.status == 'wait_review'
      if params[:status] == 'carry_out'
        @review_equip.update(:status => params[:status])
      elsif params[:status] == 'not_carry'
        @review_equip.update(:status => 'wait_carried')
        h_overhaul_notes = @review_equip.h_overhaul_notes
        change_spares = @review_equip.h_equip_spare
        h_equip_boots = @review_equip.h_equip_boots
        if h_overhaul_notes.present?
          delete_img_note(h_overhaul_notes)
        end
        if change_spares.present?
          change_spares.destroy
        end
        if h_equip_boots.present?
          h_equip_boots.destroy_all
        end
      end
    end
    render json: {status: 200}
  end

  private
  def get_review_equip
    @review_equip = HandleEquip.find(params[:id])
  end

  #对上传的图片和记录进行删除
  def delete_img_note(arg)
      arg.each do |a|
        if a.img_path.present?
          File::unlink("#{Rails.root}/public/#{a.img_path}")
        end
        a.destroy
      end
  end

end