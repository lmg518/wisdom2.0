class StationValidityDataController < ApplicationController
  before_action :current_user

  def index
    if params[:search].present?
      @search = params[:search]
      @day_time = Time.parse(@search[:day_time]) if @search[:day_time].present?
      @station_ids = @search[:station_ids] if @search[:station_ids].present?
      @vain_num = @search[:vain_num] if @search[:vain_num].present?
    end
    @day_valids =  DayValidDatum.get_by_station_id(@station_ids)
                                .get_time(@day_time)
    if @vain_num.present?
      vain_num = @vain_num.to_i
      case vain_num
        when 3
          @day_valids = @day_valids.select{|x| x.so2_vain_count >= vain_num || x.no2_vain_count >= vain_num || x.o3_vain_count >= vain_num || x.co_vain_count >= vain_num || x.pm2_5_vain_count >= vain_num || x.pm10_vain_count >= vain_num }
        else
          @day_valids = @day_valids.select{|x| x.so2_vain_count == vain_num || x.no2_vain_count == vain_num || x.o3_vain_count == vain_num || x.co_vain_count == vain_num || x.pm2_5_vain_count == vain_num || x.pm10_vain_count == vain_num }
      end
    end
  end

  # id ，itemb_name
  def show
    day_valid =  DayValidDatum.find_by_id(params[:id])
    return if day_valid.blank?
    day_time = day_valid.day_time
    return if params[:item_name].blank?
    # if item_id(params[:item_name]) == '107' || item_id(params[:item_name]) == '108'
    #   @item_id = ['107','108']
    # else
    #   @item_id = params[:item_name]
    # end
    @item_name = params[:item_name]
    day_item_datas = WcDDataDayHandle.where(:STATION_ID => "#{day_valid.d_station_id}A",
                                            :ITEM_CODE => item_id(@item_name),
                                            :DATA_TIME => ('2017-07-19 01')..('2017-07-20 00'))
    # day_item_datas = WcDDataDayHandle.where(:STATION_ID => "#{day_valid.d_station_id}A",
    #                                         :ITEM_CODE => item_id(@item_name),
    #                                         :DATA_TIME => ((day_time.beginning_of_day + 1.hours).strftime("%Y-%m-%d %H"))..((day_time + 1.days).beginning_of_day).strftime("%Y-%m-%d %H"))

    render json: {    day_time: day_valid.day_time.strftime("%Y-%m-%d"),
                      station_name: day_valid.station_name,
                      item_name: @item_name.upcase,
                      item_day_data: day_item_datas.select("RECEIVE_ACCEPT",'DATA_TIME', 'DATA_VALUE', 'OLD_DATA_VALUE','EXT')
                  }
  end

  def item_id(item_name)
    item_index = %w(SO2 NO2 CO O3 PM2.5 PM10).index(item_name.upcase)
    item_id = %W(101 102 105 106 107 108)[item_index]
  end

end
