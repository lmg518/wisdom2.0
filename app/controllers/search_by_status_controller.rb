class SearchByStatusController < ApplicationController

  def index
    if params[:despatches].present?
      @alarm_status =  DAlarmDespatch.get_all_status
    else
      @alarm_status =  DAlarm.get_all_status
    end
    render json: {alarm_status: @alarm_status}
  end

end