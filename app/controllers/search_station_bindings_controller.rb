class SearchStationBindingsController < ApplicationController
  before_action :current_user
  # get  /search_station_bindings

  def index
    area_arr = []
    if params[:region_level].present?
      q_region_codes = SRegionCode.by_region_level(params[:region_level])
      q_region_codes.each do |region|
        area_arr <<  {region_chil:{id:region.id,region_name: region.region_name},
                      d_stations: d_station_josn(region) }
      end
    else
      q_region_codes = SRegionCode.by_father_region
      q_region_codes.each do |region|
        area_arr << region.get_son_region
      end
      region_first = q_region_codes.first
      if region_first.present?
        first_data = {region_chil: region_first.attributes.slice("id","region_name") ,
                      d_stations: region_first.d_stations.map{|x| {id: x.id, station_name: x.station_name} } }
        area_arr.first["chil"].unshift(first_data)
      end
    end

    render json:{area_stations: area_arr}
  end

  def station_arr(area)
    @city_arr = []
    area.children.each do |chi|
      @chi_arr = []
      if chi.d_stations.present?
        chi.d_stations.each do |se|
          @chi_arr << {id: se.id, station_name: se.station_name.split("-")[1] }
        end
      end
      @city_arr << {city: chi.attributes.slice('id','zone_name'),d_station: @chi_arr}
    end
    @city_arr
  end

  private
  def d_station_josn(arr)
    arr.d_stations.map{|x| {id: x.id, station_name: x.station_name} } if arr.d_stations.present?
  end

end