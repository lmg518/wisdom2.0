class FormModuleController < ApplicationController
  before_action :current_user
  before_action :get_form_module, only: [:show,:update,:destroy]
    #添加表格模板

    #首页信息
    def table_infos
        @funcs = FFormModule.all.order(:created_at => :desc)
        render :partial => 'table_infos', :layout => false
    end

    #新建模板
    def new_form_module
        render :partial => "new_form_module", :layout => false
    end

    #编辑页面 显示所有的字段
    def show
      @field_titles=FFieldTitle.all    #所有的字段

      @field_titlle_ids=@form_module.field_title_id

      if @field_titlle_ids.present?
        @title_ids=@field_titlle_ids.split(",") 
        @field_title_checked=FFieldTitle.where(:id => @title_ids)   #已经分配的字段
      end

      render :partial => "show", :layout => false
    end

    #保存模板
    def create
        retCode = true
        @form_module = FFormModule.new(form_module_params)
        respond_to do |format|

        # 写日志
        wLoginOpr = WLoginOpr.new
        wLoginOpr.op_code   = "form_module|create"
        wLoginOpr.op_time   = Time.now.to_s
        wLoginOpr.login_no  = current_user.login_no
        wLoginOpr.ip_addr   = current_user.ip_address
        wLoginOpr.op_note   = "创建人：" + current_user.login_name
        wLoginOpr.op_describe = "新增模板，模板名：" + @form_module.name

        @form_module.transaction do
          retCode = @form_module.save
          retCode = wLoginOpr.save if retCode
        end

        #@form_module.save
        if retCode
          format.json { render json: {status: :ok, location: @form_module} }
        else
          format.json { render json: @form_module.errors, status: :unprocessable_entity }
        end
      end
    end

    #编辑页面 为模板添加字段id
    def update
      retCode = true

      respond_to do |format|
        field_title_ids = params[:field_title_id]
        title_id='';
        if field_title_ids.length >0
          field_title_ids.each do |id|
            title_id << id + ','
          end
        end
        
        @form_module.field_title_id =title_id

        # 写日志
        wLoginOpr = WLoginOpr.new
        wLoginOpr.op_code   = "form_module|update"
        wLoginOpr.op_time   = Time.now.to_s
        wLoginOpr.login_no  = current_user.login_no
        wLoginOpr.ip_addr   = current_user.ip_address
        wLoginOpr.op_note   = "编辑模板字段，模板id:" + @form_module.id.to_s
        wLoginOpr.op_describe = "模板字段修改为：" + title_id

        @form_module.transaction do
          retCode = @form_module.save
          retCode = wLoginOpr.save if retCode
        end

        #@form_module.save
        if retCode
          format.json { render json: {status: :ok, location: @form_module} }
        else
          format.json { render json: @form_module.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
        retCode = true
        respond_to do |format|

        # 写日志
        wLoginOpr = WLoginOpr.new
        wLoginOpr.op_code   = "form_module|destroy"
        wLoginOpr.op_time   = Time.now.to_s
        wLoginOpr.login_no  = current_user.login_no
        wLoginOpr.ip_addr   = current_user.ip_address
        wLoginOpr.op_note   = "删除人：" + current_user.login_name
        wLoginOpr.op_describe = "删除模板，模板名：" + @form_module.name

        @form_module.transaction do
          retCode = @form_module.destroy
          retCode = wLoginOpr.save if retCode
        end
        #@form_module.destroy
        if retCode
          format.json { render json:{ status: :ok } }
        else
          format.json { render json: @form_module.errors, status: :unprocessable_entity }
        end
      end
    end



    private
    def get_form_module
      @form_module = FFormModule.find(params[:id])
    end
  
    def form_module_params
      params.require(:f_form_module).permit(:name, :module_type, :d_work_pro_id)
    end
end