class AccsController < ApplicationController
  before_action :get_supply, only: [:update, :show, :edit, :destroy]

  def index
    @accs = DAcc.s_by_name(params[:sypply_name])
                .s_by_no(params[:supply_no])
                .s_by_brand(params[:brand])
                .s_by_unit_ids(params[:unit_ids])
    @accs = @accs.page(params[:page]).per(params[:per])
  end

  def show
    render json: {supply: {:id => @acc.id, :unit_name =>@acc.unit_name, :s_region_code_info_id =>@acc.s_region_code_info_id,
                            :city_name =>@acc.city_name, :s_administrative_area_id =>@acc.s_administrative_area_id,
                            :brand =>@acc.brand, :supply_name =>@acc.supply_name, :supply_no =>@acc.supply_no,
                            :supply_num =>@acc.supply_num, :supply_unit =>@acc.supply_unit, :validity_date =>@acc.validity_date.strftime("%Y-%m-%d"),
                            :owner =>@acc.owner, :d_login_msg_id =>@acc.d_login_msg_id},
                  brands: SBrandMsg.pluck(:brand), provinces: SAdministrativeArea.all_provinces}
  end

  def update
    respond_to do |format|
      if @acc.update(params_to_supply)
        format.html {}
        format.json {render json: {status: "success", location: @acc}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @acc.errors}}
      end
    end
  end

  def create
    @acc = DAcc.new(params_to_supply)
    respond_to do |format|
      if @acc.save
        format.html {}
        format.json {render json: {status: "success", location: @acc}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @acc.errors}}
      end
    end
  end

  def destroy
    respond_to do |format|
      if @acc.destroy
        format.html {}
        format.json {render json: {status: "success"}}
      else
        format.html {}
        format.json {render json: {status: "false", location: @acc.errors}}
      end
    end
  end

  private
  def get_supply
    @acc = DAcc.find(params[:id])
  end

  def params_to_supply
    params.require(:acc).permit(:unit_name, :s_region_code_info_id, :brand, :city_name,
                                :s_administrative_area_id,
                                :supply_name, :supply_no, :supply_num,
                                :supply_unit, :validity_date, :owner,
                                :d_login_msg_id)
  end

end