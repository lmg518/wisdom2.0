class StationDataOffController < ApplicationController
  before_action :current_user

  def index
    search_parms
    handle_table
  end

  def export_file
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["日期", "区域名称", "站点名称", "离线时长", "离线次数"]
      @index.each do |info|
        csv << [info[:data], info[:region_name], info[:station_name], info[:off_time], info[:off_num]]
      end
    end
    if @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "站点离线时长统计"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "站点离线时长统计"
    end
    respond_to do |format|
      format.csv {
        send_data @scv,
                  :type => 'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private

  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
  end

  def search_parms
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @station_ids = @search[:station_ids]
      @data_times = @search[:data_times]
      @despatch_status = @search[:status]
    end
  end

  def handle_table
    @index = []
    @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
    @abnormals = DAbnormalDataYyyymm.by_station_off(@station_ids).by_get_time(@data_times)
    unless  @station_ids.present?
    @station_ids = DStation.all.pluck(:id)
    end
    @station_ids.each do |station_id|
      create_acce = @abnormals.select {|x| x.station_id == station_id}.map {|x| x.create_acce.strftime("%Y-%m-%d %H:%M:%S")}
      next if create_acce.blank?
      time_min = Time.parse(create_acce.min)
      time_max = Time.parse(create_acce.max)
      station = DStation.find(station_id)
      region_code = station.s_region_codes.first
      @index <<  {data: time_min.strftime("%Y-%m-%d"),
                  region_name: region_code.present? ?  region_code.region_name : '',
                  station_name: station.station_name,
                  off_time: ((time_max - time_min).to_f / 3600.to_f).round(2),
                  off_num: create_acce.length,
      }
      # create_acce = @abnormals.select {|x| x.station_id == 1090}.map {|x| x.create_acce.strftime("%Y-%m-%d %H:%M:%S")}
      # return @index if create_acce.blank?
      # time_min = Time.parse(create_acce.min)
      # time_max = Time.parse(create_acce.max)
      # station = DStation.find(1090)
      # region_code = station.s_region_codes.first
      # @index <<  {data: time_min.strftime("%Y-%m-%d"),
      #    region_name: region_code.region_name,
      #    station_name: station.station_name,
      #    off_time: ((time_max - time_min).to_f / 3600.to_f).round(2),
      #    off_num: create_acce.length,
      #   }
    end
    @index
  end

end
