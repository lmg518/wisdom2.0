class CompanyReportsController < ApplicationController
  before_action :authenticate_user
  
  #公司生产  日报表

  def index
    #@daily_reports=DDailyReport.all

    #获取查询参数
    begin_day = params[:begin_day]
    end_day = params[:end_day]
    params_region_ids = params[:region_id]


    #根据登录人员 获取人员所在的车间 或公司
    @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id)  #获取人员所在车间 或公司

    #当天的 日报表
    time_begion=Time.now.beginning_of_month.strftime("%Y-%m-%d")
    time = Time.now.strftime("%Y-%m-%d")


    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1   #2 分公司   1 总公司   3 车间
      if params_region_ids.present?   #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id)  #查询分公司下的的所有车间
      end
      elsif @region_code1.s_region_level_id == 2
        if params_region_ids.present?   #使用查询的ids
          region_code_ids = params_region_ids
        else
          region_f = SRegionCode.where(:father_region_id => @region_code1.id)
          region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
        end
      elsif @region_code1.s_region_level_id == 3
        if params_region_ids.present?   #使用查询的ids
          region_code_ids = params_region_ids
        else
          @region_code = @region_code1
          region_code_ids = @region_code.id
        end
      end
  

    Rails.logger.info "-region_code_ids----#{region_code_ids}-------"


    @daily_reports = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                                  .by_datetime(begin_day,end_day)  #按日期查询
                                  .order(:s_region_code_id => :desc, :created_at => :desc)
                                  .page(params[:page]).per(params[:per])





    #折现图数据 不要分页                              
    @daily_reports1 = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                                  .by_datetime(begin_day,end_day)  #按日期查询
                                  .order(:created_at => :desc)
    @sum_dates = DDailyReport.by_datetime(time_begion, time).where(:s_region_code_id =>region_code_ids, :status => 'Y')

    #车间项目 信息
    @ids = DMaterialReginCode.where(:s_region_code_id => region_code_ids)
                              .pluck(:s_material_id)

    @plants = SMaterial.where(:id => @ids).page(params[:page]).per(params[:per])   #从材料表中获取
    #获取当前月份
    @months=[]
    @months = @daily_reports1.pluck(:datetime).uniq.sort  #折现图使用
    #查询当前车间  各个项目的 月数据
    #根据登录人员 获取人员所在的车间 或公司
    @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    #车间项目 信息
    @month_days=[]
    #获取最小查询日期
      @plants.each do |p|
        item={}
        item["name"] = p.name
        z_data=[]  #折现数据
        @months.each do |m|
          num= DDailyReport.find_by(:datetime => m, :s_region_code_id => region_code_ids, :name => p.name)
          if num.present?
            nums = num.nums
          else
            nums = nil
          end
          z_data.push(nums)   #不管当天有没有数据都填充一条记录
        end
        item["data"] = z_data
        @month_days.push(item)
      end
      Rails.logger.info "-----11--@month_days---#{@month_days}---"
      Rails.logger.info "-----11--@months---#{@months}---"



  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    @work=DWorkPro.new
  end

  #成功跳转到show页面
  def create
    @work = DWorkPro.new(work_params)
    respond_to do |format|
      if @work.save
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Successfully create!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :new}
      end
    end
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @work.destroy
        format.html {redirect_to work_pros_path, notice: 'Successfully destroy!'}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @work.update(work_params)
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    @work = DWorkPro.find(params[:id])
  end

  def work_params
    params.require(:d_work_pro).permit(:id, :work_name, :work_code, :work_type, :work_flag)
  end

end
