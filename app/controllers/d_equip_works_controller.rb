class DEquipWorksController < ApplicationController
  before_action :get_work, only: [:edit, :show, :update, :issued, :carried_out, :destroy]
  before_action :authenticate_user
  # skip_before_filter :verify_authenticity_token  #跳过安全令牌验证
  
  #设备点检管理
  def index

    #车间级的人员只看自己车间的设备信息
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    if @region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      region_z = SRegionCode.where(:s_region_level_id => 3)
      region_code_ids = region_z.pluck(:id)  #查询所有车间
    end

    if @region_code.s_region_level_id == 2  #2 分公司   1 总公司   3 车间
      region_f = SRegionCode.where(:father_region_id => @region_code.id, :s_region_level_id => 3)
      region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
    end

    if @region_code.s_region_level_id == 3  #2 分公司   1 总公司   3 车间
      region_code_ids = @region_code.id
    end

    get_region
    end_time = params[:end_time]
    if end_time.present?
      time_end = Time.parse(end_time).end_of_day
    end

    #机修人员只看自己的工单
    if @current_user.s_role_msg.role_code == 'mechanic_repair'
      equip_works = DEquipWork.where(:s_region_code_id => region_code_ids,:work_person => @current_user.login_name).by_region_id(params[:equip_ids])
                              .by_work_time(params[:begin_time], time_end)
                              .by_work_status(params[:status])
                              .order(:created_at => 'desc', :s_area_id => 'desc')
      @equip_works = equip_works.page(params[:page]).per(params[:per])
    else
      equip_works = DEquipWork.where(:s_region_code_id => region_code_ids).by_region_id(params[:equip_ids])
                              .by_work_time(params[:begin_time], time_end)
                              .by_work_status(params[:status])
                              .order(:created_at => 'desc', :s_area_id => 'desc')
      @equip_works = equip_works.page(params[:page]).per(params[:per])
    end
  end

  def new
    work = DEquipWork.new()
  end

  #批量新增点检记录
  # d_equip_works/batch_create.json
  def batch_create
    d_equip_work = params["d_equip_work"]
    s_area_ids = d_equip_work["s_area_ids"]
    times = d_equip_work["time"]
    boot_ids = params["boot_ids"]
    times.each do |t|
      # Rails.logger.info "------#{t[1]["work_start_time"]}---"
      # Rails.logger.info "------#{t[1]["work_end_time"]}---"
      s_area_ids.each do |s_area_id|
        equip_work = DEquipWork.new
        #查询区域下的机修人员
        equip_work.work_status = 'wait_issued'
        equip_work.founder = @current_user.login_name

        equip_work.s_region_code_id = d_equip_work["s_region_code_id"]
        equip_work.region_name = d_equip_work["region_name"]
        equip_work.s_region_code_info_id = d_equip_work["s_region_code_info_id"]
        equip_work.s_area_id = s_area_id
        equip_work.work_start_time = t[1]["work_start_time"]
        equip_work.work_end_time = t[1]["work_end_time"]
        equip_work.work_desc = d_equip_work["work_desc"]
        equip_work.save

        if equip_work.save
          boot_ids.each do |b|
            WorkBoot.create!(:d_equip_work_id => equip_work.id, :boot_check_id => b.to_i,
                             :check_project => BootCheck.find(b).check_project)
          end
          WorkNote.create!(:d_equip_work_id => equip_work.id, :status => 'created',
                           :login_name => @current_user.login_name)
        end
      end
    end

    #equip_work = DEquipWork.new
    # :s_region_code_id, :s_area_id, :region_name, :s_equip_id, :equip_name, 
    #  :equip_code, :work_start_time, :work_end_time, :work_desc,:s_region_code_info_id)

    render json: {status: 200}
  end

  def create
    equip_work = DEquipWork.new(get_work_params)
    equip_work.work_status = 'wait_issued'
    equip_work.founder = @current_user.login_name
    boot_ids = params[:boot_ids]
    if equip_work.save
      boot_ids.each do |b|
        WorkBoot.create!(:d_equip_work_id => equip_work.id, :boot_check_id => b.to_i,
                         :check_project => BootCheck.find(b).check_project)
      end
      WorkNote.create!(:d_equip_work_id => equip_work.id, :status => 'created',
                       :login_name => @current_user.login_name)
      render json: {status: 200}
    end
  end

  def show
    boot_s = @equip_work.work_boots

    s_area = SArea.find_by(:id => @equip_work.s_area_id)  #查询区域
    area_name = s_area.present? ? s_area.name : ''

    @boots = []
    boot_s.each {|x| @boots << {id: x.id, boot_check_id: x.boot_check_id, check_project: x.check_project, work_result: DEquipWork.set_work_result(x.work_result), abnor_desc: x.abnor_desc, }}
    note_s = @equip_work.work_notes
    @notes = []
    note_s.each {|x| @notes << {id: x.id, login_name: x.login_name, status: WorkNote.set_status(x.status), note_time: x.created_at.strftime("%Y-%m-%d %H:%M:%S")}}
    render json: {equip_work: {id: @equip_work.id,area_name: area_name, region_name: @equip_work.region_name, equip_name: @equip_work.equip_name,
                               equip_code: @equip_work.equip_code, work_person: @equip_work.work_person, work_start_time: @equip_work.work_start_time ? @equip_work.work_start_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                               work_end_time: @equip_work.work_end_time ? @equip_work.work_end_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                               work_status: DEquipWork.set_work_status(@equip_work.work_status), work_desc: @equip_work.work_desc,
                               handle_time: @equip_work.handle_time ? @equip_work.handle_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                               founder: @equip_work.founder},
                   boots: @boots, 
                   notes: @notes,
                   img_path: @equip_work.img_path.present? ? @equip_work.img_path : ''
                  }
  end

  def edit
    if @equip_work.work_status == 'wait_issued'
      #@equip_work
      @boots = @equip_work.work_boots
    else
      render json: {status: '已经下发，不可修改'}
    end
  end

  def update
    if @equip_work.work_status == 'wait_issued'
      if @equip_work.update(get_work_params)
        if params[:boot_ids]
          boots = @equip_work.work_boots
          if boots.destroy_all
            params[:boot_ids].each do |b|
              WorkBoot.create!(:d_equip_work_id => @equip_work.id, :boot_check_id => b.to_i,
                               :check_project => BootCheck.find(b).check_project)
            end
            rende json: {status: 200}
          end
        end
      end
    end
  end


  #下发  post /d_equip_works/issuedcd t
  def issued
    if @equip_work.work_status == 'wait_issued'
      @equip_work.update(:work_person => params[:work_person], :work_status => 'wait_carried')
      WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'issued', :login_name => @current_user.login_name)

      #极光推送 提醒机修人员
      content = '你有新的点检记录要处理，请查看'
      delay_time = ((@equip_work.work_start_time - Time.now) / 60).to_i
      d_login_msg = DLoginMsg.find_by(:login_name => params[:work_person])
      if d_login_msg.present? && d_login_msg.uuid.present?
        #SidekiqJpushSingleWorker.perform_async(content,d_login_msg.uuid,'DJ')  #立即推送
        SidekiqJpushSingleWorker.perform_in(delay_time.minutes, content,d_login_msg.uuid,'DJ')  #延迟推送
      end

      render json: {status: 200}
    end
  end

  #检查项提交
  #post /d_equip_works/boot_review
  # def boot_review
  #   params[:boot_hash].each do |b|
  #     boot = WorkBoot.find_by(:id => b['id'])
  #     if boot.update(:work_result =>b['work_result'], :abnor_desc =>b['abnor_desc'])
  #       note = WorkNote.find_by(:d_equip_work_id => @equip_work.id, :status => 'deal_with')
  #       if note
  #         note.update(:login_name => @current_user.login_name)
  #       else
  #         WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'deal_with', :login_name => @current_user.login_name)
  #       end
  #     end
  #   end
  #   render json: {status: 200}
  # end

  #处理 post /d_equip_works/carried_out
  def carried_out
    if @equip_work.work_status == 'wait_carried'
      params[:boot_hash].each do |b|
        boot = WorkBoot.find_by(:id => b[1]['boot_id'].to_i)
        if boot.update(:work_result =>b[1]['work_result'], :abnor_desc =>b[1]['abnor_desc'])
          note = WorkNote.find_by(:d_equip_work_id => @equip_work.id, :status => 'deal_with')
          if note
            note.update(:login_name => @current_user.login_name)
          else
            WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'deal_with', :login_name => @current_user.login_name)
          end
        end
      end

      #添加上传图片
      image_file = params[:image_file]
      if image_file.present?
        #获取到上传的图片路径
        img_path = upload_img(image_file, @equip_work)
        @equip_work.update(:work_status => 'carry_out', :handle_time => Time.now, :img_path => img_path)
      end

      WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'carry_out', :login_name => @current_user.login_name)
      render json: {status: 200}
    end
  end

   #上传图片
  def upload_img(image_file, equip)
    if image_file.present?
      time = Time.now
      img_flag = "jpeg" if image_file[0, 15] == "data:image/jpeg"
      img_flag = "png" if image_file[0, 14] == "data:image/png"
      img_file = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
      img_name = "#{equip.id}.#{img_flag}"
      file_path = "#{Rails.root}/public/test/d_equip_works/#{time.year}/#{time.month}"
      FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
      #向dir目录写入文件
      File.open(Rails.root.join("#{file_path}", img_name), 'wb') {|f| f.write(img_file)}
      return "test/d_equip_works/#{time.year}/#{time.month}/#{img_name}"
    end
  end

  # DELETE /d_task_forms/1
  # DELETE /d_task_forms/1.json
  def destroy
    if @equip_work.destroy
      render json: {status: 200}
    else
      render json: {status: 'error'}
    end
    # @equip_work.destroy
    # respond_to do |format|
    #   format.html {}
    #   format.json { head :no_content }
    # end
  end

  
  #获取检查项
  #get /d_equip_works/get_boots
  def get_boots
    @boots = BootCheck.select(:id, :check_project)
    render json: {boots: @boots}
  end

  #新增页面 获取车间部门 信息
  #get /d_equip_works/get_equip_regions
  def get_equip_regions
    #只查询设备部门的车间单位
    #@region_codes = SRegionCodeInfo.where(:status => 'Y',:unit_code => ['equip_hc','equip_fj','equip_jz','equip_hb']).map{|x| {id: x.id, unit_code: x.unit_code, unit_name: x.unit_name}}
    #render json: {region_codes: @region_codes}
  end

  #获取人员
  #get /d_equip_works/get_login
  def get_login
    #region_id = params[:region_id]
    area_id = params[:area_id]
    group_id = params[:region_id]
    @logins = DLoginMsg.select(:id, :login_name).where(:s_area_id =>area_id, :group_id => group_id)
    render json: {login: @logins}
  end


  private
  def get_work
    @equip_work = DEquipWork.find(params[:id])
  end

  def get_work_params
    params.require(:d_equip_work).permit(:s_region_code_id, :s_area_id, :region_name, :s_equip_id, :equip_name, :equip_code, :work_start_time, :work_end_time, :work_desc,:s_region_code_info_id)
  end

  #查询接口----获取车间
  def get_region
    # puts "group_id: #{@current_user.group_id}"
    # region_code = SRegionCode.find_by(:id => @current_user.group_id)
    # father_region = SRegionCode.find_by(:father_region_id == 99999)
    # if region_code.father_region_id == 99999
    #   @region_code = SRegionCode.select(:id, :region_name).where("father_region_id != 99999", :s_region_level_id =>3)
    # elsif region_code.father_region_id == father_region.id
    #   @region_code = SRegionCode.select(:id, :region_name).where(:father_region_id => region_code.id, :s_region_level_id =>3)
    # else
    #   @region_code = SRegionCode.select(:id, :region_name).where(:father_region_id => region_code.father_region_id, :s_region_level_id =>3)
    # end

    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    if region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      @region_code = SRegionCode.select(:id, :region_name).where(:s_region_level_id =>3)
    elsif region_code.s_region_level_id == 2
      @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id =>region_code.id, :s_region_level_id =>3)
    elsif region_code.s_region_level_id == 3
      @region_code = SRegionCode.select(:id,:region_name).where(:id =>region_code.id, :s_region_level_id =>3)
    else
      @region_code = ''
    end



  end

end