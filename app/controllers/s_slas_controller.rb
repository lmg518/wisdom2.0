class SSlasController < ApplicationController
  before_action :get_slas , only: [:edit, :update,:show]

  def index
    @slas = SSla.page(params[:page]).per(params[:per])
  end

  def show
    return render json:{sla: @sla}
  end

  def edit
  end

  def update
    respond_to do |format|
      if @sla.update(slas_parms)
        format.html {}
        format.json { render json:{status: :ok, location: @sla}  }
      else
        format.html {}
        format.json { render json: @sla.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @sla = SSla.new(slas_parms)
    respond_to do |format|
      if @sla.save!
        format.html {}
        format.json { render json:{status: :ok, location: @sla}  }
      else
        format.html {}
        format.json { render json: @sla.errors, status: :unprocessable_entity }
      end
    end

  end

  private
  def get_slas
    @sla = SSla.find(params[:id])
  end

  def slas_parms
    params.require(:s_sla).permit(:alarm_level , :receive_minutes, :arrive_minutes, :recovery_minutes, :deal_minutes)
  end

end