class SearchMapCityController < ApplicationController
  before_action :current_user

  def index
    @zone_arr = []
    @alarm_zone_arr = []
    @alarm_level_1 = []
    @alarm_level_2 = []
    @alarm_level_3 = []
    @station_counts = 0
    @alarm_arr = []
    @stations = @current_user.d_stations.where(:s_administrative_area_id => SAdministrativeArea.find_by_zone_name(params[:city]))
    @stations.each do |station|
      alarm_arr =  station.d_alarms
      area_zone =  station.s_administrative_area
      @station_counts += 1  if alarm_arr.present?
      if area_zone.parent.present?
        zone_parent = area_zone.parent
        @zone_np = zone_parent.zone_name
        @area_zone = area_zone.zone_name
        @zone_arr << @zone_np
        if alarm_arr.present?
          alarm_level_arr =  alarm_arr.map{|x|x.alarm_level.to_i}
          alarm_arr.each do |alarm|
            @alarm_arr <<  "#{station.station_name} - #{alarm.alarm_rule}(#{ alarm.alarm_level_to_s})"
          end
          if  alarm_level_arr.include?(3)
            @alarm_level_3 << @area_zone
            next
          end
          if  alarm_level_arr.include?(2)
            @alarm_level_2 << @area_zone
            next
          end
          if  alarm_level_arr.include?(1)
            @alarm_level_1 << @area_zone
            # {alarm_level: alarm_arr.first.alarm_level, id: zone_parent.id,province_name: @zone_np}
            next
          end

        end
      end
    end
    @zone_arr.uniq!
    @all_infos = {alarm_level: @alarm_level_3.present? ? 3 : @alarm_level_2.present? ? 2 : @alarm_level_1.present? ?  1 : 0,
                  alarm_province: @area_zone,station_counts: @station_counts}

    render json: {all_infos: @all_infos,alarm_level_infos: @alarm_arr}

  end

end