class DEquipWorkRulesController < ApplicationController
  before_action :get_work, only: [:edit, :show, :update, :issued, :carried_out, :destroy]
  before_action :authenticate_user
  # skip_before_filter :verify_authenticity_token  #跳过安全令牌验证
  
  #设备点检规则
  def index

    #车间级的人员只看自己车间的设备信息
    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    if @region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      region_z = SRegionCode.where(:s_region_level_id => 3)
      region_code_ids = region_z.pluck(:id)  #查询所有车间
    end

    if @region_code.s_region_level_id == 2  #2 分公司   1 总公司   3 车间
      region_f = SRegionCode.where(:father_region_id => @region_code.id, :s_region_level_id => 3)
      region_code_ids = region_f.pluck(:id)  #查询分公司下的的所有车间
    end

    if @region_code.s_region_level_id == 3  #2 分公司   1 总公司   3 车间
      region_code_ids = @region_code.id
    end

    get_region
    end_time = params[:end_time]

    #机修人员只看自己的工单
    if @current_user.s_role_msg.role_code == 'mechanic_repair'
      equip_works = DEquipWorkRule.where(:s_region_code_id => region_code_ids,:work_person => @current_user.login_name).by_region_id(params[:equip_ids])
                              .by_work_time(params[:begin_time], end_time)
                              .order(:created_at => 'desc')
      @equip_works = equip_works.page(params[:page]).per(params[:per])
    else
      equip_works = DEquipWorkRule.where(:s_region_code_id => region_code_ids).by_region_id(params[:equip_ids])
                              .by_work_time(params[:begin_time], end_time)
                              .order(:created_at => 'desc')
      @equip_works = equip_works.page(params[:page]).per(params[:per])
    end
  end
  

  def new
    work = DEquipWorkRule.new()
  end

  def create
    equip_work = DEquipWorkRule.new(get_work_params)
    equip_work.fourder = @current_user.login_name

    work_name = DLoginMsg.find_by(:id => equip_work.d_login_msg_id )
    work_person = work_name.present? ? work_name.login_name : ''
    equip_work.work_person = work_person  #执行人

    ids = params[:boot_ids]
    boot_ids = ''
    if ids.present? && ids.length >0
      ids.each do |id|
        boot_ids += (id +",")
      end
    end
    equip_work.boot_ids = boot_ids #检查项id
    if equip_work.save
      render json: {status: 200}
    end
  end

  def show
    s_area = SArea.find_by(:id => @equip_work.s_area_id)  #查询区域
    area_name = s_area.present? ? s_area.name : ''

    boot_ids = @equip_work.boot_ids.split(",")
    @boots = BootCheck.select(:id, :check_project).where(:id =>boot_ids)  #检查项

    # region_code_info = SRegionCodeInfo.find_by(:id => @equip_work.s_region_code_info_id)
    # region_info_name = region_code_info.present? ? region_code_info.unit_name : ''  #单位名称


    render json: {equip_work: {id: @equip_work.id,
                                area_name: area_name, 
                                # s_region_code_info_id: @equip_work.s_region_code_info_id,
                                # region_info_name: region_info_name,
                                s_region_code_id: @equip_work.s_region_code_id,
                                s_area_id: @equip_work.s_area_id,
                                d_login_msg_id: @equip_work.d_login_msg_id,
                                region_name: @equip_work.region_name,
                                work_person: @equip_work.work_person,
                                work_start_time: @equip_work.work_start_time,
                                work_end_time: @equip_work.work_end_time,
                                work_desc: @equip_work.work_desc,
                                founder: @equip_work.fourder 
                              },
                  boots: @boots            
                  }
  end

  def edit
    if @equip_work.work_status == 'wait_issued'
      @equip_work
      @boots = @equip_work.work_boots
    else
      render json: {status: '已经下发，不可修改'}
    end
  end

  def update
      if @equip_work.update(get_work_params)

        if params[:boot_ids]
          ids = params[:boot_ids]
          boot_ids = ''
          if ids.present? && ids.length >0
            ids.each do |id|
              boot_ids += (id +",")
            end
          end
          @equip_work.boot_ids = boot_ids #检查项id
          @equip_work.save
        end
        render json: {status: 200}
      end
  end


  #下发  post /d_equip_works/issuedcd t
  def issued
    if @equip_work.work_status == 'wait_issued'
      @equip_work.update(:work_person => params[:work_person], :work_status => 'wait_carried')
      WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'issued', :login_name => @current_user.login_name)
      render json: {status: 200}
    end
  end

  #处理 post /d_equip_works/carried_out
  def carried_out
    if @equip_work.work_status == 'wait_carried'
      params[:boot_hash].each do |b|
        boot = WorkBoot.find_by(:id => b[1]['boot_id'].to_i)
        if boot.update(:work_result =>b[1]['work_result'], :abnor_desc =>b[1]['abnor_desc'])
          note = WorkNote.find_by(:d_equip_work_id => @equip_work.id, :status => 'deal_with')
          if note
            note.update(:login_name => @current_user.login_name)
          else
            WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'deal_with', :login_name => @current_user.login_name)
          end
        end
      end

      #添加上传图片
      image_file = params[:image_file]
      if image_file.present?
        #获取到上传的图片路径
        img_path = upload_img(image_file, @equip_work)
        @equip_work.update(:work_status => 'carry_out', :handle_time => Time.now, :img_path => img_path)
      end

      WorkNote.create!(:d_equip_work_id => @equip_work.id, :status => 'carry_out', :login_name => @current_user.login_name)
      render json: {status: 200}
    end
  end

   #上传图片
  def upload_img(image_file, equip)
    if image_file.present?
      time = Time.now
      img_flag = "jpeg" if image_file[0, 15] == "data:image/jpeg"
      img_flag = "png" if image_file[0, 14] == "data:image/png"
      img_file = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])
      img_name = "#{equip.id}.#{img_flag}"
      file_path = "#{Rails.root}/public/test/d_equip_works/#{time.year}/#{time.month}"
      FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
      #向dir目录写入文件
      File.open(Rails.root.join("#{file_path}", img_name), 'wb') {|f| f.write(img_file)}
      return "test/d_equip_works/#{time.year}/#{time.month}/#{img_name}"
    end
  end

  # DELETE /d_task_forms/1
  # DELETE /d_task_forms/1.json
  def destroy
    if @equip_work.destroy
      render json: {status: 200}
    else
      render json: {status: 'error'}
    end
  end

  
  #获取检查项
  #get /d_equip_works/get_boots
  def get_boots
    @boots = BootCheck.select(:id, :check_project)
    render json: {boots: @boots}
  end

  #新增页面 获取车间部门 信息
  #get /d_equip_works/get_equip_regions
  def get_equip_regions
    #只查询设备部门的车间单位
    # @region_codes = SRegionCodeInfo.where(:status => 'Y',:unit_code => ['equip_hc','equip_fj','equip_jz','equip_hb']).map{|x| {id: x.id, unit_code: x.unit_code, unit_name: x.unit_name}}
    # render json: {region_codes: @region_codes}
  end

  #获取人员
  #get /d_equip_work_rules/get_login
  def get_login
    #region_id = params[:region_id]
    area_id = params[:area_id]
    group_id = params[:region_id]

    #@logins = DLoginMsg.select(:id, :login_name).where(:s_area_id =>area_id, :s_region_code_info_id => region_id)
    @logins = DLoginMsg.select(:id, :login_name).where(:s_area_id =>area_id, :group_id => group_id)
    render json: {login: @logins}
  end


  private
  def get_work
    @equip_work = DEquipWorkRule.find(params[:id])
  end

  def get_work_params
    params.require(:d_equip_work_rule).permit(:s_region_code_id, :s_area_id, :region_name, :work_start_time, :work_end_time, :work_desc,:s_region_code_info_id,:d_login_msg_id)
  end

  #查询接口----获取车间
  def get_region
    # puts "group_id: #{@current_user.group_id}"
    # region_code = SRegionCode.find_by(:id => @current_user.group_id)
    # father_region = SRegionCode.find_by(:father_region_id == 99999)
    # if region_code.father_region_id == 99999
    #   @region_code = SRegionCode.select(:id, :region_name).where("father_region_id != 99999", :s_region_level_id =>3)
    # elsif region_code.father_region_id == father_region.id
    #   @region_code = SRegionCode.select(:id, :region_name).where(:father_region_id => region_code.id, :s_region_level_id =>3)
    # else
    #   @region_code = SRegionCode.select(:id, :region_name).where(:father_region_id => region_code.father_region_id, :s_region_level_id =>3)
    # end

    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司

    if region_code.s_region_level_id == 1  #2 分公司   1 总公司   3 车间
      @region_code = SRegionCode.select(:id, :region_name).where(:s_region_level_id =>3)
    elsif region_code.s_region_level_id == 2
      @region_code = SRegionCode.select(:id,:region_name).where(:father_region_id =>region_code.id, :s_region_level_id =>3)
    elsif region_code.s_region_level_id == 3
      @region_code = SRegionCode.select(:id,:region_name).where(:id =>region_code.id, :s_region_level_id =>3)
    else
      @region_code = ''
    end



  end

end