class EquipMainsController < ApplicationController
    before_action :get_fault_job, only: [:show, :update, :edit, :audit, :destory]
    before_action :authenticate_user

    #GET /fault_jobs
    #GET /fault_jobs.json
    def index
        @taskName = @current_user.s_role_msg.role_name
        #按条件查询
        @station_ids = params[:d_station_id] #站点id
        @job_no = params[:job_no]   #工单号
        @author = params[:author]   #创建人
        @created_time = params[:created_time].to_s + " 00:00:00" if params[:created_time].present? #创建起始时间  
        @end_time = params[:end_time].to_s + " 23:59:59" if params[:end_time].present?  #创建结束时间 
        @handle_man = params[:handle_man] #当前处理人
        @create_type = params[:create_type] #创建类型
        @job_status = params[:job_status] #工单状态
       
        # @faultJob = DTaskForm.where(:job_status => 'wait_maint')
        #                     .by_job_no(@job_no).by_author(@author)
        #                     .by_time(@created_time,@end_time).by_station_id(@station_ids)
        #                     .by_handle_man(@handle_man).by_create_type(@create_type).by_job_status_s(@job_status)
        #                     .by_fault_type('fault_form')   #只显示故障单
        #                     .order(:created_at => :desc)
        #                     .page(params[:page]).per(params[:per])
                            #.by_job_status_w( @job_status)  # 默认 只显示 待维修 审核通过的


        #运维人员登录时只显示他的工单
        if @current_user.s_role_msg.role_name == '运维成员'
            @faultJob = DTaskForm.by_handle_man_id(@current_user.id)
                                .where(:job_status => 'wait_maint')
                                .by_job_no(@job_no).by_author(@author)
                                .by_time(@created_time,@end_time).by_station_id(@station_ids)
                                .by_handle_man(@handle_man).by_create_type(@create_type).by_job_status_s(@job_status)
                                .by_fault_type('fault_form')   #只显示故障单
                                .order(:created_at => :desc)
                                .page(params[:page]).per(params[:per])
                                #.by_job_status_w( @job_status)  # 默认 只显示 待维修 审核通过的
        else
            @faultJob = DTaskForm.where(:job_status => 'wait_maint')
                                .by_job_no(@job_no).by_author(@author)
                                .by_time(@created_time,@end_time).by_station_id(@station_ids)
                                .by_handle_man(@handle_man).by_create_type(@create_type).by_job_status_s(@job_status)
                                .by_fault_type('fault_form')   #只显示故障单
                                .order(:created_at => :desc)
                                .page(params[:page]).per(params[:per])
                                #.by_job_status_w( @job_status)  # 默认 只显示 待维修 审核通过的
        end
       
    end

    #GET /fault_jobs/id
    #GET /fault_jobs/1.json
    def show
    end

    #GET /fault_jobs/new
    def new
        @faultJob = DTaskForm.new()
    end

    #GET /fault_jobs/id/edit
    def edit
        @handle = @faultJob.d_fault_handles
        @equilp = SEquipmentInfo.where(:d_station_id =>@faultJob.d_station_id,:equip_type =>@faultJob.device_name,
                                        :types => 'N')
    end

    #图片上传方法
    def upload_img(image_file,powerCutRecord)

        img_flag = "jpeg"  if image_file[0,15] == "data:image/jpeg"
        img_flag = "png"  if image_file[0,14] == "data:image/png"

        new_name = "power_img#{powerCutRecord.id}.#{img_flag}"  #新名字
        png = Base64.decode64(image_file["data:image/#{img_flag};base64,".length .. -1])   #解析文件

        time = Time.now
        file_path = "#{Rails.root}/public/test/#{time.year}/#{time.month}/#{time.day}"
        FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
        File.open(Rails.root.join("#{file_path}", new_name ), 'wb') { |f| f.write(png) }

       # File.open(Rails.root.join("public","task_app_images", new_name ), 'wb') { |f| f.write(png) }
       # powerCutRecord.img_one="task_app_images/#{new_name}"  #将路径保存到数据库中

        powerCutRecord.img_one="/test/#{time.year}/#{time.month}/#{time.day}"+'/'+"#{new_name}"

        powerCutRecord.save
    end

    #PUT /fault_jobs/id
    #PUT /fault_jobs/id.json
    def update
        #d_task_form_id = params[:id]
        maint_name = params[:maint_name]
        @image_file = params[:image_file]         #获取图片
        #创建维修项目表
        @handle = SMaintProject.new()

        #找到对应的处理表
        @fault=DFaultHandle.find_by(:d_task_form_id => params[:id])
        #保存图片
        if @image_file.present?
            upload_img(@image_file,@fault)
        end

        @handle.maint_name = maint_name
        @handle.d_task_form_id = @faultJob.id
        @handle.maint_code = @current_user.login_name  #将当前登录人作为维修人

        respond_to do |format|
            if @handle.save
                @faultJob.update(:job_status =>'wait_calib')  #待校准
                #创建日志
                details_create(@faultJob)
                format.html{redirect_to fault_jobs_path}
                format.json{render json:{status: "success",location: @faultJob}}
            else
                format.html{redirect_to edit_fault_jobs_path}
                format.json{render json:{status: "false",location: @faultJob.errors}}
            end
        end
    end


    def destory
        respond_to do |format|
            if @faultJob.destory
                format.html{}
                format.json{render json:{status: "success",location: @faultJob}}
            else
                format.html{}
                format.json{render json:{status: "false",location: @faultJob.errors}}
            end
        end 
    end

    private

    def details_create(faultJob)
        @details = @faultJob.d_fault_job_details.new(:job_status =>@faultJob.job_status,
                                                    :handle_man =>@current_user.login_name,:begin_time =>Time.now,
                                                    :end_time =>Time.now,:handle_status =>"完成",:without_time_flag =>"无")
        @details.save
    end

    def get_fault_job
        @faultJob = DTaskForm.find(params[:id])
        #@maint_project=@faultJob.s_maint_projects  #关联维修项目
        @maint_project=SMaintProject.find_by(:d_task_form_id => params[:id])  #关联维修项目
        
    end

    def params_fault_job
        params.require(:d_fault_job).permit(:fault_type,:urgent_level,:send_type,:d_station_id,
                                            :station_name,:device_name,:fault_phenomenon,:other_fault,
                                            :title,:content,:job_no,:create_type,:author,:job_status,
                                            :clraring_if)
    end

end