class TempleteManagesController < ApplicationController
  before_action :get_templete, only: [:update, :show, :edit, :destroy]
  before_action :authenticate_user # 转存公共参数 获取登录信息

  # GET /templete_manages
  # GET /templete_manages.json
  def index
    @templetes=DTemplete.order(:created_at => :desc).page(params[:page]).per(params[:per])
  end

  # GET /templete_manages/1
  # GET /templete_manages/1.json
  def show
  end


  # POST /templete_manages
  # POST /templete_manages.json
  def create
   @templete=DTemplete.new(templete_params)
   @templete.create_user=@current_user.login_name  #登录人作为创建人


   respond_to do |format|
    if @templete.save
      format.json {render json: {status: 'success', location: @templete}}
      format.html {}
    else
      format.json {render json: {status: 'false', location: @templete.errors}}
      format.html {}
    end
  end
  end

  # PATCH/PUT /templete_manages/1
  # PATCH/PUT /templete_manages/1.json
  def update
    respond_to do |format|
      if @d_templete.update(templete_params)
        format.json {render :show}
        format.html {}
      else
        format.json {render json: {status: 'false'}}
        format.html {}
      end
    end
  end

  # DELETE /templete_manages/1
  # DELETE /templete_manages/1.json
  def destroy
    @d_templete.destroy
    respond_to do |format|
      format.html { }
      format.json { render :index }
    end
  end

  private 
  def get_templete
     @d_templete=DTemplete.find(params[:id])
  end
 
  def templete_params
    params.require(:d_templete).permit(:id, :create_user, :name,
                                  :title,:templete_type,:content)
  end  

  
end
