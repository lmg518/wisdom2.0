class DataView::AirDataAnalyzeController < ApplicationController

#GET data_view/air_data_analyze.json
def index
    data_time=Time.parse(params[:data_time]).strftime("%Y%m%d") if params[:data_time].present?
    @hour_datas=DDataAnalyze.where(:data_type => 3600)
                            .by_station_id(params[:station_id])
                            .by_hour_data_time(data_time)
                            .by_status(params[:status])
                            .order(:data_time => :desc)

    @hour_datas=@hour_datas.page(params[:page]).per(params[:per])
end

#  2017123100   2017123123
#GET data_view/air_data_analyze/min5_datas.json?data_time=2017-12-31
def min5_datas
    data_time=params[:data_time]
    begin_time=Time.parse(data_time).beginning_of_day.strftime("%Y%m%d%H")
    end_time=Time.parse(data_time).end_of_day.strftime("%Y%m%d%H")

    Rails.logger.info "---begin_time----#{begin_time}-------"
    Rails.logger.info "----end_time---#{end_time}-------"

    @min_datas=DDataAnalyze.where(:data_type => 300)
                            .by_data_time(begin_time,end_time)
                            .by_station_id(params[:station_id])
                            .order(:data_time => :asc)
    
end

end