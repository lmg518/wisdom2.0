class DataView::AlarmMonitoringsController < ApplicationController
  before_action :current_user
  before_action :set_alarm_monitoring, only: [:show]

  def index

  end

  def init_index
    @now_time = Time.now
    if params[:search].present?
      @search = params[:search]
      @station_ids = @search[:station_ids]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
      @rule_name = @search[:rule_name]
    end
    @data_times = [] if @data_times.blank?
    @role = @current_user.s_role_msg
    case @role.role_name
      when '运维人员'
        @alarm_stations = @current_user.d_stations.by_station_ids(@station_ids).active
      else
        @region = @current_user.s_region_code
        @alarm_stations = @region.d_stations.by_station_ids(@station_ids).active
    end
    @alarm_arr = []
    @alarm_stations.each do |station|
      if station.d_alarms.present?
        alarms = station.d_alarms.by_times(@now_time, @data_times)
                     .by_rule_levels(@alarm_levels)
                     .by_rule_name(@rule_name)
        alarms.pluck(:s_alarm_rule_id).uniq.each do |rule_id|
          @alarm_rule_data = []
          @alarm_rule = 0, ''
          alarms.where(:s_alarm_rule_id => rule_id).each do |alarm|
            @alarm_rule = alarm.alarm_rule
            @alarm_rule_data << {id: alarm.id, alarm_level: alarm.alarm_level,
                                 d_time: alarm.first_alarm_time.strftime('%Y-%m-%d %H:00:00'),
                                 last_time: alarm.last_alarm_time.present? ? alarm.last_alarm_time.strftime('%Y-%m-%d %H:00:00') : ''}
          end
          @new_data_arr = []
          24.times do |i|
            if @data_times.present?
              begin_time = Time.parse(@data_times).beginning_of_day
              y_time = (begin_time + i.hours).strftime("%Y-%m-%d")
              h_time = (begin_time + i.hours).strftime("%H:00:00")
            else
              y_time = (Time.now - i.hours).strftime("%Y-%m-%d")
              h_time = (Time.now - i.hours).strftime("%H:00:00")
            end
            @d_hash = y_time + " " + h_time
            index = @alarm_rule_data.index{|x|   x[:d_time] == @d_hash}
            if index.present?
              @new_data_arr << @alarm_rule_data[index]
            else
              @new_data_arr << {   id: 0, alarm_level: 0,
                                   d_time: @d_hash,
                                   last_time: ""}
            end
          end
          if @data_times.present?
            @alarm_arr << {station_id: station.id, station_name: station.station_name, alarm_rule: @alarm_rule, alarms: @new_data_arr}
          else
            @alarm_arr << {station_id: station.id, station_name: station.station_name, alarm_rule: @alarm_rule, alarms: @new_data_arr.reverse}
          end
        end
      end
    end
    @alarm_monitorings =@alarm_arr
    # @alarm_monitorings.map{|x| puts x}
    if params[:sort_by_station].present?
      sort_by_station = params[:sort_by_station]
      @alarm_monitorings.sort! {|x, y| x[:station_name] <=> y[:station_name]} if sort_by_station == 'desc'
      @alarm_monitorings.sort! {|x, y| y[:station_name] <=> x[:station_name]} if sort_by_station == 'asc'
    end
    if params[:sort_by_rule].present?
      sort_by_rule = params[:sort_by_rule]
      @alarm_monitorings.sort! {|x, y| x[:alarm_rule] <=> y[:alarm_rule]} if sort_by_rule == 'desc'
      @alarm_monitorings.sort! {|x, y| y[:alarm_rule] <=> x[:alarm_rule]} if sort_by_rule == 'asc'
    end


    @alarm_monitorings #= Kaminari.paginate_array(@alarm_monitorings).page(params[:page]).per(10)
    # 24.times do |i|
    #   @flag = true
    #   @a_time = (@now_time - (23-i).hours).strftime("%Y-%m-%d %H:00:00")
    #
    #     @alarm_level_arr =  @alarm_stations.map{|x| x[:alarm_level] if x[:d_time] == @a_time}
    #   @alarm_stations.each do |ala|
    #       @station_id = ala[:station_id]
    #       @station_name = ala[:station_name]
    #       @alarm_rule = ala[:alarm_rule]
    #       @rule_id = ala[:id]
    #       @level_val = @alarm_level_arr.include?(3) ? 3 : @alarm_level_arr.include?(2) ? 2 : @alarm_level_arr.include?(1) ? 1 : 0
    #       if ala[:d_time] == @a_time
    #         @flag = true  if  @alarm_level_arr.size != 1
    #         @alam_hash << { station_id: ala[:station_id], station_name: ala[:station_name] ,id:@rule_id,alarm_rule: ala[:alarm_rule],alarm_level:  @level_val , d_time: ala[:d_time]}
    #       else
    #         @flag = false
    #         break
    #       end
    #     end
    #
    #   unless @flag
    #     @alam_hash << { station_id: @station_id, station_name: @station_name ,id:@rule_id, alarm_rule: @alarm_rule,alarm_level: @level_val, d_time: @a_time}
    #   end
    # end

    # @alam_hash.uniq!
    # @alam_hash.each_with_index do |f,index.html.erb|
    #   if  @alam_hash[index.html.erb + 1].present?
    #     @alam_hash.delete_at(index.html.erb) if @alam_hash[index.html.erb + 1][:d_time] == f[:d_time]
    #   end
    # end
    # @alarm_arr << @alam_hash
  end

  def show
    render json: {alarm_monitorings: {id: @alarm_monitoring.id, station_id: @alarm_monitoring.station_id,
                                      station_name: @alarm_monitoring.d_station.station_name,
                                      alarm_rule: @alarm_monitoring.alarm_rule, alarm_level: @alarm_monitoring.alarm_level_to_s,
                                      first_alarm_time: @alarm_monitoring.first_alarm_time.strftime("%Y-%m-%d %H:%M:%S"),
                                      last_alarm_time: @alarm_monitoring.last_alarm_time.present? ? @alarm_monitoring.last_alarm_time.strftime("%Y-%m-%d %H:%M:%S") : '',
                                      continuous_alarm_times: @alarm_monitoring.continuous_alarm_times,
                                      status: @alarm_monitoring.status_to_str,
                                      created_at: @alarm_monitoring.created_at.strftime("%Y-%m-%d %H:%M:%S"), updated_at: @alarm_monitoring.updated_at.strftime("%Y-%m-%d %H:%M:%S")}}
  end

  private

  def set_alarm_monitoring
    @alarm_monitoring = DAlarm.find(params[:id])
  end

end    