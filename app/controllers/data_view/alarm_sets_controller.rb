class DataView::AlarmSetsController < ApplicationController
 before_action :get_alarm_rule, only: [:show, :edit, :update, :destroy]

 # search:{rule_type_id:"", rule_name: '',ab_rule_instance:''}
 def index
   if params[:search].present?
     @search = params[:search]
     @rule_name = @search[:rule_name]
     @rule_type_id = @search[:rule_type_id]
     @ab_rule_instance = @search[:ab_rule_instance]
   end
    @s_alarm_rules = SAlarmRule.by_rule_name(@rule_name)
                                .by_rule_type_id(@rule_type_id)
                                 .by_rule_instance(@ab_rule_instance).order(:created_at => :desc)
   @s_alarm_rules = @s_alarm_rules.page(params[:page]).per(params[:per])
 end

 def show
   render json: {alarm_rile: @s_alarm_rule,alarm_levels: @s_alarm_rule.s_alarm_rule_levels.order(:alarm_level => :desc)}
 end

 def edit
    render json: {alarm_rile: @s_alarm_rule}
 end

 def create
   @s_alarm_rule = SAlarmRule.new(set_alarm_rule_params)
     respond_to do |format|
          if @s_alarm_rule.save
            if params[:alarm_levels].present?  # alarm_levels:[{level: 1,ab_times: 5},{level: 1,ab_times: 5}]
              JSON.parse(params[:alarm_levels]).each do |level|
                @s_alarm_rule.s_alarm_rule_levels.create(:rule_code=>@s_alarm_rule.rule_code,:alarm_level=> level['level'],:ab_times=> level['ab_times'])
             end
           end    
           format.html{}
           format.json { render json: {location: @s_alarm_rule, status: :ok } }
       else
           format.html{}
           format.json { render json: {location: @s_alarm_rule.errors, status: :unprocessable_entity } }
       end    
     end    
 end 

 def update
    respond_to do |format|
      if @s_alarm_rule.update(set_alarm_rule_params)
          if params[:alarm_levels].present?  # alarm_levels:[{level: 1,ab_times: 5},{level: 1,ab_times: 5}]
            JSON.parse(params[:alarm_levels]).each do |level|
              begin
                @s_alarm_rule.s_alarm_rule_levels.find_by(:alarm_level => level["level"]).update(:ab_times=> level['ab_times'])
              rescue Exception => e
                @s_alarm_rule.s_alarm_rule_levels.create(:alarm_level=> level['level'],:ab_times=> level['ab_times'])
              end
             end
           end  
         format.html{}
         format.json { render json: {location: @s_alarm_rule, status: :ok } }
      else
         format.html{}
         format.json { render json: {location: @s_alarm_rule.errors, status: :unprocessable_entity } }
      end    
    end    
 end

 private

 def get_alarm_rule
    @s_alarm_rule = SAlarmRule.find(params[:id])
 end    

 def set_alarm_rule_params
    params.require(:s_alarm_rule).permit(:rule_code, :rule_name, :rule_type, :rule_desc,
                                         :ab_rule_instance, :deal_interval, :sample_num, :status, :rule_instance_id, :rule_type_id)
 end    
    
end     