class DataView::ExceptionSetsController < ApplicationController
  before_action :set_exception, only: [:update, :show, :destroy]

  def index
    search = params[:search].present? ? params[:search] : []
    if search.present?
      @exception_sets = SAbnormalRuleInstance.by_station_id(search["station_id"])
                            .by_instance_name(search['instance_name'])
                            .by_abnormal_rule_name(search['rule_name'])
                            .order('created_at desc')
      #  .by_abnormal_items(search['item_id'])
      @exception_sets = @exception_sets.page(params[:page]).per(10)
    else
      @exception_sets = SAbnormalRuleInstance.order('created_at desc').page(params[:page]).per(params[:per])
    end
  end

  def show
    abnormal_item = @s_abnormal_rule_instance.s_abnormal_ltem
    abnormal_rule= @s_abnormal_rule_instance.s_abnormmal_rule
    puts abnormal_rule.inspect
    abnormal = {id: @s_abnormal_rule_instance.id, instance_name: @s_abnormal_rule_instance.instance_name, rule_code: abnormal_rule.rule_name,
                parameter_num: abnormal_rule.parameter_num,
                item_name: abnormal_item.present? ? abnormal_item.item_name : "站点",
                rule_des: abnormal_rule.rule_expression_des,
                compare_code: abnormal_rule.compare_code,
                parameter_value: @s_abnormal_rule_instance.parameter_value,
                compare_code2: abnormal_rule.compare_code2, parameter_value2: @s_abnormal_rule_instance.parameter_value2, status: @s_abnormal_rule_instance.status, note: @s_abnormal_rule_instance.note}
    render json: {exception_sets: abnormal, area_stations: @s_abnormal_rule_instance.d_stations}
  end

  def update
    respond_to do |format|
      if @s_abnormal_rule_instance.update(new_exception_params)
        if params[:station_arr].present?
          station_arr = params[:station_arr].map {|x| x.to_i}
          @rule_reles = @s_abnormal_rule_instance.d_station_rule_rels
          if station_arr.length == 1 && station_arr[0] == ''
            @rule_reles.delete_all
            format.html {}
            format.json {render json: {status: :ok, location: @s_abnormal_rule_instance.get_index}}
            return
          end
          @rule_station_ids = @rule_reles.map {|x| x.d_station_id}
          if @rule_reles.present?
            if @rule_station_ids.present?
              station_arr.each_with_index do |arr, index|
                @rule_reles.each do |x|
                  x.delete unless station_arr.include? x.d_station_id
                end
                @rule_reles.create(:d_station_id => arr.to_i) if !@rule_station_ids.include?(arr.to_i)
              end
            end
          else
            station_arr.map {|x| @rule_reles.create(:d_station_id => x)}
          end
        end
        format.html {}
        format.json {render json: {status: :ok, location: @s_abnormal_rule_instance.get_index}}
      else
        format.html {}
        format.json {render json: {status: :unprocessable_entity, location: @s_abnormal_rule_instance.errors}}
      end
    end
  end

  def create
    @s_abnormal_rule_instance = SAbnormalRuleInstance.new(new_exception_params)
    respond_to do |format|
      if @s_abnormal_rule_instance.save!
        if params[:station_arr].present?
          station_arr = params[:station_arr]
          station_arr.each do |arr|
            @s_abnormal_rule_instance.d_station_rule_rels.create(:d_station_id => arr.to_i)
          end
        end
        format.html {}
        format.json {render json: {status: :ok, location: @s_abnormal_rule_instance.get_index}}
      else
        format.html {}
        format.json {render json: {status: :unprocessable_entity, location: @s_abnormal_rule_instance.errors}}
      end
    end
  end

  def destroy

  end

  private

  def set_exception
    @s_abnormal_rule_instance = SAbnormalRuleInstance.find(params[:id])
  end

  def new_exception_params
    params.require(:s_abnormal_rule_instance).permit(:rule_instance, :instance_name, :rule_code, :parameter_value,
                                                     :parameter_value2, :parameter_value3, :parameter_value4,
                                                     :status, :note, :s_abnormal_ltem_id, :s_abnormmal_rule_id)
  end

end