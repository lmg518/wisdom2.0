class DataView::DataAbnormalFormController < ApplicationController
    before_action :authenticate_user
    before_action :set_work, only: [:show, :edit, :update, :destroy, :audit]

    def index
      #获取创建开始时间
      @created_time=params[:created_time].to_s + " 00:00:00" if params[:created_time].present?
      #获取结束时间
      @end_time=params[:end_time].to_s + " 23:59:59" if params[:end_time].present?
    end

    # GET /data_abnormal_form/1/edit
    # def edit
    # end

    #/data_view/data_abnormal_form/1
    def show
    end
    #审核操作
    #get  /data_view/data_abnormal_form/id/audit.json
    def audit
      job_status=@d_task_form.job_status #获取工单状态
      audit_if=params[:audit_if] #获取审核内容  Y  N 
  
      if audit_if=='Y' && job_status=='un_audit'
        @d_task_form.job_status='audit' #修改工单状态
        @d_task_form.audit_if='Y'
        handle_status='完成' #日志中改为  ‘完成’
      end
      if audit_if=='N' && job_status=='un_audit' && params[:audit_des].present?  #审核不通过时  必须要有审核不通过原因
        @d_task_form.audit_if='N'
        @d_task_form.audit_des = params[:audit_des]
        handle_status='未通过' #日志中改为  ‘未通过’
        @d_task_form.job_status='wait_deal'  #工单状态改为 待处理状态
  
        note=params[:audit_des]   #将不通过原因 保存到日志表中的 note字段
        
      elsif audit_if=='N' && job_status=='un_audit' && !params[:audit_des].present?
        return render json: {status: 'false', location: @d_task_form.errors}
      end
  
      @d_task_form.audit_man=@current_user.login_name  #获取当前的审核人
      @d_task_form.audit_time=Time.now   #更新审核时间
  
      #保存工单日志 （通过不通过均保存日志信息）
      @d_fault_job_detail=@d_task_form.d_fault_job_details.new(:job_status => @d_task_form.job_status, :handle_man => @current_user.login_name, :begin_time => Time.now, :end_time => Time.now, :handle_status => handle_status, :note => note)
      @d_fault_job_detail.save
      respond_to do |format|
        if @d_task_form.save && audit_if=='Y' && job_status=='un_audit' #更新表单中填写的数据  审核人、审核不通过原因
          format.html {}
          format.json {render json: {status: 'success', location: @d_task_form}}
        else
          format.html {}
          format.json {render json: {status: 'false', location: @d_task_form.errors}}
        end
      end
    end


    #编辑修改数据
    # PATCH/PUT /data_view/data_abnormal_form/1
    def update
        @image_file = params[:image_file]         #获取图片

        if @image_file.present?
            upload_img(@image_file,@d_task_form)
            @d_task_form.job_status ='un_audit'    #工单状态 改为待审核
        end

        respond_to do |format|
          if @d_task_form.save

            details_create(@d_task_form)   #创建日志

            format.json {render :index}
            format.html {}
          else
            format.json {render json: {status: 'false'}}
            format.html {}
          end
      end
    end


    #创建工单日志
    def details_create(d_task_form)
     @details = @d_task_form.d_fault_job_details.new(:job_status =>@d_task_form.job_status,
                                                :handle_man =>@d_task_form.handle_man,:begin_time =>Time.now,
                                                :end_time =>Time.now,:handle_status =>"完成",:without_time_flag =>"无")
     @details.save
    end

  #图片上传方法
  def upload_img(image_file,powerCutRecord)
        new_name = "data_abnormal_img#{Time.now.strftime('%Y%m%d%H%M%S')}.jpg"      #新名字
        png = Base64.decode64(image_file['data:image/jpeg;base64,'.length .. -1])   #解析文件
        time = Time.now
        file_path = "#{Rails.root}/public/test/#{time.year}/#{time.month}/#{time.day}"
        FileUtils.mkdir_p(file_path) unless File.exist?(file_path)
        File.open(Rails.root.join("#{file_path}", new_name ), 'wb') { |f| f.write(png) }
        powerCutRecord.img_one = "/test/#{time.year}/#{time.month}/#{time.day}"+'/'+"#{new_name}"
      end

    #给:show, :edit, :update, :destroy  提供数据
    private
    def set_work
      @d_task_form = DTaskForm.find(params[:id])
      @details= @d_task_form.d_fault_job_details  #关联日志信息
    end
    
end