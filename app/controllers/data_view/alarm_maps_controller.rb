class DataView::AlarmMapsController < ApplicationController
  before_action :current_user
  #全国地图  报警省份和站点个数，报警级别

  def index

  end

  def init_index
    @zone_arr = []
    @alarm_zone_arr = []
    @alarm_level_1,@alarm_level_2,@alarm_level_3 = [],[],[]
    @province_arr = []
    @station_counts = 0
    region_code = @current_user.s_region_code
    @stations = region_code.d_stations
    @stations.each do |station|
       alarm_arr =  station.d_alarms.by_today_data
       area_zone =  station.s_administrative_area
       # puts station.inspect
       @station_counts += 1  if alarm_arr.present?
       if alarm_arr.present?
         alarm_level_arr =  alarm_arr.map{|x|x.alarm_level.to_i}
         zone_parent = area_zone.parent
         @zone_np = zone_parent.present? ? zone_parent.zone_name : area_zone.zone_name
         if  alarm_level_arr.include?(3)
           @alarm_level_3 << @zone_np
           @zone_arr << @zone_np
           next
         end
         if  alarm_level_arr.include?(2)
           @alarm_level_2 << @zone_np
           @zone_arr << @zone_np
           next
         end
         if  alarm_level_arr.include?(1)
           @alarm_level_1 << @zone_np
           @zone_arr << @zone_np
           # {alarm_level: alarm_arr.first.alarm_level, id: zone_parent.id,province_name: @zone_np}
           next
         end
       end

       # if area_zone.parent.present?
       #     zone_parent = area_zone.parent
       #     @zone_np = zone_parent.zone_name
       #     @zone_arr << @zone_np
       #     if alarm_arr.present?
       #       puts  alarm_level_arr =  alarm_arr.map{|x|x.alarm_level.to_i}
       #       if  alarm_level_arr.include?(3)
       #         @alarm_level_3 << @zone_np
       #        next
       #       end
       #       if  alarm_level_arr.include?(2)
       #         @alarm_level_2 << @zone_np
       #         next
       #       end
       #       if  alarm_level_arr.include?(1)
       #         @alarm_level_1 << @zone_np
       #         # {alarm_level: alarm_arr.first.alarm_level, id: zone_parent.id,province_name: @zone_np}
       #         next
       #       end
       #     end
       # end
    end
    @zone_arr = @zone_arr.uniq
     # @zone_arr.map{|x| puts x}
    @all_infos = {province_counts: @zone_arr.size,alarm_stations: @station_counts}
    @alarm_level_infos = { first_level_province: @alarm_level_1.uniq, second_level_province: @alarm_level_2.uniq,
                           third_level_province: @alarm_level_3.uniq }
    @alarm_level_infos[:first_level_province].map{|x| @province_arr << {name: x,level: 1,province_data:get_province(x)}} if @alarm_level_infos[:first_level_province].present?
    @alarm_level_infos[:second_level_province].map{|x| @province_arr << {name: x,level: 2,province_data:get_province(x)}} if @alarm_level_infos[:second_level_province].present?
    @alarm_level_infos[:third_level_province].map{|x| @province_arr << {name: x,level: 3,province_data:get_province(x) }} if @alarm_level_infos[:third_level_province].present?

  end

  def get_province(province)
      @zone_arr = []
      @alarm_zone_arr = []
      @alarm_level_1 = []
      @alarm_level_2 = []
      @alarm_level_3 = []
      @station_counts, @station_count_1, @station_count_2, @station_count_3 = 0,0,0,0
      @stations = @current_user.d_stations.where(:s_administrative_area_id => SAdministrativeArea.find_by_zone_name(province).children.pluck(:id) )
      @stations.each do |station|
        alarm_arr =  station.d_alarms
        area_zone =  station.s_administrative_area
        @station_counts += 1  if alarm_arr.present?
        if area_zone.parent.present?
          zone_parent = area_zone.parent
          @zone_np = zone_parent.zone_name
          @zone_arr << @zone_np
          if alarm_arr.present?
            alarm_level_arr =  alarm_arr.map{|x|x.alarm_level.to_i}
            if  alarm_level_arr.include?(3)
              @alarm_level_3 << {station_count: @station_count_3 += 1,alarm_counts: alarm_arr.where(:alarm_level => 3).size}
              next
            end
            if  alarm_level_arr.include?(2)
              @alarm_level_2 << {station_count: @station_count_2 += 1,alarm_counts: alarm_arr.where(:alarm_level => 2).size}
              next
            end
            if  alarm_level_arr.include?(1)
              @alarm_level_1 << {station_count: @station_count_1 += 1,alarm_counts: alarm_arr.where(:alarm_level => 1).size}
              # {alarm_level: alarm_arr.first.alarm_level, id: zone_parent.id,province_name: @zone_np}
              next
            end

          end
        end
      end

      @level_infos = { first_level_province: @alarm_level_1.present? ? @alarm_level_1.last : {station_count: 0,alarm_counts: 0}, second_level_province: @alarm_level_2.present? ? @alarm_level_2.last : {station_count: 0,alarm_counts: 0},
                             third_level_province:@alarm_level_3.present? ? @alarm_level_3.last : {station_count: 0,alarm_counts: 0} }
  end

end    