class DataView::DAlarmsController < ApplicationController
  before_action :current_user
  before_action :set_alarm, only: [:show]
  def index
    if params[:search].present?
      @search = params[:search]
      @station_ids = @search[:station_ids]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
      @status = @search[:status]
      @alarms = DAlarm.by_arr_ids(@station_ids)
                    .by_search_times(@data_times)
                    .by_rule_levels(@alarm_levels)
                    .by_status(@status)
                    .order(:last_alarm_time => :desc)
    else
      @alarms = DAlarm.by_arr_ids(@station_ids)
                    .by_search_times(@data_times)
                    .by_rule_levels(@alarm_levels)
                    .by_status(@status)
                    .order(:last_alarm_time => :desc)
    end

    if params[:status] == "alarm_num_statistics"
      @params_status = false
      @alarms
    else
      @params_status = true
      @alarms = @alarms.page(params[:page]).per(params[:per])
    end
  end

  def show
    Rails.logger.info "--------------show-----d_alarm------"
    @info_arr = []
    station = @alarm.d_station
    @infos = {station_name: station.station_name, alarm_rule: @alarm.alarm_rule,
              first_create_time: @alarm.first_alarm_time.strftime("%Y-%m-%d %H:%M:%S"),
              last_create_time: @alarm.last_alarm_time.present? ? @alarm.last_alarm_time.strftime("%Y-%m-%d %H:%M:%S") : '',
              alarm_num: @alarm.continuous_alarm_times, status: @alarm.status_to_str }

    #  @alarm.d_alarm_details.each do |detail|
    #  alarm_rule = detail.d_alarm.s_alarm_rule
    #  abnormal_instance = alarm_rule.s_abnormal_rule_instance
    #  item_infos = abnormal_instance.s_abnormal_ltem

    #  item_datas = station.d_abnormal_data_yyyymms.where(:create_acce => detail.last_sample_time, :item_code_id => item_infos.id)
    #  item_datas.each do |item|
    #   #item_infos=SAbnormalLtem.find(item.item_code_id)   #修改的
    #    @info_arr << {item_code: item_infos.item_name,
    #                 item_val: item.ab_value,
    #                 item_lable: item.ab_lable,
    #                 item_create_time: item.create_acce.strftime("%Y-%m-%d %H:%M:%S"),
    #                 #abnormal_rule_instance: abnormal_instance.instance_name,
    #                 abnormal_rule_instance: item.rule_instance,
    #                 alarm_rule: detail.alarm_rule,
    #                 alarm_level: detail.alarm_level_to_s,
    #                 ab_num: detail.ab_num,
    #                 alarm_create_time: detail.create_time.strftime("%Y-%m-%d %H:%M:%S")}
    #  end
    # end


    #render json: {alarm_info: @infos, info_arr: @info_arr.uniq}
    render json: {alarm_info: @infos}
  end

  private
  def set_alarm
    @alarm = DAlarm.find(params[:id])
  end

end