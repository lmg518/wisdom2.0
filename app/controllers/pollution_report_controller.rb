class PollutionReportController < ApplicationController
  before_action :authenticate_user
  #一次水排污上报

  #排污上报
  def pollution_report
      get_datas2
      get_form_values2
    render :partial => "pollution_report", :layout => false
  end

  #一次水上报
  def once_water_report
    get_datas
    get_form_values
    render :partial => "once_water_report", :layout => false
  end



  #获取表格项目数据
  def get_datas
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id =>@region_code.id)  #表格信息  二个表时
    #表格信息
    @form_name = @d_report_forms.name  #表格名称
    @form_headers_ids = DReportFormHeader.where(:d_report_form_id =>@d_report_forms.id).pluck(:d_form_header_id)   #获取表头信息id
    #@form_headers = DFormHeader.where(:id =>@form_headers_ids)  #表头信息  无法排序
    #Rails.logger.info "------@form_body------#{@form_headers_ids}-------"
    @form_headers = []
    @form_headers_ids.each do |id|
      @fd = DFormHeader.find_by(:id =>id)
      name = @fd.present? ? @fd.name : ''
      @form_headers.push(name)
    end
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")
    @form_type = SNenrgyFormType.where(:id => type_ids)  #报表类型数据
    @type_name = @form_type[0].name  #公司
    #封装项目信息
    @ids = DMaterialReginCode.where(:s_region_code_id => @region_code.id).pluck(:s_material_id)
    @form_body = SMaterial.where(:id => @ids)   #从材料表中获取
  end


#获取表格项目数据
def get_datas2
  #根据登录人员 获取人员所在的车间 或公司
  #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
  @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
  #根据region_code id获取报表
  @d_report_forms1 = DReportForm.where(:s_region_code_id =>@region_code.id)  #表格信息  
  @d_report_forms = @d_report_forms1[1]  #获取第二个表格
  #表格头信息
  @form_name = @d_report_forms.name  #表格名称
  @form_headers_ids = DReportFormHeader.where(:d_report_form_id =>@d_report_forms.id).pluck(:d_form_header_id)   #获取表头信息id
  #Rails.logger.info "------@form_body------#{@form_headers_ids}-------"
  @form_headers = []
  @form_headers_ids.each do |id|
    @fd = DFormHeader.find_by(:id =>id)
    name = @fd.present? ? @fd.name : ''
    @form_headers.push(name)
  end
  #Rails.logger.info @form_headers.inspect
  type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")  #报表类型id
  @region_code_ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_region_code_id).uniq  #去重处理
  @steam_code = DSteamRegionCode.where(:s_region_code_id => @region_code_ids[0], :s_nenrgy_form_type_id => '5')
  s_material_ids = @steam_code.pluck(:s_material_id)  #一个车间的项目id
  @form_body1=[]
  @fs = SMaterial.where(:id => s_material_ids)  #根据id 一个车间一个车间找 分车间封装
  @fs.each do |f|
    item ={}
    item["code"] = f.code
    item["name"] = f.name
    region_code = SRegionCode.find_by(:id => @region_code_ids[0])
    item["region_name"] = region_code.region_name   #车间信息
    item["region_id"] = region_code.id              #车间id
    nenrgy_type_id = @steam_code[0].s_nenrgy_form_type_id  #表格类型id
    nenrgy_type = SNenrgyFormType.find_by(:id => nenrgy_type_id)
    item["type_name"] = nenrgy_type.name   #类型
    @form_body1.push(item)
  end

  #第二个车间
  @steam_code2 = DSteamRegionCode.where(:s_region_code_id => @region_code_ids[1], :s_nenrgy_form_type_id => '5' )  
  s_material_ids2 = @steam_code2.pluck(:s_material_id)  #一个车间的项目id
  @form_body2=[]
  @fs2 = SMaterial.where(:id => s_material_ids2)  #根据id 一个车间一个车间找 分车间封装
  @fs2.each do |f|
    item ={}
    item["code"] = f.code
    item["name"] = f.name
    region_code = SRegionCode.find_by(:id => @region_code_ids[1])
    item["region_name"] = region_code.region_name   #车间信息
    item["region_id"] = region_code.id              #车间id
    nenrgy_type_id = @steam_code2[0].s_nenrgy_form_type_id  #表格类型id
    nenrgy_type = SNenrgyFormType.find_by(:id => nenrgy_type_id)
    item["type_name"] = nenrgy_type.name   #类型
    @form_body2.push(item)
  end

  #第三个车间
  @steam_code3 = DSteamRegionCode.where(:s_region_code_id => @region_code_ids[2], :s_nenrgy_form_type_id => '5')  
  s_material_ids3 = @steam_code3.pluck(:s_material_id)  #一个车间的项目id
  @form_body3=[]
  @fs3 = SMaterial.where(:id => s_material_ids3)  #根据id 一个车间一个车间找 分车间封装
  @fs3.each do |f|
    item ={}
    item["code"] = f.code
    item["name"] = f.name
    region_code = SRegionCode.find_by(:id => @region_code_ids[2])
    item["region_name"] = region_code.region_name   #车间信息
    item["region_id"] = region_code.id              #车间id
    nenrgy_type_id = @steam_code3[0].s_nenrgy_form_type_id  #表格类型id
    nenrgy_type = SNenrgyFormType.find_by(:id => nenrgy_type_id)
    item["type_name"] = nenrgy_type.name   #类型
    @form_body3.push(item)
  end

  #第四个车间
  @steam_code4 = DSteamRegionCode.where(:s_region_code_id => @region_code_ids[3], :s_nenrgy_form_type_id => '5')  
  s_material_ids4 = @steam_code4.pluck(:s_material_id)  #一个车间的项目id
  @form_body4=[]
  @fs4 = SMaterial.where(:id => s_material_ids4)  #根据id 一个车间一个车间找 分车间封装
  @fs4.each do |f|
    item ={}
    item["code"] = f.code
    item["name"] = f.name
    region_code = SRegionCode.find_by(:id => @region_code_ids[3])
    item["region_name"] = region_code.region_name   #车间信息
    item["region_id"] = region_code.id              #车间id
    nenrgy_type_id = @steam_code4[0].s_nenrgy_form_type_id  #表格类型id
    nenrgy_type = SNenrgyFormType.find_by(:id => nenrgy_type_id)
    item["type_name"] = nenrgy_type.name   #类型
    @form_body4.push(item)
  end
  # Rails.logger.info @form_body1.inspect
  # Rails.logger.info "------------------1------"
  # Rails.logger.info @form_body2.inspect
  # Rails.logger.info "------------------2------"
  # Rails.logger.info @form_body3.inspect
  # Rails.logger.info "------------------3------"
  # Rails.logger.info @form_body4.inspect
  # Rails.logger.info "------------------4------"
end


  def get_form_values
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    time = Time.now.strftime("%Y-%m-%d")
    #根据人员车间 找报表id
    @d_report_form = DReportForm.find_by(:s_region_code_id => @region_code.id)
    report_form_id = @d_report_form.id  #报表id
    @form_values = SNenrgyValue.where(:s_region_code_id => @region_code.id, :d_report_form_id => report_form_id, :datetime => time)
    #日报状态
    reports = @form_values.where(:status => 'Y') if @form_values.present?
    @status = reports.present? && reports.length >0 ? '已上报' : '未上报' 
    # Rails.logger.info '=========1111111111=========='
    # Rails.logger.info @form_values.inspect
  end


  def get_form_values2  #排污报表数据
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    #根据region_code id获取报表
    @d_report_forms1 = DReportForm.where(:s_region_code_id =>@region_code.id)  #表格信息  
    @d_report_forms = @d_report_forms1[1]  #获取第二个表格
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")  #报表类型id
    @region_code_ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_region_code_id).uniq  #去重处理
    time = Time.now.strftime("%Y-%m-%d")
    @form_values1 = SNenrgyValue.where(:s_region_code_id => '14', :d_report_form_id => '5', :datetime => time)
    @form_values2 = SNenrgyValue.where(:s_region_code_id => '5', :d_report_form_id => '5', :datetime => time)
    @form_values3 = SNenrgyValue.where(:s_region_code_id => '7', :d_report_form_id => '5', :datetime => time)
    @form_values4 = SNenrgyValue.where(:s_region_code_id => '15', :d_report_form_id => '5', :datetime => time)
    #日报状态
    reports1 = @form_values1.where(:status => 'Y') if @form_values1.present?
    @status = reports1.present? && reports1.length >0 ? '已上报' : '未上报' 
    # Rails.logger.info '=========1111111111=========='
    # Rails.logger.info @form_values.inspect
  end

  def index
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    #根据region_code id获取报表
    #@d_report_forms = DReportForm.where(:s_region_code_id =>@region_code.id)  #表格信息
    # @d_report_form_ids = @d_report_forms.pluck(:id)
    # @form_headers_ids = DReportFormHeader.where(:d_report_form_id => @d_report_form_ids).pluck(:d_form_header_id)   #获取表头信息id
    # @form_headers = DFormHeader.where(:id =>@form_headers_ids)
    #当天的 日报表
    # time = Time.now.strftime("%Y-%m-%d")
    # @daily_report = DDailyReport.where(:datetime => time, :s_region_code_id => @region_code.id)
  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    #@d_daily_report=DDailyReport.new
  end

  #成功跳转到show页面
  #保存 操作  还能修改

  # POST  energy_report
  def create
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    @d_daily_reports = params[:energy_params]
    status = params[:status]   #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:id]  #报表id
    @d_daily_reports.each do |d|
    #判断是否已经存在当天的日报
    @day_report = SNenrgyValue.find_by(:s_region_code_id => @region_code.id, :datetime => date, :field_code => d[1]["field_code"] )
    if @day_report.present?
      @day_report.update(:field_value => d[1]["field_value"].to_f, :status => status )
    else
      @d_daily_report = SNenrgyValue.new
      #@d_daily_report.name = d[1]["name"]
      @d_daily_report.field_value = d[1]["field_value"].to_f
      @d_daily_report.field_code = d[1]["field_code"]
      @d_daily_report.datetime = date
      @d_daily_report.status = status
      @d_daily_report.s_region_code_id = @region_code.id  #车间id
      @d_daily_report.d_report_form_id = d_report_form_id  #报表id
      @d_daily_report.save
    end
   end
    render json: {status: 'success'}
  end

  #上报   不能再修改
  def report
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    @d_daily_reports = params[:energy_params]
    status = params[:status]   #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:id]  #报表id
    @d_daily_reports.each do |d|
    #判断是否已经存在当天的日报
    @day_report = SNenrgyValue.find_by(:s_region_code_id => d[1]["region_id"], :datetime => date, :field_code => d[1]["field_code"] )
    if @day_report.present?
      @day_report.update(:field_value => d[1]["field_value"].to_f, :status => status )
   else
      @d_daily_report = SNenrgyValue.new
      @d_daily_report.field_value = d[1]["field_value"].to_f
      @d_daily_report.field_code = d[1]["field_code"]
      @d_daily_report.datetime = date
      @d_daily_report.status = status
      @d_daily_report.s_region_code_id = d[1]["region_id"]  #车间id
      @d_daily_report.d_report_form_id = d_report_form_id  #报表id
      @d_daily_report.save
   end
   end
    render json: {status: 'success'}
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    
  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
