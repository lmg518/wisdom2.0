class SearchReginCodesController < ApplicationController
  # before_action :set_s_area_login,only: [:update]

  def index
    @region_codes = SRegionCode.where(:region_code => params[:region_code])
  end

  # def update
  #   respond_to do |format|
  #     if @d_abnormal_data_yyyymm.update(d_abnormal_data_yyyymm_params)
  #       format.html { redirect_to @d_abnormal_data_yyyymm, notice: 'D abnormal data yyyymm was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @d_abnormal_data_yyyymm }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @d_abnormal_data_yyyymm.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  def create
    flsg = true
    @s_area_login = SAreaLogin.new(regin_params)
    if params[:d_station_id_arr].present?

      params[:d_station_id_arr].each do  |arr_id|

        flag =  SAreaLogin.save!(:d_station_id => params[:d_station_id],:d_login_msg_id => arr_id)

      end
    end

    respond_to do |format|
      if flsg
        format.json {render json: {location: @d_login_msg , status: :ok}}
      else
        format.json {render json: @d_login_msg.errors, status: :unprocessable_entity}
      end
    end
  end

  private
  # def set_s_area_login
  #   @s_area_login =
  # end

  def regin_params
    params.require(:region_code).permit(:s_region_code_id,:d_login_msg_id, :d_station_id)
  end

end