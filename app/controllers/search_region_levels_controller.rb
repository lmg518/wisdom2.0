class SearchRegionLevelsController < ApplicationController

  def index
      @s_region_levels = SRegionLevel.all
    render json: { region_levels: @s_region_levels }
  end

end