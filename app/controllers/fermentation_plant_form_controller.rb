class FermentationPlantFormController < ApplicationController
  include EnergyFormHelper
  before_action :get_fault_job, only: [:show, :update, :edit, :audit, :destory]
  before_action :authenticate_user

  #车间日生产报表  4个车间的日报表

  $flag = false #差值计算和不差值计算的 标识  true 差值计算   false 不差值计算

  def index
  end

  #获取折线图数据
  def get_datas_lines(time, region_code_ids)

    begin_day=Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")

    #折现图数据 不要分页
    @daily_reports1 = DDailyReport.where(:s_region_code_id => region_code_ids, :status => 'Y')
                          .by_datetime(begin_day, time) #按日期查询
                          .order(:created_at => :desc)
    @sum_dates = DDailyReport.by_datetime(begin_day, time).where(:s_region_code_id => region_code_ids, :status => 'Y')

    #车间项目 信息
    @ids = DMaterialReginCode.where(:s_region_code_id => region_code_ids)
               .pluck(:s_material_id)

    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    #获取当前月份
    @months=[]
    @months = @daily_reports1.pluck(:datetime).uniq.sort #折现图使用
    #查询当前车间  各个项目的 月数据
    #根据登录人员 获取人员所在的车间 或公司
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    #车间项目 信息
    @month_days=[]
    #获取最小查询日期
    @plants.each do |p|
      item={}
      item["name"] = p.name
      z_data=[] #折现数据
      @months.each do |m|
        num= DDailyReport.find_by(:datetime => m, :s_region_code_id => region_code_ids, :name => p.name)
        if num.present?
          nums = num.nums
        else
          nums = nil
        end
        z_data.push(nums) #不管当天有没有数据都填充一条记录
      end
      item["data"] = z_data
      @month_days.push(item)
    end
  end

  #差值计算方法1 计算多个项目的，参数有项目的
  def js_method_many(time, s_region_code_id, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

    @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => time, :field_code => item)
    @form_values_bf = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => bf_time, :field_code => item)

    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :field_code => item)
    min_time = @form_values_month.minimum("datetime") #获取最小日期
    max_time = @form_values_month.maximum("datetime") #获取最大日期
    @form_values_min = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => min_time, :field_code => item)
    @form_values_max = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => max_time, :field_code => item)

    d1 = 0
    d2 = 0
    @form_values.each do |m|
      d1 += m.field_value.to_f
    end
    @form_values_bf.each do |m|
      d2 += m.field_value.to_f
    end
    @steam_values = (d1 - d2).round(2) #日用量

    dd1 = 0
    dd2 = 0
    @form_values_max.each do |m|
      dd1 += m.field_value.to_f
    end
    @form_values_min.each do |m|
      dd2 += m.field_value.to_f
    end
    @steam_month_sum = (dd1 - dd2).round(2) #累计用量
  end


  #差值计算方法 2  计算单个项目的  item 项目参数名
  def js_method_one(time, s_region_code_id, d_report_form_id, item)
    @steam_values = 0
    @steam_month_sum = 0

    #计算差值
    bf_time = (Time.parse(time) + (-1.day)).strftime("%Y-%m-%d") #前天的日期
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期

    @form_values = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => time)
    @form_values_bf = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => bf_time)

    @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id)
    min_time = @form_values_month.minimum("datetime") #获取最小日期
    max_time = @form_values_month.maximum("datetime") #获取最大日期

    @form_values_min = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => min_time)
    @form_values_max = SNenrgyValue.find_by(:s_region_code_id => s_region_code_id, :field_code => item, :d_report_form_id => d_report_form_id, :datetime => max_time)

    if @form_values.present? && @form_values.field_value.present?
      d1 = @form_values.field_value.to_f
    else
      d1 = 0
    end

    if @form_values_bf.present? && @form_values_bf.field_value.present?
      d2 = @form_values_bf.field_value.to_f
    else
      d2 = 0
    end

    #添加判断
    if @form_values_min.present? && @form_values_min.field_value.present?
      min = @form_values_min.field_value.to_f
    else
      min = 0
    end
    if @form_values_max.present? && @form_values_max.field_value.present?
      max = @form_values_max.field_value.to_f
    else
      max = 0
    end
    @steam_month_sum = max -min

    @steam_values = d1 - d2 #昨天的 - 前天的
    #@steam_month_sum = @form_values_max.field_value.to_f - @form_values_min.field_value.to_f   #月累计用量
  end

  #计算公司总产量的方法
  def js_company_finished(before_time)
    #计算 公司 总产量 
    time = before_time
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    @company_day_total = 1 #日产量量      计算日单耗时  除数不能为0
    @company_month_total = 1 #累计用量

    @na_form_values = DDailyReport.find_by(:s_region_code_id => 8, :code => 'na_input', :datetime => time) # 8 烘包车间  na_input 钠入库
    @sour_form_values = DDailyReport.find_by(:s_region_code_id => 8, :code => 'sour_input', :datetime => time) # 8 烘包车间  sour_input 酸入库

    @na_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'na_input')
    @sour_form_values_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'sour_input')

    if @na_form_values.present? && @sour_form_values.present?
      @company_day_total = (@na_form_values.nums.to_f + @sour_form_values.nums.to_f / 0.73).round(2)
    end

    d1 = 0
    d2 = 0
    @na_form_values_month.each do |m|
      d1 += m.nums.to_f
    end
    @sour_form_values_month.each do |m|
      d2 += m.nums.to_f
    end
    @company_month_total = (d1 + d2 / 0.73).round(2) ##公司月累计产量
  end

  # 发酵车间生产报表
  def fermentation_workshop
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d") #查询昨天的数据
    end
    @time = before_time
    get_datas_fj(before_time)
    render :partial => "fermentation_workshop", :layout => false
  end

  # 烘包车间生产报表
  def bake_shop
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d") #查询昨天的数据
    end
    @time = before_time

    #计算公司总产量
    js_company_finished(before_time)

    get_datas_hb(before_time)
    render :partial => "bake_shop", :layout => false
  end

  # 合成车间生产报表
  def synthetic_workshop
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d") #查询昨天的数据
    end
    @time = before_time
    get_datas_hc(before_time)
    render :partial => "synthetic_workshop", :layout => false
  end

  # 精制车间生产报表
  def refining_workshop
    time = params[:time]
    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d") #查询昨天的数据
    end
    @time = before_time
    get_datas_jz(before_time)
    render :partial => "refining_workshop", :layout => false
  end


  #精制车间
  def get_datas_jz(before_time)
    form_code = params[:code] #表单code
    time = before_time

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code => form_code) #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : '' #表名


    #由于 湿成品钠、湿成品酸 在 烘包车间 s_region_code_id=8  、粗品 在合成车间s_region_code_id= 5   
    #@form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @form_values = DDailyReport.where(:s_region_code_id => [@d_report_forms.s_region_code_id, 8, 5], :datetime => time)

    @region_datas = []
    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)

    #精制车间 客户要看 湿成品钠、湿成品酸 -》 烘包车间  、粗品 --》合成车间
    # @ids.push(9)    #湿成品钠
    # @ids.push(11)   #湿成品酸
    # @ids.push(6)    #粗品

    # Rails.logger.info "-----2222222222222-----#{@ids}-----"
    @ids.push(9, 11, 6)
    #Rails.logger.info "-----2222222222222-----#{@ids}-----"


    @plants = SMaterial.where(:id => @ids) #从材料表中获取

    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => [@d_report_forms.s_region_code_id, 8, 5], :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计

      #质量信息
      data2 = []

      if 'YCL' == plant.code #一次料
        data2.push({:technic_01 => '批号', :technic_17 => '比重', :technic_18 => '料浆含量', :technic_19 => '加炭量', :technic_20 => '一次母液含量', :technic_21 => '溶解温度', })
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'ECL' == plant.code #二次料
        data2.push({:technic_01 => '批号', :technic_21 => '溶解温度', :technic_12 => 'PH', :technic_22 => '加EDTA量', :technic_23 => '加草酸量'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end


      #精制车间 客户要看 湿成品钠、湿成品酸、粗品   
      if 'SPCN' == plant.code #湿成品钠
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'SCPS' == plant.code #湿成品酸
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'CP' == plant.code #粗品
        data2.push({:technic_01 => '批号', :technic_13 => '含量', :technic_12 => 'PH', :technic_14 => '水份', :technic_15 => '料浆比重', :technic_16 => '母液含量', })
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas.push(item)
    end

    #-------------v2------
    #蒸汽  总收率  数据
    #计算精制车架总收率 = 一次料 * 二次料
    @daily_report_yc = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'YCL')
    @daily_report_ec = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'ECL')

    yc_yield= @daily_report_yc.present? ? @daily_report_yc.daily_yield : 0
    ec_yield= @daily_report_ec.present? ? @daily_report_ec.daily_yield : 0

    @total_yield = ((yc_yield.to_f * ec_yield.to_f) / 100).round(2) #精制车间总收率

    #精制车间日产量=钠包装量+酸包装量*0.73
    #精制车间 计算 日单耗 = 日蒸汽/ 精制车间日产量     月单耗 = 月蒸汽/ 精制车间月产量
    month_begin = Time.now.beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    @daily_report_na = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'na_nums') # na_nums  在 s_region_code_id = 8
    @daily_report_na_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'na_nums')
    @daily_report_suan = DDailyReport.find_by(:s_region_code_id => 8, :datetime => time, :code => 'suan_nums')
    @daily_report_suan_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 8, :code => 'suan_nums')

    na_day_nums = @daily_report_na.present? ? @daily_report_na.nums : 1 #钠日产量
    suan_day_nums = @daily_report_suan.present? ? @daily_report_suan.nums : 1 #酸日产量

    @nums_day_jz = na_day_nums + suan_day_nums * 0.73 #精制车间日产量

    dd1= 0
    dd2 =0
    @daily_report_na_month.each do |d1|
      dd1 += d1.nums.to_f
    end
    @daily_report_suan_month.each do |d1|
      dd2 += d1.nums.to_f
    end
    @nums_month_jz = dd1 + dd2 * 0.73 #精制车间 月累计产量

    #精制车间 蒸汽 表格数据
    @steam_datas = []
    steam_names = [{:name => '浓缩', :code => 'steam_6'},
                   {:name => '溶解', :code => 'steam_5'},
                   {:name => '合计', :code => 'total'},
                   {:name => '生产废水', :code => 'pollution_4'},
                   {:name => '降膜水', :code => 'pollution_5'},
                   {:name => '浓缩母液', :code => 'steam_11'} #新增 蒸汽报表中的  浓缩母液
    ]
    steam_names.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]

      #计算单个 项目 蒸汽的日用量  月累计


      #精制车间 报表 蒸汽数据  新增 浓缩母液
      if s[:code] == 'steam_11' #steam_11
        if $flag
          js_method_one(time, 7, 3, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 3, s[:code])
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        # item1[:day_expend]= (@steam_values.to_f / @nums_day_jz ).round(2)        #日单耗
        # item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2)       #月单耗

        item1[:day_expend]= '' #日单耗
        item1[:month_expend]= '' #月单耗
      end

      #-----------------------------
      if s[:code] == 'steam_6' #浓缩
        if $flag
          #js_method_one(time, s_region_code_id, d_report_form_id, item)
          js_method_one(time, 7, 3, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 3, s[:code])
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end

      if s[:code] == 'steam_5' #溶解

        if $flag
          js_method_one(time, 7, 3, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 3, s[:code])
        end

        item1[:steam_values]= @steam_values.round(2) #日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end

      if s[:code] == 'total' #合计
        item = ['steam_6', 'steam_5']
        if $flag
          js_method_many(time, 7, 3, item) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 7, 3, item)
        end

        item1[:steam_values]= @steam_values.round(2) #合计日用量
        item1[:steam_month_sum]= @steam_month_sum.round(2) #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end


      if s[:code] == 'pollution_4' #生产废水

        if $flag
          js_method_one(time, 7, 5, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end

      if s[:code] == 'pollution_5' #降膜水

        if $flag
          js_method_one(time, 7, 5, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 7, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @nums_day_jz).round(2) #日单耗
        item1[:month_expend]= (@steam_month_sum / @nums_month_jz).round(2) #月单耗
      end

      @steam_datas.push(item1)
    end


  end

  #合成车间
  def get_datas_hc(before_time)
    form_code = params[:code] #表单code

    time = before_time

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code => form_code) #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : '' #表名

    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.now.beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计

      #质量信息
      data2 = []

      if 'LJ' == plant.code #离交
        data2.push({:technic_01 => '批号', :technic_09 => '交料旋光', :technic_10 => '交料透光', :technic_11 => 'Ca2+', :technic_12 => 'PH', })
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'CP' == plant.code #粗品
        data2.push({:technic_01 => '批号', :technic_13 => '含量', :technic_12 => 'PH', :technic_14 => '水份', :technic_15 => '料浆比重', :technic_16 => '母液含量', })
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas.push(item)
    end

    #-------------------v2----------
    #合成车间 蒸汽  总收率  数据
    #合成车间 总收率= 离交*粗品*三效
    @daily_report_lj = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'LJ')
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'CP')
    @daily_report_sx = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'steam_1')

    lj_yield= @daily_report_lj.present? ? @daily_report_lj.daily_yield : 0
    cp_yield= @daily_report_cp.present? ? @daily_report_cp.daily_yield : 0
    sx_yield= @daily_report_sx.present? ? @daily_report_sx.daily_yield : 0

    @total_yield = ((lj_yield.to_f * cp_yield.to_f * sx_yield.to_f) / 10000).round(2) #合成车间 总收率

    #合成车间日产量= 粗品
    #合成车间 计算 日单耗 = 日蒸汽/ 合成车间日产量     月单耗 = 月蒸汽/ 合成车间月产量
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    @daily_report_cp = DDailyReport.find_by(:s_region_code_id => 5, :datetime => time, :code => 'CP') # CP  在 s_region_code_id = 5
    @daily_report_cp_month = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => 5, :code => 'CP')

    @cp_day_nums = @daily_report_cp.present? ? @daily_report_cp.nums : 1 #粗品日产量

    @cp_month_nums = 0 #粗品月累计
    if @daily_report_cp_month.present? && @daily_report_cp_month.length >0
      @daily_report_cp_month.each do |d1|
        @cp_month_nums += d1.nums.to_f
      end
    else
      @cp_month_nums = 1 #数据不存在是 默认值为 1  除数不能为0
    end

    #蒸汽 表格数据
    @steam_datas = []
    steam_names = [{:name => '酯转化', :code => 'steam_3'},
                   {:name => '三浓', :code => 'steam_2'},
                   {:name => '三效', :code => 'steam_1'},
                   {:name => '甲醇塔', :code => 'steam_4'},
                   {:name => '合计', :code => 'total'},
                   {:name => '洗柱水', :code => 'pollution_2'},
                   {:name => '中合水', :code => 'pollution_3'},
                   {:name => '生产废水', :code => 'pollution_4'},
                   {:name => '降膜水', :code => 'pollution_5'},
                   {:name => '甲醇废水', :code => 'pollution_6'},
                   {:name => '洗效水', :code => 'pollution_7'},
                   {:name => '稀甲醇', :code => 'steam_9'}, #客户新需求 将蒸汽报表中的 稀甲醇 、 蒸精甲醇 显示到车间报表中
                   {:name => '蒸精甲醇', :code => 'steam_10'}
    ]
    steam_names.each do |s|
      item1 ={}
      item1[:name]= s[:name]
      item1[:code]= s[:code]

      #合成车间 计算单个 项目 蒸汽的日用量  月累计

      #合成车间  蒸汽数据 添加稀甲醇 、 蒸精甲醇
      if s[:code] == 'steam_9' #稀甲醇   日单耗 = 稀甲醇 steam_9 / 合成车间 合计


        # if $flag
        #   js_method_one(time, 5, 3, s[:code])  #返回 @steam_values   @steam_month_sum 
        # else
        #   #不进行差值计算
        #   @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        # end


        if $flag
          js_method_one(time, 5, 3, 'steam_9')
          #返回  @steam_values 日的  @steam_month_sum 累计的
          d1= @steam_values
          d2 = @steam_month_sum
        else
          #不差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, 'steam_9')
          d1= @steam_values
          d2 = @steam_month_sum
        end

        item1[:day_expend] = (d1.to_f / @day_total_hc.to_f).round(2) #日单耗
        item1[:month_expend] = (d2.to_f / @month_total_hc.to_f).round(2) #月单耗


        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        # item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        # item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)   #合成车间月单耗
      end

      if s[:code] == 'steam_10' #蒸精甲醇
        if $flag
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        # item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        # item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)      #合成车间月单耗
        item1[:day_expend]= '' #合成车间日单耗
        item1[:month_expend]= '' #合成车间月单耗
      end


      #------------------
      if s[:code] == 'steam_3' #酯转化
        if $flag
          #js_method_one(time, s_region_code_id, d_report_form_id, item)
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'steam_2' #三浓
        if $flag
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'steam_1' #三效

        if $flag
          js_method_one(time, 5, 3, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        end

        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'steam_4' #甲醇塔 日单耗 = 甲醇塔 steam_4 / 蒸精甲醇 steam_10

        # if $flag
        #   js_method_one(time, 5, 3, s[:code])  #返回 @steam_values   @steam_month_sum 
        # else
        #   #不进行差值计算
        #   @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, s[:code])
        # end
        # item1[:steam_values]=  @steam_values           #日用量
        # item1[:steam_month_sum]= @steam_month_sum      #月用量
        # item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2)        #合成车间日单耗
        # item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2)      #合成车间月单耗


        if $flag
          js_method_one(time, 5, 3, 'steam_4')
          #返回  @steam_values 日的  @steam_month_sum 累计的
          d1= @steam_values
          d2 = @steam_month_sum
        else
          #不差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, 'steam_4')
          d1= @steam_values
          d2 = @steam_month_sum
        end

        if $flag
          js_method_one(time, 5, 3, 'steam_10')
          dd1 = @steam_values
          dd2 = @steam_month_sum
        else
          #不差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 3, 'steam_10')
          dd1 = @steam_values
          dd2 = @steam_month_sum
        end
        item1[:steam_values]= @steam_values #日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend] = (d1.to_f / dd1.to_f).round(2) #日单耗
        item1[:month_expend] = (d2.to_f / dd2.to_f).round(2) #月单耗

      end


      if s[:code] == 'total' #合计
        item = ['steam_1', 'steam_2', 'steam_3', 'steam_4']
        if $flag
          js_method_many(time, 5, 3, item) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_many_v2(time, 5, 3, item)
        end

        #计算合计
        @day_total_hc = @steam_values.round(2)
        @month_total_hc = @steam_month_sum.round(2)

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      #-----------------
      #排污 数据  
      if s[:code] == 'pollution_2' #洗柱水

        if $flag
          js_method_one(time, 5, 5, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'pollution_3' #中合水

        if $flag
          js_method_one(time, 5, 5, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'pollution_4' #生产废水

        if $flag
          js_method_one(time, 5, 5, s[:code]) #返回 @steam_values   @steam_month_sum

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'pollution_5' #降膜水

        if $flag
          js_method_one(time, 5, 5, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'pollution_6' #甲醇废水

        if $flag
          js_method_one(time, 5, 5, s[:code]) #返回 @steam_values   @steam_month_sum
          @day_steam = @steam_values

        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      if s[:code] == 'pollution_7' #洗效水

        if $flag
          js_method_one(time, 5, 5, s[:code]) #返回 @steam_values   @steam_month_sum
        else
          #不进行差值计算
          @steam_values, @steam_month_sum = EnergyFormHelper.js_method_one_v2(time, 5, 5, s[:code])
        end

        item1[:steam_values]= @steam_values #合计日用量
        item1[:steam_month_sum]= @steam_month_sum #月用量
        item1[:day_expend]= (@steam_values.to_f / @cp_day_nums).round(2) #合成车间日单耗
        item1[:month_expend]= (@steam_month_sum / @cp_month_nums).round(2) #合成车间月单耗
      end

      @steam_datas.push(item1)
    end


  end


  #烘包车间
  def get_datas_hb(before_time)
    form_code = params[:code] #表单code
    time = before_time
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code => form_code) #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : '' #表名

    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计

      #烘包车间 质量信息
      data2 = []

      if 'SPCN' == plant.code #湿成品钠
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'SCPS' == plant.code #湿成品酸
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_25 => '透光', :technic_14 => '水份'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'na_nums' == plant.code #钠包装量
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_26 => '2小时白度', :technic_27 => '透光度', :technic_14 => '水份', :technic_28 => '等级'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'suan_nums' == plant.code #酸包装量
        data2.push({:technic_01 => '批号', :technic_24 => '白度', :technic_26 => '2小时白度', :technic_27 => '透光度', :technic_14 => '水份', :technic_28 => '等级'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas.push(item)
    end

    #---------------v2-----------
    # 烘包车间 蒸汽  总收率  数据
    #日产量

    if $flag
      @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id_v1(time, @d_report_forms.s_region_code_id, 3)
    else
      #不进行差值计算
      @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id(time, @d_report_forms.s_region_code_id, 3)
    end

    #计算 日单耗  月单耗
    #烘包车间计算 日单耗 = 烘包蒸汽日用量 / 公司日产量    月单耗 = 烘包蒸汽月用量 / 公司月产量
    @day_expend = (@steam_values / @company_day_total.to_f).round(2) #烘包车间日单耗
    @month_expend = (@steam_month_sum / @company_month_total.to_f).round(2) #烘包车间月单耗
    @day_steam = @steam_values.round(2) #蒸汽日用量
    @steam_month_sum = @steam_month_sum.round(2) #蒸汽月用量


  end


  #发酵车间
  def get_datas_fj(before_time)
    form_code = params[:code] #表单code

    #time = Time.now.strftime("%Y-%m-%d")

    time = before_time

    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:code => form_code) #根据表格code 来找
    @form_name = @d_report_forms.present? ? @d_report_forms.name : '' #表名

    @form_values = DDailyReport.where(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time)
    @region_datas = []

    @ids = DMaterialReginCode.where(:s_region_code_id => @d_report_forms.s_region_code_id).pluck(:s_material_id)
    @plants = SMaterial.where(:id => @ids) #从材料表中获取
    @plants.each do |plant|
      item = {}
      item[:name] = plant.name
      item[:code] = plant.code

      field_value = @form_values.find_by(:code => plant.code)

      item[:nums] = field_value.present? ? field_value.nums : '' #日产值
      item[:daily_yield] = field_value.present? ? field_value.daily_yield : '' #日收率

      #计算月累计
      @month_sum = 0
      month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
      @month_values = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => plant.code)
      @month_values.each do |m|
        @month_sum += m.nums.to_f
      end
      item[:month_value] = @month_sum.round(2) #月累计

      #质量信息
      data2 = []

      if 'TF' == plant.code #头粉
        data2.push({:technic_01 => '批号', :technic_02 => '波美度', :technic_03 => '淀粉酶量'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'TY' == plant.code #糖液
        data2.push({:technic_01 => '批号', :technic_04 => '糖值', :technic_05 => 'DE值', :technic_06 => '周期'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'FJY' == plant.code #发酵液
        data2.push({:technic_01 => '批号', :technic_07 => '放罐旋光', :technic_08 => '滤速', :technic_06 => '周期'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      if 'SHY' == plant.code #酸化液
        data2.push({:technic_01 => '批号', :technic_09 => '交料旋光', :technic_10 => '交料透光', :technic_11 => 'Ca2+', :technic_12 => 'PH'})
        @s_teachnics= STechnic.where(:material_code => plant.code, :datetime => time)
        data2.push({:data => @s_teachnics})
      end

      item[:data] = data2
      @region_datas.push(item)
    end


    #--------v2-----------
    #发酵车间蒸汽  总收率  数据
    @daily_report_shy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'SHY')
    @daily_report_fjy = DDailyReport.find_by(:s_region_code_id => @d_report_forms.s_region_code_id, :datetime => time, :code => 'FJY')
    shy_yield= @daily_report_shy.present? ? @daily_report_shy.daily_yield : 0
    fjy_yield= @daily_report_fjy.present? ? @daily_report_fjy.daily_yield : 0
    @total_yield = ((shy_yield.to_f * fjy_yield.to_f) / 100).round(2) #发酵车间总收率

    #蒸汽 日产量
    if $flag
      @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id_v1(time, @d_report_forms.s_region_code_id, 3)
    else
      #不进行差值计算
      @steam_values, @steam_month_sum = EnergyFormHelper.js_method_by_time_region_id_form_id(time, @d_report_forms.s_region_code_id, 3)
    end

    #发酵车间计算 日单耗 月单耗
    @shy_nums= @daily_report_shy.present? ? @daily_report_shy.nums : 1 #酸化液的 日产量
    @shy_month_nums = 1 #酸化液的 月产量
    month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d") #当前月的开始日期
    @shy_months = DDailyReport.by_datetime(month_begin, time).where(:s_region_code_id => @d_report_forms.s_region_code_id, :code => 'SHY') #酸化液的 月累计

    if @shy_months.present? && @shy_months.length > 0
      @shy_months.each do |s|
        @shy_month_nums += s.nums.to_f
        @shy_month_nums -= 1
      end
    end

    @day_steam = @steam_values #蒸汽日用量
    @day_expend_fj = (@steam_values / @shy_nums.to_f).round(2) #发酵车间日单耗
    @month_expend_fj = (@steam_month_sum / @shy_month_nums.to_f).round(2) #发酵车间月单耗

  end


  #首页  FermentationPlantFormController.new.plant_form_index
  def plant_form_index
    #获取查询参数
    params_region_ids = params[:region_id]
    time = params[:time]

    if time.present?
      before_time = params[:time]
    else
      before_time = (Time.now + (-1.day)).strftime("%Y-%m-%d") #查询昨天的数据
    end

    date_time = before_time

    #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code1= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @region_code1= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司

    #判断 是 车间级 还是  分公司级
    if @region_code1.s_region_level_id == 1 #2 分公司   1 总公司   3 车间
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_z = SRegionCode.where(:s_region_level_id => 3)
        region_code_ids = region_z.pluck(:id) #查询分公司下的的所有车间
      end
    elsif @region_code1.s_region_level_id == 2
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        region_f = SRegionCode.where(:father_region_id => @region_code1.id, :s_region_level_id => 3)
        region_code_ids = region_f.pluck(:id) #查询分公司下的的所有车间

        report_forms = DReportForm.where(:s_region_code_id => region_code_ids)
        report_form_ids= report_forms.pluck(:id)
      end
    elsif @region_code1.s_region_level_id == 3
      if params_region_ids.present? #使用查询的ids
        region_code_ids = params_region_ids
      else
        @region_code = @region_code1
        region_code_ids = @region_code.id

        report_forms = DReportForm.where(:s_region_code_id => region_code_ids)
        report_form_ids= report_forms.pluck(:id)
      end
    end

    # Rails.logger.info "-region_code_ids----#{region_code_ids}-------"
    # Rails.logger.info "-report_form_ids----#{report_form_ids}-------"


    @region_datas = [] #存表名   日期
    #从车间 报表 数据 中查询
    @form_value_ids = DDailyReport.where(:s_region_code_id => region_code_ids, :datetime => date_time).pluck(:s_region_code_id).uniq
    #Rails.logger.info "-form_value_ids----#{@form_value_ids}-------"
    @energy_forms_values = DReportForm.where(:s_region_code_id => @form_value_ids) #今天有值的报表

    if @energy_forms_values.present? && @energy_forms_values.length >0
      @energy_forms_values.each do |form|
        item = {} #存人员 拥有的 报表
        item[:form_name] = form.name
        item[:code] = form.code
        item[:date_time] = date_time
        @region_datas.push(item)
      end
    end


    #封装折现图数据
    get_datas_lines(before_time, region_code_ids)
    #返回 @month_days 数据   @months 时间

    render :partial => "plant_form_index", :layout => false
  end


  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
  end

  #成功跳转到show页面
  # POST  steam_report
  def create
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work

  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
