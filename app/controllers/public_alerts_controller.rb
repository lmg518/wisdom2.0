class PublicAlertsController < ApplicationController

  def modal
    render :partial => 'public/public_alerts/modal', :layout => false
  end

end