class HandleStationRegionsController < ApplicationController
  before_action :get_station_region_params, only: [:show, :update]

  def index
    @region_arr = []
    @region_codes = SRegionCode.all
    @region_codes.each do |region|
      station_arr = region.d_stations.map {|x| {station_id: x.id, station_name: x.station_name}}
      @region_arr << {region_id: region.id, region_name: region.region_name, d_stations: station_arr}
    end
    @region_arr = Kaminari.paginate_array(@region_arr).page(params[:page]).per(params[:per])
  end

  def show
    render json: {region_code: @region_code.attributes.slice("id", "region_name"), d_stations: @region_code.d_stations.map {|x| {station_id: x.id, station_name: x.station_name}}}
  end

  def update
    @flag = false
    if params[:station_true_ids].present?
      @region_codes =  @region_code.station_region_code_ids
      @station_id_map = @region_codes.map{|x| x.d_station_id}
      station_arr = params[:station_true_ids]
      if @region_codes.present?
        puts @region_codes.destroy_all if station_arr.first == ""
        station_arr.each do |arr|
          if  @station_id_map.include?(arr.to_i)
            @region_codes.each_with_index do |region,index|
                @region_codes[index].delete unless station_arr.include?(region.d_station_id.to_s)
            end
          else
              @region_codes.create(:d_station_id => arr) if arr != ""
          end
          @flag = true
        end
      else
        station_arr.each do |arr|
          begin
            @region_codes.create(:d_station_id => arr)
            @flag = true
            rescue Exception => e
            @flag = false
          end
        end
      end
    end
    respond_to do |format|
      if @flag
        format.html {}
        format.json {render json: {location: {region_code: @region_code.attributes.slice("id", "region_name"),
                                              d_stations: @region_code.d_stations.map{|x| {station_id: x.id, station_name: x.station_name}}, status: :created} }}
      else
        format.html {}
        format.json {render json: {location: @region_code.errors, status: :unprocessable_entity}}
      end
    end
  end

  def create
  end

  private
  def get_station_region_params
    @region_code = SRegionCode.find(params[:id])
  end

  def set_station_region_params
    params.require(:region_station).permit(:d_station_id, :s_region_code_id, :created_at, :updated_at)
  end

end