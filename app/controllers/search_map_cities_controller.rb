class SearchMapCitiesController < ApplicationController
  before_action :current_user
  # 省份infuse province
  def index
    @zone_arr = []
    @alarm_zone_arr = []
    @alarm_level_1 = []
    @alarm_level_2 = []
    @alarm_level_3 = []
    @station_counts, @station_count_1, @station_count_2, @station_count_3 = 0,0,0,0
    region_code = @current_user.s_region_code
    @stations = region_code.d_stations.where(:s_administrative_area_id => SAdministrativeArea.find_by_zone_name(params[:province]).children.pluck(:id) )
    @stations.each do |station|
      alarm_arr =  station.d_alarms.by_today_data
      area_zone =  station.s_administrative_area
      @station_counts += 1  if alarm_arr.present?
      if alarm_arr.present?
        alarm_level_arr =  alarm_arr.map{|x|x.alarm_level.to_i}
        zone_parent = area_zone.parent
        @zone_np = zone_parent.zone_name
        @area_zone = area_zone.zone_name
        @zone_np = zone_parent.zone_name
          if  alarm_level_arr.include?(3)
            @alarm_level_3 << @area_zone
            @station_count_3 += 1
            next
          end
          if  alarm_level_arr.include?(2)
            @alarm_level_2 << @area_zone
            @station_count_2 += 1
            next
          end
          if  alarm_level_arr.include?(1)
            @alarm_level_1 << @area_zone
            @station_count_1 += 1
            # {alarm_level: alarm_arr.first.alarm_level, id: zone_parent.id,province_name: @zone_np}
            next
          end
      end
      # if area_zone.parent.present?
      #   zone_parent = area_zone.parent
      #   @zone_np = zone_parent.zone_name
      #   @area_zone = area_zone.zone_name
      #   @zone_arr << @zone_np
      #   if alarm_arr.present?
      #     alarm_level_arr =  alarm_arr.map{|x|x.alarm_level.to_i}
      #     if  alarm_level_arr.include?(3)
      #       @alarm_level_3 << @area_zone
      #       next
      #     end
      #     if  alarm_level_arr.include?(2)
      #       @alarm_level_2 << @area_zone
      #       next
      #     end
      #     if  alarm_level_arr.include?(1)
      #       @alarm_level_1 << @area_zone
      #       # {alarm_level: alarm_arr.first.alarm_level, id: zone_parent.id,province_name: @zone_np}
      #       next
      #     end
      #
      #   end
      # end
    end
    @zone_arr.uniq!
    @all_infos = {alarm_level: @station_count_3 != 0 ? 3 : @station_count_2 != 0  ? 2 : @station_count_1 != 0  ?  1 : 0,
                  alarm_province: @zone_np,station_counts: @station_counts}
    @alarm_level_infos = { first_level_province: @alarm_level_1.uniq, second_level_province: @alarm_level_2.uniq,
                           third_level_province: @alarm_level_3.uniq }

    render json: {all_infos: @all_infos,alarm_level_infos: @alarm_level_infos}
  end


end