class FactorsController < ApplicationController
  before_action :set_factor, only: [:show,:update]

  def index
    @s_abnormal_ltems = SAbnormalLtem.all
    render json: {abnormal_items: @s_abnormal_ltems}
  end

  def show
    render :json => {abnormal_item: @s_abnormal_ltem,abnormal_rules: @s_abnormal_ltem.s_abnormmal_rules}
  end

  # put  /factors/1.json
  def update
    respond_to do |format|
      if @s_abnormal_ltem.update(set_factors_params)
        format.html{}
        format.json{ render json: {status: :ok, location: @s_abnormal_ltem} }
      else
        format.html { }
        format.json { render json: @s_abnormal_ltem.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST  /factors.json
  def create
    @s_abnormal_ltem = SAbnormalLtem.new(set_factors_params)
    respond_to do |format|
     if @s_abnormal_ltem.sava
       format.html{}
       format.json{ render json: {status: :ok, location: @s_abnormal_ltem} }
     else
       format.html { }
       format.json { render json: @s_abnormal_ltem.errors, status: :unprocessable_entity }
     end
    end
  end

  private

  def set_factor
    @s_abnormal_ltem = SAbnormalLtem.find(params[:id])
  end

  def set_factors_params
    params.require(:s_abnormal_ltem).permit(:item_code,:item_name,:obj_type,:data_cycle ,:table_name ,
                                            :field_name ,:field_type ,:condition_claus)
  end


end