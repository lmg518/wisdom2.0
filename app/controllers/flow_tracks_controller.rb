class FlowTracksController < ApplicationController
  before_action :authenticate_user

  # login_id  yw_unit_id levels
  def index
    login_id = params[:login_id]        #运维人员id
    yw_unit_id = params[:yw_unit_id]    #运维单位id
    @logins = []

    if login_id.present?
      login = DLoginMsg.find_by(:id => login_id)

      #从签到表中获取运维人员的工单id
      # task_forms = login.d_task_forms.map {|x| {station_name: x.station_name} }
      #Rails.logger.info "--------#{DTaskForm.where(:handle_man_id => login_id).pluck(:id)}------"
      #Rails.logger.info "--------#{login.d_task_forms.pluck(:id)}------"

      @task_lines = DLogionTaskLine.where(:d_task_form_id => login.d_task_forms.pluck(:id))
                        .select{|x| x.long_login.present? }
                        .map {|x| {login_name: login.login_name, 
                                  telphone: login.telphone, 
                                  get_time: x.get_time.strftime("%Y-%m-%d %H:%M:%S"), 
                                  long_login: x.long_login, 
                                  lat_login: x.lat_login, 
                                  d_task_form_id: x.d_task_form_id,
                                  unit_name: SRegionCodeInfo.find(yw_unit_id).unit_name, 
                                  station_name: DTaskForm.find(x.d_task_form_id).present? ? DTaskForm.find(x.d_task_form_id).station_name : '',
                                  job_no: DTaskForm.find(x.d_task_form_id).job_no,
                                  title: DTaskForm.find(x.d_task_form_id).title}}
                                
      @logins << {login: {id: login.id, login_name: login.login_name}, data: @task_lines.present? ? @task_lines : []}
      #else
      #region_login = DLoginMsg.select("id", "login_name").by_yw.by_yw_unit(yw_unit_id)
      #region_login.each do |login|
      #从签到表中获取运维人员的工单id
      # task_form_ids=DLogionTaskLine.where(:login_name => login.login_name).pluck(:d_task_form_id)
      # @task_forms = DTaskForm.where(:id => task_form_ids).map {|x| {station_name: x.station_name}}
      #task_lines = DLogionTaskLine.where(:d_task_form_id => login.d_task_forms.pluck(:id)).map {|x| {login_name: login.login_name, get_time: x.get_time.strftime("%Y-%m-%d %H:%M:%S"), long_login: x.long_login, lat_login: x.lat_login, station_name: DTaskForm.find(x.d_task_form_id).present? ? DTaskForm.find(x.d_task_form_id).station_name : ''}}
      #@logins << {login: login, data: task_lines.present? ? task_lines : []}
      #end

    end

    return render json: {tracks: @logins}
  end

end