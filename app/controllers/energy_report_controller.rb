class EnergyReportController < ApplicationController
  before_action :authenticate_user
  #能源上报

  #生物循环水报表   
  def water_report
     get_datas
     get_form_values
    render :partial => "water_report", :layout => false
  end

  #用电上报
  def power_report
    #get_datas
    # get_values2
    render :partial => "power_report", :layout => false
  end


  #获取表格项目数据
  def get_datas
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id =>@region_code.id)  #表格信息  二个表时
    #表格信息
    @form_name = @d_report_forms.name  #表格名称
    @form_headers_ids = DReportFormHeader.where(:d_report_form_id =>@d_report_forms.id).pluck(:d_form_header_id)   #获取表头信息id
    Rails.logger.info @form_headers_ids.inspect
    @form_headers = []
    @form_headers_ids.each do |id|
      @fd = DFormHeader.find_by(:id =>id)
      name = @fd.present? ? @fd.name : ''
      @form_headers.push(name)
    end
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",")
    @form_type = SNenrgyFormType.where(:id => type_ids)  #报表类型数据
    @type_name = @form_type[0].name  #公司
    #封装项目信息
    @ids = DMaterialReginCode.where(:s_region_code_id => @region_code.id).pluck(:s_material_id)
    @form_body = SMaterial.where(:id => @ids)   #从材料表中获取
  end


  def get_form_values
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    time = Time.now.strftime("%Y-%m-%d")
    #根据人员车间 找报表id
    @d_report_form = DReportForm.find_by(:s_region_code_id => @region_code.id)
    report_form_id = @d_report_form.id  #报表id
    @form_values = SNenrgyValue.where(:s_region_code_id => @region_code.id, :d_report_form_id => report_form_id, :datetime => time)
    #日报状态
    reports = @form_values.where(:status => 'Y') if @form_values.present?
    @status = reports.present? && reports.length >0 ? '已上报' : '未上报' 
  end
  




  def index
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    #@d_daily_report=DDailyReport.new
  end

  #成功跳转到show页面
  #保存 操作  还能修改

  # POST  energy_report
  def create
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id)  #获取人员所在车间 或公司
    @d_daily_reports = params[:energy_params]
    status = params[:status]   #  Y 上报   N 保存
    date = params[:date_time]
    d_report_form_id = params[:id]  #报表id
    @d_daily_reports.each do |d|
    #判断是否已经存在当天的日报
    @day_report = SNenrgyValue.find_by(:s_region_code_id => @region_code.id, :datetime => date, :field_code => d[1]["field_code"] )
    if @day_report.present?
      @day_report.update(:field_value => d[1]["field_value"].to_f, :status => status )
    else
      @d_daily_report = SNenrgyValue.new
      #@d_daily_report.name = d[1]["name"]
      @d_daily_report.field_value = d[1]["field_value"].to_f
      @d_daily_report.field_code = d[1]["field_code"]
      @d_daily_report.datetime = date
      @d_daily_report.status = status
      @d_daily_report.s_region_code_id = @region_code.id  #车间id
      @d_daily_report.d_report_form_id = d_report_form_id  #报表id
      @d_daily_report.save
    end

  end
    render json: {status: 'success'}
  end

  #上报   不能再修改
  def report
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    # @d_daily_reports = params[:energy_params]
    # date = params[:date_time]
    # #date_time = date.strftime("%Y%m%d")
    # @d_daily_reports.each do |d|
    # Rails.logger.info "-------#{d}-------"
    # Rails.logger.info "-------#{d[0]}-------"
    # Rails.logger.info "-------#{d[1]}-------"
    #判断是否已经存在当天的日报
    # @day_report = SNenrgyValue.find_by(:s_region_code_id => @region_code.id, :datetime => date, :name => d[1]["name"] )
    # if @day_report.present?
    #   @day_report.update(:nums => d[1]["nums"].to_i)
    # else
    #   @d_daily_report = SNenrgyValue.new
    #   @d_daily_report.name = d[1]["name"]
    #   @d_daily_report.nums = d[1]["nums"].to_i
    #   @d_daily_report.s_region_code_id = @region_code.id
    #   @d_daily_report.datetime = date
    #   @d_daily_report.status = 'Y'
    #   @d_daily_report.save
    # end
    # render :index
    #end
end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    
  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
