class QualityReportController < ApplicationController
  before_action :authenticate_user

  #质量上报
  def quality_report
    #get_datas
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    get_form_values
    render :partial => "quality_report", :layout => false
  end

  #获取表格项目数据
  def get_datas
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
    #根据region_code id获取报表
    @d_report_forms = DReportForm.find_by(:s_region_code_id => @region_code.id) #表格信息
    #表格头信息
    @form_name = @d_report_forms.name #表格名称
    @form_headers_ids = DReportFormHeader.where(:d_report_form_id => @d_report_forms.id).pluck(:d_form_header_id) #获取表头信息id
    Rails.logger.info @form_headers_ids.inspect
    #@form_headers = DFormHeader.where(:id =>@form_headers_ids)  #表头信息  无法排序
    @form_headers = []
    @form_headers_ids.each do |id|
      @fd = DFormHeader.find_by(:id => id)
      name = @fd.present? ? @fd.name : ''
      @form_headers.push(name)
    end

    Rails.logger.info @form_headers.inspect
    type_ids=@d_report_forms.s_nenrgy_form_type_id.split(",") #报表类型id
    #@form_type = SNenrgyFormType.where(:id => type_ids)  #报表类型数据
    #@type_name = @form_type[0].name  #类型   蒸汽
    #封装项目信息

    #region_code_ids = SNenrgyFormRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_region_code_id)  #根据类型找车间id

    #部门 车间信息
    # @region_names = []
    # if region_code_ids.length >0
    #   region_code_ids.each do |id|
    #     @rg = SRegionCode.find_by(:id => id)
    #     name = @rg.present? ? @rg.region_name : ''
    #     @region_names.push(name)
    #   end
    # end
    # Rails.logger.info "---111---#{@type_name}--------"
    # Rails.logger.info "----222--#{@region_names}--------"
    @ids = DSteamRegionCode.where(:s_nenrgy_form_type_id => type_ids).pluck(:s_material_id) #根据类型id查找
    @form_body1 = SMaterial.where(:id => @ids) #从材料表中获取
    @form_body=[]
    @form_body1.each do |f|
      item ={}
      item["code"] = f.code
      item["name"] = f.name
      steam_region_code = DSteamRegionCode.find_by(:s_material_id => f.id) #蒸汽项目与车间 类型的关联
      region_code_id = steam_region_code.s_region_code_id
      region_code = SRegionCode.find_by(:id => region_code_id)
      item["region_name"] = region_code.region_name #车间信息
      nenrgy_type_id = steam_region_code.s_nenrgy_form_type_id #表格类型id
      nenrgy_type = SNenrgyFormType.find_by(:id => nenrgy_type_id)
      item["type_name"] = nenrgy_type.name #类型
      @form_body.push(item)
    end
    # Rails.logger.info "------@form_body------#{@form_body}-------"
    # Rails.logger.info @form_body.inspect
    # Rails.logger.info "------@form_type------#{@form_type}-------"
    # Rails.logger.info "------@d_report_forms------#{@d_report_forms}-------"
    # Rails.logger.info "------@d_report_forms------#{@type_name}-------"
  end


  def get_form_values
    # #根据登录人员 获取人员所在的车间 或公司
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    # #根据人员车间 找报表id
    # @d_report_form = DReportForm.find_by(:s_region_code_id => @region_code.id)
    # report_form_id = @d_report_form.id  #报表id
    time = Time.now.strftime("%Y-%m-%d")
    @form_values1 = STechnic.where(:material_code => 'TF', :datetime => time) #头粉的数据
    @form_values2 = STechnic.where(:material_code => 'TY', :datetime => time) #糖液
    @form_values3 = STechnic.where(:material_code => 'FJY', :datetime => time) #'FJY'  #发酵液
    @form_values4 = STechnic.where(:material_code => 'SHY', :datetime => time) #'SHY'  #酸化液
    @form_values5 = STechnic.where(:material_code => 'LJ', :datetime => time) #'LJ'  #离交
    @form_values6 = STechnic.where(:material_code => 'CP', :datetime => time) #'CP'  #粗品
    @form_values7 = STechnic.where(:material_code => 'YCL', :datetime => time) #'YCL'  #一次料
    @form_values8 = STechnic.where(:material_code => 'ECL', :datetime => time) #'ECL'  #二次料
    @form_values9 = STechnic.where(:material_code => 'SPCN', :datetime => time) #'SPCN'  #湿品成钠
    @form_values10 = STechnic.where(:material_code => 'SCPS', :datetime => time) #'SCPS'  #湿成品酸
    @form_values11 = STechnic.where(:material_code => 'na_nums', :datetime => time) #'na_nums'  #钠包装数量
    @form_values12 = STechnic.where(:material_code => 'suan_nums', :datetime => time) #'suan_nums'  #钠包装数量
    Rails.logger.info "---#{@form_values1}--"
    Rails.logger.info @form_values1.inspect
    #日报状态
    if @region_code.region_code == 'TYSY_QUALITY' #质量部门上报  不包括 钠包装数量、钠包装数量
      reports = @form_values1.where(:status => 'Y') if @form_values1.present?
      @status = reports.present? && reports.length >0 ? '已上报' : '未上报'
    end
    if @region_code.region_code == 'TYSY_QCENTER' #质检中心上报  只有： 钠包装数量、钠包装数量
      reports = @form_values11.where(:status => 'Y') if @form_values11.present?
      @status = reports.present? && reports.length >0 ? '已上报' : '未上报'
    end
  end


  def index
    #根据登录人员 获取人员所在的车间 或公司
    #region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    @region_code= SRegionCode.find_by(:id => @current_user.group_id) #获取人员所在车间 或公司
  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    #@d_daily_report=DDailyReport.new
  end

  #成功跳转到show页面
  #保存 操作  还能修改

  # POST  quality_report
  def create

    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司
    @d_daily_reports = params[:quality_params]
    status = params[:status] #  Y 上报   N 保存
    datetime = params[:date_time]
    #d_report_form_id = params[:id]  #报表id
    # Rails.logger.info "----1---#{@d_daily_reports}-------"
    # Rails.logger.info "----1---#{status}-------"
    # Rails.logger.info "----1---#{datetime}-------"
    @d_daily_reports.each do |d|
      # d1  "field_code"=>"TF", "data"=>{"0"=>{"technic_01"=>"", "technic_02"=>"", "technic_03"=>""}, "1"=>{"technic_01"=>"", "technic_02"=>"", "technic_03"=>""}, "2"=>{"technic_01"=>"", "technic_02"=>"", "technic_03"=>""}}}-------
      #----222---{"technic_01"=>"", "technic_02"=>"", "technic_03"=>""

      #根据车间code 找到对于的车间
      s_material_code = d[1]["field_code"] #项目code
      if s_material_code == 'TF' #头粉
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        Rails.logger.info teachnics.inspect
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_02 = d1["technic_02"]
          technic_03 = d1["technic_03"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_02 => technic_02, :technic_03 => technic_03, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          #重新保存数据
          if technic_01 != '' && technic_02 != '' && technic_03 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_02 = technic_03
            @s_technic.technic_03 = technic_03
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'TY' #糖液
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_04 = d1["technic_04"]
          technic_05 = d1["technic_05"]
          technic_06 = d1["technic_06"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_04 => technic_04, :technic_05 => technic_05, :technic_06 => technic_06, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_04 != '' && technic_05 != '' && technic_06 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_04 = technic_04
            @s_technic.technic_05 = technic_05
            @s_technic.technic_06 = technic_06
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'FJY' #发酵液
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_07 = d1["technic_07"]
          technic_08 = d1["technic_08"]
          technic_06 = d1["technic_06"]
          #判断是否存过
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_07 => technic_07, :technic_08 => technic_08, :technic_06 => technic_06, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_07 != '' && technic_08 != '' && technic_06 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_07 = technic_07
            @s_technic.technic_08 = technic_08
            @s_technic.technic_06 = technic_06
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'SHY' #酸化液
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_09 = d1["technic_09"]
          technic_10 = d1["technic_10"]
          technic_11 = d1["technic_11"]
          technic_12 = d1["technic_12"]
          #判断是否存过
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_09 => technic_09, :technic_10 => technic_10, :technic_11 => technic_11, :technic_12 => technic_12, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_09 != '' && technic_10 != '' && technic_11 != '' && technic_12 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_09 = technic_09
            @s_technic.technic_10 = technic_10
            @s_technic.technic_11 = technic_11
            @s_technic.technic_12 = technic_12
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'LJ' #离交
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_09 = d1["technic_09"]
          technic_10 = d1["technic_10"]
          technic_11 = d1["technic_11"]
          technic_12 = d1["technic_12"]
          #判断是否存过
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_09 => technic_09, :technic_10 => technic_10, :technic_11 => technic_11, :technic_12 => technic_12, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_09 != '' && technic_10 != '' && technic_11 != '' && technic_12 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_09 = technic_09
            @s_technic.technic_10 = technic_10
            @s_technic.technic_11 = technic_11
            @s_technic.technic_12 = technic_12
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'CP' #粗品
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_13 = d1["technic_13"]
          technic_12 = d1["technic_12"]
          technic_14 = d1["technic_14"]
          technic_15 = d1["technic_15"]
          technic_16 = d1["technic_16"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_13 => technic_13, :technic_12 => technic_12, :technic_14 => technic_14, :technic_15 => technic_15, :technic_16 => technic_16, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_13 != '' && technic_12 != '' && technic_14 != '' && technic_15 != '' && technic_16 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_13 = technic_13
            @s_technic.technic_12 = technic_12
            @s_technic.technic_14 = technic_14
            @s_technic.technic_15 = technic_15
            @s_technic.technic_16 = technic_16
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'YCL' #一次料
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_17 = d1["technic_17"]
          technic_18 = d1["technic_18"]
          technic_19 = d1["technic_19"]
          technic_20 = d1["technic_20"]
          technic_21 = d1["technic_21"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_17 => technic_17, :technic_18 => technic_18, :technic_19 => technic_19, :technic_20 => technic_20, :technic_21 => technic_21, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_17 != '' && technic_18 != '' && technic_19 != '' && technic_20 != '' && technic_21 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_17 = technic_17
            @s_technic.technic_18 = technic_18
            @s_technic.technic_19 = technic_19
            @s_technic.technic_20 = technic_20
            @s_technic.technic_21 = technic_21
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'ECL' #二次料
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_21 = d1["technic_21"]
          technic_12 = d1["technic_12"]
          technic_22 = d1["technic_22"]
          technic_23 = d1["technic_23"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_21 => technic_21, :technic_12 => technic_12, :technic_22 => technic_22, :technic_23 => technic_23, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_21 != '' && technic_12 != '' && technic_22 != '' && technic_23 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_21 = technic_21
            @s_technic.technic_12 = technic_12
            @s_technic.technic_22 = technic_22
            @s_technic.technic_23 = technic_23
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'SPCN' #湿品成钠
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_24 = d1["technic_24"]
          technic_25 = d1["technic_25"]
          technic_14 = d1["technic_14"]
          #判断是否存过
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_24 => technic_24, :technic_25 => technic_25, :technic_14 => technic_14, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_24 != '' && technic_25 != '' && technic_14 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_24 = technic_24
            @s_technic.technic_25 = technic_25
            @s_technic.technic_14 = technic_14
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'SCPS' #湿成品酸
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_24 = d1["technic_24"]
          technic_27 = d1["technic_27"]
          technic_14 = d1["technic_14"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_24 => technic_24, :technic_27 => technic_27, :technic_14 => technic_14, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_24 != '' && technic_27 != '' && technic_14 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_24 = technic_24
            @s_technic.technic_27 = technic_27
            @s_technic.technic_14 = technic_14
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'na_nums' #钠包装数量
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_24 = d1["technic_24"]
          technic_26 = d1["technic_26"]
          technic_27 = d1["technic_27"]
          technic_14 = d1["technic_14"]
          technic_28 = d1["technic_28"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_24 => technic_24, :technic_26 => technic_26, :technic_27 => technic_27, :technic_14 => technic_14, :technic_28 => technic_28, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_24 != '' && technic_26 != '' && technic_27 != '' && technic_14 != '' && technic_28 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_24 = technic_24
            @s_technic.technic_26 = technic_26
            @s_technic.technic_27 = technic_27
            @s_technic.technic_14 = technic_14
            @s_technic.technic_28 = technic_28
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

      if s_material_code == 'suan_nums' #酸包装数量
        @plants = d[1]["data"] #车间下的项目  值
        #判断是否存过
        teachnics = STechnic.where(:material_code => s_material_code, :datetime => datetime)
        @plants.each_with_index do |item, index|
          d1 = item[1]
          technic_01 = d1["technic_01"]
          technic_24 = d1["technic_24"]
          technic_26 = d1["technic_26"]
          technic_27 = d1["technic_27"]
          technic_14 = d1["technic_14"]
          technic_28 = d1["technic_28"]
          if teachnics.present? && teachnics.length > 0
            #teachnics[index].update(:technic_01 => technic_01, :technic_24 => technic_24, :technic_26 => technic_26, :technic_27 => technic_27, :technic_14 => technic_14, :technic_28 => technic_28, :status => status)
            #先删除原来的数据
            teachnics.each do |t|
              t.destroy
            end
          end
          if technic_01 != '' && technic_24 != '' && technic_26 != '' && technic_27 != '' && technic_14 != '' && technic_28 != ''
            @s_technic = STechnic.new
            @s_technic.technic_01 = technic_01
            @s_technic.technic_24 = technic_24
            @s_technic.technic_26 = technic_26
            @s_technic.technic_27 = technic_27
            @s_technic.technic_14 = technic_14
            @s_technic.technic_28 = technic_28
            @s_technic.status = status
            @s_technic.datetime = datetime
            @s_technic.material_code = s_material_code #项目code
            @s_technic.save
          end
        end
      end

    end
    render json: {status: 'success'}
  end

  #上报   不能再修改
  def report
    # region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
    # @region_code= SRegionCode.find_by(:id => region_code_info.s_region_code_id)  #获取人员所在车间 或公司


    # @d_daily_reports = params[:energy_params]
    # date = params[:date_time]
    # #date_time = date.strftime("%Y%m%d")

    # @d_daily_reports.each do |d|

    # Rails.logger.info "-------#{d}-------"
    # Rails.logger.info "-------#{d[0]}-------"
    # Rails.logger.info "-------#{d[1]}-------"

    #判断是否已经存在当天的日报
    # @day_report = SNenrgyValue.find_by(:s_region_code_id => @region_code.id, :datetime => date, :name => d[1]["name"] )
    # if @day_report.present?
    #   @day_report.update(:nums => d[1]["nums"].to_i)
    # else
    #   @d_daily_report = SNenrgyValue.new
    #   @d_daily_report.name = d[1]["name"]
    #   @d_daily_report.nums = d[1]["nums"].to_i
    #   @d_daily_report.s_region_code_id = @region_code.id
    #   @d_daily_report.datetime = date
    #   @d_daily_report.status = 'Y'
    #   @d_daily_report.save
    # end
    # render :index
    #end
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @d_daily_report.destroy
        format.html {}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @d_daily_report.update(work_params)
        format.json {render json: {status: 'success', location: @d_daily_report}}
        format.html {redirect_to work_pro_path(@d_daily_report), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @d_daily_report.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work

  end

  def work_params
    params.require(:d_daily_reports).permit(:id, :nums, :name)
  end

end
