class SearchRegionCodesController < ApplicationController
  before_action :authenticate_user


  #  search_region_codes.json
  def index
   Rails.logger.info "-----current_user-----#{@current_user.s_region_code_info_id}-----"
  #根据登录人员 获取人员所在的车间 或公司
  # @region_code_info = SRegionCodeInfo.find_by(:id => @current_user.s_region_code_info_id)  #获取人员所在单位
  # @region_code1= SRegionCode.find_by(:id => @region_code_info.s_region_code_id)  #获取人员所在车间 或公司
  @region_code1= SRegionCode.find_by(:id => current_user.group_id)  #获取人员所在车间 或公司

  # Rails.logger.info "-----@region_code_info-----#{@region_code_info}-----"
  # Rails.logger.info "-----@region_code1-----#{@region_code1}-----"

    @unit1 = []
    region_code = SRegionCode.where(:father_region_id => 99999)
    region_code.each do |region|

      item={}
      item["id"] = region.id
      item["name"] = region.region_name   #总公司级
        date1=[]

        #判断 是 车间级 还是  分公司级
        if @region_code1.s_region_level_id == 1   #2 分公司   1 总公司   3 车间
          two = SRegionCode.where(:father_region_id => region.id)
        elsif @region_code1.s_region_level_id == 2
          two = SRegionCode.where(:id => @region_code1.id)
        elsif @region_code1.s_region_level_id == 3
          two = SRegionCode.where(:id => @region_code1.father_region_id)  #车间所在的分公司
        end

        two.each do |t|
          item2={}
          item2["id"] = t.id
          item2["name"] = t.region_name  #分公司级

          date3 = []

          if @region_code1.s_region_level_id == 3
            three = SRegionCode.where(:id => @region_code1.id, :s_region_level_id => 3)
          else
            three = SRegionCode.where(:father_region_id => t.id, :s_region_level_id => 3)
          end

          three.each do |th|
            item3={}
            item3["id"] = th.id
            item3["name"] = th.region_name  #车间级
            date3.push(item3)
          end
          
          item2["chil_data"] = date3  #分公司下的车间
          date1.push(item2)
        end

      item["chil_data"] = date1  #总公司下的分公司

      @unit1.push(item)
    end


    #region_code = SRegionCode.where(:father_region_id => 99999)
    # region_code.each do |region|
    #   if region.is_leaf == "Y"
    #     @unit2 = []
    #     two = SRegionCode.where(:father_region_id => region.id)
    #     next if two.blank?
    #     two.each do |t_rg|
    #       if t_rg.is_leaf == "Y"
    #         @unit3 = []
    #         there = SRegionCode.where(:father_region_id => t_rg.id)
    #          next if there.blank?
    #         there.each do |th_rg|
    #           @unit3 << {id: th_rg.id,name: th_rg.region_name,level: 3, units: th_rg.s_region_code_infos.present? ? th_rg.s_region_code_infos.select("id","unit_name") : []}
    #         end
    #         @unit2 << {id: t_rg.id,name: t_rg.region_name, chil_data: @unit3 ,level: 2, units: t_rg.s_region_code_infos.present? ? t_rg.s_region_code_infos.select("id","unit_name") : []}
    #       else
    #         @unit2 << {id: t_rg.id,name: t_rg.region_name, chil_data: [],level: 2, units: t_rg.s_region_code_infos.present? ? t_rg.s_region_code_infos.select("id","unit_name") : [] }
    #       end
    #     end
    #     @unit1 << {id: region.id,name: region.region_name,chil_data: @unit2,level: 1 , units: region.s_region_code_infos.present? ? region.s_region_code_infos.select("id","unit_name") : []}
    #   else
    #     @unit1 << {id: region.id,name: region.region_name,chil_data: [],level: 1, units: region.s_region_code_infos.present? ? region.s_region_code_infos.select("id","unit_name") : []}
    #   end
    # end


    #Rails.logger.info "-------@unit1--#{@unit1}------"
   render json: {unit_region: @unit1}
  end

end