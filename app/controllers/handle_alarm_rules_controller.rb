class HandleAlarmRulesController < ApplicationController

  def create
    @s_notice_rule = SNoticeRule.new(get_alarm_rule_params)
    respond_to do |format|
      if @s_notice_rule.save!
        format.html {}
        format.json {render json: {status: :ok, location: @s_notice_rule}}
      else
        format.html {}
        format.json {render json: {status: :unprocessable_entity, location: @s_notice_rule.errors}}
      end
    end
  end

  private

  def set_alarm_rule
    @s_notice_rule = SNoticeRule.find(:id)
  end

  def get_alarm_rule_params
     params.require(:s_notice_rule).permit(:alarm_level, :notice_type, :role_id, :up_region_level)
  end

end