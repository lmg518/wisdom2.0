class FileCategoriesController < ApplicationController

  def index
    @categories = FileCategory.all
  end

  def update
    @category = FileCategory.find(params[:id])
    respond_to do |format|
      if @category.update(params_file_category)
        format.html {}
        format.json {render json: {status: :ok, location: @category}}
      else
        format.html {}
        format.json {render json: {status: :unprocessable_entity, location: @category.errors}}
      end
    end
  end

  def create
    @category = FileCategory.new(params_file_category)
    respond_to do |format|
      if @category.save
        format.html {}
        format.json {render json: {status: :ok, location: @category}}
      else
        format.html {}
        format.json {render json: {status: :unprocessable_entity, location: @category.errors}}
      end
    end
  end

  private

  def params_file_category
    params.require(:file_category).permit(:category_name, :parent_id)
  end

end