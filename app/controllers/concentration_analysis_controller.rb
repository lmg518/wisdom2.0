class ConcentrationAnalysisController < ApplicationController
  def index
  end

  #同比增长率=（本年的指标值-去年同期的值）÷去年同期的值*100%
  #环比增长率=(本次数据 - 上次数据) ／ 上次数据 * 100
  def init_index
    search_parmas
    handle_table
  end

  def func_bj(current_num, pre_num)
    return "0.00" if pre_num == 0
    (((current_num.to_f - pre_num.to_f) / pre_num.to_f) * 100).round(2)
  end

  def export_file
    @search_status = true
    export_file_params
    handle_table
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ['站点名称', "", "", "当期", "", "", "", "", "", "", "", "", "环比", "", "", "", "", "", "", "", "", "", "", "", "同比", "", "", "", "", "", ""]
      csv << ['', "", "", "空气质量", "", "", "", "", "", "空气质量", "", "", "", "", "", "比较(%)", "", "", "", "", "", "空气质量", "", "", "", "", "", "比较(%)", "", "", ""]
      csv << ['', "PM10", "SO2", "NO2", "CO", "O3", "PM2.5", "PM10", "SO2", "NO2", "CO", "O3", "PM2.5", "PM10", "SO2", "NO2", "CO", "O3", "PM2.5", "PM10", "SO2", "NO2", "CO", "O3", "PM2.5", "PM10", "SO2", "NO2", "CO", "O3", "PM2.5",]
      @all_arr = []
      @yy_h.each do |yy|
        station = yy[:station]
        current_item = yy[:current]
        pre_data = yy[:pre_data]
        pre_year_data = yy[:pre_year_data]
        csv << [station[:station_name], current_item[:pm10_avg], current_item[:so2_avg], current_item[:no2_avg], current_item[:co_avg], current_item[:o3_avg], current_item[:pm2_5_avg],
                pre_data[:pm10_avg], pre_data[:so2_avg], pre_data[:no2_avg], pre_data[:co_avg], pre_data[:o3_avg], pre_data[:pm2_5_avg],
                func_bj(current_item[:pm10_avg],pre_data[:pm10_avg]),func_bj(current_item[:so2_avg],pre_data[:so2_avg]),
                func_bj(current_item[:no2_avg],pre_data[:no2_avg]),func_bj(current_item[:co_avg],pre_data[:co_avg]),
                func_bj(current_item[:o3_avg],pre_data[:o3_avg]),func_bj(current_item[:pm2_5_avg],pre_data[:pm2_5_avg]),
                pre_year_data[:pm10_avg], pre_year_data[:so2_avg], pre_year_data[:no2_avg], pre_year_data[:co_avg], pre_year_data[:o3_avg], pre_year_data[:pm2_5_avg],
                func_bj(current_item[:pm10_avg],pre_year_data[:pm10_avg]),func_bj(current_item[:so2_avg],pre_year_data[:no2_avg]),
                func_bj(current_item[:no2_avg],pre_year_data[:no2_avg]),func_bj(current_item[:co_avg],pre_year_data[:co_avg]),
                func_bj(current_item[:o3_avg],pre_year_data[:o3_avg]),func_bj(current_item[:pm2_5_avg],pre_year_data[:pm2_5_avg])
        ]

      end
    end

    if @data_times.present?
      @name = Time.parse(@data_times).strftime("%Y-%m-%d-") + "浓度值对比分析"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "浓度值对比分析"
    end
    respond_to do |format|
      format.csv {
        send_data @scv,
                  :type => 'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private

  def export_file_params
    @station_id = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times]
    @grading_analysis = params[:grading_analysis]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @station_id = @search[:station_ids]
      @data_times = @search[:data_times]
      @grading_analysis = @search[:grading_analysis]
    end
  end

  def handle_table
    @region_leves = SRegionLevel.select(:id, :level_name)
    @yy_h = []
    @stations = DStation.by_station_ids(@station_id)
    @stations = @stations.page(params[:page]).per(10) unless @search_status
    @stations.each do |station|
      @now_item_avgs = station.station_item_avgs.by_get_time(@data_times)
      @now_items = func_item_avg(@now_item_avgs, station, @now_item_avgs.length)
      @pre_item_avgs = station.station_item_avgs.by_pre_time(@data_times)
      @pre_item = func_item_avg(@pre_item_avgs, station, @pre_item_avgs.length)
      @pre_year_data = station.station_item_avgs.by_pre_year_time(@data_times)
      @pre_year = func_item_avg(@pre_year_data, station, @pre_year_data.length)
      @yy_h << {station: {id: station.id,station_name: station.station_name},
                current: @now_items,
                pre_data: @pre_item,
                pre_year_data: @pre_year}

      #---------old
      # @now_item_avgs = StationItemAvg.by_d_station_id(@station_id).by_get_time(@data_times)
      # @now_item_avgs = @now_item_avgs.page(params[:page]).per(10) unless @search_status
      # @now_item_avgs.each do |item_avg|
      #   pre_time = (item_avg.day_time - 1.days)
      #   pre_year_time = (item_avg.day_time - 1.years)
      #   pre_data = StationItemAvg.find_by(:d_station_id => item_avg.d_station_id,
      #                                     :day_time => (pre_time.beginning_of_day)..(pre_time.end_of_day))
      #   pre_year_data = StationItemAvg.find_by(:d_station_id => item_avg.d_station_id,
      #                                          :day_time => (pre_year_time.beginning_of_day)..(pre_year_time.end_of_day))
      #   @yy_h << {current: arr_info(item_avg), pre_data: arr_info(pre_data), pre_year_data: arr_info(pre_year_data)}
      # end
      # ---- old end
    end
  end

  def func_item_avg(objs, station, num)
    return { pm10_avg: 0, so2_avg: 0, no2_avg: 0, co_avg: 0, o3_avg: 0, pm2_5_avg: 0} if objs.blank?
    num = num.to_f
    {pm10_avg: (objs.pluck(:pm10_avg).sum / num).round(2),
     so2_avg: (objs.pluck(:so2_avg).sum / num).round(2),
     no2_avg: (objs.pluck(:no2_avg).sum / num).round(2),
     co_avg: (objs.pluck(:co_avg).sum / num).round(2),
     o3_avg: (objs.pluck(:o3_avg).sum / num).round(2),
     pm2_5_avg: (objs.pluck(:pm2_5_avg).sum / num).round(2)}
  end

  def arr_info(item_avg)
    return {id: 0, station_name: '', pm10_avg: 0, so2_avg: 0, no2_avg: 0, co_avg: 0, o3_avg: 0, pm2_5_avg: 0} if item_avg.blank?
    {id: item_avg.id,
     station_id: item_avg.d_station_id,
     station_name: item_avg.station_name,
     pm10_avg: item_avg.pm10_avg,
     so2_avg: item_avg.so2_avg,
     no2_avg: item_avg.no2_avg,
     co_avg: item_avg.co_avg,
     o3_avg: item_avg.o3_avg,
     pm2_5_avg: item_avg.pm2_5_avg}
  end

end
