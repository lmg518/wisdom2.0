class OpsAuditController < ApplicationController
  before_action :set_ops_audit, only: [:show, :edit, :update, :review]
  before_action :authenticate_user # 转存公共参数

  def index
    @ops_audits = DTaskForm.order(:created_at => :desc).page(params[:page]).per(params[:per])
    #按条件查询
    @station_ids = params[:d_station_id] #站点id
    @job_no = params[:job_no]   #工单号
    @author = params[:author]   #创建人
    @created_time = params[:created_time].to_s + " 00:00:00" if params[:created_time].present? #创建起始时间  
    @end_time = params[:end_time].to_s + " 23:59:59" if params[:end_time].present?             #创建结束时间 
    @handle_man = params[:handle_man]    #当前处理人
    @create_type = params[:create_type]  #创建类型
    @job_status = params[:job_status]    #工单状态
    @fault_type = params[:fault_type]                  #工单类型
    @polling_ops_type = params[:polling_ops_type]      #任务周期
    @ops_audits = DTaskForm.by_job_no(@job_no)
                            .by_author(@author)
                            .by_time(@created_time,@end_time)
                            .by_station_id(@station_ids)
                            .by_handle_man(@handle_man)
                            .by_create_type(@create_type)
                            .by_job_status(@job_status)
                            .by_fault_type(@fault_type)
                            .by_polling_ops_type(@polling_ops_type) 
                            .order(:created_at => :desc)
                            .page(params[:page]).per(params[:per])

  end

  def show
  end

  def edit
  end

  #批量审核工单
  def batch_audit
    @batch_audit_ids = params[:batch_audit_ids] #获取要批量审核的工单的 id
    @batch_audits=DTaskForm.where(:id => @batch_audit_ids) if @batch_audit_ids.present? #获取要批量审核的工单
    if @batch_audits && @batch_audits.length !=0
      @batch_audits.each do |audit|
          if audit.job_status=='un_audit'
              audit.job_status='audit'
              audit.audit_time=Time.now
              audit.audit_man=@current_user.login_name
              audit.audit_if='Y'
              audit.save
              #批量审核通过时 为每条工单生成 日志
              @d_fault_job_detail=DFaultJobDetail.new(:job_status => audit.job_status,:handle_man => @current_user.login_name,:begin_time => Time.now,:end_time =>Time.now,:handle_status =>'完成',:d_task_form_id =>audit.id)
              @d_fault_job_detail.save
          end
       end
      #返回所有数据 封装到json中
      @ops_audits = DTaskForm.order(:created_at => :desc).page(params[:page]).per(params[:per]).by_job_status(@job_status)
   else
    return render json: {status: 'false'}
   end    
  end

  #复核
  def review
    job_status=@ops_audit.job_status #获取工单状态
    audit_if=params[:audit_if] #获取复核内容  Y  N 

    if audit_if=='Y' && job_status=='wait_review'
      @ops_audit.job_status='un_audit'   #修改工单状态  待审核
      @ops_audit.review_if='Y'           # Y 复核通过
      handle_status='待审核'              #日志中改为  待审核
    end
    if audit_if=='N' && job_status=='wait_review' && params[:audit_des].present?  #审核不通过时  必须要有审核不通过原因
      @ops_audit.review_if='N'     #复核没通过
      @ops_audit.review_des = params[:audit_des]    #复核不通过原因
      handle_status='未通过'       #日志中改为  ‘未通过’
      @ops_audit.job_status='not_review'  #工单状态改为 复核不通过

      note=params[:audit_des]   #将不通过原因 保存到日志表中的 note字段
      
    elsif audit_if=='N' && job_status=='wait_review' && !params[:audit_des].present?
      return render json: {status: 'false', location: @ops_audit.errors}
    end

    @ops_audit.review_man=@current_user.login_name  #获取当前的复核人
    @ops_audit.review_time=Time.now                 #更新复核时间

    #保存工单日志 （通过不通过均保存日志信息）
    @d_fault_job_detail=@ops_audit.d_fault_job_details.new(:job_status => @ops_audit.job_status, :handle_man => @current_user.login_name, :begin_time => Time.now, :end_time => Time.now, :handle_status => handle_status, :note => note)
    @d_fault_job_detail.save
    respond_to do |format|
      if @ops_audit.save && audit_if=='Y' && job_status=='wait_review' #更新表单中填写的数据  审核人、审核不通过原因
        format.html {}
        format.json {render json: {status: 'success', location: @ops_audit}}
      else
        format.html {}
        format.json {render json: {status: 'false', location: @ops_audit.errors}}
      end
    end
  end






  #单个审核工单
  def update 
    job_status=@ops_audit.job_status #获取工单状态
    handle_man = @ops_audit.handle_man #获取工单日志信息  处理人
    audit_if=params[:audit_if] #获取审核内容  Y  N 

    if audit_if=='Y' && job_status=='un_audit'
      @ops_audit.job_status='audit' #修改工单状态
      @ops_audit.audit_if='Y'
      handle_status='完成' #日志中改为  ‘完成’
    end
    if audit_if=='N' && job_status=='un_audit' && params[:audit_des].present?  #审核不通过时  必须要有审核不通过原因
      @ops_audit.audit_if='N'
      @ops_audit.audit_des = params[:audit_des]
      handle_status='未通过' #日志中改为  ‘未通过’
      #@ops_audit.job_status='wait_deal'  #工单状态改为 待处理状态
      @ops_audit.job_status='not_audit'   #工单状态改为 审核不通过

      note=params[:audit_des]   #将不通过原因 保存到日志表中的 note字段
      
    elsif audit_if=='N' && job_status=='un_audit' && !params[:audit_des].present?
      return render json: {status: 'false', location: @ops_audit.errors}
    end

    @ops_audit.audit_man=@current_user.login_name  #获取当前的审核人
    @ops_audit.audit_time=Time.now   #更新审核时间

    #保存工单日志 （通过不通过均保存日志信息）
    @d_fault_job_detail=@ops_audit.d_fault_job_details.new(:job_status => @ops_audit.job_status, :handle_man => @current_user.login_name, :begin_time => Time.now, :end_time => Time.now, :handle_status => handle_status, :note => note)
    @d_fault_job_detail.save
    respond_to do |format|
      if @ops_audit.save && audit_if=='Y' && job_status=='un_audit' #更新表单中填写的数据  审核人、审核不通过原因
        format.html {}
        format.json {render json: {status: 'success', location: @ops_audit}}
      else
        format.html {}
        format.json {render json: {status: 'false', location: @ops_audit.errors}}
      end
    end
  end

  private
  def set_ops_audit
    @ops_audit = DTaskForm.find(params[:id])
    @d_fault_job_details=DFaultJobDetail.where(:d_task_form_id => params[:id]) #获取日志信息  关联对应的工单
  end
end