class SearchStationLoginsController < ApplicationController
  before_action :current_user

  #params[:station_arr] = [1,2,3,4]
  def index

    if params[:station_arr].present?
      station_arr = params[:station_arr]
      station_arr.each do |arr|
        station = DStation.find_by(:id => arr.to_i)
        next if station.blank?
        @login_arr= []
        station.s_region_codes.each do |region|
          login_ha = []
          logins = region.d_login_msgs
          @login_arr << {region_name: region.region_name, login_arr: logins}
        end
      end
    end

    render json: {all_infos: @login_arr}
  end

  # POST /search_station_logins/despatch_create
  # data:{ alarm_arr:[报警ids], login_arr: 1 }
  def despatch_create
    if params[:alarm_arr].present? && params[:login_arr].present?
      respond_to do |format|
        if params[:alarm_arr][0] == ''
          format.json {render json: {status: :unprocessable_entity, location: "任务分配失败！"}}
          return
        end
        now_time = Time.now

        DAlarmDespatch.transaction do
          login_msg = DLoginMsg.find(params[:login_arr].to_i)  #分配的处理人
          @alarm_despatch = DAlarmDespatch.create!(despatch_time: now_time,
                                                   despatch_login: @current_user.login_name,
                                                   receive_login: login_msg.login_name,
                                                   d_login_msg_id: login_msg.id,
                                                   task_status: "assign")
          @alarm_despatch.create_d_alarm_despatch_alarm_rels(params[:alarm_arr], login_msg)
         

          if @alarm_despatch

              #创建数据异常工单
              d_alarms_ids = params[:alarm_arr]
              d_alarms_ids.each do |id|
                d_alarm=DAlarm.find(id)
                station = DStation.find(d_alarm.station_id)
                @faultJob = DTaskForm.new()
                @faultJob.job_no = "GD"+crc(Time.now.strftime("%H%M%S")).to_s << rand(9999).to_s
                @faultJob.create_type = "plan_out" 
                @faultJob.author = @current_user.login_name
                @faultJob.job_status = "wait_deal"
                @faultJob.clraring_if = @faultJob.clraring_if.present? ? @faultJob.clraring_if : "N"
                @faultJob.fault_type= "data_abnormal_form"    #工单类型：数据异常单
                @faultJob.handle_man_id=login_msg.id
                @faultJob.handle_man=login_msg.login_name
                @faultJob.s_region_code_info_id=login_msg.s_region_code_info_id
                @faultJob.d_station_id= d_alarm.station_id
                @faultJob.title= d_alarm.alarm_rule
                @faultJob.station_name= station.station_name
                @faultJob.urgent_level= d_alarm.alarm_level_str      #紧急程度
                @faultJob.save

                details_create(@faultJob)  #创建日志
              end
          
            format.json {render json: {status: :ok, location: "任务分配成功！"}}
          else
            format.json {render json: {status: :unprocessable_entity, location: "任务分配失败！"}}
          end

        end


      end
    end
  end

  #创建工单日志
  def details_create(d_task_form)
    @d_task_form = d_task_form
    @details = @d_task_form.d_fault_job_details.new(:job_status =>'writing',
                                              :handle_man =>'系统',:begin_time =>Time.now,
                                              :end_time =>Time.now,:handle_status =>"完成",:without_time_flag =>"无")
    @details.save
  end




end