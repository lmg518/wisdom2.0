class SearchReginCodeInfosController < ApplicationController

  #  search_regin_code_infos
  def index
    #@region_codes = SRegionCodeInfo.where(:status => 'Y').map{|x| {id: x.id, unit_code: x.unit_code, unit_name: x.unit_name}}

    @region_codes = SRegionCode.all.map{|x| {id: x.id, unit_code: x.region_code, unit_name: x.region_name}}
    render json: {unit_region: @region_codes}
  end

end