class WorkerManagesController < ApplicationController
  before_action :get_worker, only: [:update, :show, :edit, :destroy]
  # GET /worker_manages
  # GET /worker_manages.json
  def index
    @d_workers=DNoticeWorker.order(:created_at => :desc).page(params[:page]).per(params[:per])


  end

  #获取组信息的json
  # GET /worker_manages/get_group_infos.json
  def get_group_infos
    @gruop_infos=DGroup.all
    render json: {group_infos: @gruop_infos}
  end





  # GET /worker_manages/1
  # GET /worker_manages/1.json
  def show
  end

  # GET /worker_manages/new
  def new
    #@d_worker = DNoticeWorker.new
  end

  # GET /worker_manages/1/edit
  def edit
  end

  # POST /worker_manages
  # POST /worker_manages.json
  def create
    @d_worker = DNoticeWorker.new(worker_params)

    #获取所属组id 
    @group_ids=params[:group_ids]
    @group_ids.each do |id|
      @d_work_group=@d_worker.d_worker_groups.new(:d_group_id => id)
      @d_work_group.save
    end

    respond_to do |format|
      if @d_worker.save
        format.json {render json: {status: 'success', location: @d_worker}}
        format.html {}
      else
        format.json {render json: {status: 'false', location: @d_worker.errors}}
        format.html {}
      end
    end
  end

  # PATCH/PUT /worker_manages/1
  # PATCH/PUT /worker_manages/1.json
  def update

      #把原来的所属组删除
      @d_worker_olds=@d_worker.d_worker_groups
      @d_worker_olds.each do |o|
        o.destroy
      end

      #获取所属组id 重新创建所属组
      @group_ids=params[:group_ids]
      @group_ids.each do |id|
        @d_work_group=@d_worker.d_worker_groups.new(:d_group_id => id)
        @d_work_group.save
      end

    respond_to do |format|
      if @d_worker.update(worker_params)
        format.json {render :show}
        format.html {}
      else
        format.json {render json: {status: 'false'}}
        format.html {}
      end
    end
  end

  # DELETE /worker_manages/1
  # DELETE /worker_manages/1.json
  def destroy

  #删除关联组 表中的信息
 # @d_work_group=DWorkerGroup.where(:d_notice_worker_id => params[:id])
  #@d_work_group.destroy

  @d_work_groups = @d_worker.d_worker_groups
  #Rails.logger.info "----1--#{@d_work_groups}------"
  @d_work_groups.each do |g|
    #Rails.logger.info "--2----#{g}------"
    g.destroy
  end

    @d_worker.destroy
    respond_to do |format|
      format.html {}
      format.json {render :index }
    end
  end

 private 
 def get_worker
    @d_worker=DNoticeWorker.find(params[:id])
    @d_worker_g=DNoticeWorker.find(params[:id]).d_groups
 end

 def worker_params
   params.require(:d_notice_worker).permit(:id, :name, :telphone,
                                 :wee_chat, :qq_num, :email,
                                 :work_status)
 end   

   
end
