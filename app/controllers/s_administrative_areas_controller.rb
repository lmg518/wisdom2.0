class SAdministrativeAreasController < ApplicationController
  before_action :set_s_administrative_area, only: [:show, :edit, :update, :destroy]

  # GET /s_administrative_areas
  # GET /s_administrative_areas.json
  def index
    @s_administrative_areas = SAdministrativeArea.where(:parent_id => nil)
  end

  # GET /s_administrative_areas/1
  # GET /s_administrative_areas/1.json
  def show
  end

  # GET /s_administrative_areas/new
  def new
    @s_administrative_area = SAdministrativeArea.new
  end

  # GET /s_administrative_areas/1/edit
  def edit
  end

  # POST /s_administrative_areas
  # POST /s_administrative_areas.json
  def create
    @s_administrative_area = SAdministrativeArea.new(s_administrative_area_params)

    respond_to do |format|
      if @s_administrative_area.save
        format.html { redirect_to @s_administrative_area, notice: 'S administrative area was successfully created.' }
        format.json { render :show, status: :created, location: @s_administrative_area }
      else
        format.html { render :new }
        format.json { render json: @s_administrative_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /s_administrative_areas/1
  # PATCH/PUT /s_administrative_areas/1.json
  def update
    respond_to do |format|
      if @s_administrative_area.update(s_administrative_area_params)
        format.html { redirect_to @s_administrative_area, notice: 'S administrative area was successfully updated.' }
        format.json { render :show, status: :ok, location: @s_administrative_area }
      else
        format.html { render :edit }
        format.json { render json: @s_administrative_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /s_administrative_areas/1
  # DELETE /s_administrative_areas/1.json
  def destroy
    @s_administrative_area.destroy
    respond_to do |format|
      format.html { redirect_to s_administrative_areas_url, notice: 'S administrative area was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_s_administrative_area
      @s_administrative_area = SAdministrativeArea.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def s_administrative_area_params
      params.require(:s_administrative_area).permit(:zone_name, :zone_rank, :parent_id, :des, :active_state, :sort_num)
    end
end
