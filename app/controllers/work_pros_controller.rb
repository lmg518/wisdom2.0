class WorkProsController < ApplicationController
  before_action :set_work, only: [:show, :edit, :update, :destroy]

  def index
  end

  #从set_user 获取数据
  def show
  end

  # GET /users/1/edit
  def edit
  end


  def new
    @work=DWorkPro.new
  end

  #成功跳转到show页面
  def create
    @work = DWorkPro.new(work_params)
    respond_to do |format|
      if @work.save
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Successfully create!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :new}
      end
    end
  end


  #删除后调转到首页  /work_pros
  def destroy
    respond_to do |format|
      if @work.destroy
        format.html {redirect_to work_pros_path, notice: 'Successfully destroy!'}
        format.json {render json: {status: 'success'}}
      else
        format.json {render json: {status: 'false'}}
      end
    end
  end


  #成功跳转到show页面
  # PATCH/PUT /work_pros/1
  def update
    respond_to do |format|
      if @work.update(work_params)
        format.json {render json: {status: 'success', location: @work}}
        format.html {redirect_to work_pro_path(@work), notice: 'Succesfully updated!'}
      else
        format.json {render json: {status: 'false', location: @work.errors}}
        format.html {render :edit}
      end
    end

  end

  private
  def set_work
    @work = DWorkPro.find(params[:id])
  end

  def work_params
    params.require(:d_work_pro).permit(:id, :work_name, :work_code, :work_type, :work_flag)
  end

end
