class DataStatisticsController < ApplicationController
  before_action :current_user

  def index
  end

  def init_index
    search_parmas
    handle_new
  end

  def export_file
    export_file_params
    handle_new
    @scv = CSV.generate(encoding: "GB18030") do |csv|
      csv << ["站点名称", "SO2", "NO2", "CO", "O3", "PM2.5", "PM10"]
      @yy_h.each do |yy|
        csv << [yy[:station_name],yy[:so2], yy[:no2], yy[:co], yy[:o3], yy[:pm2_5], yy[:pm10]]
      end
      csv << ["站点平均", @all_infos[:so2], @all_infos[:no2], @all_infos[:co], @all_infos[:o3], @all_infos[:pm2_5], @all_infos[:pm10]]
    end
    if  @name =@data_times.present?
      @name = Time.parse(@data_times[0]).strftime("%Y-%m-%d-") + Time.parse(@data_times[1]).strftime("%Y-%m-%d") + "站点数据有效性统计"
    else
      @name = Time.now.strftime("%Y-%m-%d-") + "站点数据有效性统计"
    end
    respond_to do |format|
      format.csv{
        send_data @scv,
                  :type=>'text/csv; charset=GB18030; header=present',
                  :disposition => "attachment; filename=#{@name}.csv"
      }
    end
  end

  private
  def export_file_params
    @region_leve = params[:region_level]
    @station_ids = params[:station_ids].present? ? params[:station_ids].split(",") : ''
    @data_times = params[:data_times].present? ? params[:data_times].split(",") : ''
    @alarm_levels = params[:alarm_levels]
  end

  def search_parmas
    if params[:search].present?
      @search = params[:search]
      @region_leve = @search[:region_level]
      @station_ids = @search[:station_ids]
      @data_times = @search[:data_times]
      @alarm_levels = @search[:alarm_levels]
    end
  end

  #站点数据有效性
  def handle_new
    @yy_h, @so2,@co,@no2,@o3,@pm2_5,@pm10, = [],0,0,0,0,0,0,0
    @region_leves = @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
    @station_item_avgs = StationItemAvg.by_get_time(@data_times).by_d_station_id(@station_ids)
     item_length = @station_item_avgs.length == 0 ?  1 : @station_item_avgs.length
    @all_infos = {so2:( @station_item_avgs.sum("so2_avg").to_f / item_length.to_f ).round(2),
                  no2: (@station_item_avgs.sum("no2_avg").to_f / item_length.to_f).round(2) ,
                  co: (@station_item_avgs.sum("co_avg").to_f  / item_length.to_f).round(2) ,
                  o3: (@station_item_avgs.sum("o3_avg").to_f  / item_length.to_f ).round(2),
                  pm2_5: (@station_item_avgs.sum("pm2_5_avg").to_f  / item_length.to_f ).round(2),
                  pm10: (@station_item_avgs.sum("pm10_avg").to_f  / item_length.to_f ).round(2)}
    @station_item_avgs.each do |item|
      @yy_h << {station_name: DStation.find(item.d_station_id).station_name.split("-")[1],
                so2: item.so2_avg,
                so2_avg: @all_infos[:so2],
                no2: item.no2_avg ,
                no2_avg: @all_infos[:no2],
                co: item.co_avg,
                co_avg: @all_infos[:co],
                o3: item.o3_avg,
                o3_avg: @all_infos[:o3],
                pm2_5: item.pm2_5_avg,
                pm2_5_avg: @all_infos[:pm2_5],
                pm10: item.pm10_avg,
                pm10_avg: @all_infos[:pm10],
      }

    end

  end

  def handle_table
    @yy_h, @so2,@co,@no2,@o3,@pm2_5,@pm10, = [],0,0,0,0,0,0,0
    @region_leves = @region_leves = SRegionLevel.select(:id, :level_name)
    @region_codes = SRegionCode.by_region_level(@region_leve)
    @data_yyyy_mms = DDataYyyymm.by_station_statistics(@station_ids).by_get_time(@data_times).unactive
                         # .by_station_ids(@station_ids).unactive
    if @station_ids.present?
      @station_ids.each do |station_id|
        item_avg(station_id.to_i)
      end
    else
      item_avg(1484)
    end
    @all_infos = {so2:@data_yyyy_mms.where(:item_code => 101).average("data_value").round(2),
                  no2:@data_yyyy_mms.where(:item_code => 102).average("data_value").round(2),
                  co:@data_yyyy_mms.where(:item_code => 105).average("data_value").round(2),
                  o3:@data_yyyy_mms.where(:item_code => 106).average("data_value").round(2),
                  pm2_5:@data_yyyy_mms.where(:item_code => 107).average("data_value").round(2),
                  pm10:@data_yyyy_mms.where(:item_code => 108).average("data_value").round(2),}

  end

  def item_avg(station_id)
    @so2 = @data_yyyy_mms.where(:item_code => 101, :station_id => station_id).average("data_value").round(2)
    @co = @data_yyyy_mms.where(:item_code => 105, :station_id => station_id).average("data_value").round(2)
    @no2 = @data_yyyy_mms.where(:item_code => 102, :station_id => station_id).average("data_value").round(2)
    @o3 = @data_yyyy_mms.where(:item_code => 106, :station_id => station_id).average("data_value").round(2)
    @pm2_5 = @data_yyyy_mms.where(:item_code => 107, :station_id => station_id).average("data_value").round(2)
    @pm10 = @data_yyyy_mms.where(:item_code => 108, :station_id => station_id).average("data_value").round(2)
    @yy_h << {station_name: DStation.find(station_id).station_name.split("-")[1],
              so2: @so2,
              co: @co,
              no2: @no2,
              o3: @o3,
              pm2_5: @pm2_5,
              pm10: @pm10
    }
  end

end
