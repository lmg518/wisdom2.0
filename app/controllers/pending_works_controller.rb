class PendingWorksController < ApplicationController  
    #Get /pending_works
    #Get /pending_works.json?create_time=?&end_time=?&polling_time=?&job_status=?&over_time=?
    def index
        @works = DWorkPro.select(:id,:work_name,:work_type)
        created_time = params[:created_time]
        end_time = params[:end_time]
        @task = DTaskForm.where(:fault_type => 'polling_form')
                         .where.not(job_status: 'audit')
                         .by_polling_time(params[:created_time].to_s+' 00:00:00',params[:end_time].to_s+' 23:59:59')
    end
    
    #Get /pending_works/id
    #Get /pending_works/id.json
    def show
        @task_work = DTaskForm.find(params[:id])
    end

end
