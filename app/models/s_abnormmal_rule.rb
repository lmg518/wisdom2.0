class SAbnormmalRule < ActiveRecord::Base

  paginates_per 10

  belongs_to :s_abnormal_ltem

  # H：有效数据不足 BB：连接不良 B：运行不良 W：等待数据恢复 HSp：数据超上限 LSp：数据超下限
  # PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
  # ZERO：出零出负 TH1：突然高／低 TH2：突然高／低(变化率) DBG1: 对标高／低
  # DBG2: 对标高／低(变化率) KDG：颗粒物倒挂 ,DING: 定值

# SAbnormmalRule.create_rule  穿件异常规则
  def self.create_rule
    SAbnormalLtem.all.each do |item|
      FlagType.all.each do |flag|
        SAbnormmalRule.rule_create(item.id, flag.flag_code, flag.type_id)
      end
    end
  end

  private

  def self.rule_create(item_id, flag_str, type_id)
    @dec = ""
    yz = SAbnormalLtem.find(item_id)
    fl = SAbnormalType.find(type_id)
    # ckz = SAbnormalCompare.find(ckz_id)
    num = 0
    compare_val1= ''
    compare_val2= ''
    compare_val3= ''
    case flag_str
      when 'TRG'
        compare_val1= '量值高于（ '
        compare_val2= '）  '
        num = 0
        # @dec = yz.item_name + "与上一正常数据差值，量值高于（X）"
        @dec = yz.item_name + "污染物突然高"
      when 'TRD'
        compare_val1= '量值低于（ '
        compare_val2= '）  '
        num = 0
        # @dec = yz.item_name + "与上一正常数据差值，量值低于（X）"
        @dec = yz.item_name + "污染物突然低"
      when 'DBGG'
        compare_val1= '量值高于（ '
        compare_val2= '）  '
        num = 0
        # @dec = yz.item_name + "与同城站点数据差值，量值高于（X）"
        @dec = yz.item_name + "与同城站点数据平均值，量值高于50%"
      when 'DBGD'
        compare_val1= '量值低于（ '
        compare_val2= '）  '
        num = 0
        # @dec = yz.item_name + "与同城站点数据差值，量值低于（X）"
        @dec = yz.item_name + "与同城站点数据平均值，量值低于50%"
      when 'KDG'
        @dec = yz.item_name + "颗粒物倒挂（PM2.5 > PM10）"
      when "DING"
        @dec = yz.item_name + "定值"
      # when "LOST"
      #   @dec = yz.item_name + "站点质控离线"
      when "D2DC1B"
        @dec = yz.item_name + "连续两天平均值超1倍"
    end

    SAbnormmalRule.find_or_create_by!(rule_code: 'KZ' + flag_str,
                                      ab_lable: flag_str, item_code: yz.item_name,
                                      rule_type: fl.rule_type, rule_name: flag_to_str(flag_str),
                                      rule_expression: flag_code_str(flag_str),
                                      rule_expression_des: @dec.present? ? @dec : "#{yz.item_name}#{flag_to_str(flag_str)}",
                                      compare_code: compare_val1, compare_code2: compare_val2,
                                      comoare_code3: compare_val3, parameter_num: num, note: '',
                                      s_abnormal_type_id: fl.id, s_abnormal_ltem_id: yz.id)
  end

  #因子值：yz_val  第一个值： first_val  第二个值:second_val
  def self.flag_code_str(flag_code)
    case flag_code
      when 'TH1'
        " - input_val  > ( yz_val - pre_val ) > input_val "
      when 'TH2'
        " - input_val/100  > ( yz_val - pre_val ) > input_val/100  "
      when 'DBG1'
        "  "
      when 'DBG2'
        ""
      when 'HDG'
        ' first_val > second_val'
    end
  end

  #因子标示
  def self.flag_to_str(flag_code)
    case flag_code
      when 'H'
        "有效数据不足"
      when 'BB'
        "连接不良"
      when 'B'
        "运行不良"
      when 'W'
        "等待数据恢复"
      when 'HSp'
        "数据超上限"
      when 'LSp'
        "数据超下限"
      when 'PS'
        "跨度检查"
      when 'PZ'
        "零点检查"
      when 'AS'
        "精度检查"
      when 'CZ'
        "零点校准"
      when 'CS'
        "跨度校准"
      when 'RM'
        "自动或人工审核为无效"
      when 'ZERO'
        "出零出负"
      when 'TH1'
        "突然高／低"
      when 'TH2'
        "突然高／低(变化率)"
      when 'DBG1'
        "对标高／低"
      when 'DBG2'
        "对标高／低(变化率)"
      when 'KDG'
        "颗粒物倒挂"
      when 'TRG'
        "突然高"
      when 'TRD'
        "突然低"
      when 'DBGG'
        "对标高"
      when 'DBGD'
        "对标低"
      when "DING"
        "定值"
      when "LOST"
        "离线"
    end
  end

  def test
    #   根据站点和因子查找异常实例
    #   通过实例和因子确定一个模版
    #   通过实例的值和模版的判定公式判断是够生成异常数据
    zhandia = DDataYYYYMM.where(:created_at > Time.now - 5.minutes, d_station_id => 1)
    a = Dstation.find(:id).s_abnormal_rule_instances.find_by(:s_abnormal_ltem_id => SAbnormalLtem.find_by_item_code("因子")) #查出监控站点的因子实例
    moban = a.s_abnormmal_rules
    x = yinzi.val
    y = a.parameter_value
    if eval moban.rule_expression #  'y  > x'
      DAbnormalDataYyyymm.create() #创建一个异常数据
    end
    # s = Rufus::Scheduler.singleton
    # job_id = s.every '5s' do
    #   Rails.logger.info "time flies, it's now #{Time.now}"
    # end
    # puts "job if #{job_id.inspect}"
  end

end
