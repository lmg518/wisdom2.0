class DAlarmDespatch < ActiveRecord::Base
  include AlarmDespatchesHelper

  paginates_per 10

  has_many :d_alarm_despatch_alarm_rels # 报警
  has_many :d_alarms, :through => :d_alarm_despatch_alarm_rels

  has_many :d_task_deal_records, :foreign_key => "task_id"

  belongs_to :d_login_msg

 # task_status = assign 分配 ，answer 响应，Retweet 转发，doing completed 完成, audit 审核， finish 结束

  scope :by_date_time, ->(date_time) {
    if date_time.present?
      where("created_at between ? and ?",Time.parse(date_time).beginning_of_day,Time.parse(date_time).end_of_day)
    else
      # where("created_at between ? and ?",Time.now.beginning_of_day,Time.now.end_of_day)
    end
  }

  scope :by_status, ->(status){
    where(:task_status => status) if status.present?
  }

  scope :by_done_status, ->(status){
    if status.present?
      where(:task_status => status)
    else
      where(:task_status => 'done')
    end
  }

  scope :by_create_user, ->(create_user){
    where("despatch_login like ?", "%#{create_user}%") if create_user.present?
  }

  scope :by_receive_user, -> (r_user){
    where("receive_login like ?","%#{r_user}%") if r_user.present?
  }

  scope :by_alarm_level, ->(level){
    where(:id => DAlarmDespatchAlarmRel.where(:d_alarm_id => DAlarm.where(:alarm_level => level.to_i).pluck(:id) ).pluck(:d_alarm_despatch_id)) if level.present?
  }

  scope :by_create_time, ->(r_date){
    if r_date.present?
      if r_date.length == 1
        r_rime1 = Time.parse(r_date[0])
        where(:created_at => ((r_rime1).beginning_of_day)..(r_rime1.end_of_day))
      elsif r_date.length == 2
        r_rime1 = Time.parse(r_date[0])
        r_rime2 = Time.parse(r_date[1])
        where(:created_at => ((r_rime1).beginning_of_day)..(r_rime2.end_of_day))
      end
    else
      where(:created_at => (Time.now.beginning_of_day)..(Time.now.end_of_day))
    end
  }


  def despatch_info_json
    alarms = self.d_alarms
    station = alarms.map{|x|  x.d_station.station_name}
    return [{stations: station,rule_name: alarms.map{|x| x.alarm_rule}, rule_leves: alarms.map{|x| x.alarm_level_to_s},
            id: self.id, despatch_time: self.despatch_time.strftime("%Y-%m-%d %H:%M:%S"),
            despatch_login: self.despatch_login, task_status: self.status_to_str,
            receive_login: self.receive_login,
            receive_time: self.receive_time.present? ? self.receive_time.strftime("%Y-%m-%d %H:%M:%S") : '',
            deal_time: self.deal_time.present? ? self.deal_time.strftime("%Y-%m-%d %H:%M:%S") : ''}]
  end

 # 多个报警生成一个任务，即： 一个任务对应多个报警
  def create_d_alarm_despatch_alarm_rels(alarm_arr,login_msg)
    alarm_arr.each do |alarm|
      alarm_info = DAlarm.find_by(:id => alarm.to_i)
      next if alarm_info.blank?
      alarm_info.d_alarm_despatch_alarm_rels.create!(:d_alarm_despatch_id => id )
      self.d_alarms.update_all(:status => "assign") if self.task_status == "assign"
    end
  end

  def status_to_str
    case task_status
      when 'assign'
        '待响应'
      when 'answer'
        '待处理'
      when "doing"
        "处理中"
      when "done"
        "已完成"
      when 'retweet'
        "待分配"
      when 'completed'
        "待审核"
      when 'audit'
        "已审核"
      when "finish"
        '结束'
    end
  end

  def self.get_all_status
    status_arr = []
    %W(assign answer retweet completed audit finish).each do |val|
      status_arr <<  {status: val, status_name: all_status_str(val)}
    end
    return status_arr
  end

  def self.all_status_str(obj)
    case obj
      when 'assign'
        '待响应'
      when 'answer'
        '已响应'
      when "doing"
        '处理中'
      when 'retweet'
        "待分配"
      when 'completed'
        "待审核"
      when 'audit'
        "已审核"
      when "finish"
        '结束'
    end
  end

 # flow_no 流程号
 # deal_accept 操作流水规则 "RW" + Time.now.strftime("%Y%m%d%H%M%S") + self.id
  after_commit on: [:create, :update] do
    case task_status
      when "assign"
        d_task_deal_record_create(self)
      when "answer"
        handle_despath(self)
      when "doing"
        handle_despath(self)
      when "done"
        handle_despath(self)
      when 'retweet'
        handle_despath(self)
      # when 'completed'
      #   handle_despath(self)
      when 'audit'
        handle_despath(self)
      when "finish"
        handle_despath(self)
    end
  end

end
