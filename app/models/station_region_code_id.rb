class StationRegionCodeId < ActiveRecord::Base
  belongs_to :d_station
  belongs_to :s_region_code
end
