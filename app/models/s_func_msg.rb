class SFuncMsg < ActiveRecord::Base
  has_many :son_func, class_name: "SFuncMsg", foreign_key: "father_func_code"
  belongs_to :father_func, class_name: "SFuncMsg"
  
  has_many :s_role_funcs
  has_many :s_role_msgs, through: :s_role_funcs

  scope :s_by_father_id, ->(father_id){
    if father_id.present?
      where(:father_func_id => father_id)
    else
      where(:father_func_id => 9999)
    end

  }

  ### 查询功能点
  def son_funcs
    SFuncMsg.where( "father_func_id = ? and valid_flag = 0", self.id ).order( :queue_index )
  end

  def mean_childern_path
     son_funcs  if valid_flag == "1"
    # SFuncMsg.where(:father_func_code => func_code ).where.not(:father_func_code => '9999').order( :queue_index )
  end

end
