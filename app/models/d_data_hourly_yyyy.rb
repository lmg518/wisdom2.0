include ItemIaqiAqiHelper
class DDataHourlyYyyy < ActiveRecord::Base

  belongs_to :d_station, :foreign_key => :station_id

  #计算每个站点小时  因子API
  #  DDataHourlyYyyy.hour_avg_create
  #[#<DDataYyyymm id: 11134110, station_id: 2682, item_code: 101, data_cycle: 3600, data_attr: "T", data_time: "2017-07-04 07:00:00", data_value: "8", data_label: "", created_at: "2017-07-04 07:37:45", updated_at: "2017-07-04 07:37:45", receive_time: "2017-07-04 07:26:23", receive_accept: 1067243>]
  def self.hour_avg_create
    now_time = Time.now
    @data_time = (now_time - 1.hours)
    DStation.find_each(batch_size: 230) do |station|
      data_yy_mms = station.d_data_yyyymms.where(:data_cycle => 3600, :data_time => @data_time.beginning_of_hour)
      next if data_yy_mms.blank?
      so2 = data_yy_mms.select {|x| x.item_code.to_i == 101}
      no2 = data_yy_mms.select {|x| x.item_code.to_i == 102}
      co = data_yy_mms.select {|x| x.item_code.to_i == 105}
      o3_8hour = station.d_data_yyyymms.where(:data_cycle => 3600, :item_code => "106", :data_time => ((@data_time.beginning_of_hour - 8.hours))..(@data_time.beginning_of_hour)).average("data_value")
      o3 = data_yy_mms.select {|x| x.item_code.to_i == 106}
      pm2_5 = data_yy_mms.select {|x| x.item_code.to_i == 107}
      pm25_24 = station.d_data_yyyymms.where(:data_cycle => 3600, :item_code => "107", :data_time => ((@data_time.beginning_of_hour - 24.hours))..(@data_time.beginning_of_hour)).average("data_value")
      pm10 = data_yy_mms.select {|x| x.item_code.to_i == 108}
      pm10_24 = station.d_data_yyyymms.where(:data_cycle => 3600, :item_code => "108", :data_time => ((@data_time.beginning_of_hour - 24.hours))..(@data_time.beginning_of_hour)).average("data_value")

      @AQI_arr = []
      #创建小时AQI报表
      hourly_yyyys = station.d_data_hourly_yyyys.build(
          station_name: station.station_name,
          data_time: @data_time.beginning_of_hour,
          avg_so2: so2.present? ? so2[0].data_value : 0,
          so2_label: so2.present? ? so2[0].data_label : '',
          iaqi_so2: so2.present? ? handle_hour_so2(so2[0].data_value) : 0,
          avg_no2: no2.present? ? no2[0].data_value : 0,
          no2_label: no2.present? ? no2[0].data_label : '',
          iaqi_no2: no2.present? ? handle_hour_so2(no2[0].data_value) : 0,
          avg_co: co.present? ? co[0].data_value : 0,
          co_label: co.present? ? co[0].data_label : '',
          iaqi_co: co.present? ? handle_hour_co(co[0].data_value) : 0,
          avg_o3: o3.present? ? o3[0].data_value : 0,
          o3_label: o3.present? ? o3[0].data_label : '',
          iaqi_o3: o3.present? ? handle_o3(o3[0].data_value) : 0,
          ma8_o3: o3_8hour.present? ? o3_8hour : 0,
          iaqi_ma8_o3: o3_8hour.present? ? handle_hour8_o3(o3_8hour) : 0,
          avg_pm10: pm10.present? ? pm10[0].data_value : 0,
          pm10_label: pm10.present? ? pm10[0].data_label : '',
          ma24_pm10: pm10_24.present? ? pm10_24 : 0,
          iaqi_ma24_pm10: pm10_24.present? ? handle_24_pm10(pm10_24) : 0,
          avg_pm25: pm2_5.present? ? pm2_5[0].data_value : 0,
          pm25_label: pm2_5.present? ? pm2_5[0].data_label : '',
          ma24_pm25: pm25_24.present? ? pm25_24 : 0,
          iaqi_ma24_pm25: pm25_24.present? ? handle_24_pm2_5(pm25_24) : 0,
          # windspeed: nil,
          # windspeed_label: nil,
          # winddirection: nil,
          # winddirection_label: nil,
          # temperature: nil,
          # temperature_label: nil,
          # barometer: nil,
          # barometer_label: nil,
          # humidity: nil,
          # humidity_label: nil,
          # visibility: nil,
          # visibility_label: nil,
          chief_pollutant: nil,
          aqi: @AQI_arr.present? ? @AQI_arr.max : 0,
          aqi_level: aqi_days(@AQI_arr.max),
      )
      hourly_yyyys.chief_pollutant = chief_pollutant_add(hourly_yyyys)
      hourly_yyyys.save!
    end
  end

  def self.chief_pollutant_add(hourly_yyyys)
    item_arr = [hourly_yyyys.iaqi_so2, hourly_yyyys.iaqi_no2, hourly_yyyys.iaqi_co,
                hourly_yyyys.iaqi_o3, hourly_yyyys.iaqi_ma8_o3, hourly_yyyys.iaqi_ma24_pm10,
                hourly_yyyys.iaqi_ma24_pm25]
    item_info = ["so2", "no2", "co", "o3", "o3", 'pm10', 'pm2.5']
    item_arr_max = item_arr.max
    item_index = item_arr.index(item_arr_max)
    item_info[item_index]
  end

  #<DDataHourlyYyyy
  # id: nil,
  # station_id: nil,
  # station_name: nil,
  # data_time: nil,
  # avg_so2: nil,
  # so2_label: nil,
  # iaqi_so2: nil,
  # avg_no2: nil,
  # no2_label: nil,
  # iaqi_no2: nil,
  # avg_co: nil,
  # co_label: nil,
  # iaqi_co: nil,
  # avg_o3: nil,
  # o3_label: nil,
  # iaqi_o3: nil,
  # ma8_o3: nil,
  # iaqi_ma8_o3: nil,
  # avg_pm10: nil,
  # pm10_label: nil,
  # ma24_pm10: nil,
  # iaqi_ma24_pm10: nil,
  # avg_pm25: nil,
  # pm25_label: nil,
  # ma24_pm25: nil,
  # iaqi_ma24_pm25: nil,
  # windspeed: nil,
  # windspeed_label: nil,
  # winddirection: nil,
  # winddirection_label: nil,
  # temperature: nil,
  # temperature_label: nil,
  # barometer: nil,
  # barometer_label: nil,
  # humidity: nil,
  # humidity_label: nil,
  # visibility: nil,
  # visibility_label: nil,
  # aqi: nil,
  # chief_pollutant: nil,
  # aqi_level: nil,
  # aqi_class: nil,
  # aqi_color: nil,
  # created_at: nil,
  # updated_at: nil>
end
