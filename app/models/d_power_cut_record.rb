class DPowerCutRecord < ActiveRecord::Base
  
  #设置分页
  paginates_per 10

  belongs_to :s_region_code
  belongs_to :d_station
  belongs_to :d_login_msg
  has_many :d_power_cut_audit_logs

#按运运维人员
scope :by_handle_man, ->(handle_man) {
  where(:handle_man => handle_man) if handle_man.present?
}
#按运维单位搜索
scope :by_unit_yw, ->(unit_yw) {
  where(:unit_yw => unit_yw) if unit_yw.present?
}

#按站点搜索 
scope :by_station_name, ->(station_name) {
  where(:station_name => station_name) if station_name.present?
}

#按工单状态搜索  
scope :by_job_status, ->(job_status) {
  where(:job_status => job_status) if job_status.present?
}

#按来源搜索 
scope :by_origin, ->(origin) {
  where(:origin => origin) if origin.present?
}

#按回退原因搜索 
scope :by_audit_des, ->(audit_des) {
  where(:audit_des => audit_des) if audit_des.present?
}


#按创建时间  
scope :by_created_at, ->(created_at,end_at) {
  where(:created_at => (created_at..end_at)) if created_at.present? && end_at.present?
}


#按站点id查询
scope :by_station_ids, ->(station_id) {
  where(:d_station_id => station_id) if station_id.present?
}

#转换工单状态
def job_status_str
    case self.job_status
  when 'C'
   "待上报"
  when 'Y'
    "待审核"
  when 'W'
    "审核通过"
  when 'N'
    "审核不通过"
    else
      ''
    end
end



#封装 按状态 搜素信息
def self.status
  @str = []
   %w(待审核 审核通过 审核不通过).each_with_index do |statu,index|
      hande_no = %w(Y W N )[index]
      @str << {value: statu, text: hande_no }
   end
   return @str
end


#封装 按来源 搜素信息
def self.origins
  @str = []
   %w(地方站报送 其它报送 市站盖章 地方供电局证明 公司盖章 其他).each_with_index do |statu,index|
      hande_no = %w(local_submit other_submit city_stamp local_prove company_seal other )[index]
      @str << {value: statu, text: hande_no }
   end
   return @str
end

#封装 按异常 填写的信息
def self.abnormals
  @str = []
   %w(电力中断 其它情况 数据异常 通讯中断 网络中断 仪器异常).each_with_index do |statu,index|
      hande_no = %w(power_cut other_situations abnormal_data communication_cut net_cut instrument_abnormal )[index]
      @str << {value: statu, text: hande_no }
   end
   return @str
end

#封装 回退原因
def self.fallbacks
  @str = []
   %w(停电时间超出证明范围 停电时间填写有误 停电站点与凭证中的站点不符 间断停电时间请分开录入 证明中停电时间不详 照片印章不清晰 异常情况选择不正确 其他).each_with_index do |statu,index|
      hande_no = %w(out_range write_wrong not_conform separate_write not_detail not_purging select_wrong other)[index]
      @str << {value: statu, text: hande_no }
   end
   return @str
end



end



