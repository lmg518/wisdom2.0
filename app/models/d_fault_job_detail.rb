class DFaultJobDetail < ActiveRecord::Base

  belongs_to :d_task_form

   # 巡检单流程：1创建：un_write；2:填写 writing；3代审核： un_audit; 4:审核：audit 5 工单复审 review_audit
   def job_status_str
    case job_status
      when 'un_write'    #故障日志中，手动创建巡检单时日志
        '创建工单'
      when 'writing'     #自动创建巡检工单时
        '创建工单'
      when 'wait_deal'
        '工单待处理'
      when 'deal_with'
        '工单处理中'
      when 'wait_maint'
        '设备待维修'
      when 'wait_calib'
        '设备待校准'
      when 'un_audit'
        '工单待审核'
      when 'audit'
        '工单已审核'
      when "wait_review"
        '工单待复核'
      when "not_review"
        '复核不通过'
      when "not_audit"
        '审核不通过'
      else
        ''
    end
  end
  


  

end
