class DayHourFalgData < ActiveRecord::Base

  scope :by_get_data, ->(data_time){
    where(:acce_data => (Time.parse(data_time).beginning_of_day)..(Time.parse(data_time).end_of_day)) if data_time.present?
  }
  scope :by_province, ->(provience){
    where("province like ?","%#{provience}%") if provience.present?
  }

end
