class SRoleMsg < ActiveRecord::Base

  paginates_per 10

  has_many :s_role_funcs
  has_many :s_func_msgs, through: :s_role_funcs

  has_many :d_login_msgs

  validates :role_name, presence: true, uniqueness: {message:'角色名称已存在'}

end
