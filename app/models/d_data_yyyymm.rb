class DDataYyyymm < ActiveRecord::Base

  paginates_per 10

  belongs_to :d_station, foreign_key: :station_id

  scope :active, -> {
    where.not(:data_label => '')
  }

  scope :unactive, ->{
    where(:data_label => '')
  }

  scope :by_item_name, ->(item_name){
    where(:item_code => item_name) if item_name.present?
  }

  scope :by_item_lable, ->(lable){
    if lable.present?
      where(:data_label => lable) if lable[0].present?
    end
  }

  scope :by_station_statistics, ->(station_ids){
    if station_ids.present?
      where(:data_cycle => 3600 , :station_id => station_ids)
    else
      where(:data_cycle => 3600 )
    end
  }

  scope :by_hour_data, ->(obj_time) {
    if obj_time.present?
      where(data_cycle: 3600, data_time: (Time.parse(obj_time).beginning_of_day)..(Time.parse(obj_time).end_of_day))
    else
      where(data_cycle: 3600, data_time: ("2017-05-16 00:00:00".."2017-05-16 23:59:59"))
    end
  }

  scope :by_time_arr, ->(obj_time){
    if obj_time.present?
      where(data_cycle: 3600, data_time: (Time.parse(obj_time[0]).beginning_of_day)..(Time.parse(obj_time[1]).end_of_day))
    else
      where(data_cycle: 3600, data_time: ("2017-05-16 00:00:00".."2017-05-16 23:59:59"))
    end

  }

  scope :by_hour_month_data, ->(obj_time){
    if obj_time.present?
      where(data_cycle: 3600, data_time: (Time.parse(obj_time).beginning_of_month)..(Time.parse(obj_time).end_of_month))
    else
      where(data_cycle: 3600, data_time: ("2017-05-01 00:00:00".."2017-05-31 23:59:59"))
    end
  }

  scope :by_get_time, ->(get_time) {
    if get_time.present?
      begin
        first_time = Time.parse(get_time[0])
        second_time = Time.parse(get_time[1])
      rescue
        time_arr = get_time.split(",")
        first_time = Time.parse(time_arr[0])
        second_time = Time.parse(time_arr[1])
      end

      where("data_time between ? and ?", first_time.beginning_of_day, second_time.end_of_day)
    else
      where("data_time between ? and ?", Time.now.beginning_of_day, Time.now.end_of_day)
    end
  }

  scope :by_get_hour_time, ->(get_time) {
    if get_time.present?
      begin
        first_time = Time.parse(get_time[0])
        second_time = Time.parse(get_time[1])
      rescue
        time_arr = get_time.split(",")
        first_time = Time.parse(time_arr[0])
        second_time = Time.parse(time_arr[1])
      end

      where(:data_cycle => 3600 ,:data_time => ( first_time.beginning_of_day)..(second_time.end_of_day) )
    else
      where(:data_cycle => 3600 ,:data_time => (Time.now.beginning_of_day)..( Time.now.end_of_day))
    end
  }


  scope :by_stations, ->(station_name) {
    where(:station_id => DStation.where("station_name like ?", "%#{station_name}%").pluck(:id)) if station_name.present?
  }

  scope :by_station_ids, ->(station_ids) {
    where(:station_id => station_ids) if station_ids.present?
  }

  def station_hours_data
    yz_arr = []
    yz_arr << {yz_name: self.item_code_name,
               yz_lable: self.data_label ,
               yz_begin_time: self.data_time.strftime("%Y-%m-%d %H:%M:%S"),
               yz_end_time: self.data_time.strftime("%Y-%m-%d %H:%M:%S")  }
  end

  def item_code_name
    item_ids = %W(101 102 103 104 105 106 107 108 201 202 203 204 205 206)
    item_index =  item_ids.index(self.item_code.to_s)
    item_name = %W(SO2 NO2 NO NOX CO O3 PM2.5 PM10 风速 风向 气温 气压 湿度 能见度)[item_index]
    return item_name
  end

  def data_cycle_to_s
    case data_cycle
      when 300
        '5分钟数据'
      when 3600
        '1小时数据'
    end
  end

  # H：有效数据不足 BB：连接不良 B：运行不良 W：等待数据恢复 HSp：数据超上限 LSp：数据超下限
  # PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
  def data_label_to_s
    case data_label
      when 'H'
        '有效数据不足'
      when 'BB'
        '连接不良'
      when 'B'
        '运行不良'
      when 'W'
        '等待数据恢复'
      when 'HSp'
        '数据超上限'
      when 'LSp'
        '数据超下限'
      when 'PS'
        '跨度检查'
      when 'PZ'
        '零点检查'
      when 'AS'
        '精度检查'
      when 'CZ'
        '零点校准'
      when 'CS'
        '跨度校准'
      when 'RM'
        '自动或人工审核为无效'
    end
  end

  def self.fiv_times
     now_time = Time.now()
     mm = now_time.min.to_i
     case ( mm % 5 )
       when 0
         @time = now_time
       when 5
         @time = now_time
       else
        @time = ( now_time - ( mm % 5 ).minutes )
     end
     @time.beginning_of_minute
  end

  # 5 minutes data
  # DDataYyyymm.get_five_minutes_data
  def self.get_five_minutes_data
    fiv_times
    # @five_datas = DDataYyyymm.where("data_time BETWEEN ? AND ?",Time.parse(time_str).beginning_of_minute , Time.parse(time_str).end_of_minute)
    # @five_datas = DDataYyyymm.where("data_cycle = ? and data_time between ? and ?" ,300,"2017-05-18 17:00:00","2017-05-18 17:59:59").order(:receive_accept => :desc)
    @five_datas = DDataYyyymm.where(:data_cycle => 300, :data_time => (@time - 10.minutes).beginning_of_minute).order(:data_time => :desc)
    # @five_datas.map{|x| puts x.inspect}
    @data_arr = @five_datas.map {|x| x.attributes.slice("station_id", "item_code", 'data_value', 'data_label')}
    @five_datas.each do |f|
      SAbnormalRuleInstance.is_exception?(f.station_id, f.item_code, f.data_value, f.data_label, f.data_time, f.created_at, @data_arr, 300)
    end
  end

  #------定值统计-----------
scope :by_d_station_ids, ->(station_ids) {
  where(station_id: station_ids) if station_ids.present?
}

#按时间段查询
scope :by_pm_time, ->(begin_time,end_time) {
  where(data_time: begin_time..end_time) if begin_time.present? && end_time.present?
}

#按因子查询
scope :by_item_id, ->(item_ids) {
  where(:item_code => item_ids) if item_ids.present?
}

end
