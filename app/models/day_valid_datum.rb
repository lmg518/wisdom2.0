class DayValidDatum < ActiveRecord::Base
  belongs_to :d_station

  scope :get_by_station_id, ->(s_id){
    where(:d_station_id => s_id) if s_id.present?
  }
  #  2017-07-17 01:00:00  --- 2017-07-18 00:00:00
  scope :get_time, ->(day_time){
    where(:day_time => (day_time.beginning_of_day + 1.hours)..((day_time + 1.days).beginning_of_day)) if day_time.present?
  }

  # scope :get_vain_count, ->(vain_num){
  #   if vain_num.present?
  #     case vain_num.to_i
  #       when 3
  #         where(:co_vain_count => (3)..(24))
  #       else
  #         where(:co_vain_count => vain_num )
  #     end
  #   end
  #
  # }

end
