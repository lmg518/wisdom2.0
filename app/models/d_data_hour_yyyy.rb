class DDataHourYyyy < ActiveRecord::Base

  #  DDataHourYyyy.get_hour_data
  def self.get_hour_data
    now_time = Time.now
    # @hour_datas = DDataYyyymm.where("data_cycle = 3600 and data_time between ? and ?","2017-05-18 17:00:00",
    #                                 "2017-05-18 17:00:00").order(:receive_accept => :desc)
    @hour_datas = DDataYyyymm.where(:data_cycle => 3600, :data_time => now_time.beginning_of_hour - 1.hours).order(:data_time => :desc)
    @data_arr = @hour_datas.map {|x| x.attributes.slice("station_id", "item_code", 'data_value', 'data_label')}
    @hour_datas.each do |f|
      SAbnormalRuleInstance.is_exception?(f.station_id, f.item_code, f.data_value, f.data_label, f.data_time, f.created_at, @data_arr, 3600)
    end
  end

end
