class TestIframe < ActiveRecord::Base

  def self.login_num

  end

  def self.sidekiq_create
    now_time = Time.now
    SidekiqMinuteWorker.perform_in(now_time)
    SidekiqHourWorker.perform_in( now_time.beginning_of_hour + 40.minutes)
    SidekiqAlarmRuleWorker.perform_in(now_time)
    SidekiqStationLostWorker.perform_in(now_time)
    SidekiqStationLengthWorker.perform_in( now_time.beginning_of_day + 1.hours)
    SidekiqStationItemavgWorker.perform_in(now_time.beginning_of_day + 1.hours + 45.minutes )
    SidekiqStationDayFlagWorker.perform_in(now_time.beginning_of_day + 1.hours + 18.minutes )
    SidekiqAqiHourWorker.perform_in(now_time.beginning_of_hour + 21.minutes )
    SidekiqAqiDayWorker.perform_in( now_time.beginning_of_day + 2.hours + 13.minutes)
  end

  def self.test
    # "黑龙江管理处	运维人员	黑龙江省	大庆市	大同区 拜梦亚	17603850532",
    [
    # "河南管理处 运维人员 河南 平顶山市	高压开关厂 李振 17603851019",
    # "河南管理处 运维人员 河南 平顶山市	规划设计院  李振 17603851019",
    # "河南管理处 运维人员 河南 平顶山市	新华旅馆 董志文 17839213109",
    # "河南管理处 运维人员 河南 平顶山市	工学院 董志文 17839213109",
    # "河南管理处 运维人员 河南 平顶山市	高新区 李振 17603851019",
    # "河南管理处 运维人员 河南 许昌市	开发区 孙万华 15537852171",
    # "河南管理处 运维人员 河南 许昌市	监测站 孙万华 15537852171",
    # "河南管理处 运维人员 河南 漯河市	三五一五 麻建勋 176003853357",
    # "河南管理处 运维人员 河南 漯河市	广电局 麻建勋 176003853357",
    # "河南管理处 运维人员 河南 漯河市	漯河大学 严田田 176003852885",
    # "河南管理处 运维人员 河南 漯河市	水利局 严田田 176003852885",
    # "河南管理处 运维人员 河南 南阳市	环保局 李赛迪 15137517015",
    # "河南管理处 运维人员 河南 南阳市	瓦房庄 李赛迪 15137517015",
    # "河南管理处 运维人员 河南 南阳市	汉画馆 李赛迪 15137517015",
    # "河南管理处 运维人员 河南 南阳市	气象站 李赛迪 15137517015",
    # "河南管理处 运维人员 河南 南阳市	理工学院 李赛迪 15137517015",
    # "河南管理处 运维人员 河南 商丘市	环境监测站 孙久恒 17603853257",
    # "河南管理处 运维人员 河南 商丘市	粮食局 孙久恒 17603853257",
    # "河南管理处 运维人员 河南 商丘市	睢阳区环保局 孙久恒 17603853257",
    # "河南管理处 运维人员 河南 信阳市	南湾水厂 黄源 17603850332",
    # "河南管理处 运维人员 河南 信阳市	平桥分局 黄源 17603850332",
    # "河南管理处 运维人员 河南 信阳市	市酿酒公司 黄源 17603850332",
    # "河南管理处 运维人员 河南 信阳市	职业技术学院/市政府 黄源 17603850332",
    # "河南管理处 运维人员 河南 周口市	市环境监测站 齐康 17603852836",
    # "河南管理处 运维人员 河南 周口市	周口师范 齐康 17603852836",
    # "河南管理处 运维人员 河南 周口市	市运管处 齐康 17603852836",
    # "河南管理处 运维人员 河南 周口市	川汇区环保局 齐康 17603852836",
    # "河南管理处 运维人员 河南 周口市	文昌生态园/港区 齐康 17603852836",
    # "河南管理处 运维人员 河南 驻马店市	市一纸厂 王家庆 17603850519",
    # "河南管理处 运维人员 河南 驻马店市	市彩印厂 王家庆 17603850519",
    # "河南管理处 运维人员 河南 驻马店市	天方二分厂 许赛男 17603851851",
    ].each do | indo|
        arr = indo.split(" ")
        grop_id = SRegionCode.find_by(:region_name => "河南管理处")
        role = SRoleMsg.find_by(:role_name => "运维人员")
        puts "////#{arr[3] + "-" + arr[4][0,2]}"
        name = arr[3] + "-" + arr[4][0,2]
        station = DStation.where("station_name like ?" , "%#{name}%").first
        next if station.blank?
        begin
          login = DLoginMsg.create( login_no: Pinyin.t(arr[5], splitter: ''),
                                    login_name: arr[5],
                                    password: "111111",
                                    password_confirmation: "111111",
                                    group_id: grop_id.id,
                                    s_role_msg_id: role.id,
                                    valid_flag: 1,
                                    telphone: arr[6].present? ? arr[6][0,11] : "")
        rescue Exception => e

          login =  DLoginMsg.find_by(:login_name => arr[5])
        end
        next if login.blank?
        next if station.d_login_msgs.present?
        station.s_area_logins.create(:d_login_msg_id => login.id)
    end
  end

end
