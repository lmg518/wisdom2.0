class SItemJob < ActiveRecord::Base

  def self.return_job(item)
    case item
      when 'so2'
        "流量报警,内部温度报警,压力报警,灯光强度报警,灯光电压报警"
      when 'no2'
        "内部温度报警,流量报警,转换炉温度报警,制冷器温度报警,臭氧报警"
      when 'co'
        "流量报警,内部温度报警,压力报警,AGC强度报警,马达转速报警"
      when 'o3'
        "A、B路流量报警,A、B路光强报警,温度报警,压力报警"
      when 'pm10'
        "流量报警,温度报警,振荡频率报警"
      when 'pm2.5'
        "流量报警,温度报警,振荡频率报警"
    end
  end

end