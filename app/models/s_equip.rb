class SEquip < ActiveRecord::Base
  has_many :s_spares #备件表
  has_many :handle_equips #设备检修表
  belongs_to :s_region_code
  belongs_to :s_area

  paginates_per(10)

  #按照车间查询
  scope :by_region, ->(region_ids) {
    where(s_region_code_id: region_ids) if region_ids.present?
  }
  #按照设备编号查询
  scope :by_equip_ids, ->(equip_ids) {
    where(id: equip_ids) if equip_ids.present?
  }

  #按照车间区域id查询
  scope :by_s_area_id, ->(s_area_id) {
    where(s_area_id: s_area_id) if s_area_id.present?
  }

  #设备状态转换
  def equip_status_str
    case equip_status
      when '0'
        return '禁用'
      when '1'
        return '启用'
      else
        ''
    end
  end

  #启用禁用
  def self.set_status(arg)
    case arg
      when '0'
        return '禁用'
      when '1'
        return '启用'
    end
  end

  #批量插入
  # SEquip.save_excel
  def self.save_excel(excel_datas)

    destination_columns = [:bit_code,
                           :equip_code,
                           :equip_name,
                           :equip_location,
                           :equip_norm,
                           :equip_nature,
                           :equip_material,
                           :equip_num,
                           :apper_code,
                           :apper_time,
                           :factory,
                           :equip_note,
                           :s_area_id,
                           :created_at,
                           :updated_at,
                           :equip_status,
                           :s_region_code_id,
                           :region_name]

    SEquip.bulk_insert(*destination_columns) do |worker|
      Rails.logger.info "=====excel_datas====#{excel_datas}===="
      excel_datas.each do |attrs|
        Rails.logger.info "导入台账信息----#{attrs}"
        worker.add attrs
      end
      #worker.add ["L-FJ0101", nil, "附防爆电机，测试数据", nil, "BZDY21-4、", "P=3KW", "碳钢", 2, "110639292 （3）", Time.now, "河南黄河防爆起重机有限公司", "测试", 2, Time.now, Time.now, 1, 6, "发酵车间"]
      #worker.add ["L-FJ0101", nil, "附防爆电机，测试数据", nil, "BZDY21-4、", "P=3KW", "碳钢", 2, "110639292（3）", Time.now, "河南黄河防爆起重机有限公司", "测试", 2, Time.now, Time.now, 1, 6, "发酵车间"]
    end



  end


end
