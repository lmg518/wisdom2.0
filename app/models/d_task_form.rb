class DTaskForm < ActiveRecord::Base
  belongs_to :d_station
  has_many :d_fault_job_details
  has_many :d_fault_handles
  belongs_to :s_region_code

  belongs_to :d_login_msg, foreign_key: :handle_man_id
  belongs_to :s_region_code_info

  has_many :d_logion_task_lines #签到表

  has_many :s_equipment_infos  #一个故障单对应2个设备信息
  has_many :s_maint_projects  #维修项目信息表
  has_many :s_equip_calibs    #校准信息表

  #设置分页
  paginates_per 10

  #按站点id查询
  scope :by_station_id, ->(station_ids) {
    where(d_station_id: station_ids) if station_ids.present?
  }

  #按完成计划时间查询
  scope :by_get_day, ->(day_time) {
    where(:polling_plan_time => day_time) if day_time.present?
  }


  #按工单号查询
  scope :by_job_no, ->(job_no) {
    where(:job_no => job_no) if job_no.present?
  }

  #按创建人查询
  scope :by_author, ->(author) {
    where(:author => author) if author.present?
  }

  #按创建时间查询
  scope :by_time, ->(created_time, end_time) {
    where(created_at: (created_time)..end_time) if created_time.present? && end_time.present?
  }

  #按当前处理人
  scope :by_handle_man, ->(handle_man) {
    where(:handle_man => handle_man) if handle_man.present?
  }


  #按当前处理人id 查询
  scope :by_handle_man_id, ->(handle_man_id) {
    where(:handle_man_id => handle_man_id) if handle_man_id.present?
  }

  #按创建类型 计划内/计划外
  scope :by_create_type, ->(create_type) {
      where(:create_type => create_type)  if create_type.present?
  }

  #按工单状态
  scope :by_job_status, ->(job_status) {
    if job_status.present?
      where(:job_status => job_status)
    else
      where(:job_status => ["audit","un_audit","wait_review"])
    end
  }

  #按工单状态job_status
  scope :by_job_status_s, ->(job_status) {
      where(:job_status => job_status) if job_status.present?
  }

  #维修处理 只显示 待维修的 审核通过的  wait_maint
  scope :by_job_status_w, ->(job_status) {
    if job_status.present?
      where(:job_status => job_status)
    else
      where(:job_status => ["audit","wait_maint"])
    end
  }

  #按工单类型
 scope :by_fault_type, ->(fault_type) {
    if fault_type.present?  
      where(:fault_type => fault_type)
    else
      where(:fault_type => ["polling_form","abnormal_form"])  #工单信息管理中显示  巡检单、异常巡检单
    end
  }

  #按任务周期
 scope :by_polling_ops_type, ->(polling_ops_type) {
  where(:polling_ops_type => polling_ops_type) if polling_ops_type.present?
  }

  #按计划完成时间
  scope :by_polling_time, ->(created_time, end_time) {
    where(polling_plan_time: (created_time)..end_time) if created_time.present? && end_time.present?
  }

  #故障任务：fault_form；异常巡检：abnormal_form；巡检单：polling_form
  def fault_type_str
    case fault_type
      when 'fault_form'
        '故障'
      when 'abnormal_form'
        '异常巡检'
      when 'polling_form'
        '巡检'
      when 'data_abnormal_form'
        '数据异常'
      else
        ''
    end
  end


  # 巡检单流程：1创建：un_write；2:填写 writing；3代审核： un_audit; 4:审核：audit 5 工单复审 review_audit 6工单待处理 wait_deal 7工单处理中 deal_with
  def job_status_str
    case job_status
      when 'un_write'
        '工单待分配'
      when 'writing'
        '创建工单'
      when 'wait_deal'
        '工单待处理'
      when 'deal_with'
        '工单处理中'
      when 'wait_maint'
        '设备待维修'
      when 'wait_calib'
        '设备待校准'
      when 'un_audit'
        '工单待审核'
      when 'audit'
        '工单已审核'
      when "wait_review"
        '工单待复核'
      when "not_review"
        '复核不通过'
      when "not_audit"
        '审核不通过'
      else
        ''
    end
  end




  def polling_ops_type_str
    case polling_ops_type
      when 7
        '周'
      when 30
        '月'
      when 90
        "季"
      when 360
        '半年'
      else
        ''
    end
  end

  #app, sms, email
  def send_type_str
    return "" unless send_type.present?
    @send_arr = send_type.split(",")
    send_arr_length = @send_arr.length
    @arr = []
    case send_arr_length
      when 1
        @arr << DTaskForm.send_handle_str(@send_arr[0])
      when 2
        2.times do |i|
          @arr <<   DTaskForm.send_handle_str(@send_arr[i])
        end
      when 3
        3.times do |i|
          @arr <<  DTaskForm.send_handle_str(@send_arr[i])
        end
    end
    @arr
  end

  def self.send_handle_str(val)
    case val
      when 'app'
        'app推送'
      when 'sms'
        '短信'
      when 'email'
        "邮件"
      else
        ''
    end
  end

  def create_type_str
    case create_type
      when "plan_out"
        '计划外'
      when "plan_in"
        '计划内'
      else
        ''
    end
  end

  def urgent_level_str
    case urgent_level
      when 'I'
        '一般'
      when 'II'
        '中等'
      when 'III'
        '紧急'
      else
        ''
    end
  end

  #按照工作周期获得工单信息
  def self.type_polling(arge)
    @polling_work = []
    4.times do |i|
        num = [7,30,90,360]
        work = {polling_type: num[i],polling_work: arge.select{|x| x.polling_ops_type == num[i]}}
        @polling_work << work
    end
    @polling_work
  end


end
