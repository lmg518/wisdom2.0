class FileCategory < ActiveRecord::Base
  acts_as_nested_set
  has_many :file_maintenances

end
