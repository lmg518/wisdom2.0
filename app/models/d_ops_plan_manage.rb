class DOpsPlanManage < ActiveRecord::Base
  paginates_per 10
  belongs_to :d_station
  belongs_to :s_administrative_area
  belongs_to :s_region_code_info
  has_many :d_ops_plan_details

  #按周固定查询
  scope :s_by_week, ->(begin_time) {
    where(:week_begin_time => (begin_time)) if begin_time.present?
  }

  #按运维单位
  scope :by_unit_name_id, ->(unit_name_id) {
    where(:s_region_code_info_id => (unit_name_id)) if unit_name_id.present?
  }
  #按城市模糊查询
  scope :by_zone_name, ->(zone_name) {
    where("zone_name like ?","%#{zone_name}%") if zone_name.present?
  }

  #按站点id
  scope :by_ops_info, ->(info_id) {
    where(s_region_code_info: info_id) if info_id.present?
  }
  #按状态查询
  scope :by_edit_status, ->(edit_status) {
    where(:edit_status => edit_status) if edit_status.present?
  }

  #转换计划状态显示  #Y 待审核 O 待上报  W 已审核  N 未制定
  def edit_status_str
    case self.edit_status
    when 'Y'
        "待审核"
    when 'O'
        "待上报"
    when 'W'
        "已审核"
    when 'N'
        "未制定"
    else
      ''
    end
end


  #计划主表创建提交成功后同时创建誉为任务时间表
  after_commit on: [:create]  do |document|
    work_pros = DWorkPro.active
    work_pros.each do |pros|
      document.d_ops_plan_details.create!(
          job_name: pros.work_name,
          job_flag: pros.work_type,
          d_work_pro_id: pros.id,
          job_checked: "N",
      )
    end
  end

end
