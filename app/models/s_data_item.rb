class SDataItem < ActiveRecord::Base

  # SDataItem.add_items
  def self.add_items
    item_ids = %W(101 102 103 104 105 106 107 108 201 202 203 204 205 206)
    item_name = %W(SO2 NO2 NO NOX CO O3 PM2.5 PM10 风速 风向 气温 气压 湿度 能见度)
    item_data_unit =%w(μg/m3 μg/m3 μg/m3 μg/m3 mg/m3 μg/m3 μg/m3 μg/m3 m/s ° ℃ hPa % km)
    item_ids.each_with_index do |ids,index|
      SDataItem.create!(:item_code => ids, :item_name => item_name[index], :data_unit => item_data_unit[index] )
    end
  end

end
