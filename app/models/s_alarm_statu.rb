class SAlarmStatu < ActiveRecord::Base
#status : waitting:待处理  cancel：以取消 receive: 已接收 done：已处理

  def self.test
    SAlarmStatu.find_or_create_by!(:status => 'waitting',:status_name => "待处理")
    SAlarmStatu.find_or_create_by!(:status => 'cancel',:status_name => "以取消")
    SAlarmStatu.find_or_create_by!(:status => 'receive',:status_name => "已接收")
    SAlarmStatu.find_or_create_by!(:status => 'done',:status_name => "已处理")
  end
end
