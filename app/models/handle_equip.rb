class HandleEquip < ActiveRecord::Base

  belongs_to :s_equip #设备表
  has_many :h_overhaul_notes #检修记录表
  has_one :h_equip_spare #更换备件表
  has_many :h_equip_boots #试车记录

  paginates_per 10

  #按照设备查询
  scope :by_equip, ->(equip_ids) {
    where(s_equip_id: equip_ids) if equip_ids.present?
  }
  #按照时间查询
  scope :by_create_time, ->(begin_time, end_time) {
    where(created_at: begin_time..end_time) if begin_time.present? && end_time.present?
  }

  #按照维修开始时间、维修结束时间 查询
  scope :by_handle_time, ->(begin_time, end_time) {
    where("handle_start_time >= ? AND handle_end_time <= ?",begin_time,end_time) if begin_time.present? && end_time.present?
  }

  #按照状态查询
  scope :by_handle_status, ->(status) {
    where(:status => status) if status.present?
  }

  #状态转换
  def status_str
    case status
      when 'wait_issued'
        return '待下发'
      when 'wait_carried'
        return '待执行'
      when 'tamp_save'
        return '待维修'
      when 'boot_save'
        return '待试车'
      when 'wait_review'
        return '待审核'
      when 'carry_out'
        return '完成'
      else
        ''
    end
  end

  def self.set_status(arg)
    case arg
      when 'wait_issued'
        return '待下发'
      when 'wait_carried'
        return '待执行'
      when 'tamp_save'
        return '待维修'
      when 'boot_save'
        return '待试车'
      when 'wait_review'
        return '待审核'
      when 'carry_out'
        return '完成'
    end
  end

  #判断下发后时间是否超出一半还未执行
  def self.handle_remind(arg)
    if arg.status == 'wait_issued' || arg.status == 'wait_carried'
      time = Time.now()
      start_time = arg.handle_start_time
      end_time = arg.handle_end_time
      diff_time = (end_time - start_time) / 2
      t = start_time + diff_time
      if time > t
        return 'red'
      end
    end
  end

  #检修记录转成中文
  def self.set_note_type(arg)
    case arg
      when 'overhaul_begin'
        return '检修前'
      when 'overhaul_end'
        return '检修后'
    end
  end

  #试车结果
  def self.set_check_status(arg)
    case arg
      when 'Y'
        return '正常'
      when 'N'
        return '异常'
    end
  end

end
