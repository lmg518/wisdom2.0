class SNoticeRule < ActiveRecord::Base

  paginates_per 10

  belongs_to :s_notice_template

  # validates :notice_type, presence: true, uniqueness: {message:'通知方式已存在'}

  has_many :notice_rule_roles
  has_many :s_role_msgs, through: :notice_rule_roles

  def json_has
    begin
      @alarm_level = SAlarmLevel.find_by_alarm_level(alarm_level).level_name
      @role = self.s_role_msgs
    rescue Exception => e
      @alarm_level =  ""
      @role = []
    end
     return { id: id, alarm_level: @alarm_level,
              notice_type: notice_type,
              role_id:  @role.pluck(:id),
              role_name:  @role.pluck(:role_name).join(","),
              up_region_level:  @region_level}
  end

end
