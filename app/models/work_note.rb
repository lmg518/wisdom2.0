class WorkNote < ActiveRecord::Base

  belongs_to :d_equip_work


  def self.set_status(arg)
    case arg
      when 'created'
        return '创建'
      when 'issued'
        return '下发'
      when 'deal_with'
        return '处理'
      when 'carry_out'
        return '完成'
    end
  end
end
