class SEquipmentInfo < ActiveRecord::Base
    belongs_to :d_station   #一个站点有多个设备信息
    
    belongs_to :d_task_form  #一个设备信息对应一个故障单（更换备机时）
    #设置分页
    paginates_per 10

    #设备类型转换
    def types_str
        case types
            when 'Y'
            '正式'
            when 'N'
            '备机'
            when 'O'
            '更换'
            else
            ''
        end
    end

    scope :by_equip_brands, ->(equip_brands) {
        where(:equip_brands => equip_brands) if equip_brands.present?
    }

    scope :by_equip_nums, ->(equip_nums) {
        where(:equip_nums => equip_nums) if equip_nums.present?
    }

    scope :by_equip_type, ->(equip_type) {
        where(:equip_type => equip_type) if equip_type.present?
    }

    

end
