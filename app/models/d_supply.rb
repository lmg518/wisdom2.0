class DSupply < ActiveRecord::Base

  scope :s_by_name, ->(supply_name){
    where("supply_name like ?", "%#{supply_name}%") if supply_name.present?
  }

  scope :s_by_no, ->(supply_no){
    where("supply_no like ?", "%%#{supply_no}") if supply_no.present?
  }

  scope :s_by_brand, ->(brand){
    where(:brand => brand) if brand.present?
  }

  scope :s_by_unit_ids, ->(unit_id){
    where(:s_region_code_info_id => unit_id) if unit_id.present?
  }


end
