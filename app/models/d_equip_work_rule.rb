class DEquipWorkRule < ActiveRecord::Base
 paginates_per 10

#按照车间查询
scope :by_region_id, ->(region_id) {
    where(s_region_code_id: region_id) if region_id.present?
}

  
#按照时间查询
scope :by_create_time, ->(begin_time,end_time){
    where(created_at: begin_time..end_time ) if begin_time.present? && end_time.present?
}

#按照计划点检 开始时间、计划点检 结束时间 查询
scope :by_work_time, ->(begin_time, end_time) {
    where("work_start_time >= ? AND work_end_time <= ?",begin_time,end_time) if begin_time.present? && end_time.present?
}


end
