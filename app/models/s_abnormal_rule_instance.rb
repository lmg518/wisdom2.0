include SAbnormalRuleInstancesHelper
class SAbnormalRuleInstance < ActiveRecord::Base

  paginates_per 10

  has_many :d_station_rule_rels
  has_many :d_stations, through: :d_station_rule_rels
  belongs_to :s_abnormal_ltem
  belongs_to :s_abnormmal_rule

  has_many :d_alarm_abnormal_rels
  has_many :d_alarm_details, through: :d_alarm_abnormal_rels

  validates :instance_name, presence: true, uniqueness: {message:'实例名称已存在'}
  # validates :rule_code, presence: true
  # validates :s_abnormmal_rule_id, presence: true

  scope :active, -> {
    order(:created_at => 'desc')
  }
#按站点查
  scope :by_station_id, ->(station_id) {
    where(:id => DStationRuleRel.where(:d_station_id => station_id).pluck(:s_abnormal_rule_instance_id)) if station_id.present? && station_id[0] != ""
  }
#按因子id
  scope :by_abnormal_items, -> (item_id) {
    where(:s_abnormal_ltem_id => item_id) if item_id.present?
  }
#按规则代码
  scope :by_abnormal_rule_name, -> (rule_name) {
    where(:s_abnormmal_rule_id => SAbnormmalRule.where('rule_name like ?', "%#{rule_name}%").pluck(:id)) if rule_name.present?
  }
#按名字查询
  scope :by_instance_name, ->(instance_name) {
    where('instance_name like ?', "%#{instance_name}%") if instance_name.present?
  }

  # def self.test
  #   rule_info = SAbnormmalRule.create( rule_code: "SLOST",
  #                          rule_name: "站点离线",
  #                          rule_expression_des: "站点离线",
  #                          compare_code: "", compare_code2: "", comoare_code3: "", parameter_num: 0, note: "")
  #   SAbnormalRuleInstance.create( instance_name: "站点离线",
  #                                  parameter_value: 0,
  #                                  status: "是", note: "",
  #                                  s_abnormmal_rule_id: rule_info.id)
  # end

  def get_index
    ltem = s_abnormal_ltem
    rule = s_abnormmal_rule
    return {id: id, instance_name: instance_name,
            item_name: ltem.present? ? ltem.item_name : '',
            rule_name: rule.present? ? rule.rule_name : '',
            status: status, local_station: d_stations}
  end

# H：有效数据不足 BB：连接不良 B：运行不良 W：等待数据恢复 HSp：数据超上限 LSp：数据超下限
# PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
# NU: 无数据（-99） TH1:突然高/低 TH2：突然高/低 （变化率）DBG1：对标高/低 DBG2：对标高/低 （变化率）
# KDG：颗粒物倒挂
#因子值：yz_val  第一个值： first_val  第二个值:second_val
  def self.is_exception?(station_id, item_code, item_val, flag_str, new_time, create_time, data_arr,data_cycle)
    flag_str = flag_str.present? ? flag_str : ''
    ltem = SAbnormalLtem.find(item_code) #因子
    station = DStation.find(station_id) #站点
    # puts "?????????#{station.s_abnormal_rule_instances.inspect}"
    abnormal_rule_instances = station.s_abnormal_rule_instances.where(:s_abnormal_ltem_id => ltem.id) #实例s
    yz_val = item_val.to_f
    flag_arr = abnormal_rule_instances.map {|x| x.s_abnormmal_rule.ab_lable} #获取实例模版规则的标
     # puts "异常规则标示##{flag_arr},小时数据因子标示#{flag_arr},abnormal_rule_instances>>>#{abnormal_rule_instances.map{|x| puts x.id}}??????^^^%%%%&&&&&&"
    if flag_arr.include? flag_str
      # puts ">>>>>>>>>>>>>>>>true"
      handle_true(data_arr, abnormal_rule_instances, ltem, flag_str, station_id, item_val, new_time, create_time,data_cycle)
    else #标示不存在
      # puts ">>>>>>>>>>>>>>>>false"
      handle_false(data_arr, abnormal_rule_instances, ltem, station_id, item_val, new_time, create_time,data_cycle)
    end
  end

  private
  def self.handle_false(data_arr, rule_instances, item_code, station_id, item_val, new_time, create_time,data_cycle)
    # puts ">>>>>>>>>>>>>>>>#{rule_instances.inspect}"
    rule_instances.each do |rule|
      abnormal_rule = rule.s_abnormmal_rule #规则模版
      # puts ">>>>>>>>>false2>>>>>>>#{abnormal_rule.ab_lable}>>>#{item_code.inspect}>>>#{abnormal_rule.ab_lable}"
      case abnormal_rule.ab_lable
        #进行异常判定
        when 'KDG'
          handle_kdg(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'ZERO'
          handle_zero(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'TRG'
          handle_trg(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'TRD'
          handle_trd(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'DBGG'
          handle_dbgg(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'DBGD'
          handle_dbgd(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when "DING"
          handle_ding(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        # when "LOST"
        #   handle_lost(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'TH1'
          handle_th1(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'TH2'
          handle_th2(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'DBG1'
          handle_dbg1(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when 'DBG2'
          handle_dbg2(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        when "D2DC1B"
          handle_2_days_abnormal(item_code, rule, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
      end
      next
    end
  end

# H：有效数据不足 BB：连接不良 B：运行不良 W：等待数据恢复 HSp：数据超上限 LSp：数据超下限
# PS：跨度检查 PZ：零点检查 AS：精度检查 CZ：零点校准 CS：跨度校准 RM：自动或人工审核为无效
# NU: 无数据（-99）
  def self.handle_true(data_arr, rule_instances, item_code, flag_strs, station_id, item_val, new_time, create_time,data_cycle)
    first_val, second_val = 0
    rule_instances.each do |instacne|
      abnormal_rule = instacne.s_abnormmal_rule #规则模版
      #如果flag_str 存在并且和模版里的标示一样
      if flag_strs.include? abnormal_rule.ab_lable
        case flag_strs
          when 'H'
            handle_h(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'BB'
            handle_bb(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'B'
            handle_b(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'W'
            handle_w(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'HSp' #超上限
            handle_hsp(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'LSp'
            handle_lsp(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'PS'
            handle_ps(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'PZ'
            handle_pz(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          #
          # when 'AS'
          #   handle_as(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          # when 'CZ'
          #   handle_cz(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          # when 'CS'
          #   handle_cs(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          #
          when 'RM'
            handle_rm(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
          when 'NU'
            handle_null(item_code, instacne, station_id, new_time, create_time, item_val, abnormal_rule, data_arr, data_cycle)
        end
        next
      end
    end
  end

end
