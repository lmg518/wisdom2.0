class WcDDataDayHandle < WebCralerRecod
  now_time = Time.zone.now
  puts now_time
  self.table_name = "dDataDay#{now_time.strftime("%Y%m")}"
  # WcDDataDayHandle.add_in_wc_data_day
  def self.add_in_wc_data_day
    stations = DStation.pluck(:id)
    stations.each do |station|
      now_time = Time.now
      @day_data = where(:STATION_ID => "#{station}A", :DATA_TIME => (((now_time - 3.days).beginning_of_day + 1.hours).strftime("%Y-%m-%d %H"))..((now_time - 2.days).beginning_of_day.strftime("%Y-%m-%d %H")))
      #so2: 101 no2: 102 co:105 o3:106 pm2.5: 107 pm10: 108
      @so2 = WcDDataDayHandle.day_data_handle("101")
      @no2 = WcDDataDayHandle.day_data_handle("102")
      @co = WcDDataDayHandle.day_data_handle("105")
      @o3 = WcDDataDayHandle.day_data_handle("106")
      @pm2_5 = WcDDataDayHandle.day_data_handle("107")
      @pm10 = WcDDataDayHandle.day_data_handle("108")
      DayValidDatum.create!(
          d_station_id: station,
          station_name: DStation.find(station).station_name,
          so2_avg: @so2[:item_avg],
          so2_val_max: @so2[:item_max].present? ? @so2[:item_max].DATA_VALUE : 0.00,
          so2_val_min: @so2[:item_min].present? ? @so2[:item_min].DATA_VALUE : 0.00,
          so2_count: @so2[:item].length,
          so2_valid_count: @so2[:item_valid_count],
          so2_vain_count: @so2[:item_vain_count],
          no2_avg: @no2[:item_avg],
          no2_val_max: @no2[:item_max].present? ? @no2[:item_max].DATA_VALUE : 0.00,
          no2_val_min: @no2[:item_min].present? ? @no2[:item_min].DATA_VALUE : 0.00,
          no2_count: @no2[:item].length,
          no2_valid_count: @no2[:item_valid_count],
          no2_vain_count: @no2[:item_vain_count],
          o3_avg: @o3[:item_avg],
          o3_val_max: @o3[:item_max].present? ? @o3[:item_max].DATA_VALUE : 0.00,
          o3_val_min: @o3[:item_min].present? ? @o3[:item_min].DATA_VALUE : 0.00,
          o3_count: @o3[:item].length,
          o3_valid_count: @o3[:item_valid_count],
          o3_vain_count: @o3[:item_vain_count],
          co_avg: @co[:item_avg],
          co_val_max: @co[:item_max].present? ? @co[:item_max].DATA_VALUE : 0.00,
          co_val_min: @co[:item_min].present? ? @co[:item_min].DATA_VALUE : 0.00,
          co_count: @co[:item].length,
          co_valid_count: @co[:item_valid_count],
          co_vain_count: @co[:item_vain_count],
          pm2_5_avg: @pm2_5[:item_avg],
          pm2_5_val_max: @pm2_5[:item_max].present? ? @pm2_5[:item_max].DATA_VALUE : 0.00,
          pm2_5_val_min: @pm2_5[:item_min].present? ? @pm2_5[:item_min].DATA_VALUE : 0.00,
          pm2_5_count: @pm2_5[:item].length,
          pm2_5_valid_count: @pm2_5[:item_valid_count],
          pm2_5_vain_count: @pm2_5[:item_vain_count],
          pm10_avg: @pm10[:item_avg],
          pm10_val_max: @pm10[:item_max].present? ? @pm10[:item_max].DATA_VALUE : 0.00,
          pm10_val_min: @pm10[:item_min].present? ? @pm10[:item_min].DATA_VALUE : 0.00,
          pm10_count: @pm10[:item].length,
          pm10_valid_count: @pm10[:item_valid_count],
          pm10_vain_count: @pm10[:item_vain_count],
          day_time: Time.zone.now,
      )
    end
  end

  private

  def self.day_data_handle(item_code)
    item_data = @day_data.select {|x| x.ITEM_CODE == item_code}
    item_data_max = item_data.max_by {|x| x.DATA_VALUE}
    item_data_min = item_data.min_by {|x| x.DATA_VALUE}
    @so2_valid, @so2_unvalid = 0, 0
    item_data.each do |item|
      case item.STATUS.to_i
        when 3
          @so2_valid += 1
        when 0
          if item.EXT.blank?
            @so2_valid += 1
          else
            @so2_unvalid += 1
          end
        else
          @so2_unvalid += 1
      end
    end
    return {item: item_data,
            item_avg: item_data.present? ? (item_data.map {|x| x.DATA_VALUE.to_f}.sum / item_data.length.to_f).round(2) : 0,
            item_max: item_data_max,
            item_min: item_data_min,
            item_valid_count: @so2_valid,
            item_vain_count: @so2_unvalid}
  end

end