class SAlarmRule < ActiveRecord::Base

  paginates_per 10

  #报警规则
  # s_alarm_rules  报警规则	规则代码	rule_code	string
  # 报警分类	rule_type	string
  # 规则描述	rule_desc	string
  # 异常规则实例	ab_rule_instance	string
  # 次数	判断频率	deal_interval	integer
  # (取消使用)数据采样量	sample_num	integer
  # 状态	status	string
  # 规则名称	rule_name	string
  # 规则实例id	rule_instance_id	integer
  # 报警分类id	rule_type_id	integer

  belongs_to :s_abnormal_rule_instance, foreign_key: "rule_instance_id"
  belongs_to :s_alarm_type, foreign_key: "rule_type_id"
  has_many :d_alarms
  has_many :s_alarm_rule_levels

  # validates :rule_code, presence: true, uniqueness: {message:'规则代码已存在'}

  scope :by_rule_name, -> (rule_name) {
    where("rule_name like ?", "%#{rule_name}%") if rule_name.present?
  }

  scope :by_rule_type_id, -> (rule_type_id) {
    where(:rule_type_id => rule_type_id) if rule_type_id.present?
  }

  scope :by_rule_instance, -> (rule_instance) {
    where("ab_rule_instance like ?", "%#{rule_instance}%") if rule_instance.present?
  }

  # 每分钟执行一次最新异常查询，发现新的异常生成一条报警记录
  # SAlarmRule.check_abnormal_datas
  def self.check_abnormal_datas
    @station_exceptions = []
    @data_count = 0
    @level1, @level2, @level3 = 0, 0, 0
    @now_time = Time.now
    @stations = DStation.active
    @stations.each do |station|
      #  站点该小时的所有异常
      @data_abnormals = station.d_abnormal_data_yyyymms.where(:create_acce => (@now_time.beginning_of_hour)..(@now_time.end_of_hour))
      next if @data_abnormals.blank?
      @data_abnormals.each do |abnormal|
        @abnormal_instance1 = abnormal.s_abnormal_rule_instance # 异常实例
        @abnormal_rule1 = @abnormal_instance1.s_abnormmal_rule # 异常规则
        @alarm_rule = SAlarmRule.find_by_rule_instance_id(@abnormal_instance1.id) #异常实例的报警规则
        next if @alarm_rule.blank?
        @alarm_levels = @alarm_rule.s_alarm_rule_levels #报警级别
        if @alarm_levels.present?
          @alarm_levels.each do |level|
            if level.alarm_level.to_i == 1
              @level1 = level.ab_times.to_i #通知级别次数
            end
            if level.alarm_level.to_i == 2
              @level2 = level.ab_times.to_i
            end
            if level.alarm_level.to_i == 3
              @level3 = level.ab_times.to_i
            end
          end
        end
        #站点的该时段的报警
        @d_alarms = station.d_alarms.where(:first_alarm_time => (abnormal.create_acce.beginning_of_hour)..(abnormal.create_acce.end_of_hour),
                                           :s_alarm_rule_id => @alarm_rule.id)
        if @d_alarms.present?
          @d_alarms.each do |alarm|
            alarm.update(:last_alarm_time => abnormal.create_acce, :continuous_alarm_times => @d_alarms.length)
            alarm.d_alarm_details.first.update(:last_sample_time => abnormal.create_acce) if alarm.d_alarm_details.present?
          end
        else
          #  没有报警直接产生一条报警,根据异常判断报警的级别
          @data_count = @data_abnormals.length
          if @data_count <= @level1
            @level_size = 1
          elsif @data_count <= @level2
            @level_size = 2
          elsif @data_count <= @level3
            @level_size = 3
          end
          @d_alarms.create(alarm_rule: @alarm_rule.rule_name, alarm_level: @level_size,
                           first_alarm_time: abnormal.create_acce, continuous_alarm_times: 1, status: 'waitting', s_alarm_rule_id: @alarm_rule.id)
        end
      end
    end
  end
  # def self.check_abnormal_datas
  #   @station_exceptions = []
  #   @data_count = 0
  #   @level1, @level2, @level3 = 0,0,0
  #   @now_time = Time.now
  #   # @data_abnormals = DAbnormalDataYyyymm.where(:create_acce => "2017-05-17 10:00:00")
  #   @data_abnormals = DAbnormalDataYyyymm.where(:create_acce => (@now_time.beginning_of_hour)..(@now_time.end_of_hour))
  #   if @data_abnormals.blank?
  #     @data_abnormals = DAbnormalDataYyyymm.where(:create_acce => (@now_time.beginning_of_minute - 2.minutes)..(@now_time))
  #   end
  #   @data_abnormals.each_with_index do |abnormal, index| #所有站点异常数据
  #     @station1 = abnormal.d_station
  #     # puts "@station1===#{@station1.inspect}"
  #     @abnormal_instance1 = abnormal.s_abnormal_rule_instance
  #     # puts "@@abnormal_instance1===#{@abnormal_instance1.inspect}"
  #     @abnormal_rule1 = @abnormal_instance1.s_abnormmal_rule
  #     # puts "@@@abnormal_rule1===#{@abnormal_rule1.inspect}"
  #     @alarm_rule = SAlarmRule.find_by_rule_instance_id(@abnormal_instance1.id)
  #     # puts "@@@@alarm_rule===#{@alarm_rule.inspect}"
  #     next if @alarm_rule.blank?
  #     @alarm_levels = @alarm_rule.s_alarm_rule_levels
  #     # @station_exceptions << {station_id:station.id, abnormal_instance_id: abnormal_instance.id, abnormal_rule: abnormal_rule, ab_data_time:"",}
  #     if @alarm_levels.present?
  #       @alarm_levels.each do |level|
  #         if level.alarm_level.to_i == 1
  #           @level1 = level.ab_times.to_i    #通知级别次数
  #         end
  #         if level.alarm_level.to_i == 2
  #           @level2 = level.ab_times.to_i
  #         end
  #         if level.alarm_level.to_i == 3
  #           @level3 = level.ab_times.to_i
  #         end
  #       end
  #     end
  #     # puts "alarm_levels ===>#{@level1},#{@level2},#{@level3}"
  #     @d_alarms = @station1.d_alarms.where(:first_alarm_time => (abnormal.create_acce.beginning_of_hour)..(abnormal.create_acce.end_of_hour))
  #     # puts ">>>>#{@d_alarms.inspect}"
  #     if @d_alarms.present?
  #       #如果存在报警
  #       # puts "alarm  update"
  #       @d_alarms.each do |alarm|
  #         s_abnormal_rule_instance = alarm.s_alarm_rule.s_abnormal_rule_instance
  #         alarm_data_yy_mms = alarm.d_station.d_abnormal_data_yyyymms.where(:create_acce => (abnormal.create_acce.beginning_of_hour)..(abnormal.create_acce.end_of_hour),
  #                                               :item_code_id => s_abnormal_rule_instance.s_abnormal_ltem_id,
  #                                               :s_abnormal_rule_instance_id => s_abnormal_rule_instance.id ).length
  #         alarm_data = alarm.update(:last_alarm_time => abnormal.create_acce,:continuous_alarm_times => alarm_data_yy_mms.length)
  #         alarm.d_alarm_details.first.update(:last_sample_time => abnormal.create_acce) if alarm.d_alarm_details.present?
  #       end
  #     else
  #       # puts "alarm  cerete"
  #        @data_abnormals.map{|x| @data_count += 1 if x.id == @alarm_rule.rule_instance_id}
  #       if @data_count <= @level1
  #         @level_size = 1
  #       elsif @data_count <= @level2
  #         @level_size = 2
  #       elsif @data_count <= @level3
  #         @level_size = 3
  #       end
  #       @d_alarms.create(alarm_rule: @alarm_rule.rule_name, alarm_level: @level_size,
  #                               first_alarm_time: abnormal.create_acce, continuous_alarm_times: 1, status: 'waitting',s_alarm_rule_id: @alarm_rule.id)
  #     end
  #
  #   end
  #   # @excepton_map =@station_exceptions.map{|x| {station_id: x.station_id,abnormal_instance_id: x.abnormal_instance_id,ab_data_time: x.ab_data_time}}
  # end

end
