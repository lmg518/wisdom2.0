include ItemIaqiAqiHelper
class DDataDaily < ActiveRecord::Base

  # 站点aqi日报数据

  belongs_to :d_station, :foreign_key => :station_id

  #  DDataDaily.day_avg_create
  def self.day_avg_create
    now_time = Time.now
    @data_time = (now_time.beginning_of_day - 1.days)
    DStation.find_each(batch_size: 230) do |station|
      @AQI_arr = []
      data_yy_mms = station.d_data_yyyymms.where(:data_cycle => 3600, :data_time => (@data_time.beginning_of_day)..(@data_time.end_of_day))
      next if data_yy_mms.blank?
      so2 = data_yy_mms.select {|x| x.item_code.to_i == 101}
      no2 = data_yy_mms.select {|x| x.item_code.to_i == 102}
      co = data_yy_mms.select {|x| x.item_code.to_i == 105}
      o3_8hour = station.d_data_yyyymms.where(:data_cycle => 3600, :item_code => "106", :data_time => ((@data_time.end_of_day - 8.hours))..(@data_time.end_of_day)).average("data_value")
      o3 = data_yy_mms.select {|x| x.item_code.to_i == 106}
      pm25_24 = data_yy_mms.select {|x| x.item_code.to_i == 107}
      pm10 = data_yy_mms.select {|x| x.item_code.to_i == 108}
      so2_sum, no2_sum, co_sum, o3_8hour_sum, o3_sum, pm25_24_sum, pm10_sum = 0, 0, 0, 0, 0, 0, 0
      so2.map {|x| so2_sum += x.data_value.to_f}
      no2.map {|x| no2_sum += x.data_value.to_f}
      co.map {|x| co_sum += x.data_value.to_f}
      pm25_24.map {|x| pm25_24_sum += x.data_value.to_f}
      pm10.map {|x| pm10_sum += x.data_value.to_f}
      o3_hour_arr = []
      o3.map {|x| o3_hour_arr << x.data_value.to_f}
      day_yyys = station.d_data_dailies.build(
          station_name: station.station_name,
          data_time: @data_time,
          avg_so2: so2.present? ? (so2_sum / so2.length.to_f).round(2) : 0,
          iaqi_so2: so2.present? ? handle_so2((so2_sum / so2.length.to_f).round(2)) : '',
          avg_no2: no2.present? ? (no2_sum / no2.length.to_f).round(2) : 0,
          iaqi_no2: no2.present? ? handle_no2((no2_sum / no2.length.to_f).round(2)) : '',
          avg_co: co.present? ? (co_sum / co.length.to_f).round(2) : 0,
          iaqi_co: co.present? ? handle_co((co_sum / co.length.to_f).round(2)) : '',
          max_avg_1h_o3: o3_hour_arr.present? ? o3_hour_arr.max : 0,
          iaqi_o3: o3_hour_arr.present? ? handle_o3(o3_hour_arr.max) : '',
          max_ma8_o3: o3_8hour.present? ? o3_8hour.round(2) : 0,
          iaqi_ma8_o3: o3_8hour.present? ? handle_hour8_o3(o3_8hour.round(2)) : '',
          avg_pm10: pm10.present? ? (pm10_sum / pm10.length.to_f).round(2) : 0,
          iaqi_pm10: pm10.present? ? handle_24_pm10((pm10_sum / pm10.length.to_f).round(2)) : '',
          avg_pm25: pm25_24.present? ? (pm25_24_sum / pm25_24.length.to_f).round(2) : 0,
          iaqi_pm25: pm10.present? ? handle_24_pm2_5((pm10_sum / pm10.length.to_f).round(2)) : '',
          aqi: @AQI_arr.present? ? @AQI_arr.max : 0,
          chief_pollutant: nil,
          aqi_level: aqi_days(@AQI_arr.max),
      )
      day_yyys.chief_pollutant = chief_pollutant_add(day_yyys)
      day_yyys.save!
    end

  end

  def self.chief_pollutant_add(hourly_yyyys)
    item_arr = [hourly_yyyys.iaqi_so2, hourly_yyyys.iaqi_no2, hourly_yyyys.iaqi_co,
                hourly_yyyys.iaqi_o3, hourly_yyyys.iaqi_ma8_o3, hourly_yyyys.iaqi_pm10,
                hourly_yyyys.iaqi_pm25]
    item_info = ["so2", "no2", "co", "o3", "o3", 'pm10', 'pm2.5']
    item_arr_max = item_arr.max
    item_index = item_arr.index(item_arr_max)
    item_info[item_index]
  end

  #<DDataDaily
  # id: nil,
  # station_id: nil,
  # station_name: nil,
  # data_time: nil,
  # avg_so2: nil,
  # iaqi_so2: nil,
  # avg_no2: nil,
  # iaqi_no2: nil,
  # avg_co: nil,
  # iaqi_co: nil,
  # max_avg_1h_o3: nil,
  # iaqi_o3: nil,
  # max_ma8_o3: nil,
  # iaqi_ma8_o3: nil,
  # avg_pm10: nil,
  # iaqi_pm10: nil,
  # avg_pm25: nil,
  # iaqi_pm25: nil,
  # aqi: nil,
  # chief_pollutant: nil,
  # aqi_level: nil,
  # aqi_class: nil,
  # aqi_color: nil,
  # created_at: nil,
  # updated_at: nil>
  scope :by_data_time, ->(begin_time,end_time){
    where(data_time: begin_time..end_time) if begin_time.present? && end_time.present?
  }
end
