class DAlarmDetail < ActiveRecord::Base
  belongs_to :d_alarm, foreign_key: 'alarm_detail_id'

  # has_many :d_alarm_abnormal_rels
  # has_many :s_alarm_rules, through: :d_alarm_abnormal_rels

  def alarm_level_to_s
    case alarm_level
      when 3
        "III级"
      when 2
        "II级"
      when 1
        "I级"
    end
  end

end
