class DDataAnalyze < ActiveRecord::Base
    belongs_to :d_station
	
	#设置分页
    paginates_per 10


    #首页按时间查询小时数据
    scope :by_hour_data_time, ->(data_time) {
        where(data_time: data_time) if data_time.present?
    }

    #首页按状态查询小时数据
    scope :by_status, ->(status) {
        where(status: status) if status.present?
    }

    #详细页面按时间查询5min数据
    scope :by_data_time, ->(begin_time, end_time) {
        where(data_time: (begin_time)..end_time) if begin_time.present? && end_time.present?
    }
    #按站点id查询
    scope :by_station_id, ->(station_id) {
        where(d_station_id: station_id) if station_id.present?
    }

end
