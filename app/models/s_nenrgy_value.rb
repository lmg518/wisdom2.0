class SNenrgyValue < ActiveRecord::Base
    
    #按创建时间查询
    scope :by_datetime, ->(created_time, end_time) {
        where(datetime: (created_time)..end_time) if created_time.present? && end_time.present?
    }

    #按车间id 查询
    scope :by_region_ids, ->(code_ids) {
    where(:s_region_code_id => code_ids) if code_ids.present?
    }

    # 通过物料 级别 报表 时间  取值
    scope :by_time_form_region_code, ->(regio_code_info_id,report_form_id,file_code_info,data_info){
        find_by(:s_region_code_id => regio_code_info_id, :d_report_form_id => report_form_id,:field_code => file_code_info, :datetime => data_info)
    }


    #不进行差值计算的方法 SNenrgyValue
    def self.js_method_by_time_region_id_form_id(time, s_region_code_id, d_report_form_id)
        
        month_begin = Time.parse(time).beginning_of_month.strftime("%Y-%m-%d")   #当前月的开始日期
        @form_values = SNenrgyValue.where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => time)  
        @form_values_month = SNenrgyValue.by_datetime(month_begin, time).where(:s_region_code_id => s_region_code_id, :d_report_form_id => d_report_form_id, :datetime => time)      
        
        #计算日产量
        if @form_values.present? && @form_values.length >0
        @form_values.each do |m|
            steam_values += m.field_value.to_f
            end
            Rails.logger.info "3333333#{steam_values}"
        else
            steam_values = 0
            Rails.logger.info "44444444#{steam_values}"
        end

        #计算月累计
        if @form_values_month.present? && @form_values_month.length >0
        @form_values_month.each do |m|
            steam_month_sum += m.field_value.to_f
            end
        else
            steam_month_sum  = 0
        end

        @steam_month_sum = steam_month_sum #累计用量
        @steam_values = steam_values          #当天的日用量
    end


end
