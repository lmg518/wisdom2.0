class DAbnormalDataYyyymm < ActiveRecord::Base
  # 异常id	abnormal_id	string
  # 站点ID	station_id	integer
  # 异常规则实例	rule_instance	string
  # 生成时间	create_acce	string
  # 异常标示	ab_lable	string
  # 异常数据时间	ab_data_time	string
  # 异常值	ab_value	integer
  # 参考值	compare_value	string
  # 参考值2	compare_value2	string
  # 参考值3	compare_value3	string
  # 异常描述	ab_desc	string
  # 异常实例id	s_abnormal_rule_instance_id

  paginates_per 10

  belongs_to :d_station, foreign_key: "station_id"
  belongs_to :s_abnormal_ltem, foreign_key: 'item_code_id'
  belongs_to :s_abnormal_rule_instance

  after_save :pm_ding_two

  scope :by_station_ids, ->(id_arr) {
    where(:station_id => id_arr) if id_arr.present?
  }
  scope :by_d_time, ->(d_time) {
    where("created_at BETWEEN ? AND ?", Time.parse(d_time).beginning_of_day, Time.parse(d_time).end_of_day) if d_time.present?
  }

  scope :by_get_time, ->(d_time) {
    if d_time.present?
      where("create_acce BETWEEN ? AND ?", Time.parse(d_time[0]).beginning_of_day, Time.parse(d_time[1]).end_of_day)
    else
      where("create_acce BETWEEN ? AND ?", Time.now.beginning_of_day, Time.now.end_of_day)
    end
  }

  scope :by_station_off, ->(station_ids){
    if station_ids.present?
      where(:ab_lable => 'lost', :station_id => station_ids)
    # else
    #   where(:ab_lable => 'lost', :station_id => 1090)
    end
  }

  def self.item_hash(item_id)
    item_ids = %W(101 102 103 104 105 106 107 108 201 202 203 204 205 206)
    item_index =  item_ids.index(item_id.to_s)
    item_name = %W(SO2 NO2 NO NOX CO O3 PM2.5 PM10 风速 风向 气温 气压 湿度 能见度)[item_index]
    return {id: item_ids[item_index],name: item_name }
  end

  # def self.test
  #   station = DStation.first
  #   instance = SAbnormalRuleInstance.first
  #   DAbnormalDataYyyymm.create(abnormal_id: '', station_id: station.id, rule_instance: instance.instance_name,
  #                              create_acce: '2017-04-19',ab_lable: 'Hsp', ab_data_time: '2017-04-19', ab_value: 3000, compare_value: '30',
  #                              compare_value2: '', compare_value3: '', ab_desc: '超出上限',s_abnormal_rule_instance_id: instance.id)
  # end

  private

  def pm_ding_two

  end

end
