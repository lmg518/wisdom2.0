class DNoticeInfo < ActiveRecord::Base
    #设置分页
    paginates_per 10
    
    belongs_to :d_templete

    #状态转换
    def status_str
        case status
          when 'Y'
            '已发送'
          when 'N'
            '未发送'
          else
            ''
        end
      end

    #app, sms, email
    def send_type_str
        return "" unless send_type.present?
        @send_arr = send_type.split(",")
        send_arr_length = @send_arr.length
        @arr = []
        case send_arr_length
        when 1
            @arr << DNoticeInfo.send_handle_str(@send_arr[0])
        when 2
            2.times do |i|
            @arr <<   DNoticeInfo.send_handle_str(@send_arr[i])
            end
        when 3
            3.times do |i|
            @arr <<  DNoticeInfo.send_handle_str(@send_arr[i])
            end
        end
        @arr
    end

    def self.send_handle_str(val)
        case val
        when 'app'
            'app推送'
        when 'sms'
            '短信'
        when 'email'
            "邮件"
        else
            ''
        end
    end


end
