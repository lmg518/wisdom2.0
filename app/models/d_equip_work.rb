class DEquipWork < ActiveRecord::Base

  has_many :work_boots
  has_many :work_notes

  paginates_per 10

  #按照车间查询
  scope :by_region_id, ->(region_id) {
    where(s_region_code_id: region_id) if region_id.present?
  }

  
  #按照时间查询
  scope :by_create_time, ->(begin_time,end_time){
    where(created_at: begin_time..end_time ) if begin_time.present? && end_time.present?
  }

   #按照计划点检 开始时间、计划点检 结束时间 查询
   scope :by_work_time, ->(begin_time, end_time) {
    where("work_start_time >= ? AND work_end_time <= ?",begin_time,end_time) if begin_time.present? && end_time.present?
  }


  #按照状态查询
  scope :by_work_status, ->(status){
    where(:work_status => status) if status.present?
  }

  #判断计划点检时间是否超出一半还未完成
  def self.handle_remind(arg)
    if arg.work_status == 'wait_issued' || arg.work_status == 'wait_carried'
      time = Time.now()
      start_time = arg.work_start_time
      end_time = arg.work_end_time
      diff_time = (end_time - start_time) / 2
      t = start_time + diff_time
      if time > t
        return 'red'
      end
    end
  end

  #状态转换
  def work_status_str
    case work_status
      when 'wait_issued'
        return '待下发'
      when 'wait_carried'
        return '处理中'
      when 'carry_out'
        return '完成'
      else
        ''
    end
  end

  def self.set_work_status(arg)
    case arg
      when 'wait_issued'
        return '待下发'
      when 'wait_carried'
        return '处理中'
      when 'carry_out'
        return '完成'
    end
  end

  #点检结果
  def self.set_work_result(arg)
    case arg
      when 'Y'
        return '正常'
      when 'N'
        return '异常'
    end
  end


end
