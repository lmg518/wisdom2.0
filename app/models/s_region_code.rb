class SRegionCode < ActiveRecord::Base
  #设置分页
  paginates_per 10

  has_many :son_region, class_name: "SRegionCode", foreign_key: "father_region_id" 
  belongs_to :father_region, class_name: "SRegionCode"
  has_many :d_login_msgs, foreign_key: "group_id"
  has_many :s_equips

  has_many :s_region_code_infos,dependent: :destroy  #单位表

  belongs_to :s_region_level

  #station
  has_many :station_region_code_ids
  has_many :d_stations, through: :station_region_code_ids

  # has_many :s_area_logins
  # has_many :d_login_msgs, :through => :s_area_logins

  belongs_to :s_administrative_area


  has_many :d_daily_reports
  has_many :d_plants   #车间项目


  
  # def d_stations
  #   DStation.where( "region_code = ? ", self.region_code )
  # end

  scope :by_father_region, ->{
    where(:father_region_id => 99999)
  }
  scope :by_region_level, ->(level){
    where(:s_region_level_id => level.to_i ) if level.present?
  }

  def get_son_region
    code_arr = []
    code_hash = {}
    # code_hash["first_region"] = self
    son_region.each do |code|
       code_arr << {region_chil: code.attributes.slice("id","region_name") ,d_stations: code.d_stations.map{|x| {id: x.id, station_name: x.station_name} } } if code.d_stations.present?
    end
    code_hash['chil'] = code_arr
    return code_hash
  end

  #按单位名称模糊查询
  scope :by_unit_name, ->(unit_name) {
    where("unit_name like ?", "%#{unit_name}%") if unit_name.present?
  }

  #按所属单位查询
  scope :by_region_code, ->(region_code) {
    where(:region_code => region_code) if region_code.present?
  }

  #按车间id
  scope :by_region_id, ->(id) {
    where(:id => id) if id.present?
  }

end
