class DNoticeWorker < ActiveRecord::Base
  #设置分页
  paginates_per 10
  
  has_many :d_worker_groups
  has_many :d_groups, :through => :d_worker_groups  #关联组信息 通过d_worker_groups
  
  belongs_to :d_login_msg
end
