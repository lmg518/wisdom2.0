class DAlarm < ActiveRecord::Base

    paginates_per 10

    has_many :d_alarm_details, foreign_key: 'alarm_record_id'
    belongs_to :d_station, foreign_key: 'station_id'
    has_many :d_alarm_notices, foreign_key: "alarm_id"
    belongs_to :s_alarm_rule

    has_many :d_alarm_despatch_alarm_rels #  任务
    has_many :d_alarm_despatches, :through => :d_alarm_despatch_alarm_rels

    #status : waitting:待处理  cancel：以取消 receive: 已接收 done：已处理

    scope :active, ->{
      n_time = Time.now
      where("created_at between ? and ?",n_time.beginning_of_day,n_time.end_of_day)
    }

    scope :by_arr_ids, -> (id_arr){
       where(:station_id => id_arr) if id_arr.present? && id_arr[0] != ""
    }

    scope :by_times,->(now_time,r_date){
      if r_date.present?
        r_rime = Time.parse(r_date)
        where("d_alarms.created_at BETWEEN ? AND ?",r_rime.beginning_of_day,r_rime.end_of_day)
      else
        where("d_alarms.created_at BETWEEN ? AND ?",(now_time - 23.hours).beginning_of_hour,now_time)
      end
    }
    
    #按日期范围查询
    scope :by_date_time, ->(begin_time, end_time) {
      where(created_at: (begin_time)..end_time) if begin_time.present? && end_time.present?
    }

    scope :by_search_times,->(r_date){
      if r_date.present?
        if r_date.length == 1
          r_rime1 = Time.parse(r_date[0])
          where(:created_at => ((r_rime1).beginning_of_day)..(r_rime1.end_of_day))
        elsif r_date.length == 2
          r_rime1 = Time.parse(r_date[0])
          r_rime2 = Time.parse(r_date[1])
          where(:created_at => ((r_rime1).beginning_of_day)..(r_rime2.end_of_day))
        else
          time_new = Time.parse(r_date)
          where(:created_at => ((time_new).beginning_of_day)..(time_new.end_of_day))
        end
      else
        where(:created_at => (Time.now.beginning_of_day)..(Time.now.end_of_day))
      end
    }

    scope :by_rule_levels, ->(alarm_level){
      where(:alarm_level => alarm_level) if alarm_level.present? && alarm_level != ''
    }

    scope :by_station_id, ->(stations_id){
      where(:station_id => stations_id) if stations_id.present?
    }

    scope :by_status, ->(status){
       if status.present?
         if status == "other"
           where.not(:status => "waitting")
         else
           where(:status => status)
         end
       # else
       #   where(:status => []) unless status == ''
       end
    }

    scope :by_rule_name, ->(rule_name){
      where("alarm_rule like ?", "%#{rule_name}%") if rule_name.present?
    }

    scope :by_today_data, -> {
      where(:created_at => (Time.now.beginning_of_day)..(Time.now.end_of_day) )
    }

    scope :by_alarm_times,->(r_date){
      if r_date.present?
        if r_date.length == 1
          r_rime1 = Time.parse(r_date[0])
          where(:first_alarm_time => ((r_rime1).beginning_of_day)..(r_rime1.end_of_day))
        elsif r_date.length == 2
          r_rime1 = Time.parse(r_date[0])
          r_rime2 = Time.parse(r_date[1])
          where(:first_alarm_time => ((r_rime1).beginning_of_day)..(r_rime2.end_of_day))
        end
      else
        where(:first_alarm_time => (Time.now.beginning_of_day)..(Time.now.end_of_day))
      end
    }

    scope :by_station_regin_leve, ->(level){
      where(:station_id => SRegionCode.find_by(:s_region_level_id => level.to_i ).d_stations.pluck(:id) ) if level.present?
    }

    #status : waitting:待处理  cancel：以取消 receive: 已接收 done：已处理
  def status_to_str
      case status
        when 'waitting'
          '待分配'
        when 'assign'
          '待响应'
        when 'doing'
          "待完成"
        when 'done'
          '已完成'
        when ''
      end
  end

  def alarm_level_to_s
     case alarm_level
       when 3
         "III级"
       when 2
         "II级"
       when 1
         "I级"
     end
  end

  def alarm_level_str
    case alarm_level
      when 3
        "III"
      when 2
        "II"
      when 1
        "I"
    end
 end

  def self.get_all_status
    status_arr = []
    %W(waitting assign).each do |val|
       status_arr <<  {status: val, status_name: all_status_str(val)}
    end
    return status_arr
  end

  def self.all_status_str(obj)
    case obj
      when 'waitting'
        '待分配'
      when 'assign'
        '待响应'
      when ''
      when ''
      when ''
    end
  end

  after_commit on: [:create] do
    alarm_detail = self.d_alarm_details.create!(:alarm_detail_id => id, :station_id => station_id, :alarm_rule => alarm_rule, :last_sample_time => first_alarm_time,
                                :ab_num => continuous_alarm_times, :alarm_level => alarm_level, :create_time => Time.now)
    # alarm_detail.d_alarm_abnormal_rels.create!(:s_alarm_rule_id => s_alarm_rule_id)
    send_notice
  end

  private
  def send_notice
    @users = []
    @user_name = []
    now_time = Time.now
    login_name = ''
    notice_rule = SNoticeRule.find_by_alarm_level( self.alarm_level ) #获取级别角色
    notice_template = notice_rule.s_notice_template
    # logins = self.d_station.d_login_msgs.where(:group_id => SRegionCode.where(:s_region_level_id => notice_rule.up_region_level).pluck(:id))
    stations = self.d_station
    logins = stations.d_login_msgs
    logins.map{|x| @user_name << x.login_name }
    admin_users= notice_rule.s_role_msgs.map{|x| @users << x.d_login_msgs.map{|x| login_name }  }
    notice_dec = "【鑫属运维系统】##{stations.station_name}#在##{self.first_alarm_time.strftime('%Y-%m-%d %H:%M:%S')}#发生异常,异常名称：##{self.alarm_rule}#。"
    self.d_alarm_notices.create!(:alarm_level => alarm_level ,
                                 :notice_type => notice_rule.notice_type ,
                                 :notice_login => @user_name.uniq.join(",") ,
                                 :notice_content => notice_dec ,
                                 :notice_accept => now_time.strftime("%Y%m%d") + now_time.strftime("%H%M%S") + now_time.usec.to_s ,
                                 :notice_time => now_time)
    # if self.alarm_level.to_i == 1
    #   logins.each do |login|
    #     send_sms(login.telphone,notice_dec) unless login.telphone.blank?
    #   end
    # end
  end

  def send_sms(mobile, notice_dec)
    ChinaSMS.use :yunpian, password: ProductConfig::YUN_PIAN_KEY
    # ChinaSMS.to mobile, "【鑫属运维系统】尊敬的#{current_user.login_name},您上次的登录时间为#{Time.now.strftime('%Y-%m-%d %H:%M:%S')}"
    ChinaSMS.to mobile, notice_dec
  end

  def d_alarm_despatch_create
    d_alarm_despatches.create!()
  end

end
