class DTwoMenuValue < ActiveRecord::Base
  #设置分页
  #paginates_per 10

  #按日期范围查询
  scope :by_datetime, ->(begin_time, end_time) {
    where(datetime: (begin_time)..end_time) if begin_time.present? && end_time.present?
  }

  # scope :page_set, ->(page) {
  #   where(datetime: (begin_time)..end_time) if begin_time.present? && end_time.present?
  # }


end
