class StationItemAvg < ActiveRecord::Base
  belongs_to :d_station

  scope :by_d_station_id, ->(station_id){
    if station_id.present?
      where(:d_station_id => station_id)
    else
      where(:d_station_id => 1484)
    end
  }

  scope :by_get_time, ->(get_time){
    if get_time.present?
      begin
        first_time = Time.parse(get_time[0])
        second_time = Time.parse(get_time[1])
      rescue
        time_arr = get_time.split(",")
        first_time = Time.parse(time_arr[0])
        second_time = Time.parse(time_arr[1])
      end

      where(:day_time =>  (first_time.beginning_of_day)..(second_time.end_of_day) )
    else
      where(:day_time => ((Time.now - 1.days).beginning_of_day)..((Time.now - 1.days).end_of_day) )
    end
  }

  scope :by_pre_year_time, ->(get_time){
    if get_time.present?
      begin
        first_time = Time.parse(get_time[0])
        second_time = Time.parse(get_time[1])
      rescue
        time_arr = get_time.split(",")
        first_time = Time.parse(time_arr[0])
        second_time = Time.parse(time_arr[1])
      end

      where(:day_time =>  ((first_time - 1.years ).beginning_of_day)..((second_time - 1.years).end_of_day) )
    else
      where(:day_time => (((Time.now - 1.days) -1.years ).beginning_of_day)..(((Time.now - 1.days) - 1.years).end_of_day) )
    end
  }

  scope :by_pre_time, ->(get_time){
    if get_time.present?
      begin
        first_time = Time.parse(get_time[0])
        second_time = Time.parse(get_time[1])
      rescue
        time_arr = get_time.split(",")
        first_time = Time.parse(time_arr[0])
        second_time = Time.parse(time_arr[1])
      end
      where(:day_time =>  ((first_time - 1.days).beginning_of_day)..((second_time - 1.days).end_of_day) )
    else
      where(:day_time => ((Time.now - 2.days).beginning_of_day)..((Time.now - 2.days).end_of_day) )
    end
  }



  scope :by_hour_month_data, ->(obj_time){
    if obj_time.present?
      where(:day_time =>  (obj_time.beginning_of_month)..(obj_time.end_of_month))
    else
      where(:day_time =>  (Time.now.beginning_of_month)..(Time.now.end_of_mo))
    end
  }

  scope :by_month_between_and, ->(month){
   if month.present?
     where(:day_time => (month.beginning_of_year)..(month.end_of_month))
   else
     where(:day_time => (Time.now.beginning_of_year)..(Time.now.end_of_month))
   end
  }

end
