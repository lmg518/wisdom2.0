class DLoginMsg < ActiveRecord::Base
  has_secure_password

  paginates_per 10

  belongs_to :s_role_msg
  belongs_to :s_region_code, foreign_key: "group_id"
  belongs_to :s_administrative_area
  belongs_to :s_region_code_info #单位

  has_many :s_area_logins
  has_many :d_stations, :through => :s_area_logins #站点人员关联

  has_many :d_alarm_despatches #任务

  has_many :d_alarm_notices

  has_many :file_maintenances

  has_many :d_task_forms, foreign_key: :handle_man_id

  has_one :d_localize       #时时定位
  has_one :d_notice_worker  #通告人员

  scope :by_role_ids, ->(role_ids) {
    where(:s_role_msg_id => role_ids) if role_ids.present?
  }

  scope :by_login_name, ->(login_name) {
    where("login_name like ?", "%#{login_name}%") if login_name.present?
  }

  scope :by_yw, ->{
    where(:s_role_msg_id => SRoleMsg.where(:role_name => "运维成员").pluck(:id))
  }

  scope :by_yw_unit, ->(unit_id){
    where(:s_region_code_info_id => unit_id) if unit_id.present?
  }

  scope :by_login_id, ->(login_id){
    where(:id => login_id) if login_id.present?
  }

  def show_to_hash
    #人员所在的车间区域
    area = SArea.find_by(:id => self.s_area_id)
    area_name = area.present? ? area.name : ''
    #group_name 作为一级选择数据  region_name 作为 二级选择 数据
    region_code = SRegionCode.find_by(:id => group_id)
    # if region_code.s_region_level_id == 3  #车间级的
    #   father_region = SRegionCode.find_by(:id => region_code.father_region_id)
    #   father_id = father_region.id
    #   father_name = father_region.region_name
    # end
    if [3,4,5,6].include? region_code.s_region_level_id   # 3 车间级的
      father_region = SRegionCode.find_by(:id => region_code.father_region_id)
      father_id = father_region.id
      father_name = father_region.region_name
    end

    {id: self.id,
     login_no: login_no,
     login_name: login_name,
     telphone: telphone,
     father_id:father_id,  #一级选择数据
     father_name:father_name,

     group_id: group_id,
     group_name: s_region_code.present? ? s_region_code.region_name : '',
     role_id: s_role_msg_id,
     role_name:s_role_msg.present? ? s_role_msg.role_name : '',
     role_code:s_role_msg.present? ? s_role_msg.role_code : '',
     valid_flag: valid_flag,
     describe: describe,
     email: email,
     id_card: id_card,
     work_num: work_num,
     #s_region_code_info_id: s_region_code_info_id,
     #unit_name: s_region_code_info.present? ? s_region_code_info.unit_name : '',
     area_name: area_name
    }
  end

  def new_session
    SecureRandom.urlsafe_base64
  end

  def navi_bar
    self.s_role_msg.s_func_msgs.where("root_distance = 1 and valid_flag = 0").order(:queue_index)
  end

  #### 工号所属区域及子区
  def region_codes
    @d_login_region_codes = Array.new
    @d_login_region_codes[0] = self.s_region_code.region_code
    puts self.s_region_code
    if s_region_code.son_region.present?
      self.s_region_code.son_region.each do |son_region_code|
        @d_login_region_codes.push(son_region_code.region_code)
      end
    end

    return @d_login_region_codes
  end

  def get_zone_login

  end

end
