class DTaskDealRecord < ActiveRecord::Base

  belongs_to :d_alarm_despatch, :foreign_key => "task_id"

  after_commit on: [:create] do
  end

  def status_to_str
    case task_status
      when 'assign'
        '待响应'
      when 'answer'
        '待处理'
      when "doing"
        "处理中"
      when "done"
        "已完成"
      when 'retweet'
        "待分配"
      when 'completed'
        "待审核"
      when 'audit'
        "已审核"
      when "finish"
        '结束'
    end
  end


end
