class SRegionCodeInfo < ActiveRecord::Base
  #设置分页
  paginates_per 10
  
  belongs_to :s_region_code
  has_many :d_login_msgs
  has_many :d_ops_plan_manages #任务计划
  has_many :d_task_forms

  has_many :d_stations

  scope :active,->{
    where(:status => "Y")
  }

  scope :by_id, ->(ids){
    where(:id => ids) if ids.present? && ids[0].present?
  }




end
