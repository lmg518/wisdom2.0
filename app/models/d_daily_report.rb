class DDailyReport < ActiveRecord::Base
paginates_per 10

belongs_to :s_region_code


#按创建时间查询
scope :by_datetime, ->(created_time, end_time) {
    where(datetime: (created_time)..end_time) if created_time.present? && end_time.present?
  }

#按车间id 查询
scope :by_region_ids, ->(code_ids) {
  where(:s_region_code_id => code_ids) if code_ids.present?
}

end
