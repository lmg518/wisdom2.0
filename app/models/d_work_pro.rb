class DWorkPro < ActiveRecord::Base
    #设置分页
    paginates_per 10
    #启用的work
    scope :active, ->(){
        where(:work_flag => "Y")
    }
    #按名字模糊搜索
    scope :by_work_name, ->(work_name) {
        where("work_name like ?", "%#{work_name}%") if work_name.present?
    }

    #按类型搜索  7  30   90  360
    scope :by_work_type, ->(work_type) {
        where(:work_type => work_type) if work_type.present?
    }

    #按标记搜索  Y 启用   N 禁用
    scope :by_work_flag, ->(work_flag) {
        where(:work_flag => work_flag) if work_flag.present?
    }
     #根据输入的数字转换为相应的工作类型（7:周  30:月   90:季度   360：年  ）
    def work_type_str
        case self.work_type
        when 7
            "周"
        when 30
            "月"
        when 90
            "季度"
        when 360
            "年"
        end    
    end

end
