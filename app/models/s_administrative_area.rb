class SAdministrativeArea < ActiveRecord::Base
  # validates :sort_num,presence: true
  has_many :d_login_msgs
  has_many :d_stations
  has_many :d_ops_plan_manages #任务计划
  acts_as_nested_set

  # 所有后代的ID 组合
  def descendants_ids
    self.descendants_map_of(:id)
  end

  def descendants_map_of(attr)
    self.self_and_descendants.map{|c|c.read_attribute attr}
  end

  # 祖父 > 父亲 > 自己
  def ancestors_awesome_names
    self.self_and_ancestors.map(&:zone_name).join ' > '
  end

  # 父节点
  def format_ancestors
    hash = self.attributes
    return hash unless parent

    hash[:parent] = parent.format_ancestors

    hash
  end

  #all
  def format_descendants
    # hash = self.attributes
    # hash['key']   = id.to_s
    # hash['value'] = id.to_s
    # hash['label'] = zone_name
    # hash['d_stations'] = d_stations.pluck(:id,:station_name)
   #  return hash if children.blank?
   #
   # children.reorder(:sort_num).map do |pc|
   #    pc.format_descendants
   #  end

  end


  def format_descendants_stations
    hash = self.attributes.slice("id","zone_name")
    # hash['key']   = id.to_s
    # hash['value'] = id.to_s
    # hash['label'] = zone_name
    # hash['chil_stations'] = d_stations
    return hash if children.blank?

    hash['stations'] = children.reorder(:sort_num).map do |pc|
      # pc.format_descendants
        pc.d_stations
    end

    hash
  end

  def self.all_provinces
     @arr = []
     provinces =  SAdministrativeArea.roots.select{|x| x["zone_name"] != "商丘市"}
     provinces.each do |province|
       if province.children.blank?
         @arr << {id: province["id"], name: province["zone_name"], cities: []}
       else
         @arr << {id: province["id"], name: province["zone_name"], cities: province.children.select("id","zone_name") }
       end
     end
     @arr
  end


  def self.add
    s1 = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CHNS', :zone_name=> '河南',:active_state=>true,:sort_num=>1)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0375', :zone_name=> '平顶山市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0376', :zone_name=> '信阳市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0394', :zone_name=> '周口市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0374', :zone_name=> '许昌市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0395', :zone_name=> '漯河市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0377', :zone_name=> '南阳市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0370', :zone_name=> '商丘市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0396', :zone_name=> '驻马店市',:active_state=>true,:sort_num=>1,:parent_id=>s1.id)

    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C010', :zone_name=> '北京市',:active_state=>true,:sort_num=>1)

    hb = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CHBS', :zone_name=> '河北',:active_state=>true,:sort_num=>1)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0311', :zone_name=> '石家庄市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0310', :zone_name=> '邯郸市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0317', :zone_name=> '沧州市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0318', :zone_name=> '衡水市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0319', :zone_name=> '邢台市',:active_state=>true,:sort_num=>1,:parent_id=>hb.id)

    sx = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CSXS', :zone_name=> '山西',:active_state=>true,:sort_num=>1)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0351', :zone_name=> '太原市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0352', :zone_name=> '大同市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0353', :zone_name=> '阳泉市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0349', :zone_name=> '朔州市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0354', :zone_name=> '晋中市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0350', :zone_name=> '忻州市',:active_state=>true,:sort_num=>1,:parent_id=>sx.id)

    nmg = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CNMG', :zone_name=> '内蒙古',:active_state=>true,:sort_num=>1)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0471', :zone_name=> '呼和浩特市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0472', :zone_name=> '包头市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0477', :zone_name=> '鄂尔多斯市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0473', :zone_name=> '乌海市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0478', :zone_name=> '巴彦淖尔市',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0483', :zone_name=> '阿拉善盟',:active_state=>true,:sort_num=>1,:parent_id=>nmg.id)

    hlj = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CHLJ', :zone_name=> '黑龙江',:active_state=>true,:sort_num=>1)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0451', :zone_name=> '哈尔滨市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0452', :zone_name=> '齐齐哈尔市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0459', :zone_name=> '大庆市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0456', :zone_name=> '黑河市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0455', :zone_name=> '绥化市',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0457', :zone_name=> '大兴安岭地区',:active_state=>true,:sort_num=>1,:parent_id=>hlj.id)

    ah = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CAHS', :zone_name=> '安徽',:active_state=>true,:sort_num=>1)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0551', :zone_name=> '合肥市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0553', :zone_name=> '芜湖市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0555', :zone_name=> '马鞍山市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0562', :zone_name=> '铜陵市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0556', :zone_name=> '安庆市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0559', :zone_name=> '黄山市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0566', :zone_name=> '池州市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0563', :zone_name=> '宣城市',:active_state=>true,:sort_num=>1,:parent_id=>ah.id)

    nx = SAdministrativeArea.find_or_create_by!(:zone_rank => 'CNXS', :zone_name=> '宁夏',:active_state=>true,:sort_num=>1)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0951', :zone_name=> '银川市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0952', :zone_name=> '石嘴山市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0953', :zone_name=> '吴忠市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0955', :zone_name=> '中卫市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)
    SAdministrativeArea.find_or_create_by!(:zone_rank => 'C0954', :zone_name=> '固原市',:active_state=>true,:sort_num=>1,:parent_id=>nx.id)

  end


end
