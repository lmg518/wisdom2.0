class WcDStation < WebCralerRecod
  self.table_name = 'dStation'

  # WcDStation.insert_to_d_station
  def self.insert_to_d_station
    @wc_stations = all.order(:REGION_CODE)
    @wc_stations.each do |wc_s|
      DStation.create(:id => wc_s.STATION_ID.delete("A"),:station_name => wc_s.STATION_NAME, :station_status => '1',
      :is_online => "Y", :wc_station_id => wc_s.STATION_ID.delete("A"),:region_code => wc_s.STATION_NAME.split("-")[0],
      :s_administrative_area_id => SAdministrativeArea.find_by_zone_name(wc_s.STATION_NAME.split("-")[0]).id,:station_type => '小型站',
      :station_location => wc_s.STATION_NAME.split("-")[0],:longitude => "0",:latitude => "0")
    end
  end
end