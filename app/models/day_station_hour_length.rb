class DayStationHourLength < ActiveRecord::Base

  scope :by_get_time, ->(get_time){
    if get_time.present?
      begin
        first_time = Time.parse(get_time[0])
        second_time = Time.parse(get_time[1])
      rescue
        time_arr = get_time.split(",")
        first_time = Time.parse(time_arr[0])
        second_time = Time.parse(time_arr[1])
      end
      where(:created_at => ( first_time.beginning_of_day)..(second_time.end_of_day) )
    else
      where(:created_at => (Time.now.beginning_of_day)..( Time.now.end_of_day))
    end
  }

  scope :by_station_ids, ->(station_ids) {
    where(:d_station_id => station_ids) if station_ids.present?
  }

end
