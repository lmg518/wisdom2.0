class FFieldTitle < ActiveRecord::Base
    paginates_per 10
    belongs_to :f_form_module, primary_key: :id
end
