class SBrandMsg < ActiveRecord::Base

  def self.add
    %w(OPSIS API ESA FPI KIMOTO LD TH XH ADVANTECH XH2000E METONE DASIBI Thermo AR-500S EC ESC).each do |brand|
      SBrandMsg.find_or_create_by(:brand => brand)
    end
  end

end
