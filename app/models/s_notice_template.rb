class SNoticeTemplate < ActiveRecord::Base
  has_many :s_notice_rules

  def self.test
    SNoticeTemplate.find_or_create_by!(template_name: "站点报警通知模版", alarm_rule: 1, abnormal_instance: 1,
                                       station_info: 1, login_name: 1, abnormal_time: 1,
                                       alarm_time: 1, alarm_last_time: 1, alarm_num: 1,
                                       alarm_status: 1,
                                       template_des: "尊敬的XXX,您好！您所管辖的XX站点，在2017-05-15 12：22：00，SO2超高，采集值为100，标示 HSp，报警开始时间2017-05-15 12：22：00，报警持续时间2017-05-15 13：22：00，持续报警次数10次，报警状态：待处理")
  end

end
