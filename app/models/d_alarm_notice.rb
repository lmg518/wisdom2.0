class DAlarmNotice < ActiveRecord::Base

  paginates_per 10

  belongs_to :d_alarm, foreign_key: "alarm_id"
  belongs_to :d_login_msg

  scope :by_name, ->(name){
    where("notice_login like ?","%#{name}%") if name.present?
  }
  scope :by_d_time, ->(time){
    where("created_at between ? and ?",Time.parse(time).beginning_of_day,Time.parse(time).end_of_day) if time.present?
  }
  scope :by_status, ->(status){
    where(:status => status) if status.present?
  }
  scope :by_level, ->(level){
    where(:alarm_level => level.to_i) if level.present?
  }
#status : waitting:待处理  cancel：以取消 receive: 已接收 done：已处理
  def status_to_str
     case status
       when 'noread'
         '未读'
       when 'read'
         '已读'
       when 'receive'
         '已接收'
       when ''
       when ''
       when 'done'
         '已处理'
     end
  end

  def self.test
    login1 = DLoginMsg.find_by_login_no( 'dingfj')
    login2 = DLoginMsg.find_by_login_no( 'wenpd')
    # DAlarm.find_each(start: 10, batch_size: 20).each_with_index do |alarm,index|
    #  login1.d_alarm_notices.create( alarm_id: alarm.id,
    #                                 alarm_level: alarm.alarm_level,
    #                                 notice_type: 1,
    #                                 notice_login: login1.login_name,
    #                                 notice_content: "xxxxxxxxxxxxxxxxx,测试数据#{index}",
    #                                 notice_time: Time.now,
    #                                 notice_accept:  Time.now,
    #                                 status: "noread"
    #  )
    # end
    DAlarm.find_each(start: 100, batch_size: 20).each_with_index do |alarm,index|
      login2.d_alarm_notices.create( alarm_id: alarm.id,
                                      alarm_level: alarm.alarm_level,
                                      notice_type: 1,
                                      notice_login: login2.login_name,
                                      notice_content: "xxxxxxxxxxxxxxxxx,测试数据#{index}",
                                      notice_time: Time.now,
                                      notice_accept:  Time.now,
                                     status: "noread"
      )
    end
  end

end
