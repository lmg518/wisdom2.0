class DStation < ActiveRecord::Base

  paginates_per 10

  validates :station_name, presence: true

  has_many :d_abnormal_data_yyyymms, foreign_key: "station_id"
  has_many :d_data_yyyymms, foreign_key: :station_id

  has_many :d_station_rule_rels #站点和异常实例
  has_many :s_abnormal_rule_instances, through: :d_station_rule_rels

  belongs_to :s_administrative_area

  has_many :s_area_logins
  has_many :d_login_msgs, :through => :s_area_logins

  has_many :d_alarms, foreign_key: 'station_id'

  has_many :station_item_avgs

  #region_code
  has_many :station_region_code_ids
  has_many :s_region_codes, :through => :station_region_code_ids

  #aqi
  has_many :d_data_hourly_yyyys, foreign_key: :station_id
  has_many :d_data_dailies, foreign_key: :station_id

  has_many :day_valid_data
  
  #d_fault_job
 
  has_many :d_task_forms  #任务工单

  has_many :d_ops_plan_manages #任务计划

  # 单位
  belongs_to :s_region_code_info

  # 设备信息表
  has_many :s_equipment_infos

  #报警信息表
  has_many :d_data_analyzes

  scope :active, -> {
    where(:station_status => "1")
  }

  scope :by_unit_ids, ->(unit_ids) {
    where(:s_region_code_info_id => unit_ids) if unit_ids.present? && unit_ids[0].present?
  }

  scope :by_station_ids, ->(station_ids) {
    where(:id => station_ids) if station_ids.present? && station_ids[0].present?
  }

  scope :by_province, ->(province) {
    where(:s_administrative_area_id => SAdministrativeArea.where("zone_name like ?", "%#{province}%").present? ? SAdministrativeArea.where("zone_name like ?", "%#{province}%").first.descendants_ids : []) if province.present?
  }

  def region_code_to_s
    return s_administrative_area.zone_name
  end

  def get_region
    if self.s_region_codes.present?
      s_region_codes[0].region_name
    else
      ""
    end
  end

  def hour_data(obj_time)
    yz_arr = []
    if obj_time.present?
      time_now = Time.parse(obj_time)
      @data_yyyy_mms = self.d_data_yyyymms.where(data_cycle: 3600, data_time: (time_now.beginning_of_day)..(time_now.end_of_day)).active
    else
      @data_yyyy_mms = self.d_data_yyyymms.where(data_cycle: 3600, data_time: ("2017-05-15 00:00:00".."2017-05-15 23:59:59")).active
    end

    @data_yyyy_mms.each do |yz_data|
      yz_arr << {yz_name: yz_data.item_code_name, yz_lable: yz_data.data_label, yz_begin_time: yz_data.data_time.strftime("%Y-%m-%d %H:%M:%S"), yz_end_time: yz_data.data_time.strftime("%Y-%m-%d %H:%M:%S")}
    end
    return yz_arr
  end

  def create_alarm_create(acce_time, describe)
    # 异常id	abnormal_id
    # 站点ID	station_id
    # 异常规则实例	rule_instance
    # 生成时间	create_acce
    # 异常标示	ab_lable
    # 异常数据时间	ab_data_time
    # 异常值	ab_value
    # 参考值	compare_value
    # 参考值2	compare_value2
    # 参考值3	compare_value3
    # 异常描述	ab_desc
    # 异常实例id	s_abnormal_rule_instance_id
    abnormal_rule = SAbnormmalRule.find_by(:rule_code => "SLOST")
    rule_instance = SAbnormalRuleInstance.find_by(:s_abnormmal_rule_id => abnormal_rule.id)
    self.d_abnormal_data_yyyymms.create!(abnormal_id: '',
                                         station_id: self.id,
                                         item_code_id: 9999,
                                         rule_instance: rule_instance.instance_name,
                                         s_abnormal_rule_instance_id: rule_instance.id,
                                         ab_lable: "lost",
                                         create_acce: acce_time,
                                         ab_data_time: acce_time,
                                         ab_value: 0,
                                         compare_value: 0,
                                         compare_value2: 0,
                                         compare_value3: 0,
                                         ab_desc: describe)

  end

end
