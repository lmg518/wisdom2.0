class DFaultJob < ActiveRecord::Base
    belongs_to :s_region_code
    has_one :d_fault_handle
    has_many :d_fault_job_details
    belongs_to :d_station
    
    # #按站点模糊查
    # scope :by_station_name, ->(station_name) {
    #     where("station_name like ?","%#{station_name}%") if station_name.present?
    # }

    #按站点id查询
    scope :by_station_id, ->(station_ids){
        where(d_station_id: station_ids) if station_ids.present?
    }

    #按工单号查询
    scope :by_job_no, ->(job_no) {
        where(:job_no => job_no) if job_no.present?
    }

    #按创建人查询
    scope :by_author, ->(author) {
        where(:author => author) if author.present?
    }

    #按创建时间查询
    scope :by_time, ->(created_time,end_time) {
        where(created_at: (created_time)..end_time) if created_time.present? && end_time.present?
    }

    #按当前处理人
    scope :by_handle_man, ->(handle_man) {
        where(:handle_man =>handle_man) if handle_man.present?
    }

    #按创建类型
    scope :by_create_type, ->(create_type) {
        where(:create_type =>create_type) if create_type.present?
    }

    #按工单状态
    scope :by_job_status, ->(job_status) {
        where(:job_status =>job_status) if job_status.present?
    }

end

    
