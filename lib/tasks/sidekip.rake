desc "批量启动sidekiq"
task "sidekiq:start" => :environment do
  now_time = Time.now
  SidekiqMinuteWorker.perform_in(now_time)
  SidekiqHourWorker.perform_in( now_time.beginning_of_hour + 40.minutes)
  SidekiqAlarmRuleWorker.perform_in(now_time)
  SidekiqStationLostWorker.perform_in(now_time)
  SidekiqStationLengthWorker.perform_in( now_time.beginning_of_day + 1.hours)
  SidekiqStationItemavgWorker.perform_in(now_time.beginning_of_day + 1.hours + 45.minutes )
  SidekiqStationDayFlagWorker.perform_in(now_time.beginning_of_day + 1.hours + 18.minutes )
  SidekiqAqiHourWorker.perform_in(now_time.beginning_of_hour + 21.minutes )
  SidekiqAqiDayWorker.perform_in( now_time.beginning_of_day + 2.hours + 13.minutes)
end