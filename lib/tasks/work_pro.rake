desc "初始化"
task "work_pros" => :environment  do
  %w"站点设备半年维护记录（每半年） 氮氧化物分析仪钼炉转化率记录表（每半年） 多气体动态校准仪校准检查记录表（每半年） 臭氧（O3）校准仪（工作标准）量值传递记录表（每半年） 能见度分析仪校准记录表（每半年）".each_with_index do |name,index|
    index_str = index + 1
    str = index_str.to_s.length < 2 ? "00#{index_str}" : "0#{index_str}"
    DWorkPro.find_or_create_by(:work_name =>  name, :work_code => str , :work_type => 360, :work_flag => "Y")
  end
  %w"气体分析仪（二氧化硫）多点校准记录表（每季度） 气体分析仪（臭氧）多点校准记录表（每季度） 气体分析仪（二氧化氮）多点校准记录表（每季度） 气体分析仪（一氧化碳）多点校准记录表（每季度） (二氧化硫)仪器精密度审核记录表（每季度） (氮氧化物)仪器精密度审核记录表（每季度） (一氧化碳)仪器精密度审核记录表（每季度） (臭氧)仪器精密度审核记录表（每季度） 站点设备清洁记录 颗粒物温度、压力校准记录表 颗粒物PM10自动监测分析仪运行状况检查记录 颗粒物PM2.5自动监测分析仪运行状况检查记录 长光程SO2分析仪单点校准检查记录表（每季度） 长光程NO2分析仪单点校准检查记录表（每季度） 长光程O3分析仪单点校准检查记录表（每季度）".each_with_index do |name,index|
    index_str = index + 1
    str = index_str.to_s.length < 2 ? "00#{index_str}" : "0#{index_str}"
    DWorkPro.find_or_create_by(:work_name =>  name, :work_code => str , :work_type => 90, :work_flag => "Y")
  end
  %w"站点设备维护记录 气体分析仪流量检查记录表 多气体动态校准仪校准检查记录表 颗粒物手工比对采样记录表".each_with_index do |name,index|
    index_str = index + 1
    str = index_str.to_s.length < 2 ? "00#{index_str}" : "0#{index_str}"
    DWorkPro.find_or_create_by(:work_name =>  name, :work_code => str , :work_type => 30, :work_flag => "Y")
  end
  %w"每周(次)巡检工作汇总表 长光程(SO2,NO2,O3)分析仪器运行状况检查记录表 站点巡检记录 二氧化硫（SO2）分析仪运行状况检查记录表 氮氧化物（NOX）分析仪运行状况检查记录表 臭氧（O3）分析仪运行状况检查记录表 一氧化碳（CO）分析仪运行状况检查记录表 颗粒物PM10自动监测分析仪运行状况检查记录 颗粒物PM2.5自动监测分析仪运行状况检查记录 其他仪器、设备运行状况检查记录".each_with_index do |name,index|
    index_str = index + 1
    str = index_str.to_s.length < 2 ? "00#{index_str}" : "0#{index_str}"
    DWorkPro.find_or_create_by(:work_name =>  name, :work_code => str , :work_type => 7, :work_flag => "Y")
  end
end