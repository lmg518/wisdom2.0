desc "初始化d_work_pro表数据"
task "work_data_init" => :environment do
  #周工作数据初始化（12项）
  DWorkPro.find_or_create_by(:work_name => "站房环境", :work_type => 7, :work_code=>"W001", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "子站环境卫生", :work_type => 7, :work_code=>"W002", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "采样管路检查", :work_type => 7, :work_code=>"W003", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "避雷与接地检查", :work_type => 7, :work_code=>"W004", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "空调运行", :work_type => 7, :work_code=>"W005", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "通讯线路检查", :work_type => 7, :work_code=>"W006", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "N02等六因子分析仪器状态检查", :work_type => 7, :work_code=>"W007", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "设备校零校跨", :work_type => 7, :work_code=>"W008", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "供电系统", :work_type => 7, :work_code=>"W009", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "通信系统", :work_type => 7, :work_code=>"W010", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "气象仪器", :work_type => 7, :work_code=>"W011", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "耗材更换", :work_type => 7, :work_code=>"W012", :work_flag=>"Y")
   
  #月工作数据初始化(10项)
  DWorkPro.find_or_create_by(:work_name => "采样管路", :work_type => 30, :work_code=>"M001", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "采样头等清洁", :work_type => 30, :work_code=>"M002", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "空调滤网清洁", :work_type => 30, :work_code=>"M003", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "PM10流量", :work_type => 30, :work_code=>"M004", :work_flag=>"Y")  
  DWorkPro.find_or_create_by(:work_name => "标准膜等检查", :work_type => 30, :work_code=>"M005", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "颗粒物手工比对", :work_type => 30, :work_code=>"M006", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "设备检查", :work_type => 30, :work_code=>"M007", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "设备清洗", :work_type => 30, :work_code=>"M008", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "对比测试", :work_type => 30, :work_code=>"M009", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "数据备份", :work_type => 30, :work_code=>"M010", :work_flag=>"Y")

  #季度工作数据初始化
  DWorkPro.find_or_create_by(:work_name => "更换耗材", :work_type => 90, :work_code=>"J001", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "设备清洗", :work_type => 90, :work_code=>"J002", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "参数校准", :work_type => 90, :work_code=>"J003", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "标准膜检查", :work_type => 90, :work_code=>"J004", :work_flag=>"Y")  

  #年工作数据初始化
  DWorkPro.find_or_create_by(:work_name => "设备检查校准", :work_type => 360, :work_code=>"Y001", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "转化率检查", :work_type => 360, :work_code=>"Y002", :work_flag=>"Y")
  DWorkPro.find_or_create_by(:work_name => "备件更换", :work_type => 360, :work_code=>"Y003", :work_flag=>"Y")
  

  end