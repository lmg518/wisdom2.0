# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( d_stations.js  d_login_msgs.js s_func_msgs.js s_role_msgs.js 
exception_monitorings.js exception_sets.js 
alarm_maps.js alarm_monitorings.js alarm_lists.js 
alarm_sets.js alarm_notices.js alarm_notice_sets.js 
d_alarms.js search_five_datas.js handle_station_regions.js 
alarm_despatches.js alarm_num_statistics.js alarm_num_scales.js 
alarm_num_handles.js alarm_num_trend.js alarm_num_time_analyse.js 
s_slas.js s_administrative_areas.js superior_air_days.js
single_station_pollutants_analysis.js more_station_pollutants_analysis.js data_statistics.js 
data_access_statistics func_models.js search_alarm_num_handles.js station_data_off.js 
concentration_analysis.js contaminant_leves.js facility_fault_day_reports.js file_maintenances.js
load_kindeditor station_validity_data.js work_pros.js root_application.css root_application.js 
s_region_code.js power_cut_records.js fault_jobs.js d_task_forms.js ops_audit.js next_plan_manages.js 
ops_audit_manages.js ops_week_plan_manages.js equip_mains.js pending_works.js equip_calibration.js 
plan_analysis.js statistic_place.js order_completion.js worker_manages.js notice_info_manages.js 
supplies.js accs.js localize.js templete_manages.js check_ups.js wx_application.js  wx_application.css wx_css/manages.css wx_css/index.css wx_css/jquery.datetimepicker.css wx_css/place_infos.css wx_css/plan_done_infos.css wx_css/task_done_infos.css wx_css/task_forms.css wx_css/wx_public.css
 sq_alarm_monitorings.js data_abnormal_form.js air_data_analyze.js form_module.js field_title.js equipment_infos.js handle_equips.js review_equips.js d_equip_works.js
 d_fixed_nums.js create_station_plan.js get_daily_reports.js daily_report.js company_reports.js company_month_reports.js 
  energy_report.js s_equips.js steam_report.js pollution_report.js quality_report.js fermentation_plant_form.js 
  energy_daily_form.js company_statement.js company_month_statement.js handle_equips.css d_equip_works.css s_equips equips_area_manage.js d_equip_work_rules.js post_reports.js)




