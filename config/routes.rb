require 'crono/web'
require 'sidekiq/web'
Rails.application.routes.draw do


  root 'homes#index'

  mount AirAppV1API => '/api/v1'
  mount Crono::Web, at: '/job_web'
  mount Resque::Server, :at => "/resque"
  mount Sidekiq::Web => '/sidekiq_web'
  resources :s_area_logins
  resources :d_abnormal_data_yyyymms
  resources :s_role_msgs
  resources :log_queries, only:[:index,:show]
  resources :sessions, only:[:new, :create, :destroy]
  resources :d_login_msgs do
    get "get_branchs", on: :collection        #获取所有的分公司
    get "get_region_codes", on: :collection   #获取分公司下的所有车间或部门
    post 'get_datas', on: :collection
  end
  resources :test_iframes
  resources :handle_station_regions
  resources :reset_pds

  
  resources :s_func_msgs
  resources :homes, only:[:index]
  resources :file_maintenances do
    post "edit_new", on: :collection
  end
  resources :file_categories

  resources :d_stations do
    get "all_stations", on: :collection
    get 'area_logins', on: :member
    post 'handle_station', on: :member 
    put 'update_station', on: :member
  end
  resources :exception_monitorings  do  #ycjk
    get "init", on: :collection
  end
  resources :exception_sets  #ycSL
  resources :factors  # yz
  resources :get_obj_types  #jkdx
  resources :consults  #ckz
  resources :s_administrative_areas #xzqy

  resources :alarm_sets #bjsz
  resources :alarm_monitorings do#bjjk
    get "init_index", on: :collection
  end
  resources :alarm_lists  #bjhz
  resources :alarm_maps do  #bjdt
    get "init_index", on: :collection
  end
  resources :s_alarm_types #bjfl
  resources :s_alarm_levels  #bjjb
  resources :d_alarms

  resources :alarm_notices  #bjtz
  resources :alarm_notice_sets  #bjtzsz

  resources :alarm_despatches do #报警任务g
    get "despatch_infos", on: :collection
  end
  resources :alarm_despatch_sets #报警任务设置
  resources :search_station_logins do #站点管理区域人员
    post "despatch_create", on: :collection
  end
  resources :s_slas #SLA设置

  resources :alarm_num_statistics do # 统计
    get "init_index", on: :collection
    get "export_file", on: :collection
  end
  resources :alarm_num_scales do # 占比
    get "export_file", on: :collection
  end

  resources :alarm_num_time_analyse do  #时段
    get "export_file", on: :collection
  end
  resources :alarm_num_handles do   # 处理
    get "export_file", on: :collection
  end
  resources :search_alarm_num_handles do  #反馈查询
    get "export_file", on: :collection
  end
  resources :station_data_off do  #  站点离线时长统计
    get "export_file", on: :collection
  end

  resources :station_validity_data  #  站点24小时有效数据

  resources :alarm_num_trend # 趋势  X
  resources :ops_day_reports   #运维日报  x
  resources :ops_week_reports  #运维周报  x
  resources :ops_month_reports  #运维月报  x
  resources :ops_polling_week_reports    #运维巡检周报  x
  resources :single_station_pollutants_analysis   #单站点多污染分析
  resources :more_station_pollutants_analysis   #多站点多污染分析
  resources :data_statistics do   #站点有效性统计
    get 'init_index', on: :collection
    get "export_file", on: :collection
  end
  resources :data_access_statistics do  #站点获取率统计
    get 'init_index', on: :collection
    get "export_file", on: :collection
  end
  resources :facility_fault_day_reports do #设备运行故障日报
    get 'init_index', on: :collection
    get "export_file", on: :collection
  end
  resources :superior_air_days do  #优良天数对比
    get 'init_index', on: :collection
    get "export_file", on: :collection
  end
  resources :contaminant_leves do   #污染天气级别分布
    get 'init_index', on: :collection
    get "export_file", on: :collection
  end
  resources :concentration_analysis do   #浓度值对比分析
    get 'init_index', on: :collection
    get "export_file", on: :collection
  end

  resources :search_regin_codes
  resources :search_region_codes_all  #查询所有的车间、部门
  resources :search_region_codes_areas  #查询所有的车间、部门


  #-------------拓样实业-------------
  resources :search_regin_code_infos   #查询所有单位
  resources :search_region_codes       #根据分公司 查询车间  查询车间接口

  resources :daily_report do #日报表        车间生产上报
    get "report", on: :collection   #上报操作
  end

  resources :energy_report do       #能源上报      
    get "report", on: :collection   #上报操作
    get "water_report", on: :collection            #生物循环水报表  用电上报
    get "power_report", on: :collection            #用电上报
  end

  resources :steam_report do       #蒸汽上报      
    get "steam_report", on: :collection            
  end

  resources :quality_report do       #质量上报      
    get "quality_report", on: :collection            
  end

  resources :pollution_report do       #一次水排污统计上报      
    get "pollution_report", on: :collection          #排污上报    
    get "once_water_report", on: :collection          #一次水上报  
    get "report", on: :collection   #排污报表上报操作
  end


  resources :fermentation_plant_form do            #车间日报表
    get "plant_form_index", on: :collection        #首页

    get "fermentation_workshop", on: :collection   #发酵车间生产报表
    get "bake_shop", on: :collection               #烘包车间生产报表
    get "refining_workshop", on: :collection       #精制车间生产报表
    get "synthetic_workshop", on: :collection      #合成车间生产报表
  end

  resources :energy_daily_form do        #  能源日报表
    get "energy_index", on: :collection        #首页

    get "energy_form_power", on: :collection        #生物用电报表  
    get "energy_form_water", on: :collection        #生物用循环水报表
    get "energy_form_once_water", on: :collection   #一次水报表
    get "energy_form_pollution", on: :collection    #排污报表
    get "energy_form_steam", on: :collection        #蒸汽报表
  end

  resources :company_statement   do        
    get "index_table", on: :collection      #公司总报表
  end

  resources :company_month_statement   do        
    get "index_table", on: :collection      #公司月总报表
  end

  resources :developing    #临时首页  开发中提示




  #-------------v1----------------------
  # resources :get_daily_reports  #日报表   日生产报表
  # resources :company_reports          #公司生产报表  日报
  # resources :company_month_reports    #公司生产报表  月报
  #-------------------------------------------------

  #设备台账
  resources :s_equips do
    get 'get_equips',on: :collection #根据车间获取到设备
    get 'set_status',on: :collection #禁用启用
    post 'create_spare', on: :collection #创建跟设备相关的备件信息
    get 'edit_spare',on: :collection
    post 'update_spare',on: :collection #修改跟设备相关的备件信息
    delete 'delete_spare',on: :collection #删除跟设备相关的备件信息
    get 'get_handle_equip',on: :collection #获取到设备维修记录

    post 'excel_import',on: :collection #从excel导入台账信息
  end

  #设备检修
  resources :handle_equips do
    get 'get_equips',on: :collection #获取到登录人所属车间设备
    get 'get_spare',on: :collection #获取设备所属备件
    get 'get_boots', on: :collection #获取检查项
    get 'get_login', on: :collection #获取人员
    post 'issued',on: :collection #下发
    post 'carried_out',on: :collection #执行
  end

  #设备检修审核
  resources :review_equips

  #设备点检
  resources :d_equip_works do
    get 'get_boots',on: :collection #获取检查项
    post 'issued',on: :collection #下发
    post 'carried_out',on: :collection #执行
    get 'get_login', on: :collection #获取人员
    get 'get_equip_regions', on: :collection #新增页面，车间部门 数据
    post 'batch_create',on: :collection #批量新增点检
  end

  #设备点检规则制定
  resources :d_equip_work_rules do
    get 'get_boots',on: :collection #获取检查项
    post 'issued',on: :collection #下发
    get 'get_login', on: :collection #获取人员
    get 'get_equip_regions', on: :collection #新增页面，车间部门 数据
  end


  # #设备管理--查询接口
  # resources :serach_equip_datas do 
  #   get 'get_region',on: :collection     #根据车间获取到车间下的区域
  # end

  #新增 客户要求
  #设备区域管理
  resources :equips_area_manage do 
    get 'get_areas',on: :collection     #根据车间获取到车间下的区域
    get 'area_edit',on: :collection     #修改
  end
  #--------------------------
  #拓样V2.0
  resources :post_reports do  #车间岗位盘点报表
    get 'get_product',on: :collection     #发酵、酸化 交料产量详细
    get 'period_inventory',on: :collection     #期盘点 接口
  end
  resources :publics do
    get 'status', on: :collection
  end






  resources :search_regin_logins  # area logions
  resources :search_login_regins  #login area  ghglqy
  resources :search_station_bindings  #bdyz
  resources :search_abnormal_rules   #ycmb  infos
  resources :search_abnormarl_instances  #ycgzsl
  resources :search_region_levels #gsjb
  resources :search_map_provinces #province  省
  resources :search_map_cities #city  城市
  resources :search_map_city
  resources :search_by_status
  resources :search_by_province_alarm #bjsf city infos  城市预警
  resources :search_five_datas do #5.minutes data infos
    get "export_file", on: :collection
  end
  resources :search_stations # all stations
  resources :search_notice_templates

  resources :handle_station_bindings  #bdyz handle
  resources :handle_alarm_rules #bjtzgz

  resources :tests
  resources :func_models do #means
    get "modal_form", on: :member
    get "table_infos", on: :collection
  end

  resources :assist do
    get "download_explorer" , on: :collection
  end
  
  #======================================
  #工作项目模块表
  resources :work_pros
  #停电记录模块表
  resources :power_cut_records do
    get "power_audit", on: :member  #审核
    get "report", on: :member  #上报，完成
    get "get_all_msgs", on: :collection  #根据运维单位获取所有的运维人员
  end

  #单位信息管理
  resources :s_region_code
  #故障任务管理
  resources :fault_jobs do
    member do 
      post "audit"    #审核
      get "forword"   #转发
    end
  end

  #运维耗材管理
  resources :supplies
  #运维配件管理
  resources :accs

  #设备信息管理
  resources :equipment_infos


  #运维工单审核
  resources :ops_audit do
    get "batch_audit", on: :collection  #批量审核的路由
    get "review", on: :member       #复核
  end
  #运维工单信息管理
  resources :d_task_forms do
    get "get_form_module", on: :member       #根据工作项id获取表格模板信息
    get "save_form_module", on: :collection  #保存表格中的数据
    get "sign_in", on: :collection           #运维成员签到
  end

  #运维下月计划管理
  resources :next_plan_manages do
    get "report", on: :collection   #上报操作
  end

  #新需求  新增站点 手动创建计划
  resources :create_station_plan do
    get "create_plan", on: :collection   #上报操作
  end

  #运维计划周计划
  resources :ops_week_plan_manages
  #运维计划审核
  resources :ops_audit_manages do
    get "plan_audit", on: :collection  #单个审核的路由
    get "search_ops", on: :collection  #根据week_time查询
  end

  #查询接口
  resources :search_region_codes #获取所属区域（单位）
  
  resources :s_item_job # 因子设备job
  resources :search_units # 根据站点获取单位
  resources :search_unit_pers #根据单位id获取人
  resources :search_unit_manys #获取全部单位及所属人员   派遣操作

  resources :all_stations_area #按区域显示站点信息（商丘客户需要）


  #工作代办事项
  resources :pending_works
  #设备维修处理
  resources :equip_mains
  #设备校准处理
  resources :equip_calibration
  #自动生成下月计划
  resources :next_mmm_work_jobs

  #到位统计
  resources :statistic_place
  #计划执行分析
  resources :plan_analysis do
    get "search_ops", on: :collection  #根据week_time查询
  end
  #工单完成率
  resources :order_completion do
    get "export_file", on: :collection #导出表格数据
    get "task_forms", on: :collection  #智慧门户接口显示单个站点的所有工单
  end

  #公用接口
  namespace :puclic do
    resources :public_alerts do
      get "modal", :on => :collection
    end
  end

  #公告人员管理
  resources :worker_manages do
    get "get_group_infos", on: :collection #获取组信息
  end
  #公告信息管理
  resources :notice_info_manages do
    get "get_all_groups", on: :collection  #获取所有组的成员名单
    get "send_message", on: :member        #发送操作
  end
  #公告模块管理
  resources :templete_manages

  #巡检工单管理
  resources :check_ups

  #获取用户名和Id
  resources :get_users
  #获取运维人员轨迹
  resources :flow_tracks
  #根据单位返回人员
  resources :unit_regions
  resources :localize    #时时定位


  #微信h5
  namespace :wx_html do
    resources :task_forms       # 工单信息
    resources :task_form_audits  # 工单任务审核
    resources :ops_plan_audits   # 运维计划审核
    resources :place_infos      # 到位统计
    resources :plan_done_infos  # 计划执行分析
    resources :task_done_infos  # 工单完成率
  end
  
  #智慧门户接口
  resources :get_wisdoms

  #数据监控模块
  namespace :data_view do
    resources :alarm_monitorings do        #报警图表监控
      get "init_index", on: :collection
    end
    resources :alarm_lists                 #报警汇总
    resources :alarm_maps do               #报警地图查看
      get "init_index", on: :collection
    end

    resources :exception_monitorings  do   #异常监控
      get "init", on: :collection
    end
    resources :d_alarms                    #历史查询
    resources :exception_sets              #异常设置
    resources :alarm_sets                  #报警设置
    resources :data_abnormal_form do       #数据异常单管理 （新增）
      get "audit", on: :member             #审核
    end

    resources :air_data_analyze  do        #上月监控数据统计，1小时，5min
      get "min5_datas", on: :collection    #详细页面查看5min数据
    end
  end


  #===========================
  #新增电子表格  公共接口形式
  resources :get_form_module  do 

    put "save", on: :member                 #校准模板保存

    #周
    get "run_status_week", on: :collection            #其他仪器、设备运行状况检查记录（每周）
    get "station_inspection_week", on: :collection    #站点巡检记录（每周）
    get "inspection_work_week", on: :collection       #每周（次）巡检工作汇总表
    get "pm_check_week", on: :collection              #颗粒物PM2.5自动监测分析仪运行状况检查记录（每周）
    get "optical_distance_week", on: :collection      #长光程（SO2、NO2、O3）分析仪运行状况检查记录表（每周）
    get "analyser_status_week", on: :collection       #臭氧(O3)分析仪运行状况检查记录表
    get "nox_analyser_status_week", on: :collection   #氮氧化物（NOX）分析仪运行状况检查记录表
    get "so2_analyser_status_week", on: :collection   #二氧化硫（SO2）分析仪运行状况检查记录表
    get "co_analyser_status_week", on: :collection    #一氧化碳（CO）分析仪运行状况检查记录表
    
    #月
    get "site_plant_maintenance_month", on: :collection       #站点设备维护记录（每月）
    get "gas_analyzer_FlowCheck_month", on: :collection       #气体分析仪流量检查记录表（每月）
    get "multiGas_dynamic_calibrator_month", on: :collection  #多气体动态校准仪校准检查记录表

    get "manual_comparison_particulates_month", on: :collection  #颗粒物手工比对采样记录表
    get "weighing_record_table", on: :collection                 #称量记录表
    get "sampling_record_table", on: :collection                 #采样记录表


    #季度
    get "multipoint_calibration_quarter", on: :collection              #气体分析仪（二氧化硫）多点校准记录表（每季度）
    get "instrument_precision_review_quarter", on: :collection         #(二氧化硫)仪器精密度审核记录表（每季度）
    get "site_equipment_cleaning_quarter", on: :collection             #站点设备清洁记录（每季度）
    get "grain_temperature_pressure_calibration_quarter", on: :collection       #颗粒物温度、压力校准记录表
    get "automatic_monitoring_analyzer_quarter", on: :collection                #颗粒物PM10自动监测分析仪运行状况检查记录（每季度）
    get "long_path_analyzer_quarter", on: :collection                  #长光程SO2分析仪单点校准检查记录表（每季度）
    get "o3long_path_analyzer_quarter", on: :collection                #长光程O3分析仪单点校准检查记录表（每季度）
    
    #年
    get "site_equipment_maintain_year", on: :collection          #站点设备半年维护记录（每半年）
    get "nox_analyzer_mofce_year", on: :collection               #氮氧化物分析仪钼炉转化率记录表（每半年） 
    get "multi_gas_dynamic_calibrator_year", on: :collection     #多气体动态校准仪校准检查记录表（每半年）） 
    get "visibility_analyzer_calibration_year", on: :collection  #能见度分析仪校准记录表（每半年） 


    get "o3_calibrator_value_transfer", on: :collection  #臭氧（O3）校准仪（工作标准）量值传递记录表（每半年）  
    

    get "replacement_recorder", on: :collection                         # 备机更换记录表
    get "atmosphere_automatic_monitoring_instrument", on: :collection   # 空气自动监测仪器设备检修记录表


    get "o3_monitoring_instrument_calibration", on: :collection    # 臭氧监测仪器校准数据报告
    get "co_monitoring_instrument_calibration", on: :collection    # 一氧化碳监测仪器校准数据报告
    get "so2_monitoring_instrument_calibration", on: :collection   # 二氧化硫监测仪器校准数据报告
    get "nox_monitoring_instrument_calibration", on: :collection   # 氮氧化物监测仪器校准数据报告
    get "pm10_check_calibration_record", on: :collection           # PM10检查校准记录表
    get "pm25_check_calibration_record", on: :collection           # PM2.5检查校准记录表

    get "get_values_klw", on: :collection           # PM2.5检查校准记录表
  end

  #resources :get_field_titles   #查询所有字段类型
  resources :form_module  do  #添加模板
    get "new_form_module", on: :collection  #新建模板
    get "table_infos", on: :collection      #首页模板信息
    get "show", on: :member                 #编辑页面
  end

  resources :field_title  do  #添加字段标题
    get "table_infos", on: :collection      #首页模板信息
    get "show", on: :member                 #编辑页面
  end

#定值因子统计
resources :d_fixed_nums do
  collection do
    get "get_item_code"  #获取因子
    get "export_file"  #导出表格
  end
end

#全年AQI级别统计
resources :d_get_years do
  collection do
    get "get_region_code" #获取区域
    get "get_year"  #获取年份
  end
end





  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index.html.erb'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
