# cronotab.rb — Crono configuration file
#
# Here you can specify periodic jobs and schedule.
# You can use ActiveJob's jobs from `app/jobs/`
# You can use any class. The only requirement is that
# class should have a method `perform` without arguments.
#
# class TestJob
#   def perform
#     puts 'Test!'
#   end
# end

Crono.perform(AbnormalMinute).every 5.minutes

Crono.perform(AbnormalHour).every 40.minutes

Crono.perform(AlarmRuleJob).every 2.minutes

Crono.perform( StationLostJob).every 30.minutes

Crono.perform( DayAccessStatistucsJob).every 1.days, at:{hour: 1, min: 00}  #站点小时条数

Crono.perform( StationItemAvgJob).every 1.days, at:{hour: 1, min: 45}  #站点 天 小时平均值

Crono.perform( DayHourAverageJob).every 1.days, at: {hour: 1, min: 18}


#AQI 小时数据
Crono.perform(AqiHourJob).every 21.minutes
# AQI 日报
Crono.perform(AqiDayJob).every 1.days, at:{hour: 2, min: 13}
