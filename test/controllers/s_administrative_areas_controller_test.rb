require 'test_helper'

class SAdministrativeAreasControllerTest < ActionController::TestCase
  setup do
    @s_administrative_area = s_administrative_areas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:s_administrative_areas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create s_administrative_area" do
    assert_difference('SAdministrativeArea.count') do
      post :create, s_administrative_area: { active_state: @s_administrative_area.active_state, des: @s_administrative_area.des, parent_id: @s_administrative_area.parent_id, sort_num: @s_administrative_area.sort_num, zone_name: @s_administrative_area.zone_name, zone_rank: @s_administrative_area.zone_rank }
    end

    assert_redirected_to s_administrative_area_path(assigns(:s_administrative_area))
  end

  test "should show s_administrative_area" do
    get :show, id: @s_administrative_area
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @s_administrative_area
    assert_response :success
  end

  test "should update s_administrative_area" do
    patch :update, id: @s_administrative_area, s_administrative_area: { active_state: @s_administrative_area.active_state, des: @s_administrative_area.des, parent_id: @s_administrative_area.parent_id, sort_num: @s_administrative_area.sort_num, zone_name: @s_administrative_area.zone_name, zone_rank: @s_administrative_area.zone_rank }
    assert_redirected_to s_administrative_area_path(assigns(:s_administrative_area))
  end

  test "should destroy s_administrative_area" do
    assert_difference('SAdministrativeArea.count', -1) do
      delete :destroy, id: @s_administrative_area
    end

    assert_redirected_to s_administrative_areas_path
  end
end
