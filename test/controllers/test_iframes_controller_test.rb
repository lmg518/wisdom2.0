require 'test_helper'

class TestIframesControllerTest < ActionController::TestCase
  setup do
    @test_iframe = test_iframes(:one)
  end


  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:test_iframes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create test_iframe" do
    assert_difference('TestIframe.count') do
      post :create, test_iframe: { mark: @test_iframe.mark, yangna: @test_iframe.yangna }
    end

    assert_redirected_to test_iframe_path(assigns(:test_iframe))
  end

  test "should show test_iframe" do
    get :show, id: @test_iframe
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @test_iframe
    assert_response :success
  end

  test "should update test_iframe" do
    patch :update, id: @test_iframe, test_iframe: { mark: @test_iframe.mark, yangna: @test_iframe.yangna }
    assert_redirected_to test_iframe_path(assigns(:test_iframe))
  end

  test "should destroy test_iframe" do
    assert_difference('TestIframe.count', -1) do
      delete :destroy, id: @test_iframe
    end

    assert_redirected_to test_iframes_path
  end
end
