require 'test_helper'

class SAreaLoginsControllerTest < ActionController::TestCase
  setup do
    @s_area_login = s_area_logins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:s_area_logins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create s_area_login" do
    assert_difference('SAreaLogin.count') do
      post :create, s_area_login: { d_login_msg_id: @s_area_login.d_login_msg_id, d_station_id: @s_area_login.d_station_id, s_administrative_area_id: @s_area_login.s_administrative_area_id }
    end

    assert_redirected_to s_area_login_path(assigns(:s_area_login))
  end

  test "should show s_area_login" do
    get :show, id: @s_area_login
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @s_area_login
    assert_response :success
  end

  test "should update s_area_login" do
    patch :update, id: @s_area_login, s_area_login: { d_login_msg_id: @s_area_login.d_login_msg_id, d_station_id: @s_area_login.d_station_id, s_administrative_area_id: @s_area_login.s_administrative_area_id }
    assert_redirected_to s_area_login_path(assigns(:s_area_login))
  end

  test "should destroy s_area_login" do
    assert_difference('SAreaLogin.count', -1) do
      delete :destroy, id: @s_area_login
    end

    assert_redirected_to s_area_logins_path
  end
end
