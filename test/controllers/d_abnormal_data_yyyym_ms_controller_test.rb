require 'test_helper'

class DAbnormalDataYyyymMsControllerTest < ActionController::TestCase
  setup do
    @d_abnormal_data_yyyymm = d_abnormal_data_yyyymms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:d_abnormal_data_yyyymms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create d_abnormal_data_yyyymm" do
    assert_difference('DAbnormalDataYyyymm.count') do
      post :create, d_abnormal_data_yyyymm: { ab_data_time: @d_abnormal_data_yyyymm.ab_data_time, ab_desc: @d_abnormal_data_yyyymm.ab_desc, ab_lable: @d_abnormal_data_yyyymm.ab_lable, ab_value: @d_abnormal_data_yyyymm.ab_value, abnormal_id: @d_abnormal_data_yyyymm.abnormal_id, compare_value2: @d_abnormal_data_yyyymm.compare_value2, compare_value3: @d_abnormal_data_yyyymm.compare_value3, compare_value: @d_abnormal_data_yyyymm.compare_value, create_acce: @d_abnormal_data_yyyymm.create_acce, rule_instance: @d_abnormal_data_yyyymm.rule_instance, station_id: @d_abnormal_data_yyyymm.station_id }
    end

    assert_redirected_to d_abnormal_data_yyyymm_path(assigns(:d_abnormal_data_yyyymm))
  end

  test "should show d_abnormal_data_yyyymm" do
    get :show, id: @d_abnormal_data_yyyymm
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @d_abnormal_data_yyyymm
    assert_response :success
  end

  test "should update d_abnormal_data_yyyymm" do
    patch :update, id: @d_abnormal_data_yyyymm, d_abnormal_data_yyyymm: { ab_data_time: @d_abnormal_data_yyyymm.ab_data_time, ab_desc: @d_abnormal_data_yyyymm.ab_desc, ab_lable: @d_abnormal_data_yyyymm.ab_lable, ab_value: @d_abnormal_data_yyyymm.ab_value, abnormal_id: @d_abnormal_data_yyyymm.abnormal_id, compare_value2: @d_abnormal_data_yyyymm.compare_value2, compare_value3: @d_abnormal_data_yyyymm.compare_value3, compare_value: @d_abnormal_data_yyyymm.compare_value, create_acce: @d_abnormal_data_yyyymm.create_acce, rule_instance: @d_abnormal_data_yyyymm.rule_instance, station_id: @d_abnormal_data_yyyymm.station_id }
    assert_redirected_to d_abnormal_data_yyyymm_path(assigns(:d_abnormal_data_yyyymm))
  end

  test "should destroy d_abnormal_data_yyyymm" do
    assert_difference('DAbnormalDataYyyymm.count', -1) do
      delete :destroy, id: @d_abnormal_data_yyyymm
    end

    assert_redirected_to d_abnormal_data_yyyymms_path
  end
end
